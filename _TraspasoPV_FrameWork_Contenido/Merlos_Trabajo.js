<div id="dvTrabajoInputs" class="tbSeccion">
    <div>
        NÚMERO
        <br>
        <input type="text" id="inpNumero" class="tecNum sld">
    </div>
    <div>
        SERIE
        <br>
        <input type="text" id="inpSerie" class='clsSerie' onclick="$(this).select();" onkeyup="this.value=this.value.toUpperCase();">
        <div id="dvSeries" class="dvTrabajoInp"></div>
    </div>
    <div>
        ZONA
        <br>
        <input type="text" id="inpZona" class='clsZona' onkeyup="this.value=this.value.toUpperCase();">
        <div id="dvZonas" class="dvTrabajoInp"></div>
    </div>
    <div>
        RUTA
        <br>
        <input type="text" id="inpRuta" class='clsRuta' onclick="$(this).select();">
        <div id="dvRutas" class="dvTrabajoInp"></div>
    </div>
    <div>
        ARTÍCULO
        <br>
        <input type="text" id="inpArticulo" class='clsArticulo sld' onclick="$(this).select();">
    </div>
    <div>
        ENTREGA
        <br>
        <merlos-calendar id="inpEntrega" class="clsEntrega sld"></merlos-calendar>
    </div>
    <div class="btns taC" style="padding:0;"><div class="btnAzul esq05 dvBtn" onclick="flexygo.nav.execProcess('GoHome','','',null,null,'current',false,$(this));">Limpiar</div></div>
    <div class="btns taC" style="padding:0;"><div class="btnVerde esq05 dvBtn" onclick="buscarGlobal()">buscar</div></div>
</div>

<div id="dvResultados" class="tbSeccion" style="margin-top:20px;"></div>
<div id="dvDatosDePedido" class="tbSeccion"></div>
<div id="dvLineasDePedido" class="tbSeccion"></div>
<div id="dvLineasDeArticulo" class="tbSeccion"></div>

<div id="dvCambiarArticulos" class="tbSeccion dv inv">
    <div class="dvTit">Cambiar Artículos
        <img src="./Merlos/images/cerrar.png" style="float:right; width:30px; margin-top:-5px; cursor:pointer;" onclick="cerrarCambiarArticulos()">
    </div>
    <div id="dvCambiarArticulosContenido" style="padding:10px;"></div>
</div> 

<div id="dvCambiarArticulosEq" class="tbSeccion dv inv">
    <div class="dvTit">Cambiar Artículos - Equivalencias
        <img src="./Merlos/images/cerrar.png" style="float:right; width:30px; margin-top:-5px; cursor:pointer;" onclick="cerrarCambiarArticulosEq()">
    </div>
    <div id="dvCambiarArticulosEqContenido" style="padding:10px;"></div>
</div> 


<script>
    // Comprobar Ejercicio en Curso ----------------------------------------------------------------------------------------------------------------------------
    var parametros = '{"sp":"pConfiguracion","modo":"ComprobarEjercicio"}';
    flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
        if(ret.JSCode!=="OK"){ console.log("Se ha actualizado el portal al ejercicio "+ret.JSCode); }
    }else{ alert('Error S.P. pConfiguracion - ComprobarEjercicio!!!\n'+JSON.stringify(ret)); } }, false);
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------

    $("#mainNav, .tbSeccion").hide();
    $("#inpEntrega-input").val(fechaDeManyana());

    var SeriesCargadas = false;
    var RutasCargadas = false;
    var ZonasCargadas = false;
    var btnMerlosTraspasoIO = false;
    var facturaDirecta = "";
    var FacturaDirecta = 0;
    var cambiarArticuloListado = [];
    var modificarPedidoListado = [];
    var listadoArticulos = "";
    var contadorTraspasoEnUso = 0;
    var modificandoPedido = false;
    var cambiandoArticulo = false;

    var spnOrden_numero = 0;

    if (IntegridadVerificada) { iniciar(); } else { ComprobarIntegridad(); }

    function ComprobarIntegridad() {
        abrirVelo(icoCarga50 + "<br><br>comprobando la integridad de la aplicación...");
        var parametros = '{"sp":"pConfiguracion","modo":"comprobarConfiguracionEmpresa",' + params + '}';
        flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
            var js = JSON.parse(limpiarCadena(ret.JSCode));
            if(js.error=="NoExisteConfiguracion"){ 
                if (currentRole === "Admins") { flexygo.nav.openPageName('Merlos_Administrador', '', '', '', 'current', false, $(this)); }
                else {
                    abrirVelo(
                        icoAlert50
                        + "<h1 style='font:24px arial; color:#900;'>No existe una configuración!</h1>"
                        + "<h2 style='font:18px arial; color:#900;'>Póngase en contacto con su distribuidor.</h2>"
                    );
                }
            }
            else{  cerrarVelo(); iniciarTrabajo(); }
        } else { alert('Error SP pConfiguracion - comprobarConfiguracionEmpresa!' + JSON.stringify(ret)); } }, false);
    }

    function iniciarTrabajo() {
        $("#dvTrabajoInputs").slideDown();
        cargarSeries();
        cargarRutas();
        cargarZonas();
        activarObjetos();
        cargarUsuarioFiltro();
    }


    $(document).keypress(function (e) {
        if (e.keyCode === 13) {
            if (ControlActivo === "entradaPesoManual") { capturarPesoDeSuma($.trim($("#entradaPesoManual").val())); }
            else {
                if      ($("#nBultos").is(":focus")) { $("nCajas").focus(); return; }
                else if ($("#nBultos").is(":focus")) { generarAlbaranDeVenta(); return; }
                else if ($("#nBultos, #nCajas, .inpPesoArt, .tecV, #inpNumero").is(":focus")) { tecladoNumerico("ENTER"); }
                else if ($(".tecNum, #inpArticulo").is(":focus")) { buscarGlobal(); }
                else if ($("#inpRuta").is(":focus")) { asignarRuta($.trim($("#inpRuta").val())); }
            }
        }else {
            if ($("#tecladoNumericoPantalla").is(":visible")) { e.preventDefault(); tecladoNumerico(String.fromCharCode(e.which)); }
        }
    });

    $(".sld").on("click", function () { $(".dvTrabajoInp").slideUp(); });


    function activarObjetos() {
        ControlActivo = ClaseActiva = "";

        $("input").off().on("click", function () {
            ControlActivo = this.id;
            ClaseActiva = $(this).attr("class").split(" ")[0];

            if ($("#" + ControlActivo).hasClass("noInput")) { return; }

            if (ControlActivo === "inpNumero") { tecladoModo = "Buscar"; }

            if       (ClaseActiva === "clsSerie") { mostrarSeries(); }
            else if  (ClaseActiva === "clsZona")  { mostrarZonas(); }
            else if  (ClaseActiva === "clsRuta")  { mostrarRutas(); }
            else if  (ClaseActiva === "tecNum")   { ControlActivo = this.id; $("#" + ControlActivo).val(""); tecladoNum(ControlActivo); $("#dvRutas").hide(); }
            else if  (ClaseActiva === "tecAlf")   { tecladoAlf(ControlActivo); $("#dvRutas").hide(); }
        });

        $(".imgImp").off().on("click", function () {
            var imp = $(this).attr("class").split(" ")[1];
            if (imp === "alb") {
                if (imprimirAlbaran === 0) {
                    imprimirAlbaran = 1;
                    $("#btnImpAlb").attr("src", "./Merlos/images/btnImprimirAlbaranI.png");
                    $("#btnGenerarAlbaran").html("GENERAR ALBARÁN");
                }
                else {
                    imprimirAlbaran = 0;
                    $("#btnImpAlb").attr("src", "./Merlos/images/btnImprimirAlbaranO.png");
                    $("#btnGenerarAlbaran").html("IMPRIMIR ETIQUETAS");
                }
            }
            if (imp === "eti") {
                if (imprimirEtiquetas === 0) { imprimirEtiquetas = 1; $("#btnImpEti").attr("src", "./Merlos/images/btnImprimirEtiquetasI.png"); }
                else { imprimirEtiquetas = 0; $("#btnImpEti").attr("src", "./Merlos/images/btnImprimirEtiquetasO.png"); }
            }

            if (imprimirAlbaran === 0 && imprimirEtiquetas === 0) { $("#btnGenerarAlbaran").hide(); } else { $("#btnGenerarAlbaran").show(); }
        });

        BotoneraEstado();
    }

    function BotoneraEstado(){
        // inicializar botones
        $(".btnObservaciones img").attr("src","./Merlos/images/btnObservaciones.png").addClass("btnAlo curP");
        $(".btnPreparacion img").attr("src","./Merlos/images/btnPrep0_O.png").removeClass("btnAlo curP"); 
        $(".btnRecargar img").attr("src","./Merlos/images/btnRecargarC.png").addClass("btnAlo curP");
        $(".btnCambiarArt img").attr("src","./Merlos/images/btnCambiarArtC.png").addClass("btnAlo curP");
        $(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedido.png").addClass("btnAlo curP");
        $(".btnMerlosAtras img").attr("src","./Merlos/images/btnMerlosAtrasC.png").addClass("btnAlo curP");
        $(".btnMerlosPreparar img").attr("src","./Merlos/images/Preparar.png").addClass("btnAlo curP"); 
        $(".btnMerlosTraspaso img").attr("src","./Merlos/images/Traspasar.png").addClass("btnAlo curP");
        $(".btnTraspasoCompleto img").attr("src","./Merlos/images/TraspasoCompleto.png").addClass("btnAlo curP");
        $(".btnCancelarPreparacion img").attr("src","./Merlos/images/CancelarPreparacion.png").addClass("btnAlo curP");
        $(".btnFinalizarPedido img").attr("src","./Merlos/images/FinalizarPedido.png").addClass("btnAlo curP");

        // botones estados
        if( $("#dvLineasDePedidoTraspaso").is(":visible") ){
            $(".btnPreparacion img").attr("src","./Merlos/images/btnPrep0_O.png").removeClass("btnAlo curP"); 
            $(".btnMerlosPreparar img").attr("src","./Merlos/images/Preparar_O.png").removeClass("btnAlo curP"); 
        }

        if( $("#dvLineasDeArticulo").is(":visible") ){
            $(".btnCambiarArt img").attr("src","./Merlos/images/btnCambiarArtC_O.png").removeClass("btnAlo curP"); 
            $(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedido_O.png").removeClass("btnAlo curP"); 
            $(".btnPreparacion img").attr("src","./Merlos/images/btnPrep0.png").addClass("btnAlo curP");
        }

        if(!btnMerlosTraspasoIO){$(".btnMerlosTraspaso img").attr("src","./Merlos/images/Traspasar_O.png").removeClass("btnAlo curP"); }

        if(modificandoPedido){
            $(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedidoVerde.png").addClass("btnAlo curP");
            $("#tbDescripcion").css("background","#DBFFD6").html("Modificación del Pedido");

            $(".btnMerlosPreparar img").attr("src","./Merlos/images/Preparar_O.png").removeClass("btnAlo curP"); 
            $(".btnRecargar img").attr("src","./Merlos/images/btnRecargarC_O.png").removeClass("btnAlo curP"); 
            $(".btnCambiarArt img").attr("src","./Merlos/images/btnCambiarArtC_O.png").removeClass("btnAlo curP"); 
            $(".btnMerlosTraspaso img").attr("src","./Merlos/images/Traspasar_O.png").removeClass("btnAlo curP"); 
            $(".btnTraspasoCompleto img").attr("src","./Merlos/images/TraspasoCompleto_O.png").removeClass("btnAlo curP"); 
            $(".btnPreparacion img").attr("src","./Merlos/images/btnPrep0_O.png").removeClass("btnAlo curP"); 
            $(".btnMerlosAtras img").attr("src","./Merlos/images/btnMerlosAtrasC_O.png").removeClass("btnAlo curP"); 
        }

        if(cambiandoArticulo){
            $(".btnCambiarArt img").attr("src","./Merlos/images/CambiarArticuloVerde.png").addClass("btnAlo curP");
            $("#tbDescripcion").css("background","#DBFFD6").html("Cambiar Artículo con equivalente");

            $(".btnMerlosPreparar img").attr("src","./Merlos/images/Preparar_O.png").removeClass("btnAlo curP"); 
            $(".btnRecargar img").attr("src","./Merlos/images/btnRecargarC_O.png").removeClass("btnAlo curP"); 
            $(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedido_O.png").removeClass("btnAlo curP"); 
            $(".btnMerlosTraspaso img").attr("src","./Merlos/images/Traspasar_O.png").removeClass("btnAlo curP"); 
            $(".btnTraspasoCompleto img").attr("src","./Merlos/images/TraspasoCompleto_O.png").removeClass("btnAlo curP"); 
            $(".btnPreparacion img").attr("src","./Merlos/images/btnPrep0_O.png").removeClass("btnAlo curP"); 
            $(".btnMerlosAtras img").attr("src","./Merlos/images/btnMerlosAtrasC_O.png").removeClass("btnAlo curP"); 
        }
    }

    function cargarUsuarioFiltro(){
        var parametros = '{"sp":"pUsuario","modo":"dameFiltro",' + params + '}';
        flexygo.nav.execProcess('pMerlos', '', null, null, [{ 'Key': 'parametros', 'Value': limpiarCadena(parametros) }], 'current', false, $(this), function (ret) { if (ret) {
            var js = JSON.parse(limpiarCadena(ret.JSCode));
            if(js.length>0){ usuarioFiltro = js.filtro+";"+js.valor; }else{ usuarioFiltro=""; }
        } else { alert('Error SP pUsuario - dameFiltro!' + JSON.stringify(ret)); } }, false);
    }

    function tecladoNum(elControl) {
        cntrl = "teclado_" + elControl;
        abrirVeloTeclado("<div id='dvTeclado'></div>", 1);
        $("#dvTeclado").load("./Merlos/content/tecladoNum.html");
    }

    function tecladoNumerico(num) {
        if (num === "ENTER") {
            // entrada de peso manual
            if (ControlActivo === "entradaPesoManual") { capturarPesoDeSuma($.trim($("#entradaPesoManual").val())); }
            // cálculo de unidades/cajas SIN LOTE
            else if (ControlActivo === "sinLoteUDS") { cerrarVeloTeclado(); }
            else if (ControlActivo === "sinLoteCAJAS") { calcularUDSDesdeSinLoteCAJAS(); }
            // cálculo de unidades/cajas CON LOTE
            else if (ControlActivo.substring(0, 20) === "inp_LoteEntradaCajas") { calcularUdsDesdeConLoteCAJAS(ControlActivo); }
            else if (ControlActivo.substring(0, 18) === "inp_LoteEntradaUds") { calcularCajasDesdeConLoteUDS(ControlActivo); ventaCajas(ControlActivo); cerrarVeloTeclado(); }

            else if (ControlActivo === "inpNumero") { buscarGlobal(); cerrarVeloTeclado(); }
            else if (ControlActivo === "nBultos") { replicarBultos(); cerrarVeloTeclado(); $("#nBultos").focus(); }
            else { cerrarVeloTeclado(); }
        } else {
            var CntrlText = "";
            if (num === "<") { CntrlText = CntrlText.substr(0, CntrlText.length - 1); }
            else if (num === "<<") { CntrlText = ""; }
            else if (num === "") { $("#dvTeclado").slideUp(); cerrarVeloTeclado(); }
            else { CntrlText = $("#" + ControlActivo).val() + num; }
            $("#" + ControlActivo).val(CntrlText);
            $("#tecladoNumericoPantalla").text(CntrlText);
        }
    }

    function cargarSeries() {
        if (!SeriesCargadas) {
            $("#inpSerie").val("cargando series...").css("color", "red");
            var parametros = '{"sp":"pSeries"}';
            flexygo.nav.execProcess('pMerlos', '', null, null, [{ 'Key': 'parametros', 'Value': limpiarCadena(parametros) }], 'current', false, $(this), function (ret) {
                if (ret) {
                    var contenido = "";
                    var js = JSON.parse(limpiarCadena(ret.JSCode));
                    if (js.length > 0) {
                        for (var i = 0; i < js.length; i++) { contenido += "<div class='dvSub' onclick='asignarSerie(\"" + js[i].codigo + " - " + js[i].nombre + "\")'>" + js[i].codigo + "-" + js[i].nombre + "</div>"; }
                    }
                    $("#dvSeries").html(contenido);
                    $("#inpSerie").val("").css("color", "#333333");
                } else { alert('Error SP pSeries!' + JSON.stringify(ret)); }
            }, false);
        }
    }

    function cargarZonas() {
        $("#inpZona").val("cargando zonas...").css("color", "red");
        var parametros = '{"sp":"pZonas"}';
        flexygo.nav.execProcess('pMerlos', '', null, null, [{ 'Key': 'parametros', 'Value': limpiarCadena(parametros) }], 'current', false, $(this), function (ret) {
            if (ret) {
                var contenido = "";
                var js = JSON.parse(limpiarCadena(ret.JSCode));
                if (js.length > 0) {
                    for (var i = 0; i < js.length; i++) { contenido += "<div class='dvSub' onclick='asignarZona(\""+js[i].VALOR+"\")'>" + js[i].VALOR.toUpperCase() + "</div>"; }
                }
                $("#dvZonas").html(contenido);
                $("#inpZona").val("").css("color", "#333333");
            } else { alert('Error SP pZonas!' + JSON.stringify(ret)); }
        }, false);
    }

    function cargarRutas() {
        $("#inpRuta").val("cargando rutas...").css("color", "red");
        var parametros = '{"sp":"pRutas"}';
        flexygo.nav.execProcess('pMerlos', '', null, null, [{ 'Key': 'parametros', 'Value': limpiarCadena(parametros) }], 'current', false, $(this), function (ret) {
            if (ret) {
                var contenido = "";
                var js = JSON.parse(limpiarCadena(ret.JSCode));
                if (js.length > 0) {
                    for (var i = 0; i < js.length; i++) { contenido += "<div class='dvSub' onclick='asignarRuta(\"" + js[i].codigo + "\",\"" + js[i].nombre + "\")'>" + js[i].codigo + "-" + js[i].nombre + "</div>"; }
                }
                $("#dvRutas").html(contenido);
                $("#inpRuta").val("").css("color", "#333333");
            } else { alert('Error SP pRutas!' + JSON.stringify(ret)); }
        }, false);
    }

    function mostrarSeries() {
        $(".teclado, .resultados").hide();
        if ($("#dvSeries").is(":visible")) { $("#dvSeries").slideUp(); }
        else { $(".dvTrabajoInp").hide(); $("#dvSeries").slideDown(); }
    }

    function mostrarZonas() {
        $(".teclado, .resultados").hide();
        if ($("#dvZonas").is(":visible")) { $("#dvZonas").slideUp(); }
        else { $(".dvTrabajoInp").hide(); $("#dvZonas").slideDown(); }
    }

    function mostrarRutas() {
        $(".tdRuta").removeClass("inv");
        $(".teclado, .resultados").hide();
        if ($("#dvRutas").is(":visible")) { $("#dvRutas").slideUp(); }
        else {
            $(".dvTrabajoInp").hide(); $("#dvRutas").slideDown();
            if (Left($("#inpSerie").val(),2) === "ET") { 
                $("#dvRutas").find(".dvSub").each(function () {
                    if (($(this).text()).substring(0, 1) !== "E" && ($(this).text()).substring(0, 1) !== "A") { $(this).addClass("inv"); } else { $(this).removeClass("inv"); }
                });
            }
        }
    }

    function asignarRuta(codigo, nombre) {
        $("#inpRuta").val(codigo + " - " + nombre);
        $("#dvRutas").hide();
        Ruta = trsps_ruta = codigo;
        buscarGlobal();
    }

    function asignarSerie(codigo, nombre) {
        $("#inpSerie").val(codigo + " - " + nombre);
        $("#dvSeries").hide();
        Serie = trsps_serie = codigo;
        buscarGlobal();
    }

    function asignarZona(Zona) {
        $("#inpZona").val(Zona);
        $("#dvZonas").hide();
        Zona = trsps_serie = $.trim($("#inpZona").val()).toUpperCase();
        buscarGlobal();
    }

    function pedidoEnUso(numero, letra, ruta, n_ruta, usuario) {
        abrirVelo(
            "<div id='veloAvDesbloqueo' class='taC' style='font:16px arial; color:#333;'>"
            + icoAlert50 + "<br><br>"
            + "El pedido " + numero + " (" + letra + ") está EN USO por el usuario " + usuario
            + "	<br><br><br><span class='btnVerde esq05' onclick='desbloquearPedido(\"" + numero + "\",\"" + letra + "\",\"" + ruta + "\",\"" + n_ruta + "\",\"" + Almacen + "\");'>desbloquear</span>"
            + "	&nbsp;&nbsp;&nbsp;<span class='btnRojo esq05' onclick='cerrarVelo();'>cancelar</span>"
            + "</div>"
        );
    }

    function desbloquearPedido(numero, letra, ruta, RutaNombre, Almacen) {
        $("#veloAvDesbloqueo").html(infoCarga("desbloqueando el pedido " + numero + " (" + letra + ") ..."));
        var parametros = '{"sp":"pDesbloquearPedido","numero":"' + numero + '","letra":"' + letra + '","ruta":"' + ruta + '",' + params + '}';
        flexygo.nav.execProcess('pMerlos', '', null, null, [{ 'Key': 'parametros', 'Value': limpiarCadena(parametros) }], 'current', false, $(this), function (ret) {
            if (ret) {
                cerrarVelo();
                verLineasDelPedido(numero, letra, ruta, RutaNombre, Almacen);
            } else { alert('Error SP pSeries!' + JSON.stringify(ret)); }
        }, false);
    }

    function buscarGlobal(campo) {
        if (campo === undefined || campo === "undefined") { campo = ""; }
        var bNumero = $.trim($("#inpNumero").val());
        var bSerie = $.trim($("#inpSerie").val()).split(" - ")[0];
        var bRuta = $.trim($("#inpRuta").val()).split(" - ")[0];
        var bZona = $.trim($("#inpZona").val());
        var bArticulo = $.trim($("#inpArticulo").val());
        Entrega = $.trim($("#inpEntrega-input").val());
        if (Entrega !== "" && Left(Entrega, 4).indexOf("-") === -1) { Entrega = Entrega.substr(8, 2) + "-" + Entrega.substr(5, 2) + "-" + Entrega.substr(0, 4); }
        Serie = bSerie;
        $(".tbSeccion, .teclado").hide();
        $("#dvTrabajoInputs").show();
        $("#dvResultados").html(infoCarga("buscando pedidos")).slideDown();

        var parametros = '{"sp":"pBuscarGlobal","pedidoActivo":"' + PedidoActivo + '","numero":"' + bNumero + '","serie":"' + bSerie + '","ruta":"' + bRuta + '"'
            + ',"zona":"' + bZona + '","articulo":"' + bArticulo + '","entrega":"' + Entrega + '","campo":"' + campo + '",' + params + '}';
        flexygo.nav.execProcess('pMerlos', '', null, null, [{ 'Key': 'parametros', 'Value': limpiarCadena(parametros) }], 'current', false, $(this), function (ret) { 
            if (ret) { 
                var js = JSON.parse(limpiarCadena(ret.JSCode));
                var contenido = "";
                if (js.length > 0) {
                    contenido = `<div style='font:bold 14px arial; color:#333; background:#FFF;padding:6px;'>
                                    Lista de pedidos
                                    <span style='float:right;'>
                                        <span style='font:11px arial; color:#07657e; margin-right:20px;'>clic en la cabecera de la tabla para ordenar</span>
                                        <span class='btnAmarillo' onclick='eliminarFiltros()'>eliminar filtros</span>
                                    </span>
                                </div>
                                <table id='tbResultados' class='tbStd' style='border-collapse:Separate; border:1px solid #CCC;'>
                                <tr>
                                    <th style='width:130px;' onclick='buscarGlobal("numero")' style='white-space: nowrap;'>NÚMERO <span id='spnOrden_numero'></span></th>
                                    <th style='width:100px;' onclick='buscarGlobal("letra")' style='white-space: nowrap;'>SERIE <span id='spnOrden_letra'</th>
                                    <th style='width:130px;' onclick='buscarGlobal("fecha")' style='white-space: nowrap;'>FECHA <span id='spnOrden_fecha'</th>
                                    <th style='width:130px;' onclick='buscarGlobal("entrega")' style='white-space: nowrap;'>ENTREGA <span id='spnOrden_entrega'</th>
                                    <th onclick='buscarGlobal("nombre")' style='white-space: nowrap;'>NOMBRE <span id='spnOrden_nombre'</th>
                                    <th onclick='buscarGlobal("rComercial")' style='white-space: nowrap;'>R. COMERCIAL <span id='spnOrden_rComercial'</th>
                                </tr>`;
                    for (var i in js) {
                        var colorFE = " color:#333; ";
                        var colorT  = " color:#333; ";
                        facturaDirecta = ""; if (js[i].FACTDIRV) { facturaDirecta = "<span style='font:bold 14px arial;color:orange;'>(Factura Directa)</span>"; }
                        if (parseFloat(js[i].servidas) > 0 || parseFloat(js[i].bNumero) > 0) { colorT = " color:#FF6100; "; /* naranja */ }
                        var fechaCorta = js[i].fecha.substr(8, 2) + "-" + js[i].fecha.substr(5, 2) + "-" + js[i].fecha.substr(0, 4);
                        var entregaCorta = js[i].entrega.substr(8, 2) + "-" + js[i].entrega.substr(5, 2) + "-" + js[i].entrega.substr(0, 4);
                        var esteOnClick = "onclick='FacturaDirecta=\"" + js[i].FACTDIRV + "\"; verLineasDelPedido(\"" + js[i].numero + "\",\"" + js[i].letra + "\",\"" + js[i].ruta + "\",\"" + js[i].n_ruta + "\",\"" + js[i].almacen + "\")'";
                        if (parseInt(fechaNum(js[i].fecha)) >= parseInt(fechaNum(fechaMasDias(2,"amd")))) { colorFE = "color:red;"; }
                        // comprobamos si está EN_USO
                        if (js[i].TIPO != null) {
                            if ($.trim(js[i].CLAVE) == $.trim(js[i].EN_USO) && $.trim(js[i].USUARIO) !== $.trim(currentReference)) {
                                var colorT = " color:#900; ";
                                var esteOnClick = "onclick='pedidoEnUso(\"" + js[i].numero + "\",\"" + js[i].letra + "\",\"" + js[i].ruta + "\",\"" + js[i].n_ruta + "\",\"" + $.trim(js[i].USUARIO) + "\")'";
                            }
                        }
                        contenido += "<tr " + esteOnClick + ">"
                            + "     <td style='" + colorT + "'>" + js[i].numero + "</td>"
                            + "     <td style='" + colorT + "'>" + js[i].letra + "</td>"
                            + "     <td style='" + colorT + "'>" + fechaCorta + "</td>"
                            + "     <td style='" + colorFE + "'>" + entregaCorta + "</td>"
                            + "     <td style='" + colorT + "'>" + js[i].nombre + " " + facturaDirecta + "</td>"
                            + "     <td style='" + colorT + "'>" + js[i].rComercial + "</td>"
                            + " </tr>";
                    }
                    contenido += "</table></div>";
                } else { contenido = "Sin resultados!"; }
                $(".tbSeccion, .teclado").hide();
                $("#dvTrabajoInputs").show();
                $("#dvResultados").html(contenido).slideDown();
                // pintar Filtros
                if (js.length > 0) {
                    var jsf = js[0].filtros;
                    for(var i in jsf) {  $("#spnOrden_"+jsf[i].campo).html(window["spOrden"+jsf[i].orden]);  }
                }
            } else { alert('Error SP pBuscarGlobal!' + JSON.stringify(ret)); }
        }, false);
    }

    function eliminarFiltros(){
        var parametros = '{"sp":"pUsuario","modo":"eliminarFiltros",' + params + '}';
        flexygo.nav.execProcess('pMerlos', '', null, null, [{ 'Key': 'parametros', 'Value': limpiarCadena(parametros) }], 'current', false, $(this), function (ret) {if (ret) { 
                buscarGlobal();
        } else { alert('Error SP pUsuario - eliminarFiltros!\n' + JSON.stringify(ret)); } }, false);
    }

    function verLineasDelPedido(pedido, serie, ruta, nombreRuta, almacen) {
        var laRuta = ruta.split(" - ")[0];
        Numero = pedido;
        NumPedido = pedido;
        PedidoSerie = serie;
        Serie = serie;
        Ruta = laRuta;
        RutaNombre = nombreRuta;
        Almacen = almacen;
        modificacionesCancelar(1);
        cambiarArticuloListado=[];
        buscarPedido(pedido, serie, Ruta, nombreRuta);
    }

    function buscarPedido(pedido, serie, ruta, nombreRuta) {
        gPedido = trsps_numero = pedido;
        Serie = trsps_serie = serie;
        trsps_ruta = ruta;
        trsps_nRuta = nombreRuta;
        btnMerlosTraspasoIO=false;
        var zona = $.trim($("#inpZona").val());        
        var parametros = '{"sp":"pBuscarPedido","pedidoActivo":"' + PedidoActivo + '","pedido":"' + pedido + '","serie":"' + serie + '","ruta":"' + ruta + '","nombreRuta":"' + nombreRuta + '","zona":"' + zona + '",' + params + '}';
        flexygo.nav.execProcess('pMerlos', '', null, null, [{ 'Key': 'parametros', 'Value': limpiarCadena(parametros) }], 'current', false, $(this), function (ret) {
            if (ret) { 
                if ((ret.JSCode).split("!")[0] === "enUSO") { pedidoEnUso(pedido,serie,ruta,nombreRuta,(ret.JSCode).split("!")[1]); return; }
                $(".tbSeccion, .teclado").hide();
                $("#dvLineasDePedido").html(infoCarga("cargando el pedido " + pedido + "...")).slideDown();
                var js = JSON.parse(limpiarCadena(ret.JSCode));
                if (js.length > 0) {
                    PedidoActivo = js[0].PedidoActivo;
                    var cliObs = js[0].ClienteObs.replace(/"/g, '-');
                    var pedidoObs = js[0].PedidoObs.replace(/"/g, '-');
                    var rutaTxt = "<span style='margin-right:30px;font:bold 20px arial;color:#07657e;'>RUTA: " + ruta + " - " + nombreRuta + "</span>"; /* azul oscuro */
                    if (ruta.trim() === "" || ruta.trim() === undefined || ruta.trim() === "null") { rutaTxt = ""; }

                    var laTabla = "<table id='tbDatosDePedido' style='width:100%;border-collapse:collapse; border-bottom:1px solid #07657e;'>"
                        + "	    <tr>"
                        + "     	<td style='vertical-align:top;'>"
                        + "         	<span style='font:bold 20px arial; color:#07657e; '>Traspaso del Pedido " + pedido + "-" + serie + "</span><br>" 
                        + "         	<span id='spanDatosCliente' style='margin-right:30px;'></span>"
                        + "     	</td>"
                        + "     	<td style='vertical-align:top; padding-left:50px;'>" + rutaTxt + "</td>"
                        + "     	<td style='vertical-align:top; width:450px;'>"
                        + "				<div class='dvBotoneraOpciones'>"
                        + "				    <div class='btnObservaciones'><img src='./Merlos/images/btnObservaciones.png' style='width:100%' class='btnAlo curP' onclick='mostrarObservaciones(\"" + cliObs + "\",\"" + pedidoObs + "\")'></div>"
                        + "				    <div class='btnPreparacion'><img src='./Merlos/images/btnPrep0.png' style='width:100%'' class='btnAlo curP' onclick='borrarPropuesta()'></div>"
                        + "				    <div class='btnRecargar'><img src='./Merlos/images/btnRecargarC.png' style='width:100%' class='btnAlo curP' onclick='recargar()'></div>"
                        + "				    <div class='btnCambiarArt'><img src='./Merlos/images/btnCambiarArtC.png' style='width:100%' class='btnAlo curP' onclick='cambiarArticulos(\"" + pedido + "\",\"" + serie + "\",\"" + ruta + "\")'></div>"
                        + "				    <div class='btnModificarPedido'><img src='./Merlos/images/ModificarPedido.png' style='width:100%' class='btnAlo curP' onclick='modificarPedido()'></div>"
                        + "				    <div class='btnMerlosAtras'><img src='./Merlos/images/btnMerlosAtrasC.png' style='width:100%;' class='btnAlo curP' onclick='traspasarAtras()'></div>"

                        + "				    <div class='btnMerlosPreparar'><img src='./Merlos/images/Preparar.png' style='width:100%;' class='btnAlo curP' onclick='traspasarArtLote()'></div>"
                        + "				    <div class='btnMerlosTraspaso'><img src='./Merlos/images/Traspasar.png' style='width:100%;' class='btnAlo curP' onclick='traspasarPedido()'></div>"
                        + "				    <div class='btnTraspasoCompleto'><img src='./Merlos/images/TraspasoCompleto.png' style='width:100%;' class='btnAlo curP' onclick='traspasoCompleto()'></div>"
                        + "				    <div class='btnCancelarPreparacion'><img src='./Merlos/images/CancelarPreparacion.png' style='width:100%;' class='btnAlo curP' onclick='cancelarPreparacion()'></div>"
                        + "				    <div class='btnFinalizarPedido'><img src='./Merlos/images/FinalizarPedido.png' style='width:100%;' class='btnAlo curP' onclick='finalizarPedido()'></div>"
                        + "				</div>"
                        + "				<div style='height:4px;'></div>"
                        + "     	</td>"
                        + "		</tr>"
                        + "</table>"
                        + "<div id='dvModificarPedido' class='inv'></div>"
                        + "<div id='dvLineasDePedidoTraspaso' style='margin-top:10px;'>"
                        + "		<div id='dvLineasDePedidoTraspasoCB'>"
                        + "			<input type='text' id='inpLineasDePedidoTraspasoCB' class='cb' "
                        + "			style='background:#FFF; width:100%; padding:4px; border:none; outline:none; border-bottom:1px solid #07657e; text-align:center; font:14px arial; color:#666;' "
                        + "			placeholder='código de barras' onkeyup='cbKeyUp()' "
                        + "			onclick='$(this).select()'>"
                        + "		</div>"
                        + "		<table id='tbLineasDePedidoTraspaso' class='tbStd' style='margin-top:4px;'>"
                        + "  		<tr id='tbLineasDePedidoTraspasoCabecera00' style='border-bottom:1px solid #FFF;'>"
                        + "      		<th id='tbDescripcion' colspan='4' style='text-align:center; font:bold 16px arial; color:green;'></th>"
                        + "      		<th style='text-align:center;' colspan='3' class='bgAzulPalido'>PENDIENTE</th>"
                        + "      		<th style='text-align:center;' colspan='3' class='bgVerdePalido'>TRASPASADO</th>"
                        + "  		</tr>"
                        + "  		<tr id='tbLineasDePedidoTraspasoCabecera'>"
                        + "      		<th style=''></th>"
                        + "      		<th style=''>ARTICULO</th>"
                        + "      		<th style=''>UBICACIÓN</th>"
                        + "      		<th style=''>NOMBRE</th>"
                        + "      		<th style='text-align:center;' class='bgAzulPalido'>CAJAS</th>"
                        + "      		<th style='text-align:center;' class='bgAzulPalido'>UDS.</th>"
                        + "      		<th style='text-align:center;' class='bgAzulPalido'>PESO</th>"
                        + "      		<th style='text-align:center;' class='bgVerdePalido'>CAJAS</th>"
                        + "      		<th style='text-align:center;' class='bgVerdePalido'>UDS.</th>"
                        + "      		<th style='text-align:center;' class='bgVerdePalido'>PESO</th>"
                        + "  		</tr>";

                    var trColor = "#daf7ff"; 
                    $(".tbLineasDePedidoTraspasoTR").remove();
                    $(".tbSeccion, .teclado").hide();
                    $("#dvDatosDePedido").html(laTabla).slideDown();

                    for (var i in js) {
                        if (trColor == "#daf7ff") { trColor = ""; } else { trColor = "#daf7ff"; } /* azul claro */
                        var ubicacion = js[i].ubicacion; if (ubicacion === "undefined" || ubicacion===null) { ubicacion = ""; }
                        var elID = (js[i].empresa + js[i].numero + js[i].linia + js[i].letra).replace(/\s+/g, "_");

                        var cajasPendientes = parseInt(js[i].cajas_pendientes);             if (cajasPendientes < 0) { cajasPendientes = 0; }
                        var udsPendientes = parseInt(js[i].uds_pendiente);                  if (udsPendientes   < 0) { udsPendientes = 0; }
                        var pesoPendiente = parseFloat(js[i].peso_pendiente).toFixed(3);    if (pesoPendiente   < 0) { pesoPendiente = 0.000; }
                        var cajasTraspasado = parseInt(js[i].cajas_traspasadas);
                        var udsTraspasado = parseInt(js[i].uds_traspasadas);
                        var pesoTraspasado = parseFloat(js[i].peso_traspasado).toFixed(3);

                        if (cajasPendientes == 0) { cajasPendientes = ""; }
                        if (udsPendientes == 0) { udsPendientes = ""; }
                        if (pesoPendiente == 0) { pesoPendiente = ""; }
                        if (cajasTraspasado == 0) { cajasTraspasado = ""; }
                        if (udsTraspasado == 0) { udsTraspasado = ""; }
                        if (pesoTraspasado == 0) { pesoTraspasado = ""; }

                        var stockArt = js[i].stock; if(isNaN(stockArt)){ stockArt=0; }

                        var laImgTraspasoC = "style='width:30px; background:url(./Merlos/images/btnTC.png) no-repeat center; background-size:90% ' "
                                            +" onclick='traspasarLineaCompleta(\"tbLinPedTraspTR_" + i + "\"); event.stopPropagation();'";
                        var fColor = "#333;";
                        var dataTraspasar = ` data-traspasar="true" `;
                        var elOnClick = 'onclick=\'editarLineaTraspaso(\"' + elID + '\",\"' + js[i].numero + '\",\"' + js[i].articulo + '\",\"' + js[i].definicion.replace(/"/g, '') + '\",\"' + js[i].uds_pv + '\",\"' + js[i].peso_pv + '\",\"' + js[i].servidas + '\",\"' + js[i].linia + '\",\"' + js[i].cajas_pendientes + '\",\"' + js[i].uds_pendiente + '\",\"' + js[i].peso_pendiente + '\")\'';
                        var elOnClickObs="";
                        if (parseInt(js[i].uds_traspasadas) == 0 && parseInt(stockArt) < parseInt(js[i].uds_pv) && js[i].cntrlSotck == false) { fColor = "orange;"; }
                        if (parseInt(stockArt) <= 0 && js[i].cntrlSotck == false) { elOnClick = ""; fColor = "red;"; laImgTraspasoC = ""; dataTraspasar = ` data-traspasar="false" `;}
                        if (parseInt(js[i].dto) == 100) { trColor = "yellow;"; }

                        var tdID = "";   
                        if ($.trim(js[i].TipoIVA)=="" && js[i].preparado=="0" ) {
                            tdID = "tdObserva_".elID;
                            fColor = "#C15D48;"; /* marrón */
                            trColor = "rgb(246,246,246);"; /* blanco roto */
                            cajasPendientes = udsPendientes = pesoPendiente = cajasTraspasado = udsTraspasado = pesoTraspasado = "";
                            elOnClickObs = 'onclick=\'traspasarObservacion(\"' + tdID + '\",\"' + js[i].empresa + '\",\"' + js[i].numero + '\",\"' + js[i].linia + '\",\"' + js[i].letra + '\"); event.stopPropagation();\'';
                            laImgTraspasoC = "";
                        } 
                        if ($.trim(js[i].TipoIVA)!=="" && js[i].preparado == "1") {
                            tdID = "tdObserva_" + elID;
                            fColor = "#009907;"; /* verde oscuro */ laImgTraspasoC = "";
                            trColor = "#d6ffde;";   /* verde claro */
                            elOnClickObs = 'onclick=\'traspasarObservacion(\"' + tdID + '\",\"' + js[i].empresa + '\",\"' + js[i].numero + '\",\"' + js[i].linia + '\",\"' + js[i].letra + '\"); event.stopPropagation();\'';
                        }

                        var elCodigo = js[i].cliente;
                        var laUbicacion = js[i].ubicacion; if (laUbicacion === "") { laUbicacion = " "; }
                        var rCom = js[i].rComercial;
                        var elNombre = js[i].n_cliente;
                        var datosCli = js[i].direccion + "<br>" + js[i].copdpost + " " + js[i].poblacion + "<br>" + js[i].provincia + "";
                        var laRuta = "RUTA: " + js[i].ruta + " - " + js[i].n_ruta;
                        var elVendedor = "Vendedor: " + js[i].vendedor + " - " + js[i].n_vendedor + "<br>Telf: " + js[i].Movil_vendedor;

                        var cajasVis = ""; var pesoVis = "";
                        if (parseInt(js[i].cajas_pv) == 0) { cajasVis = "inv"; }
                        if (parseFloat(js[i].peso_pv) < 0.001) { pesoVis = "inv"; }

                        var pedidoArticulo = js[i].articulo;
                        var pedidoDefinicion = js[i].definicion;

                        // Pintar artículos cambiados si existen
                        var articuloCambiado = "";
                        for(var l in cambiarArticuloListado){
                            if($.trim(cambiarArticuloListado[l].articulo)===$.trim(js[i].articulo)){
                                articuloCambiado = `<br><span style="font:12px arial;color:#FF5733;">ORIGINAL: `+cambiarArticuloListado[l].articuloOriginal+` - `+cambiarArticuloListado[l].definicionOriginal+`</span>`;
                            }
                        }

                        $("#tbLineasDePedidoTraspaso").append(
                            "<tr id='tbLinPedTraspTR_" + i + "' class='tbLineasDePedidoTraspasoTR' style='background:" + trColor + "' " + elOnClick + " " +dataTraspasar+" "
                            +"  data-linea='" + js[i].linia + "' data-articulo='"+pedidoArticulo+"' data-cajas='"+cajasPendientes+"' data-unidades='"+udsPendientes+"' data-peso='"+pesoPendiente+"'>"
                            + "          <td  id='tbLinPedTraspTRTC_" + i + "' class='tbAccion' " + laImgTraspasoC
                            + "			</td>"
                            + "          <td class='tbLineasDePedidoTraspasoTDlineaPte inv' style='color:" + fColor + "'>" + js[i].linia + "</td>"
                            + "          <td class='tbLineasDePedidoTraspasoTDarticuloPte' style='color:" + fColor + "'>" + pedidoArticulo + "</td>"
                            + "          <td style='color:" + fColor + "'>" + ubicacion + "</td>"
                            + "          <td  id='" + tdID + "' class='tdDefinicion' style='color:" + fColor + "' "+elOnClickObs+">" + pedidoDefinicion + articuloCambiado + "</td>"
                            + "          <td class='tbLineasDePedidoTraspasoTDcajasPte' style='text-align:right;color:" + fColor + "'><span class='" + cajasVis + "'>" + cajasPendientes + "</span></td>"
                            + "          <td class='tbLineasDePedidoTraspasoTDunidadesPte' style='text-align:right;color:" + fColor + "'>" + udsPendientes + "</td>"
                            + "          <td class='tbLineasDePedidoTraspasoTDpesoPte' style='text-align:right;color:" + fColor + "'><span class='" + pesoVis + "'>" + pesoPendiente + "</span></td>"
                            + "          <td style='text-align:right;color:" + fColor + " border-left-color:#07657e;'><span class='" + cajasVis + " '>" + cajasTraspasado + "</span></td>"
                            + "          <td style='text-align:right;color:" + fColor + "'>" + udsTraspasado + "</td>"
                            + "          <td style='text-align:right;color:" + fColor + "'><span class='" + pesoVis + "'>" + pesoTraspasado + "</span></td>"
                            + "</tr>"
                        )                            
                        
                        if(cajasTraspasado!=="" || udsTraspasado!=="" || pesoTraspasado!==""){btnMerlosTraspasoIO=true;} // Control del botón btnMerlosTraspaso
                    }

                    $("#tbLineasDePedidoTraspaso").append("</table></div><div id='dvLineasDePedidoTraspasoContenido'></div>");

                    activarObjetos();
                    $("#inpLineasDePedidoTraspasoCB").focus();
                    $("#btnTraspasoDef").show(); $("#btnTraspasoDef").off("click").on("click", function () { traspasarPedido(); });
                    $("#btnMerlosHome").show(); $("#btnCambiarArtC,#btnRecargarC").show();
                    if ($.trim(rCom) !== "") { rCom = "<br><span style=\"font:bold 14px arial; color:#666;\">" + rCom + "</span>"; }
                    $('#spanDatosCliente').html(
                        "		    <span>" + elNombre + " (<span id=\"spCodCli\">" + elCodigo + "</span>)</span>"
                        + rCom
                        + "		    <br />"
                        + "		    <span style=\"font:14px arial; color:#333;\">" + datosCli + "</span>"
                    );
                    $('#spBPruta').html(laRuta);
                    $('#spBPvendedor').html(elVendedor);
                    $('#spCliObs').html(cliObs);
                    $('#spPedidoObs').html(pedidoObs);
                    $("#dvLineasDePedido").stop().hide();
                    if(cliObs!=="" || pedidoObs!==""){ $(".btnObservaciones img").attr("src", "./Merlos/images/btnObservaciones.png").addClass("btnAlo curP");  }
                    else{ $(".btnObservaciones img").attr("src", "./Merlos/images/btnObservaciones_O.png").removeClass("btnAlo curP"); }
                } else { $("#dvLineasDePedido").html("<div class='avRojo'>No se han obtenido resultados!</div>"); }
                BotoneraEstado();
            } else { alert('Error SP pBuscarPedido!' + JSON.stringify(ret)); }            
        }, false);
    }

    function mostrarObservaciones(cliObs, pedidoObs) {
        if(!$(".btnObservaciones img").hasClass("btnAlo")){ return; }
        $("body").prepend("<div id='dvVeloObs' class='dvVelo'>"
            + "      <div class='dvCliObs vaT taL' style='max-width:50%; margin:5% auto; background:#f2f2f2;box-shadow: 2px 4px 10px 1px rgba(0,0,0,.3);'>"
            + "             <img src='./Merlos/images/cerrar.png' class='curP' style='width:20px; float:right;' onclick='cerrarObservaciones()'>"
            + "             <br><span style='font:bold 12px arial; color:#19456B;'>Observaciones del cliente:</span>"
            + "             <br><span id='spCliObs'>" + cliObs + "</span>"
            + "             <br><br><span style='font:bold 12px arial; color:#19456B;'>Observaciones del pedido:</span>"
            + "             <br><span id='spPedidoObs'>" + pedidoObs + "</span>"
            + "             <br><br>"
            + "     </div>"
            + "</div>");
        $("#dvVeloObs").fadeIn();
    }

    function cerrarObservaciones() { $("#dvVeloObs").fadeOut(300, function () { $("#dvVeloObs").remove(); }); }

    function recargar(forzar) { 
        if((!forzar || forzar===undefined) && !$(".btnRecargar img").hasClass("btnAlo")){ return; }
        buscarPedido(trsps_numero, trsps_serie, trsps_ruta, trsps_nRuta); 
    }

    function cbKeyUp() {
        var valCB = $.trim($("#inpLineasDePedidoTraspasoCB").val());
        if (valCB === "") { return; }
        if (event.keyCode === 13) { comprobarCB(valCB); }
    }

    function comprobarCB(cb) {
        if (cb === "") { return; }
        var parametros = '{"sp":"pComprobarCB","CODBAR":"'+cb+'"}';
        flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
            var js = JSON.parse(limpiarCadena(ret.JSCode));
            if (js.Error === "") {
                artEncontrado = $(".tbLineasDePedidoTraspasoTDarticuloPte:contains('" + js.ARTICULO + "')").text();
                if (artEncontrado !== "") {
                    // el artículo pertenece al pedido y lo tratamos
                    linea = $(".tbLineasDePedidoTraspasoTDarticuloPte:contains('" + js.ARTICULO + "')").prev().text();
                    // si trabaja con lotes
                    if (js.LOTE !== "") { 
                        // accedemos a la pantalla de lotes
                        $(".tbLineasDePedidoTraspasoTDarticuloPte:contains('" + js.ARTICULO + "')").parent().click();                        
                        setTimeout(()=>{ /* espera para pintar la tabla */
                            // nos situamos en la linea del lote y verificamos stock y preparación
                            var loteLinTR = $(".linLote:contains('"+js.LOTE+"')").parent().attr("id");
                            var linStock = $("#"+loteLinTR).find(".inpStock").val();
                            var linUds = $("#"+loteLinTR).find(".inpUnidades").val(); 
                            if(parseFloat(js.UNIDADES)>parseFloat(linStock)){ abrirVelo("No hay suficiente stock del lote: "+js.LOTE+"!<br><br><br><span class='btnRojo esq05' onclick='cerrarVelo();'>aceptar</span>"); traspasarAtras(); return; }
                            if(parseFloat(linUds)>parseFloat(js.UNIDADES)){ abrirVelo("Las unidades pedidas superan a las unidades del lote!<br><br><br><span class='btnRojo esq05' onclick='cerrarVelo();'>aceptar</span>"); traspasarAtras(); return; }
                            // Traspasamos la linea
                            traspasarArtLote();
                        },500);                        
                    }
                    else { traspasoDesdeCB(cb, js.ARTICULO, js.UNIDADES, js.UNICAJA, js.VENTA_CAJA, linea); }
                } else { alert("El artículo NO pertenece a este pedido!"); }
            } else {
                // Errores y Avisos
                if (js.Error == "CodigoDividido") {
                    abrirVelo(
                        "<div class='taC' style='font:16px arial; color:#333;'>"
                        + "Este código necesita un segundo bloque"
                        + "<br><br>"
                        + "<input type='text' id='inpCB2' "
                        + "style='width:300px; padding:5px; text-align:center;' class='esq05' "
                        + "onkeyup='CBsegundoBloque(\"" + cb + "\")'>"
                        + "	<br><br><br><span class='btnRojo esq05' onclick='cerrarVelo();'>cancelar</span>"
                        + "</div>"
                    );
                    $("#inpCB2").focus();
                } else { alert(js.Error); }
            }
        } else { alert('Error SP pComprobarCB!'+JSON.stringify(ret)); }}, false);
    }

    function traspasarAtras(forzar) {
        if(!forzar && !$(".btnMerlosAtras img").hasClass("btnAlo")){ return; }
        if($("#tbModifPedido").is(":visible")){ recargar(1); }

        if ($("#tbLineasDePedidoTraspaso").is(":visible")) { 
            $(".tbSeccion").hide(); $("#dvTrabajoInputs, #dvResultados").slideDown(); 
            buscarGlobal();
        }

        if ($("#dvLineasDeArticulo").is(":visible")) {
            $(".tbSeccion, .btnPrep0 img").hide();
            $("#dvDatosDePedido, #dvLineasDePedidoTraspaso").slideDown(); 
        }

        BotoneraEstado();
    }

    function traspasarObservacion(td,empresa,numero,linia,letra){ 
        var txtDefinicion = $("#"+td).text();
        $("#"+td).html("<span style='color:red;'>"+icoCarga16+" guardando, espera...</span>");
        var parametros = '{"sp":"pPedidos","modo":"traspasarObservacion","linia":"'+linia+'","empresa":"'+empresa+'","letra":"'+letra+'","numero":"'+numero+'"}';
        flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
            var elColor="#C15D48;";
            if(ret.JSCode==="insertado"){ elColor="green;"; }
            $("#"+td).html("<span style='color:"+elColor+"'>"+txtDefinicion+"</span>");
        } else { alert('Error SP pPedidos - traspasarObservacion!'+JSON.stringify(ret)); }}, false);        
    }

    function traspasarLineaCompleta(tr) {
        abrirVelo(infoCarga("traspasando la linea...)"));

        var linea = $("#" + tr).attr("data-linea");
        var articulo = $("#" + tr).attr("data-articulo");
        var cajas = ($("#" + tr).attr("data-cajas")).replace(/,/g, "");
        var unidades = ($("#" + tr).attr("data-unidades")).replace(/,/g, "");
        var peso = ($("#" + tr).attr("data-peso")).replace(/,/g, "");

        var parametros = '{"sp":"pTraspLinCompleta","almacen":"' + Almacen + '","articulo":"' + articulo + '","cajas":"' + cajas + '","unidades":"' + unidades + '","peso":"' + peso + '","linea":"' + linea + '","numero":"' + Numero + '","serie":"' + Serie + '"}';
		flexygo.nav.execProcess('pMerlos','',null,null,[{ 'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
                cerrarVelo();
                recargar(1);
                //verLineasDelPedido(Numero, Serie, Ruta, RutaNombre, Almacen);
            } else { alert('Error SP pTraspLinCompleta!' + JSON.stringify(ret)); }
        }, false);
    }

    function cancelarPreparacion(confirmado) {
        if(!$(".btnCancelarPreparacion img").hasClass("btnAlo")){ return; }
        if (!confirmado) {
            abrirVelo(
                "<div style='text-align:center;'>"
                + "		Confirma para cancelar la preparación!"
                + "		<br><br><br>"
                + "		<span class='btnMVerde esq05' onclick='cerrarVelo();cancelarPreparacion(1);'>confirmar</span>"
                + "		&nbsp;&nbsp;&nbsp;"
                + "		<span class='btnMRojo  esq05' onclick='cerrarVelo();'>cancelar</span>"
                + "</div>"
            );
        } else {
            abrirVelo(icoCarga16+" cancelando la preparación...");
            if (trsps_linea === undefined) { trsps_linea = 0; }
            var parametros = '{"sp":"pCancelarPreparacion","pedido":"' + trsps_numero + '","serie":"' + trsps_serie + '","linea":"' + trsps_linea + '","almacen":"' + Almacen + '"}';
            flexygo.nav.execProcess('pMerlos', '', null, null, [{ 'Key': 'parametros', 'Value': limpiarCadena(parametros) }], 'current', false, $(this), function (ret) { if (ret) {
                cambiarArticuloCancelar("modificacionesCancelar"); 
            } else { alert('Error SP pCancelarPreparacion!' + JSON.stringify(ret)); } }, false);
        }
    }

    function editarLineaTraspaso(id, numero, articulo, definicion, unidades, peso, servidas, linea, cajas, traspaso, pesoPend) {
        abrirVelo(infoCarga("editando la linea..."));

        gID = id;
        gArticulo = articulo;
        gArtUnidades = unidades;
        gArtPeso = peso;
        gArtServidas = servidas;
        trsps_numero = numero;
        trsps_articulo = articulo;
        trsps_linea = linea; 

        var parametros = '{"sp":"pEditarLineaTraspaso","pedido":"' + trsps_numero + '","serie":"' + trsps_serie + '","articulo":"' + trsps_articulo + '","unidades":"' + unidades + '","linea":"' + linea + '","cajas":"' + cajas + '","peso":"' + peso + '","servidas":"' + servidas + '","traspaso":"' + traspaso + '","pesoPend":"' + pesoPend + '","basculaEnUso":"' + basculaEnUso + '"}';
        flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
            var contenido = "";
            var js = JSON.parse(limpiarCadena(ret.JSCode));
            if (js.length > 0) {
                // si existen registros en Traspaso_Temporal devolvemos un aviso y salimos de la función
                if (js[0].existeTraspTemp === 1) {
                    for (var i = 0; i < js.length; i++) {                        
                        var clCampoLote = "";
                        var clCampoCajas = "";
                        var clCampoPeso = "";
                        var fCaducidad = $.trim(js[i].fCaducidad);
                        if (js[i].lote.replace(/-/g, "") === "") { clCampoLote = "display:none;"; }
                        if (parseInt(js[i].cajas) === 0 || isNaN(js[i].cajas) || js[i].cajas === null) { clCampoCajas = "display:none;"; }
                        if (parseFloat(js[i].peso) < 0.001 || isNaN(js[i].peso) || js[i].peso === null|| js[i].peso === "") { clCampoPeso = "display:none;"; }
                        if (fCaducidad.trim() === "" || fCaducidad === "01-01-1900" || fCaducidad === undefined) { fCaducidad = "<span style='color:#900;'>no disponible</span>"; }
                        if (i === 0) {
                            contenido = "<div style='text-align:right;'><img src='./Merlos/images/cerrar.png' width='20' style='cursor:pointer;' onclick='cerrarVelo();'/></div>"
                                + "<div style='margin-bottom:20px;font:bold 16px arial; color:#900;'>"
                                + "		El artículo " + gArticulo + " ya está preparado!"
                                + "		<br><br><br>"
								+ "		<span class='btnRojo esq05' style='padding:10px;' onclick='cancelarPreparacionArticulo(\""+js[i].empresa+"\",\""+js[i].numero+"\",\""+js[i].letra+"\","+js[i].linea+");'>cancelar preparación</span>"
                                + "		<br><br>"
                                + "</div>"
                                + "<table id='tbArtEnTemp' class='tbStd'>"
                                + "	<tr>"
                                + "		<th style='text-align:center;" + clCampoLote + "'>Lote</th>"
                                + "		<th style='text-align:center;'>Caducidad</th>"
                                + "		<th style='text-align:center; " + clCampoCajas + "'>Cajas</th>"
                                + "		<th style='text-align:center;'>Uds</th>"
                                + "		<th style='text-align:center;" + clCampoPeso + "' class='campoPeso'>Peso</th>"
                                + "	</tr>";
                        }
                        contenido += "	<tr>"
                            + "		<td style='text-align:center;" + clCampoLote + "'>" + js[i].lote + "</td>"
                            + "		<td style='text-align:center;'>" + fCaducidad + "</td>"
                            + "		<td style='text-align:center;" + clCampoCajas + "'>" + parseInt(js[i].cajas) + "</td>"
                            + "		<td style='text-align:center;'>" + parseInt(js[i].unidades) + "</td>"
                            + "		<td style='text-align:center;" + clCampoPeso + "'>" + parseFloat(js[i].peso).toFixed(3) + "</td>"
                            + "	</tr>";
                    }
                    contenido += "</table>";
                    abrirVelo(contenido);
                    return;
                }

                // Trabajo SIN lotes
                if (js[0].existeTraspTemp === 2) {
                    tecladoModo = "AsignarSinLote";

                    traspaso = parseInt(js[0].TRASPASO);
                    var pedidoCajas = parseInt(js[0].pCajas);
                    var pedidoUnidades = parseInt(js[0].UNIDADES);
                    var pedidoPeso = parseFloat(js[0].PESO).toFixed(3);
                    var stockCajas = parseInt(js[0].STOCKCAJAS);
                    var stockUds = parseInt(js[0].elStock);
                    var stockPeso = parseFloat(js[0].STOCKPESO).toFixed(3);
                    var prepCajas = parseInt(js[0].tempCajas);
                    var prepUds = parseInt(js[0].tempUds);
                    var prepPeso = parseFloat(js[0].tempPeso).toFixed(3);

                    var colSpanStock = 3;
                    var colSpanPrep  = 4;

                    if (stockPeso < 0) { stockPeso = 0; }
                    var cajasVis = "display:visible;"; if (pedidoCajas < 1) { cajasVis = "display:none;"; colSpanStock--; colSpanPrep--; }
                    var pesoVis = "display:visible;"; if (pedidoPeso < 0.0001) { pesoVis = "display:none;"; colSpanStock--; colSpanPrep--; }

                    contenido = "<table id='tbCabaceraLotes' class='tbStd' style='margin-top:10px;'>"
                        + "  <tr>"
                        + "      <th style=''>ARTICULO</th>"
                        + "      <th style=''>NOMBRE</th>"
                        + "      <th style='text-align:center;" + cajasVis + "'>CAJAS</th>"
                        + "      <th style='text-align:center;'>UDS.</th>"
                        + "      <th style='text-align:center; "+pesoVis+"'>PESO</th>"
                        + "  </tr>"
                        + "	 <tr style='background:#daf7ff;'>"
                        + "          <td style=''>" + trsps_articulo + "</td>"
                        + "          <td style=''>" + js[0].DEFINICION + "</td>"
                        + "          <td style='text-align:right;" + cajasVis + "'>" + cajas + "</td>"
                        + "          <td style='text-align:right;'>" + (pedidoUnidades-traspaso) + "</td>"
                        + "          <td style='text-align:right;" + pesoVis + "'>" + parseFloat(pesoPend).toFixed(3) + "</td>"
                        + "      </tr>"
                        + "</table>"
                        + "<br><br>"
                        + "<table id='tbCoU' class='tbStd'>"
                        + "	 <tr style='border-bottom:1px solid #FFF;'>"
                        + "	    <th colspan='"+colSpanStock+"' style='text-align:center;'>STOCK</th>"
                        + "	    <th colspan='"+colSpanPrep+"' class='bgVerdePalido' style='vertical-align:middle;text-align:center;'>PREPARACIÓN "
                        + "			<span id='spanBasculas' style='float:right; vertical-align:middle; " + pesoVis + " " + js[0].verSpnTipoBascula + "'>"
                        + "				<img id='BI1' class='imgBascula01 icoBascula' src='./Merlos/images/" + js[0].bsc1img + "' style='width:30px; margin-right:20px; cursor:pointer;' onclick='basculaClick(\"imgBascula01\")'/> "
                        + "				<img id='BI2' class='imgBascula02 icoBascula' src='./Merlos/images/" + js[0].bsc2img + "' style='width:30px; margin-right:20px; cursor:pointer;' onclick='basculaClick(\"imgBascula02\")'/> "
                        + "				<img id='BI3' class='imgBascula03 icoBascula' src='./Merlos/images/" + js[0].bsc3img + "' style='width:30px; margin-right:20px; cursor:pointer;' onclick='basculaClick(\"imgBascula03\")'/> "
                        + "				<img id='BI4' class='imgBascula04 icoBascula' src='./Merlos/images/" + js[0].bsc4img + "' style='width:30px; margin-right:20px; cursor:pointer;' onclick='basculaClick(\"imgBascula04\")'/> "
                        + "			</span>"
                        + "		</th>"
                        + "  </tr>"
                        + "  <tr id='tbLineasDePedidoTraspasoCabecera'>"
                        + "      <th style='width:14%;text-align:center;" + cajasVis + "'>CAJAS</th>"
                        + "      <th style='width:14%;text-align:center;'>UDS.</th>"
                        + "      <th style='width:14%;text-align:center;" + pesoVis + "'>PESO</th>"
                        + "      <th style='width:14%;text-align:center;" + cajasVis + "' class='bgVerdePalido' >CAJAS</th>"
                        + "      <th style='width:14%;text-align:center;' class='bgVerdePalido' >UDS.</th>"
                        + "      <th style='width:14%;text-align:center;" + pesoVis + "' class='bgVerdePalido' >PESO</th>"
                        + "      <th style='width:14%;text-align:center;" + pesoVis + "' class='bgVerdePalido' >BÁSCULA</th>"
                        + "  </tr>"
                        + "	 <tr style='background:0;' class='trDatosLinea " + js[0].cntrlSotck + " " + js[0].elStock + "'>"
                        + "          <td style='text-align:right; background:0;" + cajasVis + "'><span class='stockCajas'>"+stockCajas+"</span></td>"
                        + "          <td style='text-align:right; background:0;' class='stockUnidades'>" + stockUds + "</td>"
                        + "          <td class='tdPeso' style='text-align:right; background:0;" + pesoVis + "'>" + stockPeso + "</td>"
                        + "          <td class='bgVerdePalido' style='" + cajasVis + "'><input type='text' id='sinLoteCAJAS' class='tecNum tecV taR w100' style='" + cajasVis + " padding:4px; border:0; outline:none; text-align:right;' value='" + prepCajas + "'></td>"
                        + "          <td class='bgVerdePalido' ><input type='text' id='sinLoteUDS'   class='tecNum tecV taR w100' value='" + prepUds + "' style='padding: 4px; border: 0; outline: none; text-align:right;'></td>"
                        + "          <td class='bgVerdePalido' style='" + pesoVis + "'><input type='text' id='sinLotePESO'  class='tecNum tecV taR w100' style='padding:4px; border:0; outline:none; text-align:right;' value='" + prepPeso + "'></td>"
                        + "          <td style='text-align:center;" + pesoVis + "' class='bgVerdePalido' >"
                        + "             <div style='display:flex; align-items:flex-start; justify-content: space-evenly;'>"
                        + "				    <div><img id='btnBascula' src='./Merlos/images/btnBascula.png' width='40' onclick='capturarPeso(this.id,\"sinLotePESO\")'></div>"
                        + "				    <div>&nbsp;&nbsp;&nbsp;</div>"
                        + "				    <div><img id='btnBascSum1' src='./Merlos/images/btnBasculaSuma.png' width='40' onclick='capturarPesoSuma(this.id,\"sinLotePESO\")'></div>"
                        + "             </div>"
                        + "      	</td>"
                        + "      </tr>"
                        + "</table>";

                    $(".tbSeccion, .teclado, #dvLineasDePedido, #dvLineasDePedidoTraspaso").hide();
                    $("#dvDatosDePedido").show();
                    $("#dvLineasDeArticulo").html(contenido).slideDown();
                    activarObjetos();
                    cerrarVelo();
                    return;
                }

                // Trabajo con Lotes
                tecladoModo = "AsignarConLote";

                var elID = "";
                var cajasVis = "";
                var pesoVis = "";
                var colCab = 6; var colLin1 = 5; var colLin2 = 4; var colLin3 = 9; 
                if (cajas == "" || parseInt(cajas) == 0 || isNaN(cajas)) { cajas = ""; cajasVis = "display:none;"; colCab--; colLin1--; colLin2--; colLin3--; }
                if (pesoPend == "" || parseFloat(pesoPend) == 0.000 || isNaN(pesoPend)) { pesoPend = ""; pesoVis = "display:none;"; colCab--; colLin1--; colLin2--; colLin3--; }
                var contenido = ""
                    + "<table id='tbCabaceraLotes' class='tbStd'>"
                    + "  <tr id='tbLineasDePedidoTraspasoCabecera'>"
                    + "      <th style=''>ARTICULO</th>"
                    + "      <th style=''>NOMBRE</th>"
                    + "      <th style='" + cajasVis + "'>CAJAS</th>"
                    + "      <th style='text-align:center;'>UDS.</th>"
                    + "      <th style='" + pesoVis + " text-align:center;'>PESO</th>"
                    + "  </tr>"
                    + "	 <tr style='background:#daf7ff;'>"
                    + "          <td style=''>" + articulo + "</td>"
                    + "          <td style=''>" + js[0].descripcion + "</td>"
                    + "          <td style='text-align:right;" + cajasVis + "' class='tbCabLotesCAJAS'>" + cajas + "</td>"
                    + "          <td style='text-align:right;'>" + traspaso + "</td>"
                    + "          <td style='text-align:right;" + pesoVis + "'>" + parseFloat(pesoPend).toFixed(3) + "</td>"
                    + "      </tr>"
                    + "</table>"
                    + "</div><br>"
                    + "<div id='dvLineasLotes'>"
                    + "<table id='tbLineasLotes' class='tbStd'>"
                    + "	    <tr style='border-bottom:1px solid #FFF;' class='bgVerdePalido'>"
                    + "		<th colspan='" + colLin1 + "' style='text-align:center;'>STOCK</th>"
                    + "		<th colspan='" + colLin2 + "' style='vertical-align:middle; text-align:center;' class='bgVerdePalido'>PREPARACIÓN "
                    + "			<span id='spanBasculas' style='float:right; vertical-align:middle; " + pesoVis + " " + js[0].verSpnTipoBascula + " '>"
                    + "				<img id='BI01' class='imgBascula01 icoBascula' src='./Merlos/images/" + js[0].bsc1img + "' style='width:30px; margin-right:20px; cursor:pointer;' onclick='basculaClick(\"imgBascula01\")'/> "
                    + "				<img id='BI02' class='imgBascula02 icoBascula' src='./Merlos/images/" + js[0].bsc2img + "' style='width:30px; margin-right:20px; cursor:pointer;' onclick='basculaClick(\"imgBascula02\")'/> "
                    + "				<img id='BI03' class='imgBascula03 icoBascula' src='./Merlos/images/" + js[0].bsc3img + "' style='width:30px; margin-right:20px; cursor:pointer;' onclick='basculaClick(\"imgBascula03\")'/> "
                    + "				<img id='BI04' class='imgBascula04 icoBascula' src='./Merlos/images/" + js[0].bsc4img + "' style='width:30px; margin-right:20px; cursor:pointer;' onclick='basculaClick(\"imgBascula04\")'/> "
                    + "			</span>"
                    + "		</th>"
                    + "   </tr>"
                    + "   <tr>"
                    + "       <th style=''>Lote</th>"
                    + "       <th style=''>Caducidad</th>"
                    + "       <th style='text-align:center;" + cajasVis + "'>CAJAS</th>"
                    + "       <th style='text-align:center;'>UDS.</th>"
                    + "       <th style='text-align:center;" + pesoVis + "'>PESO</th>"
                    + "       <th style='text-align:center;" + cajasVis + "'  class='bgVerdePalido'>CAJAS</th>"
                    + "       <th style='text-align:center;' class='bgVerdePalido'>UDS.</th>"
                    + "       <th style='text-align:center;" + pesoVis + "' class='bgVerdePalido'>PESO</th>"
                    + "       <th style='text-align:center;" + pesoVis + "' class='bgVerdePalido'>BÁSCULA</th>"
                    + "   </tr>"
                    + "	  <tr style='background:0;'><td colspan='" + colLin3 + "' style='height:5px; padding:0; background:0;'></td></tr>";

                var cajasLin = parseInt(cajas);
                if(cajasLin==="" || isNaN(cajasLin)){ cajasLin=0;}
                var restoCajas = parseInt(cajas);
                var udsLin = parseInt(traspaso);
                var RESTO = parseInt(traspaso);
                var suma = 0;
                var sumaCajas = 0;
                var rowUniCaja = 0;

                for (var i = 0; i < js.length; i++) {
                    var elLOTE = (js[i].Llote).replace(/\//g, "--barra--");
                    var elID = (js[i].EMPRESA + "" + $.trim(js[i].NUMERO) + "" + js[i].LINIA + "" + elLOTE).replace(/\s+/g, "_");

                    var fechaCorta = js[i].Lcaducidad.substr(8, 2) + "-" + js[i].Lcaducidad.substr(5, 2) + "-" + js[i].Lcaducidad.substr(0, 4);
                    var spanCantidad = "<span style='font:bold 14px arial; color:green;'>unidades</span>";
                    var txtColor = " color:#000; ";
                    var rowUniCaja = parseInt(js[i].UNICAJA);
                    if (rowUniCaja == "") { rowUniCaja = 0; }
                    var ttCajas = parseInt(js[i].tempCajas);
                    var ttUds = parseInt(js[i].tempUnidades);
                    var ttPeso = parseFloat(js[i].tempPeso).toFixed(3);
                    var stockCajas;
                    if(rowUniCaja>1){ stockCajas=(parseInt(js[i].Lunidades) / rowUniCaja) - ttCajas; }
                    var stockUnidades = parseInt(js[i].Lunidades) - parseInt(js[i].tempUnidades);

                    if (ttCajas > 0 || ttUds > 0 || ttPeso > 0.000) { txtColor = " color:green; "; }
                    if (stockUnidades <= 0) { continue; }

                    if (stockCajas > 0 && cajasLin > 0) {
                        if (restoCajas >= stockCajas) { cajasLin = stockCajas; restoCajas = restoCajas - stockCajas; }
                        else { cajasLin = restoCajas; restoCajas = restoCajas - cajasLin; }
                        if (cajasLin < 0) { cajasLin = 0; }
                        if (restoCajas < 0) { restoCajas = 0; }
                        sumaCajas += cajasLin;
                        if (sumaCajas > cajas) { cajasLin = 0; restoCajas = 0; }
                        cajasP = cajasLin;
                    } else { cajasP = 0; }

                    if (stockUnidades > 0 && udsLin > 0) {
                        if (RESTO >= stockUnidades) { udsLin = stockUnidades; RESTO = RESTO - stockUnidades; }
                        else { udsLin = RESTO; RESTO = RESTO - udsLin; }
                        if (udsLin < 0) { udsLin = 0; }
                        if (RESTO < 0) { RESTO = 0; }
                        suma += udsLin;
                        if (suma > traspaso) { udsLin = 0; RESTO = 0; }
                        unidadesP = udsLin;
                    } else { unidadesP = 0; }

                    pesoRes = parseFloat(js[i].Lpeso);
                    if (pesoRes < 0.000) { pesoRes = 0; }
                    if (stockCajas < 0) { stockCajas = 0; }
                    if (stockUnidades < 0) { stockUnidades = 0; }

                    var mStockCajas = stockUnidades / rowUniCaja;

                    if (rowUniCaja > 0) {
                        cajasP = unidadesP / rowUniCaja;
                        cajasLin = cajasP;
                    }

                    contenido += ""
                        + "     <tr id='trClick_" + elID + "' class='trLotesDatos inp_LoteEntradaUds_" + elID + "' data-lin='"+ js[i].LINIA+"'>"
                        + "          <td class='linLote' style='" + txtColor + "'>" + js[i].Llote + "</td>"
                        + "          <td class='linCaducidad' style='text-align:center;" + txtColor + "'>" + fechaCorta + "</td>"
                        + "          <td class='linUds' style='text-align:right;" + cajasVis + " " + txtColor + "' class='stockCajas'>" + mStockCajas + "</td>"
                        + "          <td class='linStock' style='text-align:center;'><input type='text' id='inp_LoteEntradaStock_"+elID+"' class='tecNum tecV inpStock "+js[i].controlStock+" noInput'  style='width:150px;text-align:center;border:0; background:0;$txtColor' value='"+stockUnidades+"' readonly /></td>"
                        + "          <td class='linPeso' style='text-align:center;" + txtColor + " " + pesoVis + "'>" + pesoRes.toFixed(3) + "</td>"
                        + "          <td style='text-align:center;padding:0;" + cajasVis + "'><input type='text' id='inp_LoteEntradaCajas_" + elID + "' class='tecNum tecV inpCajas ' style='text-align:center;" + txtColor + "' value='" + cajasP + "'/></td>"
                        + "          <td style='text-align:center;padding:0;'><input type='text' id='inp_LoteEntradaUds_" + elID + "' class='tecNum tecV inpUnidades ' style='text-align:center;" + txtColor + "' value='" + unidadesP + "' data-MIelId='" + elID + "' data-uc='" + js[i].VENTA_CAJA + ";" + js[i].unidadesOcajas + ";\"" + js[i].almacen + "\"'/></td>"
                        + "          <td style='text-align:center;padding:0;" + pesoVis + "'><input type='text' id='inp_LoteEntradaPeso_" + elID + "'  class='tecNum tecV inpPeso " + js[i].Llote + " ' style='text-align:center;" + txtColor + "' value='" + ttPeso + "'/></td>"
                        + "          <td style='text-align:center;;" + pesoVis + "'>"
                        + "             <div style='display:flex; align-items:flex-start; justify-content: space-evenly;'>"
                        + "				    <div><img id='btnBascula" + elID + "' src='./Merlos/images/btnBascula.png' width='40' onclick='capturarPeso(this.id,\"inp_LoteEntradaPeso_" + elID + "\")'></div>"
                        + "				    <div>&nbsp;&nbsp;&nbsp;</div>"
                        + "				    <div><img id='btnBascSum" + elID + "' src='./Merlos/images/btnBasculaSuma.png' width='40' onclick='capturarPesoSuma(this.id,\"inp_LoteEntradaPeso_" + elID + "\")'></div>"
                        + "			    </div>"
                        + "			</td>"
                        + "      </tr>"
                        + "	    <tr style=' background:0;'><td colspan='" + colLin3 + "' style='padding:2px; background:#07657e;'></td></tr>";
                }
                contenido += "</table></div>";
            } else { contenido = "<div class='avRojo'>no se han obtenido resultados!</div>"; }

            $(".tbSeccion, .teclado, #dvLineasDePedido, #dvLineasDePedidoTraspaso").hide();
            $("#dvDatosDePedido").show();
            $("#dvLineasDeArticulo").html(contenido).slideDown();
            activarObjetos();
            cerrarVelo();
        } else { alert('Error SP pEditarLineaTraspaso!' + JSON.stringify(ret)); } }, false);
    }
	
	function cancelarPreparacionArticulo(empresa,numero,letra,linea){
		var parametros = '{"sp":"pCancelarPreparacion","modo":"cancelarPrepArt","pedido":"' + numero + '","serie":"' + letra + '","linea":' + linea + '}';
		flexygo.nav.execProcess('pMerlos', '', null, null, [{ 'Key': 'parametros', 'Value': limpiarCadena(parametros) }], 'current', false, $(this), function (ret) { if (ret) {
			verLineasDelPedido(numero,letra,Ruta,RutaNombre,Almacen); cerrarVelo();
		} else { alert('Error SP pCancelarPreparacion!' + JSON.stringify(ret)); } }, false);
	}

    function traspasoCompleto() {
        if(!$(".btnTraspasoCompleto img").hasClass("btnAlo")){ return; }
        abrirVelo(infoCarga("traspasando el pedido..."));
        if (!$("#tbLineasDePedidoTraspaso").is(":visible")) { return; }
        var articulos = [];
        $("#tbLineasDePedidoTraspaso").find(".tbLineasDePedidoTraspasoTR").each(function () {
            var articulo = $(this).find(".tbLineasDePedidoTraspasoTDarticuloPte").text();
            var cajas = ($(this).find(".tbLineasDePedidoTraspasoTDcajasPte").text()).replace(/,/g, "");             if(cajas===""){ cajas="0"; }
            var unidades = ($(this).find(".tbLineasDePedidoTraspasoTDunidadesPte").text()).replace(/,/g, "");       if(unidades===""){ unidades="0"; }
            var peso = ($(this).find(".tbLineasDePedidoTraspasoTDpesoPte").text()).replace(/,/g, "");               if(peso===""){ peso="0"; }
            var linea = $(this).find(".tbLineasDePedidoTraspasoTDlineaPte").text();
            if($(this).attr("data-traspasar")==="true"){
                articulos.push('{"articulo":"' + articulo + '","cajas":"' + cajas + '","unidades":"' + unidades + '","peso":"' + peso + '","linea":"' + linea + '"}');
            }
        }); 
        var parametros = '{"sp":"pTraspasoCompleto"'
                    + ',"articulos":[' + articulos +']'
                    + ',"numero":"' + NumPedido + '"'
                    + ',"serie":"' + PedidoSerie + '"'
                    + ',"ruta":"' + Ruta + '"'
                    + ',"almacen":"' + Almacen + '"'
                    + ',' + params
                    + '}';
        flexygo.nav.execProcess('pMerlos', '', null, null, [{ 'Key': 'parametros', 'Value': limpiarCadena(parametros) }], 'current', false, $(this), function (ret) {if (ret) { 
                cerrarVelo(); recargar(1);
        } else { alert('Error SP pTraspasoCompleto!\n' + JSON.stringify(ret)); } }, false);
    }

    // cálculo de unidades/cajas SIN LOTE
    function calcularCajasDesdeSinLoteUDS() { 
        cerrarVelo();       
        return; // modificación Albert 20-10-2020
        
    }
    function calcularUDSDesdeSinLoteCAJAS() {
        var lasCajas = $.trim($("#sinLoteCAJAS").val());
        if (isNaN(lasCajas)) { alert("Ups! Parece que el valor no es correcto!"); return; }
        var parametros = '{"sp":"pDatos","modo":"dameUNICAJA","articulo":"'+trsps_articulo+'"}';
        flexygo.nav.execProcess('pMerlos', '', null, null, [{ 'Key': 'parametros', 'Value': limpiarCadena(parametros) }], 'current', false, $(this), function (ret) {if (ret) {
            // devuelve UNICAJA de GESTION_articulo
            var UNICAJA = parseFloat(ret.JSCode);
            var unidades = 0;
            if (!isNaN(UNICAJA)) { unidades = lasCajas * UNICAJA; }
            $("#sinLoteUDS").val(unidades.toFixed(0));
            cerrarVeloTeclado();
        } else { alert('Error SP pSeries!' + JSON.stringify(ret)); } }, false);
    }

    // cálculo de unidades/cajas CON LOTE
    function calcularCajasDesdeConLoteUDS(trgt) {
        cerrarVeloTeclado();
        return; // modificación Albert 20-10-2020
    }
    function calcularUdsDesdeConLoteCAJAS(trgt) {
        var lasCajas = $.trim($("#" + trgt).val());
        var elInputUds = "inp_LoteEntradaUds" + trgt.substring(20, trgt.length);
        var parametros = '{"sp":"pDatos","modo":"dameUNICAJA","articulo":"'+trsps_articulo+'"}';
        flexygo.nav.execProcess('pMerlos', '', null, null, [{ 'Key': 'parametros', 'Value': limpiarCadena(parametros) }], 'current', false, $(this), function (ret) {if (ret) {
            // devuelve UNICAJA de GESTION_articulo
            var UNICAJA = parseFloat(ret.JSCode);
            var unidades = 0;
            if (!isNaN(UNICAJA)) { unidades = lasCajas * UNICAJA; }
            $("#" + elInputUds).val(unidades.toFixed(0));
            cerrarVeloTeclado();
        } else { alert('Error SP pSeries!' + JSON.stringify(ret)); } }, false);
    }

    function ventaCajas(id) {
        var uds = $("#" + id).val();
        var dataUC = $("#" + id).attr("data-uc");
        var ventaCaja = dataUC.split(";")[0];
        var uniCaja = dataUC.split(";")[1];

        if (ventaCaja === "1") {
            if (!Number.isInteger(parseFloat(uds).toFixed(2) / parseInt(uniCaja))) {
                abrirVelo(
                    "<div class='taC' style='font:16px arial; color:#333;'>"
                    + icoAlert50 + "<br><br>"
                    + "Este artículo sólo se puede servir por cajas completas!"
                    + "	<br><br><br><span class='btnRojo esq05' onclick='cerrarVelo();'>aceptar</span>"
                    + "</div>"
                );
                var elIdCajas = "inp_LoteEntradaCajas_" + $("#" + id).attr("data-MIelId");
                $("#" + id + ",#" + elIdCajas).val("");
            }
        }
    }


    function cambiarArticulos(pedido, serie, ruta) {
        if(!$(".btnCambiarArt img").hasClass("btnAlo")){ return; }
        if($(".btnCambiarArt img").attr("src")==="./Merlos/images/CambiarArticuloVerde.png" && cambiarArticuloListado.length===0){ cambiarArticuloCancelar(); recargar(1); return; }        
        cambiandoArticulo=true;
        var parametros = '{"sp":"pCambiarArticulos","pedido":"'+pedido+'","serie":"'+serie+'"}';
        flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){ 
            var js = JSON.parse(limpiarCadena(ret.JSCode));
            if(js.length>0){
                $("#tbLineasDePedidoTraspaso").find(".tbLineasDePedidoTraspasoTR").each(function(){
                    $(this).attr("onclick","").find(".tbAccion").attr("onclick","").css("background",""); /* eliminamos imagen y onclick de las líneas */
                    for(var i in js){
                        if(js[i].articulo===$(this).attr("data-articulo")){                            
                            $(this).find(".tbAccion")
                                .css("background","url('./Merlos/images/intercambiar.png') no-repeat center").css("background-size","90%")
                                .parent().attr("onclick","cambiarArticulosEq('" + js[i].articulo + "','" + js[i].definicion + "','" + js[i].linia + "','" + js[i].uds_pv + "')"); 
                            continue; 
                        }
                    }                    
                });
            }else{ alert("No se han obtenido registros!"); }
            activarObjetos();
        } else { alert('Error SP pCambiarArticulos!'+JSON.stringify(ret)); }}, false);
    }

    function cambiarArticulosEq(articulo, definicion, linea, unidades) {
        abrirVelo(icoCarga16+" cargando artículos equivalentes...");

        var parametros = '{"sp":"pCambiarArticulos","modo":"equivalencia","articulo":"'+articulo+'","almacen":"'+Almacen+'"}';
        flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
            var elJS = JSON.parse(limpiarCadena(ret.JSCode));
            var contenido = "<div class='poderCerrarVelo'>Selecciona el artículo equivalente para sustituir a<br>"+articulo+" - "+definicion+"</div>"
                +"<br>"
                +"<table id='tbCambiarArticulosEq' class='tbStd'>"
                + "<tr>"
                + "	<th class='taL'>Artículo</th>"
                + "	<th class='taL'>Descripción</th>"
                + "	<th style='text-align:center;'>Uds.</th>"
                + "	<th style='text-align:center;'>Stock</th>"
                + "</tr>";
            if (elJS.length > 0) {
                for (var i in elJS) {
                    var elStock = parseFloat(elJS[i].stock); if (elStock === 0 || isNaN(elStock)) { elStock = ""; }
                    contenido += ""
                        + "<tr class='sobre' onclick='marcarCambiarArticulo(\"" + elJS[i].ARTICULO + "\",\"" + elJS[i].NOMBRE + "\",\"" + linea + "\",\"" + unidades + "\",\"" + articulo + "\",\"" + definicion + "\")'>"
                        + "	<td class='taL'>" + elJS[i].ARTICULO + "</td>"
                        + "	<td class='taL'>" + elJS[i].NOMBRE + "</td>"
                        + "	<td style='text-align:right;'>" + unidades + "</td>"
                        + "	<td style='text-align:right;'>" + elStock + "</td>"
                        + "</tr>";
                }
                contenido += "</table>";
            } else { contenido = "<div class='info poderCerrarVelo'>No se han obtenido resultados!</div>"; }
            abrirVelo(contenido);
        } else { alert('Error SP pCambiarArticulos - equivalencia!'+JSON.stringify(ret)); }}, false);
    }

function cambiarArticuloCancelar(modo){
	cambiandoArticulo=false;
	cambiarArticuloListado = [];
    $(".btnCambiarArt img").attr("src","./Merlos/images/btnCambiarArtC.png");    
    if(!modo){ verLineasDelPedido(Numero,Serie,Ruta,RutaNombre,Almacen); cerrarVelo(); }
    if(modo==="modificacionesCancelar"){modificacionesCancelar();}
}

function Velo(msj){
	if(!msj){ $("#dvVELO").fadeOut(300,function(){ $("#dvVELO").remove(); }); }
	else{
		$("body").prepend(
			 "<div id='dvVELO' class='inv' style='z-index:999;position:fixed;width:100%; height:100%; background:rgba(250,250,250,.75);'>"
			+"	<div style='width:500px; margin:100px auto; background:#07657e; padding:20px; text-align:center; font:20px arial; color:#FFF' "
			+"	class='esq10 sombra001'>"+msj+"</div>"
			+"</div"
		);
		$("#dvVELO").fadeIn();
	}
}

function marcarCambiarArticulo(articulo, nombre, linea, unidades, articuloOriginal, definicionOriginal) {
    abrirVelo(icoCarga16+" Sustituyendo el artículo " + articulo + "...");
    cambiarArticuloListado.push({"linea":linea,"articuloOriginal":articuloOriginal,"definicionOriginal":definicionOriginal,"articulo":articulo,"nombre":nombre});
    var parametros = '{"sp":"pSustituirArticulos","articulo":"'+articulo+'","linea":"'+linea+'","unidades":"'+parseInt(unidades)+'","empresa":"'+Empresa+'","letra":"'+Serie+'","numero":"'+Numero+'"}';
    flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){ 
        cambiandoArticulo=false; recargar(1); cerrarVelo();
    } else { alert('Error SP pSeries!'+JSON.stringify(ret)); }}, false);
}

function modificarPedido() {
    if(!$(".btnModificarPedido img").hasClass("btnAlo")){ return; }
    if($(".btnModificarPedido img").attr("src")==="./Merlos/images/ModificarPedidoVerde.png" && modificarPedidoListado.length===0){ modificacionesCancelar(); recargar(1); return; }        
    $(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedidoVerde.png");
    modificandoPedido=true;

    var parametros = '{"sp":"pPedidos","modo":"dameLineas","Empresa":"'+Empresa+'","Numero":"'+Numero+'","Serie":"'+Serie+'"'
                    +',"Ruta":"'+Ruta+'","Almacen":"'+Almacen+'","usuarioSesion":"'+currentReference+'","pedidoActivo":"'+PedidoActivo+'"}';	
    flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
        var js1 = JSON.parse(limpiarCadena(ret.JSCode));  
        var js  = js1.datos;       
        if(js.length>0){
            $("#tbLineasDePedidoTraspaso").find(".tbLineasDePedidoTraspasoTR").each(function(){
                $(this).attr("onclick","").find(".tbAccion").attr("onclick","").css("background",""); /* eliminamos imagen y onclick de las líneas */
                for(var i in js){
                    if(js[i].articulo===$(this).attr("data-articulo")){                            
                        $(this).find(".tbAccion")
                            .css("background","url('./Merlos/images/intercambiar.png') no-repeat center").css("background-size","90%")
                            .attr("onclick","intercambiarArticulo("+i+","+js[i].linia+",'"+js[i].articulo+"','"+js[i].definicion.trim()+"')"); 
                        continue; 
                    }
                }                    
            });
        }else{ alert("No se han obtenido registros!"); }
        activarObjetos();
    } else { alert('Error SP pPedidos - dameLineas!'+JSON.stringify(ret)); }}, false);
}

function modificacionesCancelar(modo){
	modificandoPedido=false;
	modificarPedidoLineas = [];
    $(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedido.png");
    if(!modo){ verLineasDelPedido(Numero,Serie,Ruta,RutaNombre,Almacen); cerrarVelo(); }
}

    function borrarPropuesta() {
        if( !$(".btnPreparacion img").hasClass("btnAlo") ){ return; }
        if ($("#tbCoU").is(":visible")) { $("#sinLoteCAJAS,#sinLotePESO,#sinLoteUDS").val(""); }
        if ($("#tbLineasLotes").is(":visible")) { $(".inpCajas,.inpUnidades,.inpPeso").val(""); }
    }

    function traspasarArtLote() {
        if( !$(".btnMerlosPreparar img").hasClass("btnAlo") ){ return; }
        if ($("#dvLineasLotes").is(":visible")) { /* artículos con lote */  traspasarConLote(); }
        else { /* artículos sin lote */  traspasarSinLote(); }
    }

    function traspasarConLote() {
        var str = "";
        var stock = 0;
        var unidades = 0;
        var cntrlStock = 0;
        var stockKO = "";
        var esteValor = 0;
        var cajas = 0;
        var stockCajas = 0;
        var peso = 0.000;
        var faltaPeso = false;

        var lineas = "";
        var lasLineas = "";
        $("#tbLineasLotes").find(".trLotesDatos").each(function () {
            $(this).find(".inpPeso").each(function () {
                esteValor = $(this).val();
                if (isNaN(esteValor) || esteValor === "") { esteValor = 0; };
                str += $(this).attr('class').split(" ")[3] + '_D_' + esteValor + "_D_"; peso = esteValor;
                lasLineas += '{"lote":"'+$(this).attr('class').split(" ")[3]+'","peso":"'+esteValor+'"';
            });
            $(this).find(".inpStock").each(function () { esteValor = $(this).val(); if (isNaN(esteValor) || esteValor === "") { esteValor = 0; }; str += esteValor + "_D_"; stock = esteValor; lasLineas += ',"stock":"'+stock+'"'; cntrlStock = $(this).attr('class').split(" ")[3]; });
            $(this).find(".inpUnidades").each(function () { esteValor = $(this).val(); if (isNaN(esteValor) || esteValor === "") { esteValor = 0; }; str += esteValor + "_D_"; unidades = esteValor; lasLineas += ',"unidades":"'+unidades+'"'; });
            $(this).find(".inpCajas").each(function () { esteValor = $(this).val(); if (isNaN(esteValor) || esteValor === "" || esteValor === "Infinity") { esteValor = 0; }; str += esteValor + "_A_"; cajas = esteValor; if (cajas === "Infinity") { cajas = "0"; } lasLineas += ',"cajas":"'+cajas+'"'; });
            $(this).find(".stockCajas").each(function () { esteValor = $(this).text(); if (isNaN(esteValor) || esteValor === "") { esteValor = 0; }; stockCajas = esteValor; });

            if ($(".inpPeso").is(":visible") && parseFloat(unidades) > 0 && parseFloat(peso) === 0.000) { faltaPeso = true; }

            // controlar stock (unidades/cajas)
            if (parseFloat(unidades) > 0 && (parseFloat(unidades) > parseFloat(stock))) { stockKO = "NoHayStock"; }

            lasLineas += '}';
        });
        str = str.substring(0, str.length - 3);
        lasLineas = "["+lasLineas.replace(/}{/g,"},{")+"]";
        if (faltaPeso) { alert("Debes especificar el PESO!"); return; }

        // Control de Stock
        if ((cntrlStock==="1" || cntrlStock==="true") && stockKO !== "") { alert("\nNo hay stock suficiente de algún lote!\nPor favor, verifica los datos."); return; }
        if (unidades === "") { alert("Verifica los campos!"); return; }
        if (isNaN(unidades) || isNaN(peso)) { alert("Ups! Parece que los datos no son correctos!"); return; }

        $("#tbLineasLotes").html(infoCarga("Actualizando los datos...</div>"));
        var jLineas = JSON.parse(lasLineas);
        for(var l=0; l<jLineas.length; l++){            
            (function(){
                var parametros = '{"sp":"pActualizaLineaTraspasoLote"'
                    + ',"pedido":"' + gPedido + '"'
                    + ',"serie":"' + trsps_serie + '"'
                    + ',"articulo":"' + trsps_articulo + '"'
                    + ',"almacen":"' + Almacen + '"'
                    + ',"linea":"' + trsps_linea + '"'
                    + ',"lote":"' + jLineas[l].lote + '"'
                    + ',"peso":"' + jLineas[l].peso + '"'
                    + ',"stock":"' + jLineas[l].stock + '"'
                    + ',"unidades":"' + jLineas[l].unidades + '"'
                    + ',"cajas":"' + parseInt(jLineas[l].cajas) + '"'
                    + ',' + params
                    + '}'; console.log(parametros);
                flexygo.nav.execProcess('pMerlos', '', null, null, [{ 'Key': 'parametros', 'Value': limpiarCadena(parametros) }], 'current', false, $(this), function (ret) {
                    if (ret) {
                        if (ret.JSCode === "KO!") { alert("Error SQL:\r" + resp[1]); return; }
                        $("#dv_LoteEntrada_" + gLoteID).html("<div class='avRojo'>Recuperando los datos...</div>");
                        recargar();
                    } else { alert('Error SP pActualizaLineaTraspasoLote!' + JSON.stringify(ret)); }
                }, false);
            })(l)
        }
    }

    function traspasarSinLote() {
        var cntlStock, elStock, stockKO = false;
        $("#tbCabaceraLotes").find(".trDatosLinea").each(function () {
            cntlStock = $(this).attr("class").split(" ")[1];
            elStock = $(this).attr("class").split(" ")[2];
        });

        var lasCajasStock = $.trim($(".stockCajas").text());
        var lasUdsStock = $.trim($(".stockUnidades").text());

        var lasCajas = $.trim($("#sinLoteCAJAS").val()); if (!$("#sinLoteCAJAS").is(":visible")) { lasCajas = "0"; }
        var laCantidad = $.trim($("#sinLoteUDS").val());
        var elPeso = $.trim($("#sinLotePESO").val()); if (!$("#sinLotePESO").is(":visible")) { elPeso = "0.000"; }

        if ($("#sinLotePESO").is(":visible") && parseFloat(elPeso) === 0.000) { alert("Debes especificar el PESO!"); return; }

        if (cntlStock === "0") {
            if ((parseFloat(lasCajasStock) < parseFloat(lasCajas)) || (parseFloat(lasUdsStock) < parseFloat(laCantidad))) {
                alert("No hay suficiente Stock del artículo!"); stockKO = true;
            }
        }
        if (stockKO) { return; }

        if (laCantidad === "" || elPeso === "") { alert("Verifica los campos!"); return; }
        if (isNaN(laCantidad) || isNaN(elPeso)) { alert("Ups! Parece que los datos no son correctos!"); return; }

        $("#dv_SinLote_" + gID).html("<div class='avRojo'>Actualizando los datos...</div>");

        var parametros = '{"sp":"pActualizaLineaTraspasoSinLote"'
            + ',"pedido":"' + gPedido + '"'
            + ',"serie":"' + trsps_serie + '"'
            + ',"lote":"-"'
            + ',"articulo":"' + trsps_articulo + '"'
            + ',"cajas":"' + lasCajas + '"'
            + ',"unidades":"' + laCantidad + '"'
            + ',"peso":"' + elPeso + '"'
            + ',"linea":"' + trsps_linea + '"'
            + '}';
        flexygo.nav.execProcess('pMerlos', '', null, null, [{ 'Key': 'parametros', 'Value': limpiarCadena(parametros) }], 'current', false, $(this), function (ret) {
            if (ret) {
                var js = JSON.parse(limpiarCadena(ret.JSCode));
                if (js.respuesta === "KO!") {
                    abrirVelo("Error en el Traspaso!");
                    recargar();
                    return;
                }
                $("#dv_LoteEntrada_" + gLoteID).html(infoCarga("Recuperando los datos..."));
                recargar();
            } else { alert('Error SP pActualizaLineaTraspasoSinLote!' + JSON.stringify(ret)); }
        }, false);
    }
    
    function traspasarPedido() {
        if(!$(".btnMerlosTraspaso img").hasClass("btnAlo")){return;}
        // Verificar tabla [MI_Traspaso_PV].[dbo].[EnUso]
        if (contadorTraspasoEnUso === 0) {
            abrirVelo(infoCarga("verificando el pedido..."));
            var parametros = '{"sp":"pTraspasarPV","numero":"'+trsps_numero+'","letra":"'+trsps_serie+'",'+params+'}'; 
            flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
                var js = JSON.parse(limpiarCadena(ret.JSCode));

                // Verificar EnUso
                if (js["msj"] === "enUso" && contadorTraspasoEnUso < 10) {
                    setTimeout(()=>{ contadorTraspasoEnUso++; traspasarPedido(); }, 200);
                }
                else if (js["msj"] === "enUso" && contadorTraspasoEnUso === 10) {
                    contadorTraspasoEnUso = 0;
                    abrirVelo("Se ha superado el número de intentos.<br>La tabla en uso devuelve True."
                        + "<br>Puedes forzar el estado."
                        + "<br>Si el problema persiste contacta con el administrador del sistema."
                        + "<br><br><br><span class='boton esq05' onclick='forzarEstadoEnUso();'>Forzar Estado</span>"
                        + "&nbsp;&nbsp;&nbsp;<span class='boton esq05' onclick='cerrarVelo();'>cancelar</span><br><br>");
                }
                else {                    
                    // Verificar Lotes
                    if (js["msj"] === "verifLotes"){
                        var losArts = "<table id='tbArtSinLoteAsignado' class='tbStd L'>"
                            + "		<tr>"
                            + "			<th>Código</th>"
                            + "			<th>Nombre</th>"
                            + "		</tr>";
                        for (var i in js) { losArts += '<tr><td>' + js[i].CODIGO + '</td><td>' + js[i].NOMBRE + '</td></tr>'; }
                        losArts += "</table>";
                        abrirVelo("Los siguientes artículos trabajan con lote y no tienen ninguno asignado!<br><br>"
                                    +"Entre dentro del artículo indicado del pedido, cancele la preparación y vuelva a asignar el lote para poder continuar con esta operación!!"
                                    + "<br><br>" + losArts
                                    + "<br><br><br><span class='boton esq05' onclick='cerrarVelo();'>aceptar</span><br><br>");
                    }else{
                        // Traspaso del Pedido
                        abrirVelo(
                              "<div id='avTraspasoDef' class='esq10' style='width:100%; margin:auto;;border:2px solid #666; padding:10px; text-align:center;'>"
                            + "   <span style='font:bold 20px arial; color:#07657e;'>Traspaso del Pedido " + gPedido + "</span>"
                            + "</div>"
                            + "<br>"
                            +"<div style='background:#666; padding:10px;' class='esqU10'>"
                            + "     <span style='font:bold 16px arial; color:#FFF;text-align:left;'>CLIENTE &nbsp;&nbsp;&nbsp;</span>"
                            + "     <span class='esqU10' style='font:bold 16px arial; color:#FFF;text-align:left;'>" + js[0].datosCliente[0].codigo + " - " + $.trim(js[0].datosCliente[0].nombre) + "</span>"
                            + "</div>"
                            + "<div class='dvTPVClienteDatos' style='background:rgb(235,235,235); padding:20px;' class='esqD10'>"
                            + "     <div class='CliDatos'>"
                            + "         <div style='font:14px arial; color:#666;text-align:left;'>Dirección</div>"
                            + "         <div style='background:#FFF; margin:1px; padding:5px; font:bold 16px arial; color:#333;text-align:left;'>" + js[0].datosCliente[0].direccion + "</div>"
                            + "         <div style='margin-top:5px; font:14px arial; color:#666;text-align:left;'>Población</div>"
                            + "         <div style='background:#FFF; margin:1px; padding:5px; font:bold 16px arial; color:#333;text-align:left;'>" + js[0].datosCliente[0].poblacion + "</div>"
                            + "         <div style='margin-top:5px; font:14px arial; color:#666;text-align:left;'>Provincia</div>"
                            + "         <div style='background:#FFF; margin:1px; padding:5px; font:bold 16px arial; color:#333;text-align:left;'>" + js[0].datosCliente[0].cp + " " + js[0].datosCliente[0].provincia + "</div>"
                            + "         <div style='margin-top:5px; font:14px arial; color:#666;text-align:left;'>Conductor</div>"
                            + "         <div style='background:#FFF; margin:1px; padding:5px; font:bold 16px arial; color:#333;text-align:left;'>" + js[0].datosCliente[0].agencia_nombre + "</div>"
                            + "         <div style='margin-top:5px; font:14px arial; color:#666;text-align:left;'>Peso</div>"
                            + "         <div style='background:#FFF; margin:1px; padding:5px; font:bold 16px arial; color:#333;text-align:left;'>" + parseFloat(js[0].pesoTotal).toFixed(3) + "</div>"
                            + "     </div>"
                            + "     <div class='Botones'>"
                            + "         <div style='font:16px arial; color:#333;'>Nº Bultos</div>"
                            + "         <input type='text' id='nBultos' onkeyup='replicarBultos()' class='tecNum esq05' style='width:100px; text-align:center; background:#f2f2f2; padding:8px; font:bold 20px arial; color:#11698E;' />"
                            + "		    <br><br>"
                            + "		    <div style='font:16px arial; color:#333;'>" + js[0].etiEtiquetas + "</div>"
                            + "         <input type='text' id='nCajas' class='tecNum esq05' style='width:100px; text-align:center; background:#f2f2f2; padding:8px; font:bold 20px arial; color:#11698E;' />"
                            + "		    <br><br><br>"
                            + "         <img id='btnImpAlb' src='./Merlos/images/btnImprimirAlbaranI.png' class='imgImp alb'> "
                            + "         <img id='btnImpEti' src='./Merlos/images/btnImprimirEtiquetasI.png' class='imgImp eti' style='margin-left:20px;'> "
                            + "     </div>"
                            + "</div>"
                            + " <div id='GenerarAlbaranBotonera' style='margin-top:40px; text-align:center;'>"
                            + "     <span class='btnMRojo esq05' onclick='cerrarVelo()'>CANCELAR</span>"
                            + "     <span id='btnGenerarAlbaran' class='btnMVerde esq05' style='margin-left:30px;' onclick='generarAlbaranDeVenta()'>GENERAR ALBARÁN</span>"
                            + " </div>"
                            + "<div id='avGenerar' style='display:none;'>"
                            + "		<div id='avGenerarTxt' style='background:#4BFE78; padding:20px; font:bold 16px arial; color:#07657e;'>"+icoCarga16+" traspasando el pedido. Por favor, espera...</div>"
                            + "</div>"
                        );
                        $("#nBultos").focus();
                        if (js[0].impAlb === "0") {
                            imprimirAlbaran = 0;
                            $("#btnImpAlb").attr("src", "./Merlos/images/btnImprimirAlbaranO.png");
                            $("#btnGenerarAlbaran").html("IMPRIMIR ETIQUETAS");
                        } else { imprimirAlbaran = 1; }
                        activarObjetos();
                    }
                }
            } else { alert('Error SP pTraspasarPV!'+JSON.stringify(ret)); }}, false);
        }
    }

    function replicarBultos(){ $("#nCajas").val($.trim($("#nBultos").val())); }

    function generarAlbaranDeVenta(){ 
        var losBultos = $.trim($("#nBultos").val());
        if(losBultos===""){ alert("Debes especificar el número de bultos!"); return; }
        if(isNaN(losBultos)){ alert("Ups! Parece que el campo BULTOS no es correcto!"); return; }  
        
        var lasCajas = $.trim($("#nCajas").val());
        if(lasCajas===""){ alert("Debes especificar el número de cajas!"); return; }
        if(isNaN(lasCajas)){ alert("Ups! Parece que el campo CAJAS no es correcto!"); return; }  

        $("#GenerarAlbaranBotonera").hide();
        $("#avGenerar").fadeIn();
        if(FacturaDirecta==="true"){FacturaDirecta=1;}else{FacturaDirecta=0;}
        var parametros = '{"sp":"pAlbaranDeVenta","numero":"'+trsps_numero+'","letra":"'+trsps_serie+'","bultos":"'+losBultos+'"'
            +',"cajas":"'+lasCajas+'","impAlb":"'+imprimirAlbaran+'","impEti":"'+imprimirEtiquetas+'","FacturaDirecta":'+FacturaDirecta+','+params+'}';
        flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
            var js = JSON.parse(ret.JSCode);
            if(js.Error===""){
                $("#avGenerarTxt").html("se ha generado el albarán "+js.respuesta);
                setTimeout(function(){ $("#btnMerlosHome").click(); cerrarVelo();  buscarGlobal(); },2000);
            }else{ alert("Error al generar el albarán de venta:\n"+js.Error); buscarGlobal(); }
        } else { alert('Error SP pAlbaranDeVenta!'+JSON.stringify(ret)); }}, false);
    }    

function calcularModif(i,modo,unicaja,unipeso){
	var cajas = $.trim($("#tbModifPedido #inpCajas"+i).val());
	var uds = $.trim($("#tbModifPedido #inpUds"+i).val());
	var peso = $.trim($("#tbModifPedido #inpPeso"+i).val());
	if(modo==="cajas"){ uds = cajas*parseInt(unicaja); if(parseFloat(unipeso)>0){ peso = uds*parseFloat(unipeso); } }
	if(modo==="uds"){ if(parseFloat(unipeso)>0){ peso = uds*parseFloat(unipeso); } }
	$("#inpCajas"+i).val(cajas);
	$("#inpUds"+i).val(uds);
	$("#inpPeso"+i).val(peso);
}

function intercambiarArticulo(i, liniaArtOrig, artOrig, defiOrig){
    if(!$("#divIntercambio").is(":visible")){
        $("#divIntercambio").remove();
        abrirVelo(
            "<div id='divIntercambio' style='background:#FFF; max-height:500px; overflow:hidden; overflow-y:auto;'>"
            +icoCarga16+" cargando artículos..."
            +"</div>"
        );	
    }	
	if(listadoArticulos===""){ cargarArticulos(Almacen); setTimeout(function(){intercambiarArticulo(i, liniaArtOrig, artOrig, defiOrig);},500); return; }
	setTimeout(function(){
		var contenido =  "<div class='poderCerrarVelo'>Selecciona el artículo para sustituir a<br>"+artOrig+" - "+defiOrig+"</div>"
						+"<div style='margin:10px 0;'>"
                        +"  <input type='text' id='inpBuscArt' style='width:100%;' placeholder='buscar artículo' onkeyup='buscarEnTabla(this.id,\"tbListadoDeArticulos\")' "
                        +"  onclick='event.stopPropagation();'>"
                        +"</div>"
						+"<div style='max-height:400px; overflow:hidden; overflow-y:auto;'>"
                        +"  <table id='tbListadoDeArticulos' class='tbStd'>"
						+"	    <tr>"
						+"		    <th>Almacen</th>"
						+"		    <th>Código</th>"
						+"		    <th>Descripción</th>"
						+"		    <th>Stock</th>"
						+"	    </tr>";
		var js = JSON.parse(listadoArticulos);
		for(var j in js.datos){ 
			if(parseFloat($.trim(js.datos[j].ArticuloStock))>0){
				contenido += "  <tr class='tbData buscarEn' onclick='seleccionarArticuloIntercambio(\""+$.trim(js.datos[j].CODIGO)+"\",\""+$.trim(js.datos[j].NOMBRE)+"\",\""+$.trim(js.datos[j].ArticuloStock)+"\",\""+$.trim(js.datos[j].UNICAJA)+"\",\""+$.trim(js.datos[j].PESO)+"\","+i+","+liniaArtOrig+",\""+artOrig+"\",\""+defiOrig+"\")'>"
							+"	    <td>"+$.trim(js.datos[j].almacen)+"</td>"
							+"	    <td class='buscarEn'>"+$.trim(js.datos[j].CODIGO)+"</td>"
							+"	    <td class='buscarEn'>"+$.trim(js.datos[j].NOMBRE)+"</td>"
							+"	    <td class='taR'>"+$.trim(js.datos[j].ArticuloStock)+"</td>"
							+"  </tr>"; 
			}
		}
		contenido += "</table></div>"
		$("#divIntercambio").html(contenido).fadeIn();
		$("#inpBuscArt").focus();
	},500);
}

function cargarArticulos(almacen){ 
	var parametros = '{"sp":"pArticulos","modo":"lista","almacen":"'+almacen+'"}';	
    flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		listadoArticulos=limpiarCadena(ret.JSCode);
    } else { alert('Error SP pArticulos - lista!'+JSON.stringify(ret)); }}, false);
}

function seleccionarArticuloIntercambio(artDest,defiDest,stockDest,unicajaDest,pesoDest,i,liniaOrig,artOrig,defiOrig){
    abrirVelo(icoCarga16+" asignando el artículo...");
	
	var mpl = {"Empresa":Empresa,"Numero":Numero,"Serie":Serie,"liniaArtOrig":liniaOrig
			,"Ruta":Ruta,"Almacen":Almacen,"pedido":PedidoActivo,"Cliente":$("#spCodCli").text()
			,"artDest":artDest,"defiDest":defiDest,"stockDest":stockDest,"unicajaDest":unicajaDest,"pesoDest":pesoDest
            ,"articuloOrig":artOrig,"defiOrig":defiOrig};
			
	modificarPedidoListado.push(mpl);
    // pintar modificaciones 
    $("#tbLineasDePedidoTraspaso").find(".tbLineasDePedidoTraspasoTR").each(function () {        
        var trId = $(this).attr("id"); 
        var articulo = $(this).attr("data-articulo"); 
        for(var i in modificarPedidoListado){
            if($.trim(modificarPedidoListado[i].articuloOrig)===$.trim(articulo)){
                $(this).find(".tbLineasDePedidoTraspasoTDarticuloPte").html(modificarPedidoListado[i].artDest);
                $(this).find(".tdDefinicion").html(
                    modificarPedidoListado[i].defiDest
                    +"<br><span style='font:12px arial;color:#FF5733;'>ORIGINAL: "+modificarPedidoListado[i].articuloOrig+" - "+modificarPedidoListado[i].defiOrig+"</span>"
                ); 
                // cambiamos imagen de la linea - añadimos inputs de cajas, unidades y peso
                $(this).find(".tbAccion").css("background","url('./Merlos/images/Modificado.png') no-repeat center").css("background-size","90%").parent(); 
                var CajasDisplay = " display:none; ";
                var PesoDisplay = " display:none; ";
                if(parseFloat(unicajaDest)>0){ CajasDisplay=""; }
                if(parseFloat(pesoDest)>0){ PesoDisplay=""; }
                $(this).find(".tbLineasDePedidoTraspasoTDcajasPte").html("<input type='text' class='inpModifCajas' style='width:60px; text-align:center; border:1px solid #CCC; "+CajasDisplay+"' "
                                                                    +"onkeyup='calcularModificacionesCUP(\""+trId+"\",\"cajas\",this.value,\""+unicajaDest+"\",\""+pesoDest+"\")' />"); 
                $(this).find(".tbLineasDePedidoTraspasoTDunidadesPte").html("<input type='text' class='inpModifUnidades' style='width:60px; text-align:center; border:1px solid #CCC; ' "
                                                                    +"onkeyup='calcularModificacionesCUP(\""+trId+"\",\"unidades\",this.value,\""+unicajaDest+"\",\""+pesoDest+"\")' />"); 
                $(this).find(".tbLineasDePedidoTraspasoTDpesoPte").html("<input type='text' class='inpModifPeso' style='width:60px; text-align:center; border:1px solid #CCC; "+PesoDisplay+"' />"); 
            }
        }
    });
    cerrarVelo();
}

function calcularModificacionesCUP(tr,modo,valor,unicaja,unipeso){
    var _uds, _cajas;
    if(modo==="cajas"){ 
        _uds = parseInt(parseInt(valor)*parseInt(unicaja));
        if(isNaN(_uds)){ _uds=0; }
        $("#"+tr).find(".tbLineasDePedidoTraspasoTDunidadesPte").find("input").val(_uds); }
    if(modo==="unidades"){ 
        if(parseFloat(unicaja)>0){
            _uds = valor;
            _cajas = parseInt(parseInt(_uds)/parseInt(unicaja));
            $("#"+tr).find(".tbLineasDePedidoTraspasoTDcajasPte").find("input").val(_cajas); 
        }
    }
    if(parseFloat(unipeso)>0){ $("#"+tr).find(".tbLineasDePedidoTraspasoTDpesoPte").find("input").val( _uds*parseFloat(unipeso) ); }
}

function capturarPeso(img,id){
    $("#"+img).attr("src","./Merlos/images/btnBasculaV.png");
    $.post("../php/traspasoPV.php",{jQueryPost:"capturarPeso",basculaEnUso:basculaEnUso}).done(function(data){
		if(data.length>10){ alert("ERROR BÁSCULA!"); $("#"+img).attr("src","../images/btnBascula.png"); return; }
		$("#"+id).val(data); $("#"+img).attr("src","../images/btnBascula.png");
    });
}

var TotalSuma=0;
function capturarPesoSuma(img,id){    
    abrirVelo(
	    "<div id='avSumaPesos' class='esq10' "+
	    "style='width:100%; margin:30px auto;background:#daf7ff;border:2px solid #07657e;padding:10px;text-align:center;'>"+
	    "   <div><img src='../images/btnMerlosCerrar.png' style='float:right; width:50px;' onclick='cerrarCapturaPesoSuma()'></div>"+
	    "   <div><span style='font:bold 20px Gothic; color:#07657e;'>Suma de Pesadas</span></div>"+
	    "   <div style='height:50px;'></div>"+
	    "   <div style='overflow:hidden;'>"+
	    "		<div style='float:left;'>"+
	    "			<div style='font:bold 16px Gothic; color:#07b55a;'>PESADAS</div>"+
	    "			<br>"+
	    "			<div id='dvPesadas' style='background:#FFF; padding:20px; text-align:right;'></div>"+
	    "		</div>"+
	    "		<div style='float:left; margin-left:30px;'>"+
	    "			<div style='font:bold 16px Gothic; color:#07b55a;'>TOTAL</div>"+
	    "			<br>"+
	    "			<div id='dvTotalPesadas' style='background:#FFF; padding:20px; text-align:center;'></div>"+
	    "		</div>"+		
	    "		<div style='float:right; margin-left:30px;'>"+
	    "			<div style='font:bold 16px Gothic; color:#07b55a;'>TOTAL</div>"+
	    "			<div><img src='../images/btnBascula.png'     style='width:50px;' onclick='traspasarSumaPesos(\""+img+"\",\""+id+"\")'></div>"+
	    "		</div>"+
	    "		<div style='float:right; margin-left:40px;'>"+
	    "			<div style='font:bold 16px Gothic; color:#07b55a;'>PESAR</div>"+
	    "			<div><img id='imgCapturaPesoSuma' src='../images/btnBasculaSuma.png' style='width:50px;' onclick='capturarPesoDeSuma()'></div>"+
	    "		</div>"+
	    "		<div style='float:right;'>"+
	    "			<div style='font:bold 16px Gothic; color:#07b55a;'>MANUAL</div>"+
	    "			<div>"+
	    "			    <input type='text' id='entradaPesoManual' style='width:100px; text-align:center;' class='tecNum'>"+
	    "			</div>"+
	    "		</div>"+
	    "	</div>"+
	    "</div>"
    );
	activarObjetos();
}

function capturarPesoDeSuma(manual){
    if(manual){
		TotalSuma += parseFloat(manual);	
		$("#dvPesadas").append("<div>"+manual+"</div>");		
		$("#dvTotalPesadas").html("<div style='font:bold 20px Gothic; color:#07b55a;'>"+TotalSuma.toFixed(3)+"</div>");	
		$("#entradaPesoManual").val("");
		$("#dvTeclado").slideUp();
    }else{ 
		$("#imgCapturaPesoSuma").attr("src","../images/btnBasculaSumaV.png");
		$.post("../php/traspasoPV.php",{jQueryPost:"capturarPeso"}).done(function(data){
			if(data.length>30){ $("#imgCapturaPesoSuma").attr("src","../images/btnBasculaSuma.png"); return; }
			TotalSuma += parseFloat(data);
			$("#dvPesadas").append("<div>"+data+"</div>");		
			$("#dvTotalPesadas").html("<div style='font:bold 20px Gothic; color:#07b55a;'>"+TotalSuma.toFixed(3)+"</div>");		
			$("#imgCapturaPesoSuma").attr("src","../images/btnBasculaSuma.png");
		});
    }
}

function traspasarSumaPesos(img,id){
	$("#"+id).val(TotalSuma);	
	cerrarCapturaPesoSuma();
}

function cerrarCapturaPesoSuma(){
	$("#dvPesadas").html("");
	TotalSuma = 0.000;
	cerrarVelo();
}

function finalizarPedido(confirmacion){
    if(!$(".btnFinalizarPedido img").hasClass("btnAlo")){ return; } 
    if(confirmacion){
        $("#veloAvDesbloqueo").html(icoAlert50+"<br><br>finalizando...");
        var parametros = '{"sp":"pPedidos","modo":"finalizarPedido","Empresa":"'+Empresa+'","Numero":"'+Numero+'","Serie":"'+Serie+'"}';
        flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
            $(".tbSeccion").hide(); $("#dvTrabajoInputs, #dvResultados").slideDown();
            cancelarPreparacion(); modificacionesCancelar(); cambiarArticuloCancelar(); buscarGlobal(); cerrarVelo();
        } else { alert('Error SP pPedidos - finalizarPedido!'+JSON.stringify(ret)); }}, false);
    }else{
        abrirVelo(`
            <div id='veloAvDesbloqueo' class='taC' style='font:16px arial; color:#333;'>
                `+icoAlert50+` <br><br>
                El pedido `+Numero+`-`+Serie+`<br>se marcará como TRASPASADO<br>en la base de datos
                <br><br><br><span class='btnVerde esq05' onclick='finalizarPedido(1);'>aceptar</span>
                &nbsp;&nbsp;&nbsp;<span class='btnRojo esq05' onclick='cerrarVelo();'>cancelar</span>
            </div>
        `);
    }
}
</script>
