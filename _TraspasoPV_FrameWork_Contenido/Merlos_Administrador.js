
<div class="dvTit">Configuración de la Aplicación</div>

<div id="dvComunesYEmpresa">
	<div class="tit1">Comunes y Empresa</div>
	<table id="tbComunEmpresa" class="tbFrm">
		<tr>
			<td>
				BD COMUN
				<br><select id="selComunes" class="esq05" onchange="cargarEmpresas()"></select>
			</td>
			<td>
				Empresa
				<br><select id="selEmpresas" class="esq05"></select>
			</td>
			<td class="btnMVerde esq05" style="text-align:center;" onclick="configurarEmpresa()">Configurar</td>
		</tr>
		<tr><td colspan="2" style="height:10px;"></td></tr>
		<tr>
			<td>Personalización:</td>
			<td id="tdPersonalizacion"><input type="text" id="inpPersonalizacion" class="padding2" style="width:90%; background:#CCC; border:0; outline: none;" readonly disabled /></td>
		</tr>
	</table>
</div>

<div id="dvAppPersonalizaciones" class="fondo">
	<div class="tit1"><img id="imgPersonalizaciones" class="imgMenu" /> Personalizaciones a Clientes</div>
	<div id="dvAppPersonalizacionesContent" class="seccion" style="width:90%; margin:10px auto;">
		<div style="margin-top:10px;">
			Crear nueva personalización <input type="text" id="inpPersEmpresa" class="esq05 padding5" style="width:200px;" />
			&nbsp;&nbsp;<span id="btnPersCrear" class="btnVerde" onclick="crearPersonalizacion_Click()">crear</span>
			<span class="FrmPers">
				<span style="margin-left:50px;"></span>Nombre <input type="text" id="inpPersNombre" class="esq05 padding5" style="width:200px;" />
				<span style="margin-left:50px;"></span>Tipo <select id="selPersTipo" class="esq05 padding5" style="width:200px;"></select>
			</span>
		</div>
		<div id="dvFrmPers" class="FrmPers" style="width:90%; margin:10px auto;">
			Contenido de la personalización
			<textarea id="taPersContenido" style="width:100%; margin:auto; height:500px; background:#FFF; padding:10px; box-sizing:border-box;"></textarea>
		</div>
	</div>
</div>

<div id="dvAppOpciones" class="fondo">
	<div class="tit1"><img id="imgOpciones" class="imgMenu" /> Opciones de la Aplicación</div>
	<div id="dvAppOpcionesContent" class="seccion">
		<div class="row">
			<span id="spSeriesFiltro" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Series - Mostrar todas</span>
			<div id="dvSeriesFiltro" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spImpAlbGen" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Imprimir Albaran al generarlo</span>
			<div id="dvImpAlbGen" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spResLin" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Resaltar lineas Dto. 100%</span>
			<div id="dvResLin" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spFactCajas" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Facturación cajas</span>
			<div id="dvFactCajas" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spImpEtiquetas" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Imprimir etiquetas</span>
			<div id="dvImpEtiquetas" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spImpAlbFact" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Imprimir Albaranes y Facturas</span>
			<div id="dvImpAlbFact" class="dvSubSel"></div>
		</div>
	</div>
</div>

<div id="dvAppBasculas" class="fondo">
	<div class="tit1"><img id="imgBasculas" class="imgMenu" /> Básculas</div>
	<div id="dvAppBasculasContent" class="seccion">
		<div class="row cab">Báscula</div>
		<div class="row cab">IP</div>
		<div class="row cab">Puerto</div>
		<div class="row cab">Sentencia</div>

		<div class="row">
			<input type="text" id="inpBasculaIPActiva01" class="inv" disabled="">
			<img id="imgBascula01" src="./Merlos/images/btnBascula01_Off.png">
		</div>
		<div class="row"><input type="text" id="inpBasculaIP01" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPPuerto01" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPSentencia01" disabled /></div>

		<div class="row">
			<input type="text" id="inpBasculaIPActiva02" class="inv" disabled="">
			<img id="imgBascula02" src="./Merlos/images/btnBascula02_Off.png">
		</div>
		<div class="row"><input type="text" id="inpBasculaIP02" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPPuerto02" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPSentencia02" disabled /></div>

		<div class="row">
			<input type="text" id="inpBasculaIPActiva03" class="inv" disabled />
			<img id="imgBascula03" src="./Merlos/images/btnBascula03_Off.png">
		</div>
		<div class="row"><input type="text" id="inpBasculaIP03" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPPuerto03" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPSentencia03" disabled /></div>

		<div class="row">
			<input type="text" id="inpBasculaIPActiva04" class="inv" disabled />
			<img id="imgBascula04" src="./Merlos/images/btnBascula04_Off.png">
		</div>
		<div class="row"><input type="text" id="inpBasculaIP04" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPPuerto04" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPSentencia04" disabled /></div>
	</div>
</div>

<div id="dvAppIdiomas" class="fondo">
	<div class="tit1"><img id="imgIdiomas" class="imgMenu" /> Idiomas</div>
	<div id="dvAppIdiomasContent" class="seccion">
		<div class="row cab chv"><img id="imgIdiomaNew" src="./Merlos/images/anadirLinea.png" onclick="idiomaNew()" /></div>
		<div class="row cab">Idioma <span style="font:12px arial; color:#666;">cód - nombre</span></div>
		<div class="row cab">Tipo <span style="font:12px arial; color:#666;">cód - nombre</span></div>
		<div class="row cab">Report <span style="font:12px arial; color:#666;">nombre</span></div>

		<div class="row new chv">
			<img src="./Merlos/images/floppy.png" onclick="idiomaNewGuardar()" />
			&nbsp;&nbsp;&nbsp;&nbsp;
			<img src="./Merlos/images/cerrar.png" onclick="idiomaNew(1)" />
		</div>
		<div class="row new">
			<input type="text" id="inpIdioma" onclick="cargarObjeto(this.id,'Idiomas','inpIdiomaCont','dvAppIdiomas'); event.stopPropagation();" />
			<div id="inpIdiomaCont" class="dvSel"></div>
		</div>
		<div class="row new">
			<input type="text" id="inpTipo" onclick="cargarObjeto(this.id,'Tipos','inpTipoCont','dvAppIdiomas'); event.stopPropagation();" />
			<div id="inpTipoCont" class="dvSel"></div>
		</div>
		<div class="row new">
			<input type="text" id="inpReport" />
		</div>
	</div>
</div>

<div id="dvAppUsuarios" class="fondo">
	<div class="tit1"><img id="imgUsuarios" class="imgMenu" /> Usuarios</div>
	<div id="dvAppUsuariosContent" class="seccion">
		<div class="row cab">Usuario</div>
		<div class="row cab">Impresora Documentos</div>
		<div class="row cab">Impresora Etiquetas</div>
		<div class="row cab">Report de Albarán</div>
		<div class="row cab">Report de Factura</div>
		<div class="row cab">Report de Etiquetas</div>
		<div class="row cab">Báscula predeterminada</div>

		<div class="row" style="position:relative;">
			<input type="text" id="inpMerlosUsuario" readonly onclick="cargarUsuarios(); event.stopPropagation();" />
			<div id="dvUsuariosListado" class="dvSel"></div>
		</div>
		<div class="row"><input type="text" id="inpImpDocu" class="inpUsuario" /></div>
		<div class="row"><input type="text" id="inpImpEti" class="inpUsuario" /></div>
		<div class="row"><input type="text" id="inpReportAlb" class="inpUsuario" /></div>
		<div class="row"><input type="text" id="inpReportFra" class="inpUsuario" /></div>
		<div class="row"><input type="text" id="inpReportEti" class="inpUsuario" /></div>
		<div class="row"><input type="text" id="inpBasculaPredet" class="inpUsuario" /></div>
	</div>
</div>




<script> /* #### script #### script #### script #### script #### script #### script #### script #### script #### script #### script #### script #### script #### script #### */
cerrarVelo();

$("#mainNav, .seccion").hide();
$("#dvComunesYEmpresa").show();

var Configuracion = "";				
var bascula01Activa = "0", bascula02Activa = "0", bascula03Activa = "0", bascula04Activa = "0";
var comunesCargados = false;
var empresasCargadas = false;
var seriesCargadas = false;
var lasSeries = "";
var seriesImpAlbGenCargadas = false;
var lasSeriesImpAlbGen = "";
var seriesResLinCargadas = false;
var lasSeriesResLin = "";
var seriesFactCajasCargadas = false;
var lasSeriesFactCajas = "";
var seriesSeriesFiltroCargadas = false;
var lasSeriesSeriesFiltro = "";
var confOpIO = 0;
var basculaTipoConexion = "Ethernet";
var personalizaciones = ["Disteco","Telsan"];

var checkO = "./Merlos/images/check_O.png";
var checkI = "./Merlos/images/check_I.png";

comprobarConfiguracion();

$(".confOp").on("click", function () { confOp_Click(this.id); event.stopPropagation(); });
$("#dvAppBasculas img").on("click",function(){ botonBascula(this.id); });
$("#dvAppBasculas input").on("blur",function(){ basculasConfiguracion();});

$(".imgMenu").off().on("click", function () { 
	var id = this.id;
	var a = $("#"+id).parent().parent().attr("id")+"Content";
	if($("#"+id).hasClass("a90g")){ 
		$("#"+id).attr("src","./Merlos/images/flechaG.png").removeClass("a90g").addClass("a0g"); 
		$("#"+a).slideUp();
	}else {		
		$(".imgMenu").attr("src","./Merlos/images/flechaG.png").removeClass("a90g").addClass("a0g"); 
		$("#"+id).attr("src","./Merlos/images/flechaVerde.png").removeClass("a0g").addClass("a90g");
		$(".seccion").hide();
		$("#"+a).slideDown();		
	}
});

function crearPersonalizacion_Click(){
	var empresa = $.trim($("#inpPersEmpresa").val());
	if(empresa===""){ abrirVelo(icoAlert50+"<br><br>Debes indicar un nombre para la personalización!<br>Recomendado: nombre de la empresa"+btnAceptarCerrarVelo); }
	else{
		if($("#btnPersCrear").text()==="guardar"){
			var persNombre = $.trim($("#inpPersNombre").val());
			var PersContenido = $.trim($("#taPersContenido").val()).replace(/"/g,"mi_COMILLAS_mi").replace(/'/g,"mi_COMILLA_mi").replace(/\n/g,"mi_ENTER_mi").replace(/\t/g,"mi_TAB_mi");
			if(persNombre===""){ abrirVelo(icoAlert50+"<br><br>Debes indicar un nombre para el contenido de la personalización!"+btnAceptarCerrarVelo); return; }
			if(PersContenido===""){ abrirVelo(icoAlert50+"<br><br>No existe contenido para la personalización!"+btnAceptarCerrarVelo); return; }

			abrirVelo(icoCarga16+" guardando la personalización...");
			var parametros = '{"sp":"pConfiguracion","modo":"guardarPersonalizacion","empresa":"'+empresa+'","nombre":"'+persNombre+'","tipo":"'+$("#selPersTipo").val()+'","contenido":"'+PersContenido+'"}';
			console.log(parametros);
			flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':parametros}],'current',false,$(this),function(ret){if(ret){
				cerrarVelo();
			} else { alert('Error SP pConfiguracion - guardarPersonalizacion!\n'+JSON.stringify(ret)); }}, false);
		}else{
			$("#btnPersCrear").text("guardar");
			$(".FrmPers").slideDown();
			cargarPersTipos();
		}
	}
}

function cargarPersTipos(){
	var color = "red";
	$("#selPersTipo").html("<option>cargando...</option>").css("color",color);
	var parametros = '{"sp":"pConfiguracion","modo":"cargarTipos"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		var contenido="";
		var js = JSON.parse(ret.JSCode);
		if(js.length>0){
			for(var i in js){ contenido += "<option value='"+js[i].Id+"'>"+js[i].Nombre+"</option>"; }
			color="#333;";
		}else{ contenido = "<option>Sin resultados</option>"; }
		$("#selPersTipo").html(contenido).css("color",color);
	} else { alert('Error SP pConfiguracion - cargarTipos!\n'+JSON.stringify(ret)); }}, false);
}

function confOp_Click(id) {
	// Objetos
	var objeto = id.split("sp")[1];
	if ($("#" + id).find("img").attr("src") === checkO) {
		$("#" + id).find("img").attr("src", checkI);
		if (id === "spImpAlbGen") { cargarImpAlbGen(false); $("#dvAppOpciones").css("overflow","visible"); }
		if (id === "spResLin") { cargarResLin(); $("#dvAppOpciones").css("overflow","visible"); }
		if (id === "spFactCajas") { cargarFactCajas(); $("#dvAppOpciones").css("overflow","visible"); }
		confOpIO = 1;
	} else {
		confOpIO = 0;
		$("#" + id).find("img").attr("src", checkO);
		if (id === "spSeriesFiltro") { cargarSeriesFiltro(); $("#dvAppOpciones").css("overflow","visible"); }
	}

	objetoIO(objeto, '', confOpIO);

	// Personalizaciones
	personalizaciones.forEach(elemento => { if(id==="spPers"+elemento){ personalizacion(elemento); } });
}

function personalizacion(cliente){
	var parametros = '{"sp":"pConfiguracion","modo":"personalizacion","cliente":"' + cliente + '","confOpIO":"' + confOpIO +'"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		
	} else { alert('Error SP pConfiguracion - personalizacion - cliente!\n'+JSON.stringify(ret)); }}, false);
}

function objetoIO(objeto, valor, confOpIO) {
	var parametros = '{"sp":"pConfiguracion","modo":"objetoIO","objeto":"' + objeto + '","valor":"' + valor + '","confOpIO":"' + confOpIO +'"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		
	} else { alert('Error SP pConfiguracion - objetoIO - '+objeto+'!\n'+JSON.stringify(ret)); }}, false);
}

function iniciar(){ 
	cargarSeries();
	cargarSeriesObjeto("ImpAlbGen");
	cargarSeriesObjeto("ResLin");
	cargarSeriesObjeto("FactCajas");
	cargarSeriesObjeto("SeriesFiltro");
	cargarConfiguracion();
	cargarIdiomas();
	cargarPersonalizacion();
	
	$(".imgMenu").attr("src","./Merlos/images/flechaG.png").css("width","20px");
	$("#imgOpciones").attr("src","./Merlos/images/flechaVerde.png").addClass("a90g");
	$("#dvAppOpcionesContent").slideDown();
}

function cargarComunes() {
	$("#tbComunEmpresa").fadeIn();
	$("#selComunes").html("<option>cargando comunes...</option>").css("color", "red");
	var contenido = "<option></option>";

	var parametros = '{"sp":"pComunes"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		var elJS = JSON.parse(ret.JSCode);
		if (elJS.length > 0) {
			for (var i in elJS) { contenido += "<option>" + elJS[i].nombre + "</option>"; }			
			$("#selComunes").html(contenido).css("color", "#333");
			comunesCargados = true;
		} 
	} else { alert('Error SP pComunes!'+JSON.stringify(ret)); }}, false);
}

function cargarEmpresas() {
	$("#selEmpresas").html("<option>cargando empresas...</option>").css("color", "red");
	var contenido = "<option></option>";
	var comunes = $.trim($("#selComunes").val());

	var parametros = '{"sp":"pEmpresas","comun":"' + comunes+'"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		var elJS = JSON.parse(ret.JSCode);
		if (elJS.empresas.length > 0) {
			for (var i in elJS.empresas) { contenido += "<option value='" + elJS.empresas[i].codigo + "'>" + elJS.empresas[i].codigo + " - " + elJS.empresas[i].nombre + "</option>"; }
			$("#selEmpresas").html(contenido).css("color", "#333");
			empresasCargadas = true;
		}
	} else { alert('Error SP pEmpresas!\n'+JSON.stringify(ret)); }}, false);
}

function configurarEmpresa(confirmado) {
	if(confirmado){	
		var comun = $.trim($("#selComunes").val());
		var empresa = $.trim($("#selEmpresas").val());
		var nombreEmpresa = $.trim($('select[id="selEmpresas"] option:selected').text());

		if (comun === "" || empresa === "") { alert("Debes seleccionar COMUNES y EMPRESA!"); return; }

		abrirVelo(icoCarga50 + "<br><br>configurando la empresa<br><br><span class='tit1'>" + nombreEmpresa+"</span>");
		var parametros = '{"sp":"pConfiguracion","modo":"configurarEmpresa","comun":"' + comun + '","empresa":"' + empresa + '","nombreEmpresa":"' + nombreEmpresa +'"}';
		flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
			if(ret.JSCode!=="OK"){ abrirVelo(icoAlert50+"<br><br>Ha ocurrido un error al configurar la empresa!!!<br><br>Contacta con el administrador."); }
			else{ crearVistasYProcedimientos(); }
		} else { alert('Error SP pConfiguracion - configurarEmpresa!\n'+JSON.stringify(ret)); }}, false);
	}else{
		abrirVelo(`
			Confirma para configurar el portal.
			<br><br><br>
			<span class='btnMVerde' onclick='configurarEmpresa(1)'>configurar</span>
			&nbsp;&nbsp;&nbsp;
			<span class='btnMRojo' onclick='cerrarVelo();'>cancelar</span>
		`);
	}
}

function crearVistasYProcedimientos(){
	abrirVelo(icoCarga50 + "<br><br><span class='tit1'>creando las vistas...</span>",null,1);
	var parametros = '{"sp":"pConfiguracion","modo":"crearVistas"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		if(ret.JSCode.indexOf("Error!")!=-1){ abrirVelo(icoAlert50+"<br><br>Se ha producido un error al generar las vistas!!!<br><br>Contacta con el administrador."); return; }
		abrirVelo(icoCarga50 + "<br><br><span class='tit1'>creando las funciones...</span>",null,1);
		parametros = '{"sp":"pConfiguracion","modo":"crearFunciones"}';
		flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
			if(ret.JSCode.indexOf("Error!")!=-1){ abrirVelo(icoAlert50+"<br><br>Se ha producido un error al generar las funciones!!!<br><br>Contacta con el administrador.",null,1); return; }
				abrirVelo(icoCarga50 + "<br><br><span class='tit1'>creando las personalizaciones...</span>",null,1);
				parametros = '{"sp":"pConfiguracion","modo":"crearPersonalizaciones"}';
				flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
					if(ret.JSCode.indexOf("Error!")!=-1){ 
						var err = JSON.parse(limpiarCadena(ret.JSCode));
						abrirVelo(icoAlert50+"<br><br>Se ha producido un error al generar la personalización:"
						+"<br><br>Empresa: "+err.empresa
						+"<br>Procedimiento: "+err.sp
						+"<br><br><br><span class='btnMRojo' onclick='cerrarVelo();'>aceptar</span>",null,1);
						$("#spPers"+err.empresa).click(); 
						return; 
					}
					iniciar();
				} else { alert('Error SP pConfiguracion - crearPersonalizaciones!\n'+JSON.stringify(ret)); }}, false);
		} else { alert('Error SP pConfiguracion - crearFunciones!\n'+JSON.stringify(ret)); }}, false);
	} else { alert('Error SP pConfiguracion - crearVistas!\n'+JSON.stringify(ret)); }}, false);
}

function esperaYcargaComunYEmpresa(comun, empresa) {
	if (!comunesCargados) { cargarComunes(); setTimeout(function () { esperaYcargaComunYEmpresa(comun, empresa); }, 250); }
	else {
		$("#selComunes").val(comun);
		if (!empresasCargadas) { setTimeout(function () { cargarEmpresas(); esperaYcargaComunYEmpresa(comun, empresa); }, 500); }
		else { setTimeout(function () { $("#selEmpresas").val(empresa); }, 500);}
	}
}

function comprobarConfiguracion() {
	var parametros = '{"sp":"pConfiguracion","modo":"comprobarConfiguracionEmpresa"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		var js = JSON.parse(limpiarCadena(ret.JSCode));
		if(js[0].error=="NoExisteConfiguracion"){ cargarComunes(); cerrarVelo(); }
		else{ esperaYcargaComunYEmpresa(js[0].Comun,js[0].Empresa); iniciar(); }
	} else { alert('Error SP pConfiguracion - comprobarConfiguracion!\n'+JSON.stringify(ret)); }}, false);
}

function cargarSeries() {
	var parametros = '{"sp":"pSeries"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		lasSeries = ret.JSCode;
		seriesCargadas = true;
	} else { alert('Error SP pSeries!\n'+JSON.stringify(ret)); }}, false);
}

function cargarSeriesObjeto(objeto) {
	var parametros = '{"sp":"pSeries","modo":"cargarSeriesObjeto","objeto":"'+objeto+'"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		if (objeto === "ImpAlbGen") { lasSeriesImpAlbGen = ret.JSCode; seriesImpAlbGenCargadas = true; }
		if (objeto === "ResLin") { lasSeriesResLin = ret.JSCode; seriesResLinCargadas = true; }
		if (objeto === "FactCajas") { lasSeriesFactCajas = ret.JSCode; seriesFactCajasCargadas = true; }
		if (objeto === "SeriesFiltro") { lasSeriesSeriesFiltro = ret.JSCode; seriesSeriesFiltroCargadas = true; }
	} else { alert('Error SP pSeries - cargarSeriesObjeto!\n'+JSON.stringify(ret)); }}, false);
}

function cargarSeriesFiltro(tf) {
	if (!tf) { $("#dvSeriesFiltro").html(icoCarga16 + " cargando series...").slideDown(); }
	var contenido = "";
	if (seriesCargadas && seriesSeriesFiltroCargadas) {
		var series = JSON.parse(lasSeries);
		if(series.length>0){
			for (var i in series) {
				var elColor = " background:#666; ";
				if (lasSeriesSeriesFiltro.indexOf(series[i].codigo) !== -1) { elColor = " background:green;"; }
				contenido += "<div class='dvConfSerieSeriesFiltro C' style='margin:2px; padding:5px; color:#FFF; " + elColor + "' "
					+ "onclick='asignarSerieConf(\"SeriesFiltro\",\"" + series[i].codigo + "\");'>"
					+ series[i].codigo
					+ "</div>";
			}
		}else{ $(document).click(); abrirVelo("No se han obtenido series para filtrar!"+btnAceptarCerrarVelo); }
		$("#dvSeriesFiltro").html(contenido);
	} else { setTimeout(function () { cargarSeries(); cargarSeriesObjeto("SeriesFiltro"); cargarSeriesFiltro(true); }, 100); }
}

function cargarImpAlbGen(tf){
	if(!tf){ $("#dvImpAlbGen").html(icoCarga16 + " cargando series...").slideDown(); }
	var contenido = "";
	if(seriesCargadas && seriesImpAlbGenCargadas){
		var series = JSON.parse(lasSeries);
		if(series.length>0){
		for(var i in series){ 
			var elColor = " background:#666; ";
			if(lasSeriesImpAlbGen.indexOf(series[i].codigo)!==-1){ elColor = " background:green;";}
			contenido += "<div class='dvConfSerieImpAlbGen C' style='margin:2px; padding:5px; color:#FFF; "+elColor+"' "
					  +  "onclick='asignarSerieConf(\"ImpAlbGen\",\""+series[i].codigo+"\");'>"
					  +  series[i].codigo
					  +  "</div>"; 
			}
		}
		$("#dvImpAlbGen").html(contenido);
	}else{ setTimeout(function(){cargarImpAlbGen(tf);},100); }
}

function cargarResLin(tf){
	if(!tf){ $("#dvResLin").html(icoCarga16 + " cargando series...").slideDown(); }
	var contenido = "";
	if(seriesCargadas && seriesResLinCargadas){
		var series = JSON.parse(lasSeries);
		if(series.length>0){
		for(var i in series){ 
			var elColor = " background:#666; ";
			if(lasSeriesResLin.indexOf(series[i].codigo)!==-1){ elColor = " background:green;";}
			contenido += "<div class='dvConfSerieResLin C' style='margin:2px; padding:5px; color:#FFF; "+elColor+"' "
					  +  "onclick='asignarSerieConf(\"ResLin\",\""+series[i].codigo+"\");'>"
					  +  series[i].codigo
					  +  "</div>"; 
			}
		}
		$("#dvResLin").html(contenido);
	}else{ setTimeout(cargarResLin,100); }
}

function cargarFactCajas(tf){
	if(!tf){ $("#dvFactCajas").html(icoCarga16 + " cargando series...").slideDown(); }
	var contenido = "";
	if(seriesCargadas && seriesFactCajasCargadas){
		var series = JSON.parse(lasSeries);
		if(series.length>0){
		for(var i in series){ 
			var elColor = " background:#666; ";
			if(lasSeriesFactCajas.indexOf(series[i].codigo)!==-1){ elColor = " background:green;";}
			contenido += "<div class='dvConfSerieFactCajas C' style='margin:2px; padding:5px; color:#FFF; "+elColor+"' "
					  +  "onclick='asignarSerieConf(\"FactCajas\",\""+series[i].codigo+"\");'>"
					  +  series[i].codigo
					  +  "</div>"; 
			}
		}
		$("#dvFactCajas").html(contenido);
	}else{ setTimeout(cargarFactCajas,100); }
}

function asignarSerieConf(objeto,serie){ 
	$('.dvConfSerie'+objeto+':contains("'+serie+'")').css("background","orange").html(icoCarga16);
	var parametros = '{"sp":"pSeries","modo":"asignarSerieConf","objeto":"'+objeto+'","serie":"'+serie+'"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		if(objeto==="ImpAlbGen"){ seriesImpAlbGenCargadas=false; cargarSeriesObjeto(objeto); cargarImpAlbGen(true); }
		if(objeto==="ResLin"){ seriesResLinCargadas=false; cargarSeriesObjeto(objeto); cargarResLin(true); }
		if(objeto==="FactCajas"){ seriesFactCajasCargadas=false; cargarSeriesObjeto(objeto); cargarFactCajas(true); }		
		if(objeto==="SeriesFiltro"){ seriesSeriesFiltroCargadas=false; cargarSeriesObjeto(objeto); cargarSeriesFiltro(true); }
    } else { alert('Error SP pSeries - asignarSerieConf!'+JSON.stringify(ret)); }}, false);
}

function botonBascula(id, noEnviar){
	var elId = id.split("imgBascula")[1];
	if($("#"+id).attr("src")==="./Merlos/images/btnBascula"+elId+"_Off.png"){
		$("#"+id).attr("src","./Merlos/images/btnBascula"+elId+"_Activa.png");
		$("#inpBasculaIP"+elId+",#inpBasculaIPPuerto"+elId+",#inpBasculaIPSentencia"+elId).prop("disabled",false).css("color","#333");
		window["bascula"+elId+"Activa"] = 1;
	}else{
		$("#"+id).attr("src","./Merlos/images/btnBascula"+elId+"_Off.png");
		$("#inpBasculaIP"+elId+",#inpBasculaIPPuerto"+elId+",#inpBasculaIPSentencia"+elId).prop("disabled",true).css("color","#999");
		window["bascula"+elId+"Activa"] = 0;
	}
	if(!noEnviar){ basculasConfiguracion(); }	
}

function basculasConfiguracion(){
	var bascula1 = '{'
				+'"bascula":"bascula01"'
				+',"activa":"'+bascula01Activa+'"'
				+',"basculaTipoConexion":"'+basculaTipoConexion+'"'
				+',"IP":"'+$.trim($("#inpBasculaIP01").val())+'"'
				+',"puerto":"'+$.trim($("#inpBasculaIPPuerto01").val())+'"'
				+',"sentencia":"'+$.trim($("#inpBasculaIPSentencia01").val())+'"'
				+'}';
	var bascula2 = '{'
				+'"bascula":"bascula02"'
				+',"activa":"'+bascula02Activa+'"'
				+',"basculaTipoConexion":"'+basculaTipoConexion+'"'
				+',"IP":"'+$.trim($("#inpBasculaIP02").val())+'"'
				+',"puerto":"'+$.trim($("#inpBasculaIPPuerto02").val())+'"'
				+',"sentencia":"'+$.trim($("#inpBasculaIPSentencia02").val())+'"'
				+'}';
	var bascula3 = '{'
				+'"bascula":"bascula03"'
				+',"activa":"'+bascula03Activa+'"'
				+',"basculaTipoConexion":"'+basculaTipoConexion+'"'
				+',"IP":"'+$.trim($("#inpBasculaIP03").val())+'"'
				+',"puerto":"'+$.trim($("#inpBasculaIPPuerto03").val())+'"'
				+',"sentencia":"'+$.trim($("#inpBasculaIPSentencia03").val())+'"'
				+'}';
	var bascula4 = '{'
				+'"bascula":"bascula04"'
				+',"activa":"'+bascula04Activa+'"'
				+',"basculaTipoConexion":"'+basculaTipoConexion+'"'
				+',"IP":"'+$.trim($("#inpBasculaIP04").val())+'"'
				+',"puerto":"'+$.trim($("#inpBasculaIPPuerto04").val())+'"'
				+',"sentencia":"'+$.trim($("#inpBasculaIPSentencia04").val())+'"'
				+'}';

	var parametros = '{"sp":"pBasculas","modo":"edicion","bascula1":['+bascula1+'],"bascula2":['+bascula2+'],"bascula3":['+bascula3+'],"bascula4":['+bascula4+']}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){

	} else { alert('Error SP pBasculas - edicion!'+JSON.stringify(ret)); }}, false);
}

function cargarConfiguracion(){
	abrirVelo(icoCarga50+"<br><br>cargando la configuración...");
	var parametros = '{"sp":"pConfiguracion","modo":"cargarConfiguracion"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		var js = JSON.parse(limpiarCadena(ret.JSCode));
		// Configuración
		for(var i in js.configuracion){
			var conf = js.configuracion[i];
			if(conf.activo===1){ $("#sp" + conf.objeto).find("img").attr("src", checkI); }else{ $("#sp" + conf.objeto).find("img").attr("src", checkO); }
		}
		// Básculas
		for(var i in js.basculas){
			var basc = js.basculas[i];
			if(basc.activa){ botonBascula("img" + capitalizar(basc.bascula),1); }
			$("#inpBasculaIP"+basc.bascula.split("bascula")[1]).val(basc.ip);
			$("#inpBasculaIPPuerto"+basc.bascula.split("bascula")[1]).val(basc.puerto);
			$("#inpBasculaIPSentencia"+basc.bascula.split("bascula")[1]).val(basc.sentencia);
		}
		cerrarVelo();
	} else { alert('Error SP pBasculas - edicion!'+JSON.stringify(ret)); }}, false);
}

function cargarIdiomas(){
	var contenido = "";
	var parametros = '{"sp":"pIdiomas","modo":"cargarIdiomas"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		$("#dvAppIdiomas .lin").remove();
		var js = JSON.parse(limpiarCadena(ret.JSCode));
		if(js.length>0){
			for(var i in js){
				contenido += '<div class="row lin chv"><img src="./Merlos/images/cerrarRojo.png" onclick="eliminarIdiomaReport(\''+js[i].idioma+'\',\''+js[i].tipo+'\',\''+js[i].report+'\')" /></div>'
							+'<div class="row lin">'+js[i].idioma+' - '+js[i].nombre+'</div>'
							+'<div class="row lin">'+js[i].tipo+'</div>'
							+'<div class="row lin">'+js[i].report+'</div>';
			}
			$("#dvAppIdiomasContent").append(contenido);
		}
	} else { alert('Error SP pBasculas - edicion!'+JSON.stringify(ret)); }}, false);
}

function idiomaNew(cerrar){	
	if(cerrar){
		$(".new").slideUp();
		$("#imgIdiomaNew").slideDown();
	}else{
		$("#imgIdiomaNew").hide();
		$(".new").slideDown();	
		$(".chv").slideDown().css("display","flex").css("justify-content","center").css("align-self","center");
	}
}

function cargarObjeto(elId,objeto,contenedor,dvAPP){
	$("#"+elId).val("cargando...").css("color","red");
	var contenido = "";
	var parametros = '{"sp":"pObjetoDatos","objeto":"'+objeto+'"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		var js = JSON.parse(limpiarCadena(ret.JSCode));
		if(js.length>0){
			for(var i in js){ contenido += '<div class="dvSelSub" onclick="objetoSeleccion(\''+elId+'\',\''+js[i].Codigo+' - '+js[i].Nombre+'\')">'+js[i].Codigo+' - '+js[i].Nombre+'</div>'; }
			$("#"+dvAPP).css("overflow","visible");
			$("#"+contenedor).html(contenido).slideDown();			
		}
		$("#"+elId).val("").css("color","#333");		
	} else { alert('Error SP pObjetoDatos - objeto: '+objeto+'!\n'+JSON.stringify(ret)); }}, false);
}

function objetoSeleccion(id,valor){ $("#"+id).val(valor); $(document).click(); }

function idiomaNewGuardar(){
	if($.trim($("#inpIdioma").val()).indexOf(" - ")===-1){ abrirVelo("El formato debe ser xx - xxxxxxxxxx !"+btnAceptarCerrarVelo); return; } 
	if($.trim($("#inpTipo").val()).indexOf(" - ")===-1){ abrirVelo("El formato debe ser xx - xxxxxxxxxx !"+btnAceptarCerrarVelo); return; } 
	var idioma = $.trim(($("#inpIdioma").val()).split(" - ")[0]);
	var nombre = $.trim(($("#inpIdioma").val()).split(" - ")[1]);
	var tipo = $.trim(($("#inpTipo").val()).split(" - ")[0]);
	var tipoNombre = $.trim(($("#inpTipo").val()).split(" - ")[1]);
	var report = $.trim($("#inpReport").val());

	if(nombre===""){nombre=idioma;}

	if(idioma==="" || tipo==="" || report===""){ abrirVelo("Debes seleccionar todos los campos!"+btnAceptarCerrarVelo); return; }

	var parametros = '{"sp":"pIdiomas","modo":"actualizar","idioma":"'+idioma+'","nombre":"'+nombre+'","tipo":"'+tipo+'","tipoNombre":"'+tipoNombre+'","report":"'+report+'"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		cargarIdiomas();	
		$("#inpIdioma, #inpTipo, #inpReport").val("");
		idiomaNew(1);
	} else { alert('Error SP pIdiomas - actualizar!\n'+JSON.stringify(ret)); }}, false);
}

function eliminarIdiomaReport(idioma,tipo,report,eliminar){
	if(eliminar){
		abrirVelo(icoCarga16+" eliminando el idioma...");
		var parametros = '{"sp":"pIdiomas","modo":"eliminar","idioma":"'+idioma+'","tipo":"'+tipo+'","report":"'+report+'"}';
		flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
			cargarIdiomas();	
			$("#inpIdioma, #inpTipo, #inpReport").val("");
			idiomaNew(1);
			cerrarVelo();
		} else { alert('Error SP pIdiomas - actualizar!\n'+JSON.stringify(ret)); }}, false);
	}
	else{ 
		abrirVelo(
			 "Confirma para eliminar PERMANENTEMENTE el registro!"
			 +"<br><br><br>"
			 +"<span class='btnMVerde' onclick='eliminarIdiomaReport(\""+idioma+"\",\""+tipo+"\",\""+report+"\",1)'>eliminar</span>"
			 +"&nbsp;&nbsp;&nbsp;"
			 +"<span class='btnMRojo' onclick='cerrarVelo();'>cancelar</span>"
		); 
	}
}

$(".inpUsuario").on("blur", function(){ guardarUsuarioDatos(); });
function cargarUsuarios(){ 
	var contenido = "";
	$("#dvUsuariosListado").html(icoCarga16+" cargando usuarios...").slideDown();
	var usuario = JSON.parse(flexygo.context.Merlos_Usuarios);
	for(var i in usuario){
		contenido += "<div class='dvSelSub' onclick='asignarUsuario(\""+usuario[i].UserName+"\")'>"+usuario[i].UserName+"</div>";
	}
	$("#dvUsuariosListado").html(contenido);
}

function asignarUsuario(usuario){
	$("#inpMerlosUsuario").val(usuario);
	$(document).click();
	$(".inpUsuario").val("");
	
	var parametros = '{"sp":"pMerlosUsuarios","modo":"dameConfiguracion","usuario":"'+usuario+'"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){ 
		var js = JSON.parse(limpiarCadena(ret.JSCode));
		if(js.length>0){
			$("#inpImpDocu").val(js[0].impDocu);
			$("#inpImpEti").val(js[0].impEti);
			$("#inpReportAlb").val(js[0].reportAlbaran);
			$("#inpReportFra").val(js[0].reportFactura);
			$("#inpReportEti").val(js[0].reportEtiquetas);
			$("#inpBasculaPredet").val(js[0].bascula);
		}
	} else { alert('Error SP pMerlosUsuarios - dameConfiguracion!\n'+JSON.stringify(ret)); }}, false);
}

function guardarUsuarioDatos(){
	var usuario = $("#inpMerlosUsuario").val().trim(); 
	var ImpDocu = $("#inpImpDocu").val().trim(); 
	var ImpEti = $("#inpImpEti").val().trim(); 
	var ReportAlb = $("#inpReportAlb").val().trim(); 
	var ReportFra = $("#inpReportFra").val().trim(); 
	var ReportEti = $("#inpReportEti").val().trim(); 
	var BasculaPredet = $("#inpBasculaPredet").val().trim(); 
	if(usuario==="" || (ImpDocu==="" && ImpEti===""&& ReportAlb===""&& ReportFra===""&& ReportEti===""&& BasculaPredet==="")){ return; }

	var parametros = '{"sp":"pMerlosUsuarios","modo":"guardarDatos","usuario":"'+usuario+'","ImpDocu":"'+ImpDocu+'","ImpEti":"'+ImpEti+'","ReportAlb":"'+ReportAlb+'","ReportFra":"'+ReportFra+'"'
					+',"ReportEti":"'+ReportEti+'","BasculaPredet":"'+BasculaPredet+'"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		
	} else { alert('Error SP pMerlosUsuarios - guardarDatos!\n'+JSON.stringify(ret)); }}, false);
}

function cargarPersonalizacion(){
	var parametros = '{"sp":"pConfiguracion","modo":"cargarPersonalizacion"}';
	flexygo.nav.execProcess('pMerlos','',null,null,[{'Key':'parametros','Value':limpiarCadena(parametros)}],'current',false,$(this),function(ret){if(ret){
		var js = JSON.parse(limpiarCadena(ret.JSCode));
		if(js.length>0){ $("#tdPersonalizacion").html(js.Empresa); }
		else{ $("#tdPersonalizacion").html("Estándar"); }
	} else { alert('Error SP pConfiguracion - cargarPersonalizacion!\n'+JSON.stringify(ret)); }}, false);
}
</script>