import { r as registerInstance, j as h } from './index-76f52202.js';
import './ionic-global-693c5dc1.js';
import { f as flxSync, u as util, C as ConftokenProvider, s as sql, p as storage } from './messages-e685043d.js';
import { j as jquery } from './jquery-5df58adb.js';
import './utils-67a6e57b.js';
import './index-023098c3.js';
import './helpers-d94a0dba.js';
import './animation-625503e5.js';
import './index-20a23da0.js';
import './ios.transition-267ba16c.js';
import './md.transition-15ebc2b8.js';
import './cubic-bezier-92995175.js';
import './index-1da44cf3.js';
import './index-53f14fc6.js';
import './hardware-back-button-c2d005b0.js';
import './index-725f2a8a.js';
import './overlays-39d86a31.js';
import { n as nav } from './navigation-2677ae93.js';
import { p as parser } from './parser-186608ed.js';

const flxHomeCss = "#menuIonTitle{position:absolute;left:0;right:0;margin-left:auto;margin-right:auto;top:0;height:100%}";

const FlxHome = class {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    componentDidLoad() {
        jquery('#loadingSpinnerModule').css('visibility', 'hidden');
        flxSync.checkSendErrors();
    }
    async componentWillLoad() {
        if (!await this.didUserSync())
            nav.goSync();
        jquery('#loadingSpinnerModule').css('visibility', 'visible');
        this.title = '';
        this.body = '';
        this.header = '';
        this.footer = '';
        this.loadData().then(() => {
            if (this.page && this.page.JSAfterLoad) {
                util.execDynamicCode(this.page.JSAfterLoad);
            }
        });
        jquery(window).off('popstate.home').on('popstate.home', () => {
            if (document.location.href.toLowerCase().indexOf('/home') > 0) {
                this.loadData();
            }
        });
    }
    async refresh(ev) {
        return this.loadData().then(() => {
            if (ev) {
                ev.target.complete();
            }
            if (this.page && this.page.JSAfterLoad) {
                util.execDynamicCode(this.page.JSAfterLoad);
            }
        });
    }
    loadData() {
        return ConftokenProvider.config().then(async (cnf) => {
            this.title = '';
            this.body = '';
            this.header = '';
            this.footer = '';
            if (cnf && cnf.homePage) {
                this.page = cnf.homePage;
                if (this.page.SQLSentence) {
                    let sentence = this.page.SQLSentence;
                    sentence = sql.addOrderBy(sentence, this.page.SQLOrderBy);
                    sql.getTable(sentence).then(async (table) => {
                        if (table && table.rows && table.rows.length > 0) {
                            this.title = await parser.recursiveCompile(sql.getRow(table, 0), this.page.title, cnf, this);
                        }
                        this.body = await this.getRows(this.page, table, null, this, cnf);
                    });
                }
                else {
                    this.title = await parser.recursiveCompile(null, this.page.title, cnf, this);
                    if (this.page.header) {
                        this.header = await parser.recursiveCompile(null, this.page.header, cnf, this);
                    }
                    if (this.page.body) {
                        this.body = await parser.recursiveCompile(null, this.page.body, cnf, this);
                    }
                    if (this.page.footer) {
                        this.footer = await parser.recursiveCompile(null, this.page.footer, cnf, this);
                    }
                }
            }
        });
    }
    async getRows(page, table, defaults, ctx, cnf) {
        let rendered = '';
        if (table && table.rows && table.rows.length > 0) {
            if (page.header && page.header != '') {
                this.header = await parser.recursiveCompile(sql.getRow(table, 0), page.header, cnf, ctx);
            }
            if (page.body && page.body != '') {
                for (let i = 0; i < table.rows.length; i++) {
                    rendered += await parser.recursiveCompile(sql.getRow(table, i), page.body, cnf, ctx);
                }
            }
            if (page.footer && page.footer != '') {
                this.footer = await parser.recursiveCompile(sql.getRow(table, 0), page.footer, cnf, this);
            }
        }
        else {
            if (page.empty && page.empty != '') {
                rendered = await parser.recursiveCompile(defaults, page.empty, cnf, this);
            }
            else {
                rendered = 'No data found';
            }
        }
        return rendered;
    }
    async didUserSync() {
        const token = await storage.get('confToken');
        if (token && token.lastSync)
            return true;
        return false;
    }
    render() {
        return [
            h("ion-header", null, h("ion-toolbar", { color: "header", class: "ion-text-center" }, h("ion-buttons", { slot: "start" }, h("ion-menu-button", { color: "outstanding" }), h("ion-icon", { name: "alert-circle", color: "danger", class: "stack sendError hide" })), h("ion-title", { id: "menuIonTitle" }, h("span", { id: "menuTitle" }, this.title)))),
            h("ion-header", { innerHTML: this.header }),
            h("ion-content", null, h("ion-refresher", { slot: "fixed", id: "refresher", onIonRefresh: (ev) => { this.refresh(ev); } }, h("ion-refresher-content", null)), h("div", { id: "mainBody", innerHTML: this.body })),
            h("ion-footer", { innerHTML: this.footer })
        ];
    }
};
FlxHome.style = flxHomeCss;

export { FlxHome as flx_home };
