﻿GO

BEGIN TRAN

EXEC sp_MSforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT all'

EXEC sp_MSforeachtable 'ALTER TABLE ? DISABLE TRIGGER all'
 
--Application data



BEGIN TRY

MERGE INTO [AspNetUsers] AS Target
USING (VALUES
  (N'1',N'admins',N'default',N'e.jene@merlos.net',1,N'ALBaGB8NK2kVq0eotHJ7v0G9KbYVR8WfRmipGjFUAOXD9jw4nUsStY+yn/eee/FSeQ==',N'f44e0667-8c6a-4c6d-9456-910d2df7e59f',NULL,0,0,NULL,1,0,N'admin',NULL,N'0',N'0',N'~/img/Avatars/avatar_blank.png',N'Admin',N'Admin',N'Admin',N'es-ES',NULL,1)
 ,(N'c699993c-c159-48a6-90c3-59fad0e4b72b',N'users',N'default',N'test@merlos.net',1,N'ALBaGB8NK2kVq0eotHJ7v0G9KbYVR8WfRmipGjFUAOXD9jw4nUsStY+yn/eee/FSeQ==',N'f44e0667-8c6a-4c6d-9456-910d2df7e59f',N'',0,0,NULL,1,0,N'test',NULL,N'',N'',N'~/img/Avatars/avatar_blank.png',N'Test',N'Merlos Infor',N'Test',N'es-es',NULL,1)
) AS Source ([Id],[RoleId],[ProfileName],[Email],[EmailConfirmed],[PasswordHash],[SecurityStamp],[PhoneNumber],[PhoneNumberConfirmed],[TwoFactorEnabled],[LockoutEndDateUtc],[LockoutEnabled],[AccessFailedCount],[UserName],[IPGroup],[Reference],[SubReference],[Avatar],[Name],[SurName],[NickName],[CultureId],[MailAccountId],[OriginId])
ON (Target.[Id] = Source.[Id])
WHEN MATCHED AND (
	NULLIF(Source.[RoleId], Target.[RoleId]) IS NOT NULL OR NULLIF(Target.[RoleId], Source.[RoleId]) IS NOT NULL OR 
	NULLIF(Source.[ProfileName], Target.[ProfileName]) IS NOT NULL OR NULLIF(Target.[ProfileName], Source.[ProfileName]) IS NOT NULL OR 
	NULLIF(Source.[Email], Target.[Email]) IS NOT NULL OR NULLIF(Target.[Email], Source.[Email]) IS NOT NULL OR 
	NULLIF(Source.[EmailConfirmed], Target.[EmailConfirmed]) IS NOT NULL OR NULLIF(Target.[EmailConfirmed], Source.[EmailConfirmed]) IS NOT NULL OR 
	NULLIF(Source.[PasswordHash], Target.[PasswordHash]) IS NOT NULL OR NULLIF(Target.[PasswordHash], Source.[PasswordHash]) IS NOT NULL OR 
	NULLIF(Source.[SecurityStamp], Target.[SecurityStamp]) IS NOT NULL OR NULLIF(Target.[SecurityStamp], Source.[SecurityStamp]) IS NOT NULL OR 
	NULLIF(Source.[PhoneNumber], Target.[PhoneNumber]) IS NOT NULL OR NULLIF(Target.[PhoneNumber], Source.[PhoneNumber]) IS NOT NULL OR 
	NULLIF(Source.[PhoneNumberConfirmed], Target.[PhoneNumberConfirmed]) IS NOT NULL OR NULLIF(Target.[PhoneNumberConfirmed], Source.[PhoneNumberConfirmed]) IS NOT NULL OR 
	NULLIF(Source.[TwoFactorEnabled], Target.[TwoFactorEnabled]) IS NOT NULL OR NULLIF(Target.[TwoFactorEnabled], Source.[TwoFactorEnabled]) IS NOT NULL OR 
	NULLIF(Source.[LockoutEndDateUtc], Target.[LockoutEndDateUtc]) IS NOT NULL OR NULLIF(Target.[LockoutEndDateUtc], Source.[LockoutEndDateUtc]) IS NOT NULL OR 
	NULLIF(Source.[LockoutEnabled], Target.[LockoutEnabled]) IS NOT NULL OR NULLIF(Target.[LockoutEnabled], Source.[LockoutEnabled]) IS NOT NULL OR 
	NULLIF(Source.[AccessFailedCount], Target.[AccessFailedCount]) IS NOT NULL OR NULLIF(Target.[AccessFailedCount], Source.[AccessFailedCount]) IS NOT NULL OR 
	NULLIF(Source.[UserName], Target.[UserName]) IS NOT NULL OR NULLIF(Target.[UserName], Source.[UserName]) IS NOT NULL OR 
	NULLIF(Source.[IPGroup], Target.[IPGroup]) IS NOT NULL OR NULLIF(Target.[IPGroup], Source.[IPGroup]) IS NOT NULL OR 
	NULLIF(Source.[Reference], Target.[Reference]) IS NOT NULL OR NULLIF(Target.[Reference], Source.[Reference]) IS NOT NULL OR 
	NULLIF(Source.[SubReference], Target.[SubReference]) IS NOT NULL OR NULLIF(Target.[SubReference], Source.[SubReference]) IS NOT NULL OR 
	NULLIF(Source.[Avatar], Target.[Avatar]) IS NOT NULL OR NULLIF(Target.[Avatar], Source.[Avatar]) IS NOT NULL OR 
	NULLIF(Source.[Name], Target.[Name]) IS NOT NULL OR NULLIF(Target.[Name], Source.[Name]) IS NOT NULL OR 
	NULLIF(Source.[SurName], Target.[SurName]) IS NOT NULL OR NULLIF(Target.[SurName], Source.[SurName]) IS NOT NULL OR 
	NULLIF(Source.[NickName], Target.[NickName]) IS NOT NULL OR NULLIF(Target.[NickName], Source.[NickName]) IS NOT NULL OR 
	NULLIF(Source.[CultureId], Target.[CultureId]) IS NOT NULL OR NULLIF(Target.[CultureId], Source.[CultureId]) IS NOT NULL OR 
	NULLIF(Source.[MailAccountId], Target.[MailAccountId]) IS NOT NULL OR NULLIF(Target.[MailAccountId], Source.[MailAccountId]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [RoleId] = Source.[RoleId], 
  [ProfileName] = Source.[ProfileName], 
  [Email] = Source.[Email], 
  [EmailConfirmed] = Source.[EmailConfirmed], 
  [PasswordHash] = Source.[PasswordHash], 
  [SecurityStamp] = Source.[SecurityStamp], 
  [PhoneNumber] = Source.[PhoneNumber], 
  [PhoneNumberConfirmed] = Source.[PhoneNumberConfirmed], 
  [TwoFactorEnabled] = Source.[TwoFactorEnabled], 
  [LockoutEndDateUtc] = Source.[LockoutEndDateUtc], 
  [LockoutEnabled] = Source.[LockoutEnabled], 
  [AccessFailedCount] = Source.[AccessFailedCount], 
  [UserName] = Source.[UserName], 
  [IPGroup] = Source.[IPGroup], 
  [Reference] = Source.[Reference], 
  [SubReference] = Source.[SubReference], 
  [Avatar] = Source.[Avatar], 
  [Name] = Source.[Name], 
  [SurName] = Source.[SurName], 
  [NickName] = Source.[NickName], 
  [CultureId] = Source.[CultureId], 
  [MailAccountId] = Source.[MailAccountId], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[RoleId],[ProfileName],[Email],[EmailConfirmed],[PasswordHash],[SecurityStamp],[PhoneNumber],[PhoneNumberConfirmed],[TwoFactorEnabled],[LockoutEndDateUtc],[LockoutEnabled],[AccessFailedCount],[UserName],[IPGroup],[Reference],[SubReference],[Avatar],[Name],[SurName],[NickName],[CultureId],[MailAccountId],[OriginId])
 VALUES(Source.[Id],Source.[RoleId],Source.[ProfileName],Source.[Email],Source.[EmailConfirmed],Source.[PasswordHash],Source.[SecurityStamp],Source.[PhoneNumber],Source.[PhoneNumberConfirmed],Source.[TwoFactorEnabled],Source.[LockoutEndDateUtc],Source.[LockoutEnabled],Source.[AccessFailedCount],Source.[UserName],Source.[IPGroup],Source.[Reference],Source.[SubReference],Source.[Avatar],Source.[Name],Source.[SurName],Source.[NickName],Source.[CultureId],Source.[MailAccountId],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO








BEGIN TRY

MERGE INTO [ContextVars] AS Target
USING (VALUES
  (N'Merlos_Usuarios',N'select RoleId, Email, UserName, Reference, Name, SurName from AspNetUsers for JSON AUTO',0,N'ConfConnectionString',1)
 ,(N'usuariosTV',N'select RoleId, Email, UserName, Reference, Name, SurName from AspNetUsers for JSON AUTO',0,N'ConfConnectionString',1)
) AS Source ([VarName],[VarSQL],[Order],[ConnStringId],[OriginId])
ON (Target.[VarName] = Source.[VarName])
WHEN MATCHED AND (
	NULLIF(Source.[VarSQL], Target.[VarSQL]) IS NOT NULL OR NULLIF(Target.[VarSQL], Source.[VarSQL]) IS NOT NULL OR 
	NULLIF(Source.[Order], Target.[Order]) IS NOT NULL OR NULLIF(Target.[Order], Source.[Order]) IS NOT NULL OR 
	NULLIF(Source.[ConnStringId], Target.[ConnStringId]) IS NOT NULL OR NULLIF(Target.[ConnStringId], Source.[ConnStringId]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [VarSQL] = Source.[VarSQL], 
  [Order] = Source.[Order], 
  [ConnStringId] = Source.[ConnStringId], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([VarName],[VarSQL],[Order],[ConnStringId],[OriginId])
 VALUES(Source.[VarName],Source.[VarSQL],Source.[Order],Source.[ConnStringId],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO










BEGIN TRY

MERGE INTO [Modules] AS Target
USING (VALUES
  (N'Merlos_Administrador',N'flx-html',N'project',NULL,NULL,N'Merlos_Administrador',N'Merlos_Administrador',N'none',1,1,1,0,NULL,NULL,N'
<div class="dvTit">Configuración de la Aplicación</div>

<div id="dvComunesYEmpresa">
	<div class="tit1">Comunes y Empresa</div>
	<table id="tbComunEmpresa" class="tbFrm">
		<tr>
			<td>
				BD COMUN
				<br><select id="selComunes" class="esq05" onchange="cargarEmpresas()"></select>
			</td>
			<td>
				Empresa
				<br><select id="selEmpresas" class="esq05"></select>
			</td>
			<td class="btnMVerde esq05" style="text-align:center;" onclick="configurarEmpresa()">Configurar</td>
		</tr>
	</table>
</div>

<div id="dvAppPersonalizaciones" class="fondo">
	<div class="tit1"><img id="imgPersonalizaciones" class="imgMenu" /> Personalizaciones a Clientes</div>
	<div id="dvAppPersonalizacionesContent" class="seccion">
		<div class="row">
			<span id="spPersDisteco" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Disteco</span>
			<div id="dvPersDisteco" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spPersTelsan" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Telsan</span>
			<div id="dvPersTelsan" class="dvSubSel"></div>
		</div>
	</div>
</div>

<div id="dvAppOpciones" class="fondo">
	<div class="tit1"><img id="imgOpciones" class="imgMenu" /> Opciones de la Aplicación</div>
	<div id="dvAppOpcionesContent" class="seccion">
		<div class="row">
			<span id="spSeriesFiltro" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Series - Mostrar todas</span>
			<div id="dvSeriesFiltro" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spImpAlbGen" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Imprimir Albaran al generarlo</span>
			<div id="dvImpAlbGen" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spResLin" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Resaltar lineas Dto. 100%</span>
			<div id="dvResLin" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spFactCajas" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Facturación cajas</span>
			<div id="dvFactCajas" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spImpEtiquetas" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Imprimir etiquetas</span>
			<div id="dvImpEtiquetas" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spImpAlbFact" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Imprimir Albaranes y Facturas</span>
			<div id="dvImpAlbFact" class="dvSubSel"></div>
		</div>
	</div>
</div>

<div id="dvAppBasculas" class="fondo">
	<div class="tit1"><img id="imgBasculas" class="imgMenu" /> Básculas</div>
	<div id="dvAppBasculasContent" class="seccion">
		<div class="row cab">Báscula</div>
		<div class="row cab">IP</div>
		<div class="row cab">Puerto</div>
		<div class="row cab">Sentencia</div>

		<div class="row">
			<input type="text" id="inpBasculaIPActiva01" class="inv" disabled="">
			<img id="imgBascula01" src="./Merlos/images/btnBascula01_Off.png">
		</div>
		<div class="row"><input type="text" id="inpBasculaIP01" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPPuerto01" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPSentencia01" disabled /></div>

		<div class="row">
			<input type="text" id="inpBasculaIPActiva02" class="inv" disabled="">
			<img id="imgBascula02" src="./Merlos/images/btnBascula02_Off.png">
		</div>
		<div class="row"><input type="text" id="inpBasculaIP02" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPPuerto02" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPSentencia02" disabled /></div>

		<div class="row">
			<input type="text" id="inpBasculaIPActiva03" class="inv" disabled />
			<img id="imgBascula03" src="./Merlos/images/btnBascula03_Off.png">
		</div>
		<div class="row"><input type="text" id="inpBasculaIP03" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPPuerto03" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPSentencia03" disabled /></div>

		<div class="row">
			<input type="text" id="inpBasculaIPActiva04" class="inv" disabled />
			<img id="imgBascula04" src="./Merlos/images/btnBascula04_Off.png">
		</div>
		<div class="row"><input type="text" id="inpBasculaIP04" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPPuerto04" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPSentencia04" disabled /></div>
	</div>
</div>

<div id="dvAppIdiomas" class="fondo">
	<div class="tit1"><img id="imgIdiomas" class="imgMenu" /> Idiomas</div>
	<div id="dvAppIdiomasContent" class="seccion">
		<div class="row cab chv"><img id="imgIdiomaNew" src="./Merlos/images/anadirLinea.png" onclick="idiomaNew()" /></div>
		<div class="row cab">Idioma <span style="font:12px arial; color:#666;">cód - nombre</span></div>
		<div class="row cab">Tipo <span style="font:12px arial; color:#666;">cód - nombre</span></div>
		<div class="row cab">Report <span style="font:12px arial; color:#666;">nombre</span></div>

		<div class="row new chv">
			<img src="./Merlos/images/floppy.png" onclick="idiomaNewGuardar()" />
			&nbsp;&nbsp;&nbsp;&nbsp;
			<img src="./Merlos/images/cerrar.png" onclick="idiomaNew(1)" />
		</div>
		<div class="row new">
			<input type="text" id="inpIdioma" onclick="cargarObjeto(this.id,''Idiomas'',''inpIdiomaCont'',''dvAppIdiomas''); event.stopPropagation();" />
			<div id="inpIdiomaCont" class="dvSel"></div>
		</div>
		<div class="row new">
			<input type="text" id="inpTipo" onclick="cargarObjeto(this.id,''Tipos'',''inpTipoCont'',''dvAppIdiomas''); event.stopPropagation();" />
			<div id="inpTipoCont" class="dvSel"></div>
		</div>
		<div class="row new">
			<input type="text" id="inpReport" />
		</div>
	</div>
</div>

<div id="dvAppUsuarios" class="fondo">
	<div class="tit1"><img id="imgUsuarios" class="imgMenu" /> Usuarios</div>
	<div id="dvAppUsuariosContent" class="seccion">
		<div class="row cab">Usuario</div>
		<div class="row cab">Impresora Documentos</div>
		<div class="row cab">Impresora Etiquetas</div>
		<div class="row cab">Report de Albarán</div>
		<div class="row cab">Report de Factura</div>
		<div class="row cab">Report de Etiquetas</div>
		<div class="row cab">Báscula predeterminada</div>

		<div class="row" style="position:relative;">
			<input type="text" id="inpMerlosUsuario" readonly onclick="cargarUsuarios(); event.stopPropagation();" />
			<div id="dvUsuariosListado" class="dvSel"></div>
		</div>
		<div class="row"><input type="text" id="inpImpDocu" class="inpUsuario" /></div>
		<div class="row"><input type="text" id="inpImpEti" class="inpUsuario" /></div>
		<div class="row"><input type="text" id="inpReportAlb" class="inpUsuario" /></div>
		<div class="row"><input type="text" id="inpReportFra" class="inpUsuario" /></div>
		<div class="row"><input type="text" id="inpReportEti" class="inpUsuario" /></div>
		<div class="row"><input type="text" id="inpBasculaPredet" class="inpUsuario" /></div>
	</div>
</div>




<script> /* #### script #### script #### script #### script #### script #### script #### script #### script #### script #### script #### script #### script #### script #### */
cerrarVelo();

' + convert(nvarchar(max),NCHAR(36)) + N'("#mainNav, .seccion").hide();
' + convert(nvarchar(max),NCHAR(36)) + N'("#dvComunesYEmpresa").show();

var Configuracion = "";				
var bascula01Activa = "0", bascula02Activa = "0", bascula03Activa = "0", bascula04Activa = "0";
var comunesCargados = false;
var empresasCargadas = false;
var seriesCargadas = false;
var lasSeries = "";
var seriesImpAlbGenCargadas = false;
var lasSeriesImpAlbGen = "";
var seriesResLinCargadas = false;
var lasSeriesResLin = "";
var seriesFactCajasCargadas = false;
var lasSeriesFactCajas = "";
var seriesSeriesFiltroCargadas = false;
var lasSeriesSeriesFiltro = "";
var confOpIO = 0;
var basculaTipoConexion = "Ethernet";
var personalizaciones = ["Disteco","Telsan"];

var checkO = "./Merlos/images/check_O.png";
var checkI = "./Merlos/images/check_I.png";

cargarComunes();
comprobarConfiguracion();

' + convert(nvarchar(max),NCHAR(36)) + N'(".confOp").on("click", function () { confOp_Click(this.id); event.stopPropagation(); });
' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppBasculas img").on("click",function(){ botonBascula(this.id); });
' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppBasculas input").on("blur",function(){ basculasConfiguracion();});

' + convert(nvarchar(max),NCHAR(36)) + N'(".imgMenu").off().on("click", function () { 
	var id = this.id; console.log(id);
	var a = ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).parent().parent().attr("id")+"Content";
	if(' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).hasClass("a90g")){ 
		' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).attr("src","./Merlos/images/flechaG.png").removeClass("a90g").addClass("a0g"); 
		' + convert(nvarchar(max),NCHAR(36)) + N'("#"+a).slideUp();
	}else {		
		' + convert(nvarchar(max),NCHAR(36)) + N'(".imgMenu").attr("src","./Merlos/images/flechaG.png").removeClass("a90g").addClass("a0g"); 
		' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).attr("src","./Merlos/images/flechaVerde.png").removeClass("a0g").addClass("a90g");
		' + convert(nvarchar(max),NCHAR(36)) + N'(".seccion").hide();
		' + convert(nvarchar(max),NCHAR(36)) + N'("#"+a).slideDown();		
	}
});

function confOp_Click(id) {
	// Objetos
	var objeto = id.split("sp")[1];
	if (' + convert(nvarchar(max),NCHAR(36)) + N'("#" + id).find("img").attr("src") === checkO) {
		' + convert(nvarchar(max),NCHAR(36)) + N'("#" + id).find("img").attr("src", checkI);
		if (id === "spImpAlbGen") { cargarImpAlbGen(false); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppOpciones").css("overflow","visible"); }
		if (id === "spResLin") { cargarResLin(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppOpciones").css("overflow","visible"); }
		if (id === "spFactCajas") { cargarFactCajas(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppOpciones").css("overflow","visible"); }
		confOpIO = 1;
	} else {
		confOpIO = 0;
		' + convert(nvarchar(max),NCHAR(36)) + N'("#" + id).find("img").attr("src", checkO);
		if (id === "spSeriesFiltro") { cargarSeriesFiltro(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppOpciones").css("overflow","visible"); }
	}

	objetoIO(objeto, '''', confOpIO);

	// Personalizaciones
	personalizaciones.forEach(elemento => { if(id==="spPers"+elemento){ personalizacion(elemento); } });
}

function personalizacion(cliente){
	var parametros = ''{"sp":"pConfiguracion","modo":"personalizacion","cliente":"'' + cliente + ''","confOpIO":"'' + confOpIO +''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		
	} else { alert(''Error SP pConfiguracion - personalizacion - cliente!\n''+JSON.stringify(ret)); }}, false);
}

function objetoIO(objeto, valor, confOpIO) {
	var parametros = ''{"sp":"pConfiguracion","modo":"objetoIO","objeto":"'' + objeto + ''","valor":"'' + valor + ''","confOpIO":"'' + confOpIO +''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		
	} else { alert(''Error SP pConfiguracion - objetoIO - ''+objeto+''!\n''+JSON.stringify(ret)); }}, false);
}

function iniciar(){ 
	cargarSeries();
	cargarSeriesObjeto("ImpAlbGen");
	cargarSeriesObjeto("ResLin");
	cargarSeriesObjeto("FactCajas");
	cargarSeriesObjeto("SeriesFiltro");
	cargarConfiguracion();
	cargarIdiomas();
	
	' + convert(nvarchar(max),NCHAR(36)) + N'(".imgMenu").attr("src","./Merlos/images/flechaG.png").css("width","20px");
	' + convert(nvarchar(max),NCHAR(36)) + N'("#imgOpciones").attr("src","./Merlos/images/flechaVerde.png").addClass("a90g");
	' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppOpcionesContent").slideDown();
}

function cargarComunes() {
	' + convert(nvarchar(max),NCHAR(36)) + N'("#tbComunEmpresa").fadeIn();
	' + convert(nvarchar(max),NCHAR(36)) + N'("#selComunes").html("<option>cargando comunes...</option>").css("color", "red");
	var contenido = "<option></option>";

	var parametros = ''{"sp":"pComunes"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		var elJS = JSON.parse(ret.JSCode);
		if (elJS.length > 0) {
			for (var i in elJS) { contenido += "<option>" + elJS[i].nombre + "</option>"; }			
			' + convert(nvarchar(max),NCHAR(36)) + N'("#selComunes").html(contenido).css("color", "#333");
			comunesCargados = true;
		} 
	} else { alert(''Error SP pComunes!''+JSON.stringify(ret)); }}, false);
}

function cargarEmpresas() {
	' + convert(nvarchar(max),NCHAR(36)) + N'("#selEmpresas").html("<option>cargando empresas...</option>").css("color", "red");
	var contenido = "<option></option>";
	var comunes = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#selComunes").val());

	var parametros = ''{"sp":"pEmpresas","comun":"'' + comunes+''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		var elJS = JSON.parse(ret.JSCode);
		if (elJS.empresas.length > 0) {
			for (var i in elJS.empresas) { contenido += "<option value=''" + elJS.empresas[i].codigo + "''>" + elJS.empresas[i].codigo + " - " + elJS.empresas[i].nombre + "</option>"; }
			' + convert(nvarchar(max),NCHAR(36)) + N'("#selEmpresas").html(contenido).css("color", "#333");
			empresasCargadas = true;
		}
	} else { alert(''Error SP pEmpresas!\n''+JSON.stringify(ret)); }}, false);
}

function configurarEmpresa(confirmado) {
	if(confirmado){	
		var comun = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#selComunes").val());
		var empresa = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#selEmpresas").val());
		var nombreEmpresa = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'(''select[id="selEmpresas"] option:selected'').text());

		if (comun === "" || empresa === "") { alert("Debes seleccionar COMUNES y EMPRESA!"); return; }

		abrirVelo(icoCarga50 + "<br><br>configurando la empresa<br><br><span class=''tit1''>" + nombreEmpresa+"</span>");
		var parametros = ''{"sp":"pConfiguracion","modo":"configurarEmpresa","comun":"'' + comun + ''","empresa":"'' + empresa + ''","nombreEmpresa":"'' + nombreEmpresa +''"}'';
		flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
			if(ret.JSCode!=="OK"){ abrirVelo(icoAlert50+"<br><br>Ha ocurrido un error al configurar la empresa!!!<br><br>Contacta con el administrador."); }
			else{ crearVistasYProcedimientos(); }
		} else { alert(''Error SP pConfiguracion - configurarEmpresa!\n''+JSON.stringify(ret)); }}, false);
	}else{
		abrirVelo(`
			Confirma para configurar el portal.
			<br><br><br>
			<span class=''btnMVerde'' onclick=''configurarEmpresa(1)''>configurar</span>
			&nbsp;&nbsp;&nbsp;
			<span class=''btnMRojo'' onclick=''cerrarVelo();''>cancelar</span>
		`);
	}
}

function crearVistasYProcedimientos(){
	abrirVelo(icoCarga50 + "<br><br><span class=''tit1''>creando las vistas...</span>",null,1);
	var parametros = ''{"sp":"pConfiguracion","modo":"crearVistas"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		if(ret.JSCode.indexOf("Error!")!=-1){ abrirVelo(icoAlert50+"<br><br>Se ha producido un error al generar las vistas!!!<br><br>Contacta con el administrador."); return; }
		abrirVelo(icoCarga50 + "<br><br><span class=''tit1''>creando los procedimientos...</span>",null,1);
		parametros = ''{"sp":"pConfiguracion","modo":"crearProcedimientos"}'';
		flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
			if(ret.JSCode.indexOf("Error!")!=-1){ abrirVelo(icoAlert50+"<br><br>Se ha producido un error al generar los procedimientos!!!<br><br>Contacta con el administrador.",null,1); return; }
			abrirVelo(icoCarga50 + "<br><br><span class=''tit1''>creando las funciones...</span>",null,1);
			parametros = ''{"sp":"pConfiguracion","modo":"crearFunciones"}'';
			flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
				if(ret.JSCode.indexOf("Error!")!=-1){ abrirVelo(icoAlert50+"<br><br>Se ha producido un error al generar las funciones!!!<br><br>Contacta con el administrador.",null,1); return; }
					abrirVelo(icoCarga50 + "<br><br><span class=''tit1''>creando las personalizaciones...</span>",null,1);
					parametros = ''{"sp":"pConfiguracion","modo":"crearPersonalizaciones"}'';
					flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
						if(ret.JSCode.indexOf("Error!")!=-1){ abrirVelo(icoAlert50+"<br><br>Se ha producido un error al generar las personalizaciones!!!<br><br>Contacta con el administrador.",null,1); return; }
						iniciar();
					} else { alert(''Error SP pConfiguracion - crearFunciones!\n''+JSON.stringify(ret)); }}, false);
			} else { alert(''Error SP pConfiguracion - crearFunciones!\n''+JSON.stringify(ret)); }}, false);
		} else { alert(''Error SP pConfiguracion - crearProcedimientos!\n''+JSON.stringify(ret)); }}, false);
	} else { alert(''Error SP pConfiguracion - crearVistas!\n''+JSON.stringify(ret)); }}, false);
}

function esperaYcargaComunEmpresa(comun, empresa) { 
	if (!comunesCargados) { setTimeout(function () { esperaYcargaComunEmpresa(comun, empresa); }, 250); }
	else {
		' + convert(nvarchar(max),NCHAR(36)) + N'("#selComunes").val(comun);
		if (!empresasCargadas) { setTimeout(function () { cargarEmpresas(); esperaYcargaComunEmpresa(comun, empresa); }, 500); }
		else { setTimeout(function () { ' + convert(nvarchar(max),NCHAR(36)) + N'("#selEmpresas").val(empresa); }, 500);}
	}
}

function comprobarConfiguracion() {
	var parametros = ''{"sp":"pConfiguracion","modo":"comprobarConfiguracionEmpresa"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		var js = JSON.parse(limpiarCadena(ret.JSCode));
		if(js[0].msj=="NoExisteConfiguracion"){ cerrarVelo(); }
		else{ esperaYcargaComunEmpresa(js[0].Comun,js[0].Empresa); iniciar(); }
	} else { alert(''Error SP pConfiguracion - comprobarConfiguracion!\n''+JSON.stringify(ret)); }}, false);
}

function cargarSeries() {
	var parametros = ''{"sp":"pSeries"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		lasSeries = ret.JSCode;
		seriesCargadas = true;
	} else { alert(''Error SP pSeries!\n''+JSON.stringify(ret)); }}, false);
}

function cargarSeriesObjeto(objeto) {
	var parametros = ''{"sp":"pSeries","modo":"cargarSeriesObjeto","objeto":"''+objeto+''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		if (objeto === "ImpAlbGen") { lasSeriesImpAlbGen = ret.JSCode; seriesImpAlbGenCargadas = true; }
		if (objeto === "ResLin") { lasSeriesResLin = ret.JSCode; seriesResLinCargadas = true; }
		if (objeto === "FactCajas") { lasSeriesFactCajas = ret.JSCode; seriesFactCajasCargadas = true; }
		if (objeto === "SeriesFiltro") { lasSeriesSeriesFiltro = ret.JSCode; seriesSeriesFiltroCargadas = true; }
	} else { alert(''Error SP pSeries - cargarSeriesObjeto!\n''+JSON.stringify(ret)); }}, false);
}

function cargarSeriesFiltro(tf) {
	if (!tf) { ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvSeriesFiltro").html(icoCarga16 + " cargando series...").slideDown(); }
	var contenido = "";
	if (seriesCargadas && seriesSeriesFiltroCargadas) {
		var series = JSON.parse(lasSeries);
		if(series.length>0){
			for (var i in series) {
				var elColor = " background:#666; ";
				if (lasSeriesSeriesFiltro.indexOf(series[i].codigo) !== -1) { elColor = " background:green;"; }
				contenido += "<div class=''dvConfSerieSeriesFiltro C'' style=''margin:2px; padding:5px; color:#FFF; " + elColor + "'' "
					+ "onclick=''asignarSerieConf(\"SeriesFiltro\",\"" + series[i].codigo + "\");''>"
					+ series[i].codigo
					+ "</div>";
			}
		}else{ ' + convert(nvarchar(max),NCHAR(36)) + N'(document).click(); abrirVelo("No se han obtenido series para filtrar!"+btnAceptarCerrarVelo); }
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvSeriesFiltro").html(contenido);
	} else { setTimeout(function () { cargarSeries(); cargarSeriesObjeto("SeriesFiltro"); cargarSeriesFiltro(true); }, 100); }
}

function cargarImpAlbGen(tf){
	if(!tf){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvImpAlbGen").html(icoCarga16 + " cargando series...").slideDown(); }
	var contenido = "";
	if(seriesCargadas && seriesImpAlbGenCargadas){
		var series = JSON.parse(lasSeries);
		if(series.length>0){
		for(var i in series){ 
			var elColor = " background:#666; ";
			if(lasSeriesImpAlbGen.indexOf(series[i].codigo)!==-1){ elColor = " background:green;";}
			contenido += "<div class=''dvConfSerieImpAlbGen C'' style=''margin:2px; padding:5px; color:#FFF; "+elColor+"'' "
					  +  "onclick=''asignarSerieConf(\"ImpAlbGen\",\""+series[i].codigo+"\");''>"
					  +  series[i].codigo
					  +  "</div>"; 
			}
		}
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvImpAlbGen").html(contenido);
	}else{ setTimeout(function(){cargarImpAlbGen(tf);},100); }
}

function cargarResLin(tf){
	if(!tf){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvResLin").html(icoCarga16 + " cargando series...").slideDown(); }
	var contenido = "";
	if(seriesCargadas && seriesResLinCargadas){
		var series = JSON.parse(lasSeries);
		if(series.length>0){
		for(var i in series){ 
			var elColor = " background:#666; ";
			if(lasSeriesResLin.indexOf(series[i].codigo)!==-1){ elColor = " background:green;";}
			contenido += "<div class=''dvConfSerieResLin C'' style=''margin:2px; padding:5px; color:#FFF; "+elColor+"'' "
					  +  "onclick=''asignarSerieConf(\"ResLin\",\""+series[i].codigo+"\");''>"
					  +  series[i].codigo
					  +  "</div>"; 
			}
		}
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvResLin").html(contenido);
	}else{ setTimeout(cargarResLin,100); }
}

function cargarFactCajas(tf){
	if(!tf){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvFactCajas").html(icoCarga16 + " cargando series...").slideDown(); }
	var contenido = "";
	if(seriesCargadas && seriesFactCajasCargadas){
		var series = JSON.parse(lasSeries);
		if(series.length>0){
		for(var i in series){ 
			var elColor = " background:#666; ";
			if(lasSeriesFactCajas.indexOf(series[i].codigo)!==-1){ elColor = " background:green;";}
			contenido += "<div class=''dvConfSerieFactCajas C'' style=''margin:2px; padding:5px; color:#FFF; "+elColor+"'' "
					  +  "onclick=''asignarSerieConf(\"FactCajas\",\""+series[i].codigo+"\");''>"
					  +  series[i].codigo
					  +  "</div>"; 
			}
		}
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvFactCajas").html(contenido);
	}else{ setTimeout(cargarFactCajas,100); }
}

function asignarSerieConf(objeto,serie){ 
	' + convert(nvarchar(max),NCHAR(36)) + N'(''.dvConfSerie''+objeto+'':contains("''+serie+''")'').css("background","orange").html(icoCarga16);
	var parametros = ''{"sp":"pSeries","modo":"asignarSerieConf","objeto":"''+objeto+''","serie":"''+serie+''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		if(objeto==="ImpAlbGen"){ seriesImpAlbGenCargadas=false; cargarSeriesObjeto(objeto); cargarImpAlbGen(true); }
		if(objeto==="ResLin"){ seriesResLinCargadas=false; cargarSeriesObjeto(objeto); cargarResLin(true); }
		if(objeto==="FactCajas"){ seriesFactCajasCargadas=false; cargarSeriesObjeto(objeto); cargarFactCajas(true); }		
		if(objeto==="SeriesFiltro"){ seriesSeriesFiltroCargadas=false; cargarSeriesObjeto(objeto); cargarSeriesFiltro(true); }
    } else { alert(''Error SP pSeries - asignarSerieConf!''+JSON.stringify(ret)); }}, false);
}

function botonBascula(id, noEnviar){
	var elId = id.split("imgBascula")[1];
	if(' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).attr("src")==="./Merlos/images/btnBascula"+elId+"_Off.png"){
		' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).attr("src","./Merlos/images/btnBascula"+elId+"_Activa.png");
		' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIP"+elId+",#inpBasculaIPPuerto"+elId+",#inpBasculaIPSentencia"+elId).prop("disabled",false).css("color","#333");
		window["bascula"+elId+"Activa"] = 1;
	}else{
		' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).attr("src","./Merlos/images/btnBascula"+elId+"_Off.png");
		' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIP"+elId+",#inpBasculaIPPuerto"+elId+",#inpBasculaIPSentencia"+elId).prop("disabled",true).css("color","#999");
		window["bascula"+elId+"Activa"] = 0;
	}
	if(!noEnviar){ basculasConfiguracion(); }	
}

function basculasConfiguracion(){
	var bascula1 = ''{''
				+''"bascula":"bascula01"''
				+'',"activa":"''+bascula01Activa+''"''
				+'',"basculaTipoConexion":"''+basculaTipoConexion+''"''
				+'',"IP":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIP01").val())+''"''
				+'',"puerto":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPPuerto01").val())+''"''
				+'',"sentencia":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPSentencia01").val())+''"''
				+''}'';
	var bascula2 = ''{''
				+''"bascula":"bascula02"''
				+'',"activa":"''+bascula02Activa+''"''
				+'',"basculaTipoConexion":"''+basculaTipoConexion+''"''
				+'',"IP":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIP02").val())+''"''
				+'',"puerto":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPPuerto02").val())+''"''
				+'',"sentencia":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPSentencia02").val())+''"''
				+''}'';
	var bascula3 = ''{''
				+''"bascula":"bascula03"''
				+'',"activa":"''+bascula03Activa+''"''
				+'',"basculaTipoConexion":"''+basculaTipoConexion+''"''
				+'',"IP":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIP03").val())+''"''
				+'',"puerto":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPPuerto03").val())+''"''
				+'',"sentencia":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPSentencia03").val())+''"''
				+''}'';
	var bascula4 = ''{''
				+''"bascula":"bascula04"''
				+'',"activa":"''+bascula04Activa+''"''
				+'',"basculaTipoConexion":"''+basculaTipoConexion+''"''
				+'',"IP":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIP04").val())+''"''
				+'',"puerto":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPPuerto04").val())+''"''
				+'',"sentencia":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPSentencia04").val())+''"''
				+''}'';

	var parametros = ''{"sp":"pBasculas","modo":"edicion","bascula1":[''+bascula1+''],"bascula2":[''+bascula2+''],"bascula3":[''+bascula3+''],"bascula4":[''+bascula4+'']}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){

	} else { alert(''Error SP pBasculas - edicion!''+JSON.stringify(ret)); }}, false);
}

function cargarConfiguracion(){
	abrirVelo(icoCarga50+"<br><br>cargando la configuración...");
	var parametros = ''{"sp":"pConfiguracion","modo":"cargarConfiguracion"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		var js = JSON.parse(limpiarCadena(ret.JSCode));
		// Configuración
		for(var i in js.configuracion){
			var conf = js.configuracion[i];
			if(conf.activo===1){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#sp" + conf.objeto).find("img").attr("src", checkI); }else{ ' + convert(nvarchar(max),NCHAR(36)) + N'("#sp" + conf.objeto).find("img").attr("src", checkO); }
		}
		// Básculas
		for(var i in js.basculas){
			var basc = js.basculas[i];
			if(basc.activa){ botonBascula("img" + capitalizar(basc.bascula),1); }
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIP"+basc.bascula.split("bascula")[1]).val(basc.ip);
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPPuerto"+basc.bascula.split("bascula")[1]).val(basc.puerto);
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPSentencia"+basc.bascula.split("bascula")[1]).val(basc.sentencia);
		}
		cerrarVelo();
	} else { alert(''Error SP pBasculas - edicion!''+JSON.stringify(ret)); }}, false);
}

function cargarIdiomas(){
	var contenido = "";
	var parametros = ''{"sp":"pIdiomas","modo":"cargarIdiomas"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppIdiomas .lin").remove();
		var js = JSON.parse(limpiarCadena(ret.JSCode));
		if(js.length>0){
			for(var i in js){
				contenido += ''<div class="row lin chv"><img src="./Merlos/images/cerrarRojo.png" onclick="eliminarIdiomaReport(\''''+js[i].idioma+''\'',\''''+js[i].tipo+''\'',\''''+js[i].report+''\'')" /></div>''
							+''<div class="row lin">''+js[i].idioma+'' - ''+js[i].nombre+''</div>''
							+''<div class="row lin">''+js[i].tipo+''</div>''
							+''<div class="row lin">''+js[i].report+''</div>'';
			}
			' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppIdiomasContent").append(contenido);
		}
	} else { alert(''Error SP pBasculas - edicion!''+JSON.stringify(ret)); }}, false);
}

function idiomaNew(cerrar){	
	if(cerrar){
		' + convert(nvarchar(max),NCHAR(36)) + N'(".new").slideUp();
		' + convert(nvarchar(max),NCHAR(36)) + N'("#imgIdiomaNew").slideDown();
	}else{
		' + convert(nvarchar(max),NCHAR(36)) + N'("#imgIdiomaNew").hide();
		' + convert(nvarchar(max),NCHAR(36)) + N'(".new").slideDown();	
		' + convert(nvarchar(max),NCHAR(36)) + N'(".chv").slideDown().css("display","flex").css("justify-content","center").css("align-self","center");
	}
}

function cargarObjeto(elId,objeto,contenedor,dvAPP){
	' + convert(nvarchar(max),NCHAR(36)) + N'("#"+elId).val("cargando...").css("color","red");
	var contenido = "";
	var parametros = ''{"sp":"pObjetoDatos","objeto":"''+objeto+''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		var js = JSON.parse(limpiarCadena(ret.JSCode));
		if(js.length>0){
			for(var i in js){ contenido += ''<div class="dvSelSub" onclick="objetoSeleccion(\''''+elId+''\'',\''''+js[i].Codigo+'' - ''+js[i].Nombre+''\'')">''+js[i].Codigo+'' - ''+js[i].Nombre+''</div>''; }
			' + convert(nvarchar(max),NCHAR(36)) + N'("#"+dvAPP).css("overflow","visible");
			' + convert(nvarchar(max),NCHAR(36)) + N'("#"+contenedor).html(contenido).slideDown();			
		}
		' + convert(nvarchar(max),NCHAR(36)) + N'("#"+elId).val("").css("color","#333");		
	} else { alert(''Error SP pObjetoDatos - objeto: ''+objeto+''!\n''+JSON.stringify(ret)); }}, false);
}

function objetoSeleccion(id,valor){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).val(valor); ' + convert(nvarchar(max),NCHAR(36)) + N'(document).click(); }

function idiomaNewGuardar(){
	if(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpIdioma").val()).indexOf(" - ")===-1){ abrirVelo("El formato debe ser xx - xxxxxxxxxx !"+btnAceptarCerrarVelo); return; } 
	if(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpTipo").val()).indexOf(" - ")===-1){ abrirVelo("El formato debe ser xx - xxxxxxxxxx !"+btnAceptarCerrarVelo); return; } 
	var idioma = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim((' + convert(nvarchar(max),NCHAR(36)) + N'("#inpIdioma").val()).split(" - ")[0]);
	var nombre = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim((' + convert(nvarchar(max),NCHAR(36)) + N'("#inpIdioma").val()).split(" - ")[1]);
	var tipo = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim((' + convert(nvarchar(max),NCHAR(36)) + N'("#inpTipo").val()).split(" - ")[0]);
	var tipoNombre = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim((' + convert(nvarchar(max),NCHAR(36)) + N'("#inpTipo").val()).split(" - ")[1]);
	var report = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpReport").val());

	if(nombre===""){nombre=idioma;}

	if(idioma==="" || tipo==="" || report===""){ abrirVelo("Debes seleccionar todos los campos!"+btnAceptarCerrarVelo); return; }

	var parametros = ''{"sp":"pIdiomas","modo":"actualizar","idioma":"''+idioma+''","nombre":"''+nombre+''","tipo":"''+tipo+''","tipoNombre":"''+tipoNombre+''","report":"''+report+''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		cargarIdiomas();	
		' + convert(nvarchar(max),NCHAR(36)) + N'("#inpIdioma, #inpTipo, #inpReport").val("");
		idiomaNew(1);
	} else { alert(''Error SP pIdiomas - actualizar!\n''+JSON.stringify(ret)); }}, false);
}

function eliminarIdiomaReport(idioma,tipo,report,eliminar){
	if(eliminar){
		abrirVelo(icoCarga16+" eliminando el idioma...");
		var parametros = ''{"sp":"pIdiomas","modo":"eliminar","idioma":"''+idioma+''","tipo":"''+tipo+''","report":"''+report+''"}'';
		flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
			cargarIdiomas();	
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpIdioma, #inpTipo, #inpReport").val("");
			idiomaNew(1);
			cerrarVelo();
		} else { alert(''Error SP pIdiomas - actualizar!\n''+JSON.stringify(ret)); }}, false);
	}
	else{ 
		abrirVelo(
			 "Confirma para eliminar PERMANENTEMENTE el registro!"
			 +"<br><br><br>"
			 +"<span class=''btnMVerde'' onclick=''eliminarIdiomaReport(\""+idioma+"\",\""+tipo+"\",\""+report+"\",1)''>eliminar</span>"
			 +"&nbsp;&nbsp;&nbsp;"
			 +"<span class=''btnMRojo'' onclick=''cerrarVelo();''>cancelar</span>"
		); 
	}
}

' + convert(nvarchar(max),NCHAR(36)) + N'(".inpUsuario").on("blur", function(){ guardarUsuarioDatos(); });
function cargarUsuarios(){ 
	var contenido = "";
	' + convert(nvarchar(max),NCHAR(36)) + N'("#dvUsuariosListado").html(icoCarga16+" cargando usuarios...").slideDown();
	var usuario = JSON.parse(flexygo.context.Merlos_Usuarios);
	for(var i in usuario){
		contenido += "<div class=''dvSelSub'' onclick=''asignarUsuario(\""+usuario[i].UserName+"\")''>"+usuario[i].UserName+"</div>";
	}
	' + convert(nvarchar(max),NCHAR(36)) + N'("#dvUsuariosListado").html(contenido);
}

function asignarUsuario(usuario){
	' + convert(nvarchar(max),NCHAR(36)) + N'("#inpMerlosUsuario").val(usuario);
	' + convert(nvarchar(max),NCHAR(36)) + N'(document).click();
	' + convert(nvarchar(max),NCHAR(36)) + N'(".inpUsuario").val("");
	
	var parametros = ''{"sp":"pMerlosUsuarios","modo":"dameConfiguracion","usuario":"''+usuario+''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){ 
		var js = JSON.parse(limpiarCadena(ret.JSCode));
		if(js.length>0){
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpImpDocu").val(js[0].impDocu);
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpImpEti").val(js[0].impEti);
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpReportAlb").val(js[0].reportAlbaran);
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpReportFra").val(js[0].reportFactura);
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpReportEti").val(js[0].reportEtiquetas);
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaPredet").val(js[0].bascula);
		}
	} else { alert(''Error SP pMerlosUsuarios - dameConfiguracion!\n''+JSON.stringify(ret)); }}, false);
}

function guardarUsuarioDatos(){
	var usuario = ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpMerlosUsuario").val().trim(); 
	var ImpDocu = ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpImpDocu").val().trim(); 
	var ImpEti = ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpImpEti").val().trim(); 
	var ReportAlb = ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpReportAlb").val().trim(); 
	var ReportFra = ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpReportFra").val().trim(); 
	var ReportEti = ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpReportEti").val().trim(); 
	var BasculaPredet = ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaPredet").val().trim(); 
	if(usuario==="" || (ImpDocu==="" && ImpEti===""&& ReportAlb===""&& ReportFra===""&& ReportEti===""&& BasculaPredet==="")){ return; }

	var parametros = ''{"sp":"pMerlosUsuarios","modo":"guardarDatos","usuario":"''+usuario+''","ImpDocu":"''+ImpDocu+''","ImpEti":"''+ImpEti+''","ReportAlb":"''+ReportAlb+''","ReportFra":"''+ReportFra+''"''
					+'',"ReportEti":"''+ReportEti+''","BasculaPredet":"''+BasculaPredet+''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		
	} else { alert(''Error SP pMerlosUsuarios - guardarDatos!\n''+JSON.stringify(ret)); }}, false);
}
</script>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'noicon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,0,0,1)
 ,(N'Merlos_Impresion',N'flx-objectlist',N'project',N'Merlos_Impresions',NULL,N'Merlos_Impresion',N'Impresión',N'default',0,0,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'printer-2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,0,0,1)
 ,(N'Merlos_Trabajo',N'flx-html',N'project',NULL,NULL,N'Merlos_Trabajo',N'Merlos_Trabajo',N'none',1,1,1,0,NULL,NULL,N'<div id="dvTrabajoInputs" class="tbSeccion">
    <div>
        NÚMERO
        <br>
        <input type="text" id="inpNumero" class="tecNum sld">
    </div>
    <div>
        SERIE
        <br>
        <input type="text" id="inpSerie" class=''clsSerie'' onclick="' + convert(nvarchar(max),NCHAR(36)) + N'(this).select();" onkeyup="this.value=this.value.toUpperCase();">
        <div id="dvSeries" class="dvTrabajoInp"></div>
    </div>
    <div>
        ZONA
        <br>
        <input type="text" id="inpZona" class=''clsZona'' onkeyup="this.value=this.value.toUpperCase();">
        <div id="dvZonas" class="dvTrabajoInp"></div>
    </div>
    <div>
        RUTA
        <br>
        <input type="text" id="inpRuta" class=''clsRuta'' onclick="' + convert(nvarchar(max),NCHAR(36)) + N'(this).select();">
        <div id="dvRutas" class="dvTrabajoInp"></div>
    </div>
    <div>
        ARTÍCULO
        <br>
        <input type="text" id="inpArticulo" class=''clsArticulo sld'' onclick="' + convert(nvarchar(max),NCHAR(36)) + N'(this).select();">
    </div>
    <div>
        ENTREGA
        <br>
        <merlos-calendar id="inpEntrega" class="clsEntrega sld"></merlos-calendar>
    </div>
    <div class="btns taC" style="padding:0;"><div class="btnAzul esq05 dvBtn" onclick="flexygo.nav.execProcess(''GoHome'','''','''',null,null,''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this));">Limpiar</div></div>
    <div class="btns taC" style="padding:0;"><div class="btnVerde esq05 dvBtn" onclick="buscarGlobal()">buscar</div></div>
</div>

<div id="dvResultados" class="tbSeccion" style="margin-top:20px;"></div>
<div id="dvDatosDePedido" class="tbSeccion"></div>
<div id="dvLineasDePedido" class="tbSeccion"></div>
<div id="dvLineasDeArticulo" class="tbSeccion"></div>

<div id="dvCambiarArticulos" class="tbSeccion dv inv">
    <div class="dvTit">Cambiar Artículos
        <img src="./Merlos/images/cerrar.png" style="float:right; width:30px; margin-top:-5px; cursor:pointer;" onclick="cerrarCambiarArticulos()">
    </div>
    <div id="dvCambiarArticulosContenido" style="padding:10px;"></div>
</div> 

<div id="dvCambiarArticulosEq" class="tbSeccion dv inv">
    <div class="dvTit">Cambiar Artículos - Equivalencias
        <img src="./Merlos/images/cerrar.png" style="float:right; width:30px; margin-top:-5px; cursor:pointer;" onclick="cerrarCambiarArticulosEq()">
    </div>
    <div id="dvCambiarArticulosEqContenido" style="padding:10px;"></div>
</div> 


<script>
    // Comprobar Ejercicio en Curso ----------------------------------------------------------------------------------------------------------------------------
    var parametros = ''{"sp":"pConfiguracion","modo":"ComprobarEjercicio"}'';
    flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
        if(ret.JSCode!=="OK"){ console.log("Se ha actualizado el portal al ejercicio "+ret.JSCode); }
    }else{ alert(''Error S.P. pConfiguracion - ComprobarEjercicio!!!\n''+JSON.stringify(ret)); } }, false);
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------

    ' + convert(nvarchar(max),NCHAR(36)) + N'("#mainNav, .tbSeccion").hide();
    ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpEntrega-input").val(fechaDeManyana());

    var SeriesCargadas = false;
    var RutasCargadas = false;
    var ZonasCargadas = false;
    var btnMerlosTraspasoIO = false;
    var facturaDirecta = "";
    var FacturaDirecta = 0;
    var cambiarArticuloListado = [];
    var modificarPedidoListado = [];
    var listadoArticulos = "";
    var contadorTraspasoEnUso = 0;
    var modificandoPedido = false;
    var cambiandoArticulo = false;

    var spnOrden_numero = 0;

    if (IntegridadVerificada) { iniciar(); } else { ComprobarIntegridad(); }

    function ComprobarIntegridad() {
        abrirVelo(icoCarga50 + "<br><br>comprobando la integridad de la aplicación...");
        var parametros = ''{"sp":"pConfiguracion","modo":"comprobarConfiguracionEmpresa",'' + params + ''}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) { if (ret) {
            var js = JSON.parse(limpiarCadena(ret.JSCode));
            if(js[0].msj=="NoExisteConfiguracion"){ 
                if (currentRole === "Admins") { flexygo.nav.openPageName(''Merlos_Administrador'', '''', '''', '''', ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this)); }
                else {
                    abrirVelo(
                        icoAlert50
                        + "<h1 style=''font:24px arial; color:#900;''>No existe una configuración!</h1>"
                        + "<h2 style=''font:18px arial; color:#900;''>Póngase en contacto con su distribuidor.</h2>"
                    );
                }
            }
            else{  cerrarVelo(); iniciarTrabajo(); }
        } else { alert(''Error SP pConfiguracion - comprobarConfiguracionEmpresa!'' + JSON.stringify(ret)); } }, false);
    }

    function iniciarTrabajo() {
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTrabajoInputs").slideDown();
        cargarSeries();
        cargarRutas();
        cargarZonas();
        activarObjetos();
        cargarUsuarioFiltro();
    }


    ' + convert(nvarchar(max),NCHAR(36)) + N'(document).keypress(function (e) {
        if (e.keyCode === 13) {
            if (ControlActivo === "entradaPesoManual") { capturarPesoDeSuma(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#entradaPesoManual").val())); }
            else {
                if      (' + convert(nvarchar(max),NCHAR(36)) + N'("#nBultos").is(":focus")) { ' + convert(nvarchar(max),NCHAR(36)) + N'("nCajas").focus(); return; }
                else if (' + convert(nvarchar(max),NCHAR(36)) + N'("#nBultos").is(":focus")) { generarAlbaranDeVenta(); return; }
                else if (' + convert(nvarchar(max),NCHAR(36)) + N'("#nBultos, #nCajas, .inpPesoArt, .tecV, #inpNumero").is(":focus")) { tecladoNumerico("ENTER"); }
                else if (' + convert(nvarchar(max),NCHAR(36)) + N'(".tecNum, #inpArticulo").is(":focus")) { buscarGlobal(); }
                else if (' + convert(nvarchar(max),NCHAR(36)) + N'("#inpRuta").is(":focus")) { asignarRuta(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpRuta").val())); }
            }
        }else {
            if (' + convert(nvarchar(max),NCHAR(36)) + N'("#tecladoNumericoPantalla").is(":visible")) { e.preventDefault(); tecladoNumerico(String.fromCharCode(e.which)); }
        }
    });

    ' + convert(nvarchar(max),NCHAR(36)) + N'(".sld").on("click", function () { ' + convert(nvarchar(max),NCHAR(36)) + N'(".dvTrabajoInp").slideUp(); });


    function activarObjetos() {
        ControlActivo = ClaseActiva = "";

        ' + convert(nvarchar(max),NCHAR(36)) + N'("input").off().on("click", function () {
            ControlActivo = this.id;
            ClaseActiva = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("class").split(" ")[0];

            if (' + convert(nvarchar(max),NCHAR(36)) + N'("#" + ControlActivo).hasClass("noInput")) { return; }

            if (ControlActivo === "inpNumero") { tecladoModo = "Buscar"; }

            if       (ClaseActiva === "clsSerie") { mostrarSeries(); }
            else if  (ClaseActiva === "clsZona")  { mostrarZonas(); }
            else if  (ClaseActiva === "clsRuta")  { mostrarRutas(); }
            else if  (ClaseActiva === "tecNum")   { ControlActivo = this.id; ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + ControlActivo).val(""); tecladoNum(ControlActivo); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvRutas").hide(); }
            else if  (ClaseActiva === "tecAlf")   { tecladoAlf(ControlActivo); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvRutas").hide(); }
        });

        ' + convert(nvarchar(max),NCHAR(36)) + N'(".imgImp").off().on("click", function () {
            var imp = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("class").split(" ")[1];
            if (imp === "alb") {
                if (imprimirAlbaran === 0) {
                    imprimirAlbaran = 1;
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnImpAlb").attr("src", "./Merlos/images/btnImprimirAlbaranI.png");
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnGenerarAlbaran").html("GENERAR ALBARÁN");
                }
                else {
                    imprimirAlbaran = 0;
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnImpAlb").attr("src", "./Merlos/images/btnImprimirAlbaranO.png");
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnGenerarAlbaran").html("IMPRIMIR ETIQUETAS");
                }
            }
            if (imp === "eti") {
                if (imprimirEtiquetas === 0) { imprimirEtiquetas = 1; ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnImpEti").attr("src", "./Merlos/images/btnImprimirEtiquetasI.png"); }
                else { imprimirEtiquetas = 0; ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnImpEti").attr("src", "./Merlos/images/btnImprimirEtiquetasO.png"); }
            }

            if (imprimirAlbaran === 0 && imprimirEtiquetas === 0) { ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnGenerarAlbaran").hide(); } else { ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnGenerarAlbaran").show(); }
        });

        BotoneraEstado();
    }

    function BotoneraEstado(){
        // inicializar botones
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnObservaciones img").attr("src","./Merlos/images/btnObservaciones.png").addClass("btnAlo curP");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnPreparacion img").attr("src","./Merlos/images/btnPrep0_O.png").removeClass("btnAlo curP"); 
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnRecargar img").attr("src","./Merlos/images/btnRecargarC.png").addClass("btnAlo curP");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCambiarArt img").attr("src","./Merlos/images/btnCambiarArtC.png").addClass("btnAlo curP");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedido.png").addClass("btnAlo curP");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosAtras img").attr("src","./Merlos/images/btnMerlosAtrasC.png").addClass("btnAlo curP");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosPreparar img").attr("src","./Merlos/images/Preparar.png").addClass("btnAlo curP"); 
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosTraspaso img").attr("src","./Merlos/images/Traspasar.png").addClass("btnAlo curP");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnTraspasoCompleto img").attr("src","./Merlos/images/TraspasoCompleto.png").addClass("btnAlo curP");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCancelarPreparacion img").attr("src","./Merlos/images/CancelarPreparacion.png").addClass("btnAlo curP");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnFinalizarPedido img").attr("src","./Merlos/images/FinalizarPedido.png").addClass("btnAlo curP");

        // botones estados
        if( ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasDePedidoTraspaso").is(":visible") ){
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnPreparacion img").attr("src","./Merlos/images/btnPrep0_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosPreparar img").attr("src","./Merlos/images/Preparar_O.png").removeClass("btnAlo curP"); 
        }

        if( ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasDeArticulo").is(":visible") ){
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCambiarArt img").attr("src","./Merlos/images/btnCambiarArtC_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedido_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnPreparacion img").attr("src","./Merlos/images/btnPrep0.png").addClass("btnAlo curP");
        }

        if(!btnMerlosTraspasoIO){' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosTraspaso img").attr("src","./Merlos/images/Traspasar_O.png").removeClass("btnAlo curP"); }

        if(modificandoPedido){
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedidoVerde.png").addClass("btnAlo curP");
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbDescripcion").css("background","#DBFFD6").html("Modificación del Pedido");

            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosPreparar img").attr("src","./Merlos/images/Preparar_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnRecargar img").attr("src","./Merlos/images/btnRecargarC_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCambiarArt img").attr("src","./Merlos/images/btnCambiarArtC_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosTraspaso img").attr("src","./Merlos/images/Traspasar_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnTraspasoCompleto img").attr("src","./Merlos/images/TraspasoCompleto_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnPreparacion img").attr("src","./Merlos/images/btnPrep0_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosAtras img").attr("src","./Merlos/images/btnMerlosAtrasC_O.png").removeClass("btnAlo curP"); 
        }

        if(cambiandoArticulo){
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCambiarArt img").attr("src","./Merlos/images/CambiarArticuloVerde.png").addClass("btnAlo curP");
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbDescripcion").css("background","#DBFFD6").html("Cambiar Artículo con equivalente");

            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosPreparar img").attr("src","./Merlos/images/Preparar_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnRecargar img").attr("src","./Merlos/images/btnRecargarC_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedido_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosTraspaso img").attr("src","./Merlos/images/Traspasar_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnTraspasoCompleto img").attr("src","./Merlos/images/TraspasoCompleto_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnPreparacion img").attr("src","./Merlos/images/btnPrep0_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosAtras img").attr("src","./Merlos/images/btnMerlosAtrasC_O.png").removeClass("btnAlo curP"); 
        }
    }

    function cargarUsuarioFiltro(){
        var parametros = ''{"sp":"pUsuario","modo":"dameFiltro",'' + params + ''}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) { if (ret) {
            var js = JSON.parse(limpiarCadena(ret.JSCode));
            if(js.length>0){ usuarioFiltro = js.filtro+";"+js.valor; }else{ usuarioFiltro=""; }
        } else { alert(''Error SP pUsuario - dameFiltro!'' + JSON.stringify(ret)); } }, false);
    }

    function tecladoNum(elControl) {
        cntrl = "teclado_" + elControl;
        abrirVeloTeclado("<div id=''dvTeclado''></div>", 1);
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTeclado").load("./Merlos/content/tecladoNum.html");
    }

    function tecladoNumerico(num) {
        if (num === "ENTER") {
            // entrada de peso manual
            if (ControlActivo === "entradaPesoManual") { capturarPesoDeSuma(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#entradaPesoManual").val())); }
            // cálculo de unidades/cajas SIN LOTE
            else if (ControlActivo === "sinLoteUDS") { cerrarVeloTeclado(); }
            else if (ControlActivo === "sinLoteCAJAS") { calcularUDSDesdeSinLoteCAJAS(); }
            // cálculo de unidades/cajas CON LOTE
            else if (ControlActivo.substring(0, 20) === "inp_LoteEntradaCajas") { calcularUdsDesdeConLoteCAJAS(ControlActivo); }
            else if (ControlActivo.substring(0, 18) === "inp_LoteEntradaUds") { calcularCajasDesdeConLoteUDS(ControlActivo); ventaCajas(ControlActivo); cerrarVeloTeclado(); }

            else if (ControlActivo === "inpNumero") { buscarGlobal(); cerrarVeloTeclado(); }
            else if (ControlActivo === "nBultos") { replicarBultos(); cerrarVeloTeclado(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#nBultos").focus(); }
            else { cerrarVeloTeclado(); }
        } else {
            var CntrlText = "";
            if (num === "<") { CntrlText = CntrlText.substr(0, CntrlText.length - 1); }
            else if (num === "<<") { CntrlText = ""; }
            else if (num === "") { ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTeclado").slideUp(); cerrarVeloTeclado(); }
            else { CntrlText = ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + ControlActivo).val() + num; }
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + ControlActivo).val(CntrlText);
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#tecladoNumericoPantalla").text(CntrlText);
        }
    }

    function cargarSeries() {
        if (!SeriesCargadas) {
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpSerie").val("cargando series...").css("color", "red");
            var parametros = ''{"sp":"pSeries"}'';
            flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {
                if (ret) {
                    var contenido = "";
                    var js = JSON.parse(limpiarCadena(ret.JSCode));
                    if (js.length > 0) {
                        for (var i = 0; i < js.length; i++) { contenido += "<div class=''dvSub'' onclick=''asignarSerie(\"" + js[i].codigo + " - " + js[i].nombre + "\")''>" + js[i].codigo + "-" + js[i].nombre + "</div>"; }
                    }
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvSeries").html(contenido);
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpSerie").val("").css("color", "#333333");
                } else { alert(''Error SP pSeries!'' + JSON.stringify(ret)); }
            }, false);
        }
    }

    function cargarZonas() {
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpZona").val("cargando zonas...").css("color", "red");
        var parametros = ''{"sp":"pZonas"}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {
            if (ret) {
                var contenido = "";
                var js = JSON.parse(limpiarCadena(ret.JSCode));
                if (js.length > 0) {
                    for (var i = 0; i < js.length; i++) { contenido += "<div class=''dvSub'' onclick=''asignarZona(\""+js[i].VALOR+"\")''>" + js[i].VALOR.toUpperCase() + "</div>"; }
                }
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvZonas").html(contenido);
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpZona").val("").css("color", "#333333");
            } else { alert(''Error SP pZonas!'' + JSON.stringify(ret)); }
        }, false);
    }

    function cargarRutas() {
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpRuta").val("cargando rutas...").css("color", "red");
        var parametros = ''{"sp":"pRutas"}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {
            if (ret) {
                var contenido = "";
                var js = JSON.parse(limpiarCadena(ret.JSCode));
                if (js.length > 0) {
                    for (var i = 0; i < js.length; i++) { contenido += "<div class=''dvSub'' onclick=''asignarRuta(\"" + js[i].codigo + "\",\"" + js[i].nombre + "\")''>" + js[i].codigo + "-" + js[i].nombre + "</div>"; }
                }
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvRutas").html(contenido);
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpRuta").val("").css("color", "#333333");
            } else { alert(''Error SP pRutas!'' + JSON.stringify(ret)); }
        }, false);
    }

    function mostrarSeries() {
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".teclado, .resultados").hide();
        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#dvSeries").is(":visible")) { ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvSeries").slideUp(); }
        else { ' + convert(nvarchar(max),NCHAR(36)) + N'(".dvTrabajoInp").hide(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvSeries").slideDown(); }
    }

    function mostrarZonas() {
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".teclado, .resultados").hide();
        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#dvZonas").is(":visible")) { ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvZonas").slideUp(); }
        else { ' + convert(nvarchar(max),NCHAR(36)) + N'(".dvTrabajoInp").hide(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvZonas").slideDown(); }
    }

    function mostrarRutas() {
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".tdRuta").removeClass("inv");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".teclado, .resultados").hide();
        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#dvRutas").is(":visible")) { ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvRutas").slideUp(); }
        else {
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".dvTrabajoInp").hide(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvRutas").slideDown();
            if (Left(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpSerie").val(),2) === "ET") { 
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvRutas").find(".dvSub").each(function () {
                    if ((' + convert(nvarchar(max),NCHAR(36)) + N'(this).text()).substring(0, 1) !== "E" && (' + convert(nvarchar(max),NCHAR(36)) + N'(this).text()).substring(0, 1) !== "A") { ' + convert(nvarchar(max),NCHAR(36)) + N'(this).addClass("inv"); } else { ' + convert(nvarchar(max),NCHAR(36)) + N'(this).removeClass("inv"); }
                });
            }
        }
    }

    function asignarRuta(codigo, nombre) {
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpRuta").val(codigo + " - " + nombre);
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvRutas").hide();
        Ruta = trsps_ruta = codigo;
        buscarGlobal();
    }

    function asignarSerie(codigo, nombre) {
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpSerie").val(codigo + " - " + nombre);
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvSeries").hide();
        Serie = trsps_serie = codigo;
        buscarGlobal();
    }

    function asignarZona(Zona) {
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpZona").val(Zona);
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvZonas").hide();
        Zona = trsps_serie = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpZona").val()).toUpperCase();
        buscarGlobal();
    }

    function pedidoEnUso(numero, letra, ruta, n_ruta, usuario) {
        abrirVelo(
            "<div id=''veloAvDesbloqueo'' class=''taC'' style=''font:16px arial; color:#333;''>"
            + icoAlert50 + "<br><br>"
            + "El pedido " + numero + " (" + letra + ") está EN USO por el usuario " + usuario
            + "	<br><br><br><span class=''btnVerde esq05'' onclick=''desbloquearPedido(\"" + numero + "\",\"" + letra + "\",\"" + ruta + "\",\"" + n_ruta + "\",\"" + Almacen + "\");''>desbloquear</span>"
            + "	&nbsp;&nbsp;&nbsp;<span class=''btnRojo esq05'' onclick=''cerrarVelo();''>cancelar</span>"
            + "</div>"
        );
    }

    function desbloquearPedido(numero, letra, ruta, RutaNombre, Almacen) {
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#veloAvDesbloqueo").html(infoCarga("desbloqueando el pedido " + numero + " (" + letra + ") ..."));
        var parametros = ''{"sp":"pDesbloquearPedido","numero":"'' + numero + ''","letra":"'' + letra + ''","ruta":"'' + ruta + ''",'' + params + ''}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {
            if (ret) {
                cerrarVelo();
                verLineasDelPedido(numero, letra, ruta, RutaNombre, Almacen);
            } else { alert(''Error SP pSeries!'' + JSON.stringify(ret)); }
        }, false);
    }

    function buscarGlobal(campo) {
        if (campo === undefined || campo === "undefined") { campo = ""; }
        var bNumero = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpNumero").val());
        var bSerie = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpSerie").val()).split(" - ")[0];
        var bRuta = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpRuta").val()).split(" - ")[0];
        var bZona = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpZona").val());
        var bArticulo = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpArticulo").val());
        Entrega = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpEntrega-input").val());
        if (Entrega !== "" && Left(Entrega, 4).indexOf("-") === -1) { Entrega = Entrega.substr(8, 2) + "-" + Entrega.substr(5, 2) + "-" + Entrega.substr(0, 4); }
        Serie = bSerie;
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion, .teclado").hide();
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTrabajoInputs").show();
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvResultados").html(infoCarga("buscando pedidos")).slideDown();

        var parametros = ''{"sp":"pBuscarGlobal","pedidoActivo":"'' + PedidoActivo + ''","numero":"'' + bNumero + ''","serie":"'' + bSerie + ''","ruta":"'' + bRuta + ''"''
            + '',"zona":"'' + bZona + ''","articulo":"'' + bArticulo + ''","entrega":"'' + Entrega + ''","campo":"'' + campo + ''",'' + params + ''}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {
            if (ret) { 
                var js = JSON.parse(limpiarCadena(ret.JSCode));
                var contenido = "";
                if (js.length > 0) {
                    contenido = `<div style=''font:bold 14px arial; color:#333; background:#FFF;padding:6px;''>
                                    lista de Pedidos
                                    <span style=''float:right;''>
                                        <span style=''font:11px arial; color:#07657e; margin-right:20px;''>clic en la cabecera de la tabla para ordenar</span>
                                        <span class=''btnAmarillo'' onclick=''eliminarFiltros()''>eliminar filtros</span>
                                    </span>
                                </div>
                                <table id=''tbResultados'' class=''tbStd'' style=''border-collapse:Separate; border:1px solid #CCC;''>
                                <tr>
                                    <th style=''width:100px;'' onclick=''buscarGlobal("numero")'' style=''white-space: nowrap;''>NÚMERO <span id=''spnOrden_numero''></span></th>
                                    <th style=''width:60px;''  onclick=''buscarGlobal("letra")'' style=''white-space: nowrap;''>SERIE <span id=''spnOrden_letra''</th>
                                    <th style=''width:120px;'' onclick=''buscarGlobal("fecha")'' style=''white-space: nowrap;''>FECHA <span id=''spnOrden_fecha''</th>
                                    <th style=''width:120px;'' onclick=''buscarGlobal("entrega")'' style=''white-space: nowrap;''>ENTREGA <span id=''spnOrden_entrega''</th>
                                    <th onclick=''buscarGlobal("nombre")'' style=''white-space: nowrap;''>NOMBRE <span id=''spnOrden_nombre''</th>
                                    <th onclick=''buscarGlobal("rComercial")'' style=''white-space: nowrap;''>R. COMERCIAL <span id=''spnOrden_rComercial''</th>
                                </tr>`;
                    for (var i in js) {
                        var colorFE = " color:#333; ";
                        var colorT  = " color:#333; ";
                        facturaDirecta = ""; if (js[i].FACTDIRV) { facturaDirecta = "<span style=''font:bold 14px arial;color:orange;''>(Factura Directa)</span>"; }
                        if (parseFloat(js[i].servidas) > 0 || parseFloat(js[i].bNumero) > 0) { colorT = " color:#FF6100; "; /* naranja */ }
                        var fechaCorta = js[i].fecha.substr(8, 2) + "-" + js[i].fecha.substr(5, 2) + "-" + js[i].fecha.substr(0, 4);
                        var entregaCorta = js[i].entrega.substr(8, 2) + "-" + js[i].entrega.substr(5, 2) + "-" + js[i].entrega.substr(0, 4);
                        var esteOnClick = "onclick=''FacturaDirecta=\"" + js[i].FACTDIRV + "\"; verLineasDelPedido(\"" + js[i].numero + "\",\"" + js[i].letra + "\",\"" + js[i].ruta + "\",\"" + js[i].n_ruta + "\",\"" + js[i].almacen + "\")''";
                        if (parseInt(fechaNum(js[i].fecha)) >= parseInt(fechaNum(fechaMasDias(2,"amd")))) { colorFE = "color:red;"; }
                        // comprobamos si está EN_USO
                        if (js[i].TIPO != null) {
                            if (' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].CLAVE) == ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].EN_USO) && ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].USUARIO) !== ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(currentReference)) {
                                var colorT = " color:#900; ";
                                var esteOnClick = "onclick=''pedidoEnUso(\"" + js[i].numero + "\",\"" + js[i].letra + "\",\"" + js[i].ruta + "\",\"" + js[i].n_ruta + "\",\"" + ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].USUARIO) + "\")''";
                            }
                        }
                        contenido += "<tr " + esteOnClick + ">"
                            + "     <td style=''" + colorT + "''>" + js[i].numero + "</td>"
                            + "     <td style=''" + colorT + "''>" + js[i].letra + "</td>"
                            + "     <td style=''" + colorT + "''>" + fechaCorta + "</td>"
                            + "     <td style=''" + colorFE + "''>" + entregaCorta + "</td>"
                            + "     <td style=''" + colorT + "''>" + js[i].nombre + " " + facturaDirecta + "</td>"
                            + "     <td style=''" + colorT + "''>" + js[i].rComercial + "</td>"
                            + " </tr>";
                    }
                    contenido += "</table></div>";
                } else { contenido = "Sin resultados!"; }
                ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion, .teclado").hide();
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTrabajoInputs").show();
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvResultados").html(contenido).slideDown();
                // pintar Filtros
                if (js.length > 0) {
                    var jsf = js[0].filtros;
                    for(var i in jsf) {  ' + convert(nvarchar(max),NCHAR(36)) + N'("#spnOrden_"+jsf[i].campo).html(window["spOrden"+jsf[i].orden]);  }
                }
            } else { alert(''Error SP pBuscarGlobal!'' + JSON.stringify(ret)); }
        }, false);
    }

    function eliminarFiltros(){
        var parametros = ''{"sp":"pUsuario","modo":"eliminarFiltros",'' + params + ''}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {if (ret) { 
                buscarGlobal();
        } else { alert(''Error SP pUsuario - eliminarFiltros!\n'' + JSON.stringify(ret)); } }, false);
    }

    function verLineasDelPedido(pedido, serie, ruta, nombreRuta, almacen) {
        var laRuta = ruta.split(" - ")[0];
        Numero = pedido;
        NumPedido = pedido;
        PedidoSerie = serie;
        Serie = serie;
        Ruta = laRuta;
        RutaNombre = nombreRuta;
        Almacen = almacen;
        modificacionesCancelar(1);
        cambiarArticuloListado=[];
        buscarPedido(pedido, serie, Ruta, nombreRuta);
    }

    function buscarPedido(pedido, serie, ruta, nombreRuta) {
        gPedido = trsps_numero = pedido;
        Serie = trsps_serie = serie;
        trsps_ruta = ruta;
        trsps_nRuta = nombreRuta;
        btnMerlosTraspasoIO=false;
        var zona = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpZona").val());        
        var parametros = ''{"sp":"pBuscarPedido","pedidoActivo":"'' + PedidoActivo + ''","pedido":"'' + pedido + ''","serie":"'' + serie + ''","ruta":"'' + ruta + ''","nombreRuta":"'' + nombreRuta + ''","zona":"'' + zona + ''",'' + params + ''}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {
            if (ret) { 
                if ((ret.JSCode).split("!")[0] === "enUSO") { pedidoEnUso(pedido,serie,ruta,nombreRuta,(ret.JSCode).split("!")[1]); return; }
                ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion, .teclado").hide();
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasDePedido").html(infoCarga("cargando el pedido " + pedido + "...")).slideDown();
                var js = JSON.parse(limpiarCadena(ret.JSCode));
                if (js.length > 0) {
                    PedidoActivo = js[0].PedidoActivo;
                    var cliObs = js[0].ClienteObs.replace(/"/g, ''-'');
                    var pedidoObs = js[0].PedidoObs.replace(/"/g, ''-'');
                    var rutaTxt = "<span style=''margin-right:30px;font:bold 20px arial;color:#07657e;''>RUTA: " + ruta + " - " + nombreRuta + "</span>"; /* azul oscuro */
                    if (ruta.trim() === "" || ruta.trim() === undefined || ruta.trim() === "null") { rutaTxt = ""; }

                    var laTabla = "<table id=''tbDatosDePedido'' style=''width:100%;border-collapse:collapse; border-bottom:1px solid #07657e;''>"
                        + "	    <tr>"
                        + "     	<td style=''vertical-align:top;''>"
                        + "         	<span style=''font:bold 20px arial; color:#07657e; ''>Traspaso del Pedido " + pedido + "-" + serie + "</span><br>" 
                        + "         	<span id=''spanDatosCliente'' style=''margin-right:30px;''></span>"
                        + "     	</td>"
                        + "     	<td style=''vertical-align:top; padding-left:50px;''>" + rutaTxt + "</td>"
                        + "     	<td style=''vertical-align:top; width:450px;''>"
                        + "				<div class=''dvBotoneraOpciones''>"
                        + "				    <div class=''btnObservaciones''><img src=''./Merlos/images/btnObservaciones.png'' style=''width:100%'' class=''btnAlo curP'' onclick=''mostrarObservaciones(\"" + cliObs + "\",\"" + pedidoObs + "\")''></div>"
                        + "				    <div class=''btnPreparacion''><img src=''./Merlos/images/btnPrep0.png'' style=''width:100%'''' class=''btnAlo curP'' onclick=''borrarPropuesta()''></div>"
                        + "				    <div class=''btnRecargar''><img src=''./Merlos/images/btnRecargarC.png'' style=''width:100%'' class=''btnAlo curP'' onclick=''recargar()''></div>"
                        + "				    <div class=''btnCambiarArt''><img src=''./Merlos/images/btnCambiarArtC.png'' style=''width:100%'' class=''btnAlo curP'' onclick=''cambiarArticulos(\"" + pedido + "\",\"" + serie + "\",\"" + ruta + "\")''></div>"
                        + "				    <div class=''btnModificarPedido''><img src=''./Merlos/images/ModificarPedido.png'' style=''width:100%'' class=''btnAlo curP'' onclick=''modificarPedido()''></div>"
                        + "				    <div class=''btnMerlosAtras''><img src=''./Merlos/images/btnMerlosAtrasC.png'' style=''width:100%;'' class=''btnAlo curP'' onclick=''traspasarAtras()''></div>"

                        + "				    <div class=''btnMerlosPreparar''><img src=''./Merlos/images/Preparar.png'' style=''width:100%;'' class=''btnAlo curP'' onclick=''traspasarArtLote()''></div>"
                        + "				    <div class=''btnMerlosTraspaso''><img src=''./Merlos/images/Traspasar.png'' style=''width:100%;'' class=''btnAlo curP'' onclick=''traspasarPedido()''></div>"
                        + "				    <div class=''btnTraspasoCompleto''><img src=''./Merlos/images/TraspasoCompleto.png'' style=''width:100%;'' class=''btnAlo curP'' onclick=''traspasoCompleto()''></div>"
                        + "				    <div class=''btnCancelarPreparacion''><img src=''./Merlos/images/CancelarPreparacion.png'' style=''width:100%;'' class=''btnAlo curP'' onclick=''cancelarPreparacion()''></div>"
                        + "				    <div class=''btnFinalizarPedido''><img src=''./Merlos/images/FinalizarPedido.png'' style=''width:100%;'' class=''btnAlo curP'' onclick=''finalizarPedido()''></div>"
                        + "				</div>"
                        + "				<div style=''height:4px;''></div>"
                        + "     	</td>"
                        + "		</tr>"
                        + "</table>"
                        + "<div id=''dvModificarPedido'' class=''inv''></div>"
                        + "<div id=''dvLineasDePedidoTraspaso'' style=''margin-top:10px;''>"
                        + "		<div id=''dvLineasDePedidoTraspasoCB''>"
                        + "			<input type=''text'' id=''inpLineasDePedidoTraspasoCB'' class=''cb'' "
                        + "			style=''background:#FFF; width:100%; padding:4px; border:none; outline:none; border-bottom:1px solid #07657e; text-align:center; font:14px arial; color:#666;'' "
                        + "			placeholder=''código de barras'' onkeyup=''cbKeyUp()'' "
                        + "			onclick=''' + convert(nvarchar(max),NCHAR(36)) + N'(this).select()''>"
                        + "		</div>"
                        + "		<table id=''tbLineasDePedidoTraspaso'' class=''tbStd'' style=''margin-top:4px;''>"
                        + "  		<tr id=''tbLineasDePedidoTraspasoCabecera00'' style=''border-bottom:1px solid #FFF;''>"
                        + "      		<th id=''tbDescripcion'' colspan=''4'' style=''text-align:center; font:bold 16px arial; color:green;''></th>"
                        + "      		<th style=''text-align:center;'' colspan=''3'' class=''bgAzulPalido''>PENDIENTE</th>"
                        + "      		<th style=''text-align:center;'' colspan=''3'' class=''bgVerdePalido''>TRASPASADO</th>"
                        + "  		</tr>"
                        + "  		<tr id=''tbLineasDePedidoTraspasoCabecera''>"
                        + "      		<th style=''''></th>"
                        + "      		<th style=''''>ARTICULO</th>"
                        + "      		<th style=''''>UBICACIÓN</th>"
                        + "      		<th style=''''>NOMBRE</th>"
                        + "      		<th style=''text-align:center;'' class=''bgAzulPalido''>CAJAS</th>"
                        + "      		<th style=''text-align:center;'' class=''bgAzulPalido''>UDS.</th>"
                        + "      		<th style=''text-align:center;'' class=''bgAzulPalido''>PESO</th>"
                        + "      		<th style=''text-align:center;'' class=''bgVerdePalido''>CAJAS</th>"
                        + "      		<th style=''text-align:center;'' class=''bgVerdePalido''>UDS.</th>"
                        + "      		<th style=''text-align:center;'' class=''bgVerdePalido''>PESO</th>"
                        + "  		</tr>";

                    var trColor = "#daf7ff"; 
                    ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbLineasDePedidoTraspasoTR").remove();
                    ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion, .teclado").hide();
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvDatosDePedido").html(laTabla).slideDown();

                    for (var i in js) {
                        if (trColor == "#daf7ff") { trColor = ""; } else { trColor = "#daf7ff"; } /* azul claro */
                        var ubicacion = js[i].ubicacion; if (ubicacion === "undefined" || ubicacion===null) { ubicacion = ""; }
                        var elID = (js[i].empresa + js[i].numero + js[i].linia + js[i].letra).replace(/\s+/g, "_");

                        var cajasPendientes = parseInt(js[i].cajas_pendientes);             if (cajasPendientes < 0) { cajasPendientes = 0; }
                        var udsPendientes = parseInt(js[i].uds_pendiente);                  if (udsPendientes   < 0) { udsPendientes = 0; }
                        var pesoPendiente = parseFloat(js[i].peso_pendiente).toFixed(3);    if (pesoPendiente   < 0) { pesoPendiente = 0.000; }
                        var cajasTraspasado = parseInt(js[i].cajas_traspasadas);
                        var udsTraspasado = parseInt(js[i].uds_traspasadas);
                        var pesoTraspasado = parseFloat(js[i].peso_traspasado).toFixed(3);

                        if (cajasPendientes == 0) { cajasPendientes = ""; }
                        if (udsPendientes == 0) { udsPendientes = ""; }
                        if (pesoPendiente == 0) { pesoPendiente = ""; }
                        if (cajasTraspasado == 0) { cajasTraspasado = ""; }
                        if (udsTraspasado == 0) { udsTraspasado = ""; }
                        if (pesoTraspasado == 0) { pesoTraspasado = ""; }

                        var stockArt = js[i].stock; if(isNaN(stockArt)){ stockArt=0; }

                        var laImgTraspasoC = "style=''width:30px; background:url(./Merlos/images/btnTC.png) no-repeat center; background-size:90% '' "
                                            +" onclick=''traspasarLineaCompleta(\"tbLinPedTraspTR_" + i + "\"); event.stopPropagation();''";
                        var fColor = "#333;";
                        var dataTraspasar = ` data-traspasar="true" `;
                        var elOnClick = ''onclick=\''editarLineaTraspaso(\"'' + elID + ''\",\"'' + js[i].numero + ''\",\"'' + js[i].articulo + ''\",\"'' + js[i].definicion.replace(/"/g, '''') + ''\",\"'' + js[i].uds_pv + ''\",\"'' + js[i].peso_pv + ''\",\"'' + js[i].servidas + ''\",\"'' + js[i].linia + ''\",\"'' + js[i].cajas_pendientes + ''\",\"'' + js[i].uds_pendiente + ''\",\"'' + js[i].peso_pendiente + ''\")\'''';
                        var elOnClickObs="";
                        if (parseInt(js[i].uds_traspasadas) == 0 && parseInt(stockArt) < parseInt(js[i].uds_pv) && js[i].cntrlSotck == false) { fColor = "orange;"; }
                        if (parseInt(stockArt) <= 0 && js[i].cntrlSotck == false) { elOnClick = ""; fColor = "red;"; laImgTraspasoC = ""; dataTraspasar = ` data-traspasar="false" `;}
                        if (parseInt(js[i].dto) == 100) { trColor = "yellow;"; }

                        var tdID = "";   
                        if (' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].TipoIVA)=="" && js[i].preparado=="0" ) {
                            tdID = "tdObserva_".elID;
                            fColor = "#C15D48;"; /* marrón */
                            trColor = "rgb(246,246,246);"; /* blanco roto */
                            cajasPendientes = udsPendientes = pesoPendiente = cajasTraspasado = udsTraspasado = pesoTraspasado = "";
                            elOnClickObs = ''onclick=\''traspasarObservacion(\"'' + tdID + ''\",\"'' + js[i].empresa + ''\",\"'' + js[i].numero + ''\",\"'' + js[i].linia + ''\",\"'' + js[i].letra + ''\"); event.stopPropagation();\'''';
                            laImgTraspasoC = "";
                        } 
                        if (' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].TipoIVA)!=="" && js[i].preparado == "1") {
                            tdID = "tdObserva_" + elID;
                            fColor = "#009907;"; /* verde oscuro */ laImgTraspasoC = "";
                            trColor = "#d6ffde;";   /* verde claro */
                            elOnClickObs = ''onclick=\''traspasarObservacion(\"'' + tdID + ''\",\"'' + js[i].empresa + ''\",\"'' + js[i].numero + ''\",\"'' + js[i].linia + ''\",\"'' + js[i].letra + ''\"); event.stopPropagation();\'''';
                        }

                        var elCodigo = js[i].cliente;
                        var laUbicacion = js[i].ubicacion; if (laUbicacion === "") { laUbicacion = " "; }
                        var rCom = js[i].rComercial;
                        var elNombre = js[i].n_cliente;
                        var datosCli = js[i].direccion + "<br>" + js[i].copdpost + " " + js[i].poblacion + "<br>" + js[i].provincia + "";
                        var laRuta = "RUTA: " + js[i].ruta + " - " + js[i].n_ruta;
                        var elVendedor = "Vendedor: " + js[i].vendedor + " - " + js[i].n_vendedor + "<br>Telf: " + js[i].Movil_vendedor;

                        var cajasVis = ""; var pesoVis = "";
                        if (parseInt(js[i].cajas_pv) == 0) { cajasVis = "inv"; }
                        if (parseFloat(js[i].peso_pv) < 0.001) { pesoVis = "inv"; }

                        var pedidoArticulo = js[i].articulo;
                        var pedidoDefinicion = js[i].definicion;

                        // Pintar artículos cambiados si existen
                        var articuloCambiado = "";
                        for(var l in cambiarArticuloListado){
                            if(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(cambiarArticuloListado[l].articulo)===' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].articulo)){
                                articuloCambiado = `<br><span style="font:12px arial;color:#FF5733;">ORIGINAL: `+cambiarArticuloListado[l].articuloOriginal+` - `+cambiarArticuloListado[l].definicionOriginal+`</span>`;
                            }
                        }

                        ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasDePedidoTraspaso").append(
                            "<tr id=''tbLinPedTraspTR_" + i + "'' class=''tbLineasDePedidoTraspasoTR'' style=''background:" + trColor + "'' " + elOnClick + " " +dataTraspasar+" "
                            +"  data-linea=''" + js[i].linia + "'' data-articulo=''"+pedidoArticulo+"'' data-cajas=''"+cajasPendientes+"'' data-unidades=''"+udsPendientes+"'' data-peso=''"+pesoPendiente+"''>"
                            + "          <td  id=''tbLinPedTraspTRTC_" + i + "'' class=''tbAccion'' " + laImgTraspasoC
                            + "			</td>"
                            + "          <td class=''tbLineasDePedidoTraspasoTDlineaPte inv'' style=''color:" + fColor + "''>" + js[i].linia + "</td>"
                            + "          <td class=''tbLineasDePedidoTraspasoTDarticuloPte'' style=''color:" + fColor + "''>" + pedidoArticulo + "</td>"
                            + "          <td style=''color:" + fColor + "''>" + ubicacion + "</td>"
                            + "          <td  id=''" + tdID + "'' class=''tdDefinicion'' style=''color:" + fColor + "'' "+elOnClickObs+">" + pedidoDefinicion + articuloCambiado + "</td>"
                            + "          <td class=''tbLineasDePedidoTraspasoTDcajasPte'' style=''text-align:right;color:" + fColor + "''><span class=''" + cajasVis + "''>" + cajasPendientes + "</span></td>"
                            + "          <td class=''tbLineasDePedidoTraspasoTDunidadesPte'' style=''text-align:right;color:" + fColor + "''>" + udsPendientes + "</td>"
                            + "          <td class=''tbLineasDePedidoTraspasoTDpesoPte'' style=''text-align:right;color:" + fColor + "''><span class=''" + pesoVis + "''>" + pesoPendiente + "</span></td>"
                            + "          <td style=''text-align:right;color:" + fColor + " border-left-color:#07657e;''><span class=''" + cajasVis + " ''>" + cajasTraspasado + "</span></td>"
                            + "          <td style=''text-align:right;color:" + fColor + "''>" + udsTraspasado + "</td>"
                            + "          <td style=''text-align:right;color:" + fColor + "''><span class=''" + pesoVis + "''>" + pesoTraspasado + "</span></td>"
                            + "</tr>"
                        )                            
                        
                        if(cajasTraspasado!=="" || udsTraspasado!=="" || pesoTraspasado!==""){btnMerlosTraspasoIO=true;} // Control del botón btnMerlosTraspaso
                    }

                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasDePedidoTraspaso").append("</table></div><div id=''dvLineasDePedidoTraspasoContenido''></div>");

                    activarObjetos();
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpLineasDePedidoTraspasoCB").focus();
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnTraspasoDef").show(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnTraspasoDef").off("click").on("click", function () { traspasarPedido(); });
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnMerlosHome").show(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnCambiarArtC,#btnRecargarC").show();
                    if (' + convert(nvarchar(max),NCHAR(36)) + N'.trim(rCom) !== "") { rCom = "<br><span style=\"font:bold 14px arial; color:#666;\">" + rCom + "</span>"; }
                    ' + convert(nvarchar(max),NCHAR(36)) + N'(''#spanDatosCliente'').html(
                        "		    <span>" + elNombre + " (<span id=\"spCodCli\">" + elCodigo + "</span>)</span>"
                        + rCom
                        + "		    <br />"
                        + "		    <span style=\"font:14px arial; color:#333;\">" + datosCli + "</span>"
                    );
                    ' + convert(nvarchar(max),NCHAR(36)) + N'(''#spBPruta'').html(laRuta);
                    ' + convert(nvarchar(max),NCHAR(36)) + N'(''#spBPvendedor'').html(elVendedor);
                    ' + convert(nvarchar(max),NCHAR(36)) + N'(''#spCliObs'').html(cliObs);
                    ' + convert(nvarchar(max),NCHAR(36)) + N'(''#spPedidoObs'').html(pedidoObs);
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasDePedido").stop().hide();
                    if(cliObs!=="" || pedidoObs!==""){ ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnObservaciones img").attr("src", "./Merlos/images/btnObservaciones.png").addClass("btnAlo curP");  }
                    else{ ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnObservaciones img").attr("src", "./Merlos/images/btnObservaciones_O.png").removeClass("btnAlo curP"); }
                } else { ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasDePedido").html("<div class=''avRojo''>No se han obtenido resultados!</div>"); }
                BotoneraEstado();
            } else { alert(''Error SP pBuscarPedido!'' + JSON.stringify(ret)); }            
        }, false);
    }

    function mostrarObservaciones(cliObs, pedidoObs) {
        if(!' + convert(nvarchar(max),NCHAR(36)) + N'(".btnObservaciones img").hasClass("btnAlo")){ return; }
        ' + convert(nvarchar(max),NCHAR(36)) + N'("body").prepend("<div id=''dvVeloObs'' class=''dvVelo''>"
            + "      <div class=''dvCliObs vaT taL'' style=''max-width:50%; margin:5% auto; background:#f2f2f2;box-shadow: 2px 4px 10px 1px rgba(0,0,0,.3);''>"
            + "             <img src=''./Merlos/images/cerrar.png'' class=''curP'' style=''width:20px; float:right;'' onclick=''cerrarObservaciones()''>"
            + "             <br><span style=''font:bold 12px arial; color:#19456B;''>Observaciones del cliente:</span>"
            + "             <br><span id=''spCliObs''>" + cliObs + "</span>"
            + "             <br><br><span style=''font:bold 12px arial; color:#19456B;''>Observaciones del pedido:</span>"
            + "             <br><span id=''spPedidoObs''>" + pedidoObs + "</span>"
            + "             <br><br>"
            + "     </div>"
            + "</div>");
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvVeloObs").fadeIn();
    }

    function cerrarObservaciones() { ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvVeloObs").fadeOut(300, function () { ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvVeloObs").remove(); }); }

    function recargar(forzar) { 
        if((!forzar || forzar===undefined) && !' + convert(nvarchar(max),NCHAR(36)) + N'(".btnRecargar img").hasClass("btnAlo")){ return; }
        buscarPedido(trsps_numero, trsps_serie, trsps_ruta, trsps_nRuta); 
    }

    function cbKeyUp() {
        var valCB = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpLineasDePedidoTraspasoCB").val());
        if (valCB === "") { return; }
        if (event.keyCode === 13) { comprobarCB(valCB); }
    }

    function comprobarCB(cb) {
        if (cb === "") { return; }
        var parametros = ''{"sp":"pComprobarCB","CODBAR":"''+cb+''"}'';
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
            var js = JSON.parse(limpiarCadena(ret.JSCode));
            if (js.Error === "") {
                artEncontrado = ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbLineasDePedidoTraspasoTDarticuloPte:contains(''" + js.ARTICULO + "'')").text();
                if (artEncontrado !== "") {
                    // el artículo pertenece al pedido y lo tratamos
                    linea = ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbLineasDePedidoTraspasoTDarticuloPte:contains(''" + js.ARTICULO + "'')").prev().text();
                    // si trabaja con lotes
                    if (js.LOTE !== "") { 
                        // accedemos a la pantalla de lotes
                        ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbLineasDePedidoTraspasoTDarticuloPte:contains(''" + js.ARTICULO + "'')").parent().click();                        
                        setTimeout(()=>{ /* espera para pintar la tabla */
                            // nos situamos en la linea del lote y verificamos stock y preparación
                            var loteLinTR = ' + convert(nvarchar(max),NCHAR(36)) + N'(".linLote:contains(''"+js.LOTE+"'')").parent().attr("id");
                            var linStock = ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+loteLinTR).find(".inpStock").val();
                            var linUds = ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+loteLinTR).find(".inpUnidades").val(); 
                            if(parseFloat(js.UNIDADES)>parseFloat(linStock)){ abrirVelo("No hay suficiente stock del lote: "+js.LOTE+"!<br><br><br><span class=''btnRojo esq05'' onclick=''cerrarVelo();''>aceptar</span>"); traspasarAtras(); return; }
                            if(parseFloat(linUds)>parseFloat(js.UNIDADES)){ abrirVelo("Las unidades pedidas superan a las unidades del lote!<br><br><br><span class=''btnRojo esq05'' onclick=''cerrarVelo();''>aceptar</span>"); traspasarAtras(); return; }
                            // Traspasamos la linea
                            traspasarArtLote();
                        },500);                        
                    }
                    else { traspasoDesdeCB(cb, js.ARTICULO, js.UNIDADES, js.UNICAJA, js.VENTA_CAJA, linea); }
                } else { alert("El artículo NO pertenece a este pedido!"); }
            } else {
                // Errores y Avisos
                if (js.Error == "CodigoDividido") {
                    abrirVelo(
                        "<div class=''taC'' style=''font:16px arial; color:#333;''>"
                        + "Este código necesita un segundo bloque"
                        + "<br><br>"
                        + "<input type=''text'' id=''inpCB2'' "
                        + "style=''width:300px; padding:5px; text-align:center;'' class=''esq05'' "
                        + "onkeyup=''CBsegundoBloque(\"" + cb + "\")''>"
                        + "	<br><br><br><span class=''btnRojo esq05'' onclick=''cerrarVelo();''>cancelar</span>"
                        + "</div>"
                    );
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpCB2").focus();
                } else { alert(js.Error); }
            }
        } else { alert(''Error SP pComprobarCB!''+JSON.stringify(ret)); }}, false);
    }

    function traspasarAtras(forzar) {
        if(!forzar && !' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosAtras img").hasClass("btnAlo")){ return; }
        if(' + convert(nvarchar(max),NCHAR(36)) + N'("#tbModifPedido").is(":visible")){ recargar(1); }

        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasDePedidoTraspaso").is(":visible")) { 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion").hide(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTrabajoInputs, #dvResultados").slideDown(); 
            buscarGlobal();
        }

        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasDeArticulo").is(":visible")) {
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion, .btnPrep0 img").hide();
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvDatosDePedido, #dvLineasDePedidoTraspaso").slideDown(); 
        }

        BotoneraEstado();
    }

    function traspasarObservacion(td,empresa,numero,linia,letra){ 
        var txtDefinicion = ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+td).text();
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+td).html("<span style=''color:red;''>"+icoCarga16+" guardando, espera...</span>");
        var parametros = ''{"sp":"pPedidos","modo":"traspasarObservacion","linia":"''+linia+''","empresa":"''+empresa+''","letra":"''+letra+''","numero":"''+numero+''"}'';
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
            var elColor="#C15D48;";
            if(ret.JSCode==="insertado"){ elColor="green;"; }
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+td).html("<span style=''color:"+elColor+"''>"+txtDefinicion+"</span>");
        } else { alert(''Error SP pPedidos - traspasarObservacion!''+JSON.stringify(ret)); }}, false);        
    }

    function traspasarLineaCompleta(tr) {
        abrirVelo(infoCarga("traspasando la linea...)"));

        var linea = ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + tr).attr("data-linea");
        var articulo = ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + tr).attr("data-articulo");
        var cajas = (' + convert(nvarchar(max),NCHAR(36)) + N'("#" + tr).attr("data-cajas")).replace(/,/g, "");
        var unidades = (' + convert(nvarchar(max),NCHAR(36)) + N'("#" + tr).attr("data-unidades")).replace(/,/g, "");
        var peso = (' + convert(nvarchar(max),NCHAR(36)) + N'("#" + tr).attr("data-peso")).replace(/,/g, "");

        var parametros = ''{"sp":"pTraspLinCompleta","almacen":"'' + Almacen + ''","articulo":"'' + articulo + ''","cajas":"'' + ifNull(cajas,0) + ''","unidades":"'' + ifNull(unidades,0) + ''","peso":"'' + ifNull(peso,0.000) + ''","linea":"'' + linea + ''","numero":"'' + Numero + ''","serie":"'' + Serie + ''"}'';
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{ ''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
                cerrarVelo();
                verLineasDelPedido(Numero, Serie, Ruta, RutaNombre, Almacen);
            } else { alert(''Error SP pTraspLinCompleta!'' + JSON.stringify(ret)); }
        }, false);
    }

    function cancelarPreparacion(confirmado) {
        if(!' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCancelarPreparacion img").hasClass("btnAlo")){ return; }
        if (!confirmado) {
            abrirVelo(
                "<div style=''text-align:center;''>"
                + "		Confirma para cancelar la preparación!"
                + "		<br><br><br>"
                + "		<span class=''btnMVerde esq05'' onclick=''cerrarVelo();cancelarPreparacion(1);''>confirmar</span>"
                + "		&nbsp;&nbsp;&nbsp;"
                + "		<span class=''btnMRojo  esq05'' onclick=''cerrarVelo();''>cancelar</span>"
                + "</div>"
            );
        } else {
            abrirVelo(icoCarga16+" cancelando la preparación...");
            if (trsps_linea === undefined) { trsps_linea = 0; }
            var parametros = ''{"sp":"pCancelarPreparacion","pedido":"'' + trsps_numero + ''","serie":"'' + trsps_serie + ''","linea":"'' + trsps_linea + ''","almacen":"'' + Almacen + ''"}'';
            flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) { if (ret) {
                cambiarArticuloCancelar("modificacionesCancelar"); 
            } else { alert(''Error SP pCancelarPreparacion!'' + JSON.stringify(ret)); } }, false);
        }
    }

    function editarLineaTraspaso(id, numero, articulo, definicion, unidades, peso, servidas, linea, cajas, traspaso, pesoPend) {
        abrirVelo(infoCarga("editando la linea..."));

        gID = id;
        gArticulo = articulo;
        gArtUnidades = unidades;
        gArtPeso = peso;
        gArtServidas = servidas;
        trsps_numero = numero;
        trsps_articulo = articulo;
        trsps_linea = linea; 

        var parametros = ''{"sp":"pEditarLineaTraspaso","pedido":"'' + trsps_numero + ''","serie":"'' + trsps_serie + ''","articulo":"'' + trsps_articulo + ''","unidades":"'' + unidades + ''","linea":"'' + linea + ''","cajas":"'' + cajas + ''","peso":"'' + peso + ''","servidas":"'' + servidas + ''","traspaso":"'' + traspaso + ''","pesoPend":"'' + pesoPend + ''","basculaEnUso":"'' + basculaEnUso + ''"}'';
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
            var contenido = "";
            var js = JSON.parse(limpiarCadena(ret.JSCode));
            if (js.length > 0) {
                // si existen registros en Traspaso_Temporal devolvemos un aviso y salimos de la función
                if (js[0].existeTraspTemp === 1) {
                    for (var i = 0; i < js.length; i++) {                        
                        var clCampoLote = "";
                        var clCampoCajas = "";
                        var clCampoPeso = "";
                        var fCaducidad = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].fCaducidad);
                        if (js[i].lote.replace(/-/g, "") === "") { clCampoLote = "display:none;"; }
                        if (parseInt(js[i].cajas) === 0 || isNaN(js[i].cajas) || js[i].cajas === null) { clCampoCajas = "display:none;"; }
                        if (parseFloat(js[i].peso) < 0.001 || isNaN(js[i].peso) || js[i].peso === null|| js[i].peso === "") { clCampoPeso = "display:none;"; }
                        if (fCaducidad.trim() === "" || fCaducidad === "01-01-1900" || fCaducidad === undefined) { fCaducidad = "<span style=''color:#900;''>no disponible</span>"; }
                        if (i === 0) {
                            contenido = "<div style=''text-align:right;''><img src=''./Merlos/images/cerrar.png'' width=''20'' style=''cursor:pointer;'' onclick=''cerrarVelo();''/></div>"
                                + "<div style=''margin-bottom:20px;font:bold 16px arial; color:#900;''>El artículo " + gArticulo + " ya está preparado!</div>"
                                + "<table id=''tbArtEnTemp'' class=''tbStd''>"
                                + "	<tr>"
                                + "		<th style=''text-align:center;" + clCampoLote + "''>Lote</th>"
                                + "		<th style=''text-align:center;''>Caducidad</th>"
                                + "		<th style=''text-align:center; " + clCampoCajas + "''>Cajas</th>"
                                + "		<th style=''text-align:center;''>Uds</th>"
                                + "		<th style=''text-align:center;" + clCampoPeso + "'' class=''campoPeso''>Peso</th>"
                                + "	</tr>";
                        }
                        contenido += "	<tr>"
                            + "		<td style=''text-align:center;" + clCampoLote + "''>" + js[i].lote + "</td>"
                            + "		<td style=''text-align:center;''>" + fCaducidad + "</td>"
                            + "		<td style=''text-align:center;" + clCampoCajas + "''>" + parseInt(js[i].cajas) + "</td>"
                            + "		<td style=''text-align:center;''>" + parseInt(js[i].unidades) + "</td>"
                            + "		<td style=''text-align:center;" + clCampoPeso + "''>" + parseFloat(js[i].peso).toFixed(3) + "</td>"
                            + "	</tr>";
                    }
                    contenido += "</table>";
                    abrirVelo(contenido);
                    return;
                }

                // Trabajo SIN lotes
                if (js[0].existeTraspTemp === 2) {
                    tecladoModo = "AsignarSinLote";

                    traspaso = parseInt(js[0].TRASPASO);
                    var pedidoCajas = parseInt(js[0].pCajas);
                    var pedidoUnidades = parseInt(js[0].UNIDADES);
                    var pedidoPeso = parseFloat(js[0].PESO).toFixed(3);
                    var stockCajas = parseInt(js[0].STOCKCAJAS);
                    var stockUds = parseInt(js[0].elStock);
                    var stockPeso = parseFloat(js[0].STOCKPESO).toFixed(3);
                    var prepCajas = parseInt(js[0].tempCajas);
                    var prepUds = parseInt(js[0].tempUds);
                    var prepPeso = parseFloat(js[0].tempPeso).toFixed(3);

                    var colSpanStock = 3;
                    var colSpanPrep  = 4;

                    if (stockPeso < 0) { stockPeso = 0; }
                    var cajasVis = "display:visible;"; if (pedidoCajas < 1) { cajasVis = "display:none;"; colSpanStock--; colSpanPrep--; }
                    var pesoVis = "display:visible;"; if (pedidoPeso < 0.0001) { pesoVis = "display:none;"; colSpanStock--; colSpanPrep--; }

                    contenido = "<table id=''tbCabaceraLotes'' class=''tbStd'' style=''margin-top:10px;''>"
                        + "  <tr>"
                        + "      <th style=''''>ARTICULO</th>"
                        + "      <th style=''''>NOMBRE</th>"
                        + "      <th style=''text-align:center;" + cajasVis + "''>CAJAS</th>"
                        + "      <th style=''text-align:center;''>UDS.</th>"
                        + "      <th style=''text-align:center; "+pesoVis+"''>PESO</th>"
                        + "  </tr>"
                        + "	 <tr style=''background:#daf7ff;''>"
                        + "          <td style=''''>" + trsps_articulo + "</td>"
                        + "          <td style=''''>" + js[0].DEFINICION + "</td>"
                        + "          <td style=''text-align:right;" + cajasVis + "''>" + cajas + "</td>"
                        + "          <td style=''text-align:right;''>" + (pedidoUnidades-traspaso) + "</td>"
                        + "          <td style=''text-align:right;" + pesoVis + "''>" + parseFloat(pesoPend).toFixed(3) + "</td>"
                        + "      </tr>"
                        + "</table>"
                        + "<br><br>"
                        + "<table id=''tbCoU'' class=''tbStd''>"
                        + "	 <tr style=''border-bottom:1px solid #FFF;''>"
                        + "	    <th colspan=''"+colSpanStock+"'' style=''text-align:center;''>STOCK</th>"
                        + "	    <th colspan=''"+colSpanPrep+"'' class=''bgVerdePalido'' style=''vertical-align:middle;text-align:center;''>PREPARACIÓN "
                        + "			<span id=''spanBasculas'' style=''float:right; vertical-align:middle; " + pesoVis + " " + js[0].verSpnTipoBascula + "''>"
                        + "				<img id=''BI1'' class=''imgBascula01 icoBascula'' src=''./Merlos/images/" + js[0].bsc1img + "'' style=''width:30px; margin-right:20px; cursor:pointer;'' onclick=''basculaClick(\"imgBascula01\")''/> "
                        + "				<img id=''BI2'' class=''imgBascula02 icoBascula'' src=''./Merlos/images/" + js[0].bsc2img + "'' style=''width:30px; margin-right:20px; cursor:pointer;'' onclick=''basculaClick(\"imgBascula02\")''/> "
                        + "				<img id=''BI3'' class=''imgBascula03 icoBascula'' src=''./Merlos/images/" + js[0].bsc3img + "'' style=''width:30px; margin-right:20px; cursor:pointer;'' onclick=''basculaClick(\"imgBascula03\")''/> "
                        + "				<img id=''BI4'' class=''imgBascula04 icoBascula'' src=''./Merlos/images/" + js[0].bsc4img + "'' style=''width:30px; margin-right:20px; cursor:pointer;'' onclick=''basculaClick(\"imgBascula04\")''/> "
                        + "			</span>"
                        + "		</th>"
                        + "  </tr>"
                        + "  <tr id=''tbLineasDePedidoTraspasoCabecera''>"
                        + "      <th style=''width:14%;text-align:center;" + cajasVis + "''>CAJAS</th>"
                        + "      <th style=''width:14%;text-align:center;''>UDS.</th>"
                        + "      <th style=''width:14%;text-align:center;" + pesoVis + "''>PESO</th>"
                        + "      <th style=''width:14%;text-align:center;" + cajasVis + "'' class=''bgVerdePalido'' >CAJAS</th>"
                        + "      <th style=''width:14%;text-align:center;'' class=''bgVerdePalido'' >UDS.</th>"
                        + "      <th style=''width:14%;text-align:center;" + pesoVis + "'' class=''bgVerdePalido'' >PESO</th>"
                        + "      <th style=''width:14%;text-align:center;" + pesoVis + "'' class=''bgVerdePalido'' >BÁSCULA</th>"
                        + "  </tr>"
                        + "	 <tr style=''background:0;'' class=''trDatosLinea " + js[0].cntrlSotck + " " + js[0].elStock + "''>"
                        + "          <td style=''text-align:right; background:0;" + cajasVis + "''><span class=''stockCajas''>"+stockCajas+"</span></td>"
                        + "          <td style=''text-align:right; background:0;'' class=''stockUnidades''>" + stockUds + "</td>"
                        + "          <td class=''tdPeso'' style=''text-align:right; background:0;" + pesoVis + "''>" + stockPeso + "</td>"
                        + "          <td class=''bgVerdePalido'' style=''" + cajasVis + "''><input type=''text'' id=''sinLoteCAJAS'' class=''tecNum tecV taR w100'' style=''" + cajasVis + " padding:4px; border:0; outline:none; text-align:right;'' value=''" + prepCajas + "''></td>"
                        + "          <td class=''bgVerdePalido'' ><input type=''text'' id=''sinLoteUDS''   class=''tecNum tecV taR w100'' value=''" + prepUds + "'' style=''padding: 4px; border: 0; outline: none; text-align:right;''></td>"
                        + "          <td class=''bgVerdePalido'' style=''" + pesoVis + "''><input type=''text'' id=''sinLotePESO''  class=''tecNum tecV taR w100'' style=''padding:4px; border:0; outline:none; text-align:right;'' value=''" + prepPeso + "''></td>"
                        + "          <td style=''text-align:center;" + pesoVis + "'' class=''bgVerdePalido'' >"
                        + "             <div style=''display:flex; align-items:flex-start; justify-content: space-evenly;''>"
                        + "				    <div><img id=''btnBascula'' src=''./Merlos/images/btnBascula.png'' width=''40'' onclick=''capturarPeso(this.id,\"sinLotePESO\")''></div>"
                        + "				    <div>&nbsp;&nbsp;&nbsp;</div>"
                        + "				    <div><img id=''btnBascSum1'' src=''./Merlos/images/btnBasculaSuma.png'' width=''40'' onclick=''capturarPesoSuma(this.id,\"sinLotePESO\")''></div>"
                        + "             </div>"
                        + "      	</td>"
                        + "      </tr>"
                        + "</table>";

                    ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion, .teclado, #dvLineasDePedido, #dvLineasDePedidoTraspaso").hide();
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvDatosDePedido").show();
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasDeArticulo").html(contenido).slideDown();
                    activarObjetos();
                    cerrarVelo();
                    return;
                }

                // Trabajo con Lotes
                tecladoModo = "AsignarConLote";

                var elID = "";
                var cajasVis = "";
                var pesoVis = "";
                var colCab = 6; var colLin1 = 5; var colLin2 = 4; var colLin3 = 9; 
                if (cajas == "" || parseInt(cajas) == 0 || isNaN(cajas)) { cajas = ""; cajasVis = "display:none;"; colCab--; colLin1--; colLin2--; colLin3--; }
                if (pesoPend == "" || parseFloat(pesoPend) == 0.000 || isNaN(pesoPend)) { pesoPend = ""; pesoVis = "display:none;"; colCab--; colLin1--; colLin2--; colLin3--; }
                var contenido = ""
                    + "<table id=''tbCabaceraLotes'' class=''tbStd''>"
                    + "  <tr id=''tbLineasDePedidoTraspasoCabecera''>"
                    + "      <th style=''''>ARTICULO</th>"
                    + "      <th style=''''>NOMBRE</th>"
                    + "      <th style=''" + cajasVis + "''>CAJAS</th>"
                    + "      <th style=''text-align:center;''>UDS.</th>"
                    + "      <th style=''" + pesoVis + " text-align:center;''>PESO</th>"
                    + "  </tr>"
                    + "	 <tr style=''background:#daf7ff;''>"
                    + "          <td style=''''>" + articulo + "</td>"
                    + "          <td style=''''>" + js[0].descripcion + "</td>"
                    + "          <td style=''text-align:right;" + cajasVis + "'' class=''tbCabLotesCAJAS''>" + cajas + "</td>"
                    + "          <td style=''text-align:right;''>" + traspaso + "</td>"
                    + "          <td style=''text-align:right;" + pesoVis + "''>" + parseFloat(pesoPend).toFixed(3) + "</td>"
                    + "      </tr>"
                    + "</table>"
                    + "</div><br>"
                    + "<div id=''dvLineasLotes''>"
                    + "<table id=''tbLineasLotes'' class=''tbStd''>"
                    + "	    <tr style=''border-bottom:1px solid #FFF;'' class=''bgVerdePalido''>"
                    + "		<th colspan=''" + colLin1 + "'' style=''text-align:center;''>STOCK</th>"
                    + "		<th colspan=''" + colLin2 + "'' style=''vertical-align:middle; text-align:center;'' class=''bgVerdePalido''>PREPARACIÓN "
                    + "			<span id=''spanBasculas'' style=''float:right; vertical-align:middle; " + pesoVis + " " + js[0].verSpnTipoBascula + " ''>"
                    + "				<img id=''BI01'' class=''imgBascula01 icoBascula'' src=''./Merlos/images/" + js[0].bsc1img + "'' style=''width:30px; margin-right:20px; cursor:pointer;'' onclick=''basculaClick(\"imgBascula01\")''/> "
                    + "				<img id=''BI02'' class=''imgBascula02 icoBascula'' src=''./Merlos/images/" + js[0].bsc2img + "'' style=''width:30px; margin-right:20px; cursor:pointer;'' onclick=''basculaClick(\"imgBascula02\")''/> "
                    + "				<img id=''BI03'' class=''imgBascula03 icoBascula'' src=''./Merlos/images/" + js[0].bsc3img + "'' style=''width:30px; margin-right:20px; cursor:pointer;'' onclick=''basculaClick(\"imgBascula03\")''/> "
                    + "				<img id=''BI04'' class=''imgBascula04 icoBascula'' src=''./Merlos/images/" + js[0].bsc4img + "'' style=''width:30px; margin-right:20px; cursor:pointer;'' onclick=''basculaClick(\"imgBascula04\")''/> "
                    + "			</span>"
                    + "		</th>"
                    + "   </tr>"
                    + "   <tr>"
                    + "       <th style=''''>Lote</th>"
                    + "       <th style=''''>Caducidad</th>"
                    + "       <th style=''text-align:center;" + cajasVis + "''>CAJAS</th>"
                    + "       <th style=''text-align:center;''>UDS.</th>"
                    + "       <th style=''text-align:center;" + pesoVis + "''>PESO</th>"
                    + "       <th style=''text-align:center;" + cajasVis + "''  class=''bgVerdePalido''>CAJAS</th>"
                    + "       <th style=''text-align:center;'' class=''bgVerdePalido''>UDS.</th>"
                    + "       <th style=''text-align:center;" + pesoVis + "'' class=''bgVerdePalido''>PESO</th>"
                    + "       <th style=''text-align:center;" + pesoVis + "'' class=''bgVerdePalido''>BÁSCULA</th>"
                    + "   </tr>"
                    + "	  <tr style=''background:0;''><td colspan=''" + colLin3 + "'' style=''height:5px; padding:0; background:0;''></td></tr>";

                var cajasLin = parseInt(cajas);
                if(cajasLin==="" || isNaN(cajasLin)){ cajasLin=0;}
                var restoCajas = parseInt(cajas);
                var udsLin = parseInt(traspaso);
                var RESTO = parseInt(traspaso);
                var suma = 0;
                var sumaCajas = 0;
                var rowUniCaja = 0;

                for (var i = 0; i < js.length; i++) {
                    var elLOTE = (js[i].Llote).replace(/\//g, "--barra--");
                    var elID = (js[i].EMPRESA + "" + ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].NUMERO) + "" + js[i].LINIA + "" + elLOTE).replace(/\s+/g, "_");

                    var fechaCorta = js[i].Lcaducidad.substr(8, 2) + "-" + js[i].Lcaducidad.substr(5, 2) + "-" + js[i].Lcaducidad.substr(0, 4);
                    var spanCantidad = "<span style=''font:bold 14px arial; color:green;''>unidades</span>";
                    var txtColor = " color:#000; ";
                    var rowUniCaja = parseInt(js[i].UNICAJA);
                    if (rowUniCaja == "") { rowUniCaja = 0; }
                    var ttCajas = parseInt(js[i].tempCajas);
                    var ttUds = parseInt(js[i].tempUnidades);
                    var ttPeso = parseFloat(js[i].tempPeso).toFixed(3);
                    var stockCajas;
                    if(rowUniCaja>1){ stockCajas=(parseInt(js[i].Lunidades) / rowUniCaja) - ttCajas; }
                    var stockUnidades = parseInt(js[i].Lunidades) - parseInt(js[i].tempUnidades);

                    if (ttCajas > 0 || ttUds > 0 || ttPeso > 0.000) { txtColor = " color:green; "; }
                    if (stockUnidades <= 0) { continue; }

                    if (stockCajas > 0 && cajasLin > 0) {
                        if (restoCajas >= stockCajas) { cajasLin = stockCajas; restoCajas = restoCajas - stockCajas; }
                        else { cajasLin = restoCajas; restoCajas = restoCajas - cajasLin; }
                        if (cajasLin < 0) { cajasLin = 0; }
                        if (restoCajas < 0) { restoCajas = 0; }
                        sumaCajas += cajasLin;
                        if (sumaCajas > cajas) { cajasLin = 0; restoCajas = 0; }
                        cajasP = cajasLin;
                    } else { cajasP = 0; }

                    if (stockUnidades > 0 && udsLin > 0) {
                        if (RESTO >= stockUnidades) { udsLin = stockUnidades; RESTO = RESTO - stockUnidades; }
                        else { udsLin = RESTO; RESTO = RESTO - udsLin; }
                        if (udsLin < 0) { udsLin = 0; }
                        if (RESTO < 0) { RESTO = 0; }
                        suma += udsLin;
                        if (suma > traspaso) { udsLin = 0; RESTO = 0; }
                        unidadesP = udsLin;
                    } else { unidadesP = 0; }

                    pesoRes = parseFloat(js[i].Lpeso);
                    if (pesoRes < 0.000) { pesoRes = 0; }
                    if (stockCajas < 0) { stockCajas = 0; }
                    if (stockUnidades < 0) { stockUnidades = 0; }

                    var mStockCajas = stockUnidades / rowUniCaja;

                    if (rowUniCaja > 0) {
                        cajasP = unidadesP / rowUniCaja;
                        cajasLin = cajasP;
                    }

                    contenido += ""
                        + "     <tr id=''trClick_" + elID + "'' class=''trLotesDatos inp_LoteEntradaUds_" + elID + "'' data-lin=''"+ js[i].LINIA+"''>"
                        + "          <td class=''linLote'' style=''" + txtColor + "''>" + js[i].Llote + "</td>"
                        + "          <td class=''linCaducidad'' style=''text-align:center;" + txtColor + "''>" + fechaCorta + "</td>"
                        + "          <td class=''linUds'' style=''text-align:right;" + cajasVis + " " + txtColor + "'' class=''stockCajas''>" + mStockCajas + "</td>"
                        + "          <td class=''linStock'' style=''text-align:center;''><input type=''text'' id=''inp_LoteEntradaStock_"+elID+"'' class=''tecNum tecV inpStock "+js[i].controlStock+" noInput''  style=''width:150px;text-align:center;border:0; background:0;' + convert(nvarchar(max),NCHAR(36)) + N'txtColor'' value=''"+stockUnidades+"'' readonly /></td>"
                        + "          <td class=''linPeso'' style=''text-align:center;" + txtColor + " " + pesoVis + "''>" + pesoRes.toFixed(3) + "</td>"
                        + "          <td style=''text-align:center;padding:0;" + cajasVis + "''><input type=''text'' id=''inp_LoteEntradaCajas_" + elID + "'' class=''tecNum tecV inpCajas '' style=''text-align:center;" + txtColor + "'' value=''" + cajasP + "''/></td>"
                        + "          <td style=''text-align:center;padding:0;''><input type=''text'' id=''inp_LoteEntradaUds_" + elID + "'' class=''tecNum tecV inpUnidades '' style=''text-align:center;" + txtColor + "'' value=''" + unidadesP + "'' data-MIelId=''" + elID + "'' data-uc=''" + js[i].VENTA_CAJA + ";" + js[i].unidadesOcajas + ";\"" + js[i].almacen + "\"''/></td>"
                        + "          <td style=''text-align:center;padding:0;" + pesoVis + "''><input type=''text'' id=''inp_LoteEntradaPeso_" + elID + "''  class=''tecNum tecV inpPeso " + js[i].Llote + " '' style=''text-align:center;" + txtColor + "'' value=''" + ttPeso + "''/></td>"
                        + "          <td style=''text-align:center;;" + pesoVis + "''>"
                        + "             <div style=''display:flex; align-items:flex-start; justify-content: space-evenly;''>"
                        + "				    <div><img id=''btnBascula" + elID + "'' src=''./Merlos/images/btnBascula.png'' width=''40'' onclick=''capturarPeso(this.id,\"inp_LoteEntradaPeso_" + elID + "\")''></div>"
                        + "				    <div>&nbsp;&nbsp;&nbsp;</div>"
                        + "				    <div><img id=''btnBascSum" + elID + "'' src=''./Merlos/images/btnBasculaSuma.png'' width=''40'' onclick=''capturarPesoSuma(this.id,\"inp_LoteEntradaPeso_" + elID + "\")''></div>"
                        + "			    </div>"
                        + "			</td>"
                        + "      </tr>"
                        + "	    <tr style='' background:0;''><td colspan=''" + colLin3 + "'' style=''padding:2px; background:#07657e;''></td></tr>";
                }
                contenido += "</table></div>";
            } else { contenido = "<div class=''avRojo''>no se han obtenido resultados!</div>"; }

            ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion, .teclado, #dvLineasDePedido, #dvLineasDePedidoTraspaso").hide();
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvDatosDePedido").show();
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasDeArticulo").html(contenido).slideDown();
            activarObjetos();
            cerrarVelo();
        } else { alert(''Error SP pEditarLineaTraspaso!'' + JSON.stringify(ret)); } }, false);
    }

    function traspasoCompleto() {
        if(!' + convert(nvarchar(max),NCHAR(36)) + N'(".btnTraspasoCompleto img").hasClass("btnAlo")){ return; }
        abrirVelo(infoCarga("traspasando el pedido..."));
        if (!' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasDePedidoTraspaso").is(":visible")) { return; }
        var articulos = [];
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasDePedidoTraspaso").find(".tbLineasDePedidoTraspasoTR").each(function () {
            var articulo = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDarticuloPte").text();
            var cajas = (' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDcajasPte").text()).replace(/,/g, "");             if(cajas===""){ cajas="0"; }
            var unidades = (' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDunidadesPte").text()).replace(/,/g, "");       if(unidades===""){ unidades="0"; }
            var peso = (' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDpesoPte").text()).replace(/,/g, "");               if(peso===""){ peso="0"; }
            var linea = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDlineaPte").text();
            if(' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("data-traspasar")==="true"){
                articulos.push(''{"articulo":"'' + articulo + ''","cajas":"'' + cajas + ''","unidades":"'' + unidades + ''","peso":"'' + peso + ''","linea":"'' + linea + ''"}'');
            }
        }); 
        var parametros = ''{"sp":"pTraspasoCompleto"''
                    + '',"articulos":['' + articulos +'']''
                    + '',"numero":"'' + NumPedido + ''"''
                    + '',"serie":"'' + PedidoSerie + ''"''
                    + '',"ruta":"'' + Ruta + ''"''
                    + '',"almacen":"'' + Almacen + ''"''
                    + '','' + params
                    + ''}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {if (ret) { 
                cerrarVelo(); recargar(1);
        } else { alert(''Error SP pTraspasoCompleto!\n'' + JSON.stringify(ret)); } }, false);
    }

    // cálculo de unidades/cajas SIN LOTE
    function calcularCajasDesdeSinLoteUDS() { 
        cerrarVelo();       
        return; // modificación Albert 20-10-2020
        
    }
    function calcularUDSDesdeSinLoteCAJAS() {
        var lasCajas = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLoteCAJAS").val());
        if (isNaN(lasCajas)) { alert("Ups! Parece que el valor no es correcto!"); return; }
        var parametros = ''{"sp":"pDatos","modo":"dameUNICAJA","articulo":"''+trsps_articulo+''"}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {if (ret) {
            // devuelve UNICAJA de GESTION_articulo
            var UNICAJA = parseFloat(ret.JSCode);
            var unidades = 0;
            if (!isNaN(UNICAJA)) { unidades = lasCajas * UNICAJA; }
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLoteUDS").val(unidades.toFixed(0));
            cerrarVeloTeclado();
        } else { alert(''Error SP pSeries!'' + JSON.stringify(ret)); } }, false);
    }

    // cálculo de unidades/cajas CON LOTE
    function calcularCajasDesdeConLoteUDS(trgt) {
        cerrarVeloTeclado();
        return; // modificación Albert 20-10-2020
    }
    function calcularUdsDesdeConLoteCAJAS(trgt) {
        var lasCajas = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#" + trgt).val());
        var elInputUds = "inp_LoteEntradaUds" + trgt.substring(20, trgt.length);
        var parametros = ''{"sp":"pDatos","modo":"dameUNICAJA","articulo":"''+trsps_articulo+''"}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {if (ret) {
            // devuelve UNICAJA de GESTION_articulo
            var UNICAJA = parseFloat(ret.JSCode);
            var unidades = 0;
            if (!isNaN(UNICAJA)) { unidades = lasCajas * UNICAJA; }
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + elInputUds).val(unidades.toFixed(0));
            cerrarVeloTeclado();
        } else { alert(''Error SP pSeries!'' + JSON.stringify(ret)); } }, false);
    }

    function ventaCajas(id) {
        var uds = ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + id).val();
        var dataUC = ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + id).attr("data-uc");
        var ventaCaja = dataUC.split(";")[0];
        var uniCaja = dataUC.split(";")[1];

        if (ventaCaja === "1") {
            if (!Number.isInteger(parseFloat(uds).toFixed(2) / parseInt(uniCaja))) {
                abrirVelo(
                    "<div class=''taC'' style=''font:16px arial; color:#333;''>"
                    + icoAlert50 + "<br><br>"
                    + "Este artículo sólo se puede servir por cajas completas!"
                    + "	<br><br><br><span class=''btnRojo esq05'' onclick=''cerrarVelo();''>aceptar</span>"
                    + "</div>"
                );
                var elIdCajas = "inp_LoteEntradaCajas_" + ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + id).attr("data-MIelId");
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + id + ",#" + elIdCajas).val("");
            }
        }
    }


    function cambiarArticulos(pedido, serie, ruta) {
        if(!' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCambiarArt img").hasClass("btnAlo")){ return; }
        if(' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCambiarArt img").attr("src")==="./Merlos/images/CambiarArticuloVerde.png" && cambiarArticuloListado.length===0){ cambiarArticuloCancelar(); recargar(1); return; }        
        cambiandoArticulo=true;
        var parametros = ''{"sp":"pCambiarArticulos","pedido":"''+pedido+''","serie":"''+serie+''"}'';
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){ 
            var js = JSON.parse(limpiarCadena(ret.JSCode));
            if(js.length>0){
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasDePedidoTraspaso").find(".tbLineasDePedidoTraspasoTR").each(function(){
                    ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("onclick","").find(".tbAccion").attr("onclick","").css("background",""); /* eliminamos imagen y onclick de las líneas */
                    for(var i in js){
                        if(js[i].articulo===' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("data-articulo")){                            
                            ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbAccion")
                                .css("background","url(''./Merlos/images/intercambiar.png'') no-repeat center").css("background-size","90%")
                                .parent().attr("onclick","cambiarArticulosEq(''" + js[i].articulo + "'',''" + js[i].definicion + "'',''" + js[i].linia + "'',''" + js[i].uds_pv + "'')"); 
                            continue; 
                        }
                    }                    
                });
            }else{ alert("No se han obtenido registros!"); }
            activarObjetos();
        } else { alert(''Error SP pCambiarArticulos!''+JSON.stringify(ret)); }}, false);
    }

    function cambiarArticulosEq(articulo, definicion, linea, unidades) {
        abrirVelo(icoCarga16+" cargando artículos equivalentes...");

        var parametros = ''{"sp":"pCambiarArticulos","modo":"equivalencia","articulo":"''+articulo+''"}'';
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
            var elJS = JSON.parse(limpiarCadena(ret.JSCode));
            var contenido = "<div class=''poderCerrarVelo''>Selecciona el artículo equivalente para sustituir a<br>"+articulo+" - "+definicion+"</div>"
                +"<br>"
                +"<table id=''tbCambiarArticulosEq'' class=''tbStd''>"
                + "<tr>"
                + "	<th class=''taL''>Artículo</th>"
                + "	<th class=''taL''>Descripción</th>"
                + "	<th style=''text-align:center;''>Uds.</th>"
                + "	<th style=''text-align:center;''>Stock</th>"
                + "</tr>";
            if (elJS.length > 0) {
                for (var i in elJS) {
                    var elStock = parseFloat(elJS[i].stock); if (elStock === 0 || isNaN(elStock)) { elStock = ""; }
                    contenido += ""
                        + "<tr class=''sobre'' onclick=''marcarCambiarArticulo(\"" + elJS[i].ARTICULO + "\",\"" + elJS[i].NOMBRE + "\",\"" + linea + "\",\"" + unidades + "\",\"" + articulo + "\",\"" + definicion + "\")''>"
                        + "	<td class=''taL''>" + elJS[i].ARTICULO + "</td>"
                        + "	<td class=''taL''>" + elJS[i].NOMBRE + "</td>"
                        + "	<td style=''text-align:right;''>" + unidades + "</td>"
                        + "	<td style=''text-align:right;''>" + elStock + "</td>"
                        + "</tr>";
                }
                contenido += "</table>";
            } else { contenido = "<div class=''info''>No se han obtenido resultados!</div>"; }
            abrirVelo(contenido);
        } else { alert(''Error SP pCambiarArticulos - equivalencia!''+JSON.stringify(ret)); }}, false);
    }

function cambiarArticuloCancelar(modo){
	cambiandoArticulo=false;
	cambiarArticuloListado = [];
    ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCambiarArt img").attr("src","./Merlos/images/btnCambiarArtC.png");    
    if(!modo){ verLineasDelPedido(Numero,Serie,Ruta,RutaNombre,Almacen); cerrarVelo(); }
    if(modo==="modificacionesCancelar"){modificacionesCancelar();}
}

function Velo(msj){
	if(!msj){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvVELO").fadeOut(300,function(){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvVELO").remove(); }); }
	else{
		' + convert(nvarchar(max),NCHAR(36)) + N'("body").prepend(
			 "<div id=''dvVELO'' class=''inv'' style=''z-index:999;position:fixed;width:100%; height:100%; background:rgba(250,250,250,.75);''>"
			+"	<div style=''width:500px; margin:100px auto; background:#07657e; padding:20px; text-align:center; font:20px arial; color:#FFF'' "
			+"	class=''esq10 sombra001''>"+msj+"</div>"
			+"</div"
		);
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvVELO").fadeIn();
	}
}

function marcarCambiarArticulo(articulo, nombre, linea, unidades, articuloOriginal, definicionOriginal) {
    abrirVelo(icoCarga16+" Sustituyendo el artículo " + articulo + "...");
    cambiarArticuloListado.push({"linea":linea,"articuloOriginal":articuloOriginal,"definicionOriginal":definicionOriginal,"articulo":articulo,"nombre":nombre});
    var parametros = ''{"sp":"pSustituirArticulos","articulo":"''+articulo+''","linea":"''+linea+''","unidades":"''+parseInt(unidades)+''","empresa":"''+Empresa+''","letra":"''+Serie+''","numero":"''+Numero+''"}'';
    flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){ 
        cambiandoArticulo=false; recargar(1); cerrarVelo();
    } else { alert(''Error SP pSeries!''+JSON.stringify(ret)); }}, false);
}

function modificarPedido() {
    if(!' + convert(nvarchar(max),NCHAR(36)) + N'(".btnModificarPedido img").hasClass("btnAlo")){ return; }
    if(' + convert(nvarchar(max),NCHAR(36)) + N'(".btnModificarPedido img").attr("src")==="./Merlos/images/ModificarPedidoVerde.png" && modificarPedidoListado.length===0){ modificacionesCancelar(); recargar(1); return; }        
    ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedidoVerde.png");
    modificandoPedido=true;

    var parametros = ''{"sp":"pPedidos","modo":"dameLineas","Empresa":"''+Empresa+''","Numero":"''+Numero+''","Serie":"''+Serie+''"''
                    +'',"Ruta":"''+Ruta+''","Almacen":"''+Almacen+''","usuarioSesion":"''+currentReference+''","pedidoActivo":"''+PedidoActivo+''"}'';	
    flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
        var js1 = JSON.parse(limpiarCadena(ret.JSCode));  
        var js  = js1.datos;       
        if(js.length>0){
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasDePedidoTraspaso").find(".tbLineasDePedidoTraspasoTR").each(function(){
                ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("onclick","").find(".tbAccion").attr("onclick","").css("background",""); /* eliminamos imagen y onclick de las líneas */
                for(var i in js){
                    if(js[i].articulo===' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("data-articulo")){                            
                        ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbAccion")
                            .css("background","url(''./Merlos/images/intercambiar.png'') no-repeat center").css("background-size","90%")
                            .parent().attr("onclick","intercambiarArticulo("+i+","+js[i].linia+",''"+js[i].articulo+"'',''"+js[i].definicion.trim()+"'')"); 
                        continue; 
                    }
                }                    
            });
        }else{ alert("No se han obtenido registros!"); }
        activarObjetos();
    } else { alert(''Error SP pPedidos - dameLineas!''+JSON.stringify(ret)); }}, false);
}

function modificacionesCancelar(modo){
	modificandoPedido=false;
	modificarPedidoLineas = [];
    ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedido.png");
    if(!modo){ verLineasDelPedido(Numero,Serie,Ruta,RutaNombre,Almacen); cerrarVelo(); }
}

    function borrarPropuesta() {
        if( !' + convert(nvarchar(max),NCHAR(36)) + N'(".btnPreparacion img").hasClass("btnAlo") ){ return; }
        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#tbCoU").is(":visible")) { ' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLoteCAJAS,#sinLotePESO,#sinLoteUDS").val(""); }
        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasLotes").is(":visible")) { ' + convert(nvarchar(max),NCHAR(36)) + N'(".inpCajas,.inpUnidades,.inpPeso").val(""); }
    }

    function traspasarArtLote() {
        if( !' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosPreparar img").hasClass("btnAlo") ){ return; }
        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasLotes").is(":visible")) { /* artículos con lote */  traspasarConLote(); }
        else { /* artículos sin lote */  traspasarSinLote(); }
    }

    function traspasarConLote() {
        var str = "";
        var stock = 0;
        var unidades = 0;
        var cntrlStock = 0;
        var stockKO = "";
        var esteValor = 0;
        var cajas = 0;
        var stockCajas = 0;
        var peso = 0.000;
        var faltaPeso = false;

        var lineas = "";
        var lasLineas = "";
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasLotes").find(".trLotesDatos").each(function () {
            ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".inpPeso").each(function () {
                esteValor = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).val();
                if (isNaN(esteValor) || esteValor === "") { esteValor = 0; };
                str += ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr(''class'').split(" ")[3] + ''_D_'' + esteValor + "_D_"; peso = esteValor;
                lasLineas += ''{"lote":"''+' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr(''class'').split(" ")[3]+''","peso":"''+esteValor+''"'';
            });
            ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".inpStock").each(function () { esteValor = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).val(); if (isNaN(esteValor) || esteValor === "") { esteValor = 0; }; str += esteValor + "_D_"; stock = esteValor; lasLineas += '',"stock":"''+stock+''"''; cntrlStock = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr(''class'').split(" ")[3]; });
            ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".inpUnidades").each(function () { esteValor = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).val(); if (isNaN(esteValor) || esteValor === "") { esteValor = 0; }; str += esteValor + "_D_"; unidades = esteValor; lasLineas += '',"unidades":"''+unidades+''"''; });
            ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".inpCajas").each(function () { esteValor = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).val(); if (isNaN(esteValor) || esteValor === "" || esteValor === "Infinity") { esteValor = 0; }; str += esteValor + "_A_"; cajas = esteValor; if (cajas === "Infinity") { cajas = "0"; } lasLineas += '',"cajas":"''+cajas+''"''; });
            ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".stockCajas").each(function () { esteValor = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).text(); if (isNaN(esteValor) || esteValor === "") { esteValor = 0; }; stockCajas = esteValor; });

            if (' + convert(nvarchar(max),NCHAR(36)) + N'(".inpPeso").is(":visible") && parseFloat(unidades) > 0 && parseFloat(peso) === 0.000) { faltaPeso = true; }

            // controlar stock (unidades/cajas)
            if (parseFloat(unidades) > 0 && (parseFloat(unidades) > parseFloat(stock))) { stockKO = "NoHayStock"; }

            lasLineas += ''}'';
        });
        str = str.substring(0, str.length - 3);
        lasLineas = "["+lasLineas.replace(/}{/g,"},{")+"]";
        if (faltaPeso) { alert("Debes especificar el PESO!"); return; }

        // Control de Stock
        if ((cntrlStock==="1" || cntrlStock==="true") && stockKO !== "") { alert("\nNo hay stock suficiente de algún lote!\nPor favor, verifica los datos."); return; }
        if (unidades === "") { alert("Verifica los campos!"); return; }
        if (isNaN(unidades) || isNaN(peso)) { alert("Ups! Parece que los datos no son correctos!"); return; }

        ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasLotes").html(infoCarga("Actualizando los datos...</div>"));
        var jLineas = JSON.parse(lasLineas);
        for(var l=0; l<jLineas.length; l++){            
            (function(){
                var parametros = ''{"sp":"pActualizaLineaTraspasoLote"''
                    + '',"pedido":"'' + gPedido + ''"''
                    + '',"serie":"'' + trsps_serie + ''"''
                    + '',"articulo":"'' + trsps_articulo + ''"''
                    + '',"almacen":"'' + Almacen + ''"''
                    + '',"linea":"'' + trsps_linea + ''"''
                    + '',"lote":"'' + jLineas[l].lote + ''"''
                    + '',"peso":"'' + jLineas[l].peso + ''"''
                    + '',"stock":"'' + jLineas[l].stock + ''"''
                    + '',"unidades":"'' + jLineas[l].unidades + ''"''
                    + '',"cajas":"'' + parseInt(jLineas[l].cajas) + ''"''
                    + '','' + params
                    + ''}'';
                flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {
                    if (ret) {
                        if (ret.JSCode === "KO!") { alert("Error SQL:\r" + resp[1]); return; }
                        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dv_LoteEntrada_" + gLoteID).html("<div class=''avRojo''>Recuperando los datos...</div>");
                        recargar();
                    } else { alert(''Error SP pActualizaLineaTraspasoLote!'' + JSON.stringify(ret)); }
                }, false);
            })(l)
        }
    }

    function traspasarSinLote() {
        var cntlStock, elStock, stockKO = false;
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbCabaceraLotes").find(".trDatosLinea").each(function () {
            cntlStock = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("class").split(" ")[1];
            elStock = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("class").split(" ")[2];
        });

        var lasCajasStock = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'(".stockCajas").text());
        var lasUdsStock = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'(".stockUnidades").text());

        var lasCajas = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLoteCAJAS").val()); if (!' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLoteCAJAS").is(":visible")) { lasCajas = "0"; }
        var laCantidad = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLoteUDS").val());
        var elPeso = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLotePESO").val()); if (!' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLotePESO").is(":visible")) { elPeso = "0.000"; }

        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLotePESO").is(":visible") && parseFloat(elPeso) === 0.000) { alert("Debes especificar el PESO!"); return; }

        if (cntlStock === "0") {
            if ((parseFloat(lasCajasStock) < parseFloat(lasCajas)) || (parseFloat(lasUdsStock) < parseFloat(laCantidad))) {
                alert("No hay suficiente Stock del artículo!"); stockKO = true;
            }
        }
        if (stockKO) { return; }

        if (laCantidad === "" || elPeso === "") { alert("Verifica los campos!"); return; }
        if (isNaN(laCantidad) || isNaN(elPeso)) { alert("Ups! Parece que los datos no son correctos!"); return; }

        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dv_SinLote_" + gID).html("<div class=''avRojo''>Actualizando los datos...</div>");

        var parametros = ''{"sp":"pActualizaLineaTraspasoSinLote"''
            + '',"pedido":"'' + gPedido + ''"''
            + '',"serie":"'' + trsps_serie + ''"''
            + '',"lote":"-"''
            + '',"articulo":"'' + trsps_articulo + ''"''
            + '',"cajas":"'' + ifNull(lasCajas,0) + ''"''
            + '',"unidades":"'' + ifNull(laCantidad,0) + ''"''
            + '',"peso":"'' + ifNull(elPeso,0.000) + ''"''
            + '',"linea":"'' + trsps_linea + ''"''
            + ''}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {
            if (ret) {
                var js = JSON.parse(limpiarCadena(ret.JSCode));
                if (js.respuesta === "KO!") {
                    abrirVelo("Error en el Traspaso!");
                    recargar();
                    return;
                }
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#dv_LoteEntrada_" + gLoteID).html(infoCarga("Recuperando los datos..."));
                recargar();
            } else { alert(''Error SP pActualizaLineaTraspasoSinLote!'' + JSON.stringify(ret)); }
        }, false);
    }
    
    function traspasarPedido() {
        if(!' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosTraspaso img").hasClass("btnAlo")){return;}
        // Verificar tabla [MI_Traspaso_PV].[dbo].[EnUso]
        if (contadorTraspasoEnUso === 0) {
            abrirVelo(infoCarga("verificando el pedido..."));
            var parametros = ''{"sp":"pTraspasarPV","numero":"''+trsps_numero+''","letra":"''+trsps_serie+''",''+params+''}''; 
            flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
                var js = JSON.parse(limpiarCadena(ret.JSCode));

                // Verificar EnUso
                if (js["msj"] === "enUso" && contadorTraspasoEnUso < 10) {
                    setTimeout(()=>{ contadorTraspasoEnUso++; traspasarPedido(); }, 200);
                }
                else if (js["msj"] === "enUso" && contadorTraspasoEnUso === 10) {
                    contadorTraspasoEnUso = 0;
                    abrirVelo("Se ha superado el número de intentos.<br>La tabla en uso devuelve True."
                        + "<br>Puedes forzar el estado."
                        + "<br>Si el problema persiste contacta con el administrador del sistema."
                        + "<br><br><br><span class=''boton esq05'' onclick=''forzarEstadoEnUso();''>Forzar Estado</span>"
                        + "&nbsp;&nbsp;&nbsp;<span class=''boton esq05'' onclick=''cerrarVelo();''>cancelar</span><br><br>");
                }
                else {                    
                    // Verificar Lotes
                    if (js["msj"] === "verifLotes"){
                        var losArts = "<table id=''tbArtSinLoteAsignado'' class=''tbStd L''>"
                            + "		<tr>"
                            + "			<th>Código</th>"
                            + "			<th>Nombre</th>"
                            + "		</tr>";
                        for (var i in js) { losArts += ''<tr><td>'' + js[i].CODIGO + ''</td><td>'' + js[i].NOMBRE + ''</td></tr>''; }
                        losArts += "</table>";
                        abrirVelo("Los siguientes artículos trabajan con lote y no tienen ninguno asignado!<br><br>"
                                    +"Entre dentro del artículo indicado del pedido, cancele la preparación y vuelva a asignar el lote para poder continuar con esta operación!!"
                                    + "<br><br>" + losArts
                                    + "<br><br><br><span class=''boton esq05'' onclick=''cerrarVelo();''>aceptar</span><br><br>");
                    }else{
                        // Traspaso del Pedido
                        abrirVelo(
                              "<div id=''avTraspasoDef'' class=''esq10'' style=''width:100%; margin:auto;;border:2px solid #666; padding:10px; text-align:center;''>"
                            + "   <span style=''font:bold 20px arial; color:#07657e;''>Traspaso del Pedido " + gPedido + "</span>"
                            + "</div>"
                            + "<br>"
                            +"<div style=''background:#666; padding:10px;'' class=''esqU10''>"
                            + "     <span style=''font:bold 16px arial; color:#FFF;text-align:left;''>CLIENTE &nbsp;&nbsp;&nbsp;</span>"
                            + "     <span class=''esqU10'' style=''font:bold 16px arial; color:#FFF;text-align:left;''>" + js[0].datosCliente[0].codigo + " - " + ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[0].datosCliente[0].nombre) + "</span>"
                            + "</div>"
                            + "<div class=''dvTPVClienteDatos'' style=''background:rgb(235,235,235); padding:20px;'' class=''esqD10''>"
                            + "     <div class=''CliDatos''>"
                            + "         <div style=''font:14px arial; color:#666;text-align:left;''>Dirección</div>"
                            + "         <div style=''background:#FFF; margin:1px; padding:5px; font:bold 16px arial; color:#333;text-align:left;''>" + js[0].datosCliente[0].direccion + "</div>"
                            + "         <div style=''margin-top:5px; font:14px arial; color:#666;text-align:left;''>Población</div>"
                            + "         <div style=''background:#FFF; margin:1px; padding:5px; font:bold 16px arial; color:#333;text-align:left;''>" + js[0].datosCliente[0].poblacion + "</div>"
                            + "         <div style=''margin-top:5px; font:14px arial; color:#666;text-align:left;''>Provincia</div>"
                            + "         <div style=''background:#FFF; margin:1px; padding:5px; font:bold 16px arial; color:#333;text-align:left;''>" + js[0].datosCliente[0].cp + " " + js[0].datosCliente[0].provincia + "</div>"
                            + "         <div style=''margin-top:5px; font:14px arial; color:#666;text-align:left;''>Conductor</div>"
                            + "         <div style=''background:#FFF; margin:1px; padding:5px; font:bold 16px arial; color:#333;text-align:left;''>" + js[0].datosCliente[0].agencia_nombre + "</div>"
                            + "         <div style=''margin-top:5px; font:14px arial; color:#666;text-align:left;''>Peso</div>"
                            + "         <div style=''background:#FFF; margin:1px; padding:5px; font:bold 16px arial; color:#333;text-align:left;''>" + parseFloat(js[0].pesoTotal).toFixed(3) + "</div>"
                            + "     </div>"
                            + "     <div class=''Botones''>"
                            + "         <div style=''font:16px arial; color:#333;''>Nº Bultos</div>"
                            + "         <input type=''text'' id=''nBultos'' onkeyup=''replicarBultos()'' class=''tecNum esq05'' style=''width:100px; text-align:center; background:#f2f2f2; padding:8px; font:bold 20px arial; color:#11698E;'' />"
                            + "		    <br><br>"
                            + "		    <div style=''font:16px arial; color:#333;''>" + js[0].etiEtiquetas + "</div>"
                            + "         <input type=''text'' id=''nCajas'' class=''tecNum esq05'' style=''width:100px; text-align:center; background:#f2f2f2; padding:8px; font:bold 20px arial; color:#11698E;'' />"
                            + "		    <br><br><br>"
                            + "         <img id=''btnImpAlb'' src=''./Merlos/images/btnImprimirAlbaranI.png'' class=''imgImp alb''> "
                            + "         <img id=''btnImpEti'' src=''./Merlos/images/btnImprimirEtiquetasI.png'' class=''imgImp eti'' style=''margin-left:20px;''> "
                            + "     </div>"
                            + "</div>"
                            + " <div id=''GenerarAlbaranBotonera'' style=''margin-top:40px; text-align:center;''>"
                            + "     <span class=''btnMRojo esq05'' onclick=''cerrarVelo()''>CANCELAR</span>"
                            + "     <span id=''btnGenerarAlbaran'' class=''btnMVerde esq05'' style=''margin-left:30px;'' onclick=''generarAlbaranDeVenta()''>GENERAR ALBARÁN</span>"
                            + " </div>"
                            + "<div id=''avGenerar'' style=''display:none;''>"
                            + "		<div id=''avGenerarTxt'' style=''background:#4BFE78; padding:20px; font:bold 16px arial; color:#07657e;''>"+icoCarga16+" traspasando el pedido. Por favor, espera...</div>"
                            + "</div>"
                        );
                        ' + convert(nvarchar(max),NCHAR(36)) + N'("#nBultos").focus();
                        if (js[0].impAlb === "0") {
                            imprimirAlbaran = 0;
                            ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnImpAlb").attr("src", "./Merlos/images/btnImprimirAlbaranO.png");
                            ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnGenerarAlbaran").html("IMPRIMIR ETIQUETAS");
                        } else { imprimirAlbaran = 1; }
                        activarObjetos();
                    }
                }
            } else { alert(''Error SP pTraspasarPV!''+JSON.stringify(ret)); }}, false);
        }
    }

    function replicarBultos(){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#nCajas").val(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#nBultos").val())); }

    function generarAlbaranDeVenta(){ 
        var losBultos = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#nBultos").val());
        if(losBultos===""){ alert("Debes especificar el número de bultos!"); return; }
        if(isNaN(losBultos)){ alert("Ups! Parece que el campo BULTOS no es correcto!"); return; }  
        
        var lasCajas = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#nCajas").val());
        if(lasCajas===""){ alert("Debes especificar el número de cajas!"); return; }
        if(isNaN(lasCajas)){ alert("Ups! Parece que el campo CAJAS no es correcto!"); return; }  

        ' + convert(nvarchar(max),NCHAR(36)) + N'("#GenerarAlbaranBotonera").hide();
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#avGenerar").fadeIn();
        if(FacturaDirecta==="true"){FacturaDirecta=1;}else{FacturaDirecta=0;}
        var parametros = ''{"sp":"pAlbaranDeVenta","numero":"''+trsps_numero+''","letra":"''+trsps_serie+''","bultos":"''+losBultos+''"''
            +'',"cajas":"''+lasCajas+''","impAlb":"''+imprimirAlbaran+''","impEti":"''+imprimirEtiquetas+''","FacturaDirecta":''+FacturaDirecta+'',''+params+''}'';
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
            if(ret.JSCode!=="ERROR!"){
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#avGenerarTxt").html("se ha generado el albarán "+ret.JSCode);
                setTimeout(function(){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnMerlosHome").click(); cerrarVelo();  buscarGlobal(); },2000);
            }else{ alert("Error al generar el albarán de venta:\n"+ret.JSCode); buscarGlobal(); }
        } else { alert(''Error SP pSeries!''+JSON.stringify(ret)); }}, false);
    }    

function calcularModif(i,modo,unicaja,unipeso){
	var cajas = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#tbModifPedido #inpCajas"+i).val());
	var uds = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#tbModifPedido #inpUds"+i).val());
	var peso = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#tbModifPedido #inpPeso"+i).val());
	if(modo==="cajas"){ uds = cajas*parseInt(unicaja); if(parseFloat(unipeso)>0){ peso = uds*parseFloat(unipeso); } }
	if(modo==="uds"){ if(parseFloat(unipeso)>0){ peso = uds*parseFloat(unipeso); } }
	' + convert(nvarchar(max),NCHAR(36)) + N'("#inpCajas"+i).val(cajas);
	' + convert(nvarchar(max),NCHAR(36)) + N'("#inpUds"+i).val(uds);
	' + convert(nvarchar(max),NCHAR(36)) + N'("#inpPeso"+i).val(peso);
}

function intercambiarArticulo(i, liniaArtOrig, artOrig, defiOrig){ 
    if(!' + convert(nvarchar(max),NCHAR(36)) + N'("#divIntercambio").is(":visible")){
    ' + convert(nvarchar(max),NCHAR(36)) + N'("#divIntercambio").remove();
        abrirVelo(
            "<div id=''divIntercambio'' style=''background:#FFF; max-height:500px; overflow:hidden; overflow-y:auto;''>"
            +icoCarga16+" cargando artículos..."
            +"</div>"
        );	
    }	
	if(listadoArticulos===""){ cargarArticulos(Almacen); setTimeout(function(){intercambiarArticulo(i, liniaArtOrig, artOrig, defiOrig);},500); return; }
	setTimeout(function(){
		var contenido =  "<div class=''poderCerrarVelo''>Selecciona el artículo para sustituir a<br>"+artOrig+" - "+defiOrig+"</div>"
						+"<div style=''margin:10px 0;''><input type=''text'' id=''inpBuscArt'' style=''width:100%;'' placeholder=''buscar artículo'' onkeyup=''buscarEnTabla(this.id,\"tbListadoDeArticulos\")''></div>"
						+"<div style=''max-height:400px; overflow:hidden; overflow-y:auto;''>"
                        +"  <table id=''tbListadoDeArticulos'' class=''tbStd''>"
						+"	    <tr>"
						+"		    <th>Almacen</th>"
						+"		    <th>Código</th>"
						+"		    <th>Descripción</th>"
						+"		    <th>Stock</th>"
						+"	    </tr>";
		var js = JSON.parse(listadoArticulos);
		for(var j in js.datos){ 
			if(parseFloat(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].ArticuloStock))>0){
				contenido += "  <tr class=''tbData buscarEn'' onclick=''seleccionarArticuloIntercambio(\""+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].CODIGO)+"\",\""+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].NOMBRE)+"\",\""+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].ArticuloStock)+"\",\""+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].UNICAJA)+"\",\""+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].PESO)+"\","+i+","+liniaArtOrig+",\""+artOrig+"\",\""+defiOrig+"\")''>"
							+"	    <td>"+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].almacen)+"</td>"
							+"	    <td class=''buscarEn''>"+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].CODIGO)+"</td>"
							+"	    <td class=''buscarEn''>"+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].NOMBRE)+"</td>"
							+"	    <td class=''taR''>"+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].ArticuloStock)+"</td>"
							+"  </tr>"; 
			}
		}
		contenido += "</table></div>"
		' + convert(nvarchar(max),NCHAR(36)) + N'("#divIntercambio").html(contenido);
		' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBuscArt").focus();
	},500);
}

function cargarArticulos(almacen){ 
	var parametros = ''{"sp":"pArticulos","modo":"lista","almacen":"''+almacen+''"}'';	
    flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		listadoArticulos=limpiarCadena(ret.JSCode);
    } else { alert(''Error SP pArticulos - lista!''+JSON.stringify(ret)); }}, false);
}

function seleccionarArticuloIntercambio(artDest,defiDest,stockDest,unicajaDest,pesoDest,i,liniaOrig,artOrig,defiOrig){
    abrirVelo(icoCarga16+" asignando el artículo...");
	
	var mpl = {"Empresa":Empresa,"Numero":Numero,"Serie":Serie,"liniaArtOrig":liniaOrig
			,"Ruta":Ruta,"Almacen":Almacen,"pedido":PedidoActivo,"Cliente":' + convert(nvarchar(max),NCHAR(36)) + N'("#spCodCli").text()
			,"artDest":artDest,"defiDest":defiDest,"stockDest":stockDest,"unicajaDest":unicajaDest,"pesoDest":pesoDest
            ,"articuloOrig":artOrig,"defiOrig":defiOrig};
			
	modificarPedidoListado.push(mpl);
    // pintar modificaciones 
    ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasDePedidoTraspaso").find(".tbLineasDePedidoTraspasoTR").each(function () {        
        var trId = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("id"); 
        var articulo = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("data-articulo"); 
        for(var i in modificarPedidoListado){
            if(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(modificarPedidoListado[i].articuloOrig)===' + convert(nvarchar(max),NCHAR(36)) + N'.trim(articulo)){
                ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDarticuloPte").html(modificarPedidoListado[i].artDest);
                ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tdDefinicion").html(
                    modificarPedidoListado[i].defiDest
                    +"<br><span style=''font:12px arial;color:#FF5733;''>ORIGINAL: "+modificarPedidoListado[i].articuloOrig+" - "+modificarPedidoListado[i].defiOrig+"</span>"
                ); 
                // cambiamos imagen de la linea - añadimos inputs de cajas, unidades y peso
                ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbAccion").css("background","url(''./Merlos/images/Modificado.png'') no-repeat center").css("background-size","90%").parent(); 
                var CajasDisplay = " display:none; ";
                var PesoDisplay = " display:none; ";
                if(parseFloat(unicajaDest)>0){ CajasDisplay=""; }
                if(parseFloat(pesoDest)>0){ PesoDisplay=""; }
                ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDcajasPte").html("<input type=''text'' class=''inpModifCajas'' style=''width:60px; text-align:center;"+CajasDisplay+"'' "
                                                                    +"onkeyup=''calcularModificacionesCUP(\""+trId+"\",\"cajas\",this.value,\""+unicajaDest+"\",\""+pesoDest+"\")'' />"); 
                ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDunidadesPte").html("<input type=''text'' class=''inpModifUnidades'' style=''width:60px; text-align:center;'' "
                                                                    +"onkeyup=''calcularModificacionesCUP(\""+trId+"\",\"unidades\",this.value,\""+unicajaDest+"\",\""+pesoDest+"\")'' />"); 
                ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDpesoPte").html("<input type=''text'' class=''inpModifPeso'' style=''width:60px; text-align:center;"+PesoDisplay+"'' />"); 
            }
        }
    });
    cerrarVelo();
}

function calcularModificacionesCUP(tr,modo,valor,unicaja,unipeso){
    var _uds, _cajas;
    if(modo==="cajas"){ 
        _uds = parseInt(parseInt(valor)*parseInt(unicaja));
        if(isNaN(_uds)){ _uds=0; }
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+tr).find(".tbLineasDePedidoTraspasoTDunidadesPte").find("input").val(_uds); }
    if(modo==="unidades"){ 
        if(parseFloat(unicaja)>0){
            _uds = valor;
            _cajas = parseInt(parseInt(_uds)/parseInt(unicaja));
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+tr).find(".tbLineasDePedidoTraspasoTDcajasPte").find("input").val(_cajas); 
        }
    }
    if(parseFloat(unipeso)>0){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+tr).find(".tbLineasDePedidoTraspasoTDpesoPte").find("input").val( _uds*parseFloat(unipeso) ); }
}

function capturarPeso(img,id){
    ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+img).attr("src","./Merlos/images/btnBasculaV.png");
    ' + convert(nvarchar(max),NCHAR(36)) + N'.post("../php/traspasoPV.php",{jQueryPost:"capturarPeso",basculaEnUso:basculaEnUso}).done(function(data){
		if(data.length>10){ alert("ERROR BÁSCULA!"); ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+img).attr("src","../images/btnBascula.png"); return; }
		' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).val(data); ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+img).attr("src","../images/btnBascula.png");
    });
}

var TotalSuma=0;
function capturarPesoSuma(img,id){    
    abrirVelo(
	    "<div id=''avSumaPesos'' class=''esq10'' "+
	    "style=''width:100%; margin:30px auto;background:#daf7ff;border:2px solid #07657e;padding:10px;text-align:center;''>"+
	    "   <div><img src=''../images/btnMerlosCerrar.png'' style=''float:right; width:50px;'' onclick=''cerrarCapturaPesoSuma()''></div>"+
	    "   <div><span style=''font:bold 20px Gothic; color:#07657e;''>Suma de Pesadas</span></div>"+
	    "   <div style=''height:50px;''></div>"+
	    "   <div style=''overflow:hidden;''>"+
	    "		<div style=''float:left;''>"+
	    "			<div style=''font:bold 16px Gothic; color:#07b55a;''>PESADAS</div>"+
	    "			<br>"+
	    "			<div id=''dvPesadas'' style=''background:#FFF; padding:20px; text-align:right;''></div>"+
	    "		</div>"+
	    "		<div style=''float:left; margin-left:30px;''>"+
	    "			<div style=''font:bold 16px Gothic; color:#07b55a;''>TOTAL</div>"+
	    "			<br>"+
	    "			<div id=''dvTotalPesadas'' style=''background:#FFF; padding:20px; text-align:center;''></div>"+
	    "		</div>"+		
	    "		<div style=''float:right; margin-left:30px;''>"+
	    "			<div style=''font:bold 16px Gothic; color:#07b55a;''>TOTAL</div>"+
	    "			<div><img src=''../images/btnBascula.png''     style=''width:50px;'' onclick=''traspasarSumaPesos(\""+img+"\",\""+id+"\")''></div>"+
	    "		</div>"+
	    "		<div style=''float:right; margin-left:40px;''>"+
	    "			<div style=''font:bold 16px Gothic; color:#07b55a;''>PESAR</div>"+
	    "			<div><img id=''imgCapturaPesoSuma'' src=''../images/btnBasculaSuma.png'' style=''width:50px;'' onclick=''capturarPesoDeSuma()''></div>"+
	    "		</div>"+
	    "		<div style=''float:right;''>"+
	    "			<div style=''font:bold 16px Gothic; color:#07b55a;''>MANUAL</div>"+
	    "			<div>"+
	    "			    <input type=''text'' id=''entradaPesoManual'' style=''width:100px; text-align:center;'' class=''tecNum''>"+
	    "			</div>"+
	    "		</div>"+
	    "	</div>"+
	    "</div>"
    );
	activarObjetos();
}

function capturarPesoDeSuma(manual){
    if(manual){
		TotalSuma += parseFloat(manual);	
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvPesadas").append("<div>"+manual+"</div>");		
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTotalPesadas").html("<div style=''font:bold 20px Gothic; color:#07b55a;''>"+TotalSuma.toFixed(3)+"</div>");	
		' + convert(nvarchar(max),NCHAR(36)) + N'("#entradaPesoManual").val("");
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTeclado").slideUp();
    }else{ 
		' + convert(nvarchar(max),NCHAR(36)) + N'("#imgCapturaPesoSuma").attr("src","../images/btnBasculaSumaV.png");
		' + convert(nvarchar(max),NCHAR(36)) + N'.post("../php/traspasoPV.php",{jQueryPost:"capturarPeso"}).done(function(data){
			if(data.length>30){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#imgCapturaPesoSuma").attr("src","../images/btnBasculaSuma.png"); return; }
			TotalSuma += parseFloat(data);
			' + convert(nvarchar(max),NCHAR(36)) + N'("#dvPesadas").append("<div>"+data+"</div>");		
			' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTotalPesadas").html("<div style=''font:bold 20px Gothic; color:#07b55a;''>"+TotalSuma.toFixed(3)+"</div>");		
			' + convert(nvarchar(max),NCHAR(36)) + N'("#imgCapturaPesoSuma").attr("src","../images/btnBasculaSuma.png");
		});
    }
}

function traspasarSumaPesos(img,id){
	' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).val(TotalSuma);	
	cerrarCapturaPesoSuma();
}

function cerrarCapturaPesoSuma(){
	' + convert(nvarchar(max),NCHAR(36)) + N'("#dvPesadas").html("");
	TotalSuma = 0.000;
	cerrarVelo();
}

function finalizarPedido(confirmacion){
    if(!' + convert(nvarchar(max),NCHAR(36)) + N'(".btnFinalizarPedido img").hasClass("btnAlo")){ return; } 
    if(confirmacion){
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#veloAvDesbloqueo").html(icoAlert50+"<br><br>finalizando...");
        var parametros = ''{"sp":"pPedidos","modo":"finalizarPedido","Empresa":"''+Empresa+''","Numero":"''+Numero+''","Serie":"''+Serie+''"}'';
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion").hide(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTrabajoInputs, #dvResultados").slideDown();
            cancelarPreparacion(); modificacionesCancelar(); cambiarArticuloCancelar(); buscarGlobal(); cerrarVelo();
        } else { alert(''Error SP pPedidos - finalizarPedido!''+JSON.stringify(ret)); }}, false);
    }else{
        abrirVelo(`
            <div id=''veloAvDesbloqueo'' class=''taC'' style=''font:16px arial; color:#333;''>
                `+icoAlert50+` <br><br>
                El pedido `+Numero+`-`+Serie+`<br>se marcará como TRASPASADO<br>en la base de datos
                <br><br><br><span class=''btnVerde esq05'' onclick=''finalizarPedido(1);''>aceptar</span>
                &nbsp;&nbsp;&nbsp;<span class=''btnRojo esq05'' onclick=''cerrarVelo();''>cancelar</span>
            </div>
        `);
    }
}
</script>
',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'noicon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,0,0,1)
) AS Source ([ModuleName],[TypeId],[ClassId],[ObjectName],[ObjectFilter],[Descrip],[Title],[ContainerId],[CollapsibleButton],[FullscreenButton],[RefreshButton],[SearchButton],[SQlSentence],[Header],[HTMLText],[Footer],[Empty],[CssText],[ScriptText],[ChartTypeId],[ChartSettingName],[Series],[Labels],[Value],[Params],[JsonOptions],[Path],[TransFormFilePath],[IconName],[PagerId],[PageSize],[ConnStringID],[ToolbarName],[GridbarName],[TemplateId],[HeaderClass],[ModuleClass],[JSAfterLoad],[Searcher],[ShowWhenNew],[ManualInit],[SchedulerName],[TimelineSettingName],[KanbanSettingsName],[ChartBackground],[ChartBorder],[Reserved],[Cache],[Offline],[PresetName],[MixedChartTypes],[MixedChartLabels],[ChartLineBorderDash],[ChartLineFill],[OriginId])
ON (Target.[ModuleName] = Source.[ModuleName])
WHEN MATCHED AND (
	NULLIF(Source.[TypeId], Target.[TypeId]) IS NOT NULL OR NULLIF(Target.[TypeId], Source.[TypeId]) IS NOT NULL OR 
	NULLIF(Source.[ClassId], Target.[ClassId]) IS NOT NULL OR NULLIF(Target.[ClassId], Source.[ClassId]) IS NOT NULL OR 
	NULLIF(Source.[ObjectName], Target.[ObjectName]) IS NOT NULL OR NULLIF(Target.[ObjectName], Source.[ObjectName]) IS NOT NULL OR 
	NULLIF(Source.[ObjectFilter], Target.[ObjectFilter]) IS NOT NULL OR NULLIF(Target.[ObjectFilter], Source.[ObjectFilter]) IS NOT NULL OR 
	NULLIF(Source.[Descrip], Target.[Descrip]) IS NOT NULL OR NULLIF(Target.[Descrip], Source.[Descrip]) IS NOT NULL OR 
	NULLIF(Source.[Title], Target.[Title]) IS NOT NULL OR NULLIF(Target.[Title], Source.[Title]) IS NOT NULL OR 
	NULLIF(Source.[ContainerId], Target.[ContainerId]) IS NOT NULL OR NULLIF(Target.[ContainerId], Source.[ContainerId]) IS NOT NULL OR 
	NULLIF(Source.[CollapsibleButton], Target.[CollapsibleButton]) IS NOT NULL OR NULLIF(Target.[CollapsibleButton], Source.[CollapsibleButton]) IS NOT NULL OR 
	NULLIF(Source.[FullscreenButton], Target.[FullscreenButton]) IS NOT NULL OR NULLIF(Target.[FullscreenButton], Source.[FullscreenButton]) IS NOT NULL OR 
	NULLIF(Source.[RefreshButton], Target.[RefreshButton]) IS NOT NULL OR NULLIF(Target.[RefreshButton], Source.[RefreshButton]) IS NOT NULL OR 
	NULLIF(Source.[SearchButton], Target.[SearchButton]) IS NOT NULL OR NULLIF(Target.[SearchButton], Source.[SearchButton]) IS NOT NULL OR 
	NULLIF(Source.[SQlSentence], Target.[SQlSentence]) IS NOT NULL OR NULLIF(Target.[SQlSentence], Source.[SQlSentence]) IS NOT NULL OR 
	NULLIF(Source.[Header], Target.[Header]) IS NOT NULL OR NULLIF(Target.[Header], Source.[Header]) IS NOT NULL OR 
	NULLIF(Source.[HTMLText], Target.[HTMLText]) IS NOT NULL OR NULLIF(Target.[HTMLText], Source.[HTMLText]) IS NOT NULL OR 
	NULLIF(Source.[Footer], Target.[Footer]) IS NOT NULL OR NULLIF(Target.[Footer], Source.[Footer]) IS NOT NULL OR 
	NULLIF(Source.[Empty], Target.[Empty]) IS NOT NULL OR NULLIF(Target.[Empty], Source.[Empty]) IS NOT NULL OR 
	NULLIF(Source.[CssText], Target.[CssText]) IS NOT NULL OR NULLIF(Target.[CssText], Source.[CssText]) IS NOT NULL OR 
	NULLIF(Source.[ScriptText], Target.[ScriptText]) IS NOT NULL OR NULLIF(Target.[ScriptText], Source.[ScriptText]) IS NOT NULL OR 
	NULLIF(Source.[ChartTypeId], Target.[ChartTypeId]) IS NOT NULL OR NULLIF(Target.[ChartTypeId], Source.[ChartTypeId]) IS NOT NULL OR 
	NULLIF(Source.[ChartSettingName], Target.[ChartSettingName]) IS NOT NULL OR NULLIF(Target.[ChartSettingName], Source.[ChartSettingName]) IS NOT NULL OR 
	NULLIF(Source.[Series], Target.[Series]) IS NOT NULL OR NULLIF(Target.[Series], Source.[Series]) IS NOT NULL OR 
	NULLIF(Source.[Labels], Target.[Labels]) IS NOT NULL OR NULLIF(Target.[Labels], Source.[Labels]) IS NOT NULL OR 
	NULLIF(Source.[Value], Target.[Value]) IS NOT NULL OR NULLIF(Target.[Value], Source.[Value]) IS NOT NULL OR 
	NULLIF(Source.[Params], Target.[Params]) IS NOT NULL OR NULLIF(Target.[Params], Source.[Params]) IS NOT NULL OR 
	NULLIF(Source.[JsonOptions], Target.[JsonOptions]) IS NOT NULL OR NULLIF(Target.[JsonOptions], Source.[JsonOptions]) IS NOT NULL OR 
	NULLIF(Source.[Path], Target.[Path]) IS NOT NULL OR NULLIF(Target.[Path], Source.[Path]) IS NOT NULL OR 
	NULLIF(Source.[TransFormFilePath], Target.[TransFormFilePath]) IS NOT NULL OR NULLIF(Target.[TransFormFilePath], Source.[TransFormFilePath]) IS NOT NULL OR 
	NULLIF(Source.[IconName], Target.[IconName]) IS NOT NULL OR NULLIF(Target.[IconName], Source.[IconName]) IS NOT NULL OR 
	NULLIF(Source.[PagerId], Target.[PagerId]) IS NOT NULL OR NULLIF(Target.[PagerId], Source.[PagerId]) IS NOT NULL OR 
	NULLIF(Source.[PageSize], Target.[PageSize]) IS NOT NULL OR NULLIF(Target.[PageSize], Source.[PageSize]) IS NOT NULL OR 
	NULLIF(Source.[ConnStringID], Target.[ConnStringID]) IS NOT NULL OR NULLIF(Target.[ConnStringID], Source.[ConnStringID]) IS NOT NULL OR 
	NULLIF(Source.[ToolbarName], Target.[ToolbarName]) IS NOT NULL OR NULLIF(Target.[ToolbarName], Source.[ToolbarName]) IS NOT NULL OR 
	NULLIF(Source.[GridbarName], Target.[GridbarName]) IS NOT NULL OR NULLIF(Target.[GridbarName], Source.[GridbarName]) IS NOT NULL OR 
	NULLIF(Source.[TemplateId], Target.[TemplateId]) IS NOT NULL OR NULLIF(Target.[TemplateId], Source.[TemplateId]) IS NOT NULL OR 
	NULLIF(Source.[HeaderClass], Target.[HeaderClass]) IS NOT NULL OR NULLIF(Target.[HeaderClass], Source.[HeaderClass]) IS NOT NULL OR 
	NULLIF(Source.[ModuleClass], Target.[ModuleClass]) IS NOT NULL OR NULLIF(Target.[ModuleClass], Source.[ModuleClass]) IS NOT NULL OR 
	NULLIF(Source.[JSAfterLoad], Target.[JSAfterLoad]) IS NOT NULL OR NULLIF(Target.[JSAfterLoad], Source.[JSAfterLoad]) IS NOT NULL OR 
	NULLIF(Source.[Searcher], Target.[Searcher]) IS NOT NULL OR NULLIF(Target.[Searcher], Source.[Searcher]) IS NOT NULL OR 
	NULLIF(Source.[ShowWhenNew], Target.[ShowWhenNew]) IS NOT NULL OR NULLIF(Target.[ShowWhenNew], Source.[ShowWhenNew]) IS NOT NULL OR 
	NULLIF(Source.[ManualInit], Target.[ManualInit]) IS NOT NULL OR NULLIF(Target.[ManualInit], Source.[ManualInit]) IS NOT NULL OR 
	NULLIF(Source.[SchedulerName], Target.[SchedulerName]) IS NOT NULL OR NULLIF(Target.[SchedulerName], Source.[SchedulerName]) IS NOT NULL OR 
	NULLIF(Source.[TimelineSettingName], Target.[TimelineSettingName]) IS NOT NULL OR NULLIF(Target.[TimelineSettingName], Source.[TimelineSettingName]) IS NOT NULL OR 
	NULLIF(Source.[KanbanSettingsName], Target.[KanbanSettingsName]) IS NOT NULL OR NULLIF(Target.[KanbanSettingsName], Source.[KanbanSettingsName]) IS NOT NULL OR 
	NULLIF(Source.[ChartBackground], Target.[ChartBackground]) IS NOT NULL OR NULLIF(Target.[ChartBackground], Source.[ChartBackground]) IS NOT NULL OR 
	NULLIF(Source.[ChartBorder], Target.[ChartBorder]) IS NOT NULL OR NULLIF(Target.[ChartBorder], Source.[ChartBorder]) IS NOT NULL OR 
	NULLIF(Source.[Reserved], Target.[Reserved]) IS NOT NULL OR NULLIF(Target.[Reserved], Source.[Reserved]) IS NOT NULL OR 
	NULLIF(Source.[Cache], Target.[Cache]) IS NOT NULL OR NULLIF(Target.[Cache], Source.[Cache]) IS NOT NULL OR 
	NULLIF(Source.[Offline], Target.[Offline]) IS NOT NULL OR NULLIF(Target.[Offline], Source.[Offline]) IS NOT NULL OR 
	NULLIF(Source.[PresetName], Target.[PresetName]) IS NOT NULL OR NULLIF(Target.[PresetName], Source.[PresetName]) IS NOT NULL OR 
	NULLIF(Source.[MixedChartTypes], Target.[MixedChartTypes]) IS NOT NULL OR NULLIF(Target.[MixedChartTypes], Source.[MixedChartTypes]) IS NOT NULL OR 
	NULLIF(Source.[MixedChartLabels], Target.[MixedChartLabels]) IS NOT NULL OR NULLIF(Target.[MixedChartLabels], Source.[MixedChartLabels]) IS NOT NULL OR 
	NULLIF(Source.[ChartLineBorderDash], Target.[ChartLineBorderDash]) IS NOT NULL OR NULLIF(Target.[ChartLineBorderDash], Source.[ChartLineBorderDash]) IS NOT NULL OR 
	NULLIF(Source.[ChartLineFill], Target.[ChartLineFill]) IS NOT NULL OR NULLIF(Target.[ChartLineFill], Source.[ChartLineFill]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [TypeId] = Source.[TypeId], 
  [ClassId] = Source.[ClassId], 
  [ObjectName] = Source.[ObjectName], 
  [ObjectFilter] = Source.[ObjectFilter], 
  [Descrip] = Source.[Descrip], 
  [Title] = Source.[Title], 
  [ContainerId] = Source.[ContainerId], 
  [CollapsibleButton] = Source.[CollapsibleButton], 
  [FullscreenButton] = Source.[FullscreenButton], 
  [RefreshButton] = Source.[RefreshButton], 
  [SearchButton] = Source.[SearchButton], 
  [SQlSentence] = Source.[SQlSentence], 
  [Header] = Source.[Header], 
  [HTMLText] = Source.[HTMLText], 
  [Footer] = Source.[Footer], 
  [Empty] = Source.[Empty], 
  [CssText] = Source.[CssText], 
  [ScriptText] = Source.[ScriptText], 
  [ChartTypeId] = Source.[ChartTypeId], 
  [ChartSettingName] = Source.[ChartSettingName], 
  [Series] = Source.[Series], 
  [Labels] = Source.[Labels], 
  [Value] = Source.[Value], 
  [Params] = Source.[Params], 
  [JsonOptions] = Source.[JsonOptions], 
  [Path] = Source.[Path], 
  [TransFormFilePath] = Source.[TransFormFilePath], 
  [IconName] = Source.[IconName], 
  [PagerId] = Source.[PagerId], 
  [PageSize] = Source.[PageSize], 
  [ConnStringID] = Source.[ConnStringID], 
  [ToolbarName] = Source.[ToolbarName], 
  [GridbarName] = Source.[GridbarName], 
  [TemplateId] = Source.[TemplateId], 
  [HeaderClass] = Source.[HeaderClass], 
  [ModuleClass] = Source.[ModuleClass], 
  [JSAfterLoad] = Source.[JSAfterLoad], 
  [Searcher] = Source.[Searcher], 
  [ShowWhenNew] = Source.[ShowWhenNew], 
  [ManualInit] = Source.[ManualInit], 
  [SchedulerName] = Source.[SchedulerName], 
  [TimelineSettingName] = Source.[TimelineSettingName], 
  [KanbanSettingsName] = Source.[KanbanSettingsName], 
  [ChartBackground] = Source.[ChartBackground], 
  [ChartBorder] = Source.[ChartBorder], 
  [Reserved] = Source.[Reserved], 
  [Cache] = Source.[Cache], 
  [Offline] = Source.[Offline], 
  [PresetName] = Source.[PresetName], 
  [MixedChartTypes] = Source.[MixedChartTypes], 
  [MixedChartLabels] = Source.[MixedChartLabels], 
  [ChartLineBorderDash] = Source.[ChartLineBorderDash], 
  [ChartLineFill] = Source.[ChartLineFill], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([ModuleName],[TypeId],[ClassId],[ObjectName],[ObjectFilter],[Descrip],[Title],[ContainerId],[CollapsibleButton],[FullscreenButton],[RefreshButton],[SearchButton],[SQlSentence],[Header],[HTMLText],[Footer],[Empty],[CssText],[ScriptText],[ChartTypeId],[ChartSettingName],[Series],[Labels],[Value],[Params],[JsonOptions],[Path],[TransFormFilePath],[IconName],[PagerId],[PageSize],[ConnStringID],[ToolbarName],[GridbarName],[TemplateId],[HeaderClass],[ModuleClass],[JSAfterLoad],[Searcher],[ShowWhenNew],[ManualInit],[SchedulerName],[TimelineSettingName],[KanbanSettingsName],[ChartBackground],[ChartBorder],[Reserved],[Cache],[Offline],[PresetName],[MixedChartTypes],[MixedChartLabels],[ChartLineBorderDash],[ChartLineFill],[OriginId])
 VALUES(Source.[ModuleName],Source.[TypeId],Source.[ClassId],Source.[ObjectName],Source.[ObjectFilter],Source.[Descrip],Source.[Title],Source.[ContainerId],Source.[CollapsibleButton],Source.[FullscreenButton],Source.[RefreshButton],Source.[SearchButton],Source.[SQlSentence],Source.[Header],Source.[HTMLText],Source.[Footer],Source.[Empty],Source.[CssText],Source.[ScriptText],Source.[ChartTypeId],Source.[ChartSettingName],Source.[Series],Source.[Labels],Source.[Value],Source.[Params],Source.[JsonOptions],Source.[Path],Source.[TransFormFilePath],Source.[IconName],Source.[PagerId],Source.[PageSize],Source.[ConnStringID],Source.[ToolbarName],Source.[GridbarName],Source.[TemplateId],Source.[HeaderClass],Source.[ModuleClass],Source.[JSAfterLoad],Source.[Searcher],Source.[ShowWhenNew],Source.[ManualInit],Source.[SchedulerName],Source.[TimelineSettingName],Source.[KanbanSettingsName],Source.[ChartBackground],Source.[ChartBorder],Source.[Reserved],Source.[Cache],Source.[Offline],Source.[PresetName],Source.[MixedChartTypes],Source.[MixedChartLabels],Source.[ChartLineBorderDash],Source.[ChartLineFill],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO








BEGIN TRY

MERGE INTO [Navigation_Nodes] AS Target
USING (VALUES
  (N'1A1C5F16-FA0F-4067-9788-43E94C4AA49B',N'502DC12A-C032-407C-AD4E-372A476E7309',8,N'MERLOS',N'admin',N'MERLOS',N'group',NULL,NULL,N'current',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,NULL,1)
 ,(N'62D8EC4D-A0EE-4939-83A1-81D8351621D1',N'1A1C5F16-FA0F-4067-9788-43E94C4AA49B',0,N'Configuración',N'settings',N'Configuración',N'page',NULL,NULL,N'current',NULL,NULL,N'Merlos_Administrador',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,NULL,1)
 ,(N'A021EB9E-4B77-4DAD-AB7B-D6F7014682FA',N'502DC12A-C032-407C-AD4E-372A476E7309',9,N'Impresión',N'printer-2',N'Impresión',N'page',NULL,NULL,N'current',NULL,NULL,N'Merlos_Impresion',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,NULL,1)
) AS Source ([NodeId],[ParentNodeId],[Order],[Title],[IconName],[Descrip],[TypeId],[Params],[Url],[TargetId],[ProcessName],[PageTypeId],[PageName],[ReportName],[HelpId],[ReportWhere],[ObjectName],[ObjectWhere],[Defaults],[SQLSentence],[SQLConStringId],[WebComponent],[TableName],[BadgeClass],[BadgeSQL],[BadgeConStringId],[BadgeRefresh],[Enabled],[cssClass],[OriginId])
ON (Target.[NodeId] = Source.[NodeId])
WHEN MATCHED AND (
	NULLIF(Source.[ParentNodeId], Target.[ParentNodeId]) IS NOT NULL OR NULLIF(Target.[ParentNodeId], Source.[ParentNodeId]) IS NOT NULL OR 
	NULLIF(Source.[Order], Target.[Order]) IS NOT NULL OR NULLIF(Target.[Order], Source.[Order]) IS NOT NULL OR 
	NULLIF(Source.[Title], Target.[Title]) IS NOT NULL OR NULLIF(Target.[Title], Source.[Title]) IS NOT NULL OR 
	NULLIF(Source.[IconName], Target.[IconName]) IS NOT NULL OR NULLIF(Target.[IconName], Source.[IconName]) IS NOT NULL OR 
	NULLIF(Source.[Descrip], Target.[Descrip]) IS NOT NULL OR NULLIF(Target.[Descrip], Source.[Descrip]) IS NOT NULL OR 
	NULLIF(Source.[TypeId], Target.[TypeId]) IS NOT NULL OR NULLIF(Target.[TypeId], Source.[TypeId]) IS NOT NULL OR 
	NULLIF(Source.[Params], Target.[Params]) IS NOT NULL OR NULLIF(Target.[Params], Source.[Params]) IS NOT NULL OR 
	NULLIF(Source.[Url], Target.[Url]) IS NOT NULL OR NULLIF(Target.[Url], Source.[Url]) IS NOT NULL OR 
	NULLIF(Source.[TargetId], Target.[TargetId]) IS NOT NULL OR NULLIF(Target.[TargetId], Source.[TargetId]) IS NOT NULL OR 
	NULLIF(Source.[ProcessName], Target.[ProcessName]) IS NOT NULL OR NULLIF(Target.[ProcessName], Source.[ProcessName]) IS NOT NULL OR 
	NULLIF(Source.[PageTypeId], Target.[PageTypeId]) IS NOT NULL OR NULLIF(Target.[PageTypeId], Source.[PageTypeId]) IS NOT NULL OR 
	NULLIF(Source.[PageName], Target.[PageName]) IS NOT NULL OR NULLIF(Target.[PageName], Source.[PageName]) IS NOT NULL OR 
	NULLIF(Source.[ReportName], Target.[ReportName]) IS NOT NULL OR NULLIF(Target.[ReportName], Source.[ReportName]) IS NOT NULL OR 
	NULLIF(Source.[HelpId], Target.[HelpId]) IS NOT NULL OR NULLIF(Target.[HelpId], Source.[HelpId]) IS NOT NULL OR 
	NULLIF(Source.[ReportWhere], Target.[ReportWhere]) IS NOT NULL OR NULLIF(Target.[ReportWhere], Source.[ReportWhere]) IS NOT NULL OR 
	NULLIF(Source.[ObjectName], Target.[ObjectName]) IS NOT NULL OR NULLIF(Target.[ObjectName], Source.[ObjectName]) IS NOT NULL OR 
	NULLIF(Source.[ObjectWhere], Target.[ObjectWhere]) IS NOT NULL OR NULLIF(Target.[ObjectWhere], Source.[ObjectWhere]) IS NOT NULL OR 
	NULLIF(Source.[Defaults], Target.[Defaults]) IS NOT NULL OR NULLIF(Target.[Defaults], Source.[Defaults]) IS NOT NULL OR 
	NULLIF(Source.[SQLSentence], Target.[SQLSentence]) IS NOT NULL OR NULLIF(Target.[SQLSentence], Source.[SQLSentence]) IS NOT NULL OR 
	NULLIF(Source.[SQLConStringId], Target.[SQLConStringId]) IS NOT NULL OR NULLIF(Target.[SQLConStringId], Source.[SQLConStringId]) IS NOT NULL OR 
	NULLIF(Source.[WebComponent], Target.[WebComponent]) IS NOT NULL OR NULLIF(Target.[WebComponent], Source.[WebComponent]) IS NOT NULL OR 
	NULLIF(Source.[TableName], Target.[TableName]) IS NOT NULL OR NULLIF(Target.[TableName], Source.[TableName]) IS NOT NULL OR 
	NULLIF(Source.[BadgeClass], Target.[BadgeClass]) IS NOT NULL OR NULLIF(Target.[BadgeClass], Source.[BadgeClass]) IS NOT NULL OR 
	NULLIF(Source.[BadgeSQL], Target.[BadgeSQL]) IS NOT NULL OR NULLIF(Target.[BadgeSQL], Source.[BadgeSQL]) IS NOT NULL OR 
	NULLIF(Source.[BadgeConStringId], Target.[BadgeConStringId]) IS NOT NULL OR NULLIF(Target.[BadgeConStringId], Source.[BadgeConStringId]) IS NOT NULL OR 
	NULLIF(Source.[BadgeRefresh], Target.[BadgeRefresh]) IS NOT NULL OR NULLIF(Target.[BadgeRefresh], Source.[BadgeRefresh]) IS NOT NULL OR 
	NULLIF(Source.[Enabled], Target.[Enabled]) IS NOT NULL OR NULLIF(Target.[Enabled], Source.[Enabled]) IS NOT NULL OR 
	NULLIF(Source.[cssClass], Target.[cssClass]) IS NOT NULL OR NULLIF(Target.[cssClass], Source.[cssClass]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [ParentNodeId] = Source.[ParentNodeId], 
  [Order] = Source.[Order], 
  [Title] = Source.[Title], 
  [IconName] = Source.[IconName], 
  [Descrip] = Source.[Descrip], 
  [TypeId] = Source.[TypeId], 
  [Params] = Source.[Params], 
  [Url] = Source.[Url], 
  [TargetId] = Source.[TargetId], 
  [ProcessName] = Source.[ProcessName], 
  [PageTypeId] = Source.[PageTypeId], 
  [PageName] = Source.[PageName], 
  [ReportName] = Source.[ReportName], 
  [HelpId] = Source.[HelpId], 
  [ReportWhere] = Source.[ReportWhere], 
  [ObjectName] = Source.[ObjectName], 
  [ObjectWhere] = Source.[ObjectWhere], 
  [Defaults] = Source.[Defaults], 
  [SQLSentence] = Source.[SQLSentence], 
  [SQLConStringId] = Source.[SQLConStringId], 
  [WebComponent] = Source.[WebComponent], 
  [TableName] = Source.[TableName], 
  [BadgeClass] = Source.[BadgeClass], 
  [BadgeSQL] = Source.[BadgeSQL], 
  [BadgeConStringId] = Source.[BadgeConStringId], 
  [BadgeRefresh] = Source.[BadgeRefresh], 
  [Enabled] = Source.[Enabled], 
  [cssClass] = Source.[cssClass], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([NodeId],[ParentNodeId],[Order],[Title],[IconName],[Descrip],[TypeId],[Params],[Url],[TargetId],[ProcessName],[PageTypeId],[PageName],[ReportName],[HelpId],[ReportWhere],[ObjectName],[ObjectWhere],[Defaults],[SQLSentence],[SQLConStringId],[WebComponent],[TableName],[BadgeClass],[BadgeSQL],[BadgeConStringId],[BadgeRefresh],[Enabled],[cssClass],[OriginId])
 VALUES(Source.[NodeId],Source.[ParentNodeId],Source.[Order],Source.[Title],Source.[IconName],Source.[Descrip],Source.[TypeId],Source.[Params],Source.[Url],Source.[TargetId],Source.[ProcessName],Source.[PageTypeId],Source.[PageName],Source.[ReportName],Source.[HelpId],Source.[ReportWhere],Source.[ObjectName],Source.[ObjectWhere],Source.[Defaults],Source.[SQLSentence],Source.[SQLConStringId],Source.[WebComponent],Source.[TableName],Source.[BadgeClass],Source.[BadgeSQL],Source.[BadgeConStringId],Source.[BadgeRefresh],Source.[Enabled],Source.[cssClass],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO








BEGIN TRY

MERGE INTO [Objects] AS Target
USING (VALUES
  (N'Merlos_Impresion',0,NULL,N'vImpresion',NULL,0,NULL,N'Merlos_Impresions',N'printer-2',NULL,0,200,N'Merlos_Impresion',0,1,1,1,1,1,1,N'standard',N'standard',N'standard',NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,NULL,0,N'DataConnectionString',1,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,0,1)
 ,(N'Merlos_Impresions',1,N'Merlos_Impresion',N'vImpresion',NULL,0,NULL,N'Merlos_Impresions',N'printer-2',NULL,0,200,N'Merlos_Impresions',0,1,1,1,1,1,1,N'standard',N'standard',N'standard',NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,NULL,0,N'DataConnectionString',1,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,0,1)
) AS Source ([ObjectName],[Iscollection],[ObjectChildName],[TableName],[WhereSentence],[ConfigDB],[OrderBy],[Descrip],[IconName],[UniqueIdentifierField],[ShowDefaultMenu],[DefaultPageSize],[ParsedDescrip],[Auditable],[Active],[CanInsert],[CanUpdate],[CanDelete],[CanView],[CanPrint],[InsertType],[UpdateType],[DeleteType],[InsertProcessName],[UpdateProcessName],[DeleteProcessName],[LoadProcessName],[InsertTriggerEvent],[UpdateTriggerEvent],[DeleteTriggerEvent],[HelpId],[OverrideObjectName],[OverrideObjectWhere],[NavigateNodeId],[Clonable],[ViewKeys],[IgnoreDBRequired],[ConnStringID],[TransactionOn],[InsertFlowText],[UpdateFlowText],[DeleteFlowText],[Offline],[BeforeUpdate],[BeforeInsert],[BeforeDelete],[AfterUpdate],[AfterInsert],[AfterDelete],[Reserved],[OriginId])
ON (Target.[ObjectName] = Source.[ObjectName])
WHEN MATCHED AND (
	NULLIF(Source.[Iscollection], Target.[Iscollection]) IS NOT NULL OR NULLIF(Target.[Iscollection], Source.[Iscollection]) IS NOT NULL OR 
	NULLIF(Source.[ObjectChildName], Target.[ObjectChildName]) IS NOT NULL OR NULLIF(Target.[ObjectChildName], Source.[ObjectChildName]) IS NOT NULL OR 
	NULLIF(Source.[TableName], Target.[TableName]) IS NOT NULL OR NULLIF(Target.[TableName], Source.[TableName]) IS NOT NULL OR 
	NULLIF(Source.[WhereSentence], Target.[WhereSentence]) IS NOT NULL OR NULLIF(Target.[WhereSentence], Source.[WhereSentence]) IS NOT NULL OR 
	NULLIF(Source.[ConfigDB], Target.[ConfigDB]) IS NOT NULL OR NULLIF(Target.[ConfigDB], Source.[ConfigDB]) IS NOT NULL OR 
	NULLIF(Source.[OrderBy], Target.[OrderBy]) IS NOT NULL OR NULLIF(Target.[OrderBy], Source.[OrderBy]) IS NOT NULL OR 
	NULLIF(Source.[Descrip], Target.[Descrip]) IS NOT NULL OR NULLIF(Target.[Descrip], Source.[Descrip]) IS NOT NULL OR 
	NULLIF(Source.[IconName], Target.[IconName]) IS NOT NULL OR NULLIF(Target.[IconName], Source.[IconName]) IS NOT NULL OR 
	NULLIF(Source.[UniqueIdentifierField], Target.[UniqueIdentifierField]) IS NOT NULL OR NULLIF(Target.[UniqueIdentifierField], Source.[UniqueIdentifierField]) IS NOT NULL OR 
	NULLIF(Source.[ShowDefaultMenu], Target.[ShowDefaultMenu]) IS NOT NULL OR NULLIF(Target.[ShowDefaultMenu], Source.[ShowDefaultMenu]) IS NOT NULL OR 
	NULLIF(Source.[DefaultPageSize], Target.[DefaultPageSize]) IS NOT NULL OR NULLIF(Target.[DefaultPageSize], Source.[DefaultPageSize]) IS NOT NULL OR 
	NULLIF(Source.[ParsedDescrip], Target.[ParsedDescrip]) IS NOT NULL OR NULLIF(Target.[ParsedDescrip], Source.[ParsedDescrip]) IS NOT NULL OR 
	NULLIF(Source.[Auditable], Target.[Auditable]) IS NOT NULL OR NULLIF(Target.[Auditable], Source.[Auditable]) IS NOT NULL OR 
	NULLIF(Source.[Active], Target.[Active]) IS NOT NULL OR NULLIF(Target.[Active], Source.[Active]) IS NOT NULL OR 
	NULLIF(Source.[CanInsert], Target.[CanInsert]) IS NOT NULL OR NULLIF(Target.[CanInsert], Source.[CanInsert]) IS NOT NULL OR 
	NULLIF(Source.[CanUpdate], Target.[CanUpdate]) IS NOT NULL OR NULLIF(Target.[CanUpdate], Source.[CanUpdate]) IS NOT NULL OR 
	NULLIF(Source.[CanDelete], Target.[CanDelete]) IS NOT NULL OR NULLIF(Target.[CanDelete], Source.[CanDelete]) IS NOT NULL OR 
	NULLIF(Source.[CanView], Target.[CanView]) IS NOT NULL OR NULLIF(Target.[CanView], Source.[CanView]) IS NOT NULL OR 
	NULLIF(Source.[CanPrint], Target.[CanPrint]) IS NOT NULL OR NULLIF(Target.[CanPrint], Source.[CanPrint]) IS NOT NULL OR 
	NULLIF(Source.[InsertType], Target.[InsertType]) IS NOT NULL OR NULLIF(Target.[InsertType], Source.[InsertType]) IS NOT NULL OR 
	NULLIF(Source.[UpdateType], Target.[UpdateType]) IS NOT NULL OR NULLIF(Target.[UpdateType], Source.[UpdateType]) IS NOT NULL OR 
	NULLIF(Source.[DeleteType], Target.[DeleteType]) IS NOT NULL OR NULLIF(Target.[DeleteType], Source.[DeleteType]) IS NOT NULL OR 
	NULLIF(Source.[InsertProcessName], Target.[InsertProcessName]) IS NOT NULL OR NULLIF(Target.[InsertProcessName], Source.[InsertProcessName]) IS NOT NULL OR 
	NULLIF(Source.[UpdateProcessName], Target.[UpdateProcessName]) IS NOT NULL OR NULLIF(Target.[UpdateProcessName], Source.[UpdateProcessName]) IS NOT NULL OR 
	NULLIF(Source.[DeleteProcessName], Target.[DeleteProcessName]) IS NOT NULL OR NULLIF(Target.[DeleteProcessName], Source.[DeleteProcessName]) IS NOT NULL OR 
	NULLIF(Source.[LoadProcessName], Target.[LoadProcessName]) IS NOT NULL OR NULLIF(Target.[LoadProcessName], Source.[LoadProcessName]) IS NOT NULL OR 
	NULLIF(Source.[InsertTriggerEvent], Target.[InsertTriggerEvent]) IS NOT NULL OR NULLIF(Target.[InsertTriggerEvent], Source.[InsertTriggerEvent]) IS NOT NULL OR 
	NULLIF(Source.[UpdateTriggerEvent], Target.[UpdateTriggerEvent]) IS NOT NULL OR NULLIF(Target.[UpdateTriggerEvent], Source.[UpdateTriggerEvent]) IS NOT NULL OR 
	NULLIF(Source.[DeleteTriggerEvent], Target.[DeleteTriggerEvent]) IS NOT NULL OR NULLIF(Target.[DeleteTriggerEvent], Source.[DeleteTriggerEvent]) IS NOT NULL OR 
	NULLIF(Source.[HelpId], Target.[HelpId]) IS NOT NULL OR NULLIF(Target.[HelpId], Source.[HelpId]) IS NOT NULL OR 
	NULLIF(Source.[OverrideObjectName], Target.[OverrideObjectName]) IS NOT NULL OR NULLIF(Target.[OverrideObjectName], Source.[OverrideObjectName]) IS NOT NULL OR 
	NULLIF(Source.[OverrideObjectWhere], Target.[OverrideObjectWhere]) IS NOT NULL OR NULLIF(Target.[OverrideObjectWhere], Source.[OverrideObjectWhere]) IS NOT NULL OR 
	NULLIF(Source.[NavigateNodeId], Target.[NavigateNodeId]) IS NOT NULL OR NULLIF(Target.[NavigateNodeId], Source.[NavigateNodeId]) IS NOT NULL OR 
	NULLIF(Source.[Clonable], Target.[Clonable]) IS NOT NULL OR NULLIF(Target.[Clonable], Source.[Clonable]) IS NOT NULL OR 
	NULLIF(Source.[ViewKeys], Target.[ViewKeys]) IS NOT NULL OR NULLIF(Target.[ViewKeys], Source.[ViewKeys]) IS NOT NULL OR 
	NULLIF(Source.[IgnoreDBRequired], Target.[IgnoreDBRequired]) IS NOT NULL OR NULLIF(Target.[IgnoreDBRequired], Source.[IgnoreDBRequired]) IS NOT NULL OR 
	NULLIF(Source.[ConnStringID], Target.[ConnStringID]) IS NOT NULL OR NULLIF(Target.[ConnStringID], Source.[ConnStringID]) IS NOT NULL OR 
	NULLIF(Source.[TransactionOn], Target.[TransactionOn]) IS NOT NULL OR NULLIF(Target.[TransactionOn], Source.[TransactionOn]) IS NOT NULL OR 
	NULLIF(Source.[InsertFlowText], Target.[InsertFlowText]) IS NOT NULL OR NULLIF(Target.[InsertFlowText], Source.[InsertFlowText]) IS NOT NULL OR 
	NULLIF(Source.[UpdateFlowText], Target.[UpdateFlowText]) IS NOT NULL OR NULLIF(Target.[UpdateFlowText], Source.[UpdateFlowText]) IS NOT NULL OR 
	NULLIF(Source.[DeleteFlowText], Target.[DeleteFlowText]) IS NOT NULL OR NULLIF(Target.[DeleteFlowText], Source.[DeleteFlowText]) IS NOT NULL OR 
	NULLIF(Source.[Offline], Target.[Offline]) IS NOT NULL OR NULLIF(Target.[Offline], Source.[Offline]) IS NOT NULL OR 
	NULLIF(Source.[BeforeUpdate], Target.[BeforeUpdate]) IS NOT NULL OR NULLIF(Target.[BeforeUpdate], Source.[BeforeUpdate]) IS NOT NULL OR 
	NULLIF(Source.[BeforeInsert], Target.[BeforeInsert]) IS NOT NULL OR NULLIF(Target.[BeforeInsert], Source.[BeforeInsert]) IS NOT NULL OR 
	NULLIF(Source.[BeforeDelete], Target.[BeforeDelete]) IS NOT NULL OR NULLIF(Target.[BeforeDelete], Source.[BeforeDelete]) IS NOT NULL OR 
	NULLIF(Source.[AfterUpdate], Target.[AfterUpdate]) IS NOT NULL OR NULLIF(Target.[AfterUpdate], Source.[AfterUpdate]) IS NOT NULL OR 
	NULLIF(Source.[AfterInsert], Target.[AfterInsert]) IS NOT NULL OR NULLIF(Target.[AfterInsert], Source.[AfterInsert]) IS NOT NULL OR 
	NULLIF(Source.[AfterDelete], Target.[AfterDelete]) IS NOT NULL OR NULLIF(Target.[AfterDelete], Source.[AfterDelete]) IS NOT NULL OR 
	NULLIF(Source.[Reserved], Target.[Reserved]) IS NOT NULL OR NULLIF(Target.[Reserved], Source.[Reserved]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [Iscollection] = Source.[Iscollection], 
  [ObjectChildName] = Source.[ObjectChildName], 
  [TableName] = Source.[TableName], 
  [WhereSentence] = Source.[WhereSentence], 
  [ConfigDB] = Source.[ConfigDB], 
  [OrderBy] = Source.[OrderBy], 
  [Descrip] = Source.[Descrip], 
  [IconName] = Source.[IconName], 
  [UniqueIdentifierField] = Source.[UniqueIdentifierField], 
  [ShowDefaultMenu] = Source.[ShowDefaultMenu], 
  [DefaultPageSize] = Source.[DefaultPageSize], 
  [ParsedDescrip] = Source.[ParsedDescrip], 
  [Auditable] = Source.[Auditable], 
  [Active] = Source.[Active], 
  [CanInsert] = Source.[CanInsert], 
  [CanUpdate] = Source.[CanUpdate], 
  [CanDelete] = Source.[CanDelete], 
  [CanView] = Source.[CanView], 
  [CanPrint] = Source.[CanPrint], 
  [InsertType] = Source.[InsertType], 
  [UpdateType] = Source.[UpdateType], 
  [DeleteType] = Source.[DeleteType], 
  [InsertProcessName] = Source.[InsertProcessName], 
  [UpdateProcessName] = Source.[UpdateProcessName], 
  [DeleteProcessName] = Source.[DeleteProcessName], 
  [LoadProcessName] = Source.[LoadProcessName], 
  [InsertTriggerEvent] = Source.[InsertTriggerEvent], 
  [UpdateTriggerEvent] = Source.[UpdateTriggerEvent], 
  [DeleteTriggerEvent] = Source.[DeleteTriggerEvent], 
  [HelpId] = Source.[HelpId], 
  [OverrideObjectName] = Source.[OverrideObjectName], 
  [OverrideObjectWhere] = Source.[OverrideObjectWhere], 
  [NavigateNodeId] = Source.[NavigateNodeId], 
  [Clonable] = Source.[Clonable], 
  [ViewKeys] = Source.[ViewKeys], 
  [IgnoreDBRequired] = Source.[IgnoreDBRequired], 
  [ConnStringID] = Source.[ConnStringID], 
  [TransactionOn] = Source.[TransactionOn], 
  [InsertFlowText] = Source.[InsertFlowText], 
  [UpdateFlowText] = Source.[UpdateFlowText], 
  [DeleteFlowText] = Source.[DeleteFlowText], 
  [Offline] = Source.[Offline], 
  [BeforeUpdate] = Source.[BeforeUpdate], 
  [BeforeInsert] = Source.[BeforeInsert], 
  [BeforeDelete] = Source.[BeforeDelete], 
  [AfterUpdate] = Source.[AfterUpdate], 
  [AfterInsert] = Source.[AfterInsert], 
  [AfterDelete] = Source.[AfterDelete], 
  [Reserved] = Source.[Reserved], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([ObjectName],[Iscollection],[ObjectChildName],[TableName],[WhereSentence],[ConfigDB],[OrderBy],[Descrip],[IconName],[UniqueIdentifierField],[ShowDefaultMenu],[DefaultPageSize],[ParsedDescrip],[Auditable],[Active],[CanInsert],[CanUpdate],[CanDelete],[CanView],[CanPrint],[InsertType],[UpdateType],[DeleteType],[InsertProcessName],[UpdateProcessName],[DeleteProcessName],[LoadProcessName],[InsertTriggerEvent],[UpdateTriggerEvent],[DeleteTriggerEvent],[HelpId],[OverrideObjectName],[OverrideObjectWhere],[NavigateNodeId],[Clonable],[ViewKeys],[IgnoreDBRequired],[ConnStringID],[TransactionOn],[InsertFlowText],[UpdateFlowText],[DeleteFlowText],[Offline],[BeforeUpdate],[BeforeInsert],[BeforeDelete],[AfterUpdate],[AfterInsert],[AfterDelete],[Reserved],[OriginId])
 VALUES(Source.[ObjectName],Source.[Iscollection],Source.[ObjectChildName],Source.[TableName],Source.[WhereSentence],Source.[ConfigDB],Source.[OrderBy],Source.[Descrip],Source.[IconName],Source.[UniqueIdentifierField],Source.[ShowDefaultMenu],Source.[DefaultPageSize],Source.[ParsedDescrip],Source.[Auditable],Source.[Active],Source.[CanInsert],Source.[CanUpdate],Source.[CanDelete],Source.[CanView],Source.[CanPrint],Source.[InsertType],Source.[UpdateType],Source.[DeleteType],Source.[InsertProcessName],Source.[UpdateProcessName],Source.[DeleteProcessName],Source.[LoadProcessName],Source.[InsertTriggerEvent],Source.[UpdateTriggerEvent],Source.[DeleteTriggerEvent],Source.[HelpId],Source.[OverrideObjectName],Source.[OverrideObjectWhere],Source.[NavigateNodeId],Source.[Clonable],Source.[ViewKeys],Source.[IgnoreDBRequired],Source.[ConnStringID],Source.[TransactionOn],Source.[InsertFlowText],Source.[UpdateFlowText],Source.[DeleteFlowText],Source.[Offline],Source.[BeforeUpdate],Source.[BeforeInsert],Source.[BeforeDelete],Source.[AfterUpdate],Source.[AfterInsert],Source.[AfterDelete],Source.[Reserved],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO









BEGIN TRY

MERGE INTO [Objects_Properties] AS Target
USING (VALUES
  (N'Merlos_Impresion',N'crEmpresa',N'crEmpresa',3,0,4,1,0,0,1,N'text',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'popup',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'|',0,NULL,0,N'RelativePath',NULL,NULL,NULL,NULL,0,N'sysAll',0,1)
 ,(N'Merlos_Impresion',N'crLetra',N'crLetra',4,0,4,1,0,0,1,N'text',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'popup',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'|',0,NULL,0,N'RelativePath',NULL,NULL,NULL,NULL,0,N'sysAll',0,1)
 ,(N'Merlos_Impresion',N'crNumero',N'crNumero',2,0,4,1,0,0,1,N'text',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'popup',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'|',0,NULL,0,N'RelativePath',NULL,NULL,NULL,NULL,0,N'sysAll',0,1)
 ,(N'Merlos_Impresion',N'tipo',N'tipo',1,0,4,1,0,0,1,N'text',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'popup',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'|',0,NULL,0,N'RelativePath',NULL,NULL,NULL,NULL,0,N'sysAll',0,1)
) AS Source ([ObjectName],[PropertyName],[Label],[PositionY],[PositionX],[Width],[Height],[Hide],[ClientReadOnly],[FormDisplay],[TypeId],[Locked],[CustomPropName],[Mask],[SQlSentence],[SQLEditSentence],[SQLFilter],[SQLValueField],[SQLDisplayField],[SQLObjectName],[SQLViewName],[SQLOfflineSentence],[SQLOfflineOrderBy],[WhereSentence],[DefaultValue],[PersistDefaultValue],[IgnoreDBDefaultValue],[DetachedFromDB],[SearchFunction],[SearchCollection],[SearchWhere],[SearchReturnFields],[SecurityObject],[AllowNew],[AllowNewFunction],[AllowNewReturnFields],[AllowNewDefaults],[ObjNameLink],[ObjWhereLink],[TargetIdLink],[Style],[CSSClass],[LabelStyle],[LabelCSSClass],[DecimalPlaces],[RootPath],[FormatString],[DirectTemplate],[Tag],[HelpId],[ConnStringId],[IsRequired],[IsRequiredMessage],[minValue],[minValueMessage],[maxValue],[maxValueMessage],[RegExp],[RegExpText],[SQLValidator],[ValidatorMessage],[OnChangeJsFunction],[OnChangeProcessName],[PlaceHolder],[IconName],[ToolbarName],[Separator],[AutoIncrement],[AutoIncrementFunction],[CascadeDependencies],[RootPathType],[PageSize],[ImageCompressionType],[ImageMaxWidth],[ImageMaxHeight],[Offline],[ExtensionId],[Autoselect],[OriginId])
ON (Target.[ObjectName] = Source.[ObjectName] AND Target.[PropertyName] = Source.[PropertyName])
WHEN MATCHED AND (
	NULLIF(Source.[Label], Target.[Label]) IS NOT NULL OR NULLIF(Target.[Label], Source.[Label]) IS NOT NULL OR 
	NULLIF(Source.[PositionY], Target.[PositionY]) IS NOT NULL OR NULLIF(Target.[PositionY], Source.[PositionY]) IS NOT NULL OR 
	NULLIF(Source.[PositionX], Target.[PositionX]) IS NOT NULL OR NULLIF(Target.[PositionX], Source.[PositionX]) IS NOT NULL OR 
	NULLIF(Source.[Width], Target.[Width]) IS NOT NULL OR NULLIF(Target.[Width], Source.[Width]) IS NOT NULL OR 
	NULLIF(Source.[Height], Target.[Height]) IS NOT NULL OR NULLIF(Target.[Height], Source.[Height]) IS NOT NULL OR 
	NULLIF(Source.[Hide], Target.[Hide]) IS NOT NULL OR NULLIF(Target.[Hide], Source.[Hide]) IS NOT NULL OR 
	NULLIF(Source.[ClientReadOnly], Target.[ClientReadOnly]) IS NOT NULL OR NULLIF(Target.[ClientReadOnly], Source.[ClientReadOnly]) IS NOT NULL OR 
	NULLIF(Source.[FormDisplay], Target.[FormDisplay]) IS NOT NULL OR NULLIF(Target.[FormDisplay], Source.[FormDisplay]) IS NOT NULL OR 
	NULLIF(Source.[TypeId], Target.[TypeId]) IS NOT NULL OR NULLIF(Target.[TypeId], Source.[TypeId]) IS NOT NULL OR 
	NULLIF(Source.[Locked], Target.[Locked]) IS NOT NULL OR NULLIF(Target.[Locked], Source.[Locked]) IS NOT NULL OR 
	NULLIF(Source.[CustomPropName], Target.[CustomPropName]) IS NOT NULL OR NULLIF(Target.[CustomPropName], Source.[CustomPropName]) IS NOT NULL OR 
	NULLIF(Source.[Mask], Target.[Mask]) IS NOT NULL OR NULLIF(Target.[Mask], Source.[Mask]) IS NOT NULL OR 
	NULLIF(Source.[SQlSentence], Target.[SQlSentence]) IS NOT NULL OR NULLIF(Target.[SQlSentence], Source.[SQlSentence]) IS NOT NULL OR 
	NULLIF(Source.[SQLEditSentence], Target.[SQLEditSentence]) IS NOT NULL OR NULLIF(Target.[SQLEditSentence], Source.[SQLEditSentence]) IS NOT NULL OR 
	NULLIF(Source.[SQLFilter], Target.[SQLFilter]) IS NOT NULL OR NULLIF(Target.[SQLFilter], Source.[SQLFilter]) IS NOT NULL OR 
	NULLIF(Source.[SQLValueField], Target.[SQLValueField]) IS NOT NULL OR NULLIF(Target.[SQLValueField], Source.[SQLValueField]) IS NOT NULL OR 
	NULLIF(Source.[SQLDisplayField], Target.[SQLDisplayField]) IS NOT NULL OR NULLIF(Target.[SQLDisplayField], Source.[SQLDisplayField]) IS NOT NULL OR 
	NULLIF(Source.[SQLObjectName], Target.[SQLObjectName]) IS NOT NULL OR NULLIF(Target.[SQLObjectName], Source.[SQLObjectName]) IS NOT NULL OR 
	NULLIF(Source.[SQLViewName], Target.[SQLViewName]) IS NOT NULL OR NULLIF(Target.[SQLViewName], Source.[SQLViewName]) IS NOT NULL OR 
	NULLIF(Source.[SQLOfflineSentence], Target.[SQLOfflineSentence]) IS NOT NULL OR NULLIF(Target.[SQLOfflineSentence], Source.[SQLOfflineSentence]) IS NOT NULL OR 
	NULLIF(Source.[SQLOfflineOrderBy], Target.[SQLOfflineOrderBy]) IS NOT NULL OR NULLIF(Target.[SQLOfflineOrderBy], Source.[SQLOfflineOrderBy]) IS NOT NULL OR 
	NULLIF(Source.[WhereSentence], Target.[WhereSentence]) IS NOT NULL OR NULLIF(Target.[WhereSentence], Source.[WhereSentence]) IS NOT NULL OR 
	NULLIF(Source.[DefaultValue], Target.[DefaultValue]) IS NOT NULL OR NULLIF(Target.[DefaultValue], Source.[DefaultValue]) IS NOT NULL OR 
	NULLIF(Source.[PersistDefaultValue], Target.[PersistDefaultValue]) IS NOT NULL OR NULLIF(Target.[PersistDefaultValue], Source.[PersistDefaultValue]) IS NOT NULL OR 
	NULLIF(Source.[IgnoreDBDefaultValue], Target.[IgnoreDBDefaultValue]) IS NOT NULL OR NULLIF(Target.[IgnoreDBDefaultValue], Source.[IgnoreDBDefaultValue]) IS NOT NULL OR 
	NULLIF(Source.[DetachedFromDB], Target.[DetachedFromDB]) IS NOT NULL OR NULLIF(Target.[DetachedFromDB], Source.[DetachedFromDB]) IS NOT NULL OR 
	NULLIF(Source.[SearchFunction], Target.[SearchFunction]) IS NOT NULL OR NULLIF(Target.[SearchFunction], Source.[SearchFunction]) IS NOT NULL OR 
	NULLIF(Source.[SearchCollection], Target.[SearchCollection]) IS NOT NULL OR NULLIF(Target.[SearchCollection], Source.[SearchCollection]) IS NOT NULL OR 
	NULLIF(Source.[SearchWhere], Target.[SearchWhere]) IS NOT NULL OR NULLIF(Target.[SearchWhere], Source.[SearchWhere]) IS NOT NULL OR 
	NULLIF(Source.[SearchReturnFields], Target.[SearchReturnFields]) IS NOT NULL OR NULLIF(Target.[SearchReturnFields], Source.[SearchReturnFields]) IS NOT NULL OR 
	NULLIF(Source.[SecurityObject], Target.[SecurityObject]) IS NOT NULL OR NULLIF(Target.[SecurityObject], Source.[SecurityObject]) IS NOT NULL OR 
	NULLIF(Source.[AllowNew], Target.[AllowNew]) IS NOT NULL OR NULLIF(Target.[AllowNew], Source.[AllowNew]) IS NOT NULL OR 
	NULLIF(Source.[AllowNewFunction], Target.[AllowNewFunction]) IS NOT NULL OR NULLIF(Target.[AllowNewFunction], Source.[AllowNewFunction]) IS NOT NULL OR 
	NULLIF(Source.[AllowNewReturnFields], Target.[AllowNewReturnFields]) IS NOT NULL OR NULLIF(Target.[AllowNewReturnFields], Source.[AllowNewReturnFields]) IS NOT NULL OR 
	NULLIF(Source.[AllowNewDefaults], Target.[AllowNewDefaults]) IS NOT NULL OR NULLIF(Target.[AllowNewDefaults], Source.[AllowNewDefaults]) IS NOT NULL OR 
	NULLIF(Source.[ObjNameLink], Target.[ObjNameLink]) IS NOT NULL OR NULLIF(Target.[ObjNameLink], Source.[ObjNameLink]) IS NOT NULL OR 
	NULLIF(Source.[ObjWhereLink], Target.[ObjWhereLink]) IS NOT NULL OR NULLIF(Target.[ObjWhereLink], Source.[ObjWhereLink]) IS NOT NULL OR 
	NULLIF(Source.[TargetIdLink], Target.[TargetIdLink]) IS NOT NULL OR NULLIF(Target.[TargetIdLink], Source.[TargetIdLink]) IS NOT NULL OR 
	NULLIF(Source.[Style], Target.[Style]) IS NOT NULL OR NULLIF(Target.[Style], Source.[Style]) IS NOT NULL OR 
	NULLIF(Source.[CSSClass], Target.[CSSClass]) IS NOT NULL OR NULLIF(Target.[CSSClass], Source.[CSSClass]) IS NOT NULL OR 
	NULLIF(Source.[LabelStyle], Target.[LabelStyle]) IS NOT NULL OR NULLIF(Target.[LabelStyle], Source.[LabelStyle]) IS NOT NULL OR 
	NULLIF(Source.[LabelCSSClass], Target.[LabelCSSClass]) IS NOT NULL OR NULLIF(Target.[LabelCSSClass], Source.[LabelCSSClass]) IS NOT NULL OR 
	NULLIF(Source.[DecimalPlaces], Target.[DecimalPlaces]) IS NOT NULL OR NULLIF(Target.[DecimalPlaces], Source.[DecimalPlaces]) IS NOT NULL OR 
	NULLIF(Source.[RootPath], Target.[RootPath]) IS NOT NULL OR NULLIF(Target.[RootPath], Source.[RootPath]) IS NOT NULL OR 
	NULLIF(Source.[FormatString], Target.[FormatString]) IS NOT NULL OR NULLIF(Target.[FormatString], Source.[FormatString]) IS NOT NULL OR 
	NULLIF(Source.[DirectTemplate], Target.[DirectTemplate]) IS NOT NULL OR NULLIF(Target.[DirectTemplate], Source.[DirectTemplate]) IS NOT NULL OR 
	NULLIF(Source.[Tag], Target.[Tag]) IS NOT NULL OR NULLIF(Target.[Tag], Source.[Tag]) IS NOT NULL OR 
	NULLIF(Source.[HelpId], Target.[HelpId]) IS NOT NULL OR NULLIF(Target.[HelpId], Source.[HelpId]) IS NOT NULL OR 
	NULLIF(Source.[ConnStringId], Target.[ConnStringId]) IS NOT NULL OR NULLIF(Target.[ConnStringId], Source.[ConnStringId]) IS NOT NULL OR 
	NULLIF(Source.[IsRequired], Target.[IsRequired]) IS NOT NULL OR NULLIF(Target.[IsRequired], Source.[IsRequired]) IS NOT NULL OR 
	NULLIF(Source.[IsRequiredMessage], Target.[IsRequiredMessage]) IS NOT NULL OR NULLIF(Target.[IsRequiredMessage], Source.[IsRequiredMessage]) IS NOT NULL OR 
	NULLIF(Source.[minValue], Target.[minValue]) IS NOT NULL OR NULLIF(Target.[minValue], Source.[minValue]) IS NOT NULL OR 
	NULLIF(Source.[minValueMessage], Target.[minValueMessage]) IS NOT NULL OR NULLIF(Target.[minValueMessage], Source.[minValueMessage]) IS NOT NULL OR 
	NULLIF(Source.[maxValue], Target.[maxValue]) IS NOT NULL OR NULLIF(Target.[maxValue], Source.[maxValue]) IS NOT NULL OR 
	NULLIF(Source.[maxValueMessage], Target.[maxValueMessage]) IS NOT NULL OR NULLIF(Target.[maxValueMessage], Source.[maxValueMessage]) IS NOT NULL OR 
	NULLIF(Source.[RegExp], Target.[RegExp]) IS NOT NULL OR NULLIF(Target.[RegExp], Source.[RegExp]) IS NOT NULL OR 
	NULLIF(Source.[RegExpText], Target.[RegExpText]) IS NOT NULL OR NULLIF(Target.[RegExpText], Source.[RegExpText]) IS NOT NULL OR 
	NULLIF(Source.[SQLValidator], Target.[SQLValidator]) IS NOT NULL OR NULLIF(Target.[SQLValidator], Source.[SQLValidator]) IS NOT NULL OR 
	NULLIF(Source.[ValidatorMessage], Target.[ValidatorMessage]) IS NOT NULL OR NULLIF(Target.[ValidatorMessage], Source.[ValidatorMessage]) IS NOT NULL OR 
	NULLIF(Source.[OnChangeJsFunction], Target.[OnChangeJsFunction]) IS NOT NULL OR NULLIF(Target.[OnChangeJsFunction], Source.[OnChangeJsFunction]) IS NOT NULL OR 
	NULLIF(Source.[OnChangeProcessName], Target.[OnChangeProcessName]) IS NOT NULL OR NULLIF(Target.[OnChangeProcessName], Source.[OnChangeProcessName]) IS NOT NULL OR 
	NULLIF(Source.[PlaceHolder], Target.[PlaceHolder]) IS NOT NULL OR NULLIF(Target.[PlaceHolder], Source.[PlaceHolder]) IS NOT NULL OR 
	NULLIF(Source.[IconName], Target.[IconName]) IS NOT NULL OR NULLIF(Target.[IconName], Source.[IconName]) IS NOT NULL OR 
	NULLIF(Source.[ToolbarName], Target.[ToolbarName]) IS NOT NULL OR NULLIF(Target.[ToolbarName], Source.[ToolbarName]) IS NOT NULL OR 
	NULLIF(Source.[Separator], Target.[Separator]) IS NOT NULL OR NULLIF(Target.[Separator], Source.[Separator]) IS NOT NULL OR 
	NULLIF(Source.[AutoIncrement], Target.[AutoIncrement]) IS NOT NULL OR NULLIF(Target.[AutoIncrement], Source.[AutoIncrement]) IS NOT NULL OR 
	NULLIF(Source.[AutoIncrementFunction], Target.[AutoIncrementFunction]) IS NOT NULL OR NULLIF(Target.[AutoIncrementFunction], Source.[AutoIncrementFunction]) IS NOT NULL OR 
	NULLIF(Source.[CascadeDependencies], Target.[CascadeDependencies]) IS NOT NULL OR NULLIF(Target.[CascadeDependencies], Source.[CascadeDependencies]) IS NOT NULL OR 
	NULLIF(Source.[RootPathType], Target.[RootPathType]) IS NOT NULL OR NULLIF(Target.[RootPathType], Source.[RootPathType]) IS NOT NULL OR 
	NULLIF(Source.[PageSize], Target.[PageSize]) IS NOT NULL OR NULLIF(Target.[PageSize], Source.[PageSize]) IS NOT NULL OR 
	NULLIF(Source.[ImageCompressionType], Target.[ImageCompressionType]) IS NOT NULL OR NULLIF(Target.[ImageCompressionType], Source.[ImageCompressionType]) IS NOT NULL OR 
	NULLIF(Source.[ImageMaxWidth], Target.[ImageMaxWidth]) IS NOT NULL OR NULLIF(Target.[ImageMaxWidth], Source.[ImageMaxWidth]) IS NOT NULL OR 
	NULLIF(Source.[ImageMaxHeight], Target.[ImageMaxHeight]) IS NOT NULL OR NULLIF(Target.[ImageMaxHeight], Source.[ImageMaxHeight]) IS NOT NULL OR 
	NULLIF(Source.[Offline], Target.[Offline]) IS NOT NULL OR NULLIF(Target.[Offline], Source.[Offline]) IS NOT NULL OR 
	NULLIF(Source.[ExtensionId], Target.[ExtensionId]) IS NOT NULL OR NULLIF(Target.[ExtensionId], Source.[ExtensionId]) IS NOT NULL OR 
	NULLIF(Source.[Autoselect], Target.[Autoselect]) IS NOT NULL OR NULLIF(Target.[Autoselect], Source.[Autoselect]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [Label] = Source.[Label], 
  [PositionY] = Source.[PositionY], 
  [PositionX] = Source.[PositionX], 
  [Width] = Source.[Width], 
  [Height] = Source.[Height], 
  [Hide] = Source.[Hide], 
  [ClientReadOnly] = Source.[ClientReadOnly], 
  [FormDisplay] = Source.[FormDisplay], 
  [TypeId] = Source.[TypeId], 
  [Locked] = Source.[Locked], 
  [CustomPropName] = Source.[CustomPropName], 
  [Mask] = Source.[Mask], 
  [SQlSentence] = Source.[SQlSentence], 
  [SQLEditSentence] = Source.[SQLEditSentence], 
  [SQLFilter] = Source.[SQLFilter], 
  [SQLValueField] = Source.[SQLValueField], 
  [SQLDisplayField] = Source.[SQLDisplayField], 
  [SQLObjectName] = Source.[SQLObjectName], 
  [SQLViewName] = Source.[SQLViewName], 
  [SQLOfflineSentence] = Source.[SQLOfflineSentence], 
  [SQLOfflineOrderBy] = Source.[SQLOfflineOrderBy], 
  [WhereSentence] = Source.[WhereSentence], 
  [DefaultValue] = Source.[DefaultValue], 
  [PersistDefaultValue] = Source.[PersistDefaultValue], 
  [IgnoreDBDefaultValue] = Source.[IgnoreDBDefaultValue], 
  [DetachedFromDB] = Source.[DetachedFromDB], 
  [SearchFunction] = Source.[SearchFunction], 
  [SearchCollection] = Source.[SearchCollection], 
  [SearchWhere] = Source.[SearchWhere], 
  [SearchReturnFields] = Source.[SearchReturnFields], 
  [SecurityObject] = Source.[SecurityObject], 
  [AllowNew] = Source.[AllowNew], 
  [AllowNewFunction] = Source.[AllowNewFunction], 
  [AllowNewReturnFields] = Source.[AllowNewReturnFields], 
  [AllowNewDefaults] = Source.[AllowNewDefaults], 
  [ObjNameLink] = Source.[ObjNameLink], 
  [ObjWhereLink] = Source.[ObjWhereLink], 
  [TargetIdLink] = Source.[TargetIdLink], 
  [Style] = Source.[Style], 
  [CSSClass] = Source.[CSSClass], 
  [LabelStyle] = Source.[LabelStyle], 
  [LabelCSSClass] = Source.[LabelCSSClass], 
  [DecimalPlaces] = Source.[DecimalPlaces], 
  [RootPath] = Source.[RootPath], 
  [FormatString] = Source.[FormatString], 
  [DirectTemplate] = Source.[DirectTemplate], 
  [Tag] = Source.[Tag], 
  [HelpId] = Source.[HelpId], 
  [ConnStringId] = Source.[ConnStringId], 
  [IsRequired] = Source.[IsRequired], 
  [IsRequiredMessage] = Source.[IsRequiredMessage], 
  [minValue] = Source.[minValue], 
  [minValueMessage] = Source.[minValueMessage], 
  [maxValue] = Source.[maxValue], 
  [maxValueMessage] = Source.[maxValueMessage], 
  [RegExp] = Source.[RegExp], 
  [RegExpText] = Source.[RegExpText], 
  [SQLValidator] = Source.[SQLValidator], 
  [ValidatorMessage] = Source.[ValidatorMessage], 
  [OnChangeJsFunction] = Source.[OnChangeJsFunction], 
  [OnChangeProcessName] = Source.[OnChangeProcessName], 
  [PlaceHolder] = Source.[PlaceHolder], 
  [IconName] = Source.[IconName], 
  [ToolbarName] = Source.[ToolbarName], 
  [Separator] = Source.[Separator], 
  [AutoIncrement] = Source.[AutoIncrement], 
  [AutoIncrementFunction] = Source.[AutoIncrementFunction], 
  [CascadeDependencies] = Source.[CascadeDependencies], 
  [RootPathType] = Source.[RootPathType], 
  [PageSize] = Source.[PageSize], 
  [ImageCompressionType] = Source.[ImageCompressionType], 
  [ImageMaxWidth] = Source.[ImageMaxWidth], 
  [ImageMaxHeight] = Source.[ImageMaxHeight], 
  [Offline] = Source.[Offline], 
  [ExtensionId] = Source.[ExtensionId], 
  [Autoselect] = Source.[Autoselect], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([ObjectName],[PropertyName],[Label],[PositionY],[PositionX],[Width],[Height],[Hide],[ClientReadOnly],[FormDisplay],[TypeId],[Locked],[CustomPropName],[Mask],[SQlSentence],[SQLEditSentence],[SQLFilter],[SQLValueField],[SQLDisplayField],[SQLObjectName],[SQLViewName],[SQLOfflineSentence],[SQLOfflineOrderBy],[WhereSentence],[DefaultValue],[PersistDefaultValue],[IgnoreDBDefaultValue],[DetachedFromDB],[SearchFunction],[SearchCollection],[SearchWhere],[SearchReturnFields],[SecurityObject],[AllowNew],[AllowNewFunction],[AllowNewReturnFields],[AllowNewDefaults],[ObjNameLink],[ObjWhereLink],[TargetIdLink],[Style],[CSSClass],[LabelStyle],[LabelCSSClass],[DecimalPlaces],[RootPath],[FormatString],[DirectTemplate],[Tag],[HelpId],[ConnStringId],[IsRequired],[IsRequiredMessage],[minValue],[minValueMessage],[maxValue],[maxValueMessage],[RegExp],[RegExpText],[SQLValidator],[ValidatorMessage],[OnChangeJsFunction],[OnChangeProcessName],[PlaceHolder],[IconName],[ToolbarName],[Separator],[AutoIncrement],[AutoIncrementFunction],[CascadeDependencies],[RootPathType],[PageSize],[ImageCompressionType],[ImageMaxWidth],[ImageMaxHeight],[Offline],[ExtensionId],[Autoselect],[OriginId])
 VALUES(Source.[ObjectName],Source.[PropertyName],Source.[Label],Source.[PositionY],Source.[PositionX],Source.[Width],Source.[Height],Source.[Hide],Source.[ClientReadOnly],Source.[FormDisplay],Source.[TypeId],Source.[Locked],Source.[CustomPropName],Source.[Mask],Source.[SQlSentence],Source.[SQLEditSentence],Source.[SQLFilter],Source.[SQLValueField],Source.[SQLDisplayField],Source.[SQLObjectName],Source.[SQLViewName],Source.[SQLOfflineSentence],Source.[SQLOfflineOrderBy],Source.[WhereSentence],Source.[DefaultValue],Source.[PersistDefaultValue],Source.[IgnoreDBDefaultValue],Source.[DetachedFromDB],Source.[SearchFunction],Source.[SearchCollection],Source.[SearchWhere],Source.[SearchReturnFields],Source.[SecurityObject],Source.[AllowNew],Source.[AllowNewFunction],Source.[AllowNewReturnFields],Source.[AllowNewDefaults],Source.[ObjNameLink],Source.[ObjWhereLink],Source.[TargetIdLink],Source.[Style],Source.[CSSClass],Source.[LabelStyle],Source.[LabelCSSClass],Source.[DecimalPlaces],Source.[RootPath],Source.[FormatString],Source.[DirectTemplate],Source.[Tag],Source.[HelpId],Source.[ConnStringId],Source.[IsRequired],Source.[IsRequiredMessage],Source.[minValue],Source.[minValueMessage],Source.[maxValue],Source.[maxValueMessage],Source.[RegExp],Source.[RegExpText],Source.[SQLValidator],Source.[ValidatorMessage],Source.[OnChangeJsFunction],Source.[OnChangeProcessName],Source.[PlaceHolder],Source.[IconName],Source.[ToolbarName],Source.[Separator],Source.[AutoIncrement],Source.[AutoIncrementFunction],Source.[CascadeDependencies],Source.[RootPathType],Source.[PageSize],Source.[ImageCompressionType],Source.[ImageMaxWidth],Source.[ImageMaxHeight],Source.[Offline],Source.[ExtensionId],Source.[Autoselect],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO









BEGIN TRY

MERGE INTO [Objects_Templates] AS Target
USING (VALUES
  (N'Merlos_ImpresionDefaultList',N'Merlos_Impresion',N'list',N'Merlos_Impresion Default List',N'<tr class="buscarEn" onclick="reimpresion(''{{Tipo}}'',''{{CrNumero}}'',''{{Crempresa}}'',''{{Crletra}}'')">  
    <td clas="buscarEn">{{Tipo}}</td>  
    <td clas="buscarEn">{{CrNumero}}</td>  
    <td clas="buscarEn">{{Crempresa}}</td>  
    <td clas="buscarEn">{{Crletra}}</td>  
</tr>',N'Merlos_ImpresionDefaultList',NULL,N'<input type="text" id="inpBuscarImpresion" style="width:100%;" placeholder="buscar" onkeyup="buscarEnTabla(this.id,''tbImpresionListado'')" />
<br><br>
<table id="tbImpresionListado" class="tbStd">
    <tr>
        <th>Tipo</th>  
        <th>Número</th>  
        <th>Empresa</th>  
        <th>Serie</th>
    </tr>',N'</table>

<script>
function reimpresion(tipo,numero,empresa,letra,confirmacion){
    if(confirmacion){
        var parametros = ''{"sp":"pImpresion","modo":"reimpresion","tipo":"''+tipo+''","numero":"''+numero+''","empresa":"''+empresa+''","letra":"''+letra+''"}'';
        console.log(parametros);
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){  console.log(ret.JSCode);
            cerrarVelo();
        } else { alert(''Error SP pSeries!''+JSON.stringify(ret)); }}, false);
    }else{
        abrirVelo(
            "Confirma para volver a imprimir el documento."
            +"<br><br><br>"
            +"<span class=''btnMVerde'' onclick=''reimpresion(\""+tipo+"\",\""+numero+"\",\""+empresa+"\",\""+letra+"\",1)''>imprimir</span>"
            +"&nbsp;&nbsp;&nbsp;"
            +"<span class=''btnMRojo'' onclick=''cerrarVelo()''>cancelar</span>"
        );
    }
}
</script>',NULL,NULL,1,0,0,1,1)
) AS Source ([TemplateId],[ObjectName],[TypeId],[Descrip],[Body],[ViewName],[WhereSentence],[Header],[Footer],[Empty],[ModuleClass],[IsDefault],[Offline],[UserDefinedGroups],[Active],[OriginId])
ON (Target.[TemplateId] = Source.[TemplateId])
WHEN MATCHED AND (
	NULLIF(Source.[ObjectName], Target.[ObjectName]) IS NOT NULL OR NULLIF(Target.[ObjectName], Source.[ObjectName]) IS NOT NULL OR 
	NULLIF(Source.[TypeId], Target.[TypeId]) IS NOT NULL OR NULLIF(Target.[TypeId], Source.[TypeId]) IS NOT NULL OR 
	NULLIF(Source.[Descrip], Target.[Descrip]) IS NOT NULL OR NULLIF(Target.[Descrip], Source.[Descrip]) IS NOT NULL OR 
	NULLIF(Source.[Body], Target.[Body]) IS NOT NULL OR NULLIF(Target.[Body], Source.[Body]) IS NOT NULL OR 
	NULLIF(Source.[ViewName], Target.[ViewName]) IS NOT NULL OR NULLIF(Target.[ViewName], Source.[ViewName]) IS NOT NULL OR 
	NULLIF(Source.[WhereSentence], Target.[WhereSentence]) IS NOT NULL OR NULLIF(Target.[WhereSentence], Source.[WhereSentence]) IS NOT NULL OR 
	NULLIF(Source.[Header], Target.[Header]) IS NOT NULL OR NULLIF(Target.[Header], Source.[Header]) IS NOT NULL OR 
	NULLIF(Source.[Footer], Target.[Footer]) IS NOT NULL OR NULLIF(Target.[Footer], Source.[Footer]) IS NOT NULL OR 
	NULLIF(Source.[Empty], Target.[Empty]) IS NOT NULL OR NULLIF(Target.[Empty], Source.[Empty]) IS NOT NULL OR 
	NULLIF(Source.[ModuleClass], Target.[ModuleClass]) IS NOT NULL OR NULLIF(Target.[ModuleClass], Source.[ModuleClass]) IS NOT NULL OR 
	NULLIF(Source.[IsDefault], Target.[IsDefault]) IS NOT NULL OR NULLIF(Target.[IsDefault], Source.[IsDefault]) IS NOT NULL OR 
	NULLIF(Source.[Offline], Target.[Offline]) IS NOT NULL OR NULLIF(Target.[Offline], Source.[Offline]) IS NOT NULL OR 
	NULLIF(Source.[UserDefinedGroups], Target.[UserDefinedGroups]) IS NOT NULL OR NULLIF(Target.[UserDefinedGroups], Source.[UserDefinedGroups]) IS NOT NULL OR 
	NULLIF(Source.[Active], Target.[Active]) IS NOT NULL OR NULLIF(Target.[Active], Source.[Active]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [ObjectName] = Source.[ObjectName], 
  [TypeId] = Source.[TypeId], 
  [Descrip] = Source.[Descrip], 
  [Body] = Source.[Body], 
  [ViewName] = Source.[ViewName], 
  [WhereSentence] = Source.[WhereSentence], 
  [Header] = Source.[Header], 
  [Footer] = Source.[Footer], 
  [Empty] = Source.[Empty], 
  [ModuleClass] = Source.[ModuleClass], 
  [IsDefault] = Source.[IsDefault], 
  [Offline] = Source.[Offline], 
  [UserDefinedGroups] = Source.[UserDefinedGroups], 
  [Active] = Source.[Active], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([TemplateId],[ObjectName],[TypeId],[Descrip],[Body],[ViewName],[WhereSentence],[Header],[Footer],[Empty],[ModuleClass],[IsDefault],[Offline],[UserDefinedGroups],[Active],[OriginId])
 VALUES(Source.[TemplateId],Source.[ObjectName],Source.[TypeId],Source.[Descrip],Source.[Body],Source.[ViewName],Source.[WhereSentence],Source.[Header],Source.[Footer],Source.[Empty],Source.[ModuleClass],Source.[IsDefault],Source.[Offline],Source.[UserDefinedGroups],Source.[Active],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO








BEGIN TRY

MERGE INTO [Objects_Views] AS Target
USING (VALUES
  (N'Merlos_Impresion',N'Merlos_ImpresionDefaultList',N'Merlos_ImpresionDefaultList',N'DataConnectionString',N'select * from vImpresion',0,1,1,0,1,NULL,0,NULL,NULL,1)
) AS Source ([ObjectName],[ViewName],[Descrip],[ConnStringId],[SQLSentence],[NoFilter],[ShowAsGrid],[Active],[System],[IsDefault],[OrderBy],[Offline],[PrimaryKeys],[IndexFields],[OriginId])
ON (Target.[ObjectName] = Source.[ObjectName] AND Target.[ViewName] = Source.[ViewName])
WHEN MATCHED AND (
	NULLIF(Source.[Descrip], Target.[Descrip]) IS NOT NULL OR NULLIF(Target.[Descrip], Source.[Descrip]) IS NOT NULL OR 
	NULLIF(Source.[ConnStringId], Target.[ConnStringId]) IS NOT NULL OR NULLIF(Target.[ConnStringId], Source.[ConnStringId]) IS NOT NULL OR 
	NULLIF(Source.[SQLSentence], Target.[SQLSentence]) IS NOT NULL OR NULLIF(Target.[SQLSentence], Source.[SQLSentence]) IS NOT NULL OR 
	NULLIF(Source.[NoFilter], Target.[NoFilter]) IS NOT NULL OR NULLIF(Target.[NoFilter], Source.[NoFilter]) IS NOT NULL OR 
	NULLIF(Source.[ShowAsGrid], Target.[ShowAsGrid]) IS NOT NULL OR NULLIF(Target.[ShowAsGrid], Source.[ShowAsGrid]) IS NOT NULL OR 
	NULLIF(Source.[Active], Target.[Active]) IS NOT NULL OR NULLIF(Target.[Active], Source.[Active]) IS NOT NULL OR 
	NULLIF(Source.[System], Target.[System]) IS NOT NULL OR NULLIF(Target.[System], Source.[System]) IS NOT NULL OR 
	NULLIF(Source.[IsDefault], Target.[IsDefault]) IS NOT NULL OR NULLIF(Target.[IsDefault], Source.[IsDefault]) IS NOT NULL OR 
	NULLIF(Source.[OrderBy], Target.[OrderBy]) IS NOT NULL OR NULLIF(Target.[OrderBy], Source.[OrderBy]) IS NOT NULL OR 
	NULLIF(Source.[Offline], Target.[Offline]) IS NOT NULL OR NULLIF(Target.[Offline], Source.[Offline]) IS NOT NULL OR 
	NULLIF(Source.[PrimaryKeys], Target.[PrimaryKeys]) IS NOT NULL OR NULLIF(Target.[PrimaryKeys], Source.[PrimaryKeys]) IS NOT NULL OR 
	NULLIF(Source.[IndexFields], Target.[IndexFields]) IS NOT NULL OR NULLIF(Target.[IndexFields], Source.[IndexFields]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [Descrip] = Source.[Descrip], 
  [ConnStringId] = Source.[ConnStringId], 
  [SQLSentence] = Source.[SQLSentence], 
  [NoFilter] = Source.[NoFilter], 
  [ShowAsGrid] = Source.[ShowAsGrid], 
  [Active] = Source.[Active], 
  [System] = Source.[System], 
  [IsDefault] = Source.[IsDefault], 
  [OrderBy] = Source.[OrderBy], 
  [Offline] = Source.[Offline], 
  [PrimaryKeys] = Source.[PrimaryKeys], 
  [IndexFields] = Source.[IndexFields], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([ObjectName],[ViewName],[Descrip],[ConnStringId],[SQLSentence],[NoFilter],[ShowAsGrid],[Active],[System],[IsDefault],[OrderBy],[Offline],[PrimaryKeys],[IndexFields],[OriginId])
 VALUES(Source.[ObjectName],Source.[ViewName],Source.[Descrip],Source.[ConnStringId],Source.[SQLSentence],Source.[NoFilter],Source.[ShowAsGrid],Source.[Active],Source.[System],Source.[IsDefault],Source.[OrderBy],Source.[Offline],Source.[PrimaryKeys],Source.[IndexFields],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO









BEGIN TRY

MERGE INTO [Pages] AS Target
USING (VALUES
  (N'Merlos_Administrador',N'generic',NULL,NULL,N'default',N'Merlos_Administrador',N'noicon',N'Merlos_Administrador',NULL,NULL,0,NULL,0,0,0,NULL,0,1)
 ,(N'Merlos_Impresion',N'generic',NULL,NULL,N'default',N'Merlos_Impresion',N'printer-2',N'Merlos_Impresion',NULL,NULL,0,NULL,0,0,0,NULL,0,1)
) AS Source ([PageName],[TypeId],[ObjectName],[InterfaceName],[LayoutName],[Name],[IconName],[Descrip],[UrlRewrite],[Script],[ScriptActive],[Style],[RefreshInterval],[Sytem],[Generic],[BodyCssClass],[Offline],[OriginId])
ON (Target.[PageName] = Source.[PageName])
WHEN MATCHED AND (
	NULLIF(Source.[TypeId], Target.[TypeId]) IS NOT NULL OR NULLIF(Target.[TypeId], Source.[TypeId]) IS NOT NULL OR 
	NULLIF(Source.[ObjectName], Target.[ObjectName]) IS NOT NULL OR NULLIF(Target.[ObjectName], Source.[ObjectName]) IS NOT NULL OR 
	NULLIF(Source.[InterfaceName], Target.[InterfaceName]) IS NOT NULL OR NULLIF(Target.[InterfaceName], Source.[InterfaceName]) IS NOT NULL OR 
	NULLIF(Source.[LayoutName], Target.[LayoutName]) IS NOT NULL OR NULLIF(Target.[LayoutName], Source.[LayoutName]) IS NOT NULL OR 
	NULLIF(Source.[Name], Target.[Name]) IS NOT NULL OR NULLIF(Target.[Name], Source.[Name]) IS NOT NULL OR 
	NULLIF(Source.[IconName], Target.[IconName]) IS NOT NULL OR NULLIF(Target.[IconName], Source.[IconName]) IS NOT NULL OR 
	NULLIF(Source.[Descrip], Target.[Descrip]) IS NOT NULL OR NULLIF(Target.[Descrip], Source.[Descrip]) IS NOT NULL OR 
	NULLIF(Source.[UrlRewrite], Target.[UrlRewrite]) IS NOT NULL OR NULLIF(Target.[UrlRewrite], Source.[UrlRewrite]) IS NOT NULL OR 
	NULLIF(Source.[Script], Target.[Script]) IS NOT NULL OR NULLIF(Target.[Script], Source.[Script]) IS NOT NULL OR 
	NULLIF(Source.[ScriptActive], Target.[ScriptActive]) IS NOT NULL OR NULLIF(Target.[ScriptActive], Source.[ScriptActive]) IS NOT NULL OR 
	NULLIF(Source.[Style], Target.[Style]) IS NOT NULL OR NULLIF(Target.[Style], Source.[Style]) IS NOT NULL OR 
	NULLIF(Source.[RefreshInterval], Target.[RefreshInterval]) IS NOT NULL OR NULLIF(Target.[RefreshInterval], Source.[RefreshInterval]) IS NOT NULL OR 
	NULLIF(Source.[Sytem], Target.[Sytem]) IS NOT NULL OR NULLIF(Target.[Sytem], Source.[Sytem]) IS NOT NULL OR 
	NULLIF(Source.[Generic], Target.[Generic]) IS NOT NULL OR NULLIF(Target.[Generic], Source.[Generic]) IS NOT NULL OR 
	NULLIF(Source.[BodyCssClass], Target.[BodyCssClass]) IS NOT NULL OR NULLIF(Target.[BodyCssClass], Source.[BodyCssClass]) IS NOT NULL OR 
	NULLIF(Source.[Offline], Target.[Offline]) IS NOT NULL OR NULLIF(Target.[Offline], Source.[Offline]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [TypeId] = Source.[TypeId], 
  [ObjectName] = Source.[ObjectName], 
  [InterfaceName] = Source.[InterfaceName], 
  [LayoutName] = Source.[LayoutName], 
  [Name] = Source.[Name], 
  [IconName] = Source.[IconName], 
  [Descrip] = Source.[Descrip], 
  [UrlRewrite] = Source.[UrlRewrite], 
  [Script] = Source.[Script], 
  [ScriptActive] = Source.[ScriptActive], 
  [Style] = Source.[Style], 
  [RefreshInterval] = Source.[RefreshInterval], 
  [Sytem] = Source.[Sytem], 
  [Generic] = Source.[Generic], 
  [BodyCssClass] = Source.[BodyCssClass], 
  [Offline] = Source.[Offline], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([PageName],[TypeId],[ObjectName],[InterfaceName],[LayoutName],[Name],[IconName],[Descrip],[UrlRewrite],[Script],[ScriptActive],[Style],[RefreshInterval],[Sytem],[Generic],[BodyCssClass],[Offline],[OriginId])
 VALUES(Source.[PageName],Source.[TypeId],Source.[ObjectName],Source.[InterfaceName],Source.[LayoutName],Source.[Name],Source.[IconName],Source.[Descrip],Source.[UrlRewrite],Source.[Script],Source.[ScriptActive],Source.[Style],Source.[RefreshInterval],Source.[Sytem],Source.[Generic],Source.[BodyCssClass],Source.[Offline],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO








BEGIN TRY

MERGE INTO [Pages_Modules] AS Target
USING (VALUES
  (N'Merlos_Administrador',N'Merlos_Administrador',N'TopPosition',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1)
 ,(N'Merlos_Impresion',N'Merlos_Impresion',N'TopPosition',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1)
 ,(N'syspage-generic-controlpanel',N'Merlos_Trabajo',N'TopPosition',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1)
) AS Source ([PageName],[ModuleName],[LayoutPositionId],[RelationWhere],[Order],[SQlEnabled],[SQLEnabledDescrip],[Title],[IconName],[HeaderClass],[ModuleClass],[ConnStringID],[OriginId])
ON (Target.[PageName] = Source.[PageName] AND Target.[ModuleName] = Source.[ModuleName])
WHEN MATCHED AND (
	NULLIF(Source.[LayoutPositionId], Target.[LayoutPositionId]) IS NOT NULL OR NULLIF(Target.[LayoutPositionId], Source.[LayoutPositionId]) IS NOT NULL OR 
	NULLIF(Source.[RelationWhere], Target.[RelationWhere]) IS NOT NULL OR NULLIF(Target.[RelationWhere], Source.[RelationWhere]) IS NOT NULL OR 
	NULLIF(Source.[Order], Target.[Order]) IS NOT NULL OR NULLIF(Target.[Order], Source.[Order]) IS NOT NULL OR 
	NULLIF(Source.[SQlEnabled], Target.[SQlEnabled]) IS NOT NULL OR NULLIF(Target.[SQlEnabled], Source.[SQlEnabled]) IS NOT NULL OR 
	NULLIF(Source.[SQLEnabledDescrip], Target.[SQLEnabledDescrip]) IS NOT NULL OR NULLIF(Target.[SQLEnabledDescrip], Source.[SQLEnabledDescrip]) IS NOT NULL OR 
	NULLIF(Source.[Title], Target.[Title]) IS NOT NULL OR NULLIF(Target.[Title], Source.[Title]) IS NOT NULL OR 
	NULLIF(Source.[IconName], Target.[IconName]) IS NOT NULL OR NULLIF(Target.[IconName], Source.[IconName]) IS NOT NULL OR 
	NULLIF(Source.[HeaderClass], Target.[HeaderClass]) IS NOT NULL OR NULLIF(Target.[HeaderClass], Source.[HeaderClass]) IS NOT NULL OR 
	NULLIF(Source.[ModuleClass], Target.[ModuleClass]) IS NOT NULL OR NULLIF(Target.[ModuleClass], Source.[ModuleClass]) IS NOT NULL OR 
	NULLIF(Source.[ConnStringID], Target.[ConnStringID]) IS NOT NULL OR NULLIF(Target.[ConnStringID], Source.[ConnStringID]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [LayoutPositionId] = Source.[LayoutPositionId], 
  [RelationWhere] = Source.[RelationWhere], 
  [Order] = Source.[Order], 
  [SQlEnabled] = Source.[SQlEnabled], 
  [SQLEnabledDescrip] = Source.[SQLEnabledDescrip], 
  [Title] = Source.[Title], 
  [IconName] = Source.[IconName], 
  [HeaderClass] = Source.[HeaderClass], 
  [ModuleClass] = Source.[ModuleClass], 
  [ConnStringID] = Source.[ConnStringID], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([PageName],[ModuleName],[LayoutPositionId],[RelationWhere],[Order],[SQlEnabled],[SQLEnabledDescrip],[Title],[IconName],[HeaderClass],[ModuleClass],[ConnStringID],[OriginId])
 VALUES(Source.[PageName],Source.[ModuleName],Source.[LayoutPositionId],Source.[RelationWhere],Source.[Order],Source.[SQlEnabled],Source.[SQLEnabledDescrip],Source.[Title],Source.[IconName],Source.[HeaderClass],Source.[ModuleClass],Source.[ConnStringID],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO








BEGIN TRY

MERGE INTO [Plugins] AS Target
USING (VALUES
  (N'AAA95725-2A76-4005-A091-0D41E6ECB0B8',N'~/Merlos/plugins/MERLOS.css',N'MerlosCSS',100,1,1,1,1)
 ,(N'F8223C62-07C5-4627-9ACF-4CB7D58FE085',N'~/Merlos/plugins/MERLOS_WC.js',N'MERLOS_WC',100,0,1,1,1)
 ,(N'77C32BF2-2806-46E1-A2C9-C958B21C2867',N'~/Merlos/plugins/MERLOS.js',N'MerlosJS',100,0,1,1,1)
) AS Source ([PluginId],[Path],[Descrip],[Order],[typeId],[Bundle],[Enabled],[OriginId])
ON (Target.[PluginId] = Source.[PluginId])
WHEN MATCHED AND (
	NULLIF(Source.[Path], Target.[Path]) IS NOT NULL OR NULLIF(Target.[Path], Source.[Path]) IS NOT NULL OR 
	NULLIF(Source.[Descrip], Target.[Descrip]) IS NOT NULL OR NULLIF(Target.[Descrip], Source.[Descrip]) IS NOT NULL OR 
	NULLIF(Source.[Order], Target.[Order]) IS NOT NULL OR NULLIF(Target.[Order], Source.[Order]) IS NOT NULL OR 
	NULLIF(Source.[typeId], Target.[typeId]) IS NOT NULL OR NULLIF(Target.[typeId], Source.[typeId]) IS NOT NULL OR 
	NULLIF(Source.[Bundle], Target.[Bundle]) IS NOT NULL OR NULLIF(Target.[Bundle], Source.[Bundle]) IS NOT NULL OR 
	NULLIF(Source.[Enabled], Target.[Enabled]) IS NOT NULL OR NULLIF(Target.[Enabled], Source.[Enabled]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [Path] = Source.[Path], 
  [Descrip] = Source.[Descrip], 
  [Order] = Source.[Order], 
  [typeId] = Source.[typeId], 
  [Bundle] = Source.[Bundle], 
  [Enabled] = Source.[Enabled], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([PluginId],[Path],[Descrip],[Order],[typeId],[Bundle],[Enabled],[OriginId])
 VALUES(Source.[PluginId],Source.[Path],Source.[Descrip],Source.[Order],Source.[typeId],Source.[Bundle],Source.[Enabled],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO








BEGIN TRY

MERGE INTO [Processes] AS Target
USING (VALUES
  (N'pMerlos',0,N'product',NULL,N'pMerlos',NULL,NULL,NULL,NULL,NULL,NULL,N'pMerlos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,N'DataConnectionString',N'modal640x480',NULL,1,0,0,0,1,0,NULL,NULL,0,NULL,NULL,0,0,1)
) AS Source ([ProcessName],[TypeId],[ClassId],[ConfirmText],[ProcessDescrip],[ParamsDescrip],[ReturnDescrip],[JSforParams],[File],[Class],[Method],[StoredName],[Code],[ExternalUrl],[PageTypeId],[PageName],[ReportName],[HelpId],[ReportWhere],[ObjectName],[ObjectWhere],[Defaults],[TimeOut],[ConnStringId],[TargetId],[ProcessFlowText],[Auditable],[Refresh],[IsTransacted],[AdminOnly],[ConfirmOkText],[CloseDialogOnOk],[RunButtonText],[LoadProcessName],[GipeParams],[Summary],[ReturnTableNames],[Reserved],[Offline],[OriginId])
ON (Target.[ProcessName] = Source.[ProcessName])
WHEN MATCHED AND (
	NULLIF(Source.[TypeId], Target.[TypeId]) IS NOT NULL OR NULLIF(Target.[TypeId], Source.[TypeId]) IS NOT NULL OR 
	NULLIF(Source.[ClassId], Target.[ClassId]) IS NOT NULL OR NULLIF(Target.[ClassId], Source.[ClassId]) IS NOT NULL OR 
	NULLIF(Source.[ConfirmText], Target.[ConfirmText]) IS NOT NULL OR NULLIF(Target.[ConfirmText], Source.[ConfirmText]) IS NOT NULL OR 
	NULLIF(Source.[ProcessDescrip], Target.[ProcessDescrip]) IS NOT NULL OR NULLIF(Target.[ProcessDescrip], Source.[ProcessDescrip]) IS NOT NULL OR 
	NULLIF(Source.[ParamsDescrip], Target.[ParamsDescrip]) IS NOT NULL OR NULLIF(Target.[ParamsDescrip], Source.[ParamsDescrip]) IS NOT NULL OR 
	NULLIF(Source.[ReturnDescrip], Target.[ReturnDescrip]) IS NOT NULL OR NULLIF(Target.[ReturnDescrip], Source.[ReturnDescrip]) IS NOT NULL OR 
	NULLIF(Source.[JSforParams], Target.[JSforParams]) IS NOT NULL OR NULLIF(Target.[JSforParams], Source.[JSforParams]) IS NOT NULL OR 
	NULLIF(Source.[File], Target.[File]) IS NOT NULL OR NULLIF(Target.[File], Source.[File]) IS NOT NULL OR 
	NULLIF(Source.[Class], Target.[Class]) IS NOT NULL OR NULLIF(Target.[Class], Source.[Class]) IS NOT NULL OR 
	NULLIF(Source.[Method], Target.[Method]) IS NOT NULL OR NULLIF(Target.[Method], Source.[Method]) IS NOT NULL OR 
	NULLIF(Source.[StoredName], Target.[StoredName]) IS NOT NULL OR NULLIF(Target.[StoredName], Source.[StoredName]) IS NOT NULL OR 
	NULLIF(Source.[Code], Target.[Code]) IS NOT NULL OR NULLIF(Target.[Code], Source.[Code]) IS NOT NULL OR 
	NULLIF(Source.[ExternalUrl], Target.[ExternalUrl]) IS NOT NULL OR NULLIF(Target.[ExternalUrl], Source.[ExternalUrl]) IS NOT NULL OR 
	NULLIF(Source.[PageTypeId], Target.[PageTypeId]) IS NOT NULL OR NULLIF(Target.[PageTypeId], Source.[PageTypeId]) IS NOT NULL OR 
	NULLIF(Source.[PageName], Target.[PageName]) IS NOT NULL OR NULLIF(Target.[PageName], Source.[PageName]) IS NOT NULL OR 
	NULLIF(Source.[ReportName], Target.[ReportName]) IS NOT NULL OR NULLIF(Target.[ReportName], Source.[ReportName]) IS NOT NULL OR 
	NULLIF(Source.[HelpId], Target.[HelpId]) IS NOT NULL OR NULLIF(Target.[HelpId], Source.[HelpId]) IS NOT NULL OR 
	NULLIF(Source.[ReportWhere], Target.[ReportWhere]) IS NOT NULL OR NULLIF(Target.[ReportWhere], Source.[ReportWhere]) IS NOT NULL OR 
	NULLIF(Source.[ObjectName], Target.[ObjectName]) IS NOT NULL OR NULLIF(Target.[ObjectName], Source.[ObjectName]) IS NOT NULL OR 
	NULLIF(Source.[ObjectWhere], Target.[ObjectWhere]) IS NOT NULL OR NULLIF(Target.[ObjectWhere], Source.[ObjectWhere]) IS NOT NULL OR 
	NULLIF(Source.[Defaults], Target.[Defaults]) IS NOT NULL OR NULLIF(Target.[Defaults], Source.[Defaults]) IS NOT NULL OR 
	NULLIF(Source.[TimeOut], Target.[TimeOut]) IS NOT NULL OR NULLIF(Target.[TimeOut], Source.[TimeOut]) IS NOT NULL OR 
	NULLIF(Source.[ConnStringId], Target.[ConnStringId]) IS NOT NULL OR NULLIF(Target.[ConnStringId], Source.[ConnStringId]) IS NOT NULL OR 
	NULLIF(Source.[TargetId], Target.[TargetId]) IS NOT NULL OR NULLIF(Target.[TargetId], Source.[TargetId]) IS NOT NULL OR 
	NULLIF(Source.[ProcessFlowText], Target.[ProcessFlowText]) IS NOT NULL OR NULLIF(Target.[ProcessFlowText], Source.[ProcessFlowText]) IS NOT NULL OR 
	NULLIF(Source.[Auditable], Target.[Auditable]) IS NOT NULL OR NULLIF(Target.[Auditable], Source.[Auditable]) IS NOT NULL OR 
	NULLIF(Source.[Refresh], Target.[Refresh]) IS NOT NULL OR NULLIF(Target.[Refresh], Source.[Refresh]) IS NOT NULL OR 
	NULLIF(Source.[IsTransacted], Target.[IsTransacted]) IS NOT NULL OR NULLIF(Target.[IsTransacted], Source.[IsTransacted]) IS NOT NULL OR 
	NULLIF(Source.[AdminOnly], Target.[AdminOnly]) IS NOT NULL OR NULLIF(Target.[AdminOnly], Source.[AdminOnly]) IS NOT NULL OR 
	NULLIF(Source.[ConfirmOkText], Target.[ConfirmOkText]) IS NOT NULL OR NULLIF(Target.[ConfirmOkText], Source.[ConfirmOkText]) IS NOT NULL OR 
	NULLIF(Source.[CloseDialogOnOk], Target.[CloseDialogOnOk]) IS NOT NULL OR NULLIF(Target.[CloseDialogOnOk], Source.[CloseDialogOnOk]) IS NOT NULL OR 
	NULLIF(Source.[RunButtonText], Target.[RunButtonText]) IS NOT NULL OR NULLIF(Target.[RunButtonText], Source.[RunButtonText]) IS NOT NULL OR 
	NULLIF(Source.[LoadProcessName], Target.[LoadProcessName]) IS NOT NULL OR NULLIF(Target.[LoadProcessName], Source.[LoadProcessName]) IS NOT NULL OR 
	NULLIF(Source.[GipeParams], Target.[GipeParams]) IS NOT NULL OR NULLIF(Target.[GipeParams], Source.[GipeParams]) IS NOT NULL OR 
	NULLIF(Source.[Summary], Target.[Summary]) IS NOT NULL OR NULLIF(Target.[Summary], Source.[Summary]) IS NOT NULL OR 
	NULLIF(Source.[ReturnTableNames], Target.[ReturnTableNames]) IS NOT NULL OR NULLIF(Target.[ReturnTableNames], Source.[ReturnTableNames]) IS NOT NULL OR 
	NULLIF(Source.[Reserved], Target.[Reserved]) IS NOT NULL OR NULLIF(Target.[Reserved], Source.[Reserved]) IS NOT NULL OR 
	NULLIF(Source.[Offline], Target.[Offline]) IS NOT NULL OR NULLIF(Target.[Offline], Source.[Offline]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [TypeId] = Source.[TypeId], 
  [ClassId] = Source.[ClassId], 
  [ConfirmText] = Source.[ConfirmText], 
  [ProcessDescrip] = Source.[ProcessDescrip], 
  [ParamsDescrip] = Source.[ParamsDescrip], 
  [ReturnDescrip] = Source.[ReturnDescrip], 
  [JSforParams] = Source.[JSforParams], 
  [File] = Source.[File], 
  [Class] = Source.[Class], 
  [Method] = Source.[Method], 
  [StoredName] = Source.[StoredName], 
  [Code] = Source.[Code], 
  [ExternalUrl] = Source.[ExternalUrl], 
  [PageTypeId] = Source.[PageTypeId], 
  [PageName] = Source.[PageName], 
  [ReportName] = Source.[ReportName], 
  [HelpId] = Source.[HelpId], 
  [ReportWhere] = Source.[ReportWhere], 
  [ObjectName] = Source.[ObjectName], 
  [ObjectWhere] = Source.[ObjectWhere], 
  [Defaults] = Source.[Defaults], 
  [TimeOut] = Source.[TimeOut], 
  [ConnStringId] = Source.[ConnStringId], 
  [TargetId] = Source.[TargetId], 
  [ProcessFlowText] = Source.[ProcessFlowText], 
  [Auditable] = Source.[Auditable], 
  [Refresh] = Source.[Refresh], 
  [IsTransacted] = Source.[IsTransacted], 
  [AdminOnly] = Source.[AdminOnly], 
  [ConfirmOkText] = Source.[ConfirmOkText], 
  [CloseDialogOnOk] = Source.[CloseDialogOnOk], 
  [RunButtonText] = Source.[RunButtonText], 
  [LoadProcessName] = Source.[LoadProcessName], 
  [GipeParams] = Source.[GipeParams], 
  [Summary] = Source.[Summary], 
  [ReturnTableNames] = Source.[ReturnTableNames], 
  [Reserved] = Source.[Reserved], 
  [Offline] = Source.[Offline], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([ProcessName],[TypeId],[ClassId],[ConfirmText],[ProcessDescrip],[ParamsDescrip],[ReturnDescrip],[JSforParams],[File],[Class],[Method],[StoredName],[Code],[ExternalUrl],[PageTypeId],[PageName],[ReportName],[HelpId],[ReportWhere],[ObjectName],[ObjectWhere],[Defaults],[TimeOut],[ConnStringId],[TargetId],[ProcessFlowText],[Auditable],[Refresh],[IsTransacted],[AdminOnly],[ConfirmOkText],[CloseDialogOnOk],[RunButtonText],[LoadProcessName],[GipeParams],[Summary],[ReturnTableNames],[Reserved],[Offline],[OriginId])
 VALUES(Source.[ProcessName],Source.[TypeId],Source.[ClassId],Source.[ConfirmText],Source.[ProcessDescrip],Source.[ParamsDescrip],Source.[ReturnDescrip],Source.[JSforParams],Source.[File],Source.[Class],Source.[Method],Source.[StoredName],Source.[Code],Source.[ExternalUrl],Source.[PageTypeId],Source.[PageName],Source.[ReportName],Source.[HelpId],Source.[ReportWhere],Source.[ObjectName],Source.[ObjectWhere],Source.[Defaults],Source.[TimeOut],Source.[ConnStringId],Source.[TargetId],Source.[ProcessFlowText],Source.[Auditable],Source.[Refresh],Source.[IsTransacted],Source.[AdminOnly],Source.[ConfirmOkText],Source.[CloseDialogOnOk],Source.[RunButtonText],Source.[LoadProcessName],Source.[GipeParams],Source.[Summary],Source.[ReturnTableNames],Source.[Reserved],Source.[Offline],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO









BEGIN TRY

MERGE INTO [Processes_Params] AS Target
USING (VALUES
  (N'pMerlos',N'parametros',0,N'string',NULL,NULL,NULL,N'parametros',N'IO',0,0,4,1,N'text',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'DataConnectionString',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'|',0,N'RelativePath',NULL,NULL,NULL,0,N'sysAll',0,1)
) AS Source ([ProcessName],[ParamName],[Hide],[ParamTypeId],[TableName],[ObjectName],[DefaultValue],[Label],[IOTypeId],[PositionX],[PositionY],[Width],[Height],[TypeId],[Locked],[CustomPropName],[Mask],[SQlSentence],[SQLFilter],[SQLValueField],[SQLDisplayField],[SQLObjectName],[SQLViewName],[SQLOfflineSentence],[SQLOfflineOrderBy],[WhereSentence],[DetachedFromProcess],[SearchFunction],[SearchCollection],[SearchWhere],[SearchReturnFields],[SecurityObject],[AllowNew],[AllowNewFunction],[AllowNewReturnFields],[AllowNewDefaults],[ObjNameLink],[ObjWhereLink],[TargetIdLink],[Style],[CSSClass],[LabelStyle],[LabelCSSClass],[DecimalPlaces],[RootPath],[FormatString],[DirectTemplate],[Tag],[HelpId],[ConnStringId],[IsRequired],[IsRequiredMessage],[minValue],[minValueMessage],[maxValue],[maxValueMessage],[RegExp],[RegExpText],[SQLValidator],[ValidatorMessage],[OnChangeJsFunction],[OnChangeProcessName],[PlaceHolder],[IconName],[ToolbarName],[Separator],[CascadeDependencies],[RootPathType],[ImageCompressionType],[ImageMaxWidth],[ImageMaxHeight],[Offline],[ExtensionId],[Autoselect],[OriginId])
ON (Target.[ProcessName] = Source.[ProcessName] AND Target.[ParamName] = Source.[ParamName])
WHEN MATCHED AND (
	NULLIF(Source.[Hide], Target.[Hide]) IS NOT NULL OR NULLIF(Target.[Hide], Source.[Hide]) IS NOT NULL OR 
	NULLIF(Source.[ParamTypeId], Target.[ParamTypeId]) IS NOT NULL OR NULLIF(Target.[ParamTypeId], Source.[ParamTypeId]) IS NOT NULL OR 
	NULLIF(Source.[TableName], Target.[TableName]) IS NOT NULL OR NULLIF(Target.[TableName], Source.[TableName]) IS NOT NULL OR 
	NULLIF(Source.[ObjectName], Target.[ObjectName]) IS NOT NULL OR NULLIF(Target.[ObjectName], Source.[ObjectName]) IS NOT NULL OR 
	NULLIF(Source.[DefaultValue], Target.[DefaultValue]) IS NOT NULL OR NULLIF(Target.[DefaultValue], Source.[DefaultValue]) IS NOT NULL OR 
	NULLIF(Source.[Label], Target.[Label]) IS NOT NULL OR NULLIF(Target.[Label], Source.[Label]) IS NOT NULL OR 
	NULLIF(Source.[IOTypeId], Target.[IOTypeId]) IS NOT NULL OR NULLIF(Target.[IOTypeId], Source.[IOTypeId]) IS NOT NULL OR 
	NULLIF(Source.[PositionX], Target.[PositionX]) IS NOT NULL OR NULLIF(Target.[PositionX], Source.[PositionX]) IS NOT NULL OR 
	NULLIF(Source.[PositionY], Target.[PositionY]) IS NOT NULL OR NULLIF(Target.[PositionY], Source.[PositionY]) IS NOT NULL OR 
	NULLIF(Source.[Width], Target.[Width]) IS NOT NULL OR NULLIF(Target.[Width], Source.[Width]) IS NOT NULL OR 
	NULLIF(Source.[Height], Target.[Height]) IS NOT NULL OR NULLIF(Target.[Height], Source.[Height]) IS NOT NULL OR 
	NULLIF(Source.[TypeId], Target.[TypeId]) IS NOT NULL OR NULLIF(Target.[TypeId], Source.[TypeId]) IS NOT NULL OR 
	NULLIF(Source.[Locked], Target.[Locked]) IS NOT NULL OR NULLIF(Target.[Locked], Source.[Locked]) IS NOT NULL OR 
	NULLIF(Source.[CustomPropName], Target.[CustomPropName]) IS NOT NULL OR NULLIF(Target.[CustomPropName], Source.[CustomPropName]) IS NOT NULL OR 
	NULLIF(Source.[Mask], Target.[Mask]) IS NOT NULL OR NULLIF(Target.[Mask], Source.[Mask]) IS NOT NULL OR 
	NULLIF(Source.[SQlSentence], Target.[SQlSentence]) IS NOT NULL OR NULLIF(Target.[SQlSentence], Source.[SQlSentence]) IS NOT NULL OR 
	NULLIF(Source.[SQLFilter], Target.[SQLFilter]) IS NOT NULL OR NULLIF(Target.[SQLFilter], Source.[SQLFilter]) IS NOT NULL OR 
	NULLIF(Source.[SQLValueField], Target.[SQLValueField]) IS NOT NULL OR NULLIF(Target.[SQLValueField], Source.[SQLValueField]) IS NOT NULL OR 
	NULLIF(Source.[SQLDisplayField], Target.[SQLDisplayField]) IS NOT NULL OR NULLIF(Target.[SQLDisplayField], Source.[SQLDisplayField]) IS NOT NULL OR 
	NULLIF(Source.[SQLObjectName], Target.[SQLObjectName]) IS NOT NULL OR NULLIF(Target.[SQLObjectName], Source.[SQLObjectName]) IS NOT NULL OR 
	NULLIF(Source.[SQLViewName], Target.[SQLViewName]) IS NOT NULL OR NULLIF(Target.[SQLViewName], Source.[SQLViewName]) IS NOT NULL OR 
	NULLIF(Source.[SQLOfflineSentence], Target.[SQLOfflineSentence]) IS NOT NULL OR NULLIF(Target.[SQLOfflineSentence], Source.[SQLOfflineSentence]) IS NOT NULL OR 
	NULLIF(Source.[SQLOfflineOrderBy], Target.[SQLOfflineOrderBy]) IS NOT NULL OR NULLIF(Target.[SQLOfflineOrderBy], Source.[SQLOfflineOrderBy]) IS NOT NULL OR 
	NULLIF(Source.[WhereSentence], Target.[WhereSentence]) IS NOT NULL OR NULLIF(Target.[WhereSentence], Source.[WhereSentence]) IS NOT NULL OR 
	NULLIF(Source.[DetachedFromProcess], Target.[DetachedFromProcess]) IS NOT NULL OR NULLIF(Target.[DetachedFromProcess], Source.[DetachedFromProcess]) IS NOT NULL OR 
	NULLIF(Source.[SearchFunction], Target.[SearchFunction]) IS NOT NULL OR NULLIF(Target.[SearchFunction], Source.[SearchFunction]) IS NOT NULL OR 
	NULLIF(Source.[SearchCollection], Target.[SearchCollection]) IS NOT NULL OR NULLIF(Target.[SearchCollection], Source.[SearchCollection]) IS NOT NULL OR 
	NULLIF(Source.[SearchWhere], Target.[SearchWhere]) IS NOT NULL OR NULLIF(Target.[SearchWhere], Source.[SearchWhere]) IS NOT NULL OR 
	NULLIF(Source.[SearchReturnFields], Target.[SearchReturnFields]) IS NOT NULL OR NULLIF(Target.[SearchReturnFields], Source.[SearchReturnFields]) IS NOT NULL OR 
	NULLIF(Source.[SecurityObject], Target.[SecurityObject]) IS NOT NULL OR NULLIF(Target.[SecurityObject], Source.[SecurityObject]) IS NOT NULL OR 
	NULLIF(Source.[AllowNew], Target.[AllowNew]) IS NOT NULL OR NULLIF(Target.[AllowNew], Source.[AllowNew]) IS NOT NULL OR 
	NULLIF(Source.[AllowNewFunction], Target.[AllowNewFunction]) IS NOT NULL OR NULLIF(Target.[AllowNewFunction], Source.[AllowNewFunction]) IS NOT NULL OR 
	NULLIF(Source.[AllowNewReturnFields], Target.[AllowNewReturnFields]) IS NOT NULL OR NULLIF(Target.[AllowNewReturnFields], Source.[AllowNewReturnFields]) IS NOT NULL OR 
	NULLIF(Source.[AllowNewDefaults], Target.[AllowNewDefaults]) IS NOT NULL OR NULLIF(Target.[AllowNewDefaults], Source.[AllowNewDefaults]) IS NOT NULL OR 
	NULLIF(Source.[ObjNameLink], Target.[ObjNameLink]) IS NOT NULL OR NULLIF(Target.[ObjNameLink], Source.[ObjNameLink]) IS NOT NULL OR 
	NULLIF(Source.[ObjWhereLink], Target.[ObjWhereLink]) IS NOT NULL OR NULLIF(Target.[ObjWhereLink], Source.[ObjWhereLink]) IS NOT NULL OR 
	NULLIF(Source.[TargetIdLink], Target.[TargetIdLink]) IS NOT NULL OR NULLIF(Target.[TargetIdLink], Source.[TargetIdLink]) IS NOT NULL OR 
	NULLIF(Source.[Style], Target.[Style]) IS NOT NULL OR NULLIF(Target.[Style], Source.[Style]) IS NOT NULL OR 
	NULLIF(Source.[CSSClass], Target.[CSSClass]) IS NOT NULL OR NULLIF(Target.[CSSClass], Source.[CSSClass]) IS NOT NULL OR 
	NULLIF(Source.[LabelStyle], Target.[LabelStyle]) IS NOT NULL OR NULLIF(Target.[LabelStyle], Source.[LabelStyle]) IS NOT NULL OR 
	NULLIF(Source.[LabelCSSClass], Target.[LabelCSSClass]) IS NOT NULL OR NULLIF(Target.[LabelCSSClass], Source.[LabelCSSClass]) IS NOT NULL OR 
	NULLIF(Source.[DecimalPlaces], Target.[DecimalPlaces]) IS NOT NULL OR NULLIF(Target.[DecimalPlaces], Source.[DecimalPlaces]) IS NOT NULL OR 
	NULLIF(Source.[RootPath], Target.[RootPath]) IS NOT NULL OR NULLIF(Target.[RootPath], Source.[RootPath]) IS NOT NULL OR 
	NULLIF(Source.[FormatString], Target.[FormatString]) IS NOT NULL OR NULLIF(Target.[FormatString], Source.[FormatString]) IS NOT NULL OR 
	NULLIF(Source.[DirectTemplate], Target.[DirectTemplate]) IS NOT NULL OR NULLIF(Target.[DirectTemplate], Source.[DirectTemplate]) IS NOT NULL OR 
	NULLIF(Source.[Tag], Target.[Tag]) IS NOT NULL OR NULLIF(Target.[Tag], Source.[Tag]) IS NOT NULL OR 
	NULLIF(Source.[HelpId], Target.[HelpId]) IS NOT NULL OR NULLIF(Target.[HelpId], Source.[HelpId]) IS NOT NULL OR 
	NULLIF(Source.[ConnStringId], Target.[ConnStringId]) IS NOT NULL OR NULLIF(Target.[ConnStringId], Source.[ConnStringId]) IS NOT NULL OR 
	NULLIF(Source.[IsRequired], Target.[IsRequired]) IS NOT NULL OR NULLIF(Target.[IsRequired], Source.[IsRequired]) IS NOT NULL OR 
	NULLIF(Source.[IsRequiredMessage], Target.[IsRequiredMessage]) IS NOT NULL OR NULLIF(Target.[IsRequiredMessage], Source.[IsRequiredMessage]) IS NOT NULL OR 
	NULLIF(Source.[minValue], Target.[minValue]) IS NOT NULL OR NULLIF(Target.[minValue], Source.[minValue]) IS NOT NULL OR 
	NULLIF(Source.[minValueMessage], Target.[minValueMessage]) IS NOT NULL OR NULLIF(Target.[minValueMessage], Source.[minValueMessage]) IS NOT NULL OR 
	NULLIF(Source.[maxValue], Target.[maxValue]) IS NOT NULL OR NULLIF(Target.[maxValue], Source.[maxValue]) IS NOT NULL OR 
	NULLIF(Source.[maxValueMessage], Target.[maxValueMessage]) IS NOT NULL OR NULLIF(Target.[maxValueMessage], Source.[maxValueMessage]) IS NOT NULL OR 
	NULLIF(Source.[RegExp], Target.[RegExp]) IS NOT NULL OR NULLIF(Target.[RegExp], Source.[RegExp]) IS NOT NULL OR 
	NULLIF(Source.[RegExpText], Target.[RegExpText]) IS NOT NULL OR NULLIF(Target.[RegExpText], Source.[RegExpText]) IS NOT NULL OR 
	NULLIF(Source.[SQLValidator], Target.[SQLValidator]) IS NOT NULL OR NULLIF(Target.[SQLValidator], Source.[SQLValidator]) IS NOT NULL OR 
	NULLIF(Source.[ValidatorMessage], Target.[ValidatorMessage]) IS NOT NULL OR NULLIF(Target.[ValidatorMessage], Source.[ValidatorMessage]) IS NOT NULL OR 
	NULLIF(Source.[OnChangeJsFunction], Target.[OnChangeJsFunction]) IS NOT NULL OR NULLIF(Target.[OnChangeJsFunction], Source.[OnChangeJsFunction]) IS NOT NULL OR 
	NULLIF(Source.[OnChangeProcessName], Target.[OnChangeProcessName]) IS NOT NULL OR NULLIF(Target.[OnChangeProcessName], Source.[OnChangeProcessName]) IS NOT NULL OR 
	NULLIF(Source.[PlaceHolder], Target.[PlaceHolder]) IS NOT NULL OR NULLIF(Target.[PlaceHolder], Source.[PlaceHolder]) IS NOT NULL OR 
	NULLIF(Source.[IconName], Target.[IconName]) IS NOT NULL OR NULLIF(Target.[IconName], Source.[IconName]) IS NOT NULL OR 
	NULLIF(Source.[ToolbarName], Target.[ToolbarName]) IS NOT NULL OR NULLIF(Target.[ToolbarName], Source.[ToolbarName]) IS NOT NULL OR 
	NULLIF(Source.[Separator], Target.[Separator]) IS NOT NULL OR NULLIF(Target.[Separator], Source.[Separator]) IS NOT NULL OR 
	NULLIF(Source.[CascadeDependencies], Target.[CascadeDependencies]) IS NOT NULL OR NULLIF(Target.[CascadeDependencies], Source.[CascadeDependencies]) IS NOT NULL OR 
	NULLIF(Source.[RootPathType], Target.[RootPathType]) IS NOT NULL OR NULLIF(Target.[RootPathType], Source.[RootPathType]) IS NOT NULL OR 
	NULLIF(Source.[ImageCompressionType], Target.[ImageCompressionType]) IS NOT NULL OR NULLIF(Target.[ImageCompressionType], Source.[ImageCompressionType]) IS NOT NULL OR 
	NULLIF(Source.[ImageMaxWidth], Target.[ImageMaxWidth]) IS NOT NULL OR NULLIF(Target.[ImageMaxWidth], Source.[ImageMaxWidth]) IS NOT NULL OR 
	NULLIF(Source.[ImageMaxHeight], Target.[ImageMaxHeight]) IS NOT NULL OR NULLIF(Target.[ImageMaxHeight], Source.[ImageMaxHeight]) IS NOT NULL OR 
	NULLIF(Source.[Offline], Target.[Offline]) IS NOT NULL OR NULLIF(Target.[Offline], Source.[Offline]) IS NOT NULL OR 
	NULLIF(Source.[ExtensionId], Target.[ExtensionId]) IS NOT NULL OR NULLIF(Target.[ExtensionId], Source.[ExtensionId]) IS NOT NULL OR 
	NULLIF(Source.[Autoselect], Target.[Autoselect]) IS NOT NULL OR NULLIF(Target.[Autoselect], Source.[Autoselect]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [Hide] = Source.[Hide], 
  [ParamTypeId] = Source.[ParamTypeId], 
  [TableName] = Source.[TableName], 
  [ObjectName] = Source.[ObjectName], 
  [DefaultValue] = Source.[DefaultValue], 
  [Label] = Source.[Label], 
  [IOTypeId] = Source.[IOTypeId], 
  [PositionX] = Source.[PositionX], 
  [PositionY] = Source.[PositionY], 
  [Width] = Source.[Width], 
  [Height] = Source.[Height], 
  [TypeId] = Source.[TypeId], 
  [Locked] = Source.[Locked], 
  [CustomPropName] = Source.[CustomPropName], 
  [Mask] = Source.[Mask], 
  [SQlSentence] = Source.[SQlSentence], 
  [SQLFilter] = Source.[SQLFilter], 
  [SQLValueField] = Source.[SQLValueField], 
  [SQLDisplayField] = Source.[SQLDisplayField], 
  [SQLObjectName] = Source.[SQLObjectName], 
  [SQLViewName] = Source.[SQLViewName], 
  [SQLOfflineSentence] = Source.[SQLOfflineSentence], 
  [SQLOfflineOrderBy] = Source.[SQLOfflineOrderBy], 
  [WhereSentence] = Source.[WhereSentence], 
  [DetachedFromProcess] = Source.[DetachedFromProcess], 
  [SearchFunction] = Source.[SearchFunction], 
  [SearchCollection] = Source.[SearchCollection], 
  [SearchWhere] = Source.[SearchWhere], 
  [SearchReturnFields] = Source.[SearchReturnFields], 
  [SecurityObject] = Source.[SecurityObject], 
  [AllowNew] = Source.[AllowNew], 
  [AllowNewFunction] = Source.[AllowNewFunction], 
  [AllowNewReturnFields] = Source.[AllowNewReturnFields], 
  [AllowNewDefaults] = Source.[AllowNewDefaults], 
  [ObjNameLink] = Source.[ObjNameLink], 
  [ObjWhereLink] = Source.[ObjWhereLink], 
  [TargetIdLink] = Source.[TargetIdLink], 
  [Style] = Source.[Style], 
  [CSSClass] = Source.[CSSClass], 
  [LabelStyle] = Source.[LabelStyle], 
  [LabelCSSClass] = Source.[LabelCSSClass], 
  [DecimalPlaces] = Source.[DecimalPlaces], 
  [RootPath] = Source.[RootPath], 
  [FormatString] = Source.[FormatString], 
  [DirectTemplate] = Source.[DirectTemplate], 
  [Tag] = Source.[Tag], 
  [HelpId] = Source.[HelpId], 
  [ConnStringId] = Source.[ConnStringId], 
  [IsRequired] = Source.[IsRequired], 
  [IsRequiredMessage] = Source.[IsRequiredMessage], 
  [minValue] = Source.[minValue], 
  [minValueMessage] = Source.[minValueMessage], 
  [maxValue] = Source.[maxValue], 
  [maxValueMessage] = Source.[maxValueMessage], 
  [RegExp] = Source.[RegExp], 
  [RegExpText] = Source.[RegExpText], 
  [SQLValidator] = Source.[SQLValidator], 
  [ValidatorMessage] = Source.[ValidatorMessage], 
  [OnChangeJsFunction] = Source.[OnChangeJsFunction], 
  [OnChangeProcessName] = Source.[OnChangeProcessName], 
  [PlaceHolder] = Source.[PlaceHolder], 
  [IconName] = Source.[IconName], 
  [ToolbarName] = Source.[ToolbarName], 
  [Separator] = Source.[Separator], 
  [CascadeDependencies] = Source.[CascadeDependencies], 
  [RootPathType] = Source.[RootPathType], 
  [ImageCompressionType] = Source.[ImageCompressionType], 
  [ImageMaxWidth] = Source.[ImageMaxWidth], 
  [ImageMaxHeight] = Source.[ImageMaxHeight], 
  [Offline] = Source.[Offline], 
  [ExtensionId] = Source.[ExtensionId], 
  [Autoselect] = Source.[Autoselect], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([ProcessName],[ParamName],[Hide],[ParamTypeId],[TableName],[ObjectName],[DefaultValue],[Label],[IOTypeId],[PositionX],[PositionY],[Width],[Height],[TypeId],[Locked],[CustomPropName],[Mask],[SQlSentence],[SQLFilter],[SQLValueField],[SQLDisplayField],[SQLObjectName],[SQLViewName],[SQLOfflineSentence],[SQLOfflineOrderBy],[WhereSentence],[DetachedFromProcess],[SearchFunction],[SearchCollection],[SearchWhere],[SearchReturnFields],[SecurityObject],[AllowNew],[AllowNewFunction],[AllowNewReturnFields],[AllowNewDefaults],[ObjNameLink],[ObjWhereLink],[TargetIdLink],[Style],[CSSClass],[LabelStyle],[LabelCSSClass],[DecimalPlaces],[RootPath],[FormatString],[DirectTemplate],[Tag],[HelpId],[ConnStringId],[IsRequired],[IsRequiredMessage],[minValue],[minValueMessage],[maxValue],[maxValueMessage],[RegExp],[RegExpText],[SQLValidator],[ValidatorMessage],[OnChangeJsFunction],[OnChangeProcessName],[PlaceHolder],[IconName],[ToolbarName],[Separator],[CascadeDependencies],[RootPathType],[ImageCompressionType],[ImageMaxWidth],[ImageMaxHeight],[Offline],[ExtensionId],[Autoselect],[OriginId])
 VALUES(Source.[ProcessName],Source.[ParamName],Source.[Hide],Source.[ParamTypeId],Source.[TableName],Source.[ObjectName],Source.[DefaultValue],Source.[Label],Source.[IOTypeId],Source.[PositionX],Source.[PositionY],Source.[Width],Source.[Height],Source.[TypeId],Source.[Locked],Source.[CustomPropName],Source.[Mask],Source.[SQlSentence],Source.[SQLFilter],Source.[SQLValueField],Source.[SQLDisplayField],Source.[SQLObjectName],Source.[SQLViewName],Source.[SQLOfflineSentence],Source.[SQLOfflineOrderBy],Source.[WhereSentence],Source.[DetachedFromProcess],Source.[SearchFunction],Source.[SearchCollection],Source.[SearchWhere],Source.[SearchReturnFields],Source.[SecurityObject],Source.[AllowNew],Source.[AllowNewFunction],Source.[AllowNewReturnFields],Source.[AllowNewDefaults],Source.[ObjNameLink],Source.[ObjWhereLink],Source.[TargetIdLink],Source.[Style],Source.[CSSClass],Source.[LabelStyle],Source.[LabelCSSClass],Source.[DecimalPlaces],Source.[RootPath],Source.[FormatString],Source.[DirectTemplate],Source.[Tag],Source.[HelpId],Source.[ConnStringId],Source.[IsRequired],Source.[IsRequiredMessage],Source.[minValue],Source.[minValueMessage],Source.[maxValue],Source.[maxValueMessage],Source.[RegExp],Source.[RegExpText],Source.[SQLValidator],Source.[ValidatorMessage],Source.[OnChangeJsFunction],Source.[OnChangeProcessName],Source.[PlaceHolder],Source.[IconName],Source.[ToolbarName],Source.[Separator],Source.[CascadeDependencies],Source.[RootPathType],Source.[ImageCompressionType],Source.[ImageMaxWidth],Source.[ImageMaxHeight],Source.[Offline],Source.[ExtensionId],Source.[Autoselect],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO








BEGIN TRY

MERGE INTO [Security_Navigation_Nodes_Roles] AS Target
USING (VALUES
  (N'2BC2E79B-BFAC-4CAA-B0DD-1D1C6758A6B7',N'users',0,1)
 ,(N'54066E2E-9EED-444E-A0AE-1E9D18E7E941',N'users',0,1)
 ,(N'3468C35B-9A5A-4F9E-9E03-2D6A1CBED087',N'users',0,1)
 ,(N'03A7CEF4-1668-4A82-A5C0-889472EB9ADF',N'users',0,1)
) AS Source ([NodeId],[RoleId],[CanView],[OriginId])
ON (Target.[NodeId] = Source.[NodeId] AND Target.[RoleId] = Source.[RoleId])
WHEN MATCHED AND (
	NULLIF(Source.[CanView], Target.[CanView]) IS NOT NULL OR NULLIF(Target.[CanView], Source.[CanView]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [CanView] = Source.[CanView], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([NodeId],[RoleId],[CanView],[OriginId])
 VALUES(Source.[NodeId],Source.[RoleId],Source.[CanView],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO










-- MERLOS
update Skins_Variables set VarValue='f0f1f1' where SkinId='flexygo' and VarName='bgColor'
update Skins_Variables set VarValue='ccd5dd' where SkinId='flexygo' and VarName='borderModuleColor'
update Skins_Variables set VarValue='ba362f' where SkinId='flexygo' and VarName='DangerColor'
update Skins_Variables set VarValue='0b5ba1' where SkinId='flexygo' and VarName='DialogHeaderColor'
update Skins_Variables set VarValue='323f4b' where SkinId='flexygo' and VarName='HeaderColor'
update Skins_Variables set VarValue='1f8eee' where SkinId='flexygo' and VarName='InfoColor'
update Skins_Variables set VarValue='1400px' where SkinId='flexygo' and VarName='large'
update Skins_Variables set VarValue='993px' where SkinId='flexygo' and VarName='medium'
update Skins_Variables set VarValue='FFFFFF' where SkinId='flexygo' and VarName='MenuColor'
update Skins_Variables set VarValue='FFFFFF' where SkinId='flexygo' and VarName='ModuleColor'
update Skins_Variables set VarValue='323f4b' where SkinId='flexygo' and VarName='ModuleHeaderColor'
update Skins_Variables set VarValue='323f4b' where SkinId='flexygo' and VarName='NavColor'
update Skins_Variables set VarValue='0b5ba1' where SkinId='flexygo' and VarName='NotifyColor'
update Skins_Variables set VarValue='2DB7B0' where SkinId='flexygo' and VarName='OutstandingColor'
update Skins_Variables set VarValue='616e7c' where SkinId='flexygo' and VarName='PrimaryColor'
update Skins_Variables set VarValue='729px' where SkinId='flexygo' and VarName='small'
update Skins_Variables set VarValue='107c10' where SkinId='flexygo' and VarName='SuccessColor'
update Skins_Variables set VarValue='ffffff' where SkinId='flexygo' and VarName='TimelineBackgroundColor'
update Skins_Variables set VarValue='3C3C46' where SkinId='flexygo' and VarName='TimelineDarkColor'
update Skins_Variables set VarValue='f1f3f4' where SkinId='flexygo' and VarName='TimelineLightColor'
update Skins_Variables set VarValue='4e4e57' where SkinId='flexygo' and VarName='TimelineMediumDarkColor'
update Skins_Variables set VarValue='bdc1c6' where SkinId='flexygo' and VarName='TimelineMediumLightColor'
update Skins_Variables set VarValue='80397b' where SkinId='flexygo' and VarName='ToolsColor'
update Skins_Variables set VarValue='777777' where SkinId='flexygo' and VarName='txtColor'
update Skins_Variables set VarValue='FFFFFF' where SkinId='flexygo' and VarName='txtDangerColor'
update Skins_Variables set VarValue='FFFFFF' where SkinId='flexygo' and VarName='txtDialogHeaderColor'
update Skins_Variables set VarValue='DDDDDD' where SkinId='flexygo' and VarName='txtHeaderColor'
update Skins_Variables set VarValue='FFFFFF' where SkinId='flexygo' and VarName='txtInfoColor'
update Skins_Variables set VarValue='323f4b' where SkinId='flexygo' and VarName='txtMenuColor'
update Skins_Variables set VarValue='777777' where SkinId='flexygo' and VarName='txtModuleColor'
update Skins_Variables set VarValue='F6F6F6' where SkinId='flexygo' and VarName='txtModuleHeaderColor'
update Skins_Variables set VarValue='DDDDDD' where SkinId='flexygo' and VarName='txtNavColor'
update Skins_Variables set VarValue='FFFFFF' where SkinId='flexygo' and VarName='txtNotifyColor'
update Skins_Variables set VarValue='FFFFFF' where SkinId='flexygo' and VarName='txtOutstandingColor'
update Skins_Variables set VarValue='DDDDDD' where SkinId='flexygo' and VarName='txtPrimaryColor'
update Skins_Variables set VarValue='FFFFFF' where SkinId='flexygo' and VarName='txtSuccessColor'
update Skins_Variables set VarValue='FFFFFF' where SkinId='flexygo' and VarName='txtToolsColor'
update Skins_Variables set VarValue='FFFFFF' where SkinId='flexygo' and VarName='txtWarningColor'
update Skins_Variables set VarValue='ee9e1f' where SkinId='flexygo' and VarName='WarningColor'
-- AutoUpdate BBDD Configuración - NuGet
Update Settings set SettingValue=N'TraspasoPedidosVenta'  where SettingName=N'AutoUpdatePackageId'
Update Settings set SettingValue=N'https://packages.nuget.org/api/v2' where SettingName=N'AutoUpdateURL'
Update Settings Set [SettingValue]=N'MTraspasoPedidosVenta' Where [Settings].[SettingName] = N'ProjectName'




-- AutoUpdate BBDD Datos
Update DbConnectionStrings set UpdateDataModel=1, DacpacName='TraspasoPedidosVenta_DataBD', Active=1 
where ConnStringId='DataConnectionString'




-- sysUser
update Objects_Properties set Label='Usuario' where ObjectName='sysUser' and PropertyName='separator2'
update Objects_Properties set PositionY=1 , PositionX=6 , Hide=1 where ObjectName='sysUser' and PropertyName='Avatar'
update Objects_Properties set Label='Nombre' , PositionY=1 , PositionX=0 where ObjectName='sysUser' and PropertyName='Name'
update Objects_Properties set Label='Apellidos' , PositionY=1 , PositionX=2 where ObjectName='sysUser' and PropertyName='SurName'
update Objects_Properties set Label='Usuario' , PositionY=1 , PositionX=0 where ObjectName='sysUser' and PropertyName='UserName'
update Objects_Properties set PositionY=1, PositionX=8, Hide=1 where ObjectName='sysUser' and PropertyName='NickName'
update Objects_Properties set Label='Email Confirmed', PositionY=2, PositionX=8, Hide=1 where ObjectName='sysUser' and PropertyName='EmailConfirmed'
update Objects_Properties set PositionY=2 , PositionX=2 where ObjectName='sysUser' and PropertyName='Email'
update Objects_Properties set Hide=1 where ObjectName='sysUser' and PropertyName='separator1'
update Objects_Properties set Hide=1 where ObjectName='sysUser' and PropertyName='PhoneNumber'
update Objects_Properties set Hide=1 where ObjectName='sysUser' and PropertyName='PhoneNumberConfirmed'
update Objects_Properties set Label='Seguridad' where ObjectName='sysUser' and PropertyName='separator4'
update Objects_Properties set Label='Rol' where ObjectName='sysUser' and PropertyName='RoleId'
update Objects_Properties set Label='Perfil' where ObjectName='sysUser' and PropertyName='ProfileName'
update Objects_Properties set DefaultValue='es-ES', Hide=1 where ObjectName='sysUser' and PropertyName='CultureId'
update Objects_Properties set Hide=1 where ObjectName='sysUser' and PropertyName='TwoFactorEnabled'
update Objects_Properties set Hide=1 where ObjectName='sysUser' and PropertyName='LockoutEnabled'
update Objects_Properties set Hide=1 where ObjectName='sysUser' and PropertyName='LockoutEndDateUtc'
update Objects_Properties set Hide=1 where ObjectName='sysUser' and PropertyName='AccessFailedCount'
update Objects_Properties set Label='Referencias Externas' where ObjectName='sysUser' and PropertyName='separator5'
update Objects_Properties set Label='Código Sage 50' where ObjectName='sysUser' and PropertyName='Reference'
update Objects_Properties set Hide=1 where ObjectName='sysUser' and PropertyName='SubReference'
update Objects_Properties set Hide=1 where ObjectName='sysUser' and PropertyName='separator3'
update Objects_Properties set Hide=1 where ObjectName='sysUser' and PropertyName='separator6'
update Objects_Properties set Hide=1 where ObjectName='sysUser' and PropertyName='IPGroup'
update Objects_Properties set Hide=1 where ObjectName='sysUser' and PropertyName='MailAccountId'




-- Email Plantailla CONFIRM
update [Mails_Templates]
set Body='
	<meta name="viewport" content="width=device-width">
    <link href="https://fonts.googleapis.com/css?family=Hind:300,400,500,600,700" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Basic</title>

    <style type="text/css">
*,h1,h2,h3,h4,h5,h6{font-family:Hind,sans-serif}*,.collapse{padding:0}.btn,.social .soc-btn{font-size:14px;text-align:center}*{margin:0}img{max-width:100%}body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%!important;height:100%}a{color:#2ba6cb}.btn{display:inline-block;padding:6px 12px;margin-bottom:0;font-weight:400;line-height:1.428571429;white-space:nowrap;vertical-align:middle;cursor:pointer;border:1px solid #CCC;border-radius:4px;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;-o-user-select:none;user-select:none;color:#333;background-color:#fff}p.callout{padding:15px;background-color:#e4f4f2;margin-bottom:15px}.callout a{font-weight:700;color:#2ba6cb}table.social{background-color:#fff}.social .soc-btn{padding:3px 7px;border-radius:3px;-webkit-border-radius:15px;-moz-border-radius:15px;margin-bottom:10px;text-decoration:none;color:#FFF;font-weight:300;display:block}a.fb{background-color:#3b5998!important}a.tw{background-color:#1daced!important}a.gp{background-color:#db4a39!important}a.ms{background-color:#000!important}.sidebar .soc-btn{display:block;width:100%}table.head-wrap{width:100%}.header.container table td.logo{padding:15px}.header.container table td.label{padding:15px 15px 15px 0}table.body-wrap{width:100%}table.footer-wrap{width:100%;clear:both!important}.footer-wrap .container td.content p{border-top:1px solid #d7d7d7;padding-top:15px;font-size:10px;font-weight:700}h1,h2{font-weight:200}h1,h2,h3,h4,h5,h6{line-height:1.1;margin-bottom:16px;color:#353b46}h1 small,h2 small,h3 small,h4 small,h5 small,h6 small{font-size:60%;color:#353b46;line-height:0;text-transform:none}h1{font-size:44px}h2{font-size:37px}h3{font-weight:600;font-size:27px}h4{font-weight:500;font-size:23px}h5,h6{font-weight:900}h5{font-size:15px;color:#4a5362}h6,p,ul{font-size:14px}h6{text-transform:uppercase;color:#444}.collapse{margin:0!important}p,ul{margin-bottom:10px;font-weight:400;line-height:1.6}p.lead{font-size:17px;font-weight:500}p.last{margin-bottom:0}ul li{margin-left:5px;list-style-position:inside}ul.sidebar li,ul.sidebar li a{display:block;margin:0}ul.sidebar{background:#ebebeb;display:block;list-style-type:none}ul.sidebar li a{text-decoration:none;color:#666;padding:10px 16px;cursor:pointer;border-bottom:1px solid #777;border-top:1px solid #fff}.column tr td,.content{padding:15px}ul.sidebar li a.last{border-bottom-width:0}ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p{margin-bottom:0!important}.container{display:block!important;max-width:600px!important;margin:0 auto!important;clear:both!important}.content{max-width:600px;margin:0 auto;display:block}.content table{width:100%}.column{width:300px;float:left}.column-wrap{padding:0!important;margin:0 auto;max-width:600px!important}.column table{width:100%}.social .column{width:280px;min-width:279px;float:left}.clear{display:block;clear:both}@media only screen and (max-width:600px){a[class=btn]{display:block!important;margin-bottom:10px!important;background-image:none!important;margin-right:0!important}div[class=column]{width:auto!important;float:none!important}table.social div[class=column]{width:auto!important}}
    </style>


<div style="text-align:center;"><p><span style="FONT-SIZE: 48pt; FONT-FAMILY: Oxygen; COLOR: #17365d"></span>&nbsp;<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAACACAYAAAAlHj/jAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgADIKlJREFUeAEA//8AAAH///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAWSlwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//z8AAkbGwAGExMA//39AAAAAAMAAAAtAAAAYwAAAFAAAAAPAAAA8gAAAO4AAADpAAAA5QAAAN4AAADAAAAAygAAAPkAAAD/AAAAAAAAAAAA/f0ACCYlAAEDAwAA/v4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIHBwDz1NUA89PTAAIHBwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADqtqQAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAEAAAADwAAAAAAAADwAAAA/QAAAP8BBAQA9+XlAAAAAAAGExMA//39/QAAANAAAABtAAAAtQAAAH0AAAA3AAAAGwAAAC0AAAAAAAAAAAAAAAAAAAC5AAAAiAAAAMYAAAD6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAANAAAASAAAAKgAAADbAAAA2gAAAKwAAABEAAAABwAAAAEBBAQA9+XlAPrt7QAAAQEAAAAAAAAAAAAAAAC4AAAAPQAAAHwAAADsAAAA/wAAAAAAAAAAAAAAAAAAAEcAAAC/AAAAgAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAcAAAAeQAAANAAAAC2AAAAUgAAABAAAAARAAAATwAAALoAAAC1AAAANwAAAAUBBAQA+OnpAPLT0wD78fEAAQEBAAAAAAAAAAD+AAAAiAAAAE4AAADVAAAA/gAAAAAAAAAAAAAAAAAAAAAAAAB5AAAAswAAACQAAAACAP7+AAAAAAIAAAAPAAAAEAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAAYAAAAbgAAAMQAAACCAAAAIgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEMAAAC2AAAAcwAAABIAAAABAQUFAPfj4wD68PAAAAAAAAAAAAAAAAAAAAAAxwAAAFEAAAC3AAAA/AAAAAAAAAAAAAAAAAAAAAAAAAA8AAAAswACAj343N0E//z8/gEDA/UAAAAeAAAASAAAAB8AAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAOgAAAK4AAACJAAAAHgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABEAAACHAAAAnwAAACYAAAACAQUFAPjn5wDy1NQA+/LyAAECAgAAAAAAAAAA3AAAAE4AAACnAAAA+AAAAAAAAAAAAAAAAAAAAAAAAAAoAAEBtgACAkb429wG//v8+//+/tIAAADjAAAAgAAAAFYAAAALAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAADgAAAAZgAAAFwAAAC+AAAA9QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABOAAAAswAAAEMAAAAEAAEBAAEFBQD34uIA+/HxAAEBAQAAAAAAAAAA/gAAAF4AAACQAAAA8gAAAAAAAAAAAAAAAAAAAAAAAAAKAAEBswACAlj33NwG+NzeAP/9/dEAAQFyAAAAUwAAAKwAAAAgAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+AAAAOMAAADUAAAAqQAAAEUAAABmAAAA0gAAAPoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlAAAAqQAAAFYAAAAGAAAAAAEFBQD24eEA+/LyAAEBAQAAAAAAAAAAAAAAAHkAAACSAAAA8QAAAAAAAAAAAAAAAAAAAAAAAAACAAEBngACAlcAAgEF+N3eAP/9/e8AAQFXAAAA8AAAAMUAAABIAAAABwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAKYAAAA+AAAAmAAAAPMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARAAAApQAAAGsAAAAJAAAAAAEFBQD14eEA+/LyAAABAQAAAAAAAAAAAAAAAH0AAACMAAAA7wAAAAAAAAAAAAAAAAAAAAAAAAADAAAAogAAAFQAAgIG+N3eAP/8/fwAAQFZAAAAuQAAALMAAAB2AAAACwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPEAAABvAAAAZQAAANoAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAjgAAAHsAAAARAAAAAAIFBQD14OAA/PLyAAABAQAAAAAAAAAAAAAAAIQAAACFAAAA7wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAowAAAFgAAQED997eAP/9/QAAAgJiAAAAmQAAAIIAAACPAAAADwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACpAAAAWwAAAMkAAAD7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAewAAAHYAAAARAAAAAAIGBgD14OAA+/LyAAABAQAAAAAAAAAAAAAAAI0AAACOAAAA8QAAAAAAAAAAAAAAAAAAAAAAAAADAAAAmQAAAEoBAQEC993eAP/8/AAAAgJsAAAAowAAAGUAAACbAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8AAAAYAAAAFAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADMAAAAUAAAAK4AAAD5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAeAAAAIQAAAAXAAAAAAIGBgD24OAA/PPzAAABAQAAAAAAAAAAAAAAAIQAAACUAAAA8wAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAqgAAADwBAQEB+N3eAP/8/P0AAgJmAAAAuQAAAFUAAACcAAAADgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAfAAAAkQAAAL0AAACnAAAAbQAAACMAAAAGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADpAAAAZgAAAJ8AAAD4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAagAAAH4AAAAWAAAAAAIGBgD14OAA/PPzAAABAQAAAAAAAAAAAAAAAHwAAACUAAAA9wAAAAAAAAAAAAAAAAAAAAAAAAAIAAAApgAAADUAAQEA+N7fAP/8/PgAAABcAAAAxAAAAFMAAACJAAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFgAAAIEAAABfAAAAKgAAAEQAAACKAAAAvAAAAGAAAAAXAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADyAAAAcgAAAJkAAAD2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAagAAAHQAAAASAAAAAAIFBQD14OAA/PPzAAABAQAAAAAAAAAAAAAAAHoAAACtAAAA+wAAAAAAAAAAAAAAAAAAAAAAAAAbAAAAoAAAABkAAAAA+N7fAAEDA+wAAABmAAAA5gAAAGgAAABzAAAABQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoAAABxAAAAXwAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAmQAAAKcAAAA9AAAABwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD3AAAAcAAAAJIAAAD0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdQAAAHwAAAARAAAAAAIFBQD1398A/PPzAAABAQAAAAAAAAAAAAAAAF0AAAC0AAAA/gAAAAAAAAAAAAAAAAAAAAAAAAApAAAApQAAABEBAgIA/vn6AAABAdcAAABXAAAA8AAAAHgAAABRAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUAAAAMAAAAEQAAABUAAAAYAAAAGAAAABgAAAAUAAAAEAAAAAsAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcAAAAOAAAAEwAAABcAAAAZAAAAGAAAABYAAAASAAAADQAAAAYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARwAAAHgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQQAAAC8AAAB7AAAAqAAAAPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAegAAAPgAAAB1AAAACwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcQAAAO8AAACPAAAA9AIGBgD1398ACyIiAAQMDAAA//8AAAAAAAAAAGYAAAARAAAAQAAAAAAAAAAAAAAAAAAAAAAAAABBAAAAiv/+/nH44OD0CCAfAAEGBscA/v57AAAAjQAAAI8AAADXAAAA0wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQAAAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABwAAABQAAAAgAAAAKwAAACAAAAAWAAAAEwAAAA0AAAACAAAA/QAAAPAAAADuAAAA6QAAAN4AAADSAAAA5gAAAOwAAAD8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABgAAAAgAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAA/AAAAPgAAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAACwAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAA+AAAAPwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABwAAABYAAAAlAAAALwAAAB8AAAAWAAAAEgAAAAcAAAD+AAAA9QAAAO8AAADrAAAA4AAAANMAAADeAAAA6wAAAPgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACQAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAPwAAAD4AAAA/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABwAAAAcAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAA+wAAAPgAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAACgAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAD+AAAA+AAAAPwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAA/AAAAPYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHAAAACAAAAP4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAP8AAAD9AAAA/gAAAP0AAAD9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACYAAACLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEgAAAJcAAAAsAAAAhgAAAOYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAAgQAAAAQAAABxAAAACwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAggAAAPMAAACXAAAA+QIGBgD1398ACyAgAAQNDQAA//8AAAAA7AAAAGEAAAC7AAAAJAAAAAAAAAAAAAAAAAAAAAAAAABkAAAA3wD//4n44eH9CCAg/QEFBZoA/v4lAAAAcAAAAJAAAABVAAAA7gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEA/vf3AP329gAAAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACQAAAB0AAABGAAAAVgAAADwAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPgAAAC/AAAAqgAAAMEAAADjAAAA/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA2AAAAUgAAAPoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUAAADcAAAArgAAAOkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAkAAAByAAAACgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAPcAAACvAAAA0QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAACQAAABGAAAAXQAAADQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAM0AAACmAAAAugAAANwAAAD4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABYAAAAJgAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFAAAA2QAAAKsAAADvAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABHAAAAPQAAAP4AAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAADWAAAAqgAAAPQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABIAAABwAAAABQAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAO4AAACxAAAA1gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFcAAAApAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAADkAAAAlgAAAPwAAAAAAAAAAAAAAAAAAAAAAAAABQAAAEMAAABGAAAA+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD6AAAA/AAAAPcAAAD2AAAA9AAAAO4AAADvAAAA7gAAAPYAAADwAAAA8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf///wAAAAAAFkpcAAAAAAAAAAAAAAAAAAAAAAAAAAAPAAAAngAAAFIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC9AAAAbAAAANgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIcAAABuAAAACgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGMAAAChAAAA/QAAAAD++/sACyEhAAQMDAAA//8AAAAAAAAAADoAAACtAAAAGAAAAAAAAAAAAAAAAAAAAAAAAAD7AAAAVAD//7IAAQIACCAeAAEFBRIA//+yAAAAOwAAAI4AAAB1AAAA/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIICADz0NEA8tLSAAMMDAAA//8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABsAAABcAAAAbgAAABoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA2QAAAIoAAACyAAAA7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHoAAACFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANMAAABdAAAA0QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFgAAAOkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAGEAAAChAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8AAABSAAAAgAAAAB4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADsAAAAiQAAAKUAAADnAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMUAAAA6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADMAAAAWAAAAN0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJ4AAABhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMYAAABTAAAA6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKQAAANYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8gAAAGYAAACpAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwwAAADwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAN0AAAArAAAA+QAAAAAAAAAAAAAAAAAAAAAAAAAJAAAAiwAAAGsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADzAAAA4QAAAMIAAACzAAAA0wAAAOUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6rakAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQAAAEwAAABSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEMAAACpAAAAPgAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAfwAAAJgAAAD5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlQAAAEsAAAAAAAAAAAIFBQD1398A/PPzAAAAAAAAAAAAAAAAxgAAAG8AAADzAAAAAAAAAAAAAAAAAAAAAAAAAAUAAACXAAEBMAD+/QD44+MAAQQE7gAAAGkAAADqAAAAYQAAAD4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP//AAEEBAABBAQAAP//AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAxAAAAngAAAIgAAAAaAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAnAAAAnQAAAIwAAAAfAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8QAAAPsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+AAAA6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADxAAAA9QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWAAAAggAAAJ4AAAAeAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQAAACLAAAAkAAAACcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA7QAAAP4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPMAAAD8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA7wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA7wAAAP0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD9AAAA8gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwAAAA9wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA7QAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP4AAAD1AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA0AAAAsAAAAagAAALcAAACcAAAASQAAAAYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAANQAAAK4AAADNAAAA6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC4AAACgAAAASQAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD6AAAAdgAAAKUAAAD7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAmAAAADsAAAAAAAAAAAIFBQD24OAA////AAAAAAAAAAAAAAAAqgAAAI0AAAD8AAAAAAAAAAAAAAAAAAAAAAAAABoAAACVAAEBFQEBAgD+9/cAAQIC1gAAAHoAAAANAAAAhgAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANgAAAKgAAABGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXwAAAJ8AAAAbAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEgAAAIMAAABuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABWAAAAiwAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEgAAACzAAAAZAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPgAAAB1AAAAUwAAAEIAAABuAAAAtAAAAPEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACEAAACbAAAAUgAAAAUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD9AAAAdAAAAK8AAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZAAAAlgAAACoAAAAAAAAAAAEEBAD99vYAAAEBAAAAAAAAAAAAAAAAgQAAAKwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADkAAQGE//v7Bfnh4gD++fr9AQICpwAAAKYAAAA5AAAAZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC4AAACdAAAAJgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQQAAAJIAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcAAABqAAAAZgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABNAAAAeQAAAA0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAACVAAAAYQAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAD//v4A//7+AP/+/gAAAAAAAAAA9wAAAPsAAAAAAAAA8gAAAKsAAABQAAAAggAAANcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABoAAACWAAAAWAAAAAcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADvAAAAdwAAALoAAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuAAAAjQAAABwAAAAAAAEBAPbf3wD88/MAAQICAAAAAAAAAAAAAAAAbAAAAMcAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAGEAAQFX//7+APnh4gD++vn2AAEBhgAAAOUAAABkAAAAEwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARAAAAjAAAACwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUAAAAG4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAPAAAAG8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABmAAAARQAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACSAAAAOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAUNDQAFDQ0ABAwMAP/+/gD//v4AAAAAAAAAAAAAAAAAAAAAAAAAAP0AAACOAAAAVQAAALEAAAD3AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQAAACQAAAAWgAAAAUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgAAAAcwAAAMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABIAAAAewAAABAAAAAAAQUFAPbg4AD78/MAAQICAAAAAAAAAADtAAAAZQAAAOYAAAAAAAAAAAAAAAAAAAAAAAAABQAAAIsAAQEv//38APnk5gACBQXZAAAAeAAAACMAAABrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGUAAAA6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAagAAACYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABEAAABiAAAAFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4AAAAGQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAACEAAAADwAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAACiAgAAogIAALICAABQwMAAQMDAD//f0A//7+AP/+/gD//v4AAAAAAAAAAAAAAADVAAAAVwAAAJEAAADtAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABIAAACJAAAAWwAAAAYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADWAAAAewAAANcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABoAAAAbgAAAAMAAAAAAQUFAPbg4AD78/MAAAAAAAAAAAAAAADVAAAAdAAAAPYAAAAAAAAAAAAAAAAAAAAAAAAAGAAAAJAAAgIRAAAAAP319QABAwOyAAAAsQAAAGAAAAAKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAbAAAAWQAAAAsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUAAAAfgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKQAAAEkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADtAAAA7AAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADUAAAArAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAABEAAAAAgAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAD//PwA//z8AP/9/QAKISEACx8fAAQNDQAFDg4ABQ4OAAQMDAD//f0AAAAAAAAAAAAAAAD5AAAAeQAAAHwAAADfAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABUAAACMAAAAWQAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAAAAAegAAAOYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACBAAAAUgAAAAAAAAAAAQQEAPbg4AABAgIAAAAAAAAAAAAAAACnAAAAmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOgABAXL/+voA+eHiAP74+PIAAQGQAAAA/AAAAFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEwAAAAiAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/QAAAOMAAADYAAAA4AAAAPsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAABCAAAAHwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAA1AAAAEQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADmAAAAmgAAAHkAAAB3AAAAkAAAAOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD4AAAAKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAA+wAAAPkAAAD9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGMAAAAFAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAP/8/AAAAAAADCEhAP8AAAAAAQEA//z8APbg4AD89fUAAQMDAAAAAAAAAAAAAAAAmAAAAOEAAACPAAAAJwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABMAAACHAAAA9QAAAK0AAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwAAAAHQAAAIYAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACWAAAA3gAAAM0AAAAAAQQEAPvy8gAFDg4A//7+AAAAAAAAAAB/AAAAMwAAAD8AAAAAAAAAAAAAAAAAAAAGAAAAbgACAr3//PzA+ebnAAomJcz++fmgAAAAWAAAAKgAAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMwAAAA4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOUAAACrAAAAvwAAAOkAAAARAAAAPQAAAFYAAAAkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAsAAABJAAAAmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFQAAADwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA7gAAAH8AAACcAAAAAAAAAAAAAAAAAAAAwwAAAJUAAAAZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQQAAAJMAAAD1AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMoAAAC8AAAA+wAAAAkAAAAZAAAAIgAAACQAAAAYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANgAAAAwAAAD5AAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+/PwAAAAAAAAAAAAAAQEACh8fAPbh4QD89PQAAQQEAAAAAAAAAAAAAAAAtwAAAPEAAACRAAAANAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABkAAACKAAAA9gAAALYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACTAAAAHgAAAHEAAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQAAACQAAAAWAAAAOUAAAAA9uLiAAoeHgAFDw8A//39AAAAAPQAAABuAAAAuQAAABgAAAAAAAAAAAAAAAAAAAAOAAAAgAACAlcAAADp/PP0+AIIB7QA/v40AAAAMQAAAM0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABUAAAAqAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD0AAAAeAAAAKQAAAAAAAAAAAAAAAAAAAC4AAAAvgAAAKcAAAAVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwAAAEAAAACYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAKwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwAAAAowAAAAAAAAAAAAAAAAAAAAAAAACvAAAARAAAAGgAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAArAAAADQAAAOgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAN0AAAC9AAAABgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAsAAADNAAAA8gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAzgAAANIAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAANoAAAD9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAtAAAAIQAAAAAAAAAAAAAAAAAAAASAAAARgAAAGoAAAA6AAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASAAAAHgAAAO0AAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAACQAAAC4AAAD3AAAA3QAAAPUAAAAAAAAAAAD//wAAAQEACh8fAPbh4QD89PQAAAICAAECAgAAAAAAAAAAxgAAAGIAAACaAAAAOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACEAAACMAAAA8wAAAMMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACCAAAAHAAAAFYAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwAAACDAAAAaAAAAPoBAwMA9uHhAAofHwAEDAwAAAAAAAAAANIAAAB9AAAAkwAAAAgAAAAAAAAAAAAAAAAAAAA6AAICcP/4+Hn65+cACicm3f74+LMAAABBAAAAzQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAABgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKoAAAClAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC9AAAAkwAAAIIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAAAALQAAAAoAAAD2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA0AAAAdAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/AAAAMoAAAD5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC+AAAAUgAAABgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4AAAAXAAAA2wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD8AAAAvwAAAGgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAClAAAAjgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJAAAAAQAAAP8AAAAAAAAAAAAAAO4AAACoAAAA0QAAAKcAAAAvAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAkAAAA0AAAAP8AAAAAAAAAAAIAAAAAAAAAAAAAAAEAAABaAAAAqAAAAKEAAACCAAAAOQAAAAcAAAAAAAAAAAD//wAAAgIACh4eAAUPDwAEDAwA//z8AAAAAAAAAAAAAAAA0wAAAF4AAADDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACkAAACLAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPMAAAB0AAAAxgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGEAAABhAAAAAAAAAAABBAQA9+PjAAEEBAAAAAAAAAAAAAAAAJ0AAACkAAAAAAAAAAAAAAAAAAAAAAAAAAUAAABeAAICNQD//wD98vP1AAMDvwAAAOcAAAAFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAkAAAAFQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAAnwAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACcAAAA8wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAALAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAAAAAsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD0AAAA7QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANEAAADiAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADXAAAArwAAAPgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABsAAAABAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAwAAAGwAAAAgAAAAMAAAAHIAAACoAAAAfQAAACQAAAAAAAAAAAD+/gAAAgIACyEhAAocHAAECwsA//z8AAAAAAAAAAAAAAAA2wAAAGMAAAC7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADcAAACQAAAAIgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOUAAAB8AAAA3QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIsAAABCAAAAAAAAAAABAwMA++/vAAEEBAAAAAAAAAAA/wAAAIUAAADXAAAAAAAAAAAAAAAAAAAAAAAAABoAAQGC//f3EPrn6AABBATpAAAAzgAAABIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABwAAAANAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAO0AAADhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOcAAADfAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcAAAAoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcAAAAPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5wAAAPEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACgAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADDAAAA6QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAAAAAEAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAATAAAAIgAAAAAAAAAAAAAAAAAAAB4AAAB7AAAAlQAAAEEAAAAAAAAAAAD//wD++/sAAQMDAAofHwAECwsA//z8AAAAAAAAAAAAAAAA2wAAAGkAAADBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD8AAACJAAAAFwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMEAAACAAAAA8QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADQAAAJAAAAAbAAAAAP///wD25OQA+/DwAAEEBAAAAAAAAAAA4AAAAHQAAAD3AAAAAAAAAAAAAAAAAAAAAAAAAD0AAwNcAQUEAAEBAfgAAQHqAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFgAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4AAAAPkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABwAAAB0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACQAAADcAAAAJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD5AAAABQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANwAAAD1AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAACYAAAAOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABGAAAApQAAAGYAAAAEAAAAAAAAAAD//v4AAQMDAAoeHgADCwsA//z8AAAAAAAAAAAAAAAA3QAAAGUAAADHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFQAAAB6AAAACwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKQAAACVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALQAAAIgAAAABAAAAAAECAgD24+MA+/DwAAABAQAAAAAAAAAAugAAAJ4AAAAAAAAAAAAAAAAAAAAAAAAACwABAWL/+vod/fPz/wEEBPAAAADvAAEBAAABAQAAAQEAAAEBAAABAQAAAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADuAAAA/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAA4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFAAAAFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+AAAAP4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASQAAAEgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAADNAAAAxwAAAMUAAADFAAAAxQAAAMUAAADFAAAAxQAAAMUAAADFAAAAyAAAAOIAAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9wAAAP0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPcAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAKgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZAAAAkgAAABgAAACSAAAA8AAAAAD//v4AAQQEAAoeHgD34uIA/PX1AAEEBAAAAAAAAAAA2wAAAGoAAACdAAAAMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGYAAADnAAAAkAAAAPgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIUAAAAtAAAATQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXAAAAL4AAACfAAAAAAECAgD35eUADywsAP77+wAAAAD8AAAAiAAAAD0AAAAuAAAAAAAAAAAAAAAAAAEBK/76+m754+N1Bh0d/QMMDP0A/v4AAAAAAAAAAAAAAAAAAAAAAAEGBgABBQUAAP7/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAAkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPIAAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADpAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAAJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD0AAAA8gAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcAAAAewAAAOUAAADKAAAAAAAAAAAAAAAAAAAA/QAAAIwAAAANAAAA/QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAA5AAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6wAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHAAAAgQAAAIQAAAAeAAAAAAAAAAD//v4AAgUFAAkdHQAEDAwA//z8AAAAAAAAAAAAAAAA0AAAAGwAAADVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAHkAAABVAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9gAAAHcAAADRAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAhwAAAC8AAAAAAAAAAAECAgD57e0AAQQEAAAAAAAAAADkAAAAegAAAPsAAAAAAAAAAAAAAAAAAAAGAAICUP77/CP65eUA/O3uAPvt7QD77e0A++3tAPvt7QD87u4A+ODgAPnp6QABBAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAABgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9AAAAP0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAABEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPMAAADhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAzAAAAlQAAAD8AAAAAAAAAAAAAAAAAAAAAAAAA6AAAANsAAADeAAAA3gAAAN4AAADeAAAA3gAAAN4AAADeAAAA3gAAANoAAAD2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABYAAAAHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgAAAA/wAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAACYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAawAAABMAAAB4AAAA4QAAAAD//f0AAgYGAAkdHQD34uIA/PX1AAEEBAAAAAAAAAAAxwAAAHAAAACbAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgAAAH0AAADjAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA1wAAAHkAAACnAAAADwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXAAAAkQAAAEgAAAD4//7+APfl5QABAQEABA4OAAD//wAAAAC+AAAAowAAAGIAAAAAAAAAAAAAAAAAAAAdAQMDbf/7/HD55OQAAAAAAAAAAAAAAAAAAAAAAAEEBAD45eYA+OnoAAMMDAAA//8AAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADzAAAA/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA1AAAALMAAAAAAAAAAQAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+AAAAywAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA7AAAAM8AAAAvAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA0AAAAmgAAAIoAAAClAAAA+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPkAAAD8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAABEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+AAAAN0AAAD+AAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWAAAAAMAAAB7AAAA2wAAAAD//f0AAgUFAAkeHgD34+MA/PT0AAEEBAAAAAAAAAAAvAAAAHYAAACPAAAAGwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJQAAAIgAAABdAAAA2wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAtwAAAIsAAACEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/AAAAcgAAAIYAAAAAAQAAAPjo6AAOFBQA/vn5AAAAAPcAAACLAAAARAAAAB4AAAAAAAAAAAAAAAcAAABLAAEBnQEEBNMAAAAAAAAAAAAAAAAAAAAAAQICAAUTEgD++/oA/ff3AAACAgAAAAAAAAAAAAAAAAAAAAAAAAAABQAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPQAAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAC9AAAAxwAAAAAAAAACAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAD3AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwAAAAwwAAAGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAmAAAAjwAAADAAAACeAAAAzwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAcAAAAHAAAABgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAPgAAAD5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACQAAABAAAAD9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+wAAAPgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABDAAAAEQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADbAAAA7AAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAADsAAAA5wAAABwAAAAfAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVQAAAAkAAAB0AAAA2QAAAAD//f0AAgUFAAkdHQD34+MA/PPzAAEFBQAAAAAAAAAAqwAAABQAAACGAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANwAAAIUAAABmAAAA8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAiAAAADcAAABNAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4AAAAwAAAAMYAAQEAAAAAAPns7AAHEhIA/vv7AAAAAN0AAACNAAAAkQAAAAAAAAAAAAAAAAAAAB8AAABdAAAAdgAAAAAAAAAAAAAAAAAAAAD//v4ABQ4OAP75+QD89fUAAQICAAAAAAAAAAAAAAAAAAAAAAAAAAAGAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9wAAAP4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPoAAAD6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP0AAADPAAAA6wAAABUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANAAAAbAAAADgAAACaAAAAqAAAAPMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAAAAOQAAAJQAAAAHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAAAAxAAAAJcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABTAAAAJwAAAP4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAADKAAAAlQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD5AAAA/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWgAAAEUAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAK8AAAD7AAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAH4AAADjAAAAHwAAAFMAAABQAAAAEwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATAAAAP8AAABvAAAA3wAAAAD//f0AAgUFAAkcHAD35eUA/fX1AAACAgAAAAAAAAAAoAAAABcAAAB2AAAACQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVQAAANUAAACGAAAA+wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD9AAAAdwAAACcAAAAmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA0AAACJAAAASwABAfL//f0A+OXlAAD//wAFDw8AAP//+wAAAKkAAABTAAAAPgAAAAAAAAAAAAAABQAAADcAAAATAAAA7QAAAAAAAAAAAAAAAAAAAAD//v4A//39AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAN4AAACvAAAAZgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANAAAACsAAAA5AAAAmAAAAMoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAjAAAAIwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALAAAAlwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC8AAAAvAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABIAAACVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcAAAAGAAAA+wAAAAYAAAAQAAAAJgAAAEgAAABkAAAAFgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADuAAAAogAAAP0AAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAuQAAANYAAAC3AAAAawAAAGsAAADBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATQAAAIsAAAAiAAAAAAAAAAD//f0AAwgIAAUQEAD++/sAAAAAAAAAAAAAAAD3AAAAjwAAAKEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcAAAAFcAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADXAAAAdgAAAPoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwAAAB0AAAAAAABAQAA//8A+erqAAIGBgAAAAAAAAAA6gAAAIwAAAD+AAAAAAAAAAAAAAAAAAAAKAAAAEMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPsAAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8QAAAKUAAADmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACQAAAGEAAACaAAAAbwAAAA4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD9AAAA8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABgP5/AAAAAABMAAAAmAAAAJkAAACYAAAAmQAAAJoAAAB6AAAAFgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAjQAAAN0AAAD/AAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPkAAACpAAAAaAAAAKkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABTAAAAhQAAABgAAAAAAAAAAP78/AADCQkABQ8PAP/7+wAAAAAAAAAAAAAAAPEAAACFAAAAuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAACBAAAAMQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALEAAACbAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAagAAAC4AAAAAAAICAAD//wD56+sAAgYGAAAAAP8AAADEAAAAsAAAAAAAAAAAAAAAAAAAABUAAAA9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADJAAAAlQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALwAAAIwAAACSAAAALwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACsAAABhAAAAZQAAAGAAAABPAAAAKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMMAAACYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAP/8/AD//PwA//39AAAAAAAAAADYAAAAbAAAAIMAAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABWAAAAfgAAAAwAAAAAAP//AAMICAAJHBwABA0NAP/7+wAAAAAAAAAAAAAAAOQAAAB3AAAA1AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACIAAACKAAAAFQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIEAAADNAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASAAAAigAAAAcAAAAA//39APjn5wD57OwAAQICAAAAAO8AAACUAAAA8AAAAAAAAAAAAAAAAQAAACQAAAAFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPkAAACbAAAAnAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAF8AAACaAAAAVwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADOAAAAbgAAAO4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAD//f0ABhgXAAYYFwAFFRUA//z8AAAAAAAAAADsAAAAgQAAAHYAAADtAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAABjAAAAfAAAAAsAAAAA//39AAIGBgAJHBwABA0NAP/7+wAAAAAAAAAAAAAAANIAAAB2AAAA5QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEIAAAB1AAAABQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6wAAAHUAAADyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAABFAAAAYgAAAAAAAQEAAP7+APrt7QABBgYAAAAAAAAAAMwAAACvAAAAAAAAAAAAAAAAAAAABQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOsAAAB4AAAAkQAAAPsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADYAAACPAAAAdwAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD9AAAAsgAAAGoAAADQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAYYFwAFExMABRMTAAUUEwAFFhUA//v7AP/9/QAAAAD9AAAAjQAAAHMAAADrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAABvAAAAbgAAAAAAAAAA//39AAIGBgAJHBwABAwMAP/7+wAAAAAAAAAAAAAAALcAAACKAAAA9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGoAAABXAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAvwAAAI0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAABwAAAAHgAAAAAAAgIA//39APno6AABAgIAAAAA8wAAAKcAAAD2AAAAAAAAAAAAAADwAAAA+wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAO0AAAB8AAAAfAAAAPEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABkAAACCAAAAggAAAAsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKoAAABfAAAAyQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAABRISAP/9/QD//f0AAP7+AAUUFAAGGBgABRUUAP77+wAAAAAAAAAAoQAAAHIAAADwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAB0AAAAXwAAAAAAAAAA//39AAIGBgAJHR0ABAwMAP/7+wAAAAAAAAAAAAAAAJ8AAACeAAAA/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwAAAIEAAAAoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAjwAAAM0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACUAAAB7AAAAAAAAAAD/+/sA+u7uAAACAgAAAAAAAAAAzAAAALIAAAAAAAAA/wAAAOcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPQAAACNAAAAbAAAANUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAAB1AAAAeAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAE8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAD//f0AAAAAAAAAAAAAAAAAAP7+AAUUEwAFFBMABhYWAP77+wAAAAAAAAAApAAAAHAAAADnAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABIAAACFAAAARQAAAAAAAAAA//39AAIGBgAJHBwABQ8PAP/+/gAAAAAAAAAA8wAAAIgAAAC7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIgAAAIgAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADzAAAAbwAAAPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAFMAAAA0AAAAAAABAQABBAQAAQQEAAAAAAAAAADvAAAAqQAAAAAAAAD5AAAA4QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP0AAACkAAAAYwAAAKoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAABxAAAAUgAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5AAAAkQAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAATAAAAIAAAAAAAAAAA//39AAD//wAFFRQABhYVAP77+wAAAAAAAAAAoQAAAHcAAAD3AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABsAAACJAAAALwAAAAAAAAAA//39AAMKCgADCQkA//v7AAAAAAAAAAAAAAAA5wAAAHoAAADaAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATQAAAGMAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADFAAAAkQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEQAAAH0AAAAAAAEBAP76+gD45+cAAQICAAAAAAAAAADWAAAA3QAAAPcAAADgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADJAAAAbgAAAHkAAADtAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAsAAACGAAAAGgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAABMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPAAAAeQAAAF4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAwAAAIoAAAAmAAAAxwAAAJsAAADbAP8AAAAA/wAFFBQAAAAAAPvq6wABBQUAAAAApQAAABAAAACOAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADEAAACBAAAAawAAAOUAAAAA/vz8AAQQEAAE9PQA/fT0AAEFBQAAAAAAAAAAzgAAAIgAAACPAAAADwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcwAAAMgAAADFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACTAAAAUQAAAC4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAQAAAAFQAAACs//v7APvw8AAHFRUAAP7+AAAAAOQAAAC6AAAAVQAAANsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/QAAAP4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADpAAAAiwAAANQAAABvAAAARAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACMAAAA8AAAAqgAAAPwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/gAAAOYAAADKAAAABwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQAAAMYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA2gAAANsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAxAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGAAAAhQAAAMEAAADhAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAABAAAANQAAAC0AAAA8AAAAOQAAAIoAAACzAP//AAABAQAFFBQAAAAAAPrq6wACBQX+AAAAlwAAABEAAACBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEoAAAB6AAAAdwAAAPQA/v4AAwcHAAodHQD24+MA/fX1AAEEBAAAAAAAAAAAqQAAACkAAABvAAAABgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZAAAAiwAAAEsAAADvAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAO0AAAByAAAAuwAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANAAAAawAAAKwAAQEAAQQEAAEEBAD//PwAAAAA9gAAAMUAAAAvAAAA3gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD8AAAA/QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD9AAAAtAAAANgAAABbAAAAbAAAABQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFgAAAC4AAAA7gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+AAAAxQAAAHMAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAACAAAA8QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACsAAAAnAAAAP4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAP8AAAD0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADnAAAA4gAAAP4AAAACAAAAEQAAACYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcAAAAWwAAAIkAAAD9AAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAP4AAADEAAAALQAAAAAAAAA8AAAAGwAAABkAAACaAP//AAABAQAFFBQAAAAAAPrq6wACBQX8AAAAjQAAABAAAABxAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQAAAF8AAADaAAAAkQAAAAD//f0AAgYGAAodHQD34+MA/PX1AAEEBAAAAAD2AAAAjAAAADQAAABBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5AAAAawAAAIUAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAL0AAACkAAAAXQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAvAAAAVAABAaz9+PgA+OfnAAcUFAD//f0AAAAA2QAAALgAAADzAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPsAAAD9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAOQAAADQAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAD/AAAA1QAAAL4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA3AAAAIYAAADLAAAAdgAAAD8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQwAAABkAAADuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAADzAAAA5QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAO0AAADfAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAK4AAACXAAAA/wAAAAEAAAAKAAAAHQAAAFsAAABSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABnAAAAmQAAAO0AAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAA/wAAAMMAAABpAAAAAAAAAAAAAAAhAAAAhQAAABYAAACEAP//AAABAQAFFBMAAAAAAPrp6wACBgX1AAAAhwAAABgAAABZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwAAAHgAAADcAAAAtQAAAAD//f0AAgYGAAkcHAD35uYA/PX1AAECAgAAAADqAAAAegAAAKcAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABxAAAAxAAAAMEAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH8AAABCAAAAGwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAABtAAAAsv/7+/r78vIABxUVAAAAAAAAAADjAAAAtQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAECAgABAwMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+gAAAP0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABMAAAAbQAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAP8AAACuAAAAvgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+wAAALYAAADdAAAAbwAAAGgAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKAAAALgAAANkAAAD8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACQAAAAQAAAD8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6wAAANUAAAAAAAAAAAAAAAAAAADLAAAAygAAAPQAAABYAAAABgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4AAAAyAAAAvQAAAP4AAAAAAAAAAAAAAAACAAAAAAAAAAAAAAD/AAAAxQAAAPUAAAAAAAAAAAAAAAAAAAAUAAAAeAAAAH8AAAAAAP//AAAAAQAFFBMABhYVAP76+gAAAADrAAAAfAAAAL4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGAAAAH8AAAApAAAAAAAAAAD//f0AAwoKAAEFBQD//f0AAAAAAAAAAAAAAADIAAAAkQAAAPQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABIAAACHAAAADQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA3wAAAIMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAACsAAABDAAEBAAEDAwABBAQAAAAAAAAAAO0AAADUAAAAAAAAAAAAAAAAAAAAAAAAAAABAgIA+u/vAPrs7AABAwMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD6AAAA/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcAAAAMAAAADQAAAA0AAAANAAAADQAAAA0AAAANAAAADQAAAA0AAAAMAAAADQAAAAsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAwAAAAWAAAAEwAAABMAAAATAAAAEwAAABMAAAATAAAAEwAAABQAAAAUAAAACAAAAAAAAAAAAAAAAAAAANkAAAB8AAAAhQAAAPgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAvAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJAAAAAgAAAAAAAAAAAAAAAAAAAAAAAADkAAAAewAAANgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAADAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAADnAAAAvAAAAAAAAAAAAAAAAAAAAAAAAAALAAAAdgAAAIEAAAAAAP//AAAAAQAFFBMABhYWAP76+gAAAADeAAAAfQAAANYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANAAAAHoAAAAUAAAAAAAAAAD++/sABxUVAAQLCwD//PwAAAAAAAAAAAAAAACfAAAAtwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgAAABpAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAqQAAANIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAF4BAQEA/ff3APfl5QABAwMAAAAA/AAAAPoAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQD++PgA/fb2AAABAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPwAAAD9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+AAAA+gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXAAAAiAAAALEAAACzAAAAswAAALMAAACzAAAAswAAALMAAACzAAAAsgAAALIAAABTAAAAAAAAAAAAAAAAAAAAAAAAAO4AAACLAAAAnwAAAP4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABwAAAAFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADkAAAA0gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMwAAAAMAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAPwAAACdAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKAAAAcAAAAHQAAAAAAP//AAABAQAFFBQABxcXAP/9/QAAAADIAAAAgQAAAPMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAUwAAAGAAAAAAAAAAAAD//wADBwcACh4eAAQNDQD//v4AAAAAAAAAAPEAAAB8AAAA3AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHUAAAApAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwAAAAewAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAAAANgABASz/+/sA/fX1AAEFBQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQICAAEEBAAAAgIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+QAAAPwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAsAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAA/AAAAOAAAADkAAAA5AAAAOQAAADkAAAA5AAAAOQAAADkAAAA5AAAAOQAAAB4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPIAAACCAAAAyQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADeAAAA+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAbAAAAAgAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAA/wAAAMMAAADjAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOAAAAeAAAAF4AAAAAAP//AAADAgACCwkA//z8AAAAAP4AAACxAAAAhgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJAAAAdQAAAEgAAAAAAAAAAP/9/QADCQkAAQQEAP/9/QAAAAAAAAAAAAAAANoAAACNAAAA9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEwAAAIAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC+AAAAuQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaAAAAUQECAgD9+PgA+OfnAAIGBgAAAAAAAAAAAAAAAAAAAAAAAAAAAAECAgD68PAA+evrAAEDAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD7AAAA9wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADQAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAPsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOgAAACzAAAA9wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOMAAAD2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAAFAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAA8wAAAJsAAADoAAAA4QAAAAAAAAAAAAAAAAAAAAAAAAATAAAAgwAAAFMAAAAAAP7+AAEHCAAHGBcA//r6AAAAAPkAAACVAAAAnAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWAAAAeAAAACEAAAAAAAAAAP77+wAIFxcABAsLAP/8/AAAAAAAAAAAAAAAAKMAAACxAAAA/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATwAAAFUAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPcAAACIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAA5AAEBAAD+/gD56uoA+OnpAAIHBwABBAQAAQQEAAEEBAABBAQAAgYGAP77+wD99/cAAAEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAD2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAA+QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMIAAADiAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA7AAAAPgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABgAAAAQAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAD9AAAAngAAAHYAAABRAAAApwAAAAAAAAAAAAAAAAAAAAAAAAAeAAAAgQAAADcA//8AAQICAAQUEwAFFhYA//r6AAAAAPMAAACLAAAAwwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA7AAAAbQAAAAsAAAAAAP//AAIFBQAKHh4ABA0NAP///wAAAAAAAAAA9QAAAIAAAADgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFAAAAgwAAAA0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAMoAAACoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4AAAAcAAEBAP///wD67OwA9d7eAPXf3wD1398A9d/fAPXf3wD03NwA++/vAAIFBQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABkAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAAWAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPsAAADtAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP4AAAD5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6AAAAPEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD1AAAA/QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFAAAAAwAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADlAAAArQAAAM8AAAB1AAAAkgAAAAAAAAAAAAAAAAAAAAAAAAArAAAAiAAAABgA//8AAAABAAUUEwAFGBcA//39AAAAANoAAACKAAAA6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAABjAAAASgAAAAAAAAAA//7+AAMJCQAAAgIA//39AAAAAAAAAAAAAAAA3AAAAJAAAADzAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAjAAAAaQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9gAAAJUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAACwAAAAAAQEBAP/9/QD78vIA/PT0APz09AD89PQA/PT0APvz8wD++fkAAAEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5wAAAPkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGwAAABUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAABwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+QAAAN8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+wAAAPEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFwAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAABQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD0AAAA/QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAACAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD2AAAAAAAAAOUAAACDAAAAkAAAAAAAAAAAAAAAAAAAAAAAAAA+AAAAhwAAAAAA//8AAQMDAAIHBwD//PwAAAAAAAAAALsAAACQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4AAAB4AAAAKQAAAAAAAAAA/vv7AAkZGQAECwsA//39AAAAAAAAAAAAAAAAnwAAAMcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB2AAAAJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA0wAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHAAAAGQAAAAAAAAAAAAEBAAECAgABAgIAAQICAAECAgABAgIAAQICAAABAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADfAAAA7gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAPwAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuAAAAGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD2AAAA1wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD8AAAA8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD5AAAA6wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAjAAAAHQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACgAAAAZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOUAAAD8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD8AAAA7gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP4AAADyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPkAAAD9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABgAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAAMAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOwAAACHAAAADwAAAGEAAAAAAAAAAAAAAAIAAABXAAAA1QAAAIz//f0AAwoKAAYXFwD76uoAAQYG+QAAAJsAAAAzAAAAVQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADEAAABpAAAAfAAAAPIA//8AAgUFAAoeHgD35OQA/PX1AAECAgAAAAD0AAAAhgAAADIAAAAUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABEAAABsAAAAcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADzAAAAkgAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPsAAADdAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANYAAADpAAAAGQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAByAAAArQAAAOYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHsAAAAFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPEAAADQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACkAAACUAAAA7QAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACgAAAK0AAAC0AAAA+QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPkAAADrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgAAAA9AAAA4QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARQAAABYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA1wAAAPkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAEQAAAClAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAsAAADKAAAAmAAAAOsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADIAAAChAAAA9wAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/gAAALYAAACwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFAAAAHsAAABaAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJAAAA1wAAAJ4AAADmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAAADcAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQAAANgAAACAAAAA+wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOoAAACDAAAAGwAAAEYAAAAAAAAAAAAAAAkAAABqAAAA0QAAALsAAgIABRQTAAECAQD66+wAAQMD7gAAAJIAAAAvAAAAJgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAGAAAAC+AAAAsAAAAAD//v4AAgcHAAABAQD///8AAQAAAAAAAAAAAADEAAAAowAAAGoAAAAHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGMAAACbAAAA1gAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP4AAADGAAAA0gAAAC4AAAAAAAAAAAAAAAAAAAAAAAAAyQAAAMoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA2wAAAN8AAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAACcAAAAegAAANAAAAAAAAAAAAAAABgAAAB2AAAASAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD9AAAA0gAAAPYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFgAAAFAAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACA/38AAAAAAAMAAADjAAAAuQAAAPkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADzAAAA3gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKAAAAbQAAAN0AAADHAAAAAAAAAAAAAAAAAAAAbAAAAFQAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAANIAAAD7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAhAAAAIQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAAAAKQAAADrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcAAAAWQAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP0AAADsAAAAsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAABDAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAMwAAACwAAAA5gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC0AAAAtAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADQAAAAggAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABwAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAYAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA3wAAAHkAAAAoAAAAHgAAAAAAAAAAAAAAIAAAAH4AAADbAP//4AEDAwABBgYA//z8AAEDAwAAAADJAAAAjQAAAJQAAAAFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANAAAAeAAAANcAAADZAAAAAP/8/AAJGxsA/+HhAP339wABAwMAAAAA/QAAAIQAAABNAAAAJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAAeQAAAFwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8QAAALEAAAAuAAAAAAAAAAAAAAAAAAAA3QAAAHUAAAD4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPsAAADOAAAASgAAAAcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASAAAACoAAADgAAAA6wAAAA8AAAAfAAAAHgAAAAYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9AAAALsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP0AAADRAAAA/QAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAzAAAApwAAAMgAAADYAAAACQAAADMAAAAzAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADLAAAA5gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP0AAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHAAAADQAAAAEAAAD/AAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAzwAAAHMAAAD9AAAAAAAAAAAAAAABAAAAOQAAAHMAAAAA//39AAMMDAAGGBcA//r6AAAAAPsAAACmAAAAqwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA1AAAAVQAAAAQAAAAAAP//AAEEBAAKHx8AAwsLAP/+/gAAAAAAAAAA7gAAAJcAAAD1AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABRAAAAIgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD5AAAAwgAAAAAAAAAAAAAAAAAAAPMAAAB6AAAA3AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAL8AAADlAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAHwAAAD8AAABTAAAARQAAACYAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADxAAAAtgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAN0AAADEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAAfAAAAVgAAAHQAAABtAAAAQwAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAK0AAADvAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+AAAA/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+AAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAQAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAAtgAAAIgAAAAAAAAAAAAAAAAAAAADAAAAWAAAAFwAAAAAAAEBAAYUFAAGFxcA//39AAAAAPQAAACVAAAA1gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAABuAAAANwAAAAAAAAAAAP7+AAIHBwAAAAAAAP//AAAAAAAAAAAAAAAAmgAAANAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoAAABuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAADZAAAA5AAAAAAAAAD8AAAAmQAAALAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAywAAAMUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+gAAALEAAADoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5AAAAKcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAACwAAAAkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD5AAAAuwAAAPgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/AAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABsAAAABAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD2AAAAoQAAAM8AAAAAAAAAAAAAAAAAAAAUAAAAZwAAABwA//8AAAMDAAEEAwD//f0AAAAAAAAAAMoAAACfAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABUAAABuAAAAGQAAAAAAAAAA//z8AAoeHgADCwsA//7+AAAAAAAAAAD0AAAAmwAAAPEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGAAAAAfAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPQAAADSAAAAAAAAALcAAACEAAAA+gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD6AAAAiQAAANsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADiAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD9AAAAwgAAANAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAI8AAADZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD4AAAA/QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKAAAAAIAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADdAAAAiAAAAAAAAAAAAAAAAAAAAAAAAAA3AAAAbgAAAAD//PwAAxAQAAYWFgD/+/sAAAAA/AAAAKkAAAC9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAE8AAAA4AAAAAAAAAAAA//8AAgYGAAAAAAAA//8AAAAAAAAAAAAAAACqAAAA1QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwAAAFcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPkAAADTAAAAZgAAAPEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADWAAAAjwAAAPMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+gAAAIYAAADuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADbAAAAkwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADMAAAAnQAAAPYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPEAAAD6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA0AAAA/wAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP4AAADHAAAAvgAAAAAAAAAAAAAAAAAAAAUAAABWAAAASgAAAAAAAAAABRUUAAUXFgD//f0AAAAA5AAAAJoAAADrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABwAAAGYAAAAeAAAAAAAAAAD//PwACx8fAAMLCwAA//8AAAAAAAAAAPcAAACQAAAA7wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAA2QAAAFgAAADoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAACgAAAAhwAAAPoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACMAAAApgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPwAAACpAAAAjgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA7wAAAHYAAADeAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6AAAAPkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADkAAAATAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOQAAACaAAAAAAAAAAAAAAAAAAAAAAAAAB4AAABcAAAAAAD//wABAgIAAQICAAD9/QAAAAD/AAAAsQAAALQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQwAAAEcAAAAAAAAAAAD//wABBQUAAP//AAD//wAAAAAAAAAAAAAAALEAAADgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAyAAAALwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPMAAACPAAAA1gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPcAAACQAAAAbwAAAPUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAIYAAAD5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPUAAACIAAAAlAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOgAAABZAAAAwgAAAP4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADhAAAA9gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEQAAAD0AAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEwAAACUAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAEIAAABTAAAAAP/9/QAEEREABRcWAP/8/AAAAAD5AAAArQAAAOMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFAAAAVwAAACAAAAAAAAAAAP/7+wALICAAAwkJAAD//wAAAAAAAAAA+gAAAKYAAAD1AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAABJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+AAAAOoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPcAAACfAAAAVwAAAMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA2gAAAGEAAACLAAAA9QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAABwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOsAAAB9AAAAcgAAAPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADDAAAATgAAALwAAAD9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABgAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAAAABQAAAAAAAAAAAAAAAAAAAAAAAAABAAAABwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABgAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAO0AAADoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcwAAAAgAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD6AAAABgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFgAAAFoAAACyAAAA+wECAgABAgIAAP//AAAAAAAAAAC6AAAAsQAAAGwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/AAAAOgAAAOAAAAAAAAAAAAEFBQD///8AAP8AAAABAAAAAAAAAAAAmwAAAGoAAAAhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD4AAACIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPkAAAC4AAAAxgAAAFIAAABsAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4gAAAJwAAACaAAAA7gAAAPsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD1AAAA/wAAAPMAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAsAAAD1AAAA/wAAAAAAAAAAAAAAAAAAAP8AAADsAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAPEAAAD5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPAAAACWAAAAuwAAAG4AAABdAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADaAAAAjAAAALEAAADrAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwAAAA8AAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAA8wAAAP0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADzAAAA9wAAAPwAAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPwAAADzAAAAAAAAAAAAAAAAAAAAAAAAAPwAAADrAAAADwAAAP0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACgAAAPEAAAD8AAAAAAAAAAAAAAAAAAAAAAAAAPAAAAD0AAAA/gAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAADuAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAPUAAAAAAAAA8QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwAAAPYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAMIAAAAuAAAACwAAAP0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAsAAAAcgAAAIYAAAAAAf///wAAAAAALJWmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAALAAAAVQAAAI4AAAAQAAAAAAAAAAAAAAAAAAAA2wAAAGYAAADAAAAAAAEBAQD67O0A++vsAAEEAwAAAABMAAAAlwAAABwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC7AAAAZQAAAOEAAAAAAAAAAAABAQD2398A/fj4AAABAQAAAAAAAAAAlAAAAGQAAAAHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA2AAAAC4AAAD7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQAAAAwAAAArAAAAPQAAADsAAAArAAAAGgAAAAYAAAAAAAAAAAAAAAAAAAAAAAAA/QAAAOkAAADZAAAAxwAAAMMAAADQAAAA8AAAAPgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA0AAAATAAAA/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAPAAAADyAAAA/gAAAAAAAAAAAAAAAgAAABkAAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAO0AAAD2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACgAAABcAAAA5AAAAQAAAADUAAAAhAAAADwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA7AAAANkAAADCAAAAwAAAANEAAADuAAAA+wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABUAAAAKAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAD3AAAA7AAAAP0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPAAAADyAAAAAAAAAAAAAAAAAAAABAAAABkAAAACAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAA+QAAAOwAAAD7AAAAAAAAAAAAAAAAAAAAFQAAAAkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPoAAADpAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAABAAAADwAAABAAAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAA8AAAAPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwAAABEAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAD96rak4wAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAPQAAACkAAAA1gAAAI4AAwM6Af7+AP///wAAAAAjAAAAOwAAAMAAAAAAAAICAAECAgAAAAAAAAAAAAAAAMAAAADKAAAAHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEEAAAAkAAAA4QAAAAAAAAAAAQMDAAABAQAAAAAAAAAAAAAAAAAAAAC7AAAAWwAAAAcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD7AAAA7wAAAMUAAADLAAAAkwAAAAgAAAATAAAAEwAAAAsAAAAEAAAA/gAAAPUAAADvAAAA7AAAAPYAAAD7AAAA+gAAAP4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8wAAAPMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+AAAA5QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD2AAAA3wAAAOMAAACwAAAABQAAAA4AAAAVAAAADwAAAAcAAAABAAAA+AAAAO4AAADpAAAA9gAAAPoAAAD6AAAA/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8AAAAPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD8AAAA4wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAADwAAAA8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD1AAAA9QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPsAAQHK/vf3Tvzx71wEEBJ/AwsLEf/8/AIAAAAyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9AAAALoAAABIAAAACgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAD8AAADHAAAA9gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMwAAAD3AAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAAD6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAPkAAAD5AAAA9AAAAOwAAAABAAAAAQAAAAAAAAAAAAAA/wAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/AAAAPwAAAAAAAAA8QAAAOQAAAABAAAAAAAAAAAAAAAAAAAA/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAwLt+/HvlfPRyzb66uiPAgYH4AAAANoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAxQAAANMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASgAAABsAAAAAAAAAAAAAAAAA//8AAAAAAAAAAAAAAAAAAAAA5gAAAN8AAAD/AAAAAAAAAAAAAAAAAAAAAAAAAOcAAACCAAAAdQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAD+AAAA/gAAAP4AAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAP4AAAD+AAAA/gAAAP4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA///8//393AD//3gAAAArAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADcAAAA4gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAxAAAAGwAAAAAAAAAA//z8AAsjIwACCAgAAP//AAAAAAAAAAD/AAAA6gAAAP4AAAAAAAAAAAAAAOUAAACLAAAANgAAAIQAAADsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+gAAAPoAAAAAAAAAAAEDAwD77u4A+urqAAEEAwAAAAAAAAAAAAAAAPgAAADyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAhAAAAwAAAAAABBAQA9d7eAP74+AAAAQEAAAAAAAAAAAEAAAAnAAAA/wAAAMIAAACiAAAAswAAAO4AAAD8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD//wAA//8AAAAAAAAAAAAAAAAAAAAAOAAAAC8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//8AAAAAAAAAAAAAAAAAAAAAJwAAAI4AAADKAAAA2AAAAPwAAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEDAwD77u4A+ujpAAEEBAAAAAAAAAAAAAAAAAIAAAD0AAAAmgAAAMkAAAAkAAAAEQAAAAgAAAAKAAAACAAAAAwAAADWAAAAygAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADZAAAA2gAAAP4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADJBMgAAAAAA/gX+AP8W/QDvHO8ACfIHAAEKAwDoKeoAC8oLAPgW+QD/AwAAB/MEAAzwCwD4MvkACdMIAPkA+wAJ8goA/Qb6AAXwBwAE9AUA+wT8AAcUBgD99vsA/B75APYP+AAF/gUA9Ab3AATeAwD3G/gAAAUAAAf6BgAK5wcA9TT2AAvhCwD87v4ABA0DAAXfBgD/3AEAAP8AAAcGBgAEDQQAAREAAPUc9AD2CfUA/wT+AADk/wD3GvsA/Qr+AAvtCAACBAIABwUIAAjwBwAB2wQA7QPuAPkp+QAB/QEAAP8AAAMEAgAL5AsAA/EDAAEDAQAOEAwA9u74APsD+wD7J/kA/gn/AAPuAQD9Cf0A9xb6AAH+AAAI7QYA/fX+AP4a+wAE3gkA/Q75AAnsCQAL4w4A/fn+AAD/AAD/AAAAAgEBAB0YGwADAgAAgP9/AwD///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBAP39/Q/r7ew5//8AAgAA/wD8/Pz/JSMi5hcXFPf///8A8fHzBdXX2RwDAwMDAAAA/wEAAQH9/v72/Pz8wwEBAP0AAAAAAAAAAP//AAD4+fkAAAAAAAAAAAAGBQUA+vr6AAIDAwQTExNDGRcWTgYGBRIEAwIFAAAA//7+/wL2+PjX4OHhqvHx8tIODg4A//7/APz8/AAAAAAA09PSAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAP39/QD+//8AEhAPAP79/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD///8AAwQDVDQ0L5wHBQUAAgICAAAAAAD+/v8AAAD/AAABAQAaGRkAKiknAAEBAQAA//8ABAQDAOfp6+XX2NsmBAQE9gAAAAACAgIA6erqAPb39wACAgIAAAAAAP39/QAJCQk+NTMxhiUkITsKCQkAAAAAAP7+/gABAAEAAgIBAP3+/gAsKSdty87QmOTm538CAAH4AAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBAOjr6QDn6OkAAP//AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPv8/AAfHh0VNTEtAPP09ffk5ufRAf8A/wIDAgcA/wAA/wAAAAEBAQH+///7///+/QEAAQEDAwIB7e3v3/T19v0DAwIAAAABAP///wDx8vMAAQEBAAAAAAD9/f0AEREQV0Q/PZsaGhcN8PDyAM/a1L/09fW3/fz88wECAwIHBwYkFxUUUCAeHCFEQD12zM/RJNLU118A///0AQEBAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEA+/z7APj1+AD+/f8AAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+vr6AP7+GxcCAwMA5efo5MfKzEYIBwf0BgYGAwEAAAD/AP8AAQEBAPv6+/79/v3/AQABAQEBAQD5+vr/9fb3/wEBAQAAAAAA/v7+AAAAAAACAgIA/v7+AAoKCyk8OTaQGhkXDeDg4+bKztFz9PT0rBMUE/z//fwAAwMEAPDx/ADV19mNKiglnDQxL0g0MS9S9PP26tvd3WsEBAT+AAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAgEACAoIAAD9AAAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAwMADAsJCAICAQD7/Pzv/f7/+AICAv8GBQX8AP//AP8A/wABAQEAAP8AAAL+AgABAAH/AQEBAP3+/gACAgIAAQEBAP///wABAQEAAAAAAAAAAAD+/v4EKSglhSEeHRjp6+v3w8fKX/z8/LwFBQUAAwICAP7/AgACAgIABAEEAAAAAOXQ0dVhCQkJsjAsKjUuLipi5efnpPn5+cUAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAABAgEACAcIAAMCAgD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAb6+wAICAgLAP8AAPv7+8wYFhX7AwMDAPz7+wD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAABwcHAAICAQD//wAA////AAYGBjMpJiRM+/z8AM/Q1Jrw8fG8BgUFAAQEBAABAAAAAAAAAAABAAAA/wAABQQEAAIBAejc3eB0HRoavBYUEgYcHBpZ09TWxQAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///wD+AP4ABQUFAAUEBAAFBAQABQQEAAUEBAAFBAQABQQEAAUEBAAFBAQABQYFAAQEBAv///8A9vf37/n7+gD6+voA/v/+AAAAAAAAAAAAAAAAAAAAAAD///8AAQEBAAEBAQABAQEA/v//AAkICAAGBgYABAQFAAUFBQACAgIAGRgXVBMREAXo6evu3+PjkwEBAQD8/PwA9/f3AP7+/gABAQEA/v7+APz8/QAJCAgADg0NAPz7+8Lj5ebFAwMDAB0bGj4BAQEHAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH9AQAC+gIAAfwBAAH8ARMB/QEKAAAAAQH8AfoB+wH0AvwC9AL6AgABBAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB/gEAAvoCAAECAQIB/AEXAf0BBwACAAIB/QH8AfsB+AH8AeoC+gIAAf3/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf0BAAH+AQD/+wAQAfwBDAH9AQQB/AH5Af8B7QIAAvoA/wAAAAAAAAH9AQAB+QEAAf0BAAD/AAAB/QEAAfwBAAH+AQAAAQAAAAAAAAAAAAAAAAAAAP4AAAH6AgAB+wAGAQUBEQH9AQgB/gAAAfwB+AH7AfMC+gH2AfsBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBAPr7+gDz8fQA////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///wD//wAODDEKLv3+/QAAAAABAf8AAAEBAAD/AAEAAQABAAICAgD//wAA/wAAAPX19wD///8AAgIBAAECAgAGBQUABAMBAAD//QAAAAAAAAAAAAEA/hEaGBdGAgMCAOfp6s/6+PndBQUFAAAAAAD6+vsA6uvsABYVEwD8+/wA+Pj4AAECAQD9/v4A///+AOnr6675+vpAFRQTKA4ODjQAAADFAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH9AQAC+QIA+xD7Pfkg9kz4Jfg19if2Jf4K/goAAgABAfoC+vH28fUH+gfN+/v72ALmBLoC7gLK/gAC+gAAAAAAAAAAAAAAAAAAAAAC+QIAAvsCDfwL/D74IPlI8xz1M/1A/CP9C/0HAAMAAgL8AvwD9QP4BP8C2P4G/9cD3gW8C/UJtQH8Afr/AAAAAAAAAAAAAAAAAAAAAf0BAAP5AwT6GftT+CD3TvdH+Dv9Dv4M/wT/A/X48/oE9AXjEdwPswbUEZ8C+gLi+h77S/gz90cA+wH9AAAAAAIDAQIFAQS+BegGsQEDAQAAAQAAAf0BAAL8AgD/Cf829yT2VPcc90L5Dvob/gr+CAD/AP8D9wL5AfQC7gjqBcYI/QG+A+8BtQH7AfIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBAAIDAQD7+v0AAQEBAAAAAAAAAAAAAAAAAAAAAAAAAAAA////AAAAAQkJCQkQ/f3+APr4+gL+/P0A+/7/APv9/AALCQkAAAAAAAAAAAAAAAAAAAAAAPn6+gD7+/sAAQEBABkXFwARERAA/v7+AAAAAAAAAAEA/v79GxIRECn7+/wA6uvswQYHBwAFAwQAAAAAAP8AAAD+/v4A8fP0AAD/AAD//wAAAP3/AAAAAAD//wAA8PDy0fX19wALCwoFFxUVGwAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC+wIA+wv7Me8+8Yr1KfRC/Q7+AgD+AAAAAQAAAP8AAAAAAAAAAAAA+wIAAPb/9yIALwAAAvICe/Ta8scNyQ2aD9AHpgH8AfwAAQEAA/sCCPIu8mvpQ+pg9R72LP4M/gAA/wAAAAAAAAAAAAAA/wAAAAAAAAABAAAA/wAZ8yn1QQH1AWoP1Q/FFq0WewL6BMH+Bf8AAAAAAAH7AQD9Cv0j6UTple007kf/Cv8AAP8AAAD+AAAAAAAA/AEAAPkB+hb1LfUz7zXwZCKaI2YAGv4881bybQD3AAAAAgAAAAIAAAT6AwYIAf6xCvcKAALvAwD/C/4g7UzshfA971b7EvwEAP8AAAD/AAAA/wAAAAAAAPsBAAAAAQAY9wH3KgE+8mQJ2AjRFrQDgQPhA6/9//4AAAAAAAAAAAAAAAAAAAAAAAAAAAD///8ABAYEAPz7/QD///8AAAAAAAAAAAAAAAAAAAAAAAAAAAD///8AAQEA/gICAv/+/v4A+Pr60/z+/wD39fMAAQICAAwKCgAAAAAAAAAAAAAAAAAAAAAAAQEBAAEBAQABAQEA9vf3AAcIBwAA/wAAAAAAAP///wAGBgUPCgoJBPf4+QD29vfXAQAAAP8A/wAAAAAAAAAAAP///wD5+PkAEBERAAAAAAAAAAAAAAAAAAAA/wD/AADV+Pj45w0MCxkHBgUYAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD///8AAvkCAPIv8VTkZ+WZ+Bn4EgD7AAAA+gAAAAAAAAABAAAAAQAAAAEAAAD/AAAA/QAAAAAAAAD/AAD+DP4C8TbyO/Ew8jvzy/NkGq0YYv7/AQHfV+Wm8DbwNP8BAAAA/QAAAPsAAAD/AAAAAQAAAAIAAAD/AAAAAAAAAP8AAAD/AAAAAAAA/wf/APA58Dv5IPmyIYQiVgHSAbkBAwEA/wv+Hdxk3LTzLvMoAPkAAAD5AAAAAAAAAAAAAAABAAAAAQAAAP8AAAAAAAD1HvYc2nLZqAviDNoA8gAAAP4AAAABAAAAAwAAAP4B+AT7BAAO6A0A/hf8LONi4q3yKfMmAPkAAAD5AAAAAAAAAAIAAAABAAAAAQAAAP4AAAD+AAAA/wAA/wMAAPYx+C/lJOmqCwoOawW1B5r8B/wAAAAAAAAAAAAAAAAAAAAAAAAAAAACAgIABwgHAP37/QAAAQAAAAAAAAAAAAAAAAAAAAAAAP///wABAgEFBAQDA/z8/QASEhHb8PHxAPr7+wADAwQA/Pz8AAEBAAD//wAA+/z8AP39/QD//wAAAAAAAAEBAQD5+voA8vPzAAUEBQD///8A/f7+ABIGDz4IBwYB8PDx9wUFBNYXFxYAAAAAAAAAAAAAAAAAAAAAAAAA/wACAgEAAAAAAAAAAAAAAAAAAAABAPv7/PH19vblGAIWGQQFBTgAAACSAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwfDQD4DPct4WPinvsY+w0A+AAAAAAAAAAFAAAAAgAAAfUBAAH6Af4A/wAAAAUAAf8F/wH/Cf8AAP4AAAD7AAAA+QAAAAkAAPgY+SQCAgMGCfkJ9e4NAwr/BgAAAPUAAAACAAAABwAAAPwAAAH4AQAB/AEAAP4AAP8E/wD/Cv8AAAMAAAD6AAAA/gAAAPcAAPcZ9w7lbOWVInUiO//zAOnqVuqH8y/zKAD2AAAAAAAAAAAAAAADAAAABAAAAPkAAAH6AQAAAAAA/wj/AAAAAAD5DvoO+S3uNAD8AAAAAAAAAAAAAAAAAAD+//4BBfEFAPwH+x3fYN6s8in0JgD2AAAAAAAAAAMAAAAFAAAB9AEACfAI6Qr5Cuv0CPMa+BD6EgAKAAAA+wAAAPgAAPsK+wTaaduWJOwm5v2+/bT+B/4AAAAAAAAAAAAAAAAAAAAAAP///gAJCAkAGh0cAP7//wAAAAAAAAAAAAAAAAAAAAAA////AAICAhcJCQgO+vr6+eHh4+rq6usJGRgXRBYWFTgPDQwRAAD/Av//Af77/Pz66uvs4uTl56T8/PzqBgYGAAAAAAD+/f4ACQ0HAP/+/wD9/f0ACQgHDgMCAgD7/P359PT29wECAQAAAAAAAAAAAAAAAAAA//8ABgYFAAQDAwAA//8AAAAAAAAAAAAAAAAA/Pv8Af79/v8CAwIABQQEAgAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQACAN485X77GvsNAPkAAAABAAAAAwAAC+EJ4RHIEaAQ6BK+ANAB5fn4+QAOGQ4E5zDlLAEKATD1LfZL8iPyMf4K/gAK0wnZKJYqTwTuBNn9/v4AHYggSwNyBKXpNOk6A/cDABPLEscB4/++FOYVxAfRCuj29fb/CB8HCe4V6zTyNfVW8TfxPf8A/wAA/wAAAPgAAPIo8SP5N/lJA9kDw+o/6lYA+QAAAAAAAAAAAAAABQAAA/MD/RXGFqYI6wW2F9ca1fgP+Az0IvA16yvuY/Mr8y7/+v8AAAAAAAABAAAAAAAAAP8AAP3+/v4D7AMA5VbkifIq9CcA9gAAAAAAAAAFAAAG+AX6GLMYmg/CEaP8G/jeEi8R9OjS5w8E8ggw8T7yY+k76FX/BAAAAPoAAPsK+wPnW+SJHtIhvfz6/Pf/Av8AAAAAAAAAAAAAAAAA////AAUIBQAVFBMAAQEBAAAAAAAAAAAAAAAAAAAAAAD/AAAAAP//EAUFBQTu7vDs/v//QURBP2YnJSI5CgoJAP7+/wD+/v4AAgICAAICAgD4+PgA5efpasjKzIjn6OmcBwYGAPj4+AD5+QcABgYFAP8A/gD/Af/+AAEAAPn6+vjs7e3yAQIBAAEAAAAAAAAAAAAAAP7+/gAVExMACg8PAP7+/QAAAAAAAAAAAAAAAAAAAAABAAABBQAAAAD+//74AAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADtM+sQ9B/zNgD8AAAA/wAAAAUAAAfdB94bjx1JDPIN2gEk/wAB5gMA/vj+AAoDCgAE7APZEtAVrRy+B4HwIe9P9T70RxSmFowI7Qja+Qf5AP/9AAD88vzgGo4bWgNrA4UKvgu8D8UPp/A07/kFDAUACOgIAPzz/AAHDgcABtoJyCHXFpnr8Omb5ErmXwD7AAAA/wAA/wH/APYr8jwHCQkO+g/6BQD3AAAAAAAAAAQAAAbqBu0gkyBkFeYWsOst6AAJ7QkA/gL+AA3YDscNsBGd7lzrleJK5F8A+QAAAP8AAAABAAAAAQAAA/sD+Psk+SnvPPBWAPsAAAAAAAAABAAACPMH8h+LHl8H0AmwBgUHAP4S+gD+Bv4AAvcC+AnhCrkUqRW5CtYKcN9i343/Bv8CAPkAAPEp8ir6OvlQDMoMp/4J/gAAAAAAAAAAAAAAAAABAAAA/vz+AOHi4gAFBAQA//8AAAAAAAAAAAAAAAAAAP7+/gAMDAsOAP//ABIREBVQS0efBwcGAOPm5+Dp6Oq79PT18P8AAAACAgIAEA8OLSQjIEQjIB4jFxYUm73AxCLh4eN8AwMC/vr6+wATExMA/Pz9AA4NDg8AAQEA+vr46AgHBvICAwIAAQAAAAAAAAAAAAAA/v//AAMDAwD+//8A//8AAAAAAAAAAAAAAAAAAP//AAD/AAD/AQEAAAEBAQEAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANwh4BH5CvkWAP4AAAAAAAAAAgAAD9wO0hX0FdrrJusABBYCAOlg7AAY5xQA6/bqAPQY9gACMPAADuMO/ArRC60LGwmtBfMIAAAEAAABAAEAAP8AAAAEAQD/8f/hD90PXP/TAAD6A/sABAAEAAADAAD8GfsAABD/AP8R/wAFBAQA9f30+wfFBpgFNwNa+xH9BQD9AAAA/wAA7BbvLAv9+goA/wAAAAEAAAAAAAAA+wAAHaocgA3WDrD8GfwA/wT/AP4E/gD/BP8A+wX7APvt+vUIkgp0CCkHRfgf+RoA+wAAAAAAAAAAAAAD/gL89iv2Wf0S/gMA+wAAAAEAAAEBAQAfqSGEC8ELsPgZ9wD3J/gA+SD2AP0H/QD1H/cA/QIDAAfhBwAYwRiQ+Rr4LeVS5mIA+wAA/wP/APgz9UYFxAan/f/8AAABAAAAAAAAAAAAAAABAQD//P4A9/X4AAEBAQAAAAAAAAAAAAAAAAAAAAAA/Pz8AAcHHS4GBgcABQUDA+Lk5+fLzdCF8PHxpQkHB/Dz8/MAAQECAAIDAQD3+PkE0tXWSEA8O44jIR4lPjs3ebf19uTw8PCW+fj6APz8/gACAgIAAQECAQD/AAADAQPyLSso+wMDAwAAAAAAAAAAAAAAAAACAQEA7/HzAPT29gACAQEAAAAAAAAAAAAAAAAA/f/+/vz9/fwA/wAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAI+Ue8OAU5OvzAvQdAP4AAAD8AADuNO5H6uTqaQDw/qQBAQLz9CD1AAMBFAD35voA9in3APQ19QD5yPkADOsKAAL1AQAEB/wAAgABAAH7AgAABv4A/QH9AP8OAQD/8/8A9i32AO454wAS7BIADt0NAPsp+QADAQEA/wL+AA4aDgD89PwA9Qrz0wj5Bej7Af0dAAAAAAD+AAAECQQB+xH8GQD+AAAA/wAAAAUAAATlBPD87vyy7SrrAAP8AwAD/wIAAP8BAAL/AgAPAw8A/h0AAAT8BOz92fy03SHeZgAAAAAAAAAAAAAAAAH+ABj2HPYqAAEAAAAAAAAAAwAABuUF8gzCC6b7KvsA5VXmAPYg9QABAAEA//sAAAE4AQD/OO4A793vACC1HwAWxxaW6kHqWvsO/AgA/gAA6x/sOhMIFwT/AQX8AAEAAAAAAAAAAAAAAAAAAAEFAAD8/fwA/fv9AAABAAAAAAAAAAAAAAAAAAACAgIA7u/vsMXIyx4FBQUV09XWgQAAAc8JCAcABQUFAAUFBQABAQEAAgMCAP7+//zx8fK0vMDC8w0MSbMWFRINLiwqasnLzqYFBQTwAgIBAP8A/wAAAAD/AAAAAAABAAIBAAAAAQAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAD/AAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJsw/vBwzt0/Mi9B0A/AAAAP0AAPwR/QnbW9yWCeEHdQvtC8UBBADg9wn53/sO/PsB/gHYA/YD8QICAfHzK/LpBswHAAEF+gD+B/0AC/EMAAAE/AD/BgAA/x8BAADqBAD6H/sA+hX6AP31/QAD+QMN/gD+/wAA/wADBAMAAQEBAAcGB/8C/wAe/wACCAAAAAAAAAAAAAAAAAQEBAECCgMJAP8AAAAAAAAA/wAAB/oF5f41+eb+Bf4A+gH/AAn0CgD83fwABvIGAAkACAABHQEABPgEAO7x7uUAAAAAAAAAAAAAAAAA/QAA/xH/9fgY+R4A/gAAAAAAAAACAAAL5Qvd8ify5eBQ4QAABwAAA/oDAAD/AAAA/wAAAP0AAP8CAgDzIvMA1WfWACLBINQK6gnm8RDzIgD+AAD/DwAQCT8HFP8B/+gAAQAAAAAAAAAAAAAAAAAAAQMBAPnp+QD2xPwAAQMBAAAAAAAAAAAAAAAAAAAAAAD9/f3T2t3eZQD//wD7+/sACQkKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUEBQD09PTB1tnbthQTNVcaGRcrBwcGMPb29sD4+PkADQ0MAAMDAwYAAAAA////Avj4+QADAwMAAAAAAAAAAAAAAAAAAQEBAP38/AD//v4A/gEBAAAAAAAAAAAAAAAAAP7+/gABAQH+AAEAAAQDAwYAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP3y/QAWvRSN7jHtSP4R/gIA+gAAAP0AAAAF/wD3J/gh9//4IQD8AADbLd8AAf0BAAH/AQAA+wH87Q/t7QT1A8kM5gK6COcIpgP8A+8BAAEAAP8BAAD+AQAC9QIB9xT6R/oW+U0ABwAZ7iXvFfYb9Rj/Av/+AAAAAAECAgAAAAAAAf8B//wF/Av1EfcVAP0AAAAAAAAAAAAA//4AAP8A/wAAAQAAAAAAAAABAAAG+wf0DvUR/v0F/QAAAAAAAAYAAOoR6AD54PsAAPIAAAAA/wAC9wIA8yLzCQABAAAAAQAAAAAAAAH+AAD3HPjy/g3+CAD/AAAAAAAAAPwAAPch9yPnVee8/Qz9+QP8AgD+B/4AAAAAAAD/AP8AAAAA/wH/AQD/AAD9CfwA40TkDfoV+hoA/QAAAAEAAPsF+RDeHdkXAAEA6AAAAAAAAAAAAAAAAAAAAAAAAQAAABsAAA7iEgAF4AcA/wX/AAAAAAAAAAAAAAAAAAEBAQAEBAQAAAAAAPr6+gAAAAAAAAAAAAAAAAAAAAAAAAAAAPz8/QD///8ABAQDAAEBBgDm5+e59ff4VwsJCQAPDw9C8/TzwP38/AACAAEAAf8AAgEBAAD+AAEW2NvdDwICAvEAAAAAAAAAAAAAAAD///8ADAwMAAkICAD+/v4AAAAAAAAAAAAAAAAA/f3+//7+//7//wAAAAEBBQAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAkBAAjoCb4UsBNw70rwifsX+wkA/wAAAP0AAAD8AAAA/wAAAAAAAAAEAAD/BP8A/wD/AAAJAAT4EP0XAAAATvQn9U4LLAtlEbMQZQLwAcv//wAA/Qj+Ju067Y3tN/BM/gz9AAD/AAAAAvoA9wL2AAD/AAAAAAAAAAAAAAAAAAAAAAAAAP8AAAD9AAAA/wAAAAAAAAAAAAAAAQAAAAAA/wAAAAAAAAAAAAAAAAABAAH//v8B//4AAAAAAAAB/QAA+v/6APwA/gABAQAAAAAAAP8BAAD/AP8BAAAAAAAAAAAAAAAAAP8BAAAD/wT/Af8CAP8AAAAAAAAA/gAA9xL4DgAUAEMA/gAA/gD9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD1AvUAAP0AAAD+AAAAAwAA/wAAAf4B+wIAAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAD/Bf4A9jPyAA/kFgABAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMDAwAAAAAAAgICAPb198T09PXvCxUJERAQDhHy8w8AAQEBAPLy8QACAgDx/v7+AAkIBxEGBgYMAgIB8QAAAAAAAAAAAAAAAP///wACAgEAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAwMBAQAAAAD9/f4CAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA/gj+AAflCJMbpxvM7jLudPkg+UT6E/oO/wf/AAADAAAA/wAAAAEAAAABAAAA/gAAAP8AAAD9AAAA/wAAAAIAAPU59S/8C/yrCpgKPgYABOfrV+aj7z7xNgD7AAAA/QAAAAEAAAD/AAAA/gAAAAAAAAAAAAAAAQAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAA/wEAAAABAAAAAAAAAAEBAAAAAgAAAAAAAAD/AAAAAAAAAP8AAP8A/wAAAAAAAP8AAAABAAAAAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAP8AAAAAAAAAAwAAAAAAAAAEAAADAQT6GvQe8v8B/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFAAD86P4AAf8AAAADAQAAAAAA//8AAAoKCQABAQAA+fn7AAAA/wAAAAAAAAEAAAEAAQABAQEAAAEAAP39/QD/AAAAAAAAAAAAAAAAAAAA+vv67fb29vUCAgIRBAMDBQUFAwABAgAA/f3+AP3+/v0AAQEABQQEBQsKCggBAQEAAAAAAAAAAAAAAAAA/wD/AAgICAAGBgYAAP8AAAAAAAAAAAAAAAAAAPv8+wEGBQUH/v39APr6+t8AAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP75/gD/AAAABvoGAP77/scRzBFT/tT/I/ka9i4BAAM27x7wHP8Q/gwC/AIcAv0BGv0N/Qn7DfwR/w7/AAD8AAAA/gAAAPcAAPkr+R/1PfRe/ewH5O858TYA+QAAAP8AAAAEAAAA+gAABO4E+AT1A/QD/wD7/wAAAf8A/wAA/wABAAAA//0G/wn6FfsPAAQAAAD/AAAAAAAAAQABAAABAAAA/wAAAAAAAAAAAAD+Af4E+wr5AP0F/gD/AgAAAv0CAAjvCAAEAv4AAQQAAAAAAAACBQIA+QH4/gAAAAAAAAAAAAAAAP8B/wAF9wP++v4B/gABAAAAAAAAAAEAAAP2A/sF8APrAv0B////AAEBAP///wAAAQAAAP///wABAgEA/wAAAQAAAAABAQAA/v8A/wEA/wAA/gT/AgvpCuARzRbm/wD/AAABAAAAAAAAAAAAAAAAAAAAAAEAAAD/APDc8AD7A/sAAQABAAAAAAAAAAAAAQEBAAEBAAD9/fwA+/v7AAEBAQD7+/sAAf//APn6AgD5+foA////AAAAAAAAAAAAAAAAAAAAAAD9/f7+/f39/gAAAAACAQEBAwMDAPv8/QD29fYABQYF8v/+/wADAwMIDhANCwIBAgAAAAAAAAAAAAAAAAAAAQAA/fv8APr5+gABAQAAAAAAAAAAAAD///8AAAABGgsLCh/8/f0A/f7+0wAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAID/fwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD78PoAAgACAA8NDgD2HvQA/Nb/APgm+d0F9ATdBvIIr/EN8gD+CQAAC9cLAgrnCBPzJPMHE/IMH/sJ+3vsR+xJAAEAAAD/AAAAAAAA9SH2U/cZ+PYABAAAAPkAAAAEAAAI5AjrHr4cge0V8LoG9QD1DfgC/P0BAAD8/PwB/QL/AAD+AP8J8gwb51LtufQb9hUA/AAAAAAAAAEAAP8B/gD+AAAAAAAAAAAA/wAA+AT4EfLu9Af/BP/2Af4BAA7tDgAJ3wsAAxECABYbFAAB/QIABfoEAOAQ4Pfr/uoAAAEAAAAAAAAAAgAAB+cGIfYY+PkAFAAAAAAAAAAFAAAO4QzVGq4VSPgk8v39Af0BAPr+//4CAAEBAAD//AAAAQcBAv8BAgIA/QP/AQb9Av7+/QAB+gH6AP8BAAIF7Qf77Nvt9wABAAAAAAAAAAAAAAAAAAAAAAAAAP8AAP/7/gAAAQAAAQEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQABAQEA//7+APn7+QD19vUA/v7/AAUFBQABAQEAAAAAAAAAAAAAAAAAAQIC/wUGBwD///8AAgICBAMCAwAA/wAAAwMCAPn6+r/3+Pj/CgoJBvLy9Rvv7+/SAwMDAAAAAAAAAAAA////AAkIBwAQDw8A/wD/AAAAAAAAAAAAAAAAAAUEBC8JCAgS+vv7AOnp6ucAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAA/wAAAf8BAAT+AwoA5P/2C/ULAAAf/QD2BPcAB/UGAPrz+wAAAv/+AfsB6wP3AwAF5wXkDMwNYvIt8dT6HvsTAPsAAAD8AAD5GPkfCP4H8wD8AAAAAgAAAAAAABShFocK0wuV8VL0AAH+/QD/BP8A/wAAAAD9AAAA+wIAA+0IAALuCQP+Av0CAAAAAAAAAAAAAQAA+v37AAPyAvMAAQAAAAAAAAD/AAD7DP0eDtUTKwjtCMv+A/4AARL8AAAE/wD3//cA2+3dAAfcCAAD+gMA60brNQABAAAA/wAAAAEAAAADAAD/8gH1CecH4Psk+hwA/AAAAP4AAPYY9yMJ4BEHAOYE4vcy8AD/CPwAAfgBAAH+AAABAgAAAAcAAAIABAAE5ggAC/kMAAT+BwDwB+sA9xzyAP0H/AAAAgAAAAEAAAAAAAAAAAAAAAAAAAAAAAD/+/4A/wH/AAAAAAAA/wAAAAAAAAAAAAAAAAEA/f3+AP///gn+/v73AAAAAAAAAAAAAAAAAQABAAEBAQAAAP8A/v7+AP///wAAAAAAAAAAAP7+/gAKCgsqDg0MFv39/QD19vb59PT1AAgIBwAMDQ0A/f3+6/X19vcWFRMKCAcGMPX19bcFBQUAAAAAAP///wACAgMA//7+AAcHCAAKCgoA////AAAAAAD///4ACQoKGAkICADu8fD19vj43wAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP4AAAX/BADwN/CF4ULkzfUt9lYF8AYAB+wJAAjtBgAhryAAFbMUAPkV+QAG5gkAGK8ZAAS6BQABIQEDAgIC/wD/AAAAAAAAAAEAAAH5APv8/PsAAAEAAAAAAAAA/wAAAe3/6Pfl+gAcshsAAuIHAO0X+QDxEfoA9Qz/AAzNFgAK3g4A9g33PvYS+hMA/gAAAAAAAAAAAAAC/wL8EesS1AACAAAAAAAAAP0AAP4T/QT3OfVmCQEIAAj9BwD/A/4AAAAAAAAAAAD8AfwAAf4AAP0k/DjtMuxEAPwAAAAAAAAAAAAAAAAAAP8BAekJ3wq6AP4AAAAAAAAA/AAA+Rf6DeVY44oG4QoEDswYAArmFAAMwRcACrYXAA7EFwAE4BAAEtUcABDbFgD//QI0+A/5UBDSGAAbyiUAEuoXAAUJBAAGBwYABgcGAAYHBgAGBwUABAEDAAQDBAAFBQUABQUFAAUFBQAFBQUABQUFAAMDAgQVFBJyNTMwyhgXF3f+/v8AAQEBAAEBAQACAgIAAAAAAP///wAGBgYABgUFAPz9/QD+/v4A+vr6ABIRDzcNDAsI+fr6AOzr7NT49vYA8/HyAPX19QD19fXi8fLz1QQEBQAfHh1NAwMDAAMDAwADAgIAAwMDAAcHBQACAgIA9fT0AO/v7wDu7u4A7+/vAOzs7AMPDQxVBwcGAO3t79r+/PzdAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAF/AUA8lnyf95a33rzJvQo5XHlp+xa6pr8DPwsAv0CAAT1BAAC9wIA/Qr8AAAAAAAE9QUAARUAKuxG66P4GvkUAPwAAAAAAAAAAgAADOMOxAHtBeIAAgAAAP4AAAADAAD6TfpgCS4GFwYABgD+/v8A9wf9APgH/QD7Cf4ABPYFAAMEAyXxQfB4AAn/AAD+AAAAAAAAAP8AAAL/Af8J3QfRAfwAAAABAAAAAAAAAP4AAORK5WTmSuaTAAn+GwP3AgAD9QMAA/UDAAP5AwD/Mf1H5nnlofsP/AkA/QAAAAAAAAAAAAAA/wAA/AP8BAzWDKcC6gL5AAUAAAAAAAAA+gAA50foV+l06KP+D/0nBPcDAAP1AwAD9AMAA/UDAAL5AgD+//8b7kPteddp173TZNSv7DPsZgnsDAAW4x4AERARABARDwAQEQ8AEBEPABETEQALCQsAAwIDAAQEBAAEBAQABAQEAAQEBAAEBAQAAQAB/ycmJWYzMC0sJSQhU////gUAAAAAAQAAAAcGBwABAQIA/wAAAAICAgAEBAQABQMDAAEBAQACAgIIKCclWgYFBQDu7/Hx8/X2uQQFBQABAgIAAP8AAP/+/vjm5+nE/f39AB0bGjESERE9AQAAAAICAgAGBgYABAQEAP///wACAwMAAwICAPr6+gD3+PgABQUFOh4dGz79/v4A4+XmxfLz9P4AAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPdk9YDcWtx///n/AAD4AAD+CP4C6FDoZew+614L4gnL8df00fkE+ekB/QLz7zruFfVH9CTuMe5K+hP6DAD5AAAAAQAAAAEAAAD7AAALzwulBRcDO/I48TwAAAAAAPsAAOlD6Ub5JPfEC8oNxPYH9eIBA//xAAYA//0I/QjqTugm8zTzRvce9xoA+gAAAAAAAAAAAAAAAQAA/AX9CA7bD70L6Avq9Bn1FgD7AAAA/gAA/wP/AONS42zsOOlXDNkMwQMwCOT9BPwG+ib4LPQs9D79CP4AAPoAAAAAAAAAAQAAAAAAAAAAAAAABv8JCfII3B7AHZLgWeF1APwAAAD/AAAA/AAA5kbmWO0z60sO2g3AASkF4QD5APsACQAG+x/5IOAg4Dn4GPkSAP8AAAAAAADfZ96ZBKUECRbhFpP9BP3+AAAAAAAAAAAAAAAAAQIBAP0O/QD20fcAAP7/AAAAAAAAAAAAAAAAAAAAAAD8/Pz93uDgtiMhHSceHBszFhUVWO7u7qMHCAgA////AAAAAAAAAAAAAAAAAP8AAAAFBQUA/f3+AB8eHW8lIiA1/f39ANna3KX19vbfBQQEAAICAgD8/v4A/v39AOnq67Dw8fJxFRQRCCcmJHfCxMfFBQUE/ggCAgAAAAMAAAAAAAD//wAEBAQABAMDAPDy8gApKCZpFhQTB+nr7Pfk5OaeCwoJAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+ub76ALwAvr9D/0GAPkAAAD+AAAA+QAA/A79B/Ev9DzxM/Q8APsAAAD5AAD3NPgAAAMAAAD7AAAA+QAAAAAAAAAAAAAABwAAE78TuxDaELDx8fDoA/ACcP0N/gIA+wAAAPoAAPAe8iTwIvIkAPsAAAD7AAAAAAAAAAEAAPQJ9AAA/AAAAP8AAAABAAAA/wAAAAAAAAAAAAAABAAFAe0C5RmzGo/baNuHAAIAAAD+AAAA/gAAAPoAAPcY+hX3HO5UAAIAAAAAAAAA/QAAAPsAAAD8AAAABAAAAPwAAAD/AAAAAQAAAAAAAAIDAgH7Bf0AGK8Yg/Ib8jjuQe89APcAAAD+AAAA+QAA+Q/7Des67k0APAAAAAAAAAD/AAAA/gAAAP0AAAD7AAAAAAAAAAIAAP8R/wD0MvJICQUGlf0G/f4AAAAAAAAAAAAAAAAAAgAAAPUAAPnN/AD/+v8AAAEAAAAAAAAAAAAAAAAAAP///wDZ2tyeLSooYhoYFw85NjOKwMPG7vn5+bUDAwMA////AP///wD///8A/v7+APn6+gYeHRxqQT47gQkJCADd3+Lm19rbjP7//v79/v4AAwICAAQCAgD/AAAA7/Dwytrc3rE4NDFPMC4rS+zu7n/V1dd/BQUFAP//BAD///8A////AP7+/gD6+/wAIyIhfjUxL1f9/v4Az9HVm+7w8M8FBAQAAAAAAAAAAAAAAAAAA4CAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACXb5jMEeERwvsO+y35GfkaAAYAAAD/AAAA/gAAAP0AAAD7AAAA/gAAAAIAAAD9AAAA/AAAAP4AAAABAAAABQAAAAUAAAzLC8kdsxtcB/cJ8QLwAeMG+wYR8i/zOAACAAAA/gAAAP4AAAD8AAAA/gAAAAIAAAABAAAAAAAAAP4AAAADAAAM5gztAAH/AP0H/gUA/wAAAAMAAAjsCOoWyBiZBNYExgYOBhvwJ/AyAAMAAAD/AAAAAAAAAP4AAAD8AAAA+wAAAPsAAAD+AAAABgAACPMI8BLcEdH1FvYcAP8AAAABAAAAAQAACdcLygHvArgG6gb6Ds4OuP4Q/Sb4I/cn/wMAAAD+AAAA/gAAAPwAAAD7AAAA+wAAAPwAAAD9AAAA/gAAAAIAAAAHAAAE9wMAFrYVnReWGWb/ygH3AdQDAAHSAwAB0gMAAdIDAAHQAwAC2AQA/f79AP79/wABAQAAAAAAAAAAAAAAAAAAAQEBAPr6+urv8PH3Hx0bSBgWFQwZGBdQ+fr7G/n6+vz4+fn3////DAcFBRQQDw5EKyonizs4NWsQDQwE5+nr+sHEx2v29/fDAwICAAAAAAACAQEAAwICAAABAQACAgL87e3uzQsMC0YaFxYUHh0aPwUFBi33+Pjz/f39+QD/AA4EAwMSDAwMOScmJI06NjRoDAwKAc3Q1MHNz9BqAgIC9wEBAQBubm0AAAAAAAAAAAAB////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACczKAAGEQYAAQwACu827GL0M/Vf8iH0M/4M/gEA/gAAAAEAAAAAAAAAAAAAAAAAAAD/AAAAAQAAAP4AAAroCekPuxCgDNILiAcJBfAJ7w0A+Pv3AAUDBAXsOe2G6krqavgV+AoAAAAAAAAAAAD/AAAAAAAAAAAAAAABAAAC9gIAE8ISuxzmGXvdU9uz8hH4FwD+AAAABgAADdUO3jSHNSf09vP8CB4HAeAm4HrsR+tt9xz4FwAAAAAAAAAAAAAAAAABAAAAAAAAAP4AAAvgCd4hnyNt8RbyMONv4oUA+QAAAAAAAAACAAAYpxaIC9kJeQjrCgAG5QoAABX9A+sx7HDsSetu+SD6Hv8E/wAA/gAAAAEAAAAAAAAAAQAAAAAAAAAAAAAC8gIAD9cPwBe6Fo0J4Qe0++/9AAD3AgAAAgAAAAAAAAAAAAAAAAAAAAAAAAEBAQD59/oA/Pr9AAEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPv7/AMlIyGAOTc1cBYVEgwBAAEA8fP0APb39/L//v8ABAMDAwwNCwsMCgoA+fr6AN3f4dvNztF48PHysAUEA/7/AAAAAAAAAAEBAQACAgIAAAAAAAAAAAD+/v4ABwcHMTEwLZcpJiQ3BwcGAPLz9AD29vf0/v//AAMCAgIJCAgKDg0MAPf4+ADQ09a4zM/QYfn4+egCAgIAAAAAANzb2gAAAAAAAAAAAAH///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJzMoAAUWBQABC/8A+w35AAT5Bwn2HPQ39Bj1SPgq+CMABwAm+gL7Ev8DAAEC+gH8DQML1//n/+ME2wXQDuIOtQbeB+H5E/gABg4EAArsDgD4/PcAAggBAAHuAgAAKv8v6CfoW/Yj9zD9Bv0p+wj7BgH9Af8I+wXlAukF1BTcE68KxAq4BR8D+O0k7XfwA/IXAv4B//4E/gEL6gzmF8AZkP8E//wKJgkA8PLwAP8U/SDqGutd9jL2OP8K/yz8CP0FBfUE+AT2A9AL0Q3FDdcLpgjRC+fyZu+O31nhcQD4AAAAAQAAAfwBABfDFFMF9AOuAvMFABC2EwD/Dv4A9vT2AAAY/hj3J/lI8ibzPgQaAif1AfYf/wX/AgX3BPsE+APZAN8D1ArnB7MQ2RPDB/sG/P8E/AD+6gAAAPUCAAACAAAAAAAAAAAAAAAAAAAAAAAAAQEBAPn3+gD8+v0AAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAA////AAoJCQAODQ42HBwZYBgWFUEQEA8cBQUECAD/AAD/AAD/+/r6+uvt7tfj5OW15ebonv///+L9/f0AAAAAAAAAAAAAAAAAAQEBAAICAgAAAAAAAAAAAAAAAAAAAAEA+vv6EB0cGl4lIyJcFBIRKwQEBAL///79/wAAAfv7/Pjq6+3b3t/gn+Xm56T+AP/1/Pv8AAAAAAAAAAAA3NvaAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAD+Bv4A/v7+9wXuBcAN6QzAABL/ABDVEAb56fwN/wn/AAPzAv0HRAPw+ej6AALZBAAACf8A/gf+AAACAAAAAAAAAAAAAAAAAAAAAQAA/Qb+AAX5A9EL2gvRA/IEAArSChX4EPgEAvcD/wYZAe72Dfr6Bu0GAP0I/QAG8AYACtIMifwA+wAB/wEA/wL/AAf1BwD39fcAAggCAAIAAQD+CP0A+gr64AP1AoMOzA8ACtsLEvkN+QQG6wP59yr58QrgCgAC6QUA+wT7Dusz6lIA/QAAAAAAAAACAAAB+AEA+Ov97/f//AAABvgABhYJAAv2CQD/B/8AAAIA6AbdB6AHA/qgEd8OAQjTChP9Df4BCe4G/Psk/O/79f0AAgIBAP4G/gD/AP8AAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQDAP7+/cro6uvK3+DjDg0NCxIMCwsHAP//AP8AAP/5+Pn98fPy5gIBAfcCAgIA////AP39/QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAICAQADAwLw7e7ukvj4+pwLCQgaAwID//z+/f0BAAAB9fn4+u/u8O0EAwP4BAQEAPv9/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wP/AP4C/gD/BP8A/wT/+v8E/+3/BP8A/wX/AP4D/gD+Bf4AAQb/AAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wL/AP4G/gD/BP8A/wX/6/8E/wD/BP8A/gQAAP4F/gD/A/8AAQABAAACAAD/Bv4A/gH/AAH/AAD/Av8A//n/AAICAgD7HPoA+gD4AAMAAwD0FvYG9Sb3DBLOD+4CDQHuAQ0BAAD8AQD7BvwA/BH8AAntCg/tb+uS9CL3HwD8AAAAAAAAAAMAAAbsBuIW2BPT/wAAAAADAAADCAEA/vn+AAD8AAAAAQAA/wX/AP8G/wD+AP7//wX/7P8E/wD/Bf8A/wX/AP8G/wAA/gEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBAAQDAwAEBAPyAgIC8gICAgAA//8A/wAAAAICAgAEBAQAAQAAAAAA+wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAwMABAQD9gICAtwCAgIAAgD/AAEAAAADAgIABAQEAAMCAgD8/PwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8A/wAAAf8A/SH7AP8V/QAC+QMA/wP/GfUl9aj4Ifgq9e4GogH9A6kFAAPbBAgD/f4B/g75Ffk/7EXec/Qy9DEA/QAAAP8AAAABAAAAAAAAEs0QrATxBOz+CP4AAAH/AAH+AAD/Bv8AAfgAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB+wEABwAIAAQHBQD/Cf8AAvYDAAADABnmTueu8znyOAD/AADsR+1y7/DwbAfsB+cB+wH8+Q76EPwV/BMACv8AAPsAAAD/AAAAAAAAAAYAAAnhCOoVzBaVA/oDAP/9/wAAAAAAAAEAAP7//wD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAA/gX+APol/AADB/8A9//5AAAAAAAB+wEJ8yvztfQ39DEA+AAAAP8AAAABAAD9Ef0G9hP2HwAAAAAA/wAAAP0AAAD6AAAAAAAAAAAAAAAGAAAF7gX5G6Qca/7qAN79BPwAAAEAAAAAAAAAAAAA//3/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB/wAL9A0AAeUDAP8C/gAAAAAAAAAAAAH8Av7/Cf/E9yP3Lf0S/QQA/AAAAP4AAAD+AAAA/QAAAAAAAAAAAAAAAAAAAAEAAAADAAAAAgAACOMH6RudHHAEzges+Af5AAABAAAAAAAAAAAAAAD/AAABAgIAAQgAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wIAAAnuCgAK3gsA/wL/AAAAAAAAAAAAAfwB+QbvBm4X2Reu6D3odOwu7En9Dv0CAAYAAAD9AAAA/wAAAAEAAAACAAAA+AAAB+8G8xXGFqcOyQ+K/uj+3f8G/gABAgEAAAAAAAAAAAAAAAAAAAEAAAEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wL/AP4G/gD/Av8AAAAAAAAAAAABAwEA/gf9AAnrDMAkrCTADLoNOupA6kgCAAIxCxcJC/8I/wcE9AXy9e/18wf4A8QYwxu5BvYI2fYE9gD9B/wAAPMCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-filename="logo.png" style="text-align: center; width: 25%; float: none;"><span style="text-align: center;">&nbsp;</span></p><div class="WordSection1"><h1 style="TEXT-ALIGN: center; LINE-HEIGHT: normal"><b><font color="#0000ff">Traspaso de Pedidos de Venta</font></b></h1></div>
<p class="MsoNormal" style="TEXT-ALIGN: justify; LINE-HEIGHT: normal"><span style="FONT-SIZE: 14pt; FONT-FAMILY: &quot;Myriad Pro Light&quot;,&quot;sans-serif&quot;">Estimado cliente,</span></p>
<p class="MsoNormal" style="TEXT-ALIGN: justify; LINE-HEIGHT: normal; MARGIN-RIGHT: -0.05pt"><span style="FONT-SIZE: 12pt; FONT-FAMILY: &quot;Myriad Pro&quot;,&quot;sans-serif&quot;">Nos complace informarles que, con la finalidad de mejorar nuestro servicio y relación con ustedes, tenemos Traspaso Pedidos de Venta, un <b>PORTAL para el Traspaso de Pedidos a Albaranes de Venta.</b></span></p>
<p class="MsoNormal" style="TEXT-ALIGN: justify; LINE-HEIGHT: normal"><span style="FONT-SIZE: 12pt; FONT-FAMILY: &quot;Myriad Pro&quot;,&quot;sans-serif&quot;">Con esta herramienta ustedes podrán crear los albaranes de venta correspondientes a los pedidos.</span></p>
<p class="MsoNormal" style="TEXT-ALIGN: justify; LINE-HEIGHT: normal"><span style="FONT-SIZE: 12pt; FONT-FAMILY: &quot;Myriad Pro&quot;,&quot;sans-serif&quot;">Si bien su utilización creemos que es muy práctica e intuitiva,&nbsp;quedamos a su disposición para resolver cualquier duda que se les pueda plantear al respecto.</span></p>
<p class="MsoNormal" style="TEXT-ALIGN: justify; LINE-HEIGHT: normal"><span style="FONT-SIZE: 12pt; FONT-FAMILY: &quot;Myriad Pro&quot;,&quot;sans-serif&quot;">Acceso desde el link:</span></p></div><!-- /HEADER -->
    <!-- BODY -->
    <table class="body-wrap">
        <tbody>
            <tr>
                <td></td>
                <td class="container" bgcolor="#FFFFFF">

                    <div class="content">
                        <table><tbody><tr><td><!-- Callout Panel -->
                                        <p style="color:#2a968b; font-weight:600;" class="callout">
                                          {{Msg}}
                                        </p><!-- /Callout Panel -->
                                        <!-- social & contact -->
                                        <table class="social" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td><br><!-- /column 1 -->
                                                        <!-- column 2 -->
                                                        <table align="left" class="column">
                                                            <tbody>
                                                                <tr>
                                                                    <td>

                                                                        <h5 style="text-transform:uppercase" class="">ContactO:</h5>
                                                                        <p>
                                                                            Phone: <strong><font color="#085294">+34 934 608 900</font></strong><br>
                                                                            Email:&nbsp; &nbsp;<b><font color="#085294">info@merlos.net</font></b></p>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table><!-- /column 2 -->

                                                        <span class="clear"></span>

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table><!-- /social & contact -->
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div><!-- /content -->
                </td>
                <td></td>
            </tr>
        </tbody>
    </table><!-- /BODY -->
    <!-- FOOTER -->
    <table class="footer-wrap">
        <tbody>
            <tr>
                <td></td>
                <td class="container">

                    <!-- content -->
                    <div class="content">
                        <table>
                            <tbody>
                                <tr>
                                    <td align="center">
                                        <p>
                                           
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div><!-- /content -->

                </td>
                <td></td>
            </tr>
        </tbody>
    </table><!-- /FOOTER -->

'
where Name='confirm'

-- ----------------------------------------------------------------------------------------------------------------------------
update AspNetUsers 
set   Email='e.jene@merlos.net'
	, PasswordHash='ALBaGB8NK2kVq0eotHJ7v0G9KbYVR8WfRmipGjFUAOXD9jw4nUsStY+yn/eee/FSeQ=='
	, SecurityStamp='f44e0667-8c6a-4c6d-9456-910d2df7e59f'
	, ProfileName='default'
	, RoleId='admins'
	, CultureId='es-ES'
	, OriginId=1
where Name='Admin'
GO



if not exists (select * from [ContextVars] where VarName='Merlos_Usuarios') BEGIN
    INSERT INTO [dbo].[ContextVars]
           ([VarName]
           ,[VarSQL]
           ,[Order]
           ,[ConnStringId]
           ,[OriginId])
     VALUES
           ('Merlos_Usuarios'
           ,'select RoleId, Email, UserName, Reference, Name, SurName from AspNetUsers for JSON AUTO'
           ,0
           ,'ConfConnectionString'
           ,1)
END
GO



IF (N'$(IsProduct)' = '1') BEGIN
       EXEC pNet_CreateOrUpdateDatabase $(OriginDatabaseName), N'$(CurrentDacVersion)'
END


EXEC sp_MSforeachtable 'ALTER TABLE ? ENABLE TRIGGER all'

EXEC sp_MSforeachtable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT ALL'

COMMIT TRANSACTION
GO
