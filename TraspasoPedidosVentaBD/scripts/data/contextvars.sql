﻿

BEGIN TRY

MERGE INTO [ContextVars] AS Target
USING (VALUES
  (N'Merlos_Usuarios',N'select RoleId, Email, UserName, Reference, Name, SurName from AspNetUsers for JSON AUTO',0,N'ConfConnectionString',1)
 ,(N'usuariosTV',N'select RoleId, Email, UserName, Reference, Name, SurName from AspNetUsers for JSON AUTO',0,N'ConfConnectionString',1)
) AS Source ([VarName],[VarSQL],[Order],[ConnStringId],[OriginId])
ON (Target.[VarName] = Source.[VarName])
WHEN MATCHED AND (
	NULLIF(Source.[VarSQL], Target.[VarSQL]) IS NOT NULL OR NULLIF(Target.[VarSQL], Source.[VarSQL]) IS NOT NULL OR 
	NULLIF(Source.[Order], Target.[Order]) IS NOT NULL OR NULLIF(Target.[Order], Source.[Order]) IS NOT NULL OR 
	NULLIF(Source.[ConnStringId], Target.[ConnStringId]) IS NOT NULL OR NULLIF(Target.[ConnStringId], Source.[ConnStringId]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [VarSQL] = Source.[VarSQL], 
  [Order] = Source.[Order], 
  [ConnStringId] = Source.[ConnStringId], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([VarName],[VarSQL],[Order],[ConnStringId],[OriginId])
 VALUES(Source.[VarName],Source.[VarSQL],Source.[Order],Source.[ConnStringId],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO





