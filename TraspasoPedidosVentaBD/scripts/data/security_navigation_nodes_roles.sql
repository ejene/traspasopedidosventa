﻿

BEGIN TRY

MERGE INTO [Security_Navigation_Nodes_Roles] AS Target
USING (VALUES
  (N'2BC2E79B-BFAC-4CAA-B0DD-1D1C6758A6B7',N'users',0,1)
 ,(N'54066E2E-9EED-444E-A0AE-1E9D18E7E941',N'users',0,1)
 ,(N'3468C35B-9A5A-4F9E-9E03-2D6A1CBED087',N'users',0,1)
 ,(N'03A7CEF4-1668-4A82-A5C0-889472EB9ADF',N'users',0,1)
) AS Source ([NodeId],[RoleId],[CanView],[OriginId])
ON (Target.[NodeId] = Source.[NodeId] AND Target.[RoleId] = Source.[RoleId])
WHEN MATCHED AND (
	NULLIF(Source.[CanView], Target.[CanView]) IS NOT NULL OR NULLIF(Target.[CanView], Source.[CanView]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [CanView] = Source.[CanView], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([NodeId],[RoleId],[CanView],[OriginId])
 VALUES(Source.[NodeId],Source.[RoleId],Source.[CanView],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO





