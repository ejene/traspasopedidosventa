﻿

BEGIN TRY

MERGE INTO [Modules] AS Target
USING (VALUES
  (N'Merlos_Administrador',N'flx-html',N'project',NULL,NULL,N'Merlos_Administrador',N'Merlos_Administrador',N'none',1,1,1,0,NULL,NULL,N'
<div class="dvTit">Configuración de la Aplicación</div>

<div id="dvComunesYEmpresa">
	<div class="tit1">Comunes y Empresa</div>
	<table id="tbComunEmpresa" class="tbFrm">
		<tr>
			<td>
				BD COMUN
				<br><select id="selComunes" class="esq05" onchange="cargarEmpresas()"></select>
			</td>
			<td>
				Empresa
				<br><select id="selEmpresas" class="esq05"></select>
			</td>
			<td class="btnMVerde esq05" style="text-align:center;" onclick="configurarEmpresa()">Configurar</td>
		</tr>
	</table>
</div>

<div id="dvAppPersonalizaciones" class="fondo">
	<div class="tit1"><img id="imgPersonalizaciones" class="imgMenu" /> Personalizaciones a Clientes</div>
	<div id="dvAppPersonalizacionesContent" class="seccion">
		<div class="row">
			<span id="spPersDisteco" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Disteco</span>
			<div id="dvPersDisteco" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spPersTelsan" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Telsan</span>
			<div id="dvPersTelsan" class="dvSubSel"></div>
		</div>
	</div>
</div>

<div id="dvAppOpciones" class="fondo">
	<div class="tit1"><img id="imgOpciones" class="imgMenu" /> Opciones de la Aplicación</div>
	<div id="dvAppOpcionesContent" class="seccion">
		<div class="row">
			<span id="spSeriesFiltro" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Series - Mostrar todas</span>
			<div id="dvSeriesFiltro" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spImpAlbGen" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Imprimir Albaran al generarlo</span>
			<div id="dvImpAlbGen" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spResLin" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Resaltar lineas Dto. 100%</span>
			<div id="dvResLin" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spFactCajas" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Facturación cajas</span>
			<div id="dvFactCajas" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spImpEtiquetas" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Imprimir etiquetas</span>
			<div id="dvImpEtiquetas" class="dvSubSel"></div>
		</div>
		<div class="row">
			<span id="spImpAlbFact" class="confOp"><img src="./Merlos/images/check_O.png" width="28"/> Imprimir Albaranes y Facturas</span>
			<div id="dvImpAlbFact" class="dvSubSel"></div>
		</div>
	</div>
</div>

<div id="dvAppBasculas" class="fondo">
	<div class="tit1"><img id="imgBasculas" class="imgMenu" /> Básculas</div>
	<div id="dvAppBasculasContent" class="seccion">
		<div class="row cab">Báscula</div>
		<div class="row cab">IP</div>
		<div class="row cab">Puerto</div>
		<div class="row cab">Sentencia</div>

		<div class="row">
			<input type="text" id="inpBasculaIPActiva01" class="inv" disabled="">
			<img id="imgBascula01" src="./Merlos/images/btnBascula01_Off.png">
		</div>
		<div class="row"><input type="text" id="inpBasculaIP01" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPPuerto01" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPSentencia01" disabled /></div>

		<div class="row">
			<input type="text" id="inpBasculaIPActiva02" class="inv" disabled="">
			<img id="imgBascula02" src="./Merlos/images/btnBascula02_Off.png">
		</div>
		<div class="row"><input type="text" id="inpBasculaIP02" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPPuerto02" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPSentencia02" disabled /></div>

		<div class="row">
			<input type="text" id="inpBasculaIPActiva03" class="inv" disabled />
			<img id="imgBascula03" src="./Merlos/images/btnBascula03_Off.png">
		</div>
		<div class="row"><input type="text" id="inpBasculaIP03" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPPuerto03" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPSentencia03" disabled /></div>

		<div class="row">
			<input type="text" id="inpBasculaIPActiva04" class="inv" disabled />
			<img id="imgBascula04" src="./Merlos/images/btnBascula04_Off.png">
		</div>
		<div class="row"><input type="text" id="inpBasculaIP04" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPPuerto04" disabled /></div>
		<div class="row"><input type="text" id="inpBasculaIPSentencia04" disabled /></div>
	</div>
</div>

<div id="dvAppIdiomas" class="fondo">
	<div class="tit1"><img id="imgIdiomas" class="imgMenu" /> Idiomas</div>
	<div id="dvAppIdiomasContent" class="seccion">
		<div class="row cab chv"><img id="imgIdiomaNew" src="./Merlos/images/anadirLinea.png" onclick="idiomaNew()" /></div>
		<div class="row cab">Idioma <span style="font:12px arial; color:#666;">cód - nombre</span></div>
		<div class="row cab">Tipo <span style="font:12px arial; color:#666;">cód - nombre</span></div>
		<div class="row cab">Report <span style="font:12px arial; color:#666;">nombre</span></div>

		<div class="row new chv">
			<img src="./Merlos/images/floppy.png" onclick="idiomaNewGuardar()" />
			&nbsp;&nbsp;&nbsp;&nbsp;
			<img src="./Merlos/images/cerrar.png" onclick="idiomaNew(1)" />
		</div>
		<div class="row new">
			<input type="text" id="inpIdioma" onclick="cargarObjeto(this.id,''Idiomas'',''inpIdiomaCont'',''dvAppIdiomas''); event.stopPropagation();" />
			<div id="inpIdiomaCont" class="dvSel"></div>
		</div>
		<div class="row new">
			<input type="text" id="inpTipo" onclick="cargarObjeto(this.id,''Tipos'',''inpTipoCont'',''dvAppIdiomas''); event.stopPropagation();" />
			<div id="inpTipoCont" class="dvSel"></div>
		</div>
		<div class="row new">
			<input type="text" id="inpReport" />
		</div>
	</div>
</div>

<div id="dvAppUsuarios" class="fondo">
	<div class="tit1"><img id="imgUsuarios" class="imgMenu" /> Usuarios</div>
	<div id="dvAppUsuariosContent" class="seccion">
		<div class="row cab">Usuario</div>
		<div class="row cab">Impresora Documentos</div>
		<div class="row cab">Impresora Etiquetas</div>
		<div class="row cab">Report de Albarán</div>
		<div class="row cab">Report de Factura</div>
		<div class="row cab">Report de Etiquetas</div>
		<div class="row cab">Báscula predeterminada</div>

		<div class="row" style="position:relative;">
			<input type="text" id="inpMerlosUsuario" readonly onclick="cargarUsuarios(); event.stopPropagation();" />
			<div id="dvUsuariosListado" class="dvSel"></div>
		</div>
		<div class="row"><input type="text" id="inpImpDocu" class="inpUsuario" /></div>
		<div class="row"><input type="text" id="inpImpEti" class="inpUsuario" /></div>
		<div class="row"><input type="text" id="inpReportAlb" class="inpUsuario" /></div>
		<div class="row"><input type="text" id="inpReportFra" class="inpUsuario" /></div>
		<div class="row"><input type="text" id="inpReportEti" class="inpUsuario" /></div>
		<div class="row"><input type="text" id="inpBasculaPredet" class="inpUsuario" /></div>
	</div>
</div>




<script> /* #### script #### script #### script #### script #### script #### script #### script #### script #### script #### script #### script #### script #### script #### */
cerrarVelo();

' + convert(nvarchar(max),NCHAR(36)) + N'("#mainNav, .seccion").hide();
' + convert(nvarchar(max),NCHAR(36)) + N'("#dvComunesYEmpresa").show();

var Configuracion = "";				
var bascula01Activa = "0", bascula02Activa = "0", bascula03Activa = "0", bascula04Activa = "0";
var comunesCargados = false;
var empresasCargadas = false;
var seriesCargadas = false;
var lasSeries = "";
var seriesImpAlbGenCargadas = false;
var lasSeriesImpAlbGen = "";
var seriesResLinCargadas = false;
var lasSeriesResLin = "";
var seriesFactCajasCargadas = false;
var lasSeriesFactCajas = "";
var seriesSeriesFiltroCargadas = false;
var lasSeriesSeriesFiltro = "";
var confOpIO = 0;
var basculaTipoConexion = "Ethernet";
var personalizaciones = ["Disteco","Telsan"];

var checkO = "./Merlos/images/check_O.png";
var checkI = "./Merlos/images/check_I.png";

cargarComunes();
comprobarConfiguracion();

' + convert(nvarchar(max),NCHAR(36)) + N'(".confOp").on("click", function () { confOp_Click(this.id); event.stopPropagation(); });
' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppBasculas img").on("click",function(){ botonBascula(this.id); });
' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppBasculas input").on("blur",function(){ basculasConfiguracion();});

' + convert(nvarchar(max),NCHAR(36)) + N'(".imgMenu").off().on("click", function () { 
	var id = this.id; console.log(id);
	var a = ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).parent().parent().attr("id")+"Content";
	if(' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).hasClass("a90g")){ 
		' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).attr("src","./Merlos/images/flechaG.png").removeClass("a90g").addClass("a0g"); 
		' + convert(nvarchar(max),NCHAR(36)) + N'("#"+a).slideUp();
	}else {		
		' + convert(nvarchar(max),NCHAR(36)) + N'(".imgMenu").attr("src","./Merlos/images/flechaG.png").removeClass("a90g").addClass("a0g"); 
		' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).attr("src","./Merlos/images/flechaVerde.png").removeClass("a0g").addClass("a90g");
		' + convert(nvarchar(max),NCHAR(36)) + N'(".seccion").hide();
		' + convert(nvarchar(max),NCHAR(36)) + N'("#"+a).slideDown();		
	}
});

function confOp_Click(id) {
	// Objetos
	var objeto = id.split("sp")[1];
	if (' + convert(nvarchar(max),NCHAR(36)) + N'("#" + id).find("img").attr("src") === checkO) {
		' + convert(nvarchar(max),NCHAR(36)) + N'("#" + id).find("img").attr("src", checkI);
		if (id === "spImpAlbGen") { cargarImpAlbGen(false); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppOpciones").css("overflow","visible"); }
		if (id === "spResLin") { cargarResLin(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppOpciones").css("overflow","visible"); }
		if (id === "spFactCajas") { cargarFactCajas(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppOpciones").css("overflow","visible"); }
		confOpIO = 1;
	} else {
		confOpIO = 0;
		' + convert(nvarchar(max),NCHAR(36)) + N'("#" + id).find("img").attr("src", checkO);
		if (id === "spSeriesFiltro") { cargarSeriesFiltro(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppOpciones").css("overflow","visible"); }
	}

	objetoIO(objeto, '''', confOpIO);

	// Personalizaciones
	personalizaciones.forEach(elemento => { if(id==="spPers"+elemento){ personalizacion(elemento); } });
}

function personalizacion(cliente){
	var parametros = ''{"sp":"pConfiguracion","modo":"personalizacion","cliente":"'' + cliente + ''","confOpIO":"'' + confOpIO +''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		
	} else { alert(''Error SP pConfiguracion - personalizacion - cliente!\n''+JSON.stringify(ret)); }}, false);
}

function objetoIO(objeto, valor, confOpIO) {
	var parametros = ''{"sp":"pConfiguracion","modo":"objetoIO","objeto":"'' + objeto + ''","valor":"'' + valor + ''","confOpIO":"'' + confOpIO +''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		
	} else { alert(''Error SP pConfiguracion - objetoIO - ''+objeto+''!\n''+JSON.stringify(ret)); }}, false);
}

function iniciar(){ 
	cargarSeries();
	cargarSeriesObjeto("ImpAlbGen");
	cargarSeriesObjeto("ResLin");
	cargarSeriesObjeto("FactCajas");
	cargarSeriesObjeto("SeriesFiltro");
	cargarConfiguracion();
	cargarIdiomas();
	
	' + convert(nvarchar(max),NCHAR(36)) + N'(".imgMenu").attr("src","./Merlos/images/flechaG.png").css("width","20px");
	' + convert(nvarchar(max),NCHAR(36)) + N'("#imgOpciones").attr("src","./Merlos/images/flechaVerde.png").addClass("a90g");
	' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppOpcionesContent").slideDown();
}

function cargarComunes() {
	' + convert(nvarchar(max),NCHAR(36)) + N'("#tbComunEmpresa").fadeIn();
	' + convert(nvarchar(max),NCHAR(36)) + N'("#selComunes").html("<option>cargando comunes...</option>").css("color", "red");
	var contenido = "<option></option>";

	var parametros = ''{"sp":"pComunes"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		var elJS = JSON.parse(ret.JSCode);
		if (elJS.length > 0) {
			for (var i in elJS) { contenido += "<option>" + elJS[i].nombre + "</option>"; }			
			' + convert(nvarchar(max),NCHAR(36)) + N'("#selComunes").html(contenido).css("color", "#333");
			comunesCargados = true;
		} 
	} else { alert(''Error SP pComunes!''+JSON.stringify(ret)); }}, false);
}

function cargarEmpresas() {
	' + convert(nvarchar(max),NCHAR(36)) + N'("#selEmpresas").html("<option>cargando empresas...</option>").css("color", "red");
	var contenido = "<option></option>";
	var comunes = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#selComunes").val());

	var parametros = ''{"sp":"pEmpresas","comun":"'' + comunes+''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		var elJS = JSON.parse(ret.JSCode);
		if (elJS.empresas.length > 0) {
			for (var i in elJS.empresas) { contenido += "<option value=''" + elJS.empresas[i].codigo + "''>" + elJS.empresas[i].codigo + " - " + elJS.empresas[i].nombre + "</option>"; }
			' + convert(nvarchar(max),NCHAR(36)) + N'("#selEmpresas").html(contenido).css("color", "#333");
			empresasCargadas = true;
		}
	} else { alert(''Error SP pEmpresas!\n''+JSON.stringify(ret)); }}, false);
}

function configurarEmpresa(confirmado) {
	if(confirmado){	
		var comun = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#selComunes").val());
		var empresa = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#selEmpresas").val());
		var nombreEmpresa = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'(''select[id="selEmpresas"] option:selected'').text());

		if (comun === "" || empresa === "") { alert("Debes seleccionar COMUNES y EMPRESA!"); return; }

		abrirVelo(icoCarga50 + "<br><br>configurando la empresa<br><br><span class=''tit1''>" + nombreEmpresa+"</span>");
		var parametros = ''{"sp":"pConfiguracion","modo":"configurarEmpresa","comun":"'' + comun + ''","empresa":"'' + empresa + ''","nombreEmpresa":"'' + nombreEmpresa +''"}'';
		flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
			if(ret.JSCode!=="OK"){ abrirVelo(icoAlert50+"<br><br>Ha ocurrido un error al configurar la empresa!!!<br><br>Contacta con el administrador."); }
			else{ crearVistasYProcedimientos(); }
		} else { alert(''Error SP pConfiguracion - configurarEmpresa!\n''+JSON.stringify(ret)); }}, false);
	}else{
		abrirVelo(`
			Confirma para configurar el portal.
			<br><br><br>
			<span class=''btnMVerde'' onclick=''configurarEmpresa(1)''>configurar</span>
			&nbsp;&nbsp;&nbsp;
			<span class=''btnMRojo'' onclick=''cerrarVelo();''>cancelar</span>
		`);
	}
}

function crearVistasYProcedimientos(){
	abrirVelo(icoCarga50 + "<br><br><span class=''tit1''>creando las vistas...</span>",null,1);
	var parametros = ''{"sp":"pConfiguracion","modo":"crearVistas"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		if(ret.JSCode.indexOf("Error!")!=-1){ abrirVelo(icoAlert50+"<br><br>Se ha producido un error al generar las vistas!!!<br><br>Contacta con el administrador."); return; }
		abrirVelo(icoCarga50 + "<br><br><span class=''tit1''>creando los procedimientos...</span>",null,1);
		parametros = ''{"sp":"pConfiguracion","modo":"crearProcedimientos"}'';
		flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
			if(ret.JSCode.indexOf("Error!")!=-1){ abrirVelo(icoAlert50+"<br><br>Se ha producido un error al generar los procedimientos!!!<br><br>Contacta con el administrador.",null,1); return; }
			abrirVelo(icoCarga50 + "<br><br><span class=''tit1''>creando las funciones...</span>",null,1);
			parametros = ''{"sp":"pConfiguracion","modo":"crearFunciones"}'';
			flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
				if(ret.JSCode.indexOf("Error!")!=-1){ abrirVelo(icoAlert50+"<br><br>Se ha producido un error al generar las funciones!!!<br><br>Contacta con el administrador.",null,1); return; }
					abrirVelo(icoCarga50 + "<br><br><span class=''tit1''>creando las personalizaciones...</span>",null,1);
					parametros = ''{"sp":"pConfiguracion","modo":"crearPersonalizaciones"}'';
					flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
						if(ret.JSCode.indexOf("Error!")!=-1){ abrirVelo(icoAlert50+"<br><br>Se ha producido un error al generar las personalizaciones!!!<br><br>Contacta con el administrador.",null,1); return; }
						iniciar();
					} else { alert(''Error SP pConfiguracion - crearFunciones!\n''+JSON.stringify(ret)); }}, false);
			} else { alert(''Error SP pConfiguracion - crearFunciones!\n''+JSON.stringify(ret)); }}, false);
		} else { alert(''Error SP pConfiguracion - crearProcedimientos!\n''+JSON.stringify(ret)); }}, false);
	} else { alert(''Error SP pConfiguracion - crearVistas!\n''+JSON.stringify(ret)); }}, false);
}

function esperaYcargaComunEmpresa(comun, empresa) { 
	if (!comunesCargados) { setTimeout(function () { esperaYcargaComunEmpresa(comun, empresa); }, 250); }
	else {
		' + convert(nvarchar(max),NCHAR(36)) + N'("#selComunes").val(comun);
		if (!empresasCargadas) { setTimeout(function () { cargarEmpresas(); esperaYcargaComunEmpresa(comun, empresa); }, 500); }
		else { setTimeout(function () { ' + convert(nvarchar(max),NCHAR(36)) + N'("#selEmpresas").val(empresa); }, 500);}
	}
}

function comprobarConfiguracion() {
	var parametros = ''{"sp":"pConfiguracion","modo":"comprobarConfiguracionEmpresa"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		var js = JSON.parse(limpiarCadena(ret.JSCode));
		if(js[0].msj=="NoExisteConfiguracion"){ cerrarVelo(); }
		else{ esperaYcargaComunEmpresa(js[0].Comun,js[0].Empresa); iniciar(); }
	} else { alert(''Error SP pConfiguracion - comprobarConfiguracion!\n''+JSON.stringify(ret)); }}, false);
}

function cargarSeries() {
	var parametros = ''{"sp":"pSeries"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		lasSeries = ret.JSCode;
		seriesCargadas = true;
	} else { alert(''Error SP pSeries!\n''+JSON.stringify(ret)); }}, false);
}

function cargarSeriesObjeto(objeto) {
	var parametros = ''{"sp":"pSeries","modo":"cargarSeriesObjeto","objeto":"''+objeto+''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		if (objeto === "ImpAlbGen") { lasSeriesImpAlbGen = ret.JSCode; seriesImpAlbGenCargadas = true; }
		if (objeto === "ResLin") { lasSeriesResLin = ret.JSCode; seriesResLinCargadas = true; }
		if (objeto === "FactCajas") { lasSeriesFactCajas = ret.JSCode; seriesFactCajasCargadas = true; }
		if (objeto === "SeriesFiltro") { lasSeriesSeriesFiltro = ret.JSCode; seriesSeriesFiltroCargadas = true; }
	} else { alert(''Error SP pSeries - cargarSeriesObjeto!\n''+JSON.stringify(ret)); }}, false);
}

function cargarSeriesFiltro(tf) {
	if (!tf) { ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvSeriesFiltro").html(icoCarga16 + " cargando series...").slideDown(); }
	var contenido = "";
	if (seriesCargadas && seriesSeriesFiltroCargadas) {
		var series = JSON.parse(lasSeries);
		if(series.length>0){
			for (var i in series) {
				var elColor = " background:#666; ";
				if (lasSeriesSeriesFiltro.indexOf(series[i].codigo) !== -1) { elColor = " background:green;"; }
				contenido += "<div class=''dvConfSerieSeriesFiltro C'' style=''margin:2px; padding:5px; color:#FFF; " + elColor + "'' "
					+ "onclick=''asignarSerieConf(\"SeriesFiltro\",\"" + series[i].codigo + "\");''>"
					+ series[i].codigo
					+ "</div>";
			}
		}else{ ' + convert(nvarchar(max),NCHAR(36)) + N'(document).click(); abrirVelo("No se han obtenido series para filtrar!"+btnAceptarCerrarVelo); }
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvSeriesFiltro").html(contenido);
	} else { setTimeout(function () { cargarSeries(); cargarSeriesObjeto("SeriesFiltro"); cargarSeriesFiltro(true); }, 100); }
}

function cargarImpAlbGen(tf){
	if(!tf){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvImpAlbGen").html(icoCarga16 + " cargando series...").slideDown(); }
	var contenido = "";
	if(seriesCargadas && seriesImpAlbGenCargadas){
		var series = JSON.parse(lasSeries);
		if(series.length>0){
		for(var i in series){ 
			var elColor = " background:#666; ";
			if(lasSeriesImpAlbGen.indexOf(series[i].codigo)!==-1){ elColor = " background:green;";}
			contenido += "<div class=''dvConfSerieImpAlbGen C'' style=''margin:2px; padding:5px; color:#FFF; "+elColor+"'' "
					  +  "onclick=''asignarSerieConf(\"ImpAlbGen\",\""+series[i].codigo+"\");''>"
					  +  series[i].codigo
					  +  "</div>"; 
			}
		}
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvImpAlbGen").html(contenido);
	}else{ setTimeout(function(){cargarImpAlbGen(tf);},100); }
}

function cargarResLin(tf){
	if(!tf){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvResLin").html(icoCarga16 + " cargando series...").slideDown(); }
	var contenido = "";
	if(seriesCargadas && seriesResLinCargadas){
		var series = JSON.parse(lasSeries);
		if(series.length>0){
		for(var i in series){ 
			var elColor = " background:#666; ";
			if(lasSeriesResLin.indexOf(series[i].codigo)!==-1){ elColor = " background:green;";}
			contenido += "<div class=''dvConfSerieResLin C'' style=''margin:2px; padding:5px; color:#FFF; "+elColor+"'' "
					  +  "onclick=''asignarSerieConf(\"ResLin\",\""+series[i].codigo+"\");''>"
					  +  series[i].codigo
					  +  "</div>"; 
			}
		}
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvResLin").html(contenido);
	}else{ setTimeout(cargarResLin,100); }
}

function cargarFactCajas(tf){
	if(!tf){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvFactCajas").html(icoCarga16 + " cargando series...").slideDown(); }
	var contenido = "";
	if(seriesCargadas && seriesFactCajasCargadas){
		var series = JSON.parse(lasSeries);
		if(series.length>0){
		for(var i in series){ 
			var elColor = " background:#666; ";
			if(lasSeriesFactCajas.indexOf(series[i].codigo)!==-1){ elColor = " background:green;";}
			contenido += "<div class=''dvConfSerieFactCajas C'' style=''margin:2px; padding:5px; color:#FFF; "+elColor+"'' "
					  +  "onclick=''asignarSerieConf(\"FactCajas\",\""+series[i].codigo+"\");''>"
					  +  series[i].codigo
					  +  "</div>"; 
			}
		}
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvFactCajas").html(contenido);
	}else{ setTimeout(cargarFactCajas,100); }
}

function asignarSerieConf(objeto,serie){ 
	' + convert(nvarchar(max),NCHAR(36)) + N'(''.dvConfSerie''+objeto+'':contains("''+serie+''")'').css("background","orange").html(icoCarga16);
	var parametros = ''{"sp":"pSeries","modo":"asignarSerieConf","objeto":"''+objeto+''","serie":"''+serie+''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		if(objeto==="ImpAlbGen"){ seriesImpAlbGenCargadas=false; cargarSeriesObjeto(objeto); cargarImpAlbGen(true); }
		if(objeto==="ResLin"){ seriesResLinCargadas=false; cargarSeriesObjeto(objeto); cargarResLin(true); }
		if(objeto==="FactCajas"){ seriesFactCajasCargadas=false; cargarSeriesObjeto(objeto); cargarFactCajas(true); }		
		if(objeto==="SeriesFiltro"){ seriesSeriesFiltroCargadas=false; cargarSeriesObjeto(objeto); cargarSeriesFiltro(true); }
    } else { alert(''Error SP pSeries - asignarSerieConf!''+JSON.stringify(ret)); }}, false);
}

function botonBascula(id, noEnviar){
	var elId = id.split("imgBascula")[1];
	if(' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).attr("src")==="./Merlos/images/btnBascula"+elId+"_Off.png"){
		' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).attr("src","./Merlos/images/btnBascula"+elId+"_Activa.png");
		' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIP"+elId+",#inpBasculaIPPuerto"+elId+",#inpBasculaIPSentencia"+elId).prop("disabled",false).css("color","#333");
		window["bascula"+elId+"Activa"] = 1;
	}else{
		' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).attr("src","./Merlos/images/btnBascula"+elId+"_Off.png");
		' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIP"+elId+",#inpBasculaIPPuerto"+elId+",#inpBasculaIPSentencia"+elId).prop("disabled",true).css("color","#999");
		window["bascula"+elId+"Activa"] = 0;
	}
	if(!noEnviar){ basculasConfiguracion(); }	
}

function basculasConfiguracion(){
	var bascula1 = ''{''
				+''"bascula":"bascula01"''
				+'',"activa":"''+bascula01Activa+''"''
				+'',"basculaTipoConexion":"''+basculaTipoConexion+''"''
				+'',"IP":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIP01").val())+''"''
				+'',"puerto":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPPuerto01").val())+''"''
				+'',"sentencia":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPSentencia01").val())+''"''
				+''}'';
	var bascula2 = ''{''
				+''"bascula":"bascula02"''
				+'',"activa":"''+bascula02Activa+''"''
				+'',"basculaTipoConexion":"''+basculaTipoConexion+''"''
				+'',"IP":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIP02").val())+''"''
				+'',"puerto":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPPuerto02").val())+''"''
				+'',"sentencia":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPSentencia02").val())+''"''
				+''}'';
	var bascula3 = ''{''
				+''"bascula":"bascula03"''
				+'',"activa":"''+bascula03Activa+''"''
				+'',"basculaTipoConexion":"''+basculaTipoConexion+''"''
				+'',"IP":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIP03").val())+''"''
				+'',"puerto":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPPuerto03").val())+''"''
				+'',"sentencia":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPSentencia03").val())+''"''
				+''}'';
	var bascula4 = ''{''
				+''"bascula":"bascula04"''
				+'',"activa":"''+bascula04Activa+''"''
				+'',"basculaTipoConexion":"''+basculaTipoConexion+''"''
				+'',"IP":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIP04").val())+''"''
				+'',"puerto":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPPuerto04").val())+''"''
				+'',"sentencia":"''+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPSentencia04").val())+''"''
				+''}'';

	var parametros = ''{"sp":"pBasculas","modo":"edicion","bascula1":[''+bascula1+''],"bascula2":[''+bascula2+''],"bascula3":[''+bascula3+''],"bascula4":[''+bascula4+'']}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){

	} else { alert(''Error SP pBasculas - edicion!''+JSON.stringify(ret)); }}, false);
}

function cargarConfiguracion(){
	abrirVelo(icoCarga50+"<br><br>cargando la configuración...");
	var parametros = ''{"sp":"pConfiguracion","modo":"cargarConfiguracion"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		var js = JSON.parse(limpiarCadena(ret.JSCode));
		// Configuración
		for(var i in js.configuracion){
			var conf = js.configuracion[i];
			if(conf.activo===1){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#sp" + conf.objeto).find("img").attr("src", checkI); }else{ ' + convert(nvarchar(max),NCHAR(36)) + N'("#sp" + conf.objeto).find("img").attr("src", checkO); }
		}
		// Básculas
		for(var i in js.basculas){
			var basc = js.basculas[i];
			if(basc.activa){ botonBascula("img" + capitalizar(basc.bascula),1); }
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIP"+basc.bascula.split("bascula")[1]).val(basc.ip);
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPPuerto"+basc.bascula.split("bascula")[1]).val(basc.puerto);
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaIPSentencia"+basc.bascula.split("bascula")[1]).val(basc.sentencia);
		}
		cerrarVelo();
	} else { alert(''Error SP pBasculas - edicion!''+JSON.stringify(ret)); }}, false);
}

function cargarIdiomas(){
	var contenido = "";
	var parametros = ''{"sp":"pIdiomas","modo":"cargarIdiomas"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppIdiomas .lin").remove();
		var js = JSON.parse(limpiarCadena(ret.JSCode));
		if(js.length>0){
			for(var i in js){
				contenido += ''<div class="row lin chv"><img src="./Merlos/images/cerrarRojo.png" onclick="eliminarIdiomaReport(\''''+js[i].idioma+''\'',\''''+js[i].tipo+''\'',\''''+js[i].report+''\'')" /></div>''
							+''<div class="row lin">''+js[i].idioma+'' - ''+js[i].nombre+''</div>''
							+''<div class="row lin">''+js[i].tipo+''</div>''
							+''<div class="row lin">''+js[i].report+''</div>'';
			}
			' + convert(nvarchar(max),NCHAR(36)) + N'("#dvAppIdiomasContent").append(contenido);
		}
	} else { alert(''Error SP pBasculas - edicion!''+JSON.stringify(ret)); }}, false);
}

function idiomaNew(cerrar){	
	if(cerrar){
		' + convert(nvarchar(max),NCHAR(36)) + N'(".new").slideUp();
		' + convert(nvarchar(max),NCHAR(36)) + N'("#imgIdiomaNew").slideDown();
	}else{
		' + convert(nvarchar(max),NCHAR(36)) + N'("#imgIdiomaNew").hide();
		' + convert(nvarchar(max),NCHAR(36)) + N'(".new").slideDown();	
		' + convert(nvarchar(max),NCHAR(36)) + N'(".chv").slideDown().css("display","flex").css("justify-content","center").css("align-self","center");
	}
}

function cargarObjeto(elId,objeto,contenedor,dvAPP){
	' + convert(nvarchar(max),NCHAR(36)) + N'("#"+elId).val("cargando...").css("color","red");
	var contenido = "";
	var parametros = ''{"sp":"pObjetoDatos","objeto":"''+objeto+''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		var js = JSON.parse(limpiarCadena(ret.JSCode));
		if(js.length>0){
			for(var i in js){ contenido += ''<div class="dvSelSub" onclick="objetoSeleccion(\''''+elId+''\'',\''''+js[i].Codigo+'' - ''+js[i].Nombre+''\'')">''+js[i].Codigo+'' - ''+js[i].Nombre+''</div>''; }
			' + convert(nvarchar(max),NCHAR(36)) + N'("#"+dvAPP).css("overflow","visible");
			' + convert(nvarchar(max),NCHAR(36)) + N'("#"+contenedor).html(contenido).slideDown();			
		}
		' + convert(nvarchar(max),NCHAR(36)) + N'("#"+elId).val("").css("color","#333");		
	} else { alert(''Error SP pObjetoDatos - objeto: ''+objeto+''!\n''+JSON.stringify(ret)); }}, false);
}

function objetoSeleccion(id,valor){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).val(valor); ' + convert(nvarchar(max),NCHAR(36)) + N'(document).click(); }

function idiomaNewGuardar(){
	if(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpIdioma").val()).indexOf(" - ")===-1){ abrirVelo("El formato debe ser xx - xxxxxxxxxx !"+btnAceptarCerrarVelo); return; } 
	if(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpTipo").val()).indexOf(" - ")===-1){ abrirVelo("El formato debe ser xx - xxxxxxxxxx !"+btnAceptarCerrarVelo); return; } 
	var idioma = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim((' + convert(nvarchar(max),NCHAR(36)) + N'("#inpIdioma").val()).split(" - ")[0]);
	var nombre = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim((' + convert(nvarchar(max),NCHAR(36)) + N'("#inpIdioma").val()).split(" - ")[1]);
	var tipo = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim((' + convert(nvarchar(max),NCHAR(36)) + N'("#inpTipo").val()).split(" - ")[0]);
	var tipoNombre = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim((' + convert(nvarchar(max),NCHAR(36)) + N'("#inpTipo").val()).split(" - ")[1]);
	var report = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpReport").val());

	if(nombre===""){nombre=idioma;}

	if(idioma==="" || tipo==="" || report===""){ abrirVelo("Debes seleccionar todos los campos!"+btnAceptarCerrarVelo); return; }

	var parametros = ''{"sp":"pIdiomas","modo":"actualizar","idioma":"''+idioma+''","nombre":"''+nombre+''","tipo":"''+tipo+''","tipoNombre":"''+tipoNombre+''","report":"''+report+''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		cargarIdiomas();	
		' + convert(nvarchar(max),NCHAR(36)) + N'("#inpIdioma, #inpTipo, #inpReport").val("");
		idiomaNew(1);
	} else { alert(''Error SP pIdiomas - actualizar!\n''+JSON.stringify(ret)); }}, false);
}

function eliminarIdiomaReport(idioma,tipo,report,eliminar){
	if(eliminar){
		abrirVelo(icoCarga16+" eliminando el idioma...");
		var parametros = ''{"sp":"pIdiomas","modo":"eliminar","idioma":"''+idioma+''","tipo":"''+tipo+''","report":"''+report+''"}'';
		flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
			cargarIdiomas();	
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpIdioma, #inpTipo, #inpReport").val("");
			idiomaNew(1);
			cerrarVelo();
		} else { alert(''Error SP pIdiomas - actualizar!\n''+JSON.stringify(ret)); }}, false);
	}
	else{ 
		abrirVelo(
			 "Confirma para eliminar PERMANENTEMENTE el registro!"
			 +"<br><br><br>"
			 +"<span class=''btnMVerde'' onclick=''eliminarIdiomaReport(\""+idioma+"\",\""+tipo+"\",\""+report+"\",1)''>eliminar</span>"
			 +"&nbsp;&nbsp;&nbsp;"
			 +"<span class=''btnMRojo'' onclick=''cerrarVelo();''>cancelar</span>"
		); 
	}
}

' + convert(nvarchar(max),NCHAR(36)) + N'(".inpUsuario").on("blur", function(){ guardarUsuarioDatos(); });
function cargarUsuarios(){ 
	var contenido = "";
	' + convert(nvarchar(max),NCHAR(36)) + N'("#dvUsuariosListado").html(icoCarga16+" cargando usuarios...").slideDown();
	var usuario = JSON.parse(flexygo.context.Merlos_Usuarios);
	for(var i in usuario){
		contenido += "<div class=''dvSelSub'' onclick=''asignarUsuario(\""+usuario[i].UserName+"\")''>"+usuario[i].UserName+"</div>";
	}
	' + convert(nvarchar(max),NCHAR(36)) + N'("#dvUsuariosListado").html(contenido);
}

function asignarUsuario(usuario){
	' + convert(nvarchar(max),NCHAR(36)) + N'("#inpMerlosUsuario").val(usuario);
	' + convert(nvarchar(max),NCHAR(36)) + N'(document).click();
	' + convert(nvarchar(max),NCHAR(36)) + N'(".inpUsuario").val("");
	
	var parametros = ''{"sp":"pMerlosUsuarios","modo":"dameConfiguracion","usuario":"''+usuario+''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){ 
		var js = JSON.parse(limpiarCadena(ret.JSCode));
		if(js.length>0){
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpImpDocu").val(js[0].impDocu);
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpImpEti").val(js[0].impEti);
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpReportAlb").val(js[0].reportAlbaran);
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpReportFra").val(js[0].reportFactura);
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpReportEti").val(js[0].reportEtiquetas);
			' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaPredet").val(js[0].bascula);
		}
	} else { alert(''Error SP pMerlosUsuarios - dameConfiguracion!\n''+JSON.stringify(ret)); }}, false);
}

function guardarUsuarioDatos(){
	var usuario = ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpMerlosUsuario").val().trim(); 
	var ImpDocu = ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpImpDocu").val().trim(); 
	var ImpEti = ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpImpEti").val().trim(); 
	var ReportAlb = ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpReportAlb").val().trim(); 
	var ReportFra = ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpReportFra").val().trim(); 
	var ReportEti = ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpReportEti").val().trim(); 
	var BasculaPredet = ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBasculaPredet").val().trim(); 
	if(usuario==="" || (ImpDocu==="" && ImpEti===""&& ReportAlb===""&& ReportFra===""&& ReportEti===""&& BasculaPredet==="")){ return; }

	var parametros = ''{"sp":"pMerlosUsuarios","modo":"guardarDatos","usuario":"''+usuario+''","ImpDocu":"''+ImpDocu+''","ImpEti":"''+ImpEti+''","ReportAlb":"''+ReportAlb+''","ReportFra":"''+ReportFra+''"''
					+'',"ReportEti":"''+ReportEti+''","BasculaPredet":"''+BasculaPredet+''"}'';
	flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		
	} else { alert(''Error SP pMerlosUsuarios - guardarDatos!\n''+JSON.stringify(ret)); }}, false);
}
</script>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'noicon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,0,0,1)
 ,(N'Merlos_Impresion',N'flx-objectlist',N'project',N'Merlos_Impresions',NULL,N'Merlos_Impresion',N'Impresión',N'default',0,0,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'printer-2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,0,0,1)
 ,(N'Merlos_Trabajo',N'flx-html',N'project',NULL,NULL,N'Merlos_Trabajo',N'Merlos_Trabajo',N'none',1,1,1,0,NULL,NULL,N'<div id="dvTrabajoInputs" class="tbSeccion">
    <div>
        NÚMERO
        <br>
        <input type="text" id="inpNumero" class="tecNum sld">
    </div>
    <div>
        SERIE
        <br>
        <input type="text" id="inpSerie" class=''clsSerie'' onclick="' + convert(nvarchar(max),NCHAR(36)) + N'(this).select();" onkeyup="this.value=this.value.toUpperCase();">
        <div id="dvSeries" class="dvTrabajoInp"></div>
    </div>
    <div>
        ZONA
        <br>
        <input type="text" id="inpZona" class=''clsZona'' onkeyup="this.value=this.value.toUpperCase();">
        <div id="dvZonas" class="dvTrabajoInp"></div>
    </div>
    <div>
        RUTA
        <br>
        <input type="text" id="inpRuta" class=''clsRuta'' onclick="' + convert(nvarchar(max),NCHAR(36)) + N'(this).select();">
        <div id="dvRutas" class="dvTrabajoInp"></div>
    </div>
    <div>
        ARTÍCULO
        <br>
        <input type="text" id="inpArticulo" class=''clsArticulo sld'' onclick="' + convert(nvarchar(max),NCHAR(36)) + N'(this).select();">
    </div>
    <div>
        ENTREGA
        <br>
        <merlos-calendar id="inpEntrega" class="clsEntrega sld"></merlos-calendar>
    </div>
    <div class="btns taC" style="padding:0;"><div class="btnAzul esq05 dvBtn" onclick="flexygo.nav.execProcess(''GoHome'','''','''',null,null,''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this));">Limpiar</div></div>
    <div class="btns taC" style="padding:0;"><div class="btnVerde esq05 dvBtn" onclick="buscarGlobal()">buscar</div></div>
</div>

<div id="dvResultados" class="tbSeccion" style="margin-top:20px;"></div>
<div id="dvDatosDePedido" class="tbSeccion"></div>
<div id="dvLineasDePedido" class="tbSeccion"></div>
<div id="dvLineasDeArticulo" class="tbSeccion"></div>

<div id="dvCambiarArticulos" class="tbSeccion dv inv">
    <div class="dvTit">Cambiar Artículos
        <img src="./Merlos/images/cerrar.png" style="float:right; width:30px; margin-top:-5px; cursor:pointer;" onclick="cerrarCambiarArticulos()">
    </div>
    <div id="dvCambiarArticulosContenido" style="padding:10px;"></div>
</div> 

<div id="dvCambiarArticulosEq" class="tbSeccion dv inv">
    <div class="dvTit">Cambiar Artículos - Equivalencias
        <img src="./Merlos/images/cerrar.png" style="float:right; width:30px; margin-top:-5px; cursor:pointer;" onclick="cerrarCambiarArticulosEq()">
    </div>
    <div id="dvCambiarArticulosEqContenido" style="padding:10px;"></div>
</div> 


<script>
    // Comprobar Ejercicio en Curso ----------------------------------------------------------------------------------------------------------------------------
    var parametros = ''{"sp":"pConfiguracion","modo":"ComprobarEjercicio"}'';
    flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
        if(ret.JSCode!=="OK"){ console.log("Se ha actualizado el portal al ejercicio "+ret.JSCode); }
    }else{ alert(''Error S.P. pConfiguracion - ComprobarEjercicio!!!\n''+JSON.stringify(ret)); } }, false);
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------

    ' + convert(nvarchar(max),NCHAR(36)) + N'("#mainNav, .tbSeccion").hide();
    ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpEntrega-input").val(fechaDeManyana());

    var SeriesCargadas = false;
    var RutasCargadas = false;
    var ZonasCargadas = false;
    var btnMerlosTraspasoIO = false;
    var facturaDirecta = "";
    var FacturaDirecta = 0;
    var cambiarArticuloListado = [];
    var modificarPedidoListado = [];
    var listadoArticulos = "";
    var contadorTraspasoEnUso = 0;
    var modificandoPedido = false;
    var cambiandoArticulo = false;

    var spnOrden_numero = 0;

    if (IntegridadVerificada) { iniciar(); } else { ComprobarIntegridad(); }

    function ComprobarIntegridad() {
        abrirVelo(icoCarga50 + "<br><br>comprobando la integridad de la aplicación...");
        var parametros = ''{"sp":"pConfiguracion","modo":"comprobarConfiguracionEmpresa",'' + params + ''}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) { if (ret) {
            var js = JSON.parse(limpiarCadena(ret.JSCode));
            if(js[0].msj=="NoExisteConfiguracion"){ 
                if (currentRole === "Admins") { flexygo.nav.openPageName(''Merlos_Administrador'', '''', '''', '''', ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this)); }
                else {
                    abrirVelo(
                        icoAlert50
                        + "<h1 style=''font:24px arial; color:#900;''>No existe una configuración!</h1>"
                        + "<h2 style=''font:18px arial; color:#900;''>Póngase en contacto con su distribuidor.</h2>"
                    );
                }
            }
            else{  cerrarVelo(); iniciarTrabajo(); }
        } else { alert(''Error SP pConfiguracion - comprobarConfiguracionEmpresa!'' + JSON.stringify(ret)); } }, false);
    }

    function iniciarTrabajo() {
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTrabajoInputs").slideDown();
        cargarSeries();
        cargarRutas();
        cargarZonas();
        activarObjetos();
        cargarUsuarioFiltro();
    }


    ' + convert(nvarchar(max),NCHAR(36)) + N'(document).keypress(function (e) {
        if (e.keyCode === 13) {
            if (ControlActivo === "entradaPesoManual") { capturarPesoDeSuma(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#entradaPesoManual").val())); }
            else {
                if      (' + convert(nvarchar(max),NCHAR(36)) + N'("#nBultos").is(":focus")) { ' + convert(nvarchar(max),NCHAR(36)) + N'("nCajas").focus(); return; }
                else if (' + convert(nvarchar(max),NCHAR(36)) + N'("#nBultos").is(":focus")) { generarAlbaranDeVenta(); return; }
                else if (' + convert(nvarchar(max),NCHAR(36)) + N'("#nBultos, #nCajas, .inpPesoArt, .tecV, #inpNumero").is(":focus")) { tecladoNumerico("ENTER"); }
                else if (' + convert(nvarchar(max),NCHAR(36)) + N'(".tecNum, #inpArticulo").is(":focus")) { buscarGlobal(); }
                else if (' + convert(nvarchar(max),NCHAR(36)) + N'("#inpRuta").is(":focus")) { asignarRuta(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpRuta").val())); }
            }
        }else {
            if (' + convert(nvarchar(max),NCHAR(36)) + N'("#tecladoNumericoPantalla").is(":visible")) { e.preventDefault(); tecladoNumerico(String.fromCharCode(e.which)); }
        }
    });

    ' + convert(nvarchar(max),NCHAR(36)) + N'(".sld").on("click", function () { ' + convert(nvarchar(max),NCHAR(36)) + N'(".dvTrabajoInp").slideUp(); });


    function activarObjetos() {
        ControlActivo = ClaseActiva = "";

        ' + convert(nvarchar(max),NCHAR(36)) + N'("input").off().on("click", function () {
            ControlActivo = this.id;
            ClaseActiva = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("class").split(" ")[0];

            if (' + convert(nvarchar(max),NCHAR(36)) + N'("#" + ControlActivo).hasClass("noInput")) { return; }

            if (ControlActivo === "inpNumero") { tecladoModo = "Buscar"; }

            if       (ClaseActiva === "clsSerie") { mostrarSeries(); }
            else if  (ClaseActiva === "clsZona")  { mostrarZonas(); }
            else if  (ClaseActiva === "clsRuta")  { mostrarRutas(); }
            else if  (ClaseActiva === "tecNum")   { ControlActivo = this.id; ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + ControlActivo).val(""); tecladoNum(ControlActivo); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvRutas").hide(); }
            else if  (ClaseActiva === "tecAlf")   { tecladoAlf(ControlActivo); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvRutas").hide(); }
        });

        ' + convert(nvarchar(max),NCHAR(36)) + N'(".imgImp").off().on("click", function () {
            var imp = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("class").split(" ")[1];
            if (imp === "alb") {
                if (imprimirAlbaran === 0) {
                    imprimirAlbaran = 1;
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnImpAlb").attr("src", "./Merlos/images/btnImprimirAlbaranI.png");
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnGenerarAlbaran").html("GENERAR ALBARÁN");
                }
                else {
                    imprimirAlbaran = 0;
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnImpAlb").attr("src", "./Merlos/images/btnImprimirAlbaranO.png");
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnGenerarAlbaran").html("IMPRIMIR ETIQUETAS");
                }
            }
            if (imp === "eti") {
                if (imprimirEtiquetas === 0) { imprimirEtiquetas = 1; ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnImpEti").attr("src", "./Merlos/images/btnImprimirEtiquetasI.png"); }
                else { imprimirEtiquetas = 0; ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnImpEti").attr("src", "./Merlos/images/btnImprimirEtiquetasO.png"); }
            }

            if (imprimirAlbaran === 0 && imprimirEtiquetas === 0) { ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnGenerarAlbaran").hide(); } else { ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnGenerarAlbaran").show(); }
        });

        BotoneraEstado();
    }

    function BotoneraEstado(){
        // inicializar botones
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnObservaciones img").attr("src","./Merlos/images/btnObservaciones.png").addClass("btnAlo curP");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnPreparacion img").attr("src","./Merlos/images/btnPrep0_O.png").removeClass("btnAlo curP"); 
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnRecargar img").attr("src","./Merlos/images/btnRecargarC.png").addClass("btnAlo curP");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCambiarArt img").attr("src","./Merlos/images/btnCambiarArtC.png").addClass("btnAlo curP");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedido.png").addClass("btnAlo curP");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosAtras img").attr("src","./Merlos/images/btnMerlosAtrasC.png").addClass("btnAlo curP");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosPreparar img").attr("src","./Merlos/images/Preparar.png").addClass("btnAlo curP"); 
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosTraspaso img").attr("src","./Merlos/images/Traspasar.png").addClass("btnAlo curP");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnTraspasoCompleto img").attr("src","./Merlos/images/TraspasoCompleto.png").addClass("btnAlo curP");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCancelarPreparacion img").attr("src","./Merlos/images/CancelarPreparacion.png").addClass("btnAlo curP");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnFinalizarPedido img").attr("src","./Merlos/images/FinalizarPedido.png").addClass("btnAlo curP");

        // botones estados
        if( ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasDePedidoTraspaso").is(":visible") ){
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnPreparacion img").attr("src","./Merlos/images/btnPrep0_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosPreparar img").attr("src","./Merlos/images/Preparar_O.png").removeClass("btnAlo curP"); 
        }

        if( ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasDeArticulo").is(":visible") ){
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCambiarArt img").attr("src","./Merlos/images/btnCambiarArtC_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedido_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnPreparacion img").attr("src","./Merlos/images/btnPrep0.png").addClass("btnAlo curP");
        }

        if(!btnMerlosTraspasoIO){' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosTraspaso img").attr("src","./Merlos/images/Traspasar_O.png").removeClass("btnAlo curP"); }

        if(modificandoPedido){
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedidoVerde.png").addClass("btnAlo curP");
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbDescripcion").css("background","#DBFFD6").html("Modificación del Pedido");

            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosPreparar img").attr("src","./Merlos/images/Preparar_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnRecargar img").attr("src","./Merlos/images/btnRecargarC_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCambiarArt img").attr("src","./Merlos/images/btnCambiarArtC_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosTraspaso img").attr("src","./Merlos/images/Traspasar_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnTraspasoCompleto img").attr("src","./Merlos/images/TraspasoCompleto_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnPreparacion img").attr("src","./Merlos/images/btnPrep0_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosAtras img").attr("src","./Merlos/images/btnMerlosAtrasC_O.png").removeClass("btnAlo curP"); 
        }

        if(cambiandoArticulo){
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCambiarArt img").attr("src","./Merlos/images/CambiarArticuloVerde.png").addClass("btnAlo curP");
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbDescripcion").css("background","#DBFFD6").html("Cambiar Artículo con equivalente");

            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosPreparar img").attr("src","./Merlos/images/Preparar_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnRecargar img").attr("src","./Merlos/images/btnRecargarC_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedido_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosTraspaso img").attr("src","./Merlos/images/Traspasar_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnTraspasoCompleto img").attr("src","./Merlos/images/TraspasoCompleto_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnPreparacion img").attr("src","./Merlos/images/btnPrep0_O.png").removeClass("btnAlo curP"); 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosAtras img").attr("src","./Merlos/images/btnMerlosAtrasC_O.png").removeClass("btnAlo curP"); 
        }
    }

    function cargarUsuarioFiltro(){
        var parametros = ''{"sp":"pUsuario","modo":"dameFiltro",'' + params + ''}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) { if (ret) {
            var js = JSON.parse(limpiarCadena(ret.JSCode));
            if(js.length>0){ usuarioFiltro = js.filtro+";"+js.valor; }else{ usuarioFiltro=""; }
        } else { alert(''Error SP pUsuario - dameFiltro!'' + JSON.stringify(ret)); } }, false);
    }

    function tecladoNum(elControl) {
        cntrl = "teclado_" + elControl;
        abrirVeloTeclado("<div id=''dvTeclado''></div>", 1);
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTeclado").load("./Merlos/content/tecladoNum.html");
    }

    function tecladoNumerico(num) {
        if (num === "ENTER") {
            // entrada de peso manual
            if (ControlActivo === "entradaPesoManual") { capturarPesoDeSuma(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#entradaPesoManual").val())); }
            // cálculo de unidades/cajas SIN LOTE
            else if (ControlActivo === "sinLoteUDS") { cerrarVeloTeclado(); }
            else if (ControlActivo === "sinLoteCAJAS") { calcularUDSDesdeSinLoteCAJAS(); }
            // cálculo de unidades/cajas CON LOTE
            else if (ControlActivo.substring(0, 20) === "inp_LoteEntradaCajas") { calcularUdsDesdeConLoteCAJAS(ControlActivo); }
            else if (ControlActivo.substring(0, 18) === "inp_LoteEntradaUds") { calcularCajasDesdeConLoteUDS(ControlActivo); ventaCajas(ControlActivo); cerrarVeloTeclado(); }

            else if (ControlActivo === "inpNumero") { buscarGlobal(); cerrarVeloTeclado(); }
            else if (ControlActivo === "nBultos") { replicarBultos(); cerrarVeloTeclado(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#nBultos").focus(); }
            else { cerrarVeloTeclado(); }
        } else {
            var CntrlText = "";
            if (num === "<") { CntrlText = CntrlText.substr(0, CntrlText.length - 1); }
            else if (num === "<<") { CntrlText = ""; }
            else if (num === "") { ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTeclado").slideUp(); cerrarVeloTeclado(); }
            else { CntrlText = ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + ControlActivo).val() + num; }
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + ControlActivo).val(CntrlText);
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#tecladoNumericoPantalla").text(CntrlText);
        }
    }

    function cargarSeries() {
        if (!SeriesCargadas) {
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpSerie").val("cargando series...").css("color", "red");
            var parametros = ''{"sp":"pSeries"}'';
            flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {
                if (ret) {
                    var contenido = "";
                    var js = JSON.parse(limpiarCadena(ret.JSCode));
                    if (js.length > 0) {
                        for (var i = 0; i < js.length; i++) { contenido += "<div class=''dvSub'' onclick=''asignarSerie(\"" + js[i].codigo + " - " + js[i].nombre + "\")''>" + js[i].codigo + "-" + js[i].nombre + "</div>"; }
                    }
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvSeries").html(contenido);
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpSerie").val("").css("color", "#333333");
                } else { alert(''Error SP pSeries!'' + JSON.stringify(ret)); }
            }, false);
        }
    }

    function cargarZonas() {
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpZona").val("cargando zonas...").css("color", "red");
        var parametros = ''{"sp":"pZonas"}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {
            if (ret) {
                var contenido = "";
                var js = JSON.parse(limpiarCadena(ret.JSCode));
                if (js.length > 0) {
                    for (var i = 0; i < js.length; i++) { contenido += "<div class=''dvSub'' onclick=''asignarZona(\""+js[i].VALOR+"\")''>" + js[i].VALOR.toUpperCase() + "</div>"; }
                }
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvZonas").html(contenido);
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpZona").val("").css("color", "#333333");
            } else { alert(''Error SP pZonas!'' + JSON.stringify(ret)); }
        }, false);
    }

    function cargarRutas() {
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpRuta").val("cargando rutas...").css("color", "red");
        var parametros = ''{"sp":"pRutas"}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {
            if (ret) {
                var contenido = "";
                var js = JSON.parse(limpiarCadena(ret.JSCode));
                if (js.length > 0) {
                    for (var i = 0; i < js.length; i++) { contenido += "<div class=''dvSub'' onclick=''asignarRuta(\"" + js[i].codigo + "\",\"" + js[i].nombre + "\")''>" + js[i].codigo + "-" + js[i].nombre + "</div>"; }
                }
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvRutas").html(contenido);
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpRuta").val("").css("color", "#333333");
            } else { alert(''Error SP pRutas!'' + JSON.stringify(ret)); }
        }, false);
    }

    function mostrarSeries() {
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".teclado, .resultados").hide();
        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#dvSeries").is(":visible")) { ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvSeries").slideUp(); }
        else { ' + convert(nvarchar(max),NCHAR(36)) + N'(".dvTrabajoInp").hide(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvSeries").slideDown(); }
    }

    function mostrarZonas() {
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".teclado, .resultados").hide();
        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#dvZonas").is(":visible")) { ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvZonas").slideUp(); }
        else { ' + convert(nvarchar(max),NCHAR(36)) + N'(".dvTrabajoInp").hide(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvZonas").slideDown(); }
    }

    function mostrarRutas() {
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".tdRuta").removeClass("inv");
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".teclado, .resultados").hide();
        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#dvRutas").is(":visible")) { ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvRutas").slideUp(); }
        else {
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".dvTrabajoInp").hide(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvRutas").slideDown();
            if (Left(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpSerie").val(),2) === "ET") { 
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvRutas").find(".dvSub").each(function () {
                    if ((' + convert(nvarchar(max),NCHAR(36)) + N'(this).text()).substring(0, 1) !== "E" && (' + convert(nvarchar(max),NCHAR(36)) + N'(this).text()).substring(0, 1) !== "A") { ' + convert(nvarchar(max),NCHAR(36)) + N'(this).addClass("inv"); } else { ' + convert(nvarchar(max),NCHAR(36)) + N'(this).removeClass("inv"); }
                });
            }
        }
    }

    function asignarRuta(codigo, nombre) {
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpRuta").val(codigo + " - " + nombre);
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvRutas").hide();
        Ruta = trsps_ruta = codigo;
        buscarGlobal();
    }

    function asignarSerie(codigo, nombre) {
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpSerie").val(codigo + " - " + nombre);
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvSeries").hide();
        Serie = trsps_serie = codigo;
        buscarGlobal();
    }

    function asignarZona(Zona) {
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpZona").val(Zona);
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvZonas").hide();
        Zona = trsps_serie = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpZona").val()).toUpperCase();
        buscarGlobal();
    }

    function pedidoEnUso(numero, letra, ruta, n_ruta, usuario) {
        abrirVelo(
            "<div id=''veloAvDesbloqueo'' class=''taC'' style=''font:16px arial; color:#333;''>"
            + icoAlert50 + "<br><br>"
            + "El pedido " + numero + " (" + letra + ") está EN USO por el usuario " + usuario
            + "	<br><br><br><span class=''btnVerde esq05'' onclick=''desbloquearPedido(\"" + numero + "\",\"" + letra + "\",\"" + ruta + "\",\"" + n_ruta + "\",\"" + Almacen + "\");''>desbloquear</span>"
            + "	&nbsp;&nbsp;&nbsp;<span class=''btnRojo esq05'' onclick=''cerrarVelo();''>cancelar</span>"
            + "</div>"
        );
    }

    function desbloquearPedido(numero, letra, ruta, RutaNombre, Almacen) {
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#veloAvDesbloqueo").html(infoCarga("desbloqueando el pedido " + numero + " (" + letra + ") ..."));
        var parametros = ''{"sp":"pDesbloquearPedido","numero":"'' + numero + ''","letra":"'' + letra + ''","ruta":"'' + ruta + ''",'' + params + ''}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {
            if (ret) {
                cerrarVelo();
                verLineasDelPedido(numero, letra, ruta, RutaNombre, Almacen);
            } else { alert(''Error SP pSeries!'' + JSON.stringify(ret)); }
        }, false);
    }

    function buscarGlobal(campo) {
        if (campo === undefined || campo === "undefined") { campo = ""; }
        var bNumero = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpNumero").val());
        var bSerie = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpSerie").val()).split(" - ")[0];
        var bRuta = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpRuta").val()).split(" - ")[0];
        var bZona = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpZona").val());
        var bArticulo = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpArticulo").val());
        Entrega = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpEntrega-input").val());
        if (Entrega !== "" && Left(Entrega, 4).indexOf("-") === -1) { Entrega = Entrega.substr(8, 2) + "-" + Entrega.substr(5, 2) + "-" + Entrega.substr(0, 4); }
        Serie = bSerie;
        ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion, .teclado").hide();
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTrabajoInputs").show();
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvResultados").html(infoCarga("buscando pedidos")).slideDown();

        var parametros = ''{"sp":"pBuscarGlobal","pedidoActivo":"'' + PedidoActivo + ''","numero":"'' + bNumero + ''","serie":"'' + bSerie + ''","ruta":"'' + bRuta + ''"''
            + '',"zona":"'' + bZona + ''","articulo":"'' + bArticulo + ''","entrega":"'' + Entrega + ''","campo":"'' + campo + ''",'' + params + ''}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {
            if (ret) { 
                var js = JSON.parse(limpiarCadena(ret.JSCode));
                var contenido = "";
                if (js.length > 0) {
                    contenido = `<div style=''font:bold 14px arial; color:#333; background:#FFF;padding:6px;''>
                                    lista de Pedidos
                                    <span style=''float:right;''>
                                        <span style=''font:11px arial; color:#07657e; margin-right:20px;''>clic en la cabecera de la tabla para ordenar</span>
                                        <span class=''btnAmarillo'' onclick=''eliminarFiltros()''>eliminar filtros</span>
                                    </span>
                                </div>
                                <table id=''tbResultados'' class=''tbStd'' style=''border-collapse:Separate; border:1px solid #CCC;''>
                                <tr>
                                    <th style=''width:100px;'' onclick=''buscarGlobal("numero")'' style=''white-space: nowrap;''>NÚMERO <span id=''spnOrden_numero''></span></th>
                                    <th style=''width:60px;''  onclick=''buscarGlobal("letra")'' style=''white-space: nowrap;''>SERIE <span id=''spnOrden_letra''</th>
                                    <th style=''width:120px;'' onclick=''buscarGlobal("fecha")'' style=''white-space: nowrap;''>FECHA <span id=''spnOrden_fecha''</th>
                                    <th style=''width:120px;'' onclick=''buscarGlobal("entrega")'' style=''white-space: nowrap;''>ENTREGA <span id=''spnOrden_entrega''</th>
                                    <th onclick=''buscarGlobal("nombre")'' style=''white-space: nowrap;''>NOMBRE <span id=''spnOrden_nombre''</th>
                                    <th onclick=''buscarGlobal("rComercial")'' style=''white-space: nowrap;''>R. COMERCIAL <span id=''spnOrden_rComercial''</th>
                                </tr>`;
                    for (var i in js) {
                        var colorFE = " color:#333; ";
                        var colorT  = " color:#333; ";
                        facturaDirecta = ""; if (js[i].FACTDIRV) { facturaDirecta = "<span style=''font:bold 14px arial;color:orange;''>(Factura Directa)</span>"; }
                        if (parseFloat(js[i].servidas) > 0 || parseFloat(js[i].bNumero) > 0) { colorT = " color:#FF6100; "; /* naranja */ }
                        var fechaCorta = js[i].fecha.substr(8, 2) + "-" + js[i].fecha.substr(5, 2) + "-" + js[i].fecha.substr(0, 4);
                        var entregaCorta = js[i].entrega.substr(8, 2) + "-" + js[i].entrega.substr(5, 2) + "-" + js[i].entrega.substr(0, 4);
                        var esteOnClick = "onclick=''FacturaDirecta=\"" + js[i].FACTDIRV + "\"; verLineasDelPedido(\"" + js[i].numero + "\",\"" + js[i].letra + "\",\"" + js[i].ruta + "\",\"" + js[i].n_ruta + "\",\"" + js[i].almacen + "\")''";
                        if (parseInt(fechaNum(js[i].fecha)) >= parseInt(fechaNum(fechaMasDias(2,"amd")))) { colorFE = "color:red;"; }
                        // comprobamos si está EN_USO
                        if (js[i].TIPO != null) {
                            if (' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].CLAVE) == ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].EN_USO) && ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].USUARIO) !== ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(currentReference)) {
                                var colorT = " color:#900; ";
                                var esteOnClick = "onclick=''pedidoEnUso(\"" + js[i].numero + "\",\"" + js[i].letra + "\",\"" + js[i].ruta + "\",\"" + js[i].n_ruta + "\",\"" + ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].USUARIO) + "\")''";
                            }
                        }
                        contenido += "<tr " + esteOnClick + ">"
                            + "     <td style=''" + colorT + "''>" + js[i].numero + "</td>"
                            + "     <td style=''" + colorT + "''>" + js[i].letra + "</td>"
                            + "     <td style=''" + colorT + "''>" + fechaCorta + "</td>"
                            + "     <td style=''" + colorFE + "''>" + entregaCorta + "</td>"
                            + "     <td style=''" + colorT + "''>" + js[i].nombre + " " + facturaDirecta + "</td>"
                            + "     <td style=''" + colorT + "''>" + js[i].rComercial + "</td>"
                            + " </tr>";
                    }
                    contenido += "</table></div>";
                } else { contenido = "Sin resultados!"; }
                ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion, .teclado").hide();
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTrabajoInputs").show();
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvResultados").html(contenido).slideDown();
                // pintar Filtros
                if (js.length > 0) {
                    var jsf = js[0].filtros;
                    for(var i in jsf) {  ' + convert(nvarchar(max),NCHAR(36)) + N'("#spnOrden_"+jsf[i].campo).html(window["spOrden"+jsf[i].orden]);  }
                }
            } else { alert(''Error SP pBuscarGlobal!'' + JSON.stringify(ret)); }
        }, false);
    }

    function eliminarFiltros(){
        var parametros = ''{"sp":"pUsuario","modo":"eliminarFiltros",'' + params + ''}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {if (ret) { 
                buscarGlobal();
        } else { alert(''Error SP pUsuario - eliminarFiltros!\n'' + JSON.stringify(ret)); } }, false);
    }

    function verLineasDelPedido(pedido, serie, ruta, nombreRuta, almacen) {
        var laRuta = ruta.split(" - ")[0];
        Numero = pedido;
        NumPedido = pedido;
        PedidoSerie = serie;
        Serie = serie;
        Ruta = laRuta;
        RutaNombre = nombreRuta;
        Almacen = almacen;
        modificacionesCancelar(1);
        cambiarArticuloListado=[];
        buscarPedido(pedido, serie, Ruta, nombreRuta);
    }

    function buscarPedido(pedido, serie, ruta, nombreRuta) {
        gPedido = trsps_numero = pedido;
        Serie = trsps_serie = serie;
        trsps_ruta = ruta;
        trsps_nRuta = nombreRuta;
        btnMerlosTraspasoIO=false;
        var zona = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpZona").val());        
        var parametros = ''{"sp":"pBuscarPedido","pedidoActivo":"'' + PedidoActivo + ''","pedido":"'' + pedido + ''","serie":"'' + serie + ''","ruta":"'' + ruta + ''","nombreRuta":"'' + nombreRuta + ''","zona":"'' + zona + ''",'' + params + ''}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {
            if (ret) { 
                if ((ret.JSCode).split("!")[0] === "enUSO") { pedidoEnUso(pedido,serie,ruta,nombreRuta,(ret.JSCode).split("!")[1]); return; }
                ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion, .teclado").hide();
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasDePedido").html(infoCarga("cargando el pedido " + pedido + "...")).slideDown();
                var js = JSON.parse(limpiarCadena(ret.JSCode));
                if (js.length > 0) {
                    PedidoActivo = js[0].PedidoActivo;
                    var cliObs = js[0].ClienteObs.replace(/"/g, ''-'');
                    var pedidoObs = js[0].PedidoObs.replace(/"/g, ''-'');
                    var rutaTxt = "<span style=''margin-right:30px;font:bold 20px arial;color:#07657e;''>RUTA: " + ruta + " - " + nombreRuta + "</span>"; /* azul oscuro */
                    if (ruta.trim() === "" || ruta.trim() === undefined || ruta.trim() === "null") { rutaTxt = ""; }

                    var laTabla = "<table id=''tbDatosDePedido'' style=''width:100%;border-collapse:collapse; border-bottom:1px solid #07657e;''>"
                        + "	    <tr>"
                        + "     	<td style=''vertical-align:top;''>"
                        + "         	<span style=''font:bold 20px arial; color:#07657e; ''>Traspaso del Pedido " + pedido + "-" + serie + "</span><br>" 
                        + "         	<span id=''spanDatosCliente'' style=''margin-right:30px;''></span>"
                        + "     	</td>"
                        + "     	<td style=''vertical-align:top; padding-left:50px;''>" + rutaTxt + "</td>"
                        + "     	<td style=''vertical-align:top; width:450px;''>"
                        + "				<div class=''dvBotoneraOpciones''>"
                        + "				    <div class=''btnObservaciones''><img src=''./Merlos/images/btnObservaciones.png'' style=''width:100%'' class=''btnAlo curP'' onclick=''mostrarObservaciones(\"" + cliObs + "\",\"" + pedidoObs + "\")''></div>"
                        + "				    <div class=''btnPreparacion''><img src=''./Merlos/images/btnPrep0.png'' style=''width:100%'''' class=''btnAlo curP'' onclick=''borrarPropuesta()''></div>"
                        + "				    <div class=''btnRecargar''><img src=''./Merlos/images/btnRecargarC.png'' style=''width:100%'' class=''btnAlo curP'' onclick=''recargar()''></div>"
                        + "				    <div class=''btnCambiarArt''><img src=''./Merlos/images/btnCambiarArtC.png'' style=''width:100%'' class=''btnAlo curP'' onclick=''cambiarArticulos(\"" + pedido + "\",\"" + serie + "\",\"" + ruta + "\")''></div>"
                        + "				    <div class=''btnModificarPedido''><img src=''./Merlos/images/ModificarPedido.png'' style=''width:100%'' class=''btnAlo curP'' onclick=''modificarPedido()''></div>"
                        + "				    <div class=''btnMerlosAtras''><img src=''./Merlos/images/btnMerlosAtrasC.png'' style=''width:100%;'' class=''btnAlo curP'' onclick=''traspasarAtras()''></div>"

                        + "				    <div class=''btnMerlosPreparar''><img src=''./Merlos/images/Preparar.png'' style=''width:100%;'' class=''btnAlo curP'' onclick=''traspasarArtLote()''></div>"
                        + "				    <div class=''btnMerlosTraspaso''><img src=''./Merlos/images/Traspasar.png'' style=''width:100%;'' class=''btnAlo curP'' onclick=''traspasarPedido()''></div>"
                        + "				    <div class=''btnTraspasoCompleto''><img src=''./Merlos/images/TraspasoCompleto.png'' style=''width:100%;'' class=''btnAlo curP'' onclick=''traspasoCompleto()''></div>"
                        + "				    <div class=''btnCancelarPreparacion''><img src=''./Merlos/images/CancelarPreparacion.png'' style=''width:100%;'' class=''btnAlo curP'' onclick=''cancelarPreparacion()''></div>"
                        + "				    <div class=''btnFinalizarPedido''><img src=''./Merlos/images/FinalizarPedido.png'' style=''width:100%;'' class=''btnAlo curP'' onclick=''finalizarPedido()''></div>"
                        + "				</div>"
                        + "				<div style=''height:4px;''></div>"
                        + "     	</td>"
                        + "		</tr>"
                        + "</table>"
                        + "<div id=''dvModificarPedido'' class=''inv''></div>"
                        + "<div id=''dvLineasDePedidoTraspaso'' style=''margin-top:10px;''>"
                        + "		<div id=''dvLineasDePedidoTraspasoCB''>"
                        + "			<input type=''text'' id=''inpLineasDePedidoTraspasoCB'' class=''cb'' "
                        + "			style=''background:#FFF; width:100%; padding:4px; border:none; outline:none; border-bottom:1px solid #07657e; text-align:center; font:14px arial; color:#666;'' "
                        + "			placeholder=''código de barras'' onkeyup=''cbKeyUp()'' "
                        + "			onclick=''' + convert(nvarchar(max),NCHAR(36)) + N'(this).select()''>"
                        + "		</div>"
                        + "		<table id=''tbLineasDePedidoTraspaso'' class=''tbStd'' style=''margin-top:4px;''>"
                        + "  		<tr id=''tbLineasDePedidoTraspasoCabecera00'' style=''border-bottom:1px solid #FFF;''>"
                        + "      		<th id=''tbDescripcion'' colspan=''4'' style=''text-align:center; font:bold 16px arial; color:green;''></th>"
                        + "      		<th style=''text-align:center;'' colspan=''3'' class=''bgAzulPalido''>PENDIENTE</th>"
                        + "      		<th style=''text-align:center;'' colspan=''3'' class=''bgVerdePalido''>TRASPASADO</th>"
                        + "  		</tr>"
                        + "  		<tr id=''tbLineasDePedidoTraspasoCabecera''>"
                        + "      		<th style=''''></th>"
                        + "      		<th style=''''>ARTICULO</th>"
                        + "      		<th style=''''>UBICACIÓN</th>"
                        + "      		<th style=''''>NOMBRE</th>"
                        + "      		<th style=''text-align:center;'' class=''bgAzulPalido''>CAJAS</th>"
                        + "      		<th style=''text-align:center;'' class=''bgAzulPalido''>UDS.</th>"
                        + "      		<th style=''text-align:center;'' class=''bgAzulPalido''>PESO</th>"
                        + "      		<th style=''text-align:center;'' class=''bgVerdePalido''>CAJAS</th>"
                        + "      		<th style=''text-align:center;'' class=''bgVerdePalido''>UDS.</th>"
                        + "      		<th style=''text-align:center;'' class=''bgVerdePalido''>PESO</th>"
                        + "  		</tr>";

                    var trColor = "#daf7ff"; 
                    ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbLineasDePedidoTraspasoTR").remove();
                    ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion, .teclado").hide();
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvDatosDePedido").html(laTabla).slideDown();

                    for (var i in js) {
                        if (trColor == "#daf7ff") { trColor = ""; } else { trColor = "#daf7ff"; } /* azul claro */
                        var ubicacion = js[i].ubicacion; if (ubicacion === "undefined" || ubicacion===null) { ubicacion = ""; }
                        var elID = (js[i].empresa + js[i].numero + js[i].linia + js[i].letra).replace(/\s+/g, "_");

                        var cajasPendientes = parseInt(js[i].cajas_pendientes);             if (cajasPendientes < 0) { cajasPendientes = 0; }
                        var udsPendientes = parseInt(js[i].uds_pendiente);                  if (udsPendientes   < 0) { udsPendientes = 0; }
                        var pesoPendiente = parseFloat(js[i].peso_pendiente).toFixed(3);    if (pesoPendiente   < 0) { pesoPendiente = 0.000; }
                        var cajasTraspasado = parseInt(js[i].cajas_traspasadas);
                        var udsTraspasado = parseInt(js[i].uds_traspasadas);
                        var pesoTraspasado = parseFloat(js[i].peso_traspasado).toFixed(3);

                        if (cajasPendientes == 0) { cajasPendientes = ""; }
                        if (udsPendientes == 0) { udsPendientes = ""; }
                        if (pesoPendiente == 0) { pesoPendiente = ""; }
                        if (cajasTraspasado == 0) { cajasTraspasado = ""; }
                        if (udsTraspasado == 0) { udsTraspasado = ""; }
                        if (pesoTraspasado == 0) { pesoTraspasado = ""; }

                        var stockArt = js[i].stock; if(isNaN(stockArt)){ stockArt=0; }

                        var laImgTraspasoC = "style=''width:30px; background:url(./Merlos/images/btnTC.png) no-repeat center; background-size:90% '' "
                                            +" onclick=''traspasarLineaCompleta(\"tbLinPedTraspTR_" + i + "\"); event.stopPropagation();''";
                        var fColor = "#333;";
                        var dataTraspasar = ` data-traspasar="true" `;
                        var elOnClick = ''onclick=\''editarLineaTraspaso(\"'' + elID + ''\",\"'' + js[i].numero + ''\",\"'' + js[i].articulo + ''\",\"'' + js[i].definicion.replace(/"/g, '''') + ''\",\"'' + js[i].uds_pv + ''\",\"'' + js[i].peso_pv + ''\",\"'' + js[i].servidas + ''\",\"'' + js[i].linia + ''\",\"'' + js[i].cajas_pendientes + ''\",\"'' + js[i].uds_pendiente + ''\",\"'' + js[i].peso_pendiente + ''\")\'''';
                        var elOnClickObs="";
                        if (parseInt(js[i].uds_traspasadas) == 0 && parseInt(stockArt) < parseInt(js[i].uds_pv) && js[i].cntrlSotck == false) { fColor = "orange;"; }
                        if (parseInt(stockArt) <= 0 && js[i].cntrlSotck == false) { elOnClick = ""; fColor = "red;"; laImgTraspasoC = ""; dataTraspasar = ` data-traspasar="false" `;}
                        if (parseInt(js[i].dto) == 100) { trColor = "yellow;"; }

                        var tdID = "";   
                        if (' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].TipoIVA)=="" && js[i].preparado=="0" ) {
                            tdID = "tdObserva_".elID;
                            fColor = "#C15D48;"; /* marrón */
                            trColor = "rgb(246,246,246);"; /* blanco roto */
                            cajasPendientes = udsPendientes = pesoPendiente = cajasTraspasado = udsTraspasado = pesoTraspasado = "";
                            elOnClickObs = ''onclick=\''traspasarObservacion(\"'' + tdID + ''\",\"'' + js[i].empresa + ''\",\"'' + js[i].numero + ''\",\"'' + js[i].linia + ''\",\"'' + js[i].letra + ''\"); event.stopPropagation();\'''';
                            laImgTraspasoC = "";
                        } 
                        if (' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].TipoIVA)!=="" && js[i].preparado == "1") {
                            tdID = "tdObserva_" + elID;
                            fColor = "#009907;"; /* verde oscuro */ laImgTraspasoC = "";
                            trColor = "#d6ffde;";   /* verde claro */
                            elOnClickObs = ''onclick=\''traspasarObservacion(\"'' + tdID + ''\",\"'' + js[i].empresa + ''\",\"'' + js[i].numero + ''\",\"'' + js[i].linia + ''\",\"'' + js[i].letra + ''\"); event.stopPropagation();\'''';
                        }

                        var elCodigo = js[i].cliente;
                        var laUbicacion = js[i].ubicacion; if (laUbicacion === "") { laUbicacion = " "; }
                        var rCom = js[i].rComercial;
                        var elNombre = js[i].n_cliente;
                        var datosCli = js[i].direccion + "<br>" + js[i].copdpost + " " + js[i].poblacion + "<br>" + js[i].provincia + "";
                        var laRuta = "RUTA: " + js[i].ruta + " - " + js[i].n_ruta;
                        var elVendedor = "Vendedor: " + js[i].vendedor + " - " + js[i].n_vendedor + "<br>Telf: " + js[i].Movil_vendedor;

                        var cajasVis = ""; var pesoVis = "";
                        if (parseInt(js[i].cajas_pv) == 0) { cajasVis = "inv"; }
                        if (parseFloat(js[i].peso_pv) < 0.001) { pesoVis = "inv"; }

                        var pedidoArticulo = js[i].articulo;
                        var pedidoDefinicion = js[i].definicion;

                        // Pintar artículos cambiados si existen
                        var articuloCambiado = "";
                        for(var l in cambiarArticuloListado){
                            if(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(cambiarArticuloListado[l].articulo)===' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].articulo)){
                                articuloCambiado = `<br><span style="font:12px arial;color:#FF5733;">ORIGINAL: `+cambiarArticuloListado[l].articuloOriginal+` - `+cambiarArticuloListado[l].definicionOriginal+`</span>`;
                            }
                        }

                        ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasDePedidoTraspaso").append(
                            "<tr id=''tbLinPedTraspTR_" + i + "'' class=''tbLineasDePedidoTraspasoTR'' style=''background:" + trColor + "'' " + elOnClick + " " +dataTraspasar+" "
                            +"  data-linea=''" + js[i].linia + "'' data-articulo=''"+pedidoArticulo+"'' data-cajas=''"+cajasPendientes+"'' data-unidades=''"+udsPendientes+"'' data-peso=''"+pesoPendiente+"''>"
                            + "          <td  id=''tbLinPedTraspTRTC_" + i + "'' class=''tbAccion'' " + laImgTraspasoC
                            + "			</td>"
                            + "          <td class=''tbLineasDePedidoTraspasoTDlineaPte inv'' style=''color:" + fColor + "''>" + js[i].linia + "</td>"
                            + "          <td class=''tbLineasDePedidoTraspasoTDarticuloPte'' style=''color:" + fColor + "''>" + pedidoArticulo + "</td>"
                            + "          <td style=''color:" + fColor + "''>" + ubicacion + "</td>"
                            + "          <td  id=''" + tdID + "'' class=''tdDefinicion'' style=''color:" + fColor + "'' "+elOnClickObs+">" + pedidoDefinicion + articuloCambiado + "</td>"
                            + "          <td class=''tbLineasDePedidoTraspasoTDcajasPte'' style=''text-align:right;color:" + fColor + "''><span class=''" + cajasVis + "''>" + cajasPendientes + "</span></td>"
                            + "          <td class=''tbLineasDePedidoTraspasoTDunidadesPte'' style=''text-align:right;color:" + fColor + "''>" + udsPendientes + "</td>"
                            + "          <td class=''tbLineasDePedidoTraspasoTDpesoPte'' style=''text-align:right;color:" + fColor + "''><span class=''" + pesoVis + "''>" + pesoPendiente + "</span></td>"
                            + "          <td style=''text-align:right;color:" + fColor + " border-left-color:#07657e;''><span class=''" + cajasVis + " ''>" + cajasTraspasado + "</span></td>"
                            + "          <td style=''text-align:right;color:" + fColor + "''>" + udsTraspasado + "</td>"
                            + "          <td style=''text-align:right;color:" + fColor + "''><span class=''" + pesoVis + "''>" + pesoTraspasado + "</span></td>"
                            + "</tr>"
                        )                            
                        
                        if(cajasTraspasado!=="" || udsTraspasado!=="" || pesoTraspasado!==""){btnMerlosTraspasoIO=true;} // Control del botón btnMerlosTraspaso
                    }

                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasDePedidoTraspaso").append("</table></div><div id=''dvLineasDePedidoTraspasoContenido''></div>");

                    activarObjetos();
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpLineasDePedidoTraspasoCB").focus();
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnTraspasoDef").show(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnTraspasoDef").off("click").on("click", function () { traspasarPedido(); });
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnMerlosHome").show(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnCambiarArtC,#btnRecargarC").show();
                    if (' + convert(nvarchar(max),NCHAR(36)) + N'.trim(rCom) !== "") { rCom = "<br><span style=\"font:bold 14px arial; color:#666;\">" + rCom + "</span>"; }
                    ' + convert(nvarchar(max),NCHAR(36)) + N'(''#spanDatosCliente'').html(
                        "		    <span>" + elNombre + " (<span id=\"spCodCli\">" + elCodigo + "</span>)</span>"
                        + rCom
                        + "		    <br />"
                        + "		    <span style=\"font:14px arial; color:#333;\">" + datosCli + "</span>"
                    );
                    ' + convert(nvarchar(max),NCHAR(36)) + N'(''#spBPruta'').html(laRuta);
                    ' + convert(nvarchar(max),NCHAR(36)) + N'(''#spBPvendedor'').html(elVendedor);
                    ' + convert(nvarchar(max),NCHAR(36)) + N'(''#spCliObs'').html(cliObs);
                    ' + convert(nvarchar(max),NCHAR(36)) + N'(''#spPedidoObs'').html(pedidoObs);
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasDePedido").stop().hide();
                    if(cliObs!=="" || pedidoObs!==""){ ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnObservaciones img").attr("src", "./Merlos/images/btnObservaciones.png").addClass("btnAlo curP");  }
                    else{ ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnObservaciones img").attr("src", "./Merlos/images/btnObservaciones_O.png").removeClass("btnAlo curP"); }
                } else { ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasDePedido").html("<div class=''avRojo''>No se han obtenido resultados!</div>"); }
                BotoneraEstado();
            } else { alert(''Error SP pBuscarPedido!'' + JSON.stringify(ret)); }            
        }, false);
    }

    function mostrarObservaciones(cliObs, pedidoObs) {
        if(!' + convert(nvarchar(max),NCHAR(36)) + N'(".btnObservaciones img").hasClass("btnAlo")){ return; }
        ' + convert(nvarchar(max),NCHAR(36)) + N'("body").prepend("<div id=''dvVeloObs'' class=''dvVelo''>"
            + "      <div class=''dvCliObs vaT taL'' style=''max-width:50%; margin:5% auto; background:#f2f2f2;box-shadow: 2px 4px 10px 1px rgba(0,0,0,.3);''>"
            + "             <img src=''./Merlos/images/cerrar.png'' class=''curP'' style=''width:20px; float:right;'' onclick=''cerrarObservaciones()''>"
            + "             <br><span style=''font:bold 12px arial; color:#19456B;''>Observaciones del cliente:</span>"
            + "             <br><span id=''spCliObs''>" + cliObs + "</span>"
            + "             <br><br><span style=''font:bold 12px arial; color:#19456B;''>Observaciones del pedido:</span>"
            + "             <br><span id=''spPedidoObs''>" + pedidoObs + "</span>"
            + "             <br><br>"
            + "     </div>"
            + "</div>");
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvVeloObs").fadeIn();
    }

    function cerrarObservaciones() { ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvVeloObs").fadeOut(300, function () { ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvVeloObs").remove(); }); }

    function recargar(forzar) { 
        if((!forzar || forzar===undefined) && !' + convert(nvarchar(max),NCHAR(36)) + N'(".btnRecargar img").hasClass("btnAlo")){ return; }
        buscarPedido(trsps_numero, trsps_serie, trsps_ruta, trsps_nRuta); 
    }

    function cbKeyUp() {
        var valCB = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#inpLineasDePedidoTraspasoCB").val());
        if (valCB === "") { return; }
        if (event.keyCode === 13) { comprobarCB(valCB); }
    }

    function comprobarCB(cb) {
        if (cb === "") { return; }
        var parametros = ''{"sp":"pComprobarCB","CODBAR":"''+cb+''"}'';
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
            var js = JSON.parse(limpiarCadena(ret.JSCode));
            if (js.Error === "") {
                artEncontrado = ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbLineasDePedidoTraspasoTDarticuloPte:contains(''" + js.ARTICULO + "'')").text();
                if (artEncontrado !== "") {
                    // el artículo pertenece al pedido y lo tratamos
                    linea = ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbLineasDePedidoTraspasoTDarticuloPte:contains(''" + js.ARTICULO + "'')").prev().text();
                    // si trabaja con lotes
                    if (js.LOTE !== "") { 
                        // accedemos a la pantalla de lotes
                        ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbLineasDePedidoTraspasoTDarticuloPte:contains(''" + js.ARTICULO + "'')").parent().click();                        
                        setTimeout(()=>{ /* espera para pintar la tabla */
                            // nos situamos en la linea del lote y verificamos stock y preparación
                            var loteLinTR = ' + convert(nvarchar(max),NCHAR(36)) + N'(".linLote:contains(''"+js.LOTE+"'')").parent().attr("id");
                            var linStock = ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+loteLinTR).find(".inpStock").val();
                            var linUds = ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+loteLinTR).find(".inpUnidades").val(); 
                            if(parseFloat(js.UNIDADES)>parseFloat(linStock)){ abrirVelo("No hay suficiente stock del lote: "+js.LOTE+"!<br><br><br><span class=''btnRojo esq05'' onclick=''cerrarVelo();''>aceptar</span>"); traspasarAtras(); return; }
                            if(parseFloat(linUds)>parseFloat(js.UNIDADES)){ abrirVelo("Las unidades pedidas superan a las unidades del lote!<br><br><br><span class=''btnRojo esq05'' onclick=''cerrarVelo();''>aceptar</span>"); traspasarAtras(); return; }
                            // Traspasamos la linea
                            traspasarArtLote();
                        },500);                        
                    }
                    else { traspasoDesdeCB(cb, js.ARTICULO, js.UNIDADES, js.UNICAJA, js.VENTA_CAJA, linea); }
                } else { alert("El artículo NO pertenece a este pedido!"); }
            } else {
                // Errores y Avisos
                if (js.Error == "CodigoDividido") {
                    abrirVelo(
                        "<div class=''taC'' style=''font:16px arial; color:#333;''>"
                        + "Este código necesita un segundo bloque"
                        + "<br><br>"
                        + "<input type=''text'' id=''inpCB2'' "
                        + "style=''width:300px; padding:5px; text-align:center;'' class=''esq05'' "
                        + "onkeyup=''CBsegundoBloque(\"" + cb + "\")''>"
                        + "	<br><br><br><span class=''btnRojo esq05'' onclick=''cerrarVelo();''>cancelar</span>"
                        + "</div>"
                    );
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#inpCB2").focus();
                } else { alert(js.Error); }
            }
        } else { alert(''Error SP pComprobarCB!''+JSON.stringify(ret)); }}, false);
    }

    function traspasarAtras(forzar) {
        if(!forzar && !' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosAtras img").hasClass("btnAlo")){ return; }
        if(' + convert(nvarchar(max),NCHAR(36)) + N'("#tbModifPedido").is(":visible")){ recargar(1); }

        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasDePedidoTraspaso").is(":visible")) { 
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion").hide(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTrabajoInputs, #dvResultados").slideDown(); 
            buscarGlobal();
        }

        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasDeArticulo").is(":visible")) {
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion, .btnPrep0 img").hide();
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvDatosDePedido, #dvLineasDePedidoTraspaso").slideDown(); 
        }

        BotoneraEstado();
    }

    function traspasarObservacion(td,empresa,numero,linia,letra){ 
        var txtDefinicion = ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+td).text();
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+td).html("<span style=''color:red;''>"+icoCarga16+" guardando, espera...</span>");
        var parametros = ''{"sp":"pPedidos","modo":"traspasarObservacion","linia":"''+linia+''","empresa":"''+empresa+''","letra":"''+letra+''","numero":"''+numero+''"}'';
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
            var elColor="#C15D48;";
            if(ret.JSCode==="insertado"){ elColor="green;"; }
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+td).html("<span style=''color:"+elColor+"''>"+txtDefinicion+"</span>");
        } else { alert(''Error SP pPedidos - traspasarObservacion!''+JSON.stringify(ret)); }}, false);        
    }

    function traspasarLineaCompleta(tr) {
        abrirVelo(infoCarga("traspasando la linea...)"));

        var linea = ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + tr).attr("data-linea");
        var articulo = ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + tr).attr("data-articulo");
        var cajas = (' + convert(nvarchar(max),NCHAR(36)) + N'("#" + tr).attr("data-cajas")).replace(/,/g, "");
        var unidades = (' + convert(nvarchar(max),NCHAR(36)) + N'("#" + tr).attr("data-unidades")).replace(/,/g, "");
        var peso = (' + convert(nvarchar(max),NCHAR(36)) + N'("#" + tr).attr("data-peso")).replace(/,/g, "");

        var parametros = ''{"sp":"pTraspLinCompleta","almacen":"'' + Almacen + ''","articulo":"'' + articulo + ''","cajas":"'' + ifNull(cajas,0) + ''","unidades":"'' + ifNull(unidades,0) + ''","peso":"'' + ifNull(peso,0.000) + ''","linea":"'' + linea + ''","numero":"'' + Numero + ''","serie":"'' + Serie + ''"}'';
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{ ''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
                cerrarVelo();
                verLineasDelPedido(Numero, Serie, Ruta, RutaNombre, Almacen);
            } else { alert(''Error SP pTraspLinCompleta!'' + JSON.stringify(ret)); }
        }, false);
    }

    function cancelarPreparacion(confirmado) {
        if(!' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCancelarPreparacion img").hasClass("btnAlo")){ return; }
        if (!confirmado) {
            abrirVelo(
                "<div style=''text-align:center;''>"
                + "		Confirma para cancelar la preparación!"
                + "		<br><br><br>"
                + "		<span class=''btnMVerde esq05'' onclick=''cerrarVelo();cancelarPreparacion(1);''>confirmar</span>"
                + "		&nbsp;&nbsp;&nbsp;"
                + "		<span class=''btnMRojo  esq05'' onclick=''cerrarVelo();''>cancelar</span>"
                + "</div>"
            );
        } else {
            abrirVelo(icoCarga16+" cancelando la preparación...");
            if (trsps_linea === undefined) { trsps_linea = 0; }
            var parametros = ''{"sp":"pCancelarPreparacion","pedido":"'' + trsps_numero + ''","serie":"'' + trsps_serie + ''","linea":"'' + trsps_linea + ''","almacen":"'' + Almacen + ''"}'';
            flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) { if (ret) {
                cambiarArticuloCancelar("modificacionesCancelar"); 
            } else { alert(''Error SP pCancelarPreparacion!'' + JSON.stringify(ret)); } }, false);
        }
    }

    function editarLineaTraspaso(id, numero, articulo, definicion, unidades, peso, servidas, linea, cajas, traspaso, pesoPend) {
        abrirVelo(infoCarga("editando la linea..."));

        gID = id;
        gArticulo = articulo;
        gArtUnidades = unidades;
        gArtPeso = peso;
        gArtServidas = servidas;
        trsps_numero = numero;
        trsps_articulo = articulo;
        trsps_linea = linea; 

        var parametros = ''{"sp":"pEditarLineaTraspaso","pedido":"'' + trsps_numero + ''","serie":"'' + trsps_serie + ''","articulo":"'' + trsps_articulo + ''","unidades":"'' + unidades + ''","linea":"'' + linea + ''","cajas":"'' + cajas + ''","peso":"'' + peso + ''","servidas":"'' + servidas + ''","traspaso":"'' + traspaso + ''","pesoPend":"'' + pesoPend + ''","basculaEnUso":"'' + basculaEnUso + ''"}'';
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
            var contenido = "";
            var js = JSON.parse(limpiarCadena(ret.JSCode));
            if (js.length > 0) {
                // si existen registros en Traspaso_Temporal devolvemos un aviso y salimos de la función
                if (js[0].existeTraspTemp === 1) {
                    for (var i = 0; i < js.length; i++) {                        
                        var clCampoLote = "";
                        var clCampoCajas = "";
                        var clCampoPeso = "";
                        var fCaducidad = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].fCaducidad);
                        if (js[i].lote.replace(/-/g, "") === "") { clCampoLote = "display:none;"; }
                        if (parseInt(js[i].cajas) === 0 || isNaN(js[i].cajas) || js[i].cajas === null) { clCampoCajas = "display:none;"; }
                        if (parseFloat(js[i].peso) < 0.001 || isNaN(js[i].peso) || js[i].peso === null|| js[i].peso === "") { clCampoPeso = "display:none;"; }
                        if (fCaducidad.trim() === "" || fCaducidad === "01-01-1900" || fCaducidad === undefined) { fCaducidad = "<span style=''color:#900;''>no disponible</span>"; }
                        if (i === 0) {
                            contenido = "<div style=''text-align:right;''><img src=''./Merlos/images/cerrar.png'' width=''20'' style=''cursor:pointer;'' onclick=''cerrarVelo();''/></div>"
                                + "<div style=''margin-bottom:20px;font:bold 16px arial; color:#900;''>El artículo " + gArticulo + " ya está preparado!</div>"
                                + "<table id=''tbArtEnTemp'' class=''tbStd''>"
                                + "	<tr>"
                                + "		<th style=''text-align:center;" + clCampoLote + "''>Lote</th>"
                                + "		<th style=''text-align:center;''>Caducidad</th>"
                                + "		<th style=''text-align:center; " + clCampoCajas + "''>Cajas</th>"
                                + "		<th style=''text-align:center;''>Uds</th>"
                                + "		<th style=''text-align:center;" + clCampoPeso + "'' class=''campoPeso''>Peso</th>"
                                + "	</tr>";
                        }
                        contenido += "	<tr>"
                            + "		<td style=''text-align:center;" + clCampoLote + "''>" + js[i].lote + "</td>"
                            + "		<td style=''text-align:center;''>" + fCaducidad + "</td>"
                            + "		<td style=''text-align:center;" + clCampoCajas + "''>" + parseInt(js[i].cajas) + "</td>"
                            + "		<td style=''text-align:center;''>" + parseInt(js[i].unidades) + "</td>"
                            + "		<td style=''text-align:center;" + clCampoPeso + "''>" + parseFloat(js[i].peso).toFixed(3) + "</td>"
                            + "	</tr>";
                    }
                    contenido += "</table>";
                    abrirVelo(contenido);
                    return;
                }

                // Trabajo SIN lotes
                if (js[0].existeTraspTemp === 2) {
                    tecladoModo = "AsignarSinLote";

                    traspaso = parseInt(js[0].TRASPASO);
                    var pedidoCajas = parseInt(js[0].pCajas);
                    var pedidoUnidades = parseInt(js[0].UNIDADES);
                    var pedidoPeso = parseFloat(js[0].PESO).toFixed(3);
                    var stockCajas = parseInt(js[0].STOCKCAJAS);
                    var stockUds = parseInt(js[0].elStock);
                    var stockPeso = parseFloat(js[0].STOCKPESO).toFixed(3);
                    var prepCajas = parseInt(js[0].tempCajas);
                    var prepUds = parseInt(js[0].tempUds);
                    var prepPeso = parseFloat(js[0].tempPeso).toFixed(3);

                    var colSpanStock = 3;
                    var colSpanPrep  = 4;

                    if (stockPeso < 0) { stockPeso = 0; }
                    var cajasVis = "display:visible;"; if (pedidoCajas < 1) { cajasVis = "display:none;"; colSpanStock--; colSpanPrep--; }
                    var pesoVis = "display:visible;"; if (pedidoPeso < 0.0001) { pesoVis = "display:none;"; colSpanStock--; colSpanPrep--; }

                    contenido = "<table id=''tbCabaceraLotes'' class=''tbStd'' style=''margin-top:10px;''>"
                        + "  <tr>"
                        + "      <th style=''''>ARTICULO</th>"
                        + "      <th style=''''>NOMBRE</th>"
                        + "      <th style=''text-align:center;" + cajasVis + "''>CAJAS</th>"
                        + "      <th style=''text-align:center;''>UDS.</th>"
                        + "      <th style=''text-align:center; "+pesoVis+"''>PESO</th>"
                        + "  </tr>"
                        + "	 <tr style=''background:#daf7ff;''>"
                        + "          <td style=''''>" + trsps_articulo + "</td>"
                        + "          <td style=''''>" + js[0].DEFINICION + "</td>"
                        + "          <td style=''text-align:right;" + cajasVis + "''>" + cajas + "</td>"
                        + "          <td style=''text-align:right;''>" + (pedidoUnidades-traspaso) + "</td>"
                        + "          <td style=''text-align:right;" + pesoVis + "''>" + parseFloat(pesoPend).toFixed(3) + "</td>"
                        + "      </tr>"
                        + "</table>"
                        + "<br><br>"
                        + "<table id=''tbCoU'' class=''tbStd''>"
                        + "	 <tr style=''border-bottom:1px solid #FFF;''>"
                        + "	    <th colspan=''"+colSpanStock+"'' style=''text-align:center;''>STOCK</th>"
                        + "	    <th colspan=''"+colSpanPrep+"'' class=''bgVerdePalido'' style=''vertical-align:middle;text-align:center;''>PREPARACIÓN "
                        + "			<span id=''spanBasculas'' style=''float:right; vertical-align:middle; " + pesoVis + " " + js[0].verSpnTipoBascula + "''>"
                        + "				<img id=''BI1'' class=''imgBascula01 icoBascula'' src=''./Merlos/images/" + js[0].bsc1img + "'' style=''width:30px; margin-right:20px; cursor:pointer;'' onclick=''basculaClick(\"imgBascula01\")''/> "
                        + "				<img id=''BI2'' class=''imgBascula02 icoBascula'' src=''./Merlos/images/" + js[0].bsc2img + "'' style=''width:30px; margin-right:20px; cursor:pointer;'' onclick=''basculaClick(\"imgBascula02\")''/> "
                        + "				<img id=''BI3'' class=''imgBascula03 icoBascula'' src=''./Merlos/images/" + js[0].bsc3img + "'' style=''width:30px; margin-right:20px; cursor:pointer;'' onclick=''basculaClick(\"imgBascula03\")''/> "
                        + "				<img id=''BI4'' class=''imgBascula04 icoBascula'' src=''./Merlos/images/" + js[0].bsc4img + "'' style=''width:30px; margin-right:20px; cursor:pointer;'' onclick=''basculaClick(\"imgBascula04\")''/> "
                        + "			</span>"
                        + "		</th>"
                        + "  </tr>"
                        + "  <tr id=''tbLineasDePedidoTraspasoCabecera''>"
                        + "      <th style=''width:14%;text-align:center;" + cajasVis + "''>CAJAS</th>"
                        + "      <th style=''width:14%;text-align:center;''>UDS.</th>"
                        + "      <th style=''width:14%;text-align:center;" + pesoVis + "''>PESO</th>"
                        + "      <th style=''width:14%;text-align:center;" + cajasVis + "'' class=''bgVerdePalido'' >CAJAS</th>"
                        + "      <th style=''width:14%;text-align:center;'' class=''bgVerdePalido'' >UDS.</th>"
                        + "      <th style=''width:14%;text-align:center;" + pesoVis + "'' class=''bgVerdePalido'' >PESO</th>"
                        + "      <th style=''width:14%;text-align:center;" + pesoVis + "'' class=''bgVerdePalido'' >BÁSCULA</th>"
                        + "  </tr>"
                        + "	 <tr style=''background:0;'' class=''trDatosLinea " + js[0].cntrlSotck + " " + js[0].elStock + "''>"
                        + "          <td style=''text-align:right; background:0;" + cajasVis + "''><span class=''stockCajas''>"+stockCajas+"</span></td>"
                        + "          <td style=''text-align:right; background:0;'' class=''stockUnidades''>" + stockUds + "</td>"
                        + "          <td class=''tdPeso'' style=''text-align:right; background:0;" + pesoVis + "''>" + stockPeso + "</td>"
                        + "          <td class=''bgVerdePalido'' style=''" + cajasVis + "''><input type=''text'' id=''sinLoteCAJAS'' class=''tecNum tecV taR w100'' style=''" + cajasVis + " padding:4px; border:0; outline:none; text-align:right;'' value=''" + prepCajas + "''></td>"
                        + "          <td class=''bgVerdePalido'' ><input type=''text'' id=''sinLoteUDS''   class=''tecNum tecV taR w100'' value=''" + prepUds + "'' style=''padding: 4px; border: 0; outline: none; text-align:right;''></td>"
                        + "          <td class=''bgVerdePalido'' style=''" + pesoVis + "''><input type=''text'' id=''sinLotePESO''  class=''tecNum tecV taR w100'' style=''padding:4px; border:0; outline:none; text-align:right;'' value=''" + prepPeso + "''></td>"
                        + "          <td style=''text-align:center;" + pesoVis + "'' class=''bgVerdePalido'' >"
                        + "             <div style=''display:flex; align-items:flex-start; justify-content: space-evenly;''>"
                        + "				    <div><img id=''btnBascula'' src=''./Merlos/images/btnBascula.png'' width=''40'' onclick=''capturarPeso(this.id,\"sinLotePESO\")''></div>"
                        + "				    <div>&nbsp;&nbsp;&nbsp;</div>"
                        + "				    <div><img id=''btnBascSum1'' src=''./Merlos/images/btnBasculaSuma.png'' width=''40'' onclick=''capturarPesoSuma(this.id,\"sinLotePESO\")''></div>"
                        + "             </div>"
                        + "      	</td>"
                        + "      </tr>"
                        + "</table>";

                    ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion, .teclado, #dvLineasDePedido, #dvLineasDePedidoTraspaso").hide();
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvDatosDePedido").show();
                    ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasDeArticulo").html(contenido).slideDown();
                    activarObjetos();
                    cerrarVelo();
                    return;
                }

                // Trabajo con Lotes
                tecladoModo = "AsignarConLote";

                var elID = "";
                var cajasVis = "";
                var pesoVis = "";
                var colCab = 6; var colLin1 = 5; var colLin2 = 4; var colLin3 = 9; 
                if (cajas == "" || parseInt(cajas) == 0 || isNaN(cajas)) { cajas = ""; cajasVis = "display:none;"; colCab--; colLin1--; colLin2--; colLin3--; }
                if (pesoPend == "" || parseFloat(pesoPend) == 0.000 || isNaN(pesoPend)) { pesoPend = ""; pesoVis = "display:none;"; colCab--; colLin1--; colLin2--; colLin3--; }
                var contenido = ""
                    + "<table id=''tbCabaceraLotes'' class=''tbStd''>"
                    + "  <tr id=''tbLineasDePedidoTraspasoCabecera''>"
                    + "      <th style=''''>ARTICULO</th>"
                    + "      <th style=''''>NOMBRE</th>"
                    + "      <th style=''" + cajasVis + "''>CAJAS</th>"
                    + "      <th style=''text-align:center;''>UDS.</th>"
                    + "      <th style=''" + pesoVis + " text-align:center;''>PESO</th>"
                    + "  </tr>"
                    + "	 <tr style=''background:#daf7ff;''>"
                    + "          <td style=''''>" + articulo + "</td>"
                    + "          <td style=''''>" + js[0].descripcion + "</td>"
                    + "          <td style=''text-align:right;" + cajasVis + "'' class=''tbCabLotesCAJAS''>" + cajas + "</td>"
                    + "          <td style=''text-align:right;''>" + traspaso + "</td>"
                    + "          <td style=''text-align:right;" + pesoVis + "''>" + parseFloat(pesoPend).toFixed(3) + "</td>"
                    + "      </tr>"
                    + "</table>"
                    + "</div><br>"
                    + "<div id=''dvLineasLotes''>"
                    + "<table id=''tbLineasLotes'' class=''tbStd''>"
                    + "	    <tr style=''border-bottom:1px solid #FFF;'' class=''bgVerdePalido''>"
                    + "		<th colspan=''" + colLin1 + "'' style=''text-align:center;''>STOCK</th>"
                    + "		<th colspan=''" + colLin2 + "'' style=''vertical-align:middle; text-align:center;'' class=''bgVerdePalido''>PREPARACIÓN "
                    + "			<span id=''spanBasculas'' style=''float:right; vertical-align:middle; " + pesoVis + " " + js[0].verSpnTipoBascula + " ''>"
                    + "				<img id=''BI01'' class=''imgBascula01 icoBascula'' src=''./Merlos/images/" + js[0].bsc1img + "'' style=''width:30px; margin-right:20px; cursor:pointer;'' onclick=''basculaClick(\"imgBascula01\")''/> "
                    + "				<img id=''BI02'' class=''imgBascula02 icoBascula'' src=''./Merlos/images/" + js[0].bsc2img + "'' style=''width:30px; margin-right:20px; cursor:pointer;'' onclick=''basculaClick(\"imgBascula02\")''/> "
                    + "				<img id=''BI03'' class=''imgBascula03 icoBascula'' src=''./Merlos/images/" + js[0].bsc3img + "'' style=''width:30px; margin-right:20px; cursor:pointer;'' onclick=''basculaClick(\"imgBascula03\")''/> "
                    + "				<img id=''BI04'' class=''imgBascula04 icoBascula'' src=''./Merlos/images/" + js[0].bsc4img + "'' style=''width:30px; margin-right:20px; cursor:pointer;'' onclick=''basculaClick(\"imgBascula04\")''/> "
                    + "			</span>"
                    + "		</th>"
                    + "   </tr>"
                    + "   <tr>"
                    + "       <th style=''''>Lote</th>"
                    + "       <th style=''''>Caducidad</th>"
                    + "       <th style=''text-align:center;" + cajasVis + "''>CAJAS</th>"
                    + "       <th style=''text-align:center;''>UDS.</th>"
                    + "       <th style=''text-align:center;" + pesoVis + "''>PESO</th>"
                    + "       <th style=''text-align:center;" + cajasVis + "''  class=''bgVerdePalido''>CAJAS</th>"
                    + "       <th style=''text-align:center;'' class=''bgVerdePalido''>UDS.</th>"
                    + "       <th style=''text-align:center;" + pesoVis + "'' class=''bgVerdePalido''>PESO</th>"
                    + "       <th style=''text-align:center;" + pesoVis + "'' class=''bgVerdePalido''>BÁSCULA</th>"
                    + "   </tr>"
                    + "	  <tr style=''background:0;''><td colspan=''" + colLin3 + "'' style=''height:5px; padding:0; background:0;''></td></tr>";

                var cajasLin = parseInt(cajas);
                if(cajasLin==="" || isNaN(cajasLin)){ cajasLin=0;}
                var restoCajas = parseInt(cajas);
                var udsLin = parseInt(traspaso);
                var RESTO = parseInt(traspaso);
                var suma = 0;
                var sumaCajas = 0;
                var rowUniCaja = 0;

                for (var i = 0; i < js.length; i++) {
                    var elLOTE = (js[i].Llote).replace(/\//g, "--barra--");
                    var elID = (js[i].EMPRESA + "" + ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[i].NUMERO) + "" + js[i].LINIA + "" + elLOTE).replace(/\s+/g, "_");

                    var fechaCorta = js[i].Lcaducidad.substr(8, 2) + "-" + js[i].Lcaducidad.substr(5, 2) + "-" + js[i].Lcaducidad.substr(0, 4);
                    var spanCantidad = "<span style=''font:bold 14px arial; color:green;''>unidades</span>";
                    var txtColor = " color:#000; ";
                    var rowUniCaja = parseInt(js[i].UNICAJA);
                    if (rowUniCaja == "") { rowUniCaja = 0; }
                    var ttCajas = parseInt(js[i].tempCajas);
                    var ttUds = parseInt(js[i].tempUnidades);
                    var ttPeso = parseFloat(js[i].tempPeso).toFixed(3);
                    var stockCajas;
                    if(rowUniCaja>1){ stockCajas=(parseInt(js[i].Lunidades) / rowUniCaja) - ttCajas; }
                    var stockUnidades = parseInt(js[i].Lunidades) - parseInt(js[i].tempUnidades);

                    if (ttCajas > 0 || ttUds > 0 || ttPeso > 0.000) { txtColor = " color:green; "; }
                    if (stockUnidades <= 0) { continue; }

                    if (stockCajas > 0 && cajasLin > 0) {
                        if (restoCajas >= stockCajas) { cajasLin = stockCajas; restoCajas = restoCajas - stockCajas; }
                        else { cajasLin = restoCajas; restoCajas = restoCajas - cajasLin; }
                        if (cajasLin < 0) { cajasLin = 0; }
                        if (restoCajas < 0) { restoCajas = 0; }
                        sumaCajas += cajasLin;
                        if (sumaCajas > cajas) { cajasLin = 0; restoCajas = 0; }
                        cajasP = cajasLin;
                    } else { cajasP = 0; }

                    if (stockUnidades > 0 && udsLin > 0) {
                        if (RESTO >= stockUnidades) { udsLin = stockUnidades; RESTO = RESTO - stockUnidades; }
                        else { udsLin = RESTO; RESTO = RESTO - udsLin; }
                        if (udsLin < 0) { udsLin = 0; }
                        if (RESTO < 0) { RESTO = 0; }
                        suma += udsLin;
                        if (suma > traspaso) { udsLin = 0; RESTO = 0; }
                        unidadesP = udsLin;
                    } else { unidadesP = 0; }

                    pesoRes = parseFloat(js[i].Lpeso);
                    if (pesoRes < 0.000) { pesoRes = 0; }
                    if (stockCajas < 0) { stockCajas = 0; }
                    if (stockUnidades < 0) { stockUnidades = 0; }

                    var mStockCajas = stockUnidades / rowUniCaja;

                    if (rowUniCaja > 0) {
                        cajasP = unidadesP / rowUniCaja;
                        cajasLin = cajasP;
                    }

                    contenido += ""
                        + "     <tr id=''trClick_" + elID + "'' class=''trLotesDatos inp_LoteEntradaUds_" + elID + "'' data-lin=''"+ js[i].LINIA+"''>"
                        + "          <td class=''linLote'' style=''" + txtColor + "''>" + js[i].Llote + "</td>"
                        + "          <td class=''linCaducidad'' style=''text-align:center;" + txtColor + "''>" + fechaCorta + "</td>"
                        + "          <td class=''linUds'' style=''text-align:right;" + cajasVis + " " + txtColor + "'' class=''stockCajas''>" + mStockCajas + "</td>"
                        + "          <td class=''linStock'' style=''text-align:center;''><input type=''text'' id=''inp_LoteEntradaStock_"+elID+"'' class=''tecNum tecV inpStock "+js[i].controlStock+" noInput''  style=''width:150px;text-align:center;border:0; background:0;' + convert(nvarchar(max),NCHAR(36)) + N'txtColor'' value=''"+stockUnidades+"'' readonly /></td>"
                        + "          <td class=''linPeso'' style=''text-align:center;" + txtColor + " " + pesoVis + "''>" + pesoRes.toFixed(3) + "</td>"
                        + "          <td style=''text-align:center;padding:0;" + cajasVis + "''><input type=''text'' id=''inp_LoteEntradaCajas_" + elID + "'' class=''tecNum tecV inpCajas '' style=''text-align:center;" + txtColor + "'' value=''" + cajasP + "''/></td>"
                        + "          <td style=''text-align:center;padding:0;''><input type=''text'' id=''inp_LoteEntradaUds_" + elID + "'' class=''tecNum tecV inpUnidades '' style=''text-align:center;" + txtColor + "'' value=''" + unidadesP + "'' data-MIelId=''" + elID + "'' data-uc=''" + js[i].VENTA_CAJA + ";" + js[i].unidadesOcajas + ";\"" + js[i].almacen + "\"''/></td>"
                        + "          <td style=''text-align:center;padding:0;" + pesoVis + "''><input type=''text'' id=''inp_LoteEntradaPeso_" + elID + "''  class=''tecNum tecV inpPeso " + js[i].Llote + " '' style=''text-align:center;" + txtColor + "'' value=''" + ttPeso + "''/></td>"
                        + "          <td style=''text-align:center;;" + pesoVis + "''>"
                        + "             <div style=''display:flex; align-items:flex-start; justify-content: space-evenly;''>"
                        + "				    <div><img id=''btnBascula" + elID + "'' src=''./Merlos/images/btnBascula.png'' width=''40'' onclick=''capturarPeso(this.id,\"inp_LoteEntradaPeso_" + elID + "\")''></div>"
                        + "				    <div>&nbsp;&nbsp;&nbsp;</div>"
                        + "				    <div><img id=''btnBascSum" + elID + "'' src=''./Merlos/images/btnBasculaSuma.png'' width=''40'' onclick=''capturarPesoSuma(this.id,\"inp_LoteEntradaPeso_" + elID + "\")''></div>"
                        + "			    </div>"
                        + "			</td>"
                        + "      </tr>"
                        + "	    <tr style='' background:0;''><td colspan=''" + colLin3 + "'' style=''padding:2px; background:#07657e;''></td></tr>";
                }
                contenido += "</table></div>";
            } else { contenido = "<div class=''avRojo''>no se han obtenido resultados!</div>"; }

            ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion, .teclado, #dvLineasDePedido, #dvLineasDePedidoTraspaso").hide();
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvDatosDePedido").show();
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasDeArticulo").html(contenido).slideDown();
            activarObjetos();
            cerrarVelo();
        } else { alert(''Error SP pEditarLineaTraspaso!'' + JSON.stringify(ret)); } }, false);
    }

    function traspasoCompleto() {
        if(!' + convert(nvarchar(max),NCHAR(36)) + N'(".btnTraspasoCompleto img").hasClass("btnAlo")){ return; }
        abrirVelo(infoCarga("traspasando el pedido..."));
        if (!' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasDePedidoTraspaso").is(":visible")) { return; }
        var articulos = [];
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasDePedidoTraspaso").find(".tbLineasDePedidoTraspasoTR").each(function () {
            var articulo = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDarticuloPte").text();
            var cajas = (' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDcajasPte").text()).replace(/,/g, "");             if(cajas===""){ cajas="0"; }
            var unidades = (' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDunidadesPte").text()).replace(/,/g, "");       if(unidades===""){ unidades="0"; }
            var peso = (' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDpesoPte").text()).replace(/,/g, "");               if(peso===""){ peso="0"; }
            var linea = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDlineaPte").text();
            if(' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("data-traspasar")==="true"){
                articulos.push(''{"articulo":"'' + articulo + ''","cajas":"'' + cajas + ''","unidades":"'' + unidades + ''","peso":"'' + peso + ''","linea":"'' + linea + ''"}'');
            }
        }); 
        var parametros = ''{"sp":"pTraspasoCompleto"''
                    + '',"articulos":['' + articulos +'']''
                    + '',"numero":"'' + NumPedido + ''"''
                    + '',"serie":"'' + PedidoSerie + ''"''
                    + '',"ruta":"'' + Ruta + ''"''
                    + '',"almacen":"'' + Almacen + ''"''
                    + '','' + params
                    + ''}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {if (ret) { 
                cerrarVelo(); recargar(1);
        } else { alert(''Error SP pTraspasoCompleto!\n'' + JSON.stringify(ret)); } }, false);
    }

    // cálculo de unidades/cajas SIN LOTE
    function calcularCajasDesdeSinLoteUDS() { 
        cerrarVelo();       
        return; // modificación Albert 20-10-2020
        
    }
    function calcularUDSDesdeSinLoteCAJAS() {
        var lasCajas = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLoteCAJAS").val());
        if (isNaN(lasCajas)) { alert("Ups! Parece que el valor no es correcto!"); return; }
        var parametros = ''{"sp":"pDatos","modo":"dameUNICAJA","articulo":"''+trsps_articulo+''"}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {if (ret) {
            // devuelve UNICAJA de GESTION_articulo
            var UNICAJA = parseFloat(ret.JSCode);
            var unidades = 0;
            if (!isNaN(UNICAJA)) { unidades = lasCajas * UNICAJA; }
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLoteUDS").val(unidades.toFixed(0));
            cerrarVeloTeclado();
        } else { alert(''Error SP pSeries!'' + JSON.stringify(ret)); } }, false);
    }

    // cálculo de unidades/cajas CON LOTE
    function calcularCajasDesdeConLoteUDS(trgt) {
        cerrarVeloTeclado();
        return; // modificación Albert 20-10-2020
    }
    function calcularUdsDesdeConLoteCAJAS(trgt) {
        var lasCajas = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#" + trgt).val());
        var elInputUds = "inp_LoteEntradaUds" + trgt.substring(20, trgt.length);
        var parametros = ''{"sp":"pDatos","modo":"dameUNICAJA","articulo":"''+trsps_articulo+''"}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {if (ret) {
            // devuelve UNICAJA de GESTION_articulo
            var UNICAJA = parseFloat(ret.JSCode);
            var unidades = 0;
            if (!isNaN(UNICAJA)) { unidades = lasCajas * UNICAJA; }
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + elInputUds).val(unidades.toFixed(0));
            cerrarVeloTeclado();
        } else { alert(''Error SP pSeries!'' + JSON.stringify(ret)); } }, false);
    }

    function ventaCajas(id) {
        var uds = ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + id).val();
        var dataUC = ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + id).attr("data-uc");
        var ventaCaja = dataUC.split(";")[0];
        var uniCaja = dataUC.split(";")[1];

        if (ventaCaja === "1") {
            if (!Number.isInteger(parseFloat(uds).toFixed(2) / parseInt(uniCaja))) {
                abrirVelo(
                    "<div class=''taC'' style=''font:16px arial; color:#333;''>"
                    + icoAlert50 + "<br><br>"
                    + "Este artículo sólo se puede servir por cajas completas!"
                    + "	<br><br><br><span class=''btnRojo esq05'' onclick=''cerrarVelo();''>aceptar</span>"
                    + "</div>"
                );
                var elIdCajas = "inp_LoteEntradaCajas_" + ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + id).attr("data-MIelId");
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#" + id + ",#" + elIdCajas).val("");
            }
        }
    }


    function cambiarArticulos(pedido, serie, ruta) {
        if(!' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCambiarArt img").hasClass("btnAlo")){ return; }
        if(' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCambiarArt img").attr("src")==="./Merlos/images/CambiarArticuloVerde.png" && cambiarArticuloListado.length===0){ cambiarArticuloCancelar(); recargar(1); return; }        
        cambiandoArticulo=true;
        var parametros = ''{"sp":"pCambiarArticulos","pedido":"''+pedido+''","serie":"''+serie+''"}'';
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){ 
            var js = JSON.parse(limpiarCadena(ret.JSCode));
            if(js.length>0){
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasDePedidoTraspaso").find(".tbLineasDePedidoTraspasoTR").each(function(){
                    ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("onclick","").find(".tbAccion").attr("onclick","").css("background",""); /* eliminamos imagen y onclick de las líneas */
                    for(var i in js){
                        if(js[i].articulo===' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("data-articulo")){                            
                            ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbAccion")
                                .css("background","url(''./Merlos/images/intercambiar.png'') no-repeat center").css("background-size","90%")
                                .parent().attr("onclick","cambiarArticulosEq(''" + js[i].articulo + "'',''" + js[i].definicion + "'',''" + js[i].linia + "'',''" + js[i].uds_pv + "'')"); 
                            continue; 
                        }
                    }                    
                });
            }else{ alert("No se han obtenido registros!"); }
            activarObjetos();
        } else { alert(''Error SP pCambiarArticulos!''+JSON.stringify(ret)); }}, false);
    }

    function cambiarArticulosEq(articulo, definicion, linea, unidades) {
        abrirVelo(icoCarga16+" cargando artículos equivalentes...");

        var parametros = ''{"sp":"pCambiarArticulos","modo":"equivalencia","articulo":"''+articulo+''"}'';
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
            var elJS = JSON.parse(limpiarCadena(ret.JSCode));
            var contenido = "<div class=''poderCerrarVelo''>Selecciona el artículo equivalente para sustituir a<br>"+articulo+" - "+definicion+"</div>"
                +"<br>"
                +"<table id=''tbCambiarArticulosEq'' class=''tbStd''>"
                + "<tr>"
                + "	<th class=''taL''>Artículo</th>"
                + "	<th class=''taL''>Descripción</th>"
                + "	<th style=''text-align:center;''>Uds.</th>"
                + "	<th style=''text-align:center;''>Stock</th>"
                + "</tr>";
            if (elJS.length > 0) {
                for (var i in elJS) {
                    var elStock = parseFloat(elJS[i].stock); if (elStock === 0 || isNaN(elStock)) { elStock = ""; }
                    contenido += ""
                        + "<tr class=''sobre'' onclick=''marcarCambiarArticulo(\"" + elJS[i].ARTICULO + "\",\"" + elJS[i].NOMBRE + "\",\"" + linea + "\",\"" + unidades + "\",\"" + articulo + "\",\"" + definicion + "\")''>"
                        + "	<td class=''taL''>" + elJS[i].ARTICULO + "</td>"
                        + "	<td class=''taL''>" + elJS[i].NOMBRE + "</td>"
                        + "	<td style=''text-align:right;''>" + unidades + "</td>"
                        + "	<td style=''text-align:right;''>" + elStock + "</td>"
                        + "</tr>";
                }
                contenido += "</table>";
            } else { contenido = "<div class=''info''>No se han obtenido resultados!</div>"; }
            abrirVelo(contenido);
        } else { alert(''Error SP pCambiarArticulos - equivalencia!''+JSON.stringify(ret)); }}, false);
    }

function cambiarArticuloCancelar(modo){
	cambiandoArticulo=false;
	cambiarArticuloListado = [];
    ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnCambiarArt img").attr("src","./Merlos/images/btnCambiarArtC.png");    
    if(!modo){ verLineasDelPedido(Numero,Serie,Ruta,RutaNombre,Almacen); cerrarVelo(); }
    if(modo==="modificacionesCancelar"){modificacionesCancelar();}
}

function Velo(msj){
	if(!msj){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvVELO").fadeOut(300,function(){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvVELO").remove(); }); }
	else{
		' + convert(nvarchar(max),NCHAR(36)) + N'("body").prepend(
			 "<div id=''dvVELO'' class=''inv'' style=''z-index:999;position:fixed;width:100%; height:100%; background:rgba(250,250,250,.75);''>"
			+"	<div style=''width:500px; margin:100px auto; background:#07657e; padding:20px; text-align:center; font:20px arial; color:#FFF'' "
			+"	class=''esq10 sombra001''>"+msj+"</div>"
			+"</div"
		);
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvVELO").fadeIn();
	}
}

function marcarCambiarArticulo(articulo, nombre, linea, unidades, articuloOriginal, definicionOriginal) {
    abrirVelo(icoCarga16+" Sustituyendo el artículo " + articulo + "...");
    cambiarArticuloListado.push({"linea":linea,"articuloOriginal":articuloOriginal,"definicionOriginal":definicionOriginal,"articulo":articulo,"nombre":nombre});
    var parametros = ''{"sp":"pSustituirArticulos","articulo":"''+articulo+''","linea":"''+linea+''","unidades":"''+parseInt(unidades)+''","empresa":"''+Empresa+''","letra":"''+Serie+''","numero":"''+Numero+''"}'';
    flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){ 
        cambiandoArticulo=false; recargar(1); cerrarVelo();
    } else { alert(''Error SP pSeries!''+JSON.stringify(ret)); }}, false);
}

function modificarPedido() {
    if(!' + convert(nvarchar(max),NCHAR(36)) + N'(".btnModificarPedido img").hasClass("btnAlo")){ return; }
    if(' + convert(nvarchar(max),NCHAR(36)) + N'(".btnModificarPedido img").attr("src")==="./Merlos/images/ModificarPedidoVerde.png" && modificarPedidoListado.length===0){ modificacionesCancelar(); recargar(1); return; }        
    ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedidoVerde.png");
    modificandoPedido=true;

    var parametros = ''{"sp":"pPedidos","modo":"dameLineas","Empresa":"''+Empresa+''","Numero":"''+Numero+''","Serie":"''+Serie+''"''
                    +'',"Ruta":"''+Ruta+''","Almacen":"''+Almacen+''","usuarioSesion":"''+currentReference+''","pedidoActivo":"''+PedidoActivo+''"}'';	
    flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
        var js1 = JSON.parse(limpiarCadena(ret.JSCode));  
        var js  = js1.datos;       
        if(js.length>0){
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasDePedidoTraspaso").find(".tbLineasDePedidoTraspasoTR").each(function(){
                ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("onclick","").find(".tbAccion").attr("onclick","").css("background",""); /* eliminamos imagen y onclick de las líneas */
                for(var i in js){
                    if(js[i].articulo===' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("data-articulo")){                            
                        ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbAccion")
                            .css("background","url(''./Merlos/images/intercambiar.png'') no-repeat center").css("background-size","90%")
                            .parent().attr("onclick","intercambiarArticulo("+i+","+js[i].linia+",''"+js[i].articulo+"'',''"+js[i].definicion.trim()+"'')"); 
                        continue; 
                    }
                }                    
            });
        }else{ alert("No se han obtenido registros!"); }
        activarObjetos();
    } else { alert(''Error SP pPedidos - dameLineas!''+JSON.stringify(ret)); }}, false);
}

function modificacionesCancelar(modo){
	modificandoPedido=false;
	modificarPedidoLineas = [];
    ' + convert(nvarchar(max),NCHAR(36)) + N'(".btnModificarPedido img").attr("src","./Merlos/images/ModificarPedido.png");
    if(!modo){ verLineasDelPedido(Numero,Serie,Ruta,RutaNombre,Almacen); cerrarVelo(); }
}

    function borrarPropuesta() {
        if( !' + convert(nvarchar(max),NCHAR(36)) + N'(".btnPreparacion img").hasClass("btnAlo") ){ return; }
        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#tbCoU").is(":visible")) { ' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLoteCAJAS,#sinLotePESO,#sinLoteUDS").val(""); }
        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasLotes").is(":visible")) { ' + convert(nvarchar(max),NCHAR(36)) + N'(".inpCajas,.inpUnidades,.inpPeso").val(""); }
    }

    function traspasarArtLote() {
        if( !' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosPreparar img").hasClass("btnAlo") ){ return; }
        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#dvLineasLotes").is(":visible")) { /* artículos con lote */  traspasarConLote(); }
        else { /* artículos sin lote */  traspasarSinLote(); }
    }

    function traspasarConLote() {
        var str = "";
        var stock = 0;
        var unidades = 0;
        var cntrlStock = 0;
        var stockKO = "";
        var esteValor = 0;
        var cajas = 0;
        var stockCajas = 0;
        var peso = 0.000;
        var faltaPeso = false;

        var lineas = "";
        var lasLineas = "";
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasLotes").find(".trLotesDatos").each(function () {
            ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".inpPeso").each(function () {
                esteValor = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).val();
                if (isNaN(esteValor) || esteValor === "") { esteValor = 0; };
                str += ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr(''class'').split(" ")[3] + ''_D_'' + esteValor + "_D_"; peso = esteValor;
                lasLineas += ''{"lote":"''+' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr(''class'').split(" ")[3]+''","peso":"''+esteValor+''"'';
            });
            ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".inpStock").each(function () { esteValor = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).val(); if (isNaN(esteValor) || esteValor === "") { esteValor = 0; }; str += esteValor + "_D_"; stock = esteValor; lasLineas += '',"stock":"''+stock+''"''; cntrlStock = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr(''class'').split(" ")[3]; });
            ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".inpUnidades").each(function () { esteValor = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).val(); if (isNaN(esteValor) || esteValor === "") { esteValor = 0; }; str += esteValor + "_D_"; unidades = esteValor; lasLineas += '',"unidades":"''+unidades+''"''; });
            ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".inpCajas").each(function () { esteValor = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).val(); if (isNaN(esteValor) || esteValor === "" || esteValor === "Infinity") { esteValor = 0; }; str += esteValor + "_A_"; cajas = esteValor; if (cajas === "Infinity") { cajas = "0"; } lasLineas += '',"cajas":"''+cajas+''"''; });
            ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".stockCajas").each(function () { esteValor = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).text(); if (isNaN(esteValor) || esteValor === "") { esteValor = 0; }; stockCajas = esteValor; });

            if (' + convert(nvarchar(max),NCHAR(36)) + N'(".inpPeso").is(":visible") && parseFloat(unidades) > 0 && parseFloat(peso) === 0.000) { faltaPeso = true; }

            // controlar stock (unidades/cajas)
            if (parseFloat(unidades) > 0 && (parseFloat(unidades) > parseFloat(stock))) { stockKO = "NoHayStock"; }

            lasLineas += ''}'';
        });
        str = str.substring(0, str.length - 3);
        lasLineas = "["+lasLineas.replace(/}{/g,"},{")+"]";
        if (faltaPeso) { alert("Debes especificar el PESO!"); return; }

        // Control de Stock
        if ((cntrlStock==="1" || cntrlStock==="true") && stockKO !== "") { alert("\nNo hay stock suficiente de algún lote!\nPor favor, verifica los datos."); return; }
        if (unidades === "") { alert("Verifica los campos!"); return; }
        if (isNaN(unidades) || isNaN(peso)) { alert("Ups! Parece que los datos no son correctos!"); return; }

        ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasLotes").html(infoCarga("Actualizando los datos...</div>"));
        var jLineas = JSON.parse(lasLineas);
        for(var l=0; l<jLineas.length; l++){            
            (function(){
                var parametros = ''{"sp":"pActualizaLineaTraspasoLote"''
                    + '',"pedido":"'' + gPedido + ''"''
                    + '',"serie":"'' + trsps_serie + ''"''
                    + '',"articulo":"'' + trsps_articulo + ''"''
                    + '',"almacen":"'' + Almacen + ''"''
                    + '',"linea":"'' + trsps_linea + ''"''
                    + '',"lote":"'' + jLineas[l].lote + ''"''
                    + '',"peso":"'' + jLineas[l].peso + ''"''
                    + '',"stock":"'' + jLineas[l].stock + ''"''
                    + '',"unidades":"'' + jLineas[l].unidades + ''"''
                    + '',"cajas":"'' + parseInt(jLineas[l].cajas) + ''"''
                    + '','' + params
                    + ''}'';
                flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {
                    if (ret) {
                        if (ret.JSCode === "KO!") { alert("Error SQL:\r" + resp[1]); return; }
                        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dv_LoteEntrada_" + gLoteID).html("<div class=''avRojo''>Recuperando los datos...</div>");
                        recargar();
                    } else { alert(''Error SP pActualizaLineaTraspasoLote!'' + JSON.stringify(ret)); }
                }, false);
            })(l)
        }
    }

    function traspasarSinLote() {
        var cntlStock, elStock, stockKO = false;
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbCabaceraLotes").find(".trDatosLinea").each(function () {
            cntlStock = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("class").split(" ")[1];
            elStock = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("class").split(" ")[2];
        });

        var lasCajasStock = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'(".stockCajas").text());
        var lasUdsStock = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'(".stockUnidades").text());

        var lasCajas = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLoteCAJAS").val()); if (!' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLoteCAJAS").is(":visible")) { lasCajas = "0"; }
        var laCantidad = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLoteUDS").val());
        var elPeso = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLotePESO").val()); if (!' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLotePESO").is(":visible")) { elPeso = "0.000"; }

        if (' + convert(nvarchar(max),NCHAR(36)) + N'("#sinLotePESO").is(":visible") && parseFloat(elPeso) === 0.000) { alert("Debes especificar el PESO!"); return; }

        if (cntlStock === "0") {
            if ((parseFloat(lasCajasStock) < parseFloat(lasCajas)) || (parseFloat(lasUdsStock) < parseFloat(laCantidad))) {
                alert("No hay suficiente Stock del artículo!"); stockKO = true;
            }
        }
        if (stockKO) { return; }

        if (laCantidad === "" || elPeso === "") { alert("Verifica los campos!"); return; }
        if (isNaN(laCantidad) || isNaN(elPeso)) { alert("Ups! Parece que los datos no son correctos!"); return; }

        ' + convert(nvarchar(max),NCHAR(36)) + N'("#dv_SinLote_" + gID).html("<div class=''avRojo''>Actualizando los datos...</div>");

        var parametros = ''{"sp":"pActualizaLineaTraspasoSinLote"''
            + '',"pedido":"'' + gPedido + ''"''
            + '',"serie":"'' + trsps_serie + ''"''
            + '',"lote":"-"''
            + '',"articulo":"'' + trsps_articulo + ''"''
            + '',"cajas":"'' + ifNull(lasCajas,0) + ''"''
            + '',"unidades":"'' + ifNull(laCantidad,0) + ''"''
            + '',"peso":"'' + ifNull(elPeso,0.000) + ''"''
            + '',"linea":"'' + trsps_linea + ''"''
            + ''}'';
        flexygo.nav.execProcess(''pMerlos'', '''', null, null, [{ ''Key'': ''parametros'', ''Value'': limpiarCadena(parametros) }], ''current'', false, ' + convert(nvarchar(max),NCHAR(36)) + N'(this), function (ret) {
            if (ret) {
                var js = JSON.parse(limpiarCadena(ret.JSCode));
                if (js.respuesta === "KO!") {
                    abrirVelo("Error en el Traspaso!");
                    recargar();
                    return;
                }
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#dv_LoteEntrada_" + gLoteID).html(infoCarga("Recuperando los datos..."));
                recargar();
            } else { alert(''Error SP pActualizaLineaTraspasoSinLote!'' + JSON.stringify(ret)); }
        }, false);
    }
    
    function traspasarPedido() {
        if(!' + convert(nvarchar(max),NCHAR(36)) + N'(".btnMerlosTraspaso img").hasClass("btnAlo")){return;}
        // Verificar tabla [MI_Traspaso_PV].[dbo].[EnUso]
        if (contadorTraspasoEnUso === 0) {
            abrirVelo(infoCarga("verificando el pedido..."));
            var parametros = ''{"sp":"pTraspasarPV","numero":"''+trsps_numero+''","letra":"''+trsps_serie+''",''+params+''}''; 
            flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
                var js = JSON.parse(limpiarCadena(ret.JSCode));

                // Verificar EnUso
                if (js["msj"] === "enUso" && contadorTraspasoEnUso < 10) {
                    setTimeout(()=>{ contadorTraspasoEnUso++; traspasarPedido(); }, 200);
                }
                else if (js["msj"] === "enUso" && contadorTraspasoEnUso === 10) {
                    contadorTraspasoEnUso = 0;
                    abrirVelo("Se ha superado el número de intentos.<br>La tabla en uso devuelve True."
                        + "<br>Puedes forzar el estado."
                        + "<br>Si el problema persiste contacta con el administrador del sistema."
                        + "<br><br><br><span class=''boton esq05'' onclick=''forzarEstadoEnUso();''>Forzar Estado</span>"
                        + "&nbsp;&nbsp;&nbsp;<span class=''boton esq05'' onclick=''cerrarVelo();''>cancelar</span><br><br>");
                }
                else {                    
                    // Verificar Lotes
                    if (js["msj"] === "verifLotes"){
                        var losArts = "<table id=''tbArtSinLoteAsignado'' class=''tbStd L''>"
                            + "		<tr>"
                            + "			<th>Código</th>"
                            + "			<th>Nombre</th>"
                            + "		</tr>";
                        for (var i in js) { losArts += ''<tr><td>'' + js[i].CODIGO + ''</td><td>'' + js[i].NOMBRE + ''</td></tr>''; }
                        losArts += "</table>";
                        abrirVelo("Los siguientes artículos trabajan con lote y no tienen ninguno asignado!<br><br>"
                                    +"Entre dentro del artículo indicado del pedido, cancele la preparación y vuelva a asignar el lote para poder continuar con esta operación!!"
                                    + "<br><br>" + losArts
                                    + "<br><br><br><span class=''boton esq05'' onclick=''cerrarVelo();''>aceptar</span><br><br>");
                    }else{
                        // Traspaso del Pedido
                        abrirVelo(
                              "<div id=''avTraspasoDef'' class=''esq10'' style=''width:100%; margin:auto;;border:2px solid #666; padding:10px; text-align:center;''>"
                            + "   <span style=''font:bold 20px arial; color:#07657e;''>Traspaso del Pedido " + gPedido + "</span>"
                            + "</div>"
                            + "<br>"
                            +"<div style=''background:#666; padding:10px;'' class=''esqU10''>"
                            + "     <span style=''font:bold 16px arial; color:#FFF;text-align:left;''>CLIENTE &nbsp;&nbsp;&nbsp;</span>"
                            + "     <span class=''esqU10'' style=''font:bold 16px arial; color:#FFF;text-align:left;''>" + js[0].datosCliente[0].codigo + " - " + ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js[0].datosCliente[0].nombre) + "</span>"
                            + "</div>"
                            + "<div class=''dvTPVClienteDatos'' style=''background:rgb(235,235,235); padding:20px;'' class=''esqD10''>"
                            + "     <div class=''CliDatos''>"
                            + "         <div style=''font:14px arial; color:#666;text-align:left;''>Dirección</div>"
                            + "         <div style=''background:#FFF; margin:1px; padding:5px; font:bold 16px arial; color:#333;text-align:left;''>" + js[0].datosCliente[0].direccion + "</div>"
                            + "         <div style=''margin-top:5px; font:14px arial; color:#666;text-align:left;''>Población</div>"
                            + "         <div style=''background:#FFF; margin:1px; padding:5px; font:bold 16px arial; color:#333;text-align:left;''>" + js[0].datosCliente[0].poblacion + "</div>"
                            + "         <div style=''margin-top:5px; font:14px arial; color:#666;text-align:left;''>Provincia</div>"
                            + "         <div style=''background:#FFF; margin:1px; padding:5px; font:bold 16px arial; color:#333;text-align:left;''>" + js[0].datosCliente[0].cp + " " + js[0].datosCliente[0].provincia + "</div>"
                            + "         <div style=''margin-top:5px; font:14px arial; color:#666;text-align:left;''>Conductor</div>"
                            + "         <div style=''background:#FFF; margin:1px; padding:5px; font:bold 16px arial; color:#333;text-align:left;''>" + js[0].datosCliente[0].agencia_nombre + "</div>"
                            + "         <div style=''margin-top:5px; font:14px arial; color:#666;text-align:left;''>Peso</div>"
                            + "         <div style=''background:#FFF; margin:1px; padding:5px; font:bold 16px arial; color:#333;text-align:left;''>" + parseFloat(js[0].pesoTotal).toFixed(3) + "</div>"
                            + "     </div>"
                            + "     <div class=''Botones''>"
                            + "         <div style=''font:16px arial; color:#333;''>Nº Bultos</div>"
                            + "         <input type=''text'' id=''nBultos'' onkeyup=''replicarBultos()'' class=''tecNum esq05'' style=''width:100px; text-align:center; background:#f2f2f2; padding:8px; font:bold 20px arial; color:#11698E;'' />"
                            + "		    <br><br>"
                            + "		    <div style=''font:16px arial; color:#333;''>" + js[0].etiEtiquetas + "</div>"
                            + "         <input type=''text'' id=''nCajas'' class=''tecNum esq05'' style=''width:100px; text-align:center; background:#f2f2f2; padding:8px; font:bold 20px arial; color:#11698E;'' />"
                            + "		    <br><br><br>"
                            + "         <img id=''btnImpAlb'' src=''./Merlos/images/btnImprimirAlbaranI.png'' class=''imgImp alb''> "
                            + "         <img id=''btnImpEti'' src=''./Merlos/images/btnImprimirEtiquetasI.png'' class=''imgImp eti'' style=''margin-left:20px;''> "
                            + "     </div>"
                            + "</div>"
                            + " <div id=''GenerarAlbaranBotonera'' style=''margin-top:40px; text-align:center;''>"
                            + "     <span class=''btnMRojo esq05'' onclick=''cerrarVelo()''>CANCELAR</span>"
                            + "     <span id=''btnGenerarAlbaran'' class=''btnMVerde esq05'' style=''margin-left:30px;'' onclick=''generarAlbaranDeVenta()''>GENERAR ALBARÁN</span>"
                            + " </div>"
                            + "<div id=''avGenerar'' style=''display:none;''>"
                            + "		<div id=''avGenerarTxt'' style=''background:#4BFE78; padding:20px; font:bold 16px arial; color:#07657e;''>"+icoCarga16+" traspasando el pedido. Por favor, espera...</div>"
                            + "</div>"
                        );
                        ' + convert(nvarchar(max),NCHAR(36)) + N'("#nBultos").focus();
                        if (js[0].impAlb === "0") {
                            imprimirAlbaran = 0;
                            ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnImpAlb").attr("src", "./Merlos/images/btnImprimirAlbaranO.png");
                            ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnGenerarAlbaran").html("IMPRIMIR ETIQUETAS");
                        } else { imprimirAlbaran = 1; }
                        activarObjetos();
                    }
                }
            } else { alert(''Error SP pTraspasarPV!''+JSON.stringify(ret)); }}, false);
        }
    }

    function replicarBultos(){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#nCajas").val(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#nBultos").val())); }

    function generarAlbaranDeVenta(){ 
        var losBultos = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#nBultos").val());
        if(losBultos===""){ alert("Debes especificar el número de bultos!"); return; }
        if(isNaN(losBultos)){ alert("Ups! Parece que el campo BULTOS no es correcto!"); return; }  
        
        var lasCajas = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#nCajas").val());
        if(lasCajas===""){ alert("Debes especificar el número de cajas!"); return; }
        if(isNaN(lasCajas)){ alert("Ups! Parece que el campo CAJAS no es correcto!"); return; }  

        ' + convert(nvarchar(max),NCHAR(36)) + N'("#GenerarAlbaranBotonera").hide();
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#avGenerar").fadeIn();
        if(FacturaDirecta==="true"){FacturaDirecta=1;}else{FacturaDirecta=0;}
        var parametros = ''{"sp":"pAlbaranDeVenta","numero":"''+trsps_numero+''","letra":"''+trsps_serie+''","bultos":"''+losBultos+''"''
            +'',"cajas":"''+lasCajas+''","impAlb":"''+imprimirAlbaran+''","impEti":"''+imprimirEtiquetas+''","FacturaDirecta":''+FacturaDirecta+'',''+params+''}'';
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
            if(ret.JSCode!=="ERROR!"){
                ' + convert(nvarchar(max),NCHAR(36)) + N'("#avGenerarTxt").html("se ha generado el albarán "+ret.JSCode);
                setTimeout(function(){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#btnMerlosHome").click(); cerrarVelo();  buscarGlobal(); },2000);
            }else{ alert("Error al generar el albarán de venta:\n"+ret.JSCode); buscarGlobal(); }
        } else { alert(''Error SP pSeries!''+JSON.stringify(ret)); }}, false);
    }    

function calcularModif(i,modo,unicaja,unipeso){
	var cajas = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#tbModifPedido #inpCajas"+i).val());
	var uds = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#tbModifPedido #inpUds"+i).val());
	var peso = ' + convert(nvarchar(max),NCHAR(36)) + N'.trim(' + convert(nvarchar(max),NCHAR(36)) + N'("#tbModifPedido #inpPeso"+i).val());
	if(modo==="cajas"){ uds = cajas*parseInt(unicaja); if(parseFloat(unipeso)>0){ peso = uds*parseFloat(unipeso); } }
	if(modo==="uds"){ if(parseFloat(unipeso)>0){ peso = uds*parseFloat(unipeso); } }
	' + convert(nvarchar(max),NCHAR(36)) + N'("#inpCajas"+i).val(cajas);
	' + convert(nvarchar(max),NCHAR(36)) + N'("#inpUds"+i).val(uds);
	' + convert(nvarchar(max),NCHAR(36)) + N'("#inpPeso"+i).val(peso);
}

function intercambiarArticulo(i, liniaArtOrig, artOrig, defiOrig){ 
    if(!' + convert(nvarchar(max),NCHAR(36)) + N'("#divIntercambio").is(":visible")){
    ' + convert(nvarchar(max),NCHAR(36)) + N'("#divIntercambio").remove();
        abrirVelo(
            "<div id=''divIntercambio'' style=''background:#FFF; max-height:500px; overflow:hidden; overflow-y:auto;''>"
            +icoCarga16+" cargando artículos..."
            +"</div>"
        );	
    }	
	if(listadoArticulos===""){ cargarArticulos(Almacen); setTimeout(function(){intercambiarArticulo(i, liniaArtOrig, artOrig, defiOrig);},500); return; }
	setTimeout(function(){
		var contenido =  "<div class=''poderCerrarVelo''>Selecciona el artículo para sustituir a<br>"+artOrig+" - "+defiOrig+"</div>"
						+"<div style=''margin:10px 0;''><input type=''text'' id=''inpBuscArt'' style=''width:100%;'' placeholder=''buscar artículo'' onkeyup=''buscarEnTabla(this.id,\"tbListadoDeArticulos\")''></div>"
						+"<div style=''max-height:400px; overflow:hidden; overflow-y:auto;''>"
                        +"  <table id=''tbListadoDeArticulos'' class=''tbStd''>"
						+"	    <tr>"
						+"		    <th>Almacen</th>"
						+"		    <th>Código</th>"
						+"		    <th>Descripción</th>"
						+"		    <th>Stock</th>"
						+"	    </tr>";
		var js = JSON.parse(listadoArticulos);
		for(var j in js.datos){ 
			if(parseFloat(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].ArticuloStock))>0){
				contenido += "  <tr class=''tbData buscarEn'' onclick=''seleccionarArticuloIntercambio(\""+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].CODIGO)+"\",\""+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].NOMBRE)+"\",\""+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].ArticuloStock)+"\",\""+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].UNICAJA)+"\",\""+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].PESO)+"\","+i+","+liniaArtOrig+",\""+artOrig+"\",\""+defiOrig+"\")''>"
							+"	    <td>"+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].almacen)+"</td>"
							+"	    <td class=''buscarEn''>"+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].CODIGO)+"</td>"
							+"	    <td class=''buscarEn''>"+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].NOMBRE)+"</td>"
							+"	    <td class=''taR''>"+' + convert(nvarchar(max),NCHAR(36)) + N'.trim(js.datos[j].ArticuloStock)+"</td>"
							+"  </tr>"; 
			}
		}
		contenido += "</table></div>"
		' + convert(nvarchar(max),NCHAR(36)) + N'("#divIntercambio").html(contenido);
		' + convert(nvarchar(max),NCHAR(36)) + N'("#inpBuscArt").focus();
	},500);
}

function cargarArticulos(almacen){ 
	var parametros = ''{"sp":"pArticulos","modo":"lista","almacen":"''+almacen+''"}'';	
    flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
		listadoArticulos=limpiarCadena(ret.JSCode);
    } else { alert(''Error SP pArticulos - lista!''+JSON.stringify(ret)); }}, false);
}

function seleccionarArticuloIntercambio(artDest,defiDest,stockDest,unicajaDest,pesoDest,i,liniaOrig,artOrig,defiOrig){
    abrirVelo(icoCarga16+" asignando el artículo...");
	
	var mpl = {"Empresa":Empresa,"Numero":Numero,"Serie":Serie,"liniaArtOrig":liniaOrig
			,"Ruta":Ruta,"Almacen":Almacen,"pedido":PedidoActivo,"Cliente":' + convert(nvarchar(max),NCHAR(36)) + N'("#spCodCli").text()
			,"artDest":artDest,"defiDest":defiDest,"stockDest":stockDest,"unicajaDest":unicajaDest,"pesoDest":pesoDest
            ,"articuloOrig":artOrig,"defiOrig":defiOrig};
			
	modificarPedidoListado.push(mpl);
    // pintar modificaciones 
    ' + convert(nvarchar(max),NCHAR(36)) + N'("#tbLineasDePedidoTraspaso").find(".tbLineasDePedidoTraspasoTR").each(function () {        
        var trId = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("id"); 
        var articulo = ' + convert(nvarchar(max),NCHAR(36)) + N'(this).attr("data-articulo"); 
        for(var i in modificarPedidoListado){
            if(' + convert(nvarchar(max),NCHAR(36)) + N'.trim(modificarPedidoListado[i].articuloOrig)===' + convert(nvarchar(max),NCHAR(36)) + N'.trim(articulo)){
                ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDarticuloPte").html(modificarPedidoListado[i].artDest);
                ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tdDefinicion").html(
                    modificarPedidoListado[i].defiDest
                    +"<br><span style=''font:12px arial;color:#FF5733;''>ORIGINAL: "+modificarPedidoListado[i].articuloOrig+" - "+modificarPedidoListado[i].defiOrig+"</span>"
                ); 
                // cambiamos imagen de la linea - añadimos inputs de cajas, unidades y peso
                ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbAccion").css("background","url(''./Merlos/images/Modificado.png'') no-repeat center").css("background-size","90%").parent(); 
                var CajasDisplay = " display:none; ";
                var PesoDisplay = " display:none; ";
                if(parseFloat(unicajaDest)>0){ CajasDisplay=""; }
                if(parseFloat(pesoDest)>0){ PesoDisplay=""; }
                ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDcajasPte").html("<input type=''text'' class=''inpModifCajas'' style=''width:60px; text-align:center;"+CajasDisplay+"'' "
                                                                    +"onkeyup=''calcularModificacionesCUP(\""+trId+"\",\"cajas\",this.value,\""+unicajaDest+"\",\""+pesoDest+"\")'' />"); 
                ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDunidadesPte").html("<input type=''text'' class=''inpModifUnidades'' style=''width:60px; text-align:center;'' "
                                                                    +"onkeyup=''calcularModificacionesCUP(\""+trId+"\",\"unidades\",this.value,\""+unicajaDest+"\",\""+pesoDest+"\")'' />"); 
                ' + convert(nvarchar(max),NCHAR(36)) + N'(this).find(".tbLineasDePedidoTraspasoTDpesoPte").html("<input type=''text'' class=''inpModifPeso'' style=''width:60px; text-align:center;"+PesoDisplay+"'' />"); 
            }
        }
    });
    cerrarVelo();
}

function calcularModificacionesCUP(tr,modo,valor,unicaja,unipeso){
    var _uds, _cajas;
    if(modo==="cajas"){ 
        _uds = parseInt(parseInt(valor)*parseInt(unicaja));
        if(isNaN(_uds)){ _uds=0; }
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+tr).find(".tbLineasDePedidoTraspasoTDunidadesPte").find("input").val(_uds); }
    if(modo==="unidades"){ 
        if(parseFloat(unicaja)>0){
            _uds = valor;
            _cajas = parseInt(parseInt(_uds)/parseInt(unicaja));
            ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+tr).find(".tbLineasDePedidoTraspasoTDcajasPte").find("input").val(_cajas); 
        }
    }
    if(parseFloat(unipeso)>0){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+tr).find(".tbLineasDePedidoTraspasoTDpesoPte").find("input").val( _uds*parseFloat(unipeso) ); }
}

function capturarPeso(img,id){
    ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+img).attr("src","./Merlos/images/btnBasculaV.png");
    ' + convert(nvarchar(max),NCHAR(36)) + N'.post("../php/traspasoPV.php",{jQueryPost:"capturarPeso",basculaEnUso:basculaEnUso}).done(function(data){
		if(data.length>10){ alert("ERROR BÁSCULA!"); ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+img).attr("src","../images/btnBascula.png"); return; }
		' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).val(data); ' + convert(nvarchar(max),NCHAR(36)) + N'("#"+img).attr("src","../images/btnBascula.png");
    });
}

var TotalSuma=0;
function capturarPesoSuma(img,id){    
    abrirVelo(
	    "<div id=''avSumaPesos'' class=''esq10'' "+
	    "style=''width:100%; margin:30px auto;background:#daf7ff;border:2px solid #07657e;padding:10px;text-align:center;''>"+
	    "   <div><img src=''../images/btnMerlosCerrar.png'' style=''float:right; width:50px;'' onclick=''cerrarCapturaPesoSuma()''></div>"+
	    "   <div><span style=''font:bold 20px Gothic; color:#07657e;''>Suma de Pesadas</span></div>"+
	    "   <div style=''height:50px;''></div>"+
	    "   <div style=''overflow:hidden;''>"+
	    "		<div style=''float:left;''>"+
	    "			<div style=''font:bold 16px Gothic; color:#07b55a;''>PESADAS</div>"+
	    "			<br>"+
	    "			<div id=''dvPesadas'' style=''background:#FFF; padding:20px; text-align:right;''></div>"+
	    "		</div>"+
	    "		<div style=''float:left; margin-left:30px;''>"+
	    "			<div style=''font:bold 16px Gothic; color:#07b55a;''>TOTAL</div>"+
	    "			<br>"+
	    "			<div id=''dvTotalPesadas'' style=''background:#FFF; padding:20px; text-align:center;''></div>"+
	    "		</div>"+		
	    "		<div style=''float:right; margin-left:30px;''>"+
	    "			<div style=''font:bold 16px Gothic; color:#07b55a;''>TOTAL</div>"+
	    "			<div><img src=''../images/btnBascula.png''     style=''width:50px;'' onclick=''traspasarSumaPesos(\""+img+"\",\""+id+"\")''></div>"+
	    "		</div>"+
	    "		<div style=''float:right; margin-left:40px;''>"+
	    "			<div style=''font:bold 16px Gothic; color:#07b55a;''>PESAR</div>"+
	    "			<div><img id=''imgCapturaPesoSuma'' src=''../images/btnBasculaSuma.png'' style=''width:50px;'' onclick=''capturarPesoDeSuma()''></div>"+
	    "		</div>"+
	    "		<div style=''float:right;''>"+
	    "			<div style=''font:bold 16px Gothic; color:#07b55a;''>MANUAL</div>"+
	    "			<div>"+
	    "			    <input type=''text'' id=''entradaPesoManual'' style=''width:100px; text-align:center;'' class=''tecNum''>"+
	    "			</div>"+
	    "		</div>"+
	    "	</div>"+
	    "</div>"
    );
	activarObjetos();
}

function capturarPesoDeSuma(manual){
    if(manual){
		TotalSuma += parseFloat(manual);	
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvPesadas").append("<div>"+manual+"</div>");		
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTotalPesadas").html("<div style=''font:bold 20px Gothic; color:#07b55a;''>"+TotalSuma.toFixed(3)+"</div>");	
		' + convert(nvarchar(max),NCHAR(36)) + N'("#entradaPesoManual").val("");
		' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTeclado").slideUp();
    }else{ 
		' + convert(nvarchar(max),NCHAR(36)) + N'("#imgCapturaPesoSuma").attr("src","../images/btnBasculaSumaV.png");
		' + convert(nvarchar(max),NCHAR(36)) + N'.post("../php/traspasoPV.php",{jQueryPost:"capturarPeso"}).done(function(data){
			if(data.length>30){ ' + convert(nvarchar(max),NCHAR(36)) + N'("#imgCapturaPesoSuma").attr("src","../images/btnBasculaSuma.png"); return; }
			TotalSuma += parseFloat(data);
			' + convert(nvarchar(max),NCHAR(36)) + N'("#dvPesadas").append("<div>"+data+"</div>");		
			' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTotalPesadas").html("<div style=''font:bold 20px Gothic; color:#07b55a;''>"+TotalSuma.toFixed(3)+"</div>");		
			' + convert(nvarchar(max),NCHAR(36)) + N'("#imgCapturaPesoSuma").attr("src","../images/btnBasculaSuma.png");
		});
    }
}

function traspasarSumaPesos(img,id){
	' + convert(nvarchar(max),NCHAR(36)) + N'("#"+id).val(TotalSuma);	
	cerrarCapturaPesoSuma();
}

function cerrarCapturaPesoSuma(){
	' + convert(nvarchar(max),NCHAR(36)) + N'("#dvPesadas").html("");
	TotalSuma = 0.000;
	cerrarVelo();
}

function finalizarPedido(confirmacion){
    if(!' + convert(nvarchar(max),NCHAR(36)) + N'(".btnFinalizarPedido img").hasClass("btnAlo")){ return; } 
    if(confirmacion){
        ' + convert(nvarchar(max),NCHAR(36)) + N'("#veloAvDesbloqueo").html(icoAlert50+"<br><br>finalizando...");
        var parametros = ''{"sp":"pPedidos","modo":"finalizarPedido","Empresa":"''+Empresa+''","Numero":"''+Numero+''","Serie":"''+Serie+''"}'';
        flexygo.nav.execProcess(''pMerlos'','''',null,null,[{''Key'':''parametros'',''Value'':limpiarCadena(parametros)}],''current'',false,' + convert(nvarchar(max),NCHAR(36)) + N'(this),function(ret){if(ret){
            ' + convert(nvarchar(max),NCHAR(36)) + N'(".tbSeccion").hide(); ' + convert(nvarchar(max),NCHAR(36)) + N'("#dvTrabajoInputs, #dvResultados").slideDown();
            cancelarPreparacion(); modificacionesCancelar(); cambiarArticuloCancelar(); buscarGlobal(); cerrarVelo();
        } else { alert(''Error SP pPedidos - finalizarPedido!''+JSON.stringify(ret)); }}, false);
    }else{
        abrirVelo(`
            <div id=''veloAvDesbloqueo'' class=''taC'' style=''font:16px arial; color:#333;''>
                `+icoAlert50+` <br><br>
                El pedido `+Numero+`-`+Serie+`<br>se marcará como TRASPASADO<br>en la base de datos
                <br><br><br><span class=''btnVerde esq05'' onclick=''finalizarPedido(1);''>aceptar</span>
                &nbsp;&nbsp;&nbsp;<span class=''btnRojo esq05'' onclick=''cerrarVelo();''>cancelar</span>
            </div>
        `);
    }
}
</script>
',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'noicon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,0,0,1)
) AS Source ([ModuleName],[TypeId],[ClassId],[ObjectName],[ObjectFilter],[Descrip],[Title],[ContainerId],[CollapsibleButton],[FullscreenButton],[RefreshButton],[SearchButton],[SQlSentence],[Header],[HTMLText],[Footer],[Empty],[CssText],[ScriptText],[ChartTypeId],[ChartSettingName],[Series],[Labels],[Value],[Params],[JsonOptions],[Path],[TransFormFilePath],[IconName],[PagerId],[PageSize],[ConnStringID],[ToolbarName],[GridbarName],[TemplateId],[HeaderClass],[ModuleClass],[JSAfterLoad],[Searcher],[ShowWhenNew],[ManualInit],[SchedulerName],[TimelineSettingName],[KanbanSettingsName],[ChartBackground],[ChartBorder],[Reserved],[Cache],[Offline],[PresetName],[MixedChartTypes],[MixedChartLabels],[ChartLineBorderDash],[ChartLineFill],[OriginId])
ON (Target.[ModuleName] = Source.[ModuleName])
WHEN MATCHED AND (
	NULLIF(Source.[TypeId], Target.[TypeId]) IS NOT NULL OR NULLIF(Target.[TypeId], Source.[TypeId]) IS NOT NULL OR 
	NULLIF(Source.[ClassId], Target.[ClassId]) IS NOT NULL OR NULLIF(Target.[ClassId], Source.[ClassId]) IS NOT NULL OR 
	NULLIF(Source.[ObjectName], Target.[ObjectName]) IS NOT NULL OR NULLIF(Target.[ObjectName], Source.[ObjectName]) IS NOT NULL OR 
	NULLIF(Source.[ObjectFilter], Target.[ObjectFilter]) IS NOT NULL OR NULLIF(Target.[ObjectFilter], Source.[ObjectFilter]) IS NOT NULL OR 
	NULLIF(Source.[Descrip], Target.[Descrip]) IS NOT NULL OR NULLIF(Target.[Descrip], Source.[Descrip]) IS NOT NULL OR 
	NULLIF(Source.[Title], Target.[Title]) IS NOT NULL OR NULLIF(Target.[Title], Source.[Title]) IS NOT NULL OR 
	NULLIF(Source.[ContainerId], Target.[ContainerId]) IS NOT NULL OR NULLIF(Target.[ContainerId], Source.[ContainerId]) IS NOT NULL OR 
	NULLIF(Source.[CollapsibleButton], Target.[CollapsibleButton]) IS NOT NULL OR NULLIF(Target.[CollapsibleButton], Source.[CollapsibleButton]) IS NOT NULL OR 
	NULLIF(Source.[FullscreenButton], Target.[FullscreenButton]) IS NOT NULL OR NULLIF(Target.[FullscreenButton], Source.[FullscreenButton]) IS NOT NULL OR 
	NULLIF(Source.[RefreshButton], Target.[RefreshButton]) IS NOT NULL OR NULLIF(Target.[RefreshButton], Source.[RefreshButton]) IS NOT NULL OR 
	NULLIF(Source.[SearchButton], Target.[SearchButton]) IS NOT NULL OR NULLIF(Target.[SearchButton], Source.[SearchButton]) IS NOT NULL OR 
	NULLIF(Source.[SQlSentence], Target.[SQlSentence]) IS NOT NULL OR NULLIF(Target.[SQlSentence], Source.[SQlSentence]) IS NOT NULL OR 
	NULLIF(Source.[Header], Target.[Header]) IS NOT NULL OR NULLIF(Target.[Header], Source.[Header]) IS NOT NULL OR 
	NULLIF(Source.[HTMLText], Target.[HTMLText]) IS NOT NULL OR NULLIF(Target.[HTMLText], Source.[HTMLText]) IS NOT NULL OR 
	NULLIF(Source.[Footer], Target.[Footer]) IS NOT NULL OR NULLIF(Target.[Footer], Source.[Footer]) IS NOT NULL OR 
	NULLIF(Source.[Empty], Target.[Empty]) IS NOT NULL OR NULLIF(Target.[Empty], Source.[Empty]) IS NOT NULL OR 
	NULLIF(Source.[CssText], Target.[CssText]) IS NOT NULL OR NULLIF(Target.[CssText], Source.[CssText]) IS NOT NULL OR 
	NULLIF(Source.[ScriptText], Target.[ScriptText]) IS NOT NULL OR NULLIF(Target.[ScriptText], Source.[ScriptText]) IS NOT NULL OR 
	NULLIF(Source.[ChartTypeId], Target.[ChartTypeId]) IS NOT NULL OR NULLIF(Target.[ChartTypeId], Source.[ChartTypeId]) IS NOT NULL OR 
	NULLIF(Source.[ChartSettingName], Target.[ChartSettingName]) IS NOT NULL OR NULLIF(Target.[ChartSettingName], Source.[ChartSettingName]) IS NOT NULL OR 
	NULLIF(Source.[Series], Target.[Series]) IS NOT NULL OR NULLIF(Target.[Series], Source.[Series]) IS NOT NULL OR 
	NULLIF(Source.[Labels], Target.[Labels]) IS NOT NULL OR NULLIF(Target.[Labels], Source.[Labels]) IS NOT NULL OR 
	NULLIF(Source.[Value], Target.[Value]) IS NOT NULL OR NULLIF(Target.[Value], Source.[Value]) IS NOT NULL OR 
	NULLIF(Source.[Params], Target.[Params]) IS NOT NULL OR NULLIF(Target.[Params], Source.[Params]) IS NOT NULL OR 
	NULLIF(Source.[JsonOptions], Target.[JsonOptions]) IS NOT NULL OR NULLIF(Target.[JsonOptions], Source.[JsonOptions]) IS NOT NULL OR 
	NULLIF(Source.[Path], Target.[Path]) IS NOT NULL OR NULLIF(Target.[Path], Source.[Path]) IS NOT NULL OR 
	NULLIF(Source.[TransFormFilePath], Target.[TransFormFilePath]) IS NOT NULL OR NULLIF(Target.[TransFormFilePath], Source.[TransFormFilePath]) IS NOT NULL OR 
	NULLIF(Source.[IconName], Target.[IconName]) IS NOT NULL OR NULLIF(Target.[IconName], Source.[IconName]) IS NOT NULL OR 
	NULLIF(Source.[PagerId], Target.[PagerId]) IS NOT NULL OR NULLIF(Target.[PagerId], Source.[PagerId]) IS NOT NULL OR 
	NULLIF(Source.[PageSize], Target.[PageSize]) IS NOT NULL OR NULLIF(Target.[PageSize], Source.[PageSize]) IS NOT NULL OR 
	NULLIF(Source.[ConnStringID], Target.[ConnStringID]) IS NOT NULL OR NULLIF(Target.[ConnStringID], Source.[ConnStringID]) IS NOT NULL OR 
	NULLIF(Source.[ToolbarName], Target.[ToolbarName]) IS NOT NULL OR NULLIF(Target.[ToolbarName], Source.[ToolbarName]) IS NOT NULL OR 
	NULLIF(Source.[GridbarName], Target.[GridbarName]) IS NOT NULL OR NULLIF(Target.[GridbarName], Source.[GridbarName]) IS NOT NULL OR 
	NULLIF(Source.[TemplateId], Target.[TemplateId]) IS NOT NULL OR NULLIF(Target.[TemplateId], Source.[TemplateId]) IS NOT NULL OR 
	NULLIF(Source.[HeaderClass], Target.[HeaderClass]) IS NOT NULL OR NULLIF(Target.[HeaderClass], Source.[HeaderClass]) IS NOT NULL OR 
	NULLIF(Source.[ModuleClass], Target.[ModuleClass]) IS NOT NULL OR NULLIF(Target.[ModuleClass], Source.[ModuleClass]) IS NOT NULL OR 
	NULLIF(Source.[JSAfterLoad], Target.[JSAfterLoad]) IS NOT NULL OR NULLIF(Target.[JSAfterLoad], Source.[JSAfterLoad]) IS NOT NULL OR 
	NULLIF(Source.[Searcher], Target.[Searcher]) IS NOT NULL OR NULLIF(Target.[Searcher], Source.[Searcher]) IS NOT NULL OR 
	NULLIF(Source.[ShowWhenNew], Target.[ShowWhenNew]) IS NOT NULL OR NULLIF(Target.[ShowWhenNew], Source.[ShowWhenNew]) IS NOT NULL OR 
	NULLIF(Source.[ManualInit], Target.[ManualInit]) IS NOT NULL OR NULLIF(Target.[ManualInit], Source.[ManualInit]) IS NOT NULL OR 
	NULLIF(Source.[SchedulerName], Target.[SchedulerName]) IS NOT NULL OR NULLIF(Target.[SchedulerName], Source.[SchedulerName]) IS NOT NULL OR 
	NULLIF(Source.[TimelineSettingName], Target.[TimelineSettingName]) IS NOT NULL OR NULLIF(Target.[TimelineSettingName], Source.[TimelineSettingName]) IS NOT NULL OR 
	NULLIF(Source.[KanbanSettingsName], Target.[KanbanSettingsName]) IS NOT NULL OR NULLIF(Target.[KanbanSettingsName], Source.[KanbanSettingsName]) IS NOT NULL OR 
	NULLIF(Source.[ChartBackground], Target.[ChartBackground]) IS NOT NULL OR NULLIF(Target.[ChartBackground], Source.[ChartBackground]) IS NOT NULL OR 
	NULLIF(Source.[ChartBorder], Target.[ChartBorder]) IS NOT NULL OR NULLIF(Target.[ChartBorder], Source.[ChartBorder]) IS NOT NULL OR 
	NULLIF(Source.[Reserved], Target.[Reserved]) IS NOT NULL OR NULLIF(Target.[Reserved], Source.[Reserved]) IS NOT NULL OR 
	NULLIF(Source.[Cache], Target.[Cache]) IS NOT NULL OR NULLIF(Target.[Cache], Source.[Cache]) IS NOT NULL OR 
	NULLIF(Source.[Offline], Target.[Offline]) IS NOT NULL OR NULLIF(Target.[Offline], Source.[Offline]) IS NOT NULL OR 
	NULLIF(Source.[PresetName], Target.[PresetName]) IS NOT NULL OR NULLIF(Target.[PresetName], Source.[PresetName]) IS NOT NULL OR 
	NULLIF(Source.[MixedChartTypes], Target.[MixedChartTypes]) IS NOT NULL OR NULLIF(Target.[MixedChartTypes], Source.[MixedChartTypes]) IS NOT NULL OR 
	NULLIF(Source.[MixedChartLabels], Target.[MixedChartLabels]) IS NOT NULL OR NULLIF(Target.[MixedChartLabels], Source.[MixedChartLabels]) IS NOT NULL OR 
	NULLIF(Source.[ChartLineBorderDash], Target.[ChartLineBorderDash]) IS NOT NULL OR NULLIF(Target.[ChartLineBorderDash], Source.[ChartLineBorderDash]) IS NOT NULL OR 
	NULLIF(Source.[ChartLineFill], Target.[ChartLineFill]) IS NOT NULL OR NULLIF(Target.[ChartLineFill], Source.[ChartLineFill]) IS NOT NULL OR 
	NULLIF(Source.[OriginId], Target.[OriginId]) IS NOT NULL OR NULLIF(Target.[OriginId], Source.[OriginId]) IS NOT NULL) THEN
 UPDATE SET
  [TypeId] = Source.[TypeId], 
  [ClassId] = Source.[ClassId], 
  [ObjectName] = Source.[ObjectName], 
  [ObjectFilter] = Source.[ObjectFilter], 
  [Descrip] = Source.[Descrip], 
  [Title] = Source.[Title], 
  [ContainerId] = Source.[ContainerId], 
  [CollapsibleButton] = Source.[CollapsibleButton], 
  [FullscreenButton] = Source.[FullscreenButton], 
  [RefreshButton] = Source.[RefreshButton], 
  [SearchButton] = Source.[SearchButton], 
  [SQlSentence] = Source.[SQlSentence], 
  [Header] = Source.[Header], 
  [HTMLText] = Source.[HTMLText], 
  [Footer] = Source.[Footer], 
  [Empty] = Source.[Empty], 
  [CssText] = Source.[CssText], 
  [ScriptText] = Source.[ScriptText], 
  [ChartTypeId] = Source.[ChartTypeId], 
  [ChartSettingName] = Source.[ChartSettingName], 
  [Series] = Source.[Series], 
  [Labels] = Source.[Labels], 
  [Value] = Source.[Value], 
  [Params] = Source.[Params], 
  [JsonOptions] = Source.[JsonOptions], 
  [Path] = Source.[Path], 
  [TransFormFilePath] = Source.[TransFormFilePath], 
  [IconName] = Source.[IconName], 
  [PagerId] = Source.[PagerId], 
  [PageSize] = Source.[PageSize], 
  [ConnStringID] = Source.[ConnStringID], 
  [ToolbarName] = Source.[ToolbarName], 
  [GridbarName] = Source.[GridbarName], 
  [TemplateId] = Source.[TemplateId], 
  [HeaderClass] = Source.[HeaderClass], 
  [ModuleClass] = Source.[ModuleClass], 
  [JSAfterLoad] = Source.[JSAfterLoad], 
  [Searcher] = Source.[Searcher], 
  [ShowWhenNew] = Source.[ShowWhenNew], 
  [ManualInit] = Source.[ManualInit], 
  [SchedulerName] = Source.[SchedulerName], 
  [TimelineSettingName] = Source.[TimelineSettingName], 
  [KanbanSettingsName] = Source.[KanbanSettingsName], 
  [ChartBackground] = Source.[ChartBackground], 
  [ChartBorder] = Source.[ChartBorder], 
  [Reserved] = Source.[Reserved], 
  [Cache] = Source.[Cache], 
  [Offline] = Source.[Offline], 
  [PresetName] = Source.[PresetName], 
  [MixedChartTypes] = Source.[MixedChartTypes], 
  [MixedChartLabels] = Source.[MixedChartLabels], 
  [ChartLineBorderDash] = Source.[ChartLineBorderDash], 
  [ChartLineFill] = Source.[ChartLineFill], 
  [OriginId] = Source.[OriginId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([ModuleName],[TypeId],[ClassId],[ObjectName],[ObjectFilter],[Descrip],[Title],[ContainerId],[CollapsibleButton],[FullscreenButton],[RefreshButton],[SearchButton],[SQlSentence],[Header],[HTMLText],[Footer],[Empty],[CssText],[ScriptText],[ChartTypeId],[ChartSettingName],[Series],[Labels],[Value],[Params],[JsonOptions],[Path],[TransFormFilePath],[IconName],[PagerId],[PageSize],[ConnStringID],[ToolbarName],[GridbarName],[TemplateId],[HeaderClass],[ModuleClass],[JSAfterLoad],[Searcher],[ShowWhenNew],[ManualInit],[SchedulerName],[TimelineSettingName],[KanbanSettingsName],[ChartBackground],[ChartBorder],[Reserved],[Cache],[Offline],[PresetName],[MixedChartTypes],[MixedChartLabels],[ChartLineBorderDash],[ChartLineFill],[OriginId])
 VALUES(Source.[ModuleName],Source.[TypeId],Source.[ClassId],Source.[ObjectName],Source.[ObjectFilter],Source.[Descrip],Source.[Title],Source.[ContainerId],Source.[CollapsibleButton],Source.[FullscreenButton],Source.[RefreshButton],Source.[SearchButton],Source.[SQlSentence],Source.[Header],Source.[HTMLText],Source.[Footer],Source.[Empty],Source.[CssText],Source.[ScriptText],Source.[ChartTypeId],Source.[ChartSettingName],Source.[Series],Source.[Labels],Source.[Value],Source.[Params],Source.[JsonOptions],Source.[Path],Source.[TransFormFilePath],Source.[IconName],Source.[PagerId],Source.[PageSize],Source.[ConnStringID],Source.[ToolbarName],Source.[GridbarName],Source.[TemplateId],Source.[HeaderClass],Source.[ModuleClass],Source.[JSAfterLoad],Source.[Searcher],Source.[ShowWhenNew],Source.[ManualInit],Source.[SchedulerName],Source.[TimelineSettingName],Source.[KanbanSettingsName],Source.[ChartBackground],Source.[ChartBorder],Source.[Reserved],Source.[Cache],Source.[Offline],Source.[PresetName],Source.[MixedChartTypes],Source.[MixedChartLabels],Source.[ChartLineBorderDash],Source.[ChartLineFill],Source.[OriginId])
WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = 1 THEN 
 DELETE
;
END TRY
BEGIN CATCH
    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT
    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();
    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE
END CATCH
GO





