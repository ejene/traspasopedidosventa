var PedidoActivo = "", NumPedido = "", PedidoSerie = "";
var Empresa = "01";
var Almacen = "00";
var Numero, Serie, Zona, Ruta, RutaNombre;
var Entrega = "";
var tecladoModo = "Buscar";
var ControlActivo, cntrl, ClaseActiva = "";
var gID, gPedido, gTraspaso;
var gArticulo, gArtCajas, gArtUnidades, gArtPeso, gArtServidas, gArtTraspaso;
var gLoteID, gLote, gLoteArticulo, gLoteArticuloUniCaja, gLoteCaducidad, gLoteUnidades, gLotePeso, gLotePesoArticulo;
var trsps_numero, trsps_serie, trsps_articulo, trsps_cajas, trsps_unidades, trsps_peso, trsps_cliente, trsps_ruta, trsps_nRuta, trsps_linea;
var ordenRuta = "entrega";
var imprimirAlbaran = 1;
var imprimirEtiquetas = 1;
var SinLoteStock = 1;
var basculaEnUso = 1;
var PedidoLineas = "";
var usuarioSesion = "";
var listadoArticulos = "";
var IntegridadVerificada = false;

var currentReference = flexygo.context.currentReference;
var currentRole = flexygo.context.currentRole;
var params = '"params":[{"usuario":"'+currentReference+'","rol":"'+currentRole+'"}]';

/* ICONOS */
var icoCarga50 = "<img src='./Merlos/images/carga.png' class='rotarR' style='vertical-align:middle;' width='50'>";
var icoCarga16 = "<img src='./Merlos/images/carga.png' class='rotarR' style='vertical-align:middle;' width='16'>";
var icoAlert50 = "<img src='./Merlos/images/alerta.png' width='50' />";

/* BOTONES */
var btnAceptarCerrarVelo = "<br><br><br><span class='btnMVerde' onclick='cerrarVelo();'>aceptar</span>";


var ElementosClickOff = [".dvSel", ".dvSubSel"];
$(document).click(function (e) {
    for (var i in ElementosClickOff) {
        var container = $(ElementosClickOff[i]);
        if (!container.is(e.target) && container.has(e.target).length === 0) { container.fadeOut(); $(".seccion").css("overflow","hidden"); }
    }
});


function abrirVelo(contenido, sinFondo, sinCerrar){
    var cldvVC = "dvVeloCont";
    if (sinFondo) { cldvVC = "dvVeloContSinFondo"; }
    if (!sinCerrar) {
        $("#dvVelo").remove();
        $("body").prepend('<div id="dvVelo" class="dvVelo inv"><div id="dvVeloCont" class="' + cldvVC + '">' + contenido + '</div></div>');
        $("#dvVelo").fadeIn();
    } else { $("#dvVeloCont").html(contenido); }
}
function cerrarVelo(){ $("#dvVelo").fadeOut(500,function(){ $("#dvVelo").remove(); }); }

function infoCarga(t) {
    return "<div style='vertical-align:middle; text-align:center;'>" + icoCarga16 + " <span style='vertical-align:middle; font:16px arial; color:#11698E;'>" + t +"...</span></div>";
}

function abrirVeloTeclado(contenido,sinFondo){
    var cldvVC = "dvVeloTecladoCont";
    if(sinFondo){ cldvVC="dvVeloContSinFondo";}
	$("#dvVeloTeclado").remove();
    $("body").prepend('<div id="dvVeloTeclado" class="dvVeloTeclado inv"><div id="dvVeloTecladoCont" class="'+cldvVC+'">'+contenido+'</div></div>');
	$("#dvVeloTeclado").fadeIn();
}
function cerrarVeloTeclado(){ $("#dvVeloTeclado").fadeOut(500,function(){ $("#dvVeloTeclado").remove(); }); }

function capitalizar(str) {  return str.charAt(0).toUpperCase() + str.slice(1); }

function Left(str, n) {
	if (n <= 0) return "";
	else if (n > String(str).length) return str;
	else return String(str).substring(0,n);
}
 
function Right(str,n){
    if (n <= 0) return "";
    else if (n > String(str).length) return str;
    else { var iLen = String(str).length; return String(str).substring(iLen,iLen-n); }
}

function soloNums(elID, elIDvalor) { $('#'+elID).val(elIDvalor.replace(/[^0-9]/g,"")); }

function ifNull(valor,siNull){
	if( typeof variable === 'undefined' || variable === null ){ return siNull; }
	else{ return valor; }
}

function limpiarCadena(cadena) {
    return cadena.replace(/\r/g, "").replace(/\n/g, "").replace(/\t/g, "").replace(/\b/g, "").replace(/\f/g, "").replace(/\\/g, "");
}


/*	
	***************
		FECHA
	***************
*/
var Fmeses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

function formatFecha(f) {
    var fecha = new Date(f);
    var nfM = fecha.getMonth() + 1; nfM = nfM < 10 ? "0" + (nfM) : nfM;
    var nfD = fecha.getDate(); nfD = nfD < 10 ? "0" + (nfD) : nfD;
    var laFecha = fecha.getFullYear() + "-" + nfM + "-" + nfD;
    return laFecha;
}

function formatFechaHora(f) {
    var fecha = new Date(f);
    var nfM = fecha.getMonth() + 1; nfM = nfM < 10 ? "0" + (nfM) : nfM;
    var nfD = fecha.getDate(); nfD = nfD < 10 ? "0" + (nfD) : nfD;
    var nfH = fecha.getHours(); nfH = nfH < 10 ? "0" + (nfH) : nfH;
    var nfi = fecha.getMinutes(); nfi = nfi < 10 ? "0" + (nfi) : nfi;
    var laFecha = fecha.getFullYear() + "-" + nfM + "-" + nfD + "T" + nfH + ":" + nfi;
    return laFecha;
}

function anyoActual() {
    var f = new Date();
    return f.getFullYear();
}

function fechaCorta(formato, separador="/"){
    var f = new Date();
    var dia = f.getDate();
    var mes = f.getMonth()+1;
    var any = f.getFullYear();
    var laFecha = "";

    dia = dia < 10 ? "0" + (dia):dia;
    mes = mes < 10 ? "0" + (mes):mes;

    if (formato === "amd") { laFecha = any + separador + mes + separador + dia; }
    else { laFecha = dia + separador + mes + separador + any; }
    return laFecha
}

function fechaLarga(){
    var f = new Date();
    var hora = f.getHours();
    var min  = f.getMinutes();
    var seg  = f.getSeconds();

    hora = hora < 10 ? "0" + (hora):hora;
    min  = min  < 10 ? "0" + (min):min;
    seg  = seg  < 10 ? "0" + (seg):seg;

    return fechaCorta() + " " + hora+":"+min+":"+seg;
}

function hora(){
    var f = new Date();
    var hora = f.getHours();
    var min  = f.getMinutes();
    var seg  = f.getSeconds();

    hora = hora < 10 ? "0" + (hora):hora;
    min  = min  < 10 ? "0" + (min):min;
    seg  = seg  < 10 ? "0" + (seg):seg;

    return  hora+":"+min+":"+seg;
}

function fechaDeHoy(){
    var f = new Date();
    return (f.getDate() + " de " + Fmeses[f.getMonth()] + " de " + f.getFullYear());
}

function fechaDeAyer(){
    var TuFecha = new Date();
    TuFecha.setDate(TuFecha.getDate() - 1);
    var dia = TuFecha.getDate();
    var mes = TuFecha.getMonth()+1;
    var any = TuFecha.getFullYear();
    if(dia.toString().length==1){ dia = "0"+dia;} 
    if(mes.toString().length==1){ mes = "0"+mes;}
    return dia+"/"+mes+"/"+any;
}

function fechaDeManyana(formato = "dma", simbolo = "-") {
    var TuFecha = new Date();
    TuFecha.setDate(TuFecha.getDate() + 1);
    var dia = TuFecha.getDate();
    var mes = TuFecha.getMonth() + 1;
    var ano = TuFecha.getFullYear();
    if (dia.toString().length == 1) { dia = "0" + dia; }
    if (mes.toString().length == 1) { mes = "0" + mes; }
    var laFecha = dia + simbolo + mes + simbolo + ano;
    if (formato === "amd") { laFecha = ano + simbolo + mes + simbolo + dia; }
    return laFecha
}

function fechaMasDias(dias=0, formato = "dma", simbolo = "-") {
    var TuFecha = new Date();
    TuFecha.setDate(TuFecha.getDate() + dias);
    var dia = TuFecha.getDate();
    var mes = TuFecha.getMonth() + 1;
    var ano = TuFecha.getFullYear();
    if (dia.toString().length == 1) { dia = "0" + dia; }
    if (mes.toString().length == 1) { mes = "0" + mes; }
    var laFecha = dia + simbolo + mes + simbolo + ano;
    if (formato === "amd") { laFecha = ano + simbolo + mes + simbolo + dia; }
    return laFecha
}

function fechaConDia(){
    var TuFecha = new Date();
    TuFecha.setDate(TuFecha.getDate() + 1);
    var dia = TuFecha.getDate();
    var mes = TuFecha.getMonth()+1;
    var any = TuFecha.getFullYear();
    if(dia.toString().length==1){ dia = "0"+dia;} 
    if(mes.toString().length==1){ mes = "0"+mes;}
    var dias=["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"];
    var dt = new Date(mes+' '+dia+', '+any);
    return dias[dt.getUTCDay()] + " " +(TuFecha.getDate() + " de " + Fmeses[TuFecha.getMonth()] + " de " + TuFecha.getFullYear());
}

function fechaNum(fecha) {
    var f = new Date(fecha);
    return f.getFullYear() + "" + (f.getMonth() + 1) + "" + f.getDate();
}

function PadLeft(value, length) {
    return (value.toString().length < length) ? PadLeft("0" + value, length) : value;
}
