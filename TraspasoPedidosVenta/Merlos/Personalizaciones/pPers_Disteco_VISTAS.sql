﻿

	declare @EJERCICIO char(4)	= (select Ejercicio from Configuracion)
	declare @GESTION char(6)	= (select Gestion from Configuracion)
	declare @COMUN char(8)		= (select Comun from Configuracion)
	declare @LOTES char(8)		= (select Lotes from Configuracion)
	declare @alterCreate varchar(max)
	
	declare @DISTRI char(8)		= ''+@DISTRI+''





IF EXISTS (SELECT * FROM sys.objects where name='d_Busca_PV') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +' view [dbo].[d_Busca_PV] as
SELECT distinct a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre as n_ruta, a.empresa,
		  ltrim(rtrim(b.nombre))+CASE WHEN (SUM(CASE WHEN ART.MARCA BETWEEN ''ETA'' AND ''ETN'' THEN 1 ELSE 0 END))!=0 AND A.LETRA=''ET'' THEN '' Gama ET'' ELSE '''' END as nombre
		, ltrim(rtrim(b.NOMBRE2)) as rComercial 
		, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra as EN_USO,
		MAX(DPV.SERVIDAS)+SUM(COALESCE(TP.UNIDADES,0.00)) AS servidas, isnull(TP.numero,0) as bNumero, ADI.FACTDIRV,
		e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, a.almacen, MAX(B.IDIOMA_IMP) AS IDIOMA_IMP
FROM ['+@GESTION+'].[dbo].[c_pedive] a
inner join ['+@GESTION+'].[dbo].[clientes] b on a.cliente=b.codigo
left  join ['+@GESTION+'].[dbo].rutas d on d.codigo=a.ruta
left  join ['+@DISTRI+'].[dbo].adiclient AdI on AdI.CLIENTE=A.cliente 
left  join Traspaso_Temporal TP on  TP.numero COLLATE Modern_Spanish_CI_AI =a.numero COLLATE Modern_Spanish_CI_AI
left  join ['+@COMUN+'].[DBO].[en_uso] e on RTRIM(LTRIM(e.TIPO))=''PEDIVEN'' and e.CLAVE=cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra
left join ['+@GESTION+'].dbo.d_pedive dpv on dpv.empresa=a.empresa and dpv.numero=a.numero and dpv.letra=a.letra and dpv.articulo!=''''
LEFT JOIN ['+@GESTION+'].dbo.articulo ART ON ART.CODIGO=dpv.ARTICULO
where a.traspasado=0 and a.cancelado=0 and a.TOTALPED>=0.00
GROUP BY a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre, a.empresa, b.nombre, b.NOMBRE2
, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra, isnull(TP.numero,0), ADI.FACTDIRV,	e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, a.almacen
')
select 'd_Busca_PV'




IF EXISTS (SELECT * FROM sys.objects where name='d_Busca_PV_Zona') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +' view [dbo].[d_Busca_PV_Zona] as
SELECT distinct a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre as n_ruta, a.empresa,
		  ltrim(rtrim(b.nombre))+CASE WHEN (SUM(CASE WHEN ART.MARCA BETWEEN ''ETA'' AND ''ETN'' THEN 1 ELSE 0 END))!=0 AND A.LETRA=''ET'' THEN '' Gama ET'' ELSE '''' END as nombre
		, ltrim(rtrim(b.NOMBRE2)) as rComercial 
		, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra as EN_USO,
		MAX(DPV.SERVIDAS)+SUM(COALESCE(TP.UNIDADES,0.00)) AS servidas, isnull(TP.numero,0) as bNumero, ADI.FACTDIRV,
		e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, a.almacen, MAX(B.IDIOMA_IMP) AS IDIOMA_IMP, COALESCE(UPPER(M.VALOR),'''') AS ZONA
FROM ['+@GESTION+'].[dbo].[c_pedive] a
inner join ['+@GESTION+'].[dbo].[clientes] b on a.cliente=b.codigo
left join ['+@GESTION+'].[dbo].rutas d on d.codigo=a.ruta
left join ['+@DISTRI+'].[dbo].adiclient AdI on AdI.CLIENTE=A.cliente 
left join Traspaso_Temporal TP on  TP.numero COLLATE Modern_Spanish_CI_AI =a.numero COLLATE Modern_Spanish_CI_AI
left join ['+@COMUN+'].[DBO].[en_uso] e on RTRIM(LTRIM(e.TIPO))=''PEDIVEN'' and e.CLAVE=cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra
left join ['+@GESTION+'].dbo.d_pedive dpv on dpv.empresa=a.empresa and dpv.numero=a.numero and dpv.letra=a.letra and dpv.articulo!=''''
left join ['+@GESTION+'].dbo.multicam m on m.codigo=dpv.articulo and m.campo=''ZNA''
left join ['+@GESTION+'].dbo.articulo art on art.codigo=dpv.articulo
where a.traspasado=0 and a.cancelado=0 /*and a.entrega<=getdate()+1*/ and a.TOTALPED>=0.00
--AND LTRIM(A.NUMERO)=''MERLOS''
GROUP BY  a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre, a.empresa, b.nombre, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra, isnull(TP.numero,0), ADI.FACTDIRV
		, e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, UPPER(M.VALOR), a.almacen, b.nombre2
')
select 'd_Busca_PV_Zona'




IF EXISTS (SELECT * FROM sys.objects where name='d_Busca_PV_DI') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +' view [dbo].[d_Busca_PV_DI] as
SELECT distinct a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre as n_ruta, a.empresa,
		b.nombre, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra as EN_USO,
		MAX(DPV.SERVIDAS)+SUM(COALESCE(TP.UNIDADES,0.00)) AS servidas, isnull(TP.numero,0) as bNumero, ADI.FACTDIRV,
		e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, a.almacen, MAX(B.IDIOMA_IMP) AS IDIOMA_IMP
FROM ['+@GESTION+'].[dbo].[c_pedive] a
inner join ['+@GESTION+'].[dbo].[clientes] b on a.cliente=b.codigo
left  join ['+@GESTION+'].[dbo].rutas d on d.codigo=a.ruta
left  join ['+@DISTRI+'].[dbo].adiclient AdI on AdI.CLIENTE=A.cliente 
left  join Traspaso_Temporal TP on  TP.numero COLLATE Modern_Spanish_CI_AI =a.numero COLLATE Modern_Spanish_CI_AI
left join ['+@GESTION+'].dbo.d_pedive dpv on dpv.empresa=a.empresa and dpv.numero=a.numero and dpv.letra=a.letra and dpv.articulo!=''''
left  join ['+@COMUN+'].[DBO].[en_uso] e on RTRIM(LTRIM(e.TIPO))=''PEDIVEN'' and e.CLAVE=cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra
where a.traspasado=0 and a.cancelado=0 and a.entrega<=getdate()+4 and a.TOTALPED>=0.00
--AND LTRIM(A.NUMERO)=''MERLOS''
GROUP BY  a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre, a.empresa, b.nombre, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra
		, isnull(TP.numero,0), ADI.FACTDIRV,	e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, a.almacen
')
select 'd_Busca_PV_DI'

