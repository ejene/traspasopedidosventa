﻿CREATE OR ALTER PROCEDURE [dbo].[pAlbaranDeVenta] (@parametros varchar(max))
AS
BEGIN TRY
BEGIN TRANSACTION PAV
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare   @numero varchar(20)	= (select JSON_VALUE(@parametros,'$.numero'))
			, @letra char(2)		= (select JSON_VALUE(@parametros,'$.letra'))
			, @bultos varchar(5)	= (select JSON_VALUE(@parametros,'$.bultos'))
			, @cajas varchar(5)		= (select JSON_VALUE(@parametros,'$.cajas'))
			, @impAlb int			= isnull(cast((select JSON_VALUE(@parametros,'$.impAlb')) as int),0)
			, @impEti int			= isnull(cast((select JSON_VALUE(@parametros,'$.impEti')) as int),0)
			, @FacturaDirecta int	= isnull(cast((select JSON_VALUE(@parametros,'$.FacturaDirecta')) as int),0)
			, @usuarioSesion varchar(50)= (select JSON_VALUE(@parametros,'$.usuarioSesion'))			

	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EMPRESA char(2), @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select @EMPRESA=Empresa, @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------

	declare @sql varchar(max)
	declare @nu varchar(50) = replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 

	declare   @nEti int = 1
			, @CrearAV varchar(20) = ''
			, @printerEti varchar(100) = ''
			, @printerDoc varchar(100) = ''
			, @reportEti  varchar(100) = ''
			, @reportAlb  varchar(100) = ''
			, @reportFac  varchar(100) = ''
			, @msj varchar(max) = ''
			, @unico varchar(50) = ''
			, @elPDF varchar(100) = ''
			, @error int = 0
			, @cliente varchar(20)
			, @copiasAlb int = 1
			, @copiasFac int = 1	
			, @orientacion char(1) = 'V'

	update [Traspaso_Temporal] set bultos=@bultos, eticajas=@cajas, impAlb=@impAlb, impEti=@impEti where empresa=@EMPRESA and NUMERO=@numero and LETRA=@letra	

	-- Equipo del Usuario -- (DISTECO)
	declare @EquipoUsuario varchar(50) = 'WebApp'
	if @usuarioSesion='1544458254550078' set @EquipoUsuario='PC-CAMARA2'
	if @usuarioSesion='1544458275396522' set @EquipoUsuario='DS4JX7688'

	-- obtener impresoras y reports ---------------------------------------------------------------------------------
	if @impEti=1 begin
		set @printerEti = (select isnull(impEti,'') from [usuarios_impresoras] where usuario=@usuario)		
		set @reportEti  = (select isnull(reportEtiquetas,'') from [usuarios_reports] where usuario=@usuario)
		if @reportEti='' set @reportEti = (select isnull(report,'') from [ConfigIdiomaReport] where idioma = 
											(select top 1 IDIOMA_IMP from d_Busca_PV 
											where concat(empresa,numero,letra) collate Modern_Spanish_CI_AI=concat(@EMPRESA,@numero,@letra)) collate Modern_Spanish_CI_AI
											and LEFT(tipo,2)='03'
										  )
		if @reportEti='' set @reportEti = (select isnull(report,'') from [ConfigIdiomaReport] where idioma='000' and LEFT(tipo,2)='03')
		if  @printerEti='' or @reportEti='' set @error=1
	end
	
	if @impAlb=1 begin
		set @printerDoc = (select impDocu from [usuarios_impresoras] where usuario=@usuario)
		set @reportAlb  = (select reportAlbaran from [usuarios_reports] where usuario=@usuario)
		if	@reportAlb='' set @reportAlb = (select isnull(report,'') from [ConfigIdiomaReport] where idioma = 
											(select top 1 IDIOMA_IMP from d_Busca_PV 
											where concat(empresa,numero,letra) collate Modern_Spanish_CI_AI=concat(@EMPRESA,@numero,@letra)) collate Modern_Spanish_CI_AI
											and LEFT(tipo,2)='01'
										  )
		if @reportAlb='' set @reportAlb = (select isnull(report,'') from [ConfigIdiomaReport] where idioma='000' and LEFT(tipo,2)='01')			
		if @printerDoc='' or @reportAlb=''set @error=2
	end
	
	if @FacturaDirecta=1 begin
		set @printerDoc = (select impDocu from [usuarios_impresoras] where usuario=@usuario)
		set @reportFac  = (select reportFactura from [usuarios_reports] where usuario=@usuario)
		if	@reportFac='' set @reportFac = (select isnull(report,'') from [ConfigIdiomaReport] where idioma = 
											(select top 1 IDIOMA_IMP from d_Busca_PV 
											where concat(empresa,numero,letra) collate Modern_Spanish_CI_AI=concat(@EMPRESA,@numero,@letra)) collate Modern_Spanish_CI_AI
											and LEFT(tipo,2)='02'
										  )
		if @reportFac='' set @reportFac = (select isnull(report,'') from [ConfigIdiomaReport] where idioma='000' and LEFT(tipo,2)='02')
				
		/* -- DISTECO -- si usuario es DIx - la factura toma el papel de la bandeja de papel preimpreso */ 
		if @usuario='DI1' and @reportFac='FACTUVEN_PDH.rpt' set @printerDoc='HP (FACTURAS)' 
		if @usuario='DI2' and @reportFac='FACTUVEN_PDH.rpt' set @printerDoc='(FV) HP E60155 (FACTURAS)' 
		if @usuario='DI3' and @reportFac='FACTUVEN_PDH.rpt' set @printerDoc='(FV) HP LaserJet M406-M407' 

		if  @printerDoc='' or @reportFac=''set @error=3
	end

	-- --------------------------------------------------------------------------------------------------------------
	--	generar albarán de venta 
		declare @pRet int
		EXEC @pRet = [pCrearAV] @EMPRESA, @numero, @letra, @EquipoUsuario, @impAlb, @impEti, @CrearAV output, @nEti
		if @pRet<>-1 BEGIN 			
			select CONCAT('{"Error":"[pCrearAV] ha devuelto ',@pRet,' !"}') as JAVASCRIPT 
			BEGIN TRY ROLLBACK TRANSACTION PAV END TRY BEGIN CATCH END CATCH
			return -1 
		END
	-- --------------------------------------------------------------------------------------------------------------
	/*  
	-- Personalización DISTECO
	-- tabla temporal para recopilación de datos --------------------------------------------------------------------
	declare @tablaTemp TABLE (C_ALBAVEN int, copiasFac int)
	SET @sql = 'select C_ALBAVEN as C_ALBAVEN
				, (select COPIA_FRA from ['+@GESTION+'].dbo.clientes where CODIGO='''+@cliente+''') as copiasFac
				from ['+@GESTION+'].dbo.factucnf'
	INSERT @tablaTemp EXEC(@sql)	
	SELECT @copiasAlb=C_ALBAVEN, @copiasFac=copiasFac FROM @tablaTemp
	delete @tablaTemp

	-- DISTECO -- los usuarios ET imprimirán como ET1
	if left(@usuario,2)='ET' set @usuario='ET1'

	if @error=0 begin		
		declare @report int = 0

		-- 05-01-2022 - E. Jené - los usuarios ET imprimirán como ET1
		if left(@usuario,2)='ET' set @usuario='ET1'

		if @impEti=1 or (select activo from ConfigApp where objeto='impEtiquetas')=1 begin
			set @unico = 'E' + replace(replace(replace(replace(replace(convert(varchar(50),getdate(),121),' ',''),'/',''),':',''),'.',''),'-','')
			set @elPDF = replace(CONCAT('Etiquetas_',@empresa,@letra,@CrearAV,'.pdf'),' ','_');
			insert into MI_ImpresionPDF (id, usuario, terminal, tipo, pdf, impresora, orientacion, crReport, crNumero, crEmpresa, crLetra, bultos,report,copias) 
			values (@unico,@usuario,'WebApp','Etiquetas',@elPDF,@printerEti,@orientacion,@reportEti,ltrim(rtrim(@CrearAV)),@empresa,@letra,@bultos,@report,@cajas)
		end

		--	04-05-2021  -  la serie DI no imprime albaranes
			if ltrim(rtrim(@letra))='DI' set @report=1 
		-----------------------------------------------------------

		if @FacturaDirecta<>1 and (@impAlb=1 or (select activo from ConfigApp where objeto='impAlbGen')=1) begin 
			set @copiasAlb = (select isnull(C_ALBAVEN,0)+1 from [2022YB].dbo.factucnf where EMPRESA=@empresa)
			set @unico ='A' +  replace(replace(replace(replace(replace(convert(varchar(50),getdate(),121),' ',''),'/',''),':',''),'.',''),'-','')
			set @elPDF = replace(CONCAT('Albaran',@empresa,@letra,@CrearAV,'.pdf'),' ','_');
			if @reportAlb='ALBVEN_MER.rpt' BEGIN set @orientacion='H'END else BEGIN set @orientacion='V' END
			insert into MI_ImpresionPDF (id, usuario, terminal, tipo, pdf, impresora, orientacion, crReport, crNumero, crEmpresa, crLetra, report, copias) 
			values (@unico,@usuario,'WebApp','Albaran',@elPDF,@printerDoc,@orientacion,@reportAlb,ltrim(rtrim(@CrearAV)),@empresa,@letra,@report,@copiasAlb)
		end

		if @FacturaDirecta=1 begin
			set @cliente = (select CLIENTE from vFacturas where empresa=@empresa and numfra=CONCAT(@letra,@numero))
			set @copiasFac = (select isnull(COPIA_FRA,1) + 2 from [2022YB].dbo.clientes where CODIGO=@cliente)
			if @copiasFac is null or @copiasFac='' set @copiasFac=2

			set @unico = 'F' + replace(replace(replace(replace(replace(convert(varchar(50),getdate(),121),' ',''),'/',''),':',''),'.',''),'-','')
			set @elPDF = replace(CONCAT('Factura',@empresa,@letra,@CrearAV,'.pdf'),' ','_');
			if @reportFac='FACTUVEN_PDH.rpt' BEGIN set @orientacion='H' END else BEGIN set @orientacion='V' END
			insert into MI_ImpresionPDF (id, usuario, terminal, tipo, pdf, impresora, orientacion, crReport, crNumero, crEmpresa, crLetra, report, copias) 
			values (@unico,@usuario,'WebApp','Factura',@elPDF,@printerDoc,@orientacion,@reportFac,@CrearAV,@empresa,@letra,@report,@copiasFac)
		end
		
		-- Marcar pedido como Traspasado
		EXEC('
			update ['+@GESTION+'].dbo.c_pedive set TRASPASADO=1 
			from ['+@GESTION+'].dbo.c_pedive p 
			inner join [Traspasar_Pedido] t on ltrim(Rtrim(t.empresa)) collate database_default=ltrim(Rtrim(p.empresa)) 
			and ltrim(Rtrim(t.letra)) collate database_default=ltrim(Rtrim(p.letra)) and ltrim(Rtrim(t.numero)) collate database_default=ltrim(Rtrim(p.numero))
			where p.empresa='''+@empresa+''' and p.letra='''+@letra+''' and p.numero='''+@numero+'''
		')

		delete [Traspasar_Pedido] where empresa=@empresa and letra=@letra and numero=@numero
	end
	*/
	-- marcar impreso en c_albven 
	EXEC('update ['+@GESTION+'].dbo.c_albven set IMPRESO=1 where EMPRESA='''+@empresa+''' AND NUMERO='''+@numero+''' AND LETRA='''+@letra+'''')

	-- devolver número albarán generado
	select CONCAT('{"Error":"","respuesta":"',@CrearAV,'"}') as JAVASCRIPT

	COMMIT TRANSACTION PAV
	RETURN -1
END TRY
BEGIN CATCH	
	IF @@TRANCOUNT >0 BEGIN ROLLBACK TRANSACTION PAV END
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
	RETURN 0
END CATCH