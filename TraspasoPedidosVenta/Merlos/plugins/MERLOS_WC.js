﻿class merlosCalendar extends HTMLElement{
	constructor(){
		super();

		this.id;
		this.name;
		this.class;

		if(this.id===undefined || this.id===null || this.id===""){this.id="merlosCalendar_inputDate";}
		if(this.name===undefined || this.name===null || this.name===""){this.name=this.id;}
	}
	
	connectedCallback(){
		this.innerHTML = `<div style="display:inline-block; position:relative; width:100%; ">
							<input type="text" id="${this.id}-input"  name="${this.name}-input" class="merlos-calendar-input ${this.id}" 
								style="width:100%; text-align:center;"
								onkeyup="merlosCalendar_keyUp(this.id,this.value);" 
								onclick="merlosCalendar_mostrarMerlosCalendar(this.id); event.stopPropagation();" />
							<div id="dv_merlosCalendar_Calendar" 
								style="z-index:10; position:absolute; background:#f2f2f2; padding:10px; 
								-webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);
								-moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);
								box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.2);
								display:none;">
							</div>
						</div>`;
	}
}

window.customElements.define("merlos-calendar", merlosCalendar);

/*	
	******************
		CALENDARIO
	******************
*/
var merlosCalendar_calendarioInput = "";
var merlosCalendar_calendarioFormato = "";
var merlosCalendar_actual = new Date();
function merlosCalendar_mostrarCalendario(year, month) {
    var now = new Date(year, month - 1, 1);
    var last = new Date(year, month, 0);
    var primerDiaSemana = (now.getDay() == 0) ? 7 : now.getDay();
    var ultimoDiaMes = last.getDate();
    var dia = 0;
    var merlosCalendar_Contenido = `
    <table style="border-spacing:5px;">
        <tr>
            <td id="td_merlosCalendar_Cabecera" colspan="7"></td>
        </tr>
        <tr>
            <td colspan="7" style="height:12px;"></td>
        </tr>
        <tr>
            <td style="text-align:center;font:10px arial; color:#666;">Lu</td>
            <td style="text-align:center;font:10px arial; color:#666;">Ma</td>
            <td style="text-align:center;font:10px arial; color:#666;">Mi</td>
            <td style="text-align:center;font:10px arial; color:#666;">Ju</td>
            <td style="text-align:center;font:10px arial; color:#666;">Vi</td>
            <td style="text-align:center;font:10px arial; color:#666;">Sá</td>
            <td style="text-align:center;font:10px arial; color:#FC7272;">Do</td>
        </tr>
        <tr>`;
    var diaActual = 0;

    var last_cell = primerDiaSemana + ultimoDiaMes;

    var meses = Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

    // hacemos un bucle hasta 42, que es el máximo de valores que puede
    // haber... 6 columnas de 7 dias
    for (var i = 1; i <= 42; i++) {
        if (i == primerDiaSemana) {
            // determinamos en que dia empieza
            dia = 1;
        }
        if (i < primerDiaSemana || i >= last_cell) {
            // celda vacia
            merlosCalendar_Contenido += "<td>&nbsp;</td>";
        } else {
            // mostramos el dia
            var colorDia = "";
            if (dia == merlosCalendar_actual.getDate() && month == merlosCalendar_actual.getMonth() + 1 && year == merlosCalendar_actual.getFullYear()){ colorDia = "background:#ACFDAE;"; }
            merlosCalendar_Contenido += `<td style='border:1px solid #CCC; padding:8px; text-align:center; `+colorDia+` ' 
            onmouseover="this.style.cursor='pointer'; this.style.background='#FFF'; this.style.color='#1A8300';" 
            onmouseout="this.style.cursor='none'; this.style.background='#f2f2f2';this.style.color='#666';"
            onclick='merlosCalendar_asignarFecha(` + dia + `,` + month + `,` + year + `)'>` + dia + `</td>`;
            dia++;
        }
        if (i % 7 == 0) {
            if (dia > ultimoDiaMes)
                break;
            merlosCalendar_Contenido += "</tr><tr>\n";
        }
    }
    merlosCalendar_Contenido += "</tr></table>";

    // Calculamos el siguiente mes y año
    nextMonth = month + 1;
    nextYear = year;
    if (month + 1 > 12) {
        nextMonth = 1;
        nextYear = year + 1;
    }

    // Calculamos el anterior mes y año
    prevMonth = month - 1;
    prevYear = year;
    if (month - 1 < 1) {
        prevMonth = 12;
        prevYear = year - 1;
    }

    document.getElementById("dv_merlosCalendar_Calendar").innerHTML = merlosCalendar_Contenido;
    document.getElementById("td_merlosCalendar_Cabecera").innerHTML =
        `<table>
			<tr>
				<td style="white-space: nowrap; font:bold 14px arial; color:#666;">`+ meses[month - 1] + ` / ` + year + `</td>
        		<td style="width:auto; text-align:right;">
                    <span style='cursor:pointer; font:bold 18px arial; color:#11698E;' 
                        onmouseover="this.style.cursor='pointer'; this.style.color='#16C79A';" 
                        onmouseout="this.style.cursor='none'; this.style.color='#11698E';" 
                        onclick='merlosCalendar_mostrarCalendario(` + prevYear + `,` + prevMonth + `); event.stopPropagation();'>&#x2b9c;</span>
                    <span style="margin-left:10px;"></span>
				    <span style='cursor:pointer; font:bold 18px arial; color:#11698E;' 
                        onmouseover="this.style.cursor='pointer'; this.style.color='#16C79A';" 
                        onmouseout="this.style.cursor='none'; this.style.color='#11698E';" 
                        onclick='merlosCalendar_mostrarCalendario(` + nextYear + `,` + nextMonth + `); event.stopPropagation();'>&#x2b9e;</span>
                </td>
			</tr>
        </table>`;
}

function merlosCalendar_keyUp(elInput, valor, formato = "-") {
    $("#" + elInput).val(valor.replace(/[^0-9]/g, ""));
    var vvv = $("#" + elInput).val();
    var dia = vvv.substring(0,2);
    var mes = vvv.substring(2,4);
    var ano = vvv.substring(4,8);
    if (vvv.length > 2) { $("#" + elInput).val(dia + formato + mes); }
    if (vvv.length > 4) { $("#" + elInput).val(dia + formato + mes + formato + ano); }
}

function merlosCalendar_asignarFecha(d, m, a) {
    var fechaDMA = PadLeft(d, 2) + "-" + PadLeft(m, 2) + "-" + a;
    var fechaAMD = a + "-" + PadLeft(m, 2) + "-" + PadLeft(d, 2);
    var calendarioDevolverFecha = fechaDMA; console.log("fechaDMA: "+fechaDMA);
    if (merlosCalendar_calendarioFormato === "amd") { calendarioDevolverFecha = fechaAMD; }
    $("#" + merlosCalendar_calendarioInput).val(calendarioDevolverFecha);
    $("#dv_merlosCalendar_Calendar").slideUp();
}

function merlosCalendar_mostrarMerlosCalendar(elInput, formato) {
    merlosCalendar_calendarioInput = elInput;
    merlosCalendar_calendarioFormato = formato;
    merlosCalendar_mostrarCalendario(merlosCalendar_actual.getFullYear(), merlosCalendar_actual.getMonth() + 1);
    $("#dv_merlosCalendar_Calendar").slideDown();
}

function PadLeft(value, length) {
    return (value.toString().length < length) ? PadLeft("0" + value, length) :
        value;
}

var MerlosCalendarElementosClickOff = ["#dv_merlosCalendar_Calendar"];

$(document).click(function (e) {
    for (var i in MerlosCalendarElementosClickOff) {
        var container = $(MerlosCalendarElementosClickOff[i]);
        if (!container.is(e.target) && container.has(e.target).length === 0) { $(MerlosCalendarElementosClickOff[i]).fadeOut(); }
    }
});