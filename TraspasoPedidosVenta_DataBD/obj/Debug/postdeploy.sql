﻿SET NUMERIC_ROUNDABORT OFF
GO
SET XACT_ABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

BEGIN TRANSACTION

EXEC sp_MSforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT all'

EXEC sp_MSforeachtable 'ALTER TABLE ? DISABLE TRIGGER all'

-- Data merge


-- Merlos
truncate table [AuxiliarBultos]
insert into [AuxiliarBultos] (NumBultos)
values (1)
		,(2),(2)
		,(3),(3),(3)
		,(4),(4),(4),(4)
		,(5),(5),(5),(5),(5)
		,(6),(6),(6),(6),(6),(6)
		,(7),(7),(7),(7),(7),(7),(7)
		,(8),(8),(8),(8),(8),(8),(8),(8)
		,(9),(9),(9),(9),(9),(9),(9),(9),(9)
		,(10),(10),(10),(10),(10),(10),(10),(10),(10),(10)


if not exists (select objeto from [ConfigApp] where objeto='FactCajas')		insert into [ConfigApp] (objeto, valor, activo)	values ('FactCajas','',0)
if not exists (select objeto from [ConfigApp] where objeto='ImpAlbFact')	insert into [ConfigApp] (objeto, valor, activo)	values ('ImpAlbFact','',0)
if not exists (select objeto from [ConfigApp] where objeto='ImpAlbGen')		insert into [ConfigApp] (objeto, valor, activo)	values ('ImpAlbGen','',0)
if not exists (select objeto from [ConfigApp] where objeto='ImpEtiquetas')	insert into [ConfigApp] (objeto, valor, activo)	values ('ImpEtiquetas','',0)
if not exists (select objeto from [ConfigApp] where objeto='ResLin')		insert into [ConfigApp] (objeto, valor, activo)	values ('ResLin','',0)
if not exists (select objeto from [ConfigApp] where objeto='SeriesFiltro')	insert into [ConfigApp] (objeto, valor, activo)	values ('SeriesFiltro','',0)
	

if not exists (select email from [ConfigEmail])	
insert into [ConfigEmail] (email, smtp, puerto, usuario, pswrd, tls)
values ('noreply@cliseller.com','smtp.ionos.es','587','noreply@cliseller.com','4N6Brt6IH4DKNTYzZfE@',1)


if not exists (select * from [Idiomas_Idiomas] where nombre='Por Defecto') insert into [Idiomas_Idiomas] (Codigo, Nombre) values ('000','Por Defecto')


if not exists (select * from [Idiomas_Tipos] where nombre='Albarán')	insert into [Idiomas_Tipos] (Codigo, Nombre) values ('01','Albarán')
if not exists (select * from [Idiomas_Tipos] where nombre='Factura')	insert into [Idiomas_Tipos] (Codigo, Nombre) values ('02','Factura')
if not exists (select * from [Idiomas_Tipos] where nombre='Etiquetas')	insert into [Idiomas_Tipos] (Codigo, Nombre) values ('03','Etiquetas')


if not exists (select * from [ConfigPersTipo] where Nombre='Vista')	insert into [ConfigPersTipo] (Nombre) values ('Vista')
if not exists (select * from [ConfigPersTipo] where Nombre='Stored Procedure')	insert into [ConfigPersTipo] (Nombre) values ('Stored Procedure')


GO

EXEC sp_MSforeachtable 'ALTER TABLE ? ENABLE TRIGGER all'

EXEC sp_MSforeachtable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT ALL'

COMMIT TRANSACTION

GO
