﻿CREATE TABLE [dbo].[usuarios_filtros](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[usuario] [varchar](50) NULL,
	[campo] [varchar](50) NULL,
	[orden] [varchar](4) NULL,
	[FechaInsertUpdate] [datetime] NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[usuarios_filtros] ADD  CONSTRAINT [DF_usuarios_filtros_FechaInsertUpdate]  DEFAULT (getdate()) FOR [FechaInsertUpdate]
GO


