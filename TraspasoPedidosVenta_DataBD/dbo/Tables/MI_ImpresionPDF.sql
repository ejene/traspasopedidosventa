﻿CREATE TABLE [dbo].[MI_ImpresionPDF](
	[id] [varchar](50) NOT NULL,
	[usuario] [varchar](50) NULL,
	[terminal] [varchar](100) NULL,
	[tipo] [varchar](50) NULL,
	[pdf] [varchar](250) NULL,
	[impresora] [varchar](250) NULL,
	[crReport] [varchar](250) NULL,
	[crNumero] [varchar](50) NULL,
	[crEmpresa] [varchar](10) NULL,
	[crLetra] [varchar](10) NULL,
	[bultos] [int] NULL,
	[report] [int] NULL,
	[copias] [int] NULL,
	[orientacion] [char](1) NULL,
	[log] [nvarchar](max) NULL,
	[fechaInsertUpdate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[MI_ImpresionPDF] ADD  DEFAULT ((1)) FOR [bultos]
GO

ALTER TABLE [dbo].[MI_ImpresionPDF] ADD  DEFAULT ((0)) FOR [report]
GO

ALTER TABLE [dbo].[MI_ImpresionPDF] ADD  DEFAULT ((1)) FOR [copias]
GO

ALTER TABLE [dbo].[MI_ImpresionPDF] ADD  DEFAULT ('V') FOR [orientacion]
GO

ALTER TABLE [dbo].[MI_ImpresionPDF] ADD  DEFAULT (getdate()) FOR [fechaInsertUpdate]
GO

