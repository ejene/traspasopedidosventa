﻿CREATE TABLE [dbo].[Idiomas_Tipos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [varchar](50) NULL,
	[Nombre] [varchar](100) NULL,
	[FechaInsertUpdate] [datetime] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Idiomas_Tipos] ADD  CONSTRAINT [DF_Idiomas_Tipos_FechaInsertUpdate]  DEFAULT (getdate()) FOR [FechaInsertUpdate]
GO

