﻿CREATE TABLE [dbo].[ConfigApp](
	[objeto] [varchar](50) NOT NULL,
	[valor] [varchar](max) NULL,
	[activo] [tinyint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[ConfigApp] ADD  DEFAULT ((0)) FOR [activo]
GO

