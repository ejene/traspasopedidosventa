﻿CREATE TABLE [dbo].[basculas](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[bascula] [varchar](50) NULL,
	[tipoConexion] [varchar](50) NOT NULL DEFAULT ('ETHERNET'),
	[ip] [varchar](50) NULL DEFAULT ('192.168.0.101'),
	[puerto] [varchar](50) NULL DEFAULT ((6000)),
	[sentencia] [varchar](100) NULL DEFAULT (('$')),
	[activa] [bit] NOT NULL DEFAULT ((0)),
	[Baudios] [varchar](50) NULL,
	[Paridad] [varchar](50) NULL,
	[Data] [varchar](50) NULL,
	[Stop] [varchar](50) NULL,
	[Xon] [varchar](50) NULL,
	[Peso] [varchar](50) NULL,
	[FechaInsertUpdate] [datetime] NOT NULL DEFAULT (getdate())
) ON [PRIMARY]
GO