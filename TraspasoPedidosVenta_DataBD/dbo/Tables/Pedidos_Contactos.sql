﻿CREATE TABLE [dbo].[Pedidos_Contactos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[IDPEDIDO] [varchar](18) NULL,
	[Cliente] [char](8) NULL,
	[Contacto] [int] NULL,
	[FechaInsertUpdate] [datetime] NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Pedidos_Contactos] ADD  DEFAULT (getdate()) FOR [FechaInsertUpdate]
GO

