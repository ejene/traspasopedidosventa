﻿CREATE TABLE [dbo].[ConfigEmail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](250) NULL,
	[smtp] [nvarchar](250) NULL,
	[puerto] [nvarchar](10) NULL,
	[usuario] [nvarchar](50) NULL,
	[pswrd] [nvarchar](100) NULL,
	[tls] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ConfigEmail] ADD  DEFAULT ((0)) FOR [tls]
GO

