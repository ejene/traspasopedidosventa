﻿CREATE TABLE [dbo].[usuarios_reports](
	[id] [varchar](50) NULL,
	[usuario] [varchar](50) NULL,
	[reportAlbaran] [varchar](100) NULL,
	[reportFactura] [varchar](100) NULL,
	[reportEtiquetas] [varchar](100) NULL
) ON [PRIMARY]
GO

