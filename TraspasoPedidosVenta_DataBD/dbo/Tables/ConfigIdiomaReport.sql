﻿CREATE TABLE [dbo].[ConfigIdiomaReport](
	[idioma] [varchar](50) NOT NULL,
	[nombre] [varchar](100) NULL,
	[tipo] [varchar](50) NOT NULL,
	[report] [varchar](200) NOT NULL
) ON [PRIMARY]
GO

