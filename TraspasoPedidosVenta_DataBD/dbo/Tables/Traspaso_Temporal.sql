﻿CREATE TABLE [dbo].[Traspaso_Temporal](
	[sesion] [varchar](50) NOT NULL,
	[id] [varchar](50) NULL,
	[empresa] [char](2) NOT NULL,
	[numero] [char](10) NOT NULL,
	[letra] [varchar](10) NOT NULL,
	[linea] [int] NOT NULL,
	[articulo] [varchar](20) NULL,
	[lote] [varchar](50) NOT NULL,
	[cajas] [numeric](10, 2) NULL,
	[unidades] [numeric](15, 6) NULL,
	[peso] [varchar](50) NULL,
	[cliente] [varchar](50) NULL,
	[ruta] [varchar](50) NULL,
	[almacen] [char](2) NULL,
	[caducidad] [smalldatetime] NULL,
	[bultos] [char](7) NULL,
	[impAlb] [int] NULL,
	[impEti] [int] NULL,
	[linea_av] [int] NULL,
	[eticajas] [char](7) NULL,
	[FechaInsertUpdate] [smalldatetime] NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Traspaso_Temporal] ADD  DEFAULT ((0)) FOR [impAlb]
GO

ALTER TABLE [dbo].[Traspaso_Temporal] ADD  DEFAULT ((0)) FOR [impEti]
GO

ALTER TABLE [dbo].[Traspaso_Temporal] ADD  DEFAULT (getdate()) FOR [FechaInsertUpdate]
GO

