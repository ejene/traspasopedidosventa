﻿CREATE TABLE [dbo].[Configuracion](
	[id] [int] IDENTITY(1,1) NOT NULL,	
	[Ejercicio] [varchar](4) NULL,
	[Gestion] [varchar](6) NULL,
	[Letra] [char](2) NULL,
	[Comun] [char](8) NULL,
	[Campos] [varchar](8) NULL,
	[Lotes] [varchar](8) NULL,
	[Empresa] [char](2) NULL,
	[NombreEmpresa] [varchar](100) NULL,
	[FechaInsertUpdate] [datetime] NULL DEFAULT (getdate()) 
) ON [PRIMARY]
GO
