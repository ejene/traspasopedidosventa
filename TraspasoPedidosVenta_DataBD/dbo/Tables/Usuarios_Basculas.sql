﻿CREATE TABLE [dbo].[usuarios_basculas](
	[usuario] [varchar](50) NULL,
	[bascula] [varchar](50) NULL,
	[predet] [bit] NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[usuarios_basculas] ADD  DEFAULT ((0)) FOR [predet]
GO

