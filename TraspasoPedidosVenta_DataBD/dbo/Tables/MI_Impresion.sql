﻿CREATE TABLE [dbo].[MI_Impresion](
	[terminal] [char](50) NULL,
	[empresa] [char](2) NULL,
	[numero] [char](10) NULL,
	[letra] [char](2) NULL,
	[factura] [char](10) NULL,
	[imprimir] [bit] NULL,
	[imp_eti] [bit] NULL,
	[bultos] [char](7) NULL
) ON [PRIMARY]
GO

