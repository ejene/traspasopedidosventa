﻿CREATE TABLE [dbo].[LOG_CREARAV](
	[VALOR] [nvarchar](max) NULL,
	[FECHA] [datetime] NULL,
	[EMPRESA] [char](2) NULL,
	[NUMERO] [char](20) NULL,
	[LETRA] [char](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
