﻿CREATE TABLE [dbo].[ConfigPersonalizaciones](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdEmpresa] [int] NOT NULL,
	[IdTipo] [int] NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Contenido] [varchar](max) NOT NULL,
	[FechaInsertUpdate] [datetime] DEFAULT (getdate()) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]