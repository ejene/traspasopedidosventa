﻿CREATE TABLE [dbo].[ConfigPersEmpresas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Empresa] [varchar](100) NULL,
	[Activo] [bit] DEFAULT ((0)) NOT NULL,
	[FechaInsertUpdate] [datetime] DEFAULT (getdate()) NOT NULL
) ON [PRIMARY]