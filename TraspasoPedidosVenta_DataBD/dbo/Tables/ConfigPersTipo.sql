﻿CREATE TABLE [dbo].[ConfigPersTipo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[FechaInsertUpdate] [datetime] DEFAULT (getdate()) NOT NULL
) ON [PRIMARY]