﻿CREATE PROCEDURE [dbo].[pSustituirArticulos] @parametros varchar(max)
AS
BEGIN
	SET NOCOUNT ON
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare @articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
		,	@linea int = cast(isnull((select JSON_VALUE(@parametros,'$.linea')),'0') as int)
		,	@unidades int = cast(isnull((select JSON_VALUE(@parametros,'$.unidades')),'0') as int)
		,	@letra varchar(2) = isnull((select JSON_VALUE(@parametros,'$.letra')),'')
		,	@numero varchar(50) = isnull((select JSON_VALUE(@parametros,'$.numero')),'')

	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EMPRESA char(2), @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select  @EMPRESA=Empresa, @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------	
	-- Variables
	DECLARE @CONSULTA NVARCHAR(MAX)
	-------------------------------------------------------------------------------------------------


	-- obtener datos del artículo
	DECLARE @SQLString nvarchar(500);  
	DECLARE @ParamDefinition nvarchar(500);  
	DECLARE @NOMBRE nvarchar(75), @CAJAS int=0, @UNICAJA int, @COST_ULT1 numeric(15,6), @IMPORTE numeric(15,6), @PESO varchar(50)
	SET @SQLString = N'SELECT @NOMBRE=nombre, @UNICAJA=UNICAJA, @COST_ULT1=COST_ULT1, @PESO=PESO 
					 FROM ['+@GESTION+'].DBO.articulo WHERE CODIGO=@CODIGO';
	SET @ParamDefinition = N'@CODIGO varchar(20), @NOMBRE nvarchar(75) OUTPUT, @UNICAJA int OUTPUT, @COST_ULT1 int OUTPUT
						 , @PESO varchar(50) OUTPUT';
	EXECUTE sp_executesql @SQLString, @ParamDefinition, @CODIGO=@articulo, @NOMBRE=@NOMBRE OUTPUT, @UNICAJA=@UNICAJA OUTPUT
						, @COST_ULT1=@COST_ULT1 OUTPUT, @PESO=@PESO OUTPUT 

	if @UNICAJA>0 set @CAJAS=@unidades/@UNICAJA
	if isnull(@PESO,'0.000')='' set @PESO='0.000'

	-- actualizar linea de pedido
	SET @CONSULTA=CONCAT('UPDATE [',@GESTION,'].DBO.d_pedive 
		SET ARTICULO=''',@articulo,''', DEFINICION=''',@NOMBRE,''', PRECIO=''',@COST_ULT1,'''  
		, UNIDADES=',@unidades,', CAJAS=',@CAJAS,', PESO=',@PESO,'  
		WHERE EMPRESA=''',@EMPRESA,''' AND NUMERO=''',@NUMERO,''' AND LETRA=''',@LETRA,''' AND LINIA=',@linea)
	EXEC (@CONSULTA)

	
	-- recálculo de la linea
	declare @param varchar(max) = 
	CONCAT('{"numero":"',@numero,'","letra":"',@letra,'","articulo":"',@articulo,'","linea":"',@linea,'"'
	,',"uds":"',@unidades,'","peso":"',@PESO,'"}')

	EXEC [pCalculosPV] @param

	RETURN -1 
END
