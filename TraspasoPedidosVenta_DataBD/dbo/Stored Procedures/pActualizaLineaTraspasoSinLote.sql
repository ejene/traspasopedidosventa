﻿CREATE PROCEDURE [dbo].[pActualizaLineaTraspasoSinLote] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare	  @pedido varchar(50) = isnull((select JSON_VALUE(@parametros,'$.pedido')),'')
			, @serie varchar(10) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
			, @lote varchar(50) = isnull((select JSON_VALUE(@parametros,'$.lote')),'')
			, @articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
			, @cajas int = cast(isnull((select JSON_VALUE(@parametros,'$.cajas')),'0') as int)
			, @unidades int = cast(isnull((select JSON_VALUE(@parametros,'$.unidades')),'0') as int)
			, @peso varchar(10) = isnull((select JSON_VALUE(@parametros,'$.peso')),'')
			, @linea int = cast(isnull((select JSON_VALUE(@parametros,'$.linea')),'0') as int)			
			
	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EJERCICIO char(4)
	declare @GESTION char(6)
	declare @COMUN char(8)
	declare @LOTES char(8)
	select @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	declare @empresa char(2) = (select Empresa from Configuracion)
		,	@sql varchar(max)      
	    
    declare @elCliente varchar(50), @laRuta varchar(10), @almacen varchar(10)
		,	@unicaja int=0
		,	@laCaducidad smalldatetime = cast(getdate() as smalldatetime)

	-- tabla temporal para recopilación de datos ------------------------------------------------------
	declare @tablaTemp TABLE (CLIENTE nvarchar(20), RUTA varchar(10), UNICAJA int, caducidad smalldatetime)
	SET @sql = 'select top 1 CLIENTE, RUTA
				, (select top 1 ruta from Detalle_Pedidos where numero='''+@pedido+''' and LETRA='''+@serie+''') as RUTA
				, (select UNICAJA from ['+@GESTION+'].dbo.articulo where CODIGO='''+@articulo+''') as UNICAJA
				, (select CADUCIDAD from ['+@LOTES+'].[dbo].[stocklotes] where ALMACEN='''+@almacen+''' and ARTICULO='''+@articulo+''') as caducidad
				from Detalle_Pedidos where numero='''+@pedido+'''  and LETRA='''+@serie+''''
	INSERT @tablaTemp EXEC(@sql)	
	SELECT @elCliente=CLIENTE, @laRuta=RUTA, @unicaja=UNICAJA, @laCaducidad=caducidad FROM @tablaTemp
	delete @tablaTemp
        
	-- si existe la linea en [Traspaso_Temporal] la borramos
	delete from [Traspaso_Temporal] where empresa=@empresa and numero=@pedido and letra=@serie and linea=@linea and lote=@lote
	
    -- guardamos la linea en la tabla temporal
	if(@cajas>0 or @unidades>0 or cast(@peso as numeric(15,6))>0.000) BEGIN
	declare @nu varchar(50) = replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 
	insert into [Traspaso_Temporal]
		(sesion, id, empresa, numero, letra, linea, articulo, lote, cajas, unidades, peso, cliente, ruta, almacen, caducidad) 
		values 
		(@usuario, @nu, @empresa, @pedido, @serie, @linea, @articulo, @lote, @cajas, @unidades, @peso, @elCliente, @laRuta, @almacen, @laCaducidad)
	END

	select '{"respuesta":"OK!","pedido":"'+@pedido+'","serie":"'+@serie+'","ruta":"'+@laRuta+'"}' as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
	RETURN 0
END CATCH
