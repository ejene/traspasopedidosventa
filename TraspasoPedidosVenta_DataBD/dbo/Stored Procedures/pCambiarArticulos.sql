﻿CREATE PROCEDURE [dbo].[pCambiarArticulos] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON

	-- datos JSON ----------------------------------------------------------------------------------
	declare	  @modo varchar(50)		= isnull((select JSON_VALUE(@parametros,'$.modo')),'')
			, @pedido varchar(50)	= isnull((select JSON_VALUE(@parametros,'$.pedido')),'')
			, @serie varchar(10)	= isnull((select JSON_VALUE(@parametros,'$.serie')),'')
			, @articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
			, @Almacen varchar(50)  = isnull((select JSON_VALUE(@parametros,'$.almacen')),'')

	declare @usuario varchar(50)	= isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50)		= isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-------------------------------------------------------------------------------------------------

	declare @empresa char(2) = (select Empresa from Configuracion)
		,	@sql varchar(max) 
	

	set @sql = (
		SELECT articulo, definicion, cajas_pv, uds_pv, peso_pv, linia 
		FROM [dbo].[Detalle_Pedidos] 
		WHERE empresa=@empresa and letra=@serie and numero=@pedido AND UDS_TRASPASADAS=0 AND ARTICULO!=''
		order by linia asc 
		for JSON AUTO, INCLUDE_NULL_VALUES
	)


	if @modo='equivalencia' BEGIN
		set @sql = (SELECT ARTICULO, NOMBRE, stock FROM Articulos_Sustitucion WHERE art_orig=@articulo and almacenSTC=@Almacen for JSON AUTO, INCLUDE_NULL_VALUES)
	END

	select isnull((@sql),'[]') as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
