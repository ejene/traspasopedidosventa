﻿CREATE PROCEDURE [dbo].[pMerlos] @parametros varchar(max)
AS
SET NOCOUNT ON
BEGIN TRY
	declare @sp varchar(50) = isnull((select JSON_VALUE(@parametros,'$.sp')),'')

	if @sp<>'' EXEC ('exec ' + @sp + ' '''+@parametros+''' ')

	return -1
END TRY
BEGIN CATCH
	DECLARE @Message varchar(MAX) = CONCAT('{"error":"',ERROR_MESSAGE(),'","sp","'+@sp+'","parametros","'+@parametros+'"}')
		,   @Severity int = ERROR_SEVERITY(), @State smallint = ERROR_STATE() 
	RAISERROR (@Message, @Severity, @State)
	RETURN 0
END CATCH