﻿CREATE PROCEDURE [dbo].[pCrearFA]
(
	@empresa varchar(2) = '', 
	@letra varchar(2) = '',
	@numero varchar(10)= '',
	@fecha_fac char(11) = '',
	@terminal CHAR(50) = '',
	@imprimir INT = 0,
	@imp_eti INT = 0,
	@ETICAJAS VARCHAR(7) = '',
	@bultos VARCHAR(7) = '',
	@nEti BIT = 0,
	@respuestaSP VARCHAR(100)='' output				
)			
AS
BEGIN TRY

	SET NOCOUNT ON;
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select  @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------	
	-----------------------------------------------------------------------------------------------
	--								   Variables Dinamización  
	-----------------------------------------------------------------------------------------------
	declare @cont int
	declare @tbInt TABLE (dato int)
	declare @tbNum TABLE (dato numeric(15,6))
	declare @tbStr TABLE (dato varchar(1000))
	-----------------------------------------------------------------------------------------------

	DECLARE @Mensaje varchar(max), @MensajeError varchar(max)
	DECLARE @NUMFACTURA varchar(10)='', @FECHA varchar(10), @USUARIO varchar(25)='MERLOS#API#', @NUMASIENTO int, @ASI varchar(20), 
			@CLIENTE varchar(10), @NOMCLI varchar(100), @DIVISA varchar(3), @CAMBIO numeric(20,6), @GUID1 uniqueidentifier, @TOTALDOC numeric(15,6), 
			@CUENTA varchar(10), @ASI2 varchar(20), @LINASI integer = 1, @TIPOIVA varchar(2), @PORIVA numeric(20,2), @NMES int, @BASE numeric(15,6), 
			@RECAR numeric (15,6), @PORREC numeric(20,2), @CMES varchar(4), @CMES2 varchar(6), @DIAPAG int, @DIAPAG2 int, @MANDATO varchar(35), 
			@VENCIM varchar(10), @NUMGIROS int=0, @TOTALFAC numeric(15,6), @VALGIRO numeric(15,6), @FPAG varchar(2), @VENDEDOR varchar(2), 
			@HABER numeric(15,6), @DEBE numeric(15,6), @CTABAL2 varchar(4), @DIASGIRO int, @SECU varchar(4), @PORSE numeric(20,4), @CTAPORTES varchar(10), 
			@IVAPORT varchar(2), @PORIVAPOR numeric(20,2), @CLIFINAL varchar(10), @PRONTO NUMERIC(20,2), @IVATOT NUMERIC(20,2), @SIREC bit=0, 
			@IVAREC NUMERIC(20,2), @CLIIVA varchar(2), @IVACLI NUMERIC(20,2)=0.00, @RECDIV numeric(15,6)=0.00, @NUM_FA varchar(10)='', 
			@CUENTAREC varchar(10)='', @LINASIP integer = 1, @ASI3 varchar(20)

	declare @NROREG INT=0, @NROREG2 INT=0 ---para conteos en ciclos 26/10/2019

	SET @NMES=	(select MONTH(getdate()))
	SET @CMES=	(select right(year(getdate()),2)+right(replicate('0', 2)+ ltrim(rtrim(str(@NMES))), 2))
	SET @CMES2=	(select right(year(getdate()),4)+right(replicate('0', 2)+ ltrim(rtrim(str(@NMES))), 2))

	declare @tbCAV TABLE (CLIENTE varchar(10), DIVISA varchar(3), CAMBIO numeric(20,6), NUMFACTURA varchar(10), NOMCLI varchar(100), TOTALDOC numeric(15,6)
						, DIAPAG int, DIAPAG2 int, MANDATO varchar(35), TOTALFAC numeric(15,6), FPAG varchar(2), VENDEDOR varchar(2), CLIFINAL varchar(10)
						, PRONTO NUMERIC(20,2), SIREC bit, CLIIVA varchar(2))
	INSERT	@tbCAV EXEC('select C_ALBVEN.CLIENTE, C_ALBVEN.DIVISA, C_ALBVEN.CAMBIO, C_ALBVEN.FACTURA, NOMBRE, C_ALBVEN.TOTALDOC, CLIENTES.DIAPAG, CLIENTES.DIAPAG2
						, C_ALBVEN.MANDATO, =C_ALBVEN.TOTALDOC,	CLIENTES.FPAG, C_ALBVEN.VENDEDOR, CLIENTES.CLIFINAL, C_ALBVEN.PRONTO, CLIENTES.RECARGO, CLIENTES.TIPO_IVA
						FROM ['+@GESTION+'].DBO.C_ALBVEN 
						left join ['+@GESTION+'].DBO.CLIENTES on C_ALBVEN.CLIENTE=CLIENTES.CODIGO 
						where C_ALBVEN.EMPRESA='''+@empresa+''' and C_ALBVEN.LETRA='''+@letra+''' and C_ALBVEN.NUMERO='''+@numero+'''')	
	SELECT	@CLIENTE=CLIENTE, @DIVISA=DIVISA, @CAMBIO=CAMBIO, @NUMFACTURA=NUMFACTURA, @NOMCLI=NOMCLI, @TOTALDOC=TOTALDOC, @DIAPAG=DIAPAG, @DIAPAG2=DIAPAG2, @MANDATO=MANDATO
			, @TOTALFAC=TOTALDOC, @FPAG=FPAG, @VENDEDOR=VENDEDOR, @CLIFINAL=CLIFINAL, @PRONTO=PRONTO, @SIREC=SIREC, @CLIIVA=CLIIVA 
			FROM @tbCAV		
	delete	@tbCAV

	if ltrim(rtrim(isnull(@cliente,'')))=''
	begin 
		set @respuestaSP='No existe el albaran de venta'+@letra+' '+@numero
		SET @MensajeError='","ERROR":"'
		raiserror ('No existe el albarán de venta',16,1);
		return 0
	end
	if ltrim(rtrim(isnull(@NUMFACTURA,'')))<>''
	begin
		set @respuestaSP='Albarán ya Facturado '+@letra+' '+@numero
		SET @MensajeError='","ERROR":"'
		raiserror ('Albarán ya facturado ',16,1);
		return 0
	end

	declare @tbSeries TABLE (NUM_FA varchar(10))
	INSERT  @tbSeries EXEC('SELECT @letra+SPACE(8-LEN(CAST(CONTADOR+1 AS VARCHAR(8))))+CAST(CONTADOR+1 AS VARCHAR(8))
						    from ['+@GESTION+'].dbo.series where empresa='''+@empresa+''' and serie='''+@letra+''' and TIPODOC=7')	
	SELECT  @NUM_FA=NUM_FA FROM @tbSeries		
	delete  @tbSeries
	EXEC('UPDATE ['+@GESTION+'].dbo.series set CONTADOR=CONTADOR+1 where empresa='''+@empresa+''' and serie='''+@letra+''' and TIPODOC=7')	
				
	-- Albert R. 30/06/2020. Obtengo fecha factura asignada en el albarán de venta
	declare @tbFecha TABLE (fecha int)
	INSERT  @tbFecha EXEC('select CONVERT(CHAR(11), fecha_fac, 103) from ['+@GESTION+'].dbo.c_albven where empresa='''+@empresa+''' and numero='''+@numero+''' and letra='''+@letra+'''')	
	SELECT  @fecha=fecha FROM @tbFecha		
	delete  @tbFecha
				
	SET @NUMFACTURA=REPLACE(@NUM_FA, ' ', '')
	--Iniciamos registro contable
				
	-- Albert R. 30/06/20. Este punto se ha hecho porque varias veces que se ha intentado facturar, salta error de registro duplicado en los asientos. A ver sin con esto lo evitamos.
	declare @tbNumAsiento TABLE (NUMASIENTO int)
	INSERT  @tbNumAsiento EXEC('SELECT asiento+1 FROM ['+@GESTION+'].DBO.empresa WHERE codigo='''+@empresa+'''')	
	SELECT  @NUMASIENTO=NUMASIENTO FROM @tbNumAsiento		
	delete  @tbNumAsiento

	declare @tbAsientos TABLE (asientos int)
	INSERT  @tbAsientos EXEC('SELECT count(NUMERO) FROM ['+@GESTION+'].dbo.asientos WHERE NUMERO='+@NUMASIENTO)	
	SELECT  @NUMASIENTO=asientos FROM @tbAsientos		
	delete  @tbAsientos

	declare @asientosMax int
	INSERT  @tbAsientos EXEC('SELECT MAX(NUMERO)+1 FROM ['+@GESTION+'].dbo.asientos')	
	SELECT  @asientosMax=asientos FROM @tbAsientos		
	delete  @tbAsientos

	IF @NUMASIENTO>0 SET @NUMASIENTO=@asientosMax
				
	EXEC('UPDATE ['+@GESTION+'].dbo.empresa  SET asiento='+@NUMASIENTO+' WHERE codigo='''+@empresa+'''')
				
	SET @GUID1=NEWID()
	SET @ASI=(select 'M'+ltrim(@NUMFACTURA)+ltrim(convert(char(11),getdate(),112)))
	-- Llenamos el primer registro del asiento con el total por la cuenta del cliente
	-- 16/10/2019 El Cliente se cambia por clifinal si existe
	IF LTRIM(RTRIM(ISNULL(@CLIFINAL,'')))!='' SET @CLIENTE=@CLIFINAL

	EXEC('INSERT INTO ['+@GESTION+'].dbo.asientos  (usuario,empresa,numero,linea,cuenta,fecha,definicion,debe,haber,tipo,
	punteo,factura,proveedor,asi,importediv,divisa,debediv,haberdiv,cambio,referencia,guid)
	values('''+@USUARIO+''', '''+@empresa+''', '''+@NUMASIENTO+''', '''+@LINASI+''', '''+@CLIENTE+''', '''+@FECHA+''', ''N/ FRA ''+'''+@NUM_FA+''''
		+', '+@TOTALDOC+', 0, 0, 0, '''+@NUM_FA+''', '''', '''+@ASI+''', '+@TOTALDOC+', '''+@DIVISA+''', '+@TOTALDOC+', 0, '''+@CAMBIO+''''
		+', ltrim(str('''+@NUMASIENTO+''')), '''+@GUID1+''')')
	--Fin primer registro de asiento
				
	--09/10/2019 insertamos secundarias si existen --------------------------------------------------------------------------------------------
	declare @tbSecundarias TABLE (mykey int, secundaria varchar(50), porcentaje numeric(16,4), nroreg bigint)
	INSERT  @tbSecundarias EXEC('select null mykey, secundaria, porcentaje, row_number() over(order by secundaria) as nroreg 
								 from ['+@GESTION+'].DBO.otras where codigo='''+@CLIENTE+''' order by secundaria')

	while (select count(*) from @tbSecundarias where isnull(mykey, 0)= 0 ) > 0 /* @@rowcount > 0 */	begin
		SET @NROREG=(SELECT MIN(NROREG) FROM @tbSecundarias where isnull(mykey, 0)= 0)
		SELECT @SECU=SECUNDARIA, @PORSE=PORCENTAJE from @tbSecundarias where nroreg=@NROREG
		EXEC('INSERT INTO ['+@GESTION+'].dbo.otrasien (empresa,secundar,codcuen,fecha,debe,haber,asi,debediv,haberdiv,divisa) 
			VALUES ('''+@empresa+''','''+@SECU+''','''+@CLIENTE+''','''+@FECHA+''','+@TOTALDOC+'*'+@PORSE+'/100, 0.0000,'''+@ASI+''',0.0000,0.0000,''   '')')
		update @tbSecundarias set mykey=1 where nroreg=@NROREG
	end
	delete  @tbSecundarias
	--fin secundarias  ------------------------------------------------------------------------------------------------------------------------

	SET @CUENTA=@CLIENTE

	-------------------------------------------------------------------------------------------------------------------------------------------
	--08/10/2019 ACTUALIZAMOS SALDOS, BAL1 y BAL2
	declare @tbSaldos TABLE (dato numeric(15,6))
	insert @tbSaldos EXEC('select DEBE from ['+@GESTION+'].dbo.saldos where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
	select @DEBE=dato from @tbSaldos
	delete @tbSaldos
	if ISNULL(@DEBE, -1)=-1
		EXEC('INSERT INTO ['+@GESTION+'].dbo.saldos (empresa,cuenta,debe,haber,importediv,divisa) VALUES ('''+@empresa+''','''+@CUENTA+''','''+@TOTALDOC+''',0.000000,0.000000,''   '')')
	else 
		EXEC('update ['+@GESTION+'].dbo.saldos SET DEBE='+@DEBE+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		
	--bal1
	insert @tbSaldos EXEC('select DEBE from ['+@GESTION+'].dbo.bal1 where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
	select @DEBE=dato from @tbSaldos
	delete @tbSaldos
	if ISNULL(@DEBE, -1)=-1
		EXEC('insert into ['+@GESTION+'].dbo.bal1(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CUENTA+''','+@TOTALDOC+', 0.000000,'''+@CMES+''','' '','''+@CMES2+''')')
	else 
		EXEC('update ['+@GESTION+'].dbo.bal1 SET DEBE='+@DEBE+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
	
	SET @CTABAL2=(select LEFT(@CUENTA, 4))
	--bal2
	insert @tbSaldos EXEC('select DEBE from ['+@GESTION+'].dbo.bal2 where EMPRESA='''+@empresa+''' and CUENTA='''+@CTABAL2+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
	select @DEBE=dato from @tbSaldos
	delete @tbSaldos
	if ISNULL(@DEBE, -1)=-1
		EXEC('insert into ['+@GESTION+'].dbo.bal2(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CTABAL2+''','+@TOTALDOC+', 0.000000,'''+@CMES+''','' '','''+@CMES2+''')')
	else 
		EXEC('update ['+@GESTION+'].dbo.bal2 SET DEBE='+@DEBE+'+'+@TOTALDOC+' WHERE empresa='''+@EMPRESA+''' and cuenta='''+@CTABAL2+''' and mes='''+@CMES+''' and MES2='''+@CMES2+'''')
	
	---fin actualiza saldos -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
				
	--Actualizamos cabecera de albarán con la factura creada
	EXEC('UPDATE ['+@GESTION+'].DBO.C_ALBVEN set FACTURA='''+@NUM_FA+''', FECHA_FAC='''+@FECHA+''', ASI='''+@ASI+''', EXPORTAR=null 
		  WHERE EMPRESA='''+@empresa+''' and LETRA='''+@letra+''' and NUMERO='''+@numero+'''')

	--hacemos un ciclo por el detalle de las facturas para hacer los registros contables
	--cada registro lleva un asi diferente pero el primero comparte el asi con la cabecera del albarán
	--detalle de items de la factura agrupados por cuenta de ventas de la familia

	--09/05/2019 arreglo para que saque los valores sin el calculo del pronto pago
	declare @tbValores TABLE (mykey int, CTA_VENTA varchar(50), importe numeric(15,6), nroreg bigint)
	insert @tbValores EXEC('select null mykey, f.CTA_VENTA, round(sum(d.IMPORTE+d.PVERDE),2) as importe,
							row_number() over(order by f.CTA_VENTA) as nroreg 
							FROM ['+@GESTION+'].DBO.D_ALBVEN d 
							join ['+@GESTION+'].DBO.articulo a on d.ARTICULO=a.CODIGO 
							join ['+@GESTION+'].dbo.familias f on a.FAMILIA=f.CODIGO 
							where d.EMPRESA='''+@empresa+''' and d.LETRA='''+@letra+''' and d.NUMERO='''+@numero+'''
							group by f.CTA_VENTA')
	while (select count(*) from @tbValores where isnull(mykey, 0)= 0 ) > 0  begin   --@@rowcount > 0	
		SET @NROREG=(SELECT MIN(NROREG) FROM @tbValores where isnull(mykey, 0)= 0)
		select @CUENTA=CTA_VENTA, @TOTALDOC=importe from @tbValores where nroreg=@NROREG
		SET @LINASI=@LINASI+1
		SET @GUID1=NEWID()
		SET @ASI2=(SELECT RIGHT(newid(),20))   

		declare @fraCli varchar(100) = 'N/ FRA '+@NUM_FA+' '+@NOMCLI
		EXEC('INSERT INTO ['+@GESTION+'].dbo.asientos  (usuario,empresa,numero,linea,cuenta,fecha,definicion,debe,haber,tipo,
		punteo,factura,proveedor,asi,importediv,divisa,debediv,haberdiv,cambio,referencia,guid)
		values('''+@USUARIO+''', '''+@empresa+''', '''+@NUMASIENTO+''', '''+@LINASI+''', '''+@CUENTA+''', '''+@FECHA+'''
		, '''+@fraCli+''', 0, '''+@TOTALDOC+''', 0, 0, '''+@NUM_FA+''', '''', '''+@ASI2+''', '''+@TOTALDOC+''', '''+@DIVISA+'''
		, 0, '''+@TOTALDOC+''', '''+@CAMBIO+''', ltrim(str('''+@NUMASIENTO+''')), '''+@GUID1+''')')

		--09/10/2019 insertamos secundarias si existen
		INSERT  @tbSecundarias EXEC('select null mykey, secundaria, porcentaje, row_number() over(order by secundaria) as nroreg  
									 from ['+@GESTION+'].DBO.otras where codigo='''+@CUENTA+'''')

		while (select count(*) from @tbSecundarias where isnull(mykey, 0)= 0 ) > 0 BEGIN -- @@rowcount > 0
			SET @NROREG2=(SELECT MIN(NROREG) FROM @tbSecundarias where isnull(mykey, 0)= 0)
			SELECT @SECU=SECUNDARIA, @PORSE=PORCENTAJE from @tbSecundarias where nroreg=@NROREG2
			EXEC('INSERT INTO ['+@GESTION+'].dbo.otrasien (empresa,secundar,codcuen,fecha,debe,haber,asi,debediv,haberdiv,divisa) 
				 VALUES ('''+@empresa+''','''+@SECU+''','''+@CUENTA+''','''+@FECHA+''',0.0000,'+@TOTALDOC+'*'+@PORSE+'/100,'''+@ASI2+''', 0.0000, 0.0000, ''   '')')

			update @tbSecundarias set mykey = 1 where nroreg=@NROREG2
		end 
		delete @tbSecundarias
		--fin secundarias
				
		--09/10/2019 actualizamos/creamos el registro de la cuenta en bal1, bal2 y saldos 
		--08/10/2019 ACTUALIZAMOS SALDOS, BAL1 y BAL2
		insert @tbSaldos EXEC('select HABER from ['+@GESTION+'].dbo.saldos where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		select @HABER=dato from @tbSaldos
		delete @tbSaldos
		declare @elHaber numeric(15,6) = COALESCE(@HABER,0.0000)
		if ISNULL(@HABER, -1)=-1
			EXEC('INSERT INTO ['+@GESTION+'].dbo.saldos (empresa,cuenta,debe,haber,importediv,divisa) VALUES ('''+@empresa+''','''+@CUENTA+''',0.000000,'''+@elHaber+''',0.000000,''   '')')
		else 
			EXEC('update ['+@GESTION+'].dbo.saldos SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		
		--bal1
		insert @tbSaldos EXEC('select HABER from ['+@GESTION+'].dbo.bal1 where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		select @HABER=dato from @tbSaldos
		delete @tbSaldos
		if ISNULL(@HABER, -1)=-1
			EXEC('insert into ['+@GESTION+'].dbo.bal1(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CUENTA+''',0.000000,'''+@TOTALDOC+''', '''+@CMES+''','' '','''+@CMES2+''')')
		else 
			EXEC('update ['+@GESTION+'].dbo.bal1 SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		
		SET @CTABAL2=(select LEFT(@CUENTA, 4))
		--bal2
		insert @tbSaldos EXEC('select HABER from ['+@GESTION+'].dbo.bal2 where EMPRESA='''+@empresa+''' and CUENTA='''+@CTABAL2+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		select @HABER=dato from @tbSaldos
		delete @tbSaldos
		if ISNULL(@HABER, -1)=-1
			EXEC('insert into ['+@GESTION+'].dbo.bal2(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CTABAL2+''',0.000000,'+@TOTALDOC+', '''+@CMES+''','' '','''+@CMES2+''')')
		else 
			EXEC('update ['+@GESTION+'].dbo.bal2 SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE empresa='''+@EMPRESA+''' and cuenta='''+@CTABAL2+''' and mes='''+@CMES+''' and MES2='''+@CMES2+''')')
		
		---fin actualiza saldos

		update @tbValores set mykey = 1 where nroreg=@NROREG
	end
	delete @tbValores
				
	--17/10/2019 Si el cliente tiene un iva puesto, este se superpone a todos los ivas que existan
	IF @CLIIVA!='' BEGIN
		insert @tbNum EXEC('SELECT IVA FROM ['+@GESTION+'].dbo.tipo_iva where CODIGO='''+@CLIIVA+'''')
		SELECT @IVACLI=dato FROM @tbNum
		delete @tbNum
	END

	--10/10/2018 Registros de Iva vinculado a cada artículo agrupados por cuenta
	declare @tbIVAVinc TABLE (mykey int, CODIGO char(2), IVA numeric(20,2), CTA_IV_REP char(8), CTA_RE_REP char(8), RECARG numeric(20,2), total numeric(15,6), importe numeric(15,6), nroreg bigint)
	insert @tbIVAVinc EXEC('select null mykey, isnull(f.CODIGO,'''') as CODIGO, CASE WHEN '''+@CLIIVA+'''!='''' THEN '''+@IVACLI+''' ELSE isnull(f.IVA,0.00) END AS IVA, f.CTA_IV_REP, f.CTA_RE_REP,
							case when '+@SIREC+'=1 then isnull(f.RECARG,0.00) else 0.00 end as RECARG, 
							round(sum(d.IMPORTE+d.PVERDE)-(sum(d.IMPORTE+d.PVERDE)*'+@PRONTO+'/100),2) as total, 
							round(sum(d.IMPORTE+d.PVERDE)-(sum(d.IMPORTE+d.PVERDE)*'+@PRONTO+'/100),2) as importe, 
							row_number() over(order by isnull(f.codigo,'''')) as nroreg
							FROM ['+@GESTION+'].DBO.D_ALBVEN d 
							join ['+@GESTION+'].dbo.tipo_iva f on d.TIPO_IVA=f.CODIGO 
							where d.EMPRESA='''+@empresa+''' and d.LETRA='''+@letra+''' and d.NUMERO='''+@numero+'''
							group by f.CODIGO, f.IVA, f.CTA_IV_REP, f.CTA_RE_REP, f.RECARG')
--26/03/2019 ya tengo la variable recargo en @SIREC la cuenta es codcom!recfinan

	while (select count(*) from @tbIVAVinc where isnull(mykey, 0)= 0 ) > 0  
	begin

		SET @NROREG=(SELECT MIN(nroreg) from @tbIVAVinc where isnull(mykey, 0)= 0 )
		select @CUENTA=CTA_IV_REP, @BASE=total, @TOTALDOC=importe, @PORIVA=IVA, @TIPOIVA=CODIGO, @PORREC=RECARG, @CUENTAREC=CTA_RE_REP 
		from @tbIVAVinc where nroreg=@NROREG

		SET @LINASI=@LINASI+1
		SET @GUID1=NEWID()
		SET @ASI2=(SELECT RIGHT(newid(),20)) 
		SET @RECAR=round(@BASE*@PORREC/100,2) 
		SET @RECDIV=@RECAR*@CAMBIO

		EXEC('INSERT INTO ['+@GESTION+'].dbo.asientos  (usuario,empresa,numero,linea,cuenta,fecha,definicion,debe,haber,tipo,
			  punteo,factura,proveedor,asi,importediv,divisa,debediv,haberdiv,cambio,referencia,guid)
			  values('''+@USUARIO+''', '''+@empresa+''', '+@NUMASIENTO+', '+@LINASI+', '''+@CUENTA+''', '''+@FECHA+''', ''N/ FRA ''+'''+@NUM_FA+'''+'' ''+'''+@NOMCLI+'''
			  , 0, ROUND('+@BASE+'*'+@PORIVA+'/100,2), 0, 0, '''+@NUM_FA+''', '''', '''+@ASI2+''', ROUND('+@BASE+'*'+@PORIVA+'/100,2), '''+@DIVISA+''', 0
			  , ROUND('+@BASE+'*'+@PORIVA+'/100,2), '''+@CAMBIO+''', ltrim(str('+@NUMASIENTO+')), '''+@GUID1+''')')

		--27/03/2019 CLIENTE RECARGO
		SET @LINASI=@LINASI+1
		SET @GUID1=NEWID()
				
		declare @tempasi varchar(20)
		SET @tempasi=(SELECT RIGHT(newid(),20))
		if @RECAR>0
			EXEC('INSERT INTO ['+@GESTION+'].dbo.asientos  (usuario,empresa,numero,linea,cuenta,fecha,definicion,debe,haber,tipo,
			punteo,factura,proveedor,asi,importediv,divisa,debediv,haberdiv,cambio,referencia,guid)
			values('''+@USUARIO+''', '''+@empresa+''', '+@NUMASIENTO+', '+@LINASI+', '''+@CUENTAREC+''', '''+@FECHA+''', ''N/ FRA ''+'''+@NUM_FA+'''+'' ''+'''+@NOMCLI+'''
			, 0, '+@RECAR+', 0,	0, '''+@NUM_FA+''', '''', '''+@tempasi+''', '''+@RECAR+''', '''+@DIVISA+''', 0, '''+@RECAR+''', '''+@CAMBIO+''', ltrim(str('+@NUMASIENTO+')), '''+@GUID1+''')')

		EXEC('
			INSERT INTO ['+@GESTION+'].dbo.ivareper(empresa,cuenta,numfra,tipo_iva,asi,fecha,bimpo,porcen_iva,iva,porcen_rec,
			recargo,liquidacio,comunitari,bimpodiv,ivadiv,recdiv,divisa,nombre,cif,finan,
			finandi,tipo,regtrans,liq_op, ejerliq_op,fechaoper,totcob,fecmax,critcaja) 
			VALUES('''+@empresa+''','''+@CLIENTE+''','''+@NUM_FA+''','''+@TIPOIVA+''','''+@ASI2+''','''+@FECHA+''','''+@BASE+''','''+@PORIVA+''',ROUND('+@BASE+'*'+@PORIVA+'/100,2),'''+@PORREC+'''
			, '+@RECAR+', 0, 0, '+@BASE+', ROUND('+@BASE+'*'+@PORIVA+'/100,2),'''+@RECDIV+''', '''+@DIVISA+''','''','''',0.0000,0.0000,1,0,0,0,'''+@FECHA+''',0,null,0)
		')

		--09/10/2019 insertamos secundarias si existen
		INSERT  @tbSecundarias EXEC('select null mykey, secundaria, porcentaje, row_number() over(order by secundaria) as nroreg  
									 from ['+@GESTION+'].DBO.otras where codigo='''+@CUENTA+'''')
		select null mykey, secundaria, porcentaje, row_number() over(order by secundaria) as nroreg from @tbSecundarias
		while (select count(*) from @tbSecundarias where isnull(mykey, 0)= 0 ) > 0 begin
			SET @NROREG2=(select min(nroreg) from @tbSecundarias where isnull(mykey, 0)= 0 )
			SELECT @SECU=SECUNDARIA, @PORSE=PORCENTAJE from @tbSecundarias where nroreg=@NROREG2
			EXEC('INSERT INTO ['+@GESTION+'].dbo.otrasien (empresa,secundar,codcuen,fecha,debe,haber,asi,debediv,haberdiv,divisa) 
				  VALUES ('''+@empresa+''','''+@SECU+''','''+@CUENTA+''','''+@FECHA+''', 0.0000, '+@TOTALDOC+'*'+@PORSE+'/100, '''+@ASI2+''', 0.0000,0.0000,''   '')')

			update @tbSecundarias set mykey = 1 where nroreg=@NROREG2
		end 
		delete @tbSecundarias
				
		--fin secundarias
		--09/10/2019 actualizamos/creamos el registro de la cuenta en bal1, bal2 y saldos 
		--08/10/2019 ACTUALIZAMOS SALDOS, BAL1 y BAL2
		insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.saldos where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		select @HABER=dato from @tbNum  delete @tbNum
		if ISNULL(@HABER, -1)=-1
			EXEC('INSERT INTO ['+@GESTION+'].dbo.saldos (empresa,cuenta,debe,haber,importediv,divisa) VALUES ('''+@empresa+''','''+@CUENTA+''',0.000000,'+@TOTALDOC+',0.000000,''   '')')   
		else
			EXEC('update ['+@GESTION+'].dbo.saldos SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		--bal1
		insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.bal1 where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		select @HABER=dato from @tbNum  delete @tbNum
		if ISNULL(@HABER, -1)=-1
			EXEC('insert into ['+@GESTION+'].dbo.bal1(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CUENTA+''',0.000000,'+@TOTALDOC+', '''+@CMES+''','' '','''+@CMES2+''')')
		else
			EXEC('update ['+@GESTION+'].dbo.bal1 SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		SET @CTABAL2=(select LEFT(@CUENTA, 4))
		--bal2
		insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.bal2 where EMPRESA='''+@empresa+''' and CUENTA='''+@CTABAL2+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		select @HABER=dato from @tbNum  delete @tbNum
		if ISNULL(@HABER, -1)=-1
			EXEC('insert into ['+@GESTION+'].dbo.bal2(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CTABAL2+''',0.000000,'+@TOTALDOC+', '''+@CMES+''','' '','''+@CMES2+''')')
		else
			EXEC('update ['+@GESTION+'].dbo.bal2 SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE empresa='''+@EMPRESA+''' and cuenta='''+@CTABAL2+''' and mes='''+@CMES+''' and MES2='''+@CMES2+'''')
		---fin actualiza saldos

		update @tbIVAVinc set mykey = 1 where nroreg=@NROREG
	end
	delete @tbIVAVinc

	--10/10/2018 anexamos los registros de portes si existen con su respectivo iva
	SET @LINASI=@LINASI+1
	SET @ASI2=(SELECT RIGHT(newid(),20))
	insert @tbStr EXEC('SELECT portes FROM ['+@COMUN+'].dbo.codcom')
	SELECT @CTAPORTES=dato FROM @tbStr delete @tbStr
	DECLARE @TOTPORTES NUMERIC(15,6)

	declare @tbPortes TABLE (TIPO_IVA char(2), TOTPORTES numeric(15,6))
	insert @tbPortes EXEC('SELECT isnull(TIPO_IVA,''''), isnull(IMPORTE,0.00) FROM ['+@GESTION+'].dbo.portes 
							WHERE empresa='''+@empresa+''' and albaran='''+@numero+''' and letra='''+@letra+''' and inc_fra=1')
	SELECT @IVAPORT=TIPO_IVA, @TOTPORTES=TOTPORTES FROM @tbPortes delete @tbPortes

if ISNULL(@TOTPORTES, -1)>0	begin
		SET @TOTALDOC=@TOTPORTES
						
		-- Albert 17/09/2020 - Si la cuenta ya existe hay que sumar los valores
		insert @tbInt EXEC('SELECT COUNT(EMPRESA) FROM ['+@GESTION+'].DBO.ASIENTOS WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='+@NUMASIENTO+' AND CUENTA='''+@CTAPORTES+'''')
		set @cont=(select dato from @tbInt) delete @tbInt	
		IF @cont>0 BEGIN
				EXEC('UPDATE ['+@GESTION+'].dbo.asientos SET HABER=HABER+'+@TOTALDOC+', IMPORTEDIV=IMPORTEDIV+'+@TOTALDOC+', HABERDIV=HABERDIV+'+@TOTALDOC+' 
						WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='+@NUMASIENTO+' AND CUENTA='''+@CTAPORTES+'''')
				insert @tbInt EXEC('SELECT ASI FROM ['+@GESTION+'].DBO.ASIENTOS WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='+@NUMASIENTO+' AND CUENTA='''+@CTAPORTES+'''')
				SET @ASI2=(select dato from @tbInt) delete @tbInt	
		END ELSE				
			EXEC('INSERT INTO ['+@GESTION+'].dbo.asientos  
					(usuario,empresa,numero,linea,cuenta,fecha,definicion,debe,haber,tipo,punteo,factura,proveedor,asi,importediv,divisa,debediv,haberdiv,cambio,referencia,guid) 
				VALUES ('''+@USUARIO+''','''+@empresa+''', '+@NUMASIENTO+', '+@LINASI+', '''+@CTAPORTES+''', '''+@FECHA+''',''N/ FRA ''+@NUM_FA+'' ''+'''+@NOMCLI+''', 0.0000
				, '+@TOTALDOC+',''0'', 0, '''+@NUM_FA+''',''          '', '''+@ASI2+''', '+@TOTALDOC+',''000'',0.0000,'+@TOTALDOC+',1.000000, ltrim(str('+@NUMASIENTO+')), NEWID())')
						
		--09/10/2019 insertamos secundarias si existen
		set rowcount 0
		INSERT @tbSecundarias EXEC('select null mykey, secundaria, porcentaje, row_number() over(order by secundaria) as nroreg from ['+@GESTION+'].DBO.otras where codigo='''+@CUENTA+'''')

		while (select count(*) from @tbSecundarias where isnull(mykey,0)= 0)>0
		begin
			set rowcount 0
			SET @NROREG=(select min(nroreg) from @tbSecundarias where isnull(mykey, 0)= 0 )
			SELECT @SECU=SECUNDARIA, @PORSE=PORCENTAJE from @tbSecundarias where nroreg=@NROREG

			-- Albert 17/09/2020 - Si la SECUNDARIA ya existe hay que sumar los valores
			insert @tbInt EXEC('SELECT COUNT(EMPRESA) FROM ['+@GESTION+'].DBO.otrasien WHERE EMPRESA='''+@EMPRESA+''' AND SECUNDAR='''+@SECU+''' AND ASI='''+@ASI2+'''')
			set @cont=(select dato from @tbInt) delete @tbInt	
			IF @cont>0
				EXEC('UPDATE ['+@GESTION+'].dbo.otrasien SET HABER=HABER+ROUND('+@TOTALDOC+'*'+@PORSE+'/100,2) WHERE EMPRESA='''+@EMPRESA+''' AND SECUNDAR='''+@SECU+''' AND ASI='''+@ASI2+'''')
			ELSE
				EXEC('INSERT INTO ['+@GESTION+'].dbo.otrasien (empresa,secundar,codcuen,fecha,debe,haber,asi,debediv,haberdiv,divisa) 
					  VALUES ('''+@empresa+''','''+@SECU+''','''+@CUENTA+''','''+@FECHA+''', 0.0000, ROUND('+@TOTALDOC+'*'+@PORSE+'/100,2), '''+@ASI2+''', 0.0000,0.0000,''   '')')

			update @tbSecundarias set mykey=1 where nroreg=@NROREG
		end 
		delete @tbSecundarias
		--fin secundarias

		--09/10/2019 actualizamos/creamos el registro de la cuenta en bal1, bal2 y saldos 
		--08/10/2019 ACTUALIZAMOS SALDOS, BAL1 y BAL2
		insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.saldos where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		select @HABER=(select dato from @tbNum) delete @tbNum
		if ISNULL(@HABER, -1)=-1
			EXEC('INSERT INTO ['+@GESTION+'].dbo.saldos (empresa,cuenta,debe,haber,importediv,divisa) VALUES ('''+@empresa+''','''+@CUENTA+''',0.000000,'+@HABER+',0.000000,''   '')')         
		else
			EXEC('update ['+@GESTION+'].dbo.saldos SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		--bal1
		insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.bal1 where EMPRESA=@'''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		select @HABER=(select dato from @tbNum) delete @tbNum
		if ISNULL(@HABER, -1)=-1
			EXEC('insert into ['+@GESTION+'].dbo.bal1(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CUENTA+''',0.000000,'+@TOTALDOC+', '''+@CMES+''','' '','''+@CMES2+''')')
		else
			EXEC('update ['+@GESTION+'].dbo.bal1 SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		SET @CTABAL2=(select LEFT(@CUENTA, 4))
		--bal2
		insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.bal2 where EMPRESA='''+@empresa+''' and CUENTA='''+@CTABAL2+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		select @HABER=(select dato from @tbNum) delete @tbNum
		if ISNULL(@HABER, -1)=-1
			EXEC('insert into ['+@GESTION+'].dbo.bal2(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CTABAL2+''',0.000000,'+@TOTALDOC+', '''+@CMES+''','' '','''+@CMES2+''')')
		else
			EXEC('update ['+@GESTION+'].dbo.bal2 SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE empresa='''+@EMPRESA+''' and cuenta='''+@CTABAL2+''' and mes='''+@CMES+''' and MES2='''+@CMES2+'''')
		---fin actualiza saldos
			
		
		declare @tbTipoIVA TABLE (IVA numeric (20,2), CTA_IV_REP varchar(10), RECARG numeric (20,2))
		insert 	@tbTipoIVA EXEC('SELECT IVA, CTA_IV_REP, case when '+@SIREC+'=1 then RECARG else 0.00 end FROM ['+@GESTION+'].dbo.tipo_iva WHERE codigo='''+@IVAPORT+'''')
		SELECT  @PORIVAPOR=IVA, @CUENTA=CTA_IV_REP, @PORREC=RECARG FROM @tbTipoIVA
		delete  @tbTipoIVA
						
		SET @ASI2=(SELECT RIGHT(newid(),20))

		--ingresa el ivareper del valor total del registro de asiento anterior
		SET @RECAR=round(@TOTPORTES*@PORREC/100,2)
		SET @RECDIV=@RECAR*@CAMBIO
		SET @IVAPORT=isnull(@IVAPORT,'')

		IF @IVAPORT!= '' BEGIN
			-- Albert 17/09/2020 - Si el tipo de IVA ya existe hay que sumar los valores
			insert @tbInt EXEC('SELECT COUNT(EMPRESA) FROM ['+@GESTION+'].dbo.ivareper WHERE EMPRESA='''+@EMPRESA+''' AND NUMFRA='''+@NUM_FA+''' AND TIPO_IVA='''+@IVAPORT+'''')
			set @cont = (select dato from @tbInt) delete @tbInt
			IF @cont>0
				EXEC('UPDATE ['+@GESTION+'].dbo.ivareper SET BIMPO=BIMPO+'+@TOTPORTES+', IVA=IVA+ROUND('+@TOTPORTES+'*'+@PORIVAPOR+'/100,2), RECARGO=RECARGO+'+@RECAR+'
					, BIMPODIV=BIMPODIV+'+@TOTPORTES+', IVADIV=IVADIV+ROUND('+@TOTPORTES+'*'+@PORIVAPOR+'/100,2), RECDIV=RECDIV+'+@RECAR+' 
					WHERE EMPRESA='''+@EMPRESA+''' AND NUMFRA='''+@NUM_FA+''' AND TIPO_IVA='''+@IVAPORT+'''')
			ELSE
				EXEC('INSERT INTO ['+@GESTION+'].dbo.ivareper  (empresa,cuenta,numfra,tipo_iva,asi,fecha,bimpo,porcen_iva,iva,porcen_rec,recargo,liquidacio,comunitari,
					bimpodiv,ivadiv,recdiv,divisa,nombre,cif,finan,finandi,tipo,regtrans,liq_op, ejerliq_op,fechaoper,totcob,fecmax,critcaja) 
				VALUES('''+@empresa+''', '''+@CLIENTE+''', '''+@NUM_FA+''', '''+@IVAPORT+''', '''+@ASI2+''', '''+@FECHA+''', '+@TOTPORTES+', '+@PORIVAPOR+'
				, ROUND('+@TOTPORTES+'*'+@PORIVAPOR+'/100,2), '+@PORREC+', '+@RECAR+', 0, 0, '+@TOTPORTES+', ROUND('+@TOTPORTES+'*'+@PORIVAPOR+'/100,2)
				, '+@RECDIV+', '''+@DIVISA+''','''','''',0.0000,0.0000,1,0,0,0,'''+@FECHA+''',0,null,0)')
								
			DECLARE @TOTALPOR DECIMAL(15,3)
			SET @TOTALPOR=@TOTALDOC
								
			SET @TOTALDOC=ROUND(@TOTALDOC*@PORIVAPOR/100,2)
			SET @LINASI=@LINASI+1
								
			-- Albert 17/09/2020 - Si la CUENTA ya existe hay que sumar los valores
			insert @tbInt EXEC('SELECT COUNT(EMPRESA) FROM ['+@GESTION+'].DBO.ASIENTOS WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='+@NUMASIENTO+' AND CUENTA='''+@CUENTA+'''')
			set @cont=(select dato from @tbInt) delete @tbInt
			IF @cont>0
				EXEC('UPDATE ['+@GESTION+'].dbo.asientos SET HABER=HABER+'+@TOTALDOC+', IMPORTEDIV=IMPORTEDIV+'+@TOTALDOC+', HABERDIV=HABERDIV+'+@TOTALDOC+' 
					  WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='+@NUMASIENTO+' AND CUENTA='''+@CUENTA+'''')
			ELSE				
				EXEC('INSERT INTO ['+@GESTION+'].dbo.asientos  (usuario,empresa,numero,linea,cuenta,fecha,definicion,debe,haber,tipo,
				punteo,factura,proveedor,asi,importediv,divisa,debediv,haberdiv,cambio,referencia,guid) 
				VALUES('''+@USUARIO+''','''+@empresa+''', '+@NUMASIENTO+', '''+@LINASI+''', '''+@CUENTA+''', '''+@FECHA+''',''N/ FRA ''+'''+@NUM_FA+'''+'' ''+'''+@NOMCLI+'''
				, 0.0000, '+@TOTALDOC+', ''0'', 0, '''+@NUM_FA+''','''', '''+@ASI2+''', '+@TOTALDOC+',''000'',0.0000,'+@TOTALDOC+',1.000000, ltrim(str('''+@NUMASIENTO+''')), NEWID())')
				
			-- Albert 23/09/2020 - Registro Recardo iva si el cliente trabaja con recargo Si la cuenta ya existe hay que sumar los valores
			IF @SIREC=1
				BEGIN
					DECLARE @RECPORT DECIMAL(10,3), @CTAPORT CHAR(20)
					declare @tIVA TABLE (RECARG numeric(20,2), CTA_RE_REP varchar(50))
					insert @tIVA EXEC('SELECT RECARG, CTA_RE_REP FROM ['+@GESTION+'].DBO.TIPO_IVA WHERE CODIGO='''+@IVAPORT+'''')
					SELECT @RECPORT=RECARG, @CTAPORT=CTA_RE_REP from @tIVA
					delete @tIVA
					insert @tbInt EXEC('SELECT COUNT(EMPRESA) FROM ['+@GESTION+'].DBO.ASIENTOS WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='+@NUMASIENTO+' AND CUENTA='''+@CTAPORT+'''')
					set @cont=(select dato from @tbInt) delete @tbInt
					IF @cont>0
						EXEC('UPDATE ['+@GESTION+'].dbo.asientos SET HABER=HABER+ROUND('+@TOTALPOR+'*('+@RECPORT+'/100),2), IMPORTEDIV=IMPORTEDIV+ROUND('+@TOTALPOR+'*(@RECPORT/100),2)
						, HABERDIV=HABERDIV+ROUND('+@TOTALPOR+'*('+@RECPORT+'/100),2)WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='+@NUMASIENTO+' AND CUENTA='''+@CTAPORT+'''')
					ELSE
						BEGIN
							SET @ASI3=(SELECT RIGHT(newid(),20))
							SET @LINASI=@LINASI+1
							EXEC('INSERT INTO ['+@GESTION+'].dbo.asientos  (usuario,empresa,numero,linea,cuenta,fecha,definicion,debe,haber,tipo,punteo,factura,proveedor,asi,importediv,divisa,debediv,haberdiv,cambio,referencia,guid) 
							VALUES('''+@USUARIO+''','''+@empresa+''', '+@NUMASIENTO+', '''+@LINASI+''', '''+@CTAPORT+''', '''+@FECHA+''',''N/ FRA ''+@NUM_FA+'' ''+'+@NOMCLI+'
							, 0.0000, ROUND('+@TOTALPOR+'*('+@RECPORT+'/100),2),''0'', 0, '''+@NUM_FA+''',''          '', '''+@ASI3+''',ROUND('+@TOTALPOR+'*('+@RECPORT+'/100),2),''000''
							,0.0000,ROUND('+@TOTALPOR+'*('+@RECPORT+'/100),2),1.000000, ltrim(str('+@NUMASIENTO+')), NEWID())')
						END
				END
									
								
				--09/10/2019 actualizamos/creamos el registro de la cuenta en bal1, bal2 y saldos 
				--08/10/2019 ACTUALIZAMOS SALDOS, BAL1 y BAL2
				insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.saldos where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
				select @HABER=dato from @tbNum delete @tbNum
				if ISNULL(@HABER, -1)=-1
					EXEC('INSERT INTO ['+@GESTION+'].dbo.saldos(empresa,cuenta,debe,haber,importediv,divisa) VALUES('''+@empresa+''','''+@CUENTA+''',0.000000,'+@HABER+',0.000000,''   '')')
				else
					EXEC('update ['+@GESTION+'].dbo.saldos SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
				--bal1
				insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.bal1 where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
				select @HABER=dato from @tbNum delete @tbNum				
				if ISNULL(@HABER, -1)=-1
					EXEC('insert into ['+@GESTION+'].dbo.bal1(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CUENTA+''',0.000000,'+@TOTALDOC+', '''+@CMES+''','' '','''+@CMES2+''')')
				else
					EXEC('update ['+@GESTION+'].dbo.bal1 SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
				SET @CTABAL2=(select LEFT(@CUENTA, 4))
				--bal2
				insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.bal2 where EMPRESA='''+@empresa+''' and CUENTA='''+@CTABAL2+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
				select @HABER=dato from @tbNum delete @tbNum				
				if ISNULL(@HABER, -1)=-1
					EXEC('insert into ['+@GESTION+'].dbo.bal2(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CTABAL2+''',0.000000,'+@TOTALDOC+', '''+@CMES+''','' '','''+@CMES2+''')')
				else
					EXEC('update ['+@GESTION+'].dbo.bal2 SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE empresa='''+@EMPRESA+''' and cuenta='''+@CTABAL2+''' and mes='''+@CMES+''' and MES2='''+@CMES2+'''')
				---fin actualiza saldos
			END							
		END
				
	-- Albert R. 30/06/2020. Revisamos los giros del cliente, si este no tiene ningúno, añadimos un registro con el valor cero ya que sino no se crea la previsión.
	insert @tbInt EXEC('SELECT COUNT(EMPRESA) from ['+@GESTION+'].dbo.giro_cli where CLIENTE='''+@CLIENTE+'''')
	set @cont = (select dato from @tbInt) delete @tbInt
	IF @cont>0 EXEC('INSERT INTO ['+@GESTION+'].dbo.giro_cli (LINEA, CLIENTE, GIRO, VISTA) VALUES (1, '''+@CLIENTE+''', 0, 1)')

	--08/10/2019 agregamos los registros de previ_cl **segun los registros de giros del cliente en gestion!girocli
	declare @tbPREVICL TABLE (mykey int, LINEA int, GIRO int, nroreg bigint)
	insert @tbPREVICL EXEC('select null mykey, LINEA, GIRO, row_number() over(order by linea) as nroreg from ['+@GESTION+'].dbo.giro_cli where CLIENTE='''+@CLIENTE+'''')
	set @cont = (select count(LINEA) from @tbPREVICL)	
	if @cont>0 SET @NUMGIROS=@cont
	if @NUMGIROS<1 SET @NUMGIROS=1

	SET @VALGIRO= round(@TOTALFAC/@NUMGIROS,2)

	while (select count(*) from @tbPREVICL where isnull(mykey,0)=0)>0 begin
		SET @NROREG=(select min(nroreg) from @tbPREVICL where isnull(mykey, 0)= 0 )
		select @LINASIP=LINEA, @DIASGIRO=GIRO from @tbPREVICL where nroreg=@NROREG
		--
		SET @GUID1=NEWID()
		SET @ASI2=(SELECT RIGHT(newid(),20))
		SET @RECAR=round(@BASE*@PORREC/100,2)
		SET @VENCIM=(select CAST(CONVERT(char(11), dateadd(dd, @DIASGIRO, @FECHA), 103) as varchar))
		--09/10/2019 calculamos la fecha de pago si hay diapag y diapag2
		if @DIAPAG>0 begin
			--si el dia de la fecha calculada es menor al dia 1 entonces le asignamos el día 1
			if datepart(dd,@VENCIM)<@DIAPAG
				set @VENCIM=right(replicate('0', 2)+ ltrim(rtrim(str(@DIAPAG))), 2)+'/'+right(replicate('0', 2)+ ltrim(rtrim(str(DATEPART(mm,@VENCIM)))), 2)+'/'+str(datepart(yy,@VENCIM),4)
			--si el dia de la fecha calculada es mayor al día 1 verificamos si el día 2 aplica
			if DATEPART(dd,@VENCIM)>@DIAPAG begin
				--si no hay registrado dia 2 o este está registrado y el día de la fecha calculada es mayor, volvemos a la cuota del día 1
				if @DIAPAG2=0 or DATEPART(dd,@VENCIM)>@DIAPAG2 begin
						--le subimos al mes siguiente y ponemos el día 1
						set @VENCIM=(select CAST(CONVERT(char(11), dateadd(dd, 30, @VENCIM), 103) as varchar))
						set @VENCIM=right(replicate('0', 2)+ ltrim(rtrim(str(@DIAPAG))), 2)+'/'+right(replicate('0', 2)+ ltrim(rtrim(str(DATEPART(mm,@VENCIM)))), 2)+'/'+str(datepart(yy,@VENCIM),4)
				end else begin
					--si el dia de la fecha calculada es menor al día 2 se lo asignamos directamente
					if datepart(dd,@VENCIM)<@DIAPAG2
						set @VENCIM=right(replicate('0', 2)+ ltrim(rtrim(str(@DIAPAG2))), 2)+'/'+right(replicate('0', 2)+ ltrim(rtrim(str(DATEPART(mm,@VENCIM)))), 2)+'/'+str(datepart(yy,@VENCIM),4)
				end
			end
		end
				
		EXEC('INSERT INTO ['+@COMUN+'].dbo.previ_cl  (usuario,empresa,cliente,factura,orden,vencim,importe,banco,emision,impagado,impago,gastos,remesa,fecreme,
		asiento,impreso,cobro,vencim2,concepto,periodo,f_pago,pagare,imppagare,num_banco,pendiente,refundir,importediv,divisa,cambio,vendedor,cobrador,
		asi,recepcion,recibida,reclamacio,tipoprev,libre_1,fec_oper,doccargo,numcred,bco_org,mandato,recc) 
		VALUES('''+@USUARIO+''','''+@empresa+''','''+@CLIENTE+''','''+@NUM_FA+''','''+@LINASIP+''','''+@VENCIM+''','''+@VALGIRO+''','''','''+@FECHA+''',0,''N'',0.0000,0,null
				, 0,''N'',null,null,'''',DATEPART(yy,'''+@FECHA+'''),'''+@FPAG+''','''',0.0000,1,0,'''', '''+@VALGIRO+''', '''+@DIVISA+''', '''+@CAMBIO+''', '''+@VENDEDOR+'''
				, '''+@VENDEDOR+''','''',null,0,null,'''','''', '''+@FECHA+''','''','''','''', '''+@MANDATO+''',0)')
				
		EXEC('INSERT INTO ['+@COMUN+'].dbo.adiprevicl  (periodo,empresa,factura,orden,impagado,pendiente,liquicob,carga) 
				VALUES(DATEPART(yy,'''+@FECHA+'''), '''+@empresa+''', '''+@NUM_FA+''', 1 ,0 ,0 ,'''','''')')

		update @tbPREVICL set mykey = 1 where nroreg=@NROREG
	end
	delete @tbPREVICL
				
				
	--10/05/2019 ciclo pronto pago
	if @PRONTO>0 BEGIN
		SET @GUID1=NEWID()
		SET @ASI2=(SELECT RIGHT(newid(),20)) 
		insert @tbNum EXEC('SELECT ABS(SUM(DEBE-HABER)) FROM ['+@GESTION+'].dbo.asientos WHERE NUMERO='+@NUMASIENTO)
		SELECT @TOTALDOC=(select dato from @tbNum) delete @tbNum
		SET @LINASI=@LINASI+1
		insert @tbStr EXEC('select VDTOS from ['+@COMUN+'].dbo.codcom')
		select @CUENTA=(select dato from @tbStr) delete @tbStr

		EXEC('INSERT INTO ['+@GESTION+'].dbo.asientos  (usuario,empresa,numero,linea,cuenta,fecha,definicion,debe,haber,tipo,
		punteo,factura,proveedor,asi,importediv,divisa,debediv,haberdiv,cambio,referencia,guid) 
		VALUES('''+@USUARIO+''','''+@empresa+''', '+@NUMASIENTO+', '''+@LINASI+''', '''+@CTAPORTES+''', '''+@FECHA+''',''N/ FRA ''+'''+@NUM_FA+'''+'' ''+'''+@NOMCLI+'''
		, '+@TOTALDOC+', 0.0000,''0'', 0, '''+@NUM_FA+''',''          '', '''+@ASI2+''', '+@TOTALDOC+',''000'','+@TOTALDOC+',0.0000,1.000000, ltrim(str('+@NUMASIENTO+')), NEWID())')

		--10/05/2019 agrego registro de secundarias copiado
		--09/10/2018 insertamos secundarias si existen
		set rowcount 0
		INSERT @tbSecundarias EXEC('select null mykey, secundaria, porcentaje, row_number() over(order by secundaria) as nroreg from ['+@GESTION+'].DBO.otras where codigo='''+@CUENTA+'''')
		while (select count(*) from @tbSecundarias where isnull(mykey,0)=0)>0 begin
			set rowcount 0
			SET @NROREG=(select min(nroreg) from @tbSecundarias where isnull(mykey, 0)= 0 )
			SELECT @SECU=SECUNDARIA, @PORSE=PORCENTAJE from @tbSecundarias where nroreg=@NROREG
			EXEC('INSERT INTO ['+@GESTION+'].dbo.otrasien (empresa,secundar,codcuen,fecha,debe,haber,asi,debediv,haberdiv,divisa) 
			VALUES ('''+@empresa+''','''+@SECU+''','''+@CUENTA+''','''+@FECHA+''', ROUND('+@TOTALDOC+'*'+@PORSE+'/100,2), 0.0000, '''+@ASI2+''', 0.0000,0.0000,''   '')')

			update @tbSecundarias set mykey=1 where nroreg=@NROREG
		end 
		delete @tbSecundarias				
		--fin secundarias

		--09/10/2019 actualizamos/creamos el registro de la cuenta en bal1, bal2 y saldos 
		--08/10/2019 ACTUALIZAMOS SALDOS, BAL1 y BAL2
		insert @tbNum EXEC('select DEBE from ['+@GESTION+'].dbo.saldos where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		select @DEBE=dato from @tbNum delete @tbNum
		if ISNULL(@DEBE, -1)=-1
			EXEC('INSERT INTO ['+@GESTION+'].dbo.saldos (empresa,cuenta,debe,haber,importediv,divisa) VALUES ('''+@empresa+''','''+@CUENTA+''','+@DEBE+',0.000000,0.000000,''   '')')        
		else
			EXEC('update ['+@GESTION+'].dbo.saldos SET DEBE='+@DEBE+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		--bal1
		insert @tbNum EXEC('select DEBE from ['+@GESTION+'].dbo.bal1 WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		select @DEBE=dato from @tbNum delete @tbNum		
		if ISNULL(@DEBE, -1)=-1
			EXEC('insert into ['+@GESTION+'].dbo.bal1(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CUENTA+''','+@TOTALDOC+', 0.000000,'''+@CMES+''','' '','''+@CMES2+''')')
		else
			EXEC('update ['+@GESTION+'].dbo.bal1 SET DEBE='+@DEBE+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		SET @CTABAL2=(select LEFT(@CUENTA, 4))
		--bal2
		insert @tbNum EXEC('select @DEBE=DEBE from ['+@GESTION+'].dbo.bal2 where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		select @DEBE=dato from @tbNum delete @tbNum			
		if ISNULL(@DEBE, -1)=-1
			EXEC('insert into ['+@GESTION+'].dbo.bal2(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CTABAL2+''','+@TOTALDOC+', 0.000000,'''+@CMES+''','' '','''+@CMES2+''')')
		else
			EXEC('update ['+@GESTION+'].dbo.bal2 SET DEBE='+@DEBE+'+'+@TOTALDOC+' WHERE empresa='''+@EMPRESA+''' and cuenta='''+@CTABAL2+''' and mes='''+@CMES+''' and MES2='''+@CMES2+'''')
		---fin actualiza saldos

	END
				
	-- Albert R. 20/11/18 - creación registro para las impresiones
	exec [pImprimir_Docs] @EMPRESA, @LETRA, @NUMERO, @terminal, @num_fa, @imprimir, @imp_eti, @ETICAJAS, @bultos, @nEti
				

	SELECT @respuestaSP='Factura nueva: '+@NUM_FA+' Fecha: '+@FECHA
	--COMMIT TRAN
	RETURN -1
END TRY
BEGIN CATCH 
	--ROLLBACK TRAN 
	DECLARE		@ErrorMessage NVARCHAR(4000);  
	DECLARE		@ErrorSeverity INT;  
	DECLARE		@ErrorState INT;  

	SELECT		@ErrorMessage = ERROR_MESSAGE(),  
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE();  

	RAISERROR	(@ErrorMessage, -- Message text.  
				@ErrorSeverity, -- Severity.  
				@ErrorState -- State.  
				);  
	SET @Mensaje='","ERROR":"NO se ha podido crear la factura'
	SELECT @Mensaje AS JAVASCRIPT
	RETURN 0
END CATCH