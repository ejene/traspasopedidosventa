﻿
CREATE PROCEDURE [dbo].[pAlbaranDeVenta] (@parametros varchar(max))
AS
BEGIN TRY		
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare   @numero varchar(20)	= (select JSON_VALUE(@parametros,'$.numero'))
			, @letra char(2)		= (select JSON_VALUE(@parametros,'$.letra'))
			, @bultos varchar(5)	= (select JSON_VALUE(@parametros,'$.bultos'))
			, @cajas varchar(5)		= (select JSON_VALUE(@parametros,'$.cajas'))
			, @impAlb int			= isnull(cast((select JSON_VALUE(@parametros,'$.impAlb')) as int),0)
			, @impEti int			= isnull(cast((select JSON_VALUE(@parametros,'$.impEti')) as int),0)
			, @FacturaDirecta int	= isnull(cast((select JSON_VALUE(@parametros,'$.FacturaDirecta')) as int),0)
			, @usuarioSesion varchar(50)= (select JSON_VALUE(@parametros,'$.usuarioSesion'))			

	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EMPRESA char(2), @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select @EMPRESA=Empresa, @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------

	declare @sql varchar(max)
	declare @nu varchar(50) = replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 

	declare   @nEti int = 1
			, @CrearAV varchar(max) = ''
			, @printerEti varchar(100) = ''
			, @printerDoc varchar(100) = ''
			, @reportEti  varchar(100) = ''
			, @reportAlb  varchar(100) = ''
			, @reportFac  varchar(100) = ''
			, @msj varchar(max) = ''
			, @unico varchar(50) = ''
			, @elPDF varchar(100) = ''
			, @error int = 0
			, @cliente varchar(20)
			, @copiasAlb int = 1
			, @copiasFac int = 1	
	

	update [Traspaso_Temporal] set bultos=@bultos, eticajas=@cajas, impAlb=@impAlb, impEti=@impEti where empresa=@EMPRESA and NUMERO=@numero and LETRA=@letra	

	-- Equipo del Usuario -- (DISTECO)
	declare @EquipoUsuario varchar(50) = 'WebApp'
	if @usuarioSesion='1544458254550078' set @EquipoUsuario='PC-CAMARA2'
	if @usuarioSesion='1544458275396522' set @EquipoUsuario='DS4JX7688'

	-- obtener impresoras y reports ---------------------------------------------------------------------------------
	if @impEti=1 begin
		set @printerEti = (select isnull(impEti,'') from [usuarios_impresoras] where usuario=@usuario)		
		set @reportEti  = (select isnull(reportEtiquetas,'') from [usuarios_reports] where usuario=@usuario)
		if @reportEti='' set @reportEti = (select isnull(report,'') from [ConfigIdiomaReport] where idioma = 
											(select top 1 IDIOMA_IMP from d_Busca_PV 
											where concat(empresa,numero,letra) collate Modern_Spanish_CI_AI=concat(@EMPRESA,@numero,@letra)) collate Modern_Spanish_CI_AI
											and LEFT(tipo,2)='03'
										  )
		if @reportEti='' set @reportEti = (select isnull(report,'') from [ConfigIdiomaReport] where idioma='000' and LEFT(tipo,2)='03')
		if  @printerEti='' or @reportEti='' set @error=1
	end
	
	if @impAlb=1 begin
		set @printerDoc = (select impDocu from [usuarios_impresoras] where usuario=@usuario)
		set @reportAlb  = (select reportAlbaran from [usuarios_reports] where usuario=@usuario)
		if	@reportAlb='' set @reportAlb = (select isnull(report,'') from [ConfigIdiomaReport] where idioma = 
											(select top 1 IDIOMA_IMP from d_Busca_PV 
											where concat(empresa,numero,letra) collate Modern_Spanish_CI_AI=concat(@EMPRESA,@numero,@letra)) collate Modern_Spanish_CI_AI
											and LEFT(tipo,2)='01'
										  )
		if @reportAlb='' set @reportAlb = (select isnull(report,'') from [ConfigIdiomaReport] where idioma='000' and LEFT(tipo,2)='01')			
		if @printerDoc='' or @reportAlb=''set @error=2
	end
	
	if @FacturaDirecta=1 begin
		set @printerDoc = (select impDocu from [usuarios_impresoras] where usuario=@usuario)
		set @reportFac  = (select reportFactura from [usuarios_reports] where usuario=@usuario)
		if	@reportFac='' set @reportFac = (select isnull(report,'') from [ConfigIdiomaReport] where idioma = 
											(select top 1 IDIOMA_IMP from d_Busca_PV 
											where concat(empresa,numero,letra) collate Modern_Spanish_CI_AI=concat(@EMPRESA,@numero,@letra)) collate Modern_Spanish_CI_AI
											and LEFT(tipo,2)='02'
										  )
		if @reportFac='' set @reportFac = (select isnull(report,'') from [ConfigIdiomaReport] where idioma='000' and LEFT(tipo,2)='02')
		if  @printerDoc='' or @reportFac=''set @error=3
	end

	-- --------------------------------------------------------------------------------------------------------------
	--	generar albarán de venta 
		declare @sp int  
		EXEC @sp = [pCrearAV] @EMPRESA, @numero, @letra, @EquipoUsuario, @impAlb, @impEti, @CrearAV output, @nEti
		if @sp<>-1 BEGIN
			select CONCAT('{"error":"',@CrearAV,'"}') as JAVASCRIPT
			return -1
		END
	-- --------------------------------------------------------------------------------------------------------------

	-- tabla temporal para recopilación de datos ------------------------------------------------------
	declare @tablaTemp TABLE (C_ALBAVEN int, copiasFac int)
	SET @sql = 'select C_ALBAVEN as C_ALBAVEN
				, (select COPIA_FRA from ['+@GESTION+'].dbo.clientes where CODIGO='''+@cliente+''') as copiasFac
				from ['+@GESTION+'].dbo.factucnf'
	INSERT @tablaTemp EXEC(@sql)
	SELECT @copiasAlb=C_ALBAVEN, @copiasFac=copiasFac FROM @tablaTemp
	delete @tablaTemp

	-- guardar datos en tabla de impresión --------------------------------------------------------------------------
	if @error=0 begin
		if @impEti=1 or (select activo from ConfigApp where objeto='impEtiquetas')=1 begin
			set @unico = 'E' + replace(replace(replace(replace(replace(convert(varchar(50),getdate(),121),' ',''),'/',''),':',''),'.',''),'-','')
			set @elPDF = replace(CONCAT('Etiquetas_',@EMPRESA,@letra,@CrearAV,'.pdf'),' ','_');
			insert into MI_ImpresionPDF (id, usuario, terminal, tipo, pdf, impresora, crReport, crNumero, crEmpresa, crLetra, bultos) 
			values (@unico,@usuario,'WebApp','Etiquetas',@elPDF,@printerEti,@reportEti,@CrearAV,@EMPRESA,@letra,@bultos)
		end

		if @impAlb=1 or (select activo from ConfigApp where objeto='impAlbGen')=1 begin 
			set @unico ='A' +  replace(replace(replace(replace(replace(convert(varchar(50),getdate(),121),' ',''),'/',''),':',''),'.',''),'-','')
			set @elPDF = replace(CONCAT('Albaran',@EMPRESA,@letra,@CrearAV,'.pdf'),' ','_');
			insert into MI_ImpresionPDF (id, usuario, terminal, tipo, pdf, impresora, crReport, crNumero, crEmpresa, crLetra) 
			values (@unico,@usuario,'WebApp','Albaran',@elPDF,@printerDoc,@reportAlb,ltrim(rtrim(@CrearAV)),@EMPRESA,@letra)
		end

		if @FacturaDirecta=1 begin
			set @cliente = (select CLIENTE from vFacturas where empresa=@EMPRESA and numfra=CONCAT(@letra,@numero))
			set @unico = 'F' + replace(replace(replace(replace(replace(convert(varchar(50),getdate(),121),' ',''),'/',''),':',''),'.',''),'-','')
			set @elPDF = replace(CONCAT('"Factura"',@EMPRESA,@letra,@CrearAV,'.pdf'),' ','_');
			insert into MI_ImpresionPDF (id, usuario, terminal, tipo, pdf, impresora, crReport, crNumero, crEmpresa, crLetra) 
			values (@unico,@usuario,'WebApp','Factura',@elPDF,@printerDoc,@reportFac,ltrim(rtrim(@CrearAV)),@EMPRESA,@letra)
		end
	end
	
	--	actualizar C_PEDIVE TRASPASADO=1 y eliminar registro tabla [Traspasar_Pedido]
		EXEC('update ['+@GESTION+'].dbo.c_pedive set TRASPASADO=1 where CONCAT(EMPRESA,NUMERO,LETRA)=CONCAT('''+@EMPRESA+''','''+@numero+''','''+@letra+''')')
		delete [Traspasar_Pedido] where CONCAT(empresa,numero,letra)=CONCAT(@EMPRESA,@numero,@letra)
	
	select CONCAT('{"Error":"","respuesta":"',@CrearAV,'"}') as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	select CONCAT('{"error":"', replace(ERROR_MESSAGE(),'"','-'),'"}') as JAVASCRIPT
	RETURN 0
END CATCH