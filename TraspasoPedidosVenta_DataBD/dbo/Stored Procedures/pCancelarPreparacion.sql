﻿CREATE PROCEDURE [dbo].[pCancelarPreparacion] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON

	-- datos JSON ----------------------------------------------------------------------------------
	declare	  @modo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
			, @pedido varchar(50) = isnull((select JSON_VALUE(@parametros,'$.pedido')),'')
			, @serie varchar(10) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
			, @linea int = cast(isnull((select JSON_VALUE(@parametros,'$.linea')),'0') as int)
			, @almacen char(2) = isnull((select JSON_VALUE(@parametros,'$.almacen')),'')
	declare   @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-------------------------------------------------------------------------------------------------
	-- variables App
	declare @laSesion varchar(50) = @usuario
	declare @nu varchar(50) = 
	replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 
	declare @empresa char(2) = (select Empresa from Configuracion)
	---------------------------------------------------------------------------------------------------------------


	-- cancelar la preparación de un artículo
	if @modo='cancelarPrepArt' BEGIN
		delete FROM [Traspaso_Temporal] where empresa=@empresa and numero=@pedido and letra=@serie and linea=@linea
		RETURN -1
	END
	

	-- cancelar toda la preparación
	delete FROM [Traspaso_Temporal] where empresa=@empresa and numero=@pedido and letra=@serie

	RETURN -1
END TRY

BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
