﻿CREATE PROCEDURE [dbo].[pActualizaLineaTraspasoLote] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare	  @nu varchar(50) = isnull((select JSON_VALUE(@parametros,'$.nu')),'')
			, @pedido varchar(50) = isnull((select JSON_VALUE(@parametros,'$.pedido')),'')
			, @serie varchar(10) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
			, @articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
			, @almacen varchar(10) = isnull((select JSON_VALUE(@parametros,'$.almacen')),'')
			, @lote varchar(50) = isnull((select JSON_VALUE(@parametros,'$.lote')),'')
			, @linea int = cast(isnull((select JSON_VALUE(@parametros,'$.linea')),'0') as int)
			, @unidades int = cast(isnull((select JSON_VALUE(@parametros,'$.unidades')),'0') as int)
			, @peso varchar(10) = isnull((select JSON_VALUE(@parametros,'$.peso')),'')
			, @cajas int = cast(isnull((select JSON_VALUE(@parametros,'$.cajas')),'0') as int)
	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EMPRESA char(2), @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select  @EMPRESA=Empresa, @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------	

	declare   @cliente varchar(20)
			, @ruta char(2)
			, @unicaja int
			, @caducidad smalldatetime
			, @sql varchar(max)

	-- tabla temporal para recopilación de datos ------------------------------------------------------
	declare @tablaTemp TABLE (CLIENTE nvarchar(20), RUTA varchar(10), UNICAJA int, caducidad smalldatetime)
	SET @sql = 'select top 1 CLIENTE
				, (select top 1 ruta from Detalle_Pedidos where numero='''+@pedido+''') as RUTA
				, (select UNICAJA from ['+@GESTION+'].dbo.articulo where CODIGO='''+@articulo+''') as UNICAJA
				, (select CADUCIDAD from ['+@LOTES+'].[dbo].[stocklotes] where ALMACEN='''+@almacen+''' and ARTICULO='''+@articulo+''' and LOTE='''+@lote+''') as caducidad
				from Detalle_Pedidos where numero='''+@pedido+'''  and LETRA='''+@serie+''''
	INSERT @tablaTemp EXEC(@sql)
	SELECT @cliente=CLIENTE, @ruta=RUTA, @unicaja=UNICAJA, @caducidad=caducidad FROM @tablaTemp
	delete @tablaTemp

	-- si existe la linea la borramos
	delete from [Traspaso_Temporal] 
	where empresa=@EMPRESA and numero=@pedido and letra=@serie and linea=@linea and lote=@lote

	-- guardamos la linea en la tabla temporal
	insert into [Traspaso_Temporal] 
	(sesion, id, empresa, numero, letra, linea, articulo, lote, cajas, unidades, peso, cliente, ruta, almacen, caducidad) 
	values (@usuario, @nu, @EMPRESA, @pedido, @serie, @linea, @articulo, @lote, @cajas, @unidades, @peso, @cliente, @ruta
	, @almacen, @caducidad)

	select @ruta as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select @CatchError as JAVASCRIPT
	RETURN 0
END CATCH