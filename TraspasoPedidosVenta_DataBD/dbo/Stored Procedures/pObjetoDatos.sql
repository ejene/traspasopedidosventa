﻿CREATE PROCEDURE [dbo].[pObjetoDatos] @parametros varchar(max)=''
AS
BEGIN TRY
	declare   @objeto varchar(50) = (select JSON_VALUE(@parametros,'$.objeto'))

	if @objeto='Idiomas' BEGIN
		select isnull((select ltrim(rtrim(Codigo)) as Codigo, ltrim(rtrim(Nombre)) as Nombre from Idiomas_Idiomas for JSON AUTO,INCLUDE_NULL_VALUES),'[]') as JAVASCRIPT
	END

	if @objeto='Tipos' BEGIN
		select isnull((select ltrim(rtrim(Codigo)) as Codigo, ltrim(rtrim(Nombre)) as Nombre from Idiomas_Tipos for JSON AUTO,INCLUDE_NULL_VALUES),'[]') as JAVASCRIPT
	END

	RETURN -1
END TRY

BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH