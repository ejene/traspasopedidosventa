﻿
CREATE PROCEDURE [dbo].[pRutas] @parametros varchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

		select isnull((select codigo, nombre from vRutas order by codigo ASC FOR JSON AUTO, INCLUDE_NULL_VALUES),'[]') as JAVASCRIPT

		return -1 
	END TRY
	BEGIN CATCH
		DECLARE @CatchError NVARCHAR(MAX)
		SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
		RAISERROR(@CatchError,12,1)
		RETURN 0
	END CATCH
END
