﻿CREATE PROCEDURE [dbo].[pComunes] @parametros varchar(max)='[]'
AS
BEGIN TRY	
	declare @modo varchar(50) = (select JSON_VALUE(@parametros,'$.modo'))

	Declare   @nombre varchar(10)
			, @comunes varchar(max)=''

	Declare elCursor CURSOR for
	SELECT name FROM sys.databases WHERE left(name,4) = 'COMU'
	OPEN elCursor FETCH NEXT FROM elCursor INTO @nombre
	WHILE (@@FETCH_STATUS=0)BEGIN
		set @comunes = @comunes + '{"nombre":"'+@nombre+'"}'

		declare @exec1 varchar(1000) = '
			declare @comunes varchar(max)=''''
			if (select left(PRODUCTNAME,4) from ['+@nombre+'].dbo.codcom)=''SAGE''
			set @comunes = @comunes + ''{"nombre":"'+@nombre+'"}''
		'
		exec (@exec1)

	FETCH NEXT FROM elCursor INTO @nombre END Close elCursor Deallocate elCursor
	
	select '['+replace(isnull(@comunes,'[]'),'}{','},{')+']'  as JAVASCRIPT   

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select @CatchError as JAVASCRIPT
	RETURN 0
END CATCH