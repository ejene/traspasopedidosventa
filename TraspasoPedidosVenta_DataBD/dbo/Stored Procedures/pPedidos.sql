﻿CREATE PROCEDURE [dbo].[pPedidos] ( @parametros varchar(max) )
AS
SET NOCOUNT ON
BEGIN TRY	
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare @modo     varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
		,	@Empresa  char(2)     = isnull((select JSON_VALUE(@parametros,'$.Empresa')),'')
		,	@Numero	  varchar(20) = isnull((select JSON_VALUE(@parametros,'$.Numero')),'')
		,	@Serie    varchar(10) = isnull((select JSON_VALUE(@parametros,'$.Serie')),'')
		,	@Ruta     varchar(10) = isnull((select JSON_VALUE(@parametros,'$.Ruta')),'')
		,	@Almacen  varchar(10) = isnull((select JSON_VALUE(@parametros,'$.Almacen')),'')
		,	@articulo varchar(10) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
		,	@linia    varchar(10) = isnull((select JSON_VALUE(@parametros,'$.linia')),'')
		,	@cajas    varchar(10) = isnull((select JSON_VALUE(@parametros,'$.cajas')),'0')
		,	@uds      varchar(10) = isnull((select JSON_VALUE(@parametros,'$.uds')),'0')
		,	@peso     varchar(10) = isnull((select JSON_VALUE(@parametros,'$.peso')),'0.000')
		,	@pedidoActivo  varchar(50) = isnull((select JSON_VALUE(@parametros,'$.pedidoActivo')),'')
		,	@error   varchar(max) = ''
		,	@datos   varchar(max) = ''
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select  @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------	
	-- variables
	declare @sql varchar(max)
	-------------------------------------------------------------------------------------------------


	if @modo='dameLineas' BEGIN
		set @datos =	isnull((
							select a.*
								,  art.UNICAJA, case when ltrim(rtrim(art.PESO))='' then '0' else ltrim(rtrim(art.PESO)) end as PESO
								,  vas.ArticuloStock
							from v_d_pedive a
							inner join vArticulos art on art.CODIGO=a.articulo
							left join  vArticulosStock vas on vas.CODIGO=a.articulo and vas.almacen=@Almacen
							where a.empresa COLLATE Modern_Spanish_CS_AI=@Empresa 
								  and ltrim(rtrim(a.numero)) COLLATE Modern_Spanish_CS_AI=ltrim(rtrim(@Numero)) 
								  and a.letra COLLATE Modern_Spanish_CS_AI=@Serie 
								  and a.servidas=0
								  and a.articulo COLLATE Modern_Spanish_CS_AI not in (
										select articulo from Traspaso_Temporal
										where  empresa COLLATE Modern_Spanish_CS_AI=@Empresa
											and ltrim(rtrim(numero)) COLLATE Modern_Spanish_CS_AI=ltrim(rtrim(@Numero)) 
											and letra   COLLATE Modern_Spanish_CS_AI=@Serie
								  )
							for JSON AUTO,INCLUDE_NULL_VALUES
						),'[]')
	END


	if @modo='intercambiarArticulo' BEGIN 
		declare @articuloAnterior varchar(20) = isnull((select JSON_VALUE(@parametros,'$.articuloAnterior')),'')
		declare @udsAnterior numeric(15,6) = cast(isnull((select JSON_VALUE(@parametros,'$.udsAnterior')),'0.00') as numeric(15,6))
		declare @articuloNuevo varchar(20) = isnull((select JSON_VALUE(@parametros,'$.articuloNuevo')),'')
		declare @linea varchar(10) = isnull((select JSON_VALUE(@parametros,'$.linea')),'')

		-- calcular cajas y peso y stock para el @articuloNuevo
		declare @articuloNuevoStock numeric(15,6)
		select @articuloNuevoStock=ArticuloStock from vArticulosStock where CODIGO=@articuloNuevo and almacen=@Almacen
		if @articuloNuevoStock<0.000001 BEGIN select 'Error! - El artículo no tiene Stock!' as JAVASCRIPT return -1 END
		if @articuloNuevoStock<@udsAnterior BEGIN select 'Error! - El artículo no tiene suficiente Stock!' as JAVASCRIPT return -1 END
		
		declare @nuevoCajas int=0
		declare @nuevoUNICAJA int = (select isnull(UNICAJA,0) from vArticulos where CODIGO=@articuloNuevo)
		if @nuevoUNICAJA>0 set  @nuevoCajas=@udsAnterior/@nuevoUNICAJA

		declare @nuevoPeso numeric(15,6)=0.000
		declare @nuevoArtPeso numeric(15,6) = (select case when PESO is null then '0.000' 
											when PESO='' then '0.000' else PESO END from vArticulos where CODIGO=@articuloNuevo)
		if @nuevoArtPeso>0 set  @nuevoPeso=@udsAnterior*@nuevoArtPeso


		declare @param varchar(1000) =	concat('{"numero":"',@Numero,'","linea":"',@linea,'","letra":"',@Serie,'"'
										,',"articulo":"',@articuloNuevo,'","uds":"',@udsAnterior,'"'
										,',"peso":"',@nuevoPeso,'","tarifa":""}')
		begin try EXEC pCalculosPV @param end try begin catch select 'Error! - EXEC pCalculosPV @param: '+@param  as JAVASCRIPT return -1 end catch 

		-- actualizar la linea
		set @sql = '
		update ['+@GESTION+'].[dbo].d_pedive
		set ARTICULO='''+@articuloNuevo+''', DEFINICION=(select NOMBRE from vArticulos where CODIGO='''+@articuloNuevo+''')
			, CAJAS='+cast(@nuevoCajas as varchar(50))+', UNIDADES='+cast(@udsAnterior as varchar(50))+', PESO='+cast(@nuevoPeso as varchar(50))+'
		where EMPRESA='''+@Empresa+''' and NUMERO='''+@Numero+''' and LETRA='''+@Serie+''' and ARTICULO='''+@articuloAnterior+''' and LINIA='+@linea+'
		'
		EXEC(@sql)

		select CONCAT('{"cajas":"',@nuevoCajas,'","uds":"',@udsAnterior,'","peso":"',@nuevoPeso,'"}')  as JAVASCRIPT
	END


	if @modo='modificarPedido' BEGIN
		declare @valor varchar(1000) 
		declare cur CURSOR for select [value] from openjson(@parametros,'$.lineasModificadas')
		OPEN cur FETCH NEXT FROM cur INTO @valor
		WHILE (@@FETCH_STATUS=0) BEGIN

			set @linia = isnull((select JSON_VALUE(@valor,'$.linea')),'')
			set @articulo = isnull((select JSON_VALUE(@valor,'$.articulo')),'')
			set @cajas = isnull((select JSON_VALUE(@valor,'$.cajas')),'0')
			set @uds = isnull((select JSON_VALUE(@valor,'$.unidades')),'0')
			set @peso = isnull((select JSON_VALUE(@valor,'$.peso')),'0.000')

			-- actualizar linea
			set @sql = '
			update ['+@GESTION+'].[dbo].d_pedive
			set ARTICULO='''+@articulo+''', CAJAS='+@cajas+', UNIDADES='+@uds+', PESO='''+@peso+'''
			,	DEFINICION=(select nombre from vArticulos where CODIGO='''+@articulo+''')
			where EMPRESA='''+@Empresa+''' and NUMERO='''+@Numero+''' and LETRA='''+@Serie+''' and ARTICULO='''+@articuloAnterior+''' and LINIA='+@linea+'
			'
			EXEC(@sql)

			-- recalcular linea
			declare @prmtrs varchar(max) = CONCAT('{"numero":"',@Numero,'","letra":"',@Serie,'","linea":"',@linia,'"',',"articulo":"',@articulo,'","uds":"',@uds,'","peso":"',@peso,'"}')
			EXEC [pCalculosPV] @prmtrs

			FETCH NEXT FROM cur INTO @valor
		END CLOSE cur deallocate cur		
	END


	if @modo='traspasarObservacion' BEGIN		
		if exists (
			select * from Traspaso_Temporal 
			where concat(empresa,ltrim(rtrim(numero)),letra,linea)=concat(@Empresa,ltrim(rtrim(@Numero)),@Serie,@linia)
		)BEGIN
			delete Traspaso_Temporal 
			where concat(empresa,ltrim(rtrim(numero)),letra,linea)=concat(@Empresa,ltrim(rtrim(@Numero)),@Serie,@linia)
			select 'borrado' as JAVASCRIPT
		END ELSE BEGIN
			declare @nu varchar(50) = 
			replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 

			insert into Traspaso_Temporal 
			(sesion, id, empresa, numero, letra, linea, articulo, lote, cajas, unidades, peso, cliente, ruta, almacen, caducidad)
			values ('', @nu, @Empresa, @Numero, @Serie, @linia,'','','0','0','0','','','','')

			select 'insertado' as JAVASCRIPT
		END
		
		return -1
	END


	if @modo='finalizarPedido' BEGIN
		set @sql = 'update ['+@GESTION+'].dbo.c_pedive set TRASPASADO=1 where EMPRESA='''+@Empresa+''' and NUMERO='''+@Numero+''' and LETRA='''+@Serie+''''
		EXEC(@sql)
		select 'OK' as JAVASCRIPT
		return -1
	END


	if @datos='' set @datos='[]'
	
	select CONCAT('{"error":"',@error,'","datos":',@datos,'}') as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
	select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
