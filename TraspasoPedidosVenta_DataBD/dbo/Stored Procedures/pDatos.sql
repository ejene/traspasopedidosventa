﻿CREATE PROCEDURE [dbo].[pDatos] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare	@modo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
		,	@articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')

	declare	@usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
		,	@rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EMPRESA char(2), @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select @EMPRESA=Empresa, @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------	
	
	if @modo='dameUNICAJA' EXEC('select (select isnull(UNICAJA,0) from ['+@GESTION+'].[dbo].articulo where CODIGO='''+@articulo+''') as JAVASCRIPT')

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH