﻿CREATE PROCEDURE [dbo].[pTraspasoCompleto] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON
	-----------------------------------------------------------------------------------------------
	-- Variables de Configuración 
	-----------------------------------------------------------------------------------------------
	declare @EJERCICIO char(4)
	declare @GESTION char(6)
	declare @COMUN char(8)
	declare @LOTES char(8)
	select @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-----------------------------------------------------------------------------------------------
	-- Parámetros JSON 
	declare	  @numero varchar(50) = isnull((select JSON_VALUE(@parametros,'$.numero')),'')
			, @serie varchar(10) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
			, @ruta varchar(10) = isnull((select JSON_VALUE(@parametros,'$.ruta')),'')
			, @almacen varchar(10) = isnull((select JSON_VALUE(@parametros,'$.almacen')),'')
			, @articulos varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulos')),'')

	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-------------------------------------------------------------------------------------------------
	
	declare @empresa char(2) = (select Empresa from Configuracion)
	declare @nu varchar(50) = 
		replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 

	declare @artSinStock varchar(max) = ''


	-- artículos
	declare @articulo varchar(50)
		,	@unidades numeric(15,6)
		,	@linea int
		,	@cajas numeric(15,6)
		,	@peso numeric(15,6)
		,	@art varchar(max) 

	declare cur CURSOR for select [value] from openjson(@parametros,'$.articulos')
	OPEN cur FETCH NEXT FROM cur INTO @art
	WHILE (@@FETCH_STATUS=0) BEGIN
		select @articulo = isnull((select JSON_VALUE(@art,'$.articulo')),'')
		select @cajas = cast(isnull((select JSON_VALUE(@art,'$.cajas')),'0.00') as numeric(15,6))
		select @unidades = cast(isnull((select JSON_VALUE(@art,'$.unidades')),'0.00') as numeric(15,6))
		select @peso = cast(isnull((select JSON_VALUE(@art,'$.peso')),'0.00') as numeric(15,6))
		select @linea = cast(isnull((select JSON_VALUE(@art,'$.linea')),'0') as int)
		
		declare   @cliente varchar(20)	= (select top 1 CLIENTE from Detalle_Pedidos where numero=@numero)
				, @artUnicaja int		
				, @artPeso varchar(10)	
				, @newCajas int			= 0
				, @newUnidades int		= 0
				, @newPeso varchar(10)	= ''	

		declare   @artStock numeric(15,6)
				, @pedCliente varchar(20)
				, @caducidad smalldatetime
				, @lote varchar(50) = '-'
				, @controlStock bit				
			
		-- Tabla temporal para recopilación de datos ---------------------------------------------------------------------------------------------------------------------
		DECLARE @tbTabla TABLE (UNICAJA int, PESO varchar(20), stock bit, artStock numeric(15,6), pedCliente varchar(20))
		DECLARE @sql nvarchar(max)
		SET @sql = 'select UNICAJA,PESO, stock 
					, COALESCE((select max(FINAL) from ['+@GESTION+'].dbo.stocks2 where ARTICULO='''+@articulo+'''),0.00) as artStock
					, (select CLIENTE from ['+@GESTION+'].[dbo].c_pedive where EMPRESA='''+@empresa+''' and NUMERO='''+@numero+''' and LETRA='''+@serie+''') as pedCliente
					from ['+@GESTION+'].dbo.articulo where CODIGO='''+@articulo+''''
		INSERT @tbTabla EXEC(@sql)
		select @artUnicaja=UNICAJA, @artPeso=PESO, @controlStock=stock, @artStock=artStock, @pedCliente=pedCliente from @tbTabla
		delete @tbTabla
		-------------------------------------------------------------------------------------------------------------------------------------------------------------------

		if @artStock<@unidades and @controlStock=0  
		set @artSinStock = @artSinStock + ',{"articuloSinStock":"'+@artSinStock+'"}'


		--	si trabaja con lotes 
		if (select count(NUMERO) from vBuscarLotes 	where EMPRESA=@empresa and LETRA=@serie and NUMERO=@numero 
			and ARTICULO=@articulo and LINIA=@linea  and Llote is not null)>0 BEGIN
					
				--	Trabaja con lotes ------------------------------
					declare @_pesoArticulo varchar(10), @_Lcaducidad smalldatetime, @_Llote varchar(50)
					, @_Lunidades numeric(15,6), @_Lpeso numeric(15,6), @_ARTICULO varchar(20), @_UNIDADES numeric(15,6)
					, @_SERVIDAS numeric(15,6), @_UNICAJA numeric(15,6), @_pCajas numeric(15,6), @_pCajaServ numeric(15,6)
					, @_tempCajas numeric(15,6), @_tempMaxCajas numeric(15,6), @_tempPeso varchar(10), @_tempUnidades numeric(15,6)
							  
					declare @unidadesG numeric(15,6) = @unidades					 
							, @cajasG numeric(15,6)
							, @pesoG numeric(15,6)			

					declare elCursor CURSOR LOCAL STATIC for
						select pesoArticulo, Lcaducidad, Llote, Lunidades, Lpeso, ARTICULO, UNIDADES, SERVIDAS, UNICAJA
								, pCajas, pCajaServ, tempCajas, tempMaxCajas, tempPeso, tempUnidades
						from vBuscarLotes 	
						where EMPRESA=@empresa and LETRA=@serie and NUMERO=@numero and ARTICULO=@articulo and LINIA=@linea  
							and Llote is not null  AND LUNIDADES>0.00
						order by Lcaducidad asc	
					open elCursor
					FETCH NEXT FROM elCursor 
					INTO  @_pesoArticulo, @_Lcaducidad, @_Llote, @_Lunidades, @_Lpeso, @_ARTICULO, @_UNIDADES
						, @_SERVIDAS, @_UNICAJA, @_pCajas, @_pCajaServ, @_tempCajas , @_tempMaxCajas, @_tempPeso
						, @_tempUnidades
					WHILE @@FETCH_STATUS=0 AND @unidadesG>0 BEGIN	
						declare @udsAcero int=0;
						if @artUnicaja is null or @artUnicaja=0 set @artUnicaja=1
						if @_Lunidades<=@unidadesG begin set @unidadesG=@_Lunidades end else begin set @udsAcero=1 end
						set @cajasG = CASE WHEN @artUnicaja=0 THEN 0 ELSE @unidadesG / @artUnicaja END
						if @artPeso<>'' set @pesoG = cast(@artPeso as numeric(15,6)) * @unidadesG
						
						delete	Traspaso_Temporal 
						where	empresa=@empresa and numero=@numero and letra=@serie and linea=@linea 
								and articulo=@articulo and lote=@_Llote  and cliente=@cliente
							
						insert into Traspaso_Temporal 
						(sesion, id, empresa, numero, letra, linea, articulo, lote, cajas, unidades, peso, cliente
						, ruta, almacen, caducidad) 
						values 
						(@usuario, @nu, @empresa, @numero, @serie, @linea, @articulo, @_Llote, isnull(@cajasG,0), isnull(@unidadesG,0)
						, cast(isnull(@pesoG,0.000) as varchar(10)), @cliente, @ruta, @almacen, @_Lcaducidad)

						if @udsAcero=1 
							begin 
								set @unidadesG=0
							end 
						else 
							begin 
								set @unidadesG = @unidades - @unidadesG 
								set @unidades = @unidadesG
							end
							
						FETCH NEXT FROM elCursor 
						INTO  @_pesoArticulo, @_Lcaducidad, @_Llote, @_Lunidades, @_Lpeso, @_ARTICULO, @_UNIDADES
							, @_SERVIDAS, @_UNICAJA, @_pCajas, @_pCajaServ, @_tempCajas , @_tempMaxCajas, @_tempPeso
							, @_tempUnidades
					END close elCursor deallocate elCursor
			END ELSE BEGIN
			
				--	NO trabaja con lotes ------------------------------

				if @artUnicaja>0  set @cajas=@unidades/@artUnicaja
				if @artPeso<>'' set  @peso =@unidades * cast(@artPeso as numeric(15,6))
				if @artPeso='' set @artPeso='0.00'
					
				delete	Traspaso_Temporal 
				where	empresa=@empresa and numero=@numero and letra=@serie and linea=@linea and articulo=@articulo 
						and lote='-'  and cliente=@cliente
					
				insert into Traspaso_Temporal 
				(sesion, id, empresa, numero, letra, linea, articulo, lote, cajas, unidades, peso, cliente, ruta
				, almacen, caducidad)
				values 
				(@usuario, @nu, @empresa, @numero, @serie, @linea, @articulo, '-', isnull(@cajas,0), isnull(@unidades,0)
				, cast(isnull(@Peso,0) as varchar(10)), @cliente, @ruta, @almacen, '')
			END

		FETCH NEXT FROM cur INTO @art
	END CLOSE cur deallocate cur

	select '{"artSinStock":['+@artSinStock+']}' as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select 'Error! - SP pTraspasoCompleto...'+@CatchError as JAVASCRIPT
	RETURN 0
END CATCH