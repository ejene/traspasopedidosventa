﻿CREATE PROCEDURE [dbo].[pAppVistas]
AS
BEGIN TRY	
	SET NOCOUNT ON
	
	
	
	declare @EJERCICIO char(4)	= (select Ejercicio from Configuracion)
	declare @GESTION char(6)	= (select Gestion from Configuracion)
	declare @COMUN char(8)		= (select Comun from Configuracion)
	declare @LOTES char(8)		= (select Lotes from Configuracion)
	declare @alterCreate varchar(max)




IF EXISTS (SELECT * FROM sys.objects where name='vSeries') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vSeries] as
select codigo, nombre from ['+@COMUN+'].dbo.Letras
')
select 'vSeries'




IF EXISTS (SELECT * FROM sys.objects where name='Articulos_Sustitucion') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[Articulos_Sustitucion] AS
select ART1.CODIGO AS ARTICULO, ART1.NOMBRE AS NOMBRE
, case when coalesce(lot.lote,0)=1 then coalesce(stl.stock_uds_lot,0.00) else coalesce(stc.stock_uds,0.00) end  as stock
, art2.codigo as art_orig
from ['+@GESTION+'].dbo.articulo art1 
INNER JOIN ['+@GESTION+'].DBO.ARTICULO ART2 ON ART2.SUBFAMILIA=ART1.SUBFAMILIA
LEFT JOIN ['+@LOTES+'] .DBO.artlot LOT ON LOT.ARTICULO=ART1.CODIGO

left join (
	select articulo, sum(unidades) as stock_uds_lot, almacen 
	from ['+@LOTES+'].[dbo].stocklotes 
	group by articulo, almacen
) stl on stl.articulo=ART1.CODIGO

left join (
	select articulo, sum(final) as stock_uds, ALMACEN
	from ['+@GESTION+'].dbo.stocks2 
	group by articulo, ALMACEN
) stc on stc.articulo=ART1.CODIGO

WHERE ART1.BAJA=0 AND ART1.SUBFAMILIA!='''' 
AND ( case when coalesce(lot.lote,0)=1 then coalesce(stl.stock_uds_lot,0.00) else coalesce(stc.stock_uds,0.00) end)!=0.00 
and art1.codigo!=art2.codigo	
')
select 'Articulos_Sustitucion'




IF EXISTS (SELECT * FROM sys.objects where name='vClientes') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vClientes] AS
SELECT 1 as Nodo,C.CODIGO as codigo, RTRIM(C.NOMBRE) collate database_default AS NOMBRE , C.NOMBRE2 AS RCOMERCIAL,
C.CIF, cast(RTRIM(C.DIRECCION) as varchar(40)) AS DIRECCION, C.CODPOST AS CP, C.POBLACION AS POBLACION,
C.PROVINCIA as provincia, ISNULL(PAIS.NOMBRE,'''') AS PAIS, C.EMAIL as EMAIL, C.EMAIL_F, COALESCE(T.TELEFONO, SPACE(15))
AS TELEFONO, COALESCE(P.PERSONA, SPACE(30)) AS CONTACTO, C.HTTP AS WEB, FP.NOMBRE AS N_PAG,
C.DIAPAG AS DIA_PAG, CAST(ROUND(C.CREDITO,2) AS numeric(10,2)) AS CREDITO, COALESCE(B.BANCO,
SPACE(30)) AS BANCO, COALESCE(B.IBAN, SPACE(4))+'' ''+COALESCE(B.CUENTAIBAN, SPACE(20)) AS IBAN,
COALESCE(B.SWIFT, SPACE(10)) AS SWIFT, C.VENDEDOR AS VENDEDOR, v.NOMBRE as nVendedor,
REPLACE(CONVERT(VARCHAR(MAX), 
C.OBSERVACIO),CHAR(13),''<br/>'') AS OBSERVACIO, C.TARIFA AS TARIFA, ISNULL(FP.DIAS_RIESGO,0)AS DIAS_RIESGO, 
C.DESCU1 AS DESCU1, C.DESCU2 AS DESCU2, C.OFERTA, C.BLOQ_CLI AS BLOQ_CLI, C.ENV_CLI, C.FECHA_BAJ,
case when C.FECHA_BAJ IS NULL or C.FECHA_BAJ=''1900-01-01 00:00:00'' THEN 0 ELSE 1 END AS BAJA, 
'''' as Logo, 
tiva.CODIGO as tipoIVA, isnull(tiva.IVA,0) as IVA, c.AGENCIA,
isnull(tiva.RECARG,0) as recargoIVA,
COALESCE(MCA1.VALOR,'''') collate database_default AS CENTRO,
COALESCE(MCA2.VALOR,'''') collate database_default AS MEDICO,
COALESCE(MCA3.VALOR,'''') collate database_default AS RAZON_SOCIAL_FACT,
C.RUTA, r.NOMBRE as nRuta,
mLat.VALOR as LATITUD, mLon.VALOR as LONGITUD
FROM ['+@GESTION+'].DBO.CLIENTES C 
LEFT JOIN (SELECT CLIENTE, MAX(TELEFONO) AS TELEFONO FROM ['+@GESTION+'].dbo.TELF_CLI WHERE ORDEN=1 GROUP BY CLIENTE) T ON T.CLIENTE = C.CODIGO 
LEFT JOIN ['+@GESTION+'].dbo.CONT_CLI P ON P.CLIENTE = C.CODIGO AND P.ORDEN = 1 
LEFT JOIN ['+@GESTION+'].dbo.BANC_CLI B ON B.CLIENTE = C.CODIGO AND B.ORDEN = 1 
LEFT JOIN ['+@GESTION+'].dbo.FPAG FP    ON FP.CODIGO = C.FPAG 
LEFT JOIN ['+@GESTION+'].dbo.tipo_iva tiva ON tiva.CODIGO = C.TIPO_IVA 
LEFT JOIN ['+@COMUN+'].dbo.PAISES PAIS ON PAIS.CODIGO  = C.PAIS 
LEFT JOIN ['+@GESTION+'].dbo.rutas r on r.CODIGO=C.RUTA
LEFT JOIN ['+@GESTION+'].DBO.vendedor v on v.CODIGO=C.VENDEDOR
LEFT JOIN ['+@GESTION+'].DBO.multicam MCA1 ON MCA1.CODIGO=C.CODIGO AND MCA1.FICHERO=''CLIENTES'' AND MCA1.CAMPO=''CEN''
LEFT JOIN ['+@GESTION+'].DBO.multicam MCA2 ON MCA2.CODIGO=C.CODIGO AND MCA2.FICHERO=''CLIENTES'' AND MCA2.CAMPO=''MED''
LEFT JOIN ['+@GESTION+'].DBO.multicam MCA3 ON MCA3.CODIGO=C.CODIGO AND MCA3.FICHERO=''CLIENTES'' AND MCA3.CAMPO=''RSO''
LEFT JOIN ['+@GESTION+'].DBO.multicam mLat ON mLat.CODIGO=C.CODIGO and mLat.FICHERO=''CLIENTES'' and mLat.CAMPO=''LAT''
LEFT JOIN ['+@GESTION+'].DBO.multicam mLon ON mLon.CODIGO=C.CODIGO and mLon.FICHERO=''CLIENTES'' and mLon.CAMPO=''LGT''
WHERE LEFT(C.CODIGO,3)=''430''
')
select 'vClientes'




IF EXISTS (SELECT * FROM sys.objects where name='d_Busca_PV') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +' view [dbo].[d_Busca_PV] as
SELECT distinct a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre as n_ruta, a.empresa,
               ltrim(rtrim(b.nombre)) as nombre
             , ltrim(rtrim(b.NOMBRE2)) as rComercial 
             , cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra as EN_USO,
             MAX(DPV.SERVIDAS)+SUM(COALESCE(TP.UNIDADES,0.00)) AS servidas, isnull(TP.numero,0) as bNumero,
             e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, a.almacen, MAX(B.IDIOMA_IMP) AS IDIOMA_IMP
FROM ['+@GESTION+'].[dbo].[c_pedive] a
inner join ['+@GESTION+'].[dbo].[clientes] b on a.cliente=b.codigo
left  join ['+@GESTION+'].[dbo].rutas d on d.codigo=a.ruta
left  join Traspaso_Temporal TP on  TP.numero COLLATE Modern_Spanish_CI_AI =a.numero COLLATE Modern_Spanish_CI_AI
left  join ['+@COMUN+'].[DBO].[en_uso] e on RTRIM(LTRIM(e.TIPO))=''PEDIVEN'' and e.CLAVE=cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra
left join ['+@GESTION+'].dbo.d_pedive dpv on dpv.empresa=a.empresa and dpv.numero=a.numero and dpv.letra=a.letra and dpv.articulo!=''''
LEFT JOIN ['+@GESTION+'].dbo.articulo ART ON ART.CODIGO=dpv.ARTICULO
where a.traspasado=0 and a.cancelado=0 and a.TOTALPED>=0.00
GROUP BY a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre, a.empresa, b.nombre, b.NOMBRE2
, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra, isnull(TP.numero,0), e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, a.almacen

')
select 'd_Busca_PV'




IF EXISTS (SELECT * FROM sys.objects where name='d_Busca_PV_DI') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[d_Busca_PV_DI] as
SELECT distinct a.numero, a.fecha, a.entrega, a.letra, a.ruta
, replace(d.nombre,''"'',''-'') as n_ruta
, a.empresa
, replace(b.nombre,''"'',''-'') as nombre
, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra as EN_USO
, (select max(servidas) from ['+@GESTION+'].[dbo].[d_pedive] where NUMERO=a.NUMERO) as servidas, isnull(TP.numero,0) as bNumero
, e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL
FROM ['+@GESTION+'].[dbo].[c_pedive] a
inner join ['+@GESTION+'].[dbo].[clientes] b on a.cliente=b.codigo
left  join ['+@GESTION+'].[dbo].rutas d on d.codigo=a.ruta
left  join Traspaso_Temporal TP on  TP.numero COLLATE Modern_Spanish_CI_AI =a.numero COLLATE Modern_Spanish_CI_AI
left  join ['+@COMUN+'].[DBO].[en_uso] e on RTRIM(LTRIM(e.TIPO))=''PEDIVEN'' and e.CLAVE=cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra
where a.traspasado=0 and a.cancelado=0 and a.entrega<=getdate()+4
')
select 'd_Busca_PV_DI'




IF EXISTS (SELECT * FROM sys.objects where name='d_Busca_PV_Zona') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[d_Busca_PV_Zona] as
SELECT distinct a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre as n_ruta, a.empresa,
replace(b.nombre2,''"'',''-'') as rComercial,
replace(replace(b.nombre,''"'',''-''),'''''''',''-'') as nombre, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra as EN_USO,
(select max(servidas) from ['+@GESTION+'].[dbo].[d_pedive] where NUMERO=a.NUMERO) as servidas, isnull(TP.numero,0) as bNumero,
e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, COALESCE(UPPER(M.VALOR),'''') AS ZONA
FROM ['+@GESTION+'].[dbo].[c_pedive] a
inner join ['+@GESTION+'].[dbo].[clientes] b on a.cliente=b.codigo
left join ['+@GESTION+'].[dbo].rutas d on d.codigo=a.ruta
left join Traspaso_Temporal TP on  TP.numero COLLATE Modern_Spanish_CI_AI =a.numero COLLATE Modern_Spanish_CI_AI
left join ['+@COMUN+'].[DBO].[en_uso] e on RTRIM(LTRIM(e.TIPO))=''PEDIVEN'' and e.CLAVE=cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra
left join ['+@GESTION+'].dbo.d_pedive dpv on dpv.empresa=a.empresa and dpv.numero=a.numero and dpv.letra=a.letra and dpv.articulo!=''''
left join ['+@GESTION+'].dbo.multicam m on m.codigo=dpv.articulo and m.campo=''ZNA''
where a.traspasado=0 and a.cancelado=0 and a.entrega<=getdate()+1
GROUP BY a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre, a.empresa, b.nombre2, b.nombre, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra
, isnull(TP.numero,0),e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, UPPER(M.VALOR)
')
select 'd_Busca_PV_Zona'




IF EXISTS (SELECT * FROM sys.objects where name='d_PV')set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[d_PV] as
SELECT a.numero, a.articulo, a.definicion, a.cajas, a.unidades, a.peso, a.servidas,
b.fecha, b.letra, b.ruta,
c.nombre
FROM ['+@GESTION+'].[dbo].[d_pedive] a
inner join ['+@GESTION+'].[dbo].[c_pedive] b on a.numero=b.numero
inner join ['+@GESTION+'].[dbo].[clientes] c on b.cliente=c.codigo
WHERE a.EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
')
select 'd_PV'




IF EXISTS (SELECT * FROM sys.objects where name='Detalle_Pedidos') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[Detalle_Pedidos] AS
select cpv.cliente,        
    CASE 
    WHEN CLI.CONTADO = 0 THEN cli.nombre
    WHEN CLI.CONTADO = 1 AND COALESCE(CON.NOMBRE,'''')!='''' THEN CON.NOMBRE
    ELSE cli.nombre                   
    END as n_cliente, 
    dpv.linia, dpv.servidas,
    ISNULL(art.stock,1) as cntrlSotck,
    CASE WHEN CLI.CONTADO = 0 
    THEN
        case when env.direccion='''' then cli.direccion else env.direccion end 
    ELSE
        CON.DIRECCION
    END    as direccion, 
    CASE WHEN CLI.CONTADO = 0 
    THEN
    case when env.codpos='''' then cli.codpost else env.codpos end
    ELSE
        CON.CODPOST 
    END as copdpost, 
    CASE WHEN CLI.CONTADO = 0 
    THEN
        case when env.poblacion='''' then cli.poblacion else env.poblacion end
    ELSE
        CON.POBLACION 
    END as poblacion, 
    CASE WHEN CLI.CONTADO = 0 
    THEN
        case when env.provincia='''' then cli.provincia else env.provincia end
    ELSE
        CON.PROVINCIA 
    END as provincia, 

    cli.NOMBRE2 as rComercial,
             
    cpv.empresa, cpv.numero, cpv.letra, cpv.vendedor, ven.nombre as n_vendedor, ven.mobil as Movil_vendedor, 
    cpv.ruta, rut.nombre as n_ruta, dpv.articulo, replace(dpv.definicion,''"'','''') as definicion, coalesce(lot.lote,0) as articulo_lote, 
    dpv.cajas as cajas_pv, dpv.unidades as uds_pv, 
    case when cast(case when art.peso='''' then ''0.0000'' else art.peso end as decimal(16,4))!=0.00 and dpv.peso=0.00 then cast(case when art.peso='''' then ''0.0000'' else art.peso end as decimal(16,4))*dpv.unidades else dpv.peso end as peso_pv, 
    dpv.cajas-dpv.CAJASERV-coalesce(trt.cajas,0.00) as cajas_pendientes, 
    dpv.unidades-dpv.servidas-coalesce(trt.unidades,0.000) as uds_pendiente, 
    (case when cast(case when art.peso='''' then ''0.0000'' else art.peso end as decimal(16,4))!=0.00 and dpv.peso=0.00 then cast(case when art.peso='''' then ''0.0000'' else art.peso end as decimal(16,4))*dpv.unidades else dpv.peso end)-(dpv.servidas*cast(case when art.peso='''' then ''0.0000'' else art.peso end as decimal(16,4)))-cast(coalesce(trt.peso,''000000000.0000'') as decimal(16,4)) as peso_pendiente, 
    coalesce(trt.cajas,0.00) as cajas_traspasadas, 
    coalesce(trt.unidades,0.000) as uds_traspasadas, 
    cast(coalesce(trt.peso,''0.00'') as decimal(16,4)) as peso_traspasado,
    case when coalesce(lot.lote,0)=1 then coalesce(stl.stock_uds_lot,0.00) else coalesce(stc.stock_uds,0.00) end  as stock,
    '''' as ColID
    , coalesce(DPV.LIBRE_4,'''') as notas_traspasadas
    , dpv.dto1+dpv.dto2 as dto
    , coalesce(m.valor,'''') as zona
    , art.ubicacion
    , cli.OBSERVACIO as ClienteObs
    , cpv.OBSERVACIO as PedidoObs
    , art.UNICAJA 
    , CASE WHEN coalesce(m2.valor,'''')='''' OR  coalesce(m2.valor,'''')=''.F.'' THEN 0 ELSE 1 END as VENTA_CAJA
    , case when trt.articulo is null then 0 else 1 end preparado -- Para saber si las notas estan preparadas
	, dpv.TIPO_IVA as TipoIVA
from ['+@GESTION+'].dbo.c_pedive cpv 
inner join ['+@GESTION+'].dbo.d_pedive dpv on cpv.empresa=DPV.EMPRESA AND cpv.numero=DPV.NUMERO AND cpv.letra=dpv.letra
left  join  (select empresa, numero, letra, linea, articulo, sum(coalesce(cajas,0.00)) as cajas, sum(unidades) as unidades, sum(cast(case when peso='''' then ''0.00'' else coalesce(peso,''0.00'') end as decimal(16,4))) as peso from Traspaso_Temporal group by empresa, numero, letra, linea, articulo) trt on trt.empresa collate database_default=DPV.EMPRESA AND trt.numero collate database_default=DPV.NUMERO AND trt.letra collate database_default=DPV.LETRA AND trt.linea=DPV.LINIA 
left join ['+@GESTION+'].dbo.articulo art on art.codigo=dpv.articulo 
left join  ['+@GESTION+'].dbo.rutas rut on rut.codigo=cpv.ruta 
left join  ['+@GESTION+'].dbo.vendedor ven on ven.codigo=cpv.vendedor 
inner join ['+@GESTION+'].dbo.clientes cli on cli.codigo=cpv.cliente 
left join  ['+@GESTION+'].dbo.env_cli env on env.linea=cpv.env_cli and env.cliente=cpv.cliente 
left join  ['+@LOTES+'].dbo.artlot lot on lot.articulo=dpv.articulo
left join (select empresa, almacen, articulo, sum(peso) as stock_peso_lot, sum(unidades) as stock_uds_lot from ['+@LOTES+'].[dbo].stocklotes 
group by empresa, almacen, articulo) stl on stl.empresa=cpv.empresa and stl.articulo=dpv.articulo and stl.almacen=cpv.almacen
left join (select empresa, almacen, articulo, sum(final) as stock_uds from ['+@GESTION+'].dbo.stocks2 group by empresa, almacen, articulo) stc on stc.empresa=cpv.empresa and stc.almacen=cpv.almacen and stc.articulo=dpv.articulo
left join ['+@GESTION+'].dbo.multicam m on m.codigo=dpv.articulo and m.campo=''ZNA''
left join ['+@GESTION+'].dbo.multicam m2 on m2.codigo=dpv.articulo and m2.campo=''SCA''
LEFT JOIN ['+@GESTION+'].DBO.CONTADO CON ON CON.EMPRESA=CPV.EMPRESA AND CON.NUMERO=CPV.NUMERO AND CON.LETRA=CPV.LETRA AND CON.TIPO=2
where cpv.traspasado=0 and cpv.cancelado=0 and (dpv.servidas<dpv.unidades or dpv.articulo='''') and coalesce(DPV.LIBRE_4,'''')=''''
')
select 'Detalle_Pedidos'




IF EXISTS (SELECT * FROM sys.objects where name='Pendiente_Pedidos') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[Pendiente_Pedidos] AS
SELECT DPV.EMPRESA, DPV.NUMERO, DPV.LETRA, DPV.LINIA, DPV.ARTICULO, DPV.UNIDADES 
FROM ['+@GESTION+'].DBO.D_PEDIVE DPV 
INNER JOIN ['+@GESTION+'].DBO.C_PEDIVE CPV ON CPV.EMPRESA=DPV.EMPRESA AND CPV.NUMERO=DPV.NUMERO AND CPV.LETRA=DPV.LETRA 
LEFT JOIN (SELECT EMPRESA, NUMERO, LETRA, LINEA, SUM(COALESCE(UNIDADES,0.00)) AS UNIDADES 
FROM Traspaso_Temporal 
GROUP BY  EMPRESA, NUMERO, LETRA, LINEA
) TEM ON TEM.EMPRESA collate Modern_Spanish_CI_AI=DPV.EMPRESA 
AND TEM.NUMERO collate Modern_Spanish_CI_AI=DPV.NUMERO 
AND TEM.LETRA collate Modern_Spanish_CI_AI=DPV.LETRA AND TEM.LINEA=DPV.LINIA 
WHERE CPV.TRASPASADO=0 AND CPV.CANCELADO=0 AND DPV.ARTICULO!='''' AND DPV.UNIDADES>DPV.SERVIDAS AND DPV.UNIDADES>COALESCE(TEM.UNIDADES,0.00)
')
select 'Pendiente_Pedidos'




IF EXISTS (SELECT * FROM sys.objects where name='v_d_pedive') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[v_d_pedive] as
SELECT	a.empresa, a.numero, a.linia, a.letra,
a.articulo,a.definicion,a.cajas,a.unidades, a.peso, a.servidas, a.traspaso,
c.CODIGO, c.NOMBRE, c.DIRECCION, c.CODPOST, c.POBLACION, c.PROVINCIA,
d.CODIGO as rutaCodigo, d.NOMBRE as rutaNombre,
e.CODIGO as vendedorCodigo, e.NOMBRE as vendedorNombre, e.MOBIL as vendedorMovil,
cast(f.LOTE as int) as usaLote
from ['+@GESTION+'].[dbo].d_pedive a
inner join ['+@GESTION+'].[dbo].c_pedive	b on a.NUMERO=b.NUMERO
inner join ['+@GESTION+'].[dbo].clientes	c on c.CODIGO=b.CLIENTE
inner join ['+@GESTION+'].[dbo].rutas		d on d.CODIGO=c.RUTA
inner join ['+@GESTION+'].[dbo].vendedor	e on e.CODIGO=c.VENDEDOR
left  join ['+@LOTES+'].[dbo].artlot	f on f.ARTICULO=a.ARTICULO  
where a.EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
')
select 'v_d_pedive'




IF EXISTS (SELECT * FROM sys.objects where name='v_d_traspaso') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[v_d_traspaso] as
SELECT empresa, numero, linia, letra, articulo, definicion, cajas, unidades, peso, servidas, traspaso
FROM ['+@GESTION+'].[dbo].[d_pedive]
')
select 'v_d_traspaso'




IF EXISTS (SELECT * FROM sys.objects where name='vAlbaranes') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vAlbaranes] AS
SELECT 
'''+@EJERCICIO+'''+CA.EMPRESA+REPLACE(CA.letra,SPACE(1),''0'')+REPLACE(CA.NUMERO, SPACE(1), ''0'') COLLATE Modern_Spanish_CI_AI AS IDALBARAN
,'''+@EJERCICIO+''' AS EJER, CA.EMPRESA, CA.LETRA as letra, ltrim(rtrim(CA.NUMERO)) as NUMERO, CA.LETRA+CA.NUMERO AS ALBARAN
, CA.CLIENTE
		
, CASE WHEN vcli.CONTADO = 0 
THEN
case when coalesce(vCON.PERSONA,'''')='''' then vcli.nombre else vCON.PERSONA end 
ELSE
CON.nombre
END	as nCliente	
, vcli.nombre as nFiscal
, convert(varchar(10),CA.FECHA,103) as FECHA, CAST(ROUND(CA.IMPORTE,2) AS NUMERIC(18,2)) AS IMPORTE, CA.IMPORTE as IMPORTEIVA, CA.TOTALDOC as TOTAL
, CAST(COALESCE(CASE WHEN RIGHT(CA.PEDIDO,2) = '''' THEN LTRIM(RTRIM(CA.PEDIDO)) ELSE LTRIM(RTRIM(RIGHT(CA.PEDIDO,2)))+''-''+LTRIM(RTRIM(LEFT(CA.PEDIDO,10))) END, SPACE(0)) AS VARCHAR(13)) AS PEDIDO
, CA.VENDEDOR, vven.NOMBRE COLLATE Modern_Spanish_CI_AI as nVendedor, CA.ALMACEN, CA.FINAN, CA.PRONTO 
	
, CASE WHEN vcli.CONTADO = 0 
THEN
case when coalesce(EC.direccion,'''')='''' then vcli.direccion else EC.direccion end 
ELSE
CON.DIRECCION
END	as direccion
, CASE WHEN vcli.CONTADO = 0 
THEN
case when coalesce(EC.POBLACION,'''')='''' then vcli.POBLACION else EC.POBLACION end 
ELSE
CON.POBLACION
END	as POBLACION
, CASE WHEN vcli.CONTADO = 0 
THEN
case when coalesce(EC.CODPOS,'''')='''' then vcli.CODPOST else EC.CODPOS end 
ELSE
CON.CODPOST
END	as CODPOS
, CASE WHEN vcli.CONTADO = 0 
THEN
case when coalesce(EC.PROVINCIA,'''')='''' then vcli.PROVINCIA else EC.PROVINCIA end 
ELSE
CON.PROVINCIA
END	as PROVINCIA
	
, CA.FPAG, COALESCE(B.BANCO,SPACE(0)) AS BANCO, COALESCE((B.IBAN+B.CUENTAIBAN),SPACE(0)) AS IBAN
, CA.FACTURA
, vcli.RUTA
,vcli.NOMBRE2
, concat(ltrim(rtrim(vcli.RUTA)),''-'',datepart(DW,CA.FECHA),''-'',LEFT(RIGHT(ltrim(rtrim((CA.PEDIDO))),7),5)) as rutaComp
, CAST(vcon.CLIENTE AS VARCHAR)+Replace(str(CAST(vcon.LINEA AS VARCHAR),4),space(1),''0'') COLLATE Modern_Spanish_CI_AI as contacto, vcon.PERSONA COLLATE Modern_Spanish_CI_AI as nContacto
, POR.AGENCIA COLLATE Modern_Spanish_CI_AI as agencia, ltrim(rtrim(ag.NOMBRE)) COLLATE Modern_Spanish_CI_AI as nAgencia
, VVEN.MOBIL, M.VALOR AS HORARIO, VCLI.VALOR_ALB, COALESCE(EE.BULTOS,00000) AS BULTOS, (CA.IMPORTE * CA.PRONTO)/100 as importePP, por.IMPORTE as portesIMP
, DA.SUM_CAJAS, DA.SUM_PESO, DA.SUM_DTO1, DA.SUM_DTO2, VCLI.CIF, TELF.TELEFONO, convert(nvarchar(250),ca.observacio) as observaciones
FROM ['+@GESTION+'].DBO.C_ALBVEN CA
LEFT JOIN ['+@GESTION+'].dbo.ENV_CLI EC ON CA.CLIENTE = EC.CLIENTE AND CA.ENV_CLI = EC.LINEA 
LEFT JOIN ['+@GESTION+'].DBO.BANC_CLI B ON B.CLIENTE=CA.CLIENTE AND B.CODIGO=CA.BANC_CLI
LEFT JOIN ['+@GESTION+'].dbo.clientes vcli on vcli.CODIGO =CA.CLIENTE
LEFT JOIN ['+@GESTION+'].dbo.vendedor vven on vven.CODIGO =CA.VENDEDOR
LEFT JOIN ['+@GESTION+'].DBO.CONT_CLI  vcon on vcon.CLIENTE=CA.CLIENTE and vcon.LINEA=1
LEFT JOIN ['+@GESTION+'].DBO.PORTES POR ON POR.EMPRESA=CA.EMPRESA AND POR.LETRA=CA.LETRA AND POR.ALBARAN=CA.NUMERO
LEFT JOIN ['+@GESTION+'].DBO.agencia ag on ag.CODIGO=POR.AGENCIA
LEFT JOIN ['+@GESTION+'].[dbo].MULTICAM M on M.FICHERO=''CLIENTES'' and M.CAMPO=''HOR'' AND M.CODIGO=CA.CLIENTE
left join ['+@GESTION+'].DBO.ENVIOETI EE ON EE.EMPRESA=CA.EMPRESA AND EE.NUMERO=CA.NUMERO AND EE.LETRA=CA.LETRA
INNER JOIN (SELECT D.EMPRESA, D.NUMERO, D.LETRA, SUM(D.CAJAS) AS SUM_CAJAS, SUM(D.PESO+CONVERT(DECIMAL(16,3),CASE WHEN A.LITROS='''' THEN ''0.00'' ELSE A.LITROS END)) AS SUM_PESO, SUM(D.DTO1) AS SUM_DTO1, SUM(D.DTO2) AS SUM_DTO2 FROM ['+@GESTION+'].DBO.D_ALBVEN D INNER JOIN ['+@GESTION+'].DBO.ARTICULO A ON A.CODIGO=D.ARTICULO GROUP BY D.EMPRESA, D.NUMERO, D.LETRA) DA ON DA.EMPRESA=CA.EMPRESA AND DA.NUMERO=CA.NUMERO AND DA.LETRA=CA.LETRA
LEFT JOIN (SELECT CLIENTE, MAX(TELEFONO) AS TELEFONO FROM ['+@GESTION+'].DBO.TELF_CLI WHERE ORDEN=1 GROUP BY CLIENTE) TELF ON TELF.CLIENTE=CA.CLIENTE
LEFT JOIN ['+@GESTION+'].DBO.CONTADO CON ON CON.EMPRESA=CA.EMPRESA AND CON.NUMERO=CA.NUMERO AND CON.LETRA=CA.LETRA AND CON.TIPO=1
WHERE CA.TRASPASADO = 0 AND LEFT(CA.CLIENTE,3)=''430''
')
select 'vAlbaranes'




IF EXISTS (SELECT * FROM sys.objects where name='vAlbaranes_Cabecera') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vAlbaranes_Cabecera] as
select	
'''+@EJERCICIO+'''+CA.EMPRESA  collate Modern_Spanish_CI_AI +REPLACE(CA.LETRA  collate Modern_Spanish_CI_AI,SPACE(1),''0'')
+REPLACE(CA.NUMERO  collate Modern_Spanish_CI_AI, SPACE(1),''0'') as IDALBARAN
, CA.LETRA + CA.NUMERO AS ALBARAN
, CA.PRONTO
, CA.IMPORTE as BaseImponible
, p.IMPORTE as portesIMP
, (CA.IMPORTE * CA.PRONTO)/100 as importePP
, ca.totaldoc as TOTALDOC
, e.IMPORTE as entregaIMP 
, CAST(CASE WHEN CA.FACTURA = '''' THEN (CASE WHEN CA.FACTURABLE=1 THEN ''PENDIENTE'' ELSE ''NO FACTURABLE'' END) ELSE ''FACTURADO'' END AS varchar(50)) 
    COLLATE Modern_Spanish_CI_AI AS ESTADO 
FROM ['+@GESTION+'].DBO.C_ALBVEN CA 
LEFT JOIN ['+@GESTION+'].dbo.ENV_CLI EC ON CA.CLIENTE = EC.CLIENTE AND CA.ENV_CLI = EC.LINEA 
left join ['+@GESTION+'].dbo.portes p on p.ALBARAN=CA.NUMERO and CA.EMPRESA=p.EMPRESA and CA.LETRA=p.LETRA
left join ['+@GESTION+'].dbo.entregas e on e.ALBARAN=CA.NUMERO and CA.EMPRESA=e.EMPRESA and CA.LETRA=e.LETRA
')
select 'vAlbaranes_Cabecera'




IF EXISTS (SELECT * FROM sys.objects where name='vAlbaranes_Detalle') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vAlbaranes_Detalle] AS
SELECT 
'''+@EJERCICIO+'''+C.EMPRESA+REPLACE(C.letra,SPACE(1),''0'')+REPLACE(C.NUMERO, SPACE(1), ''0'') COLLATE Modern_Spanish_CI_AI AS IDALBARAN
,'''+@EJERCICIO+''' AS EJER, d.empresa, d.letra, d.numero, C.LETRA+D.NUMERO AS ALBARAN, D.CLIENTE
, D.ARTICULO, D.DEFINICION, D.UNIDADES, D.CAJAS, D.PESO, D.SERIE, D.PRECIO, D.LINIA, D.DTO1, D.DTO2, D.IMPORTE, D.Tipo_IVA as Tipo_IVA, d.pverde, d.PRECIOIVA , d.IMPDIVIVA, coalesce(iva.iva,0.00) as porcen_iva
,case when d.doc=1 then d.DOC_NUM else '''' end as PEDIDO, C.VENDEDOR
FROM ['+@GESTION+'].DBO.D_ALBVEN D
INNER JOIN ['+@GESTION+'].dbo.C_ALBVEN C ON D.EMPRESA = C.EMPRESA AND D.LETRA = C.LETRA AND D.NUMERO = C.NUMERO
left join ['+@GESTION+'].dbo.tipo_iva iva on iva.codigo=d.TIPO_IVA 
WHERE C.TRASPASADO = 0 AND LEFT(D.CLIENTE,3)=''430''
')
select 'vAlbaranes_Detalle'




IF EXISTS (SELECT * FROM sys.objects where name='vAlbaranes_Lote') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vAlbaranes_Lote] AS
SELECT
'''+@EJERCICIO+'''+d.EMPRESA+REPLACE(d.letra,SPACE(1),''0'')+REPLACE(d.NUMERO, SPACE(1), ''0'') COLLATE Modern_Spanish_CI_AI AS IDALBARAN
,'''+@EJERCICIO+''' AS PERIODO, D.EMPRESA, d.numero, d.letra, d.letra+D.NUMERO AS ALBARAN, d.LINIA, d.LOTE, d.UNIDADES
from ['+@LOTES+'].dbo.LTALBVE d where d.periodo='''+@EJERCICIO+'''
')
select 'vAlbaranes_Lote'




IF EXISTS (SELECT * FROM sys.objects where name='vAlbaranes_Pie') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vAlbaranes_Pie] AS 	
SELECT 
'''+@EJERCICIO+'''+CAv.EMPRESA+REPLACE(CAv.letra,SPACE(1),''0'')+REPLACE(CAv.NUMERO, SPACE(1), ''0'') COLLATE Modern_Spanish_CI_AI AS IDALBARAN
,'''+@EJERCICIO+''' as EJER,	CAV.EMPRESA, CAV.LETRA, ltrim(rtrim(CAv.NUMERO)) as NUMERO, CAv.LETRA+CAv.NUMERO AS ALBARAN
, CAV.CLIENTE, CLI.RECARGO as CliRecargo, dav.tipo_iva, iva.IVA as porcen_iva, CASE WHEN CLI.RECARGO=1 THEN IVA.RECARG ELSE 0.00 END AS TIPO_RE,
CAV.PRONTO as pPago,

SUM(DAV.PVERDE) AS PVERDE, 

SUM(CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END  + DAV.PVERDE)
AS IMPORTE,

ROUND(SUM((
CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END + DAV.PVERDE)*(IVA.IVA/100)),2) 
AS IVA,

CASE WHEN CLI.RECARGO=1 
THEN round(SUM((
CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.RECARG/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END + DAV.PVERDE)*(IVA.RECARG/100)),2) 
ELSE 0.00 
END  
AS RECARGO,

SUM(CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END + DAV.PVERDE)  
+
ROUND(SUM((
CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END + DAV.PVERDE)*(IVA.IVA/100)),2) 
+
CASE WHEN CLI.RECARGO=1 
THEN round(SUM((
CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.RECARG/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END + DAV.PVERDE)*(IVA.RECARG/100)),2) 
ELSE 0.00 
END  
AS TOTALDOC

FROM ['+@GESTION+'].DBO.C_ALBVEN CAV 
INNER JOIN 
(select EMPRESA, NUMERO, LETRA, ARTICULO, IMPORTE, TIPO_iva, 1 AS INC_PP, 0 AS IVA_INC, PVERDE
from ['+@GESTION+'].dbo.d_albven where tipo_iva!='''' and importe!=0.00

union all

select  EMPRESA, albaran, LETRA, ''PORTES'' as ARTICULO, IMPORTE, TIPO_iva, INC_PP, IVA_INC, 0.00 AS PVERDE 
from ['+@GESTION+'].dbo.portes) DAV ON DAV.EMPRESA=CAV.EMPRESA AND DAV.NUMERO=CAV.NUMERO AND DAV.LETRA=CAV.LETRA 
LEFT JOIN ['+@GESTION+'].dbo.tipo_iva IVA ON IVA.CODIGO=DAV.TIPO_IVA
INNER JOIN ['+@GESTION+'].dbo.clientes CLI ON CLI.CODIGO=CAV.CLIENTE
LEFT JOIN ['+@GESTION+'].dbo.tipo_iva IVC ON IVC.CODIGO=CLI.TIPO_IVA WHERE DAV.IMPORTE!=0.00
GROUP BY CAV.EMPRESA, CAV.NUMERO, CAV.LETRA, CAV.CLIENTE, CLI.RECARGO, dav.tipo_iva, iva.IVA, IVA.RECARG, CAV.PRONTO
')
select 'vAlbaranes_Pie'




IF EXISTS (SELECT * FROM sys.objects where name='vArticulos') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vArticulos]
as
select * from ['+@GESTION+'].dbo.articulo WHERE BAJA=0
')
select 'vArticulos'




IF EXISTS (SELECT * FROM sys.objects where name='vArticulosStock') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vArticulosStock] as

select alm.codigo as almacen, 
case when coalesce(lot.lote,0)=1 then coalesce(stl.stock_uds_lot,0.00) 
else coalesce(stc.stock_uds,0.00) end as ArticuloStock

,art.[CODIGO]
,replace(art.NOMBRE,''"'',''-'') as NOMBRE
,art.[ABREV]
,art.[FAMILIA]
,art.[MARCA]
,art.[MINIMO]
,art.[MAXIMO]
,art.[AVISO]
,art.[BAJA]
,art.[TIPO_IVA]
,art.[RETENCION]
,art.[IVA_INC]
,art.[COST_ULT1]
,art.[FECHA_ULT]
,art.[ULT_FECHA]
,art.[PMCOM1]
,art.[IMAGEN]
,art.[CARAC]
,art.[FECHAALTA]
,art.[FECHABAJA]
,art.[UBICACION]
,art.[MEDIDAS]
,art.[PESO]
,art.[LITROS]
,replace(cast(art.OBSERVACIO as varchar(max)),''"'',''-'') as OBSERVACIO
,art.[UNICAJA]
,art.[DESGLOSE]
,art.[ARANCELES]
,art.[DEFINICION2]
,art.[SUBFAMILIA]
,art.[INTERNET]
,art.[VISTA]
,art.[FPAG]
,art.[PVERDE]
,art.[P_IMPORTE]
,art.[P_TAN]
,art.[LCOLOR]
,art.[MARGEN]
,art.[TCP]
,art.[VENSERIE]
,art.[PUNTOS]
,art.[DES_ESC]
,art.[TIPO_ART]
,art.[MODELO]
,art.[COCINA]
,art.[STOCK]
,art.[ART_IMPUES]
,replace(cast(art.NOMBRE2 as varchar(max)),''"'',''-'') as NOMBRE2
,art.[COLOR_ART]
,art.[TIPO_PVP]
,art.[COST_ESCAN]
,art.[TIPO_ESCAN]
,art.[ART_CANON]
,art.[ACTUA_COLO]
,art.[FACT_AREPE]
,art.[GARANTIA]
,art.[ALQUILER]
,art.[ORDEN]
,art.[C_ENT]
,art.[CN8]
,art.[IVALOT]
,art.[ARTANT]
,art.[REPORTETIQ]
,art.[GUID]
,art.[IMPORTAR]
,art.[DTO1]
,art.[DTO2]
,art.[DTO3]
,art.[ISP]
,art.[GRUPOIVA]

from ['+@GESTION+'].dbo.articulo art
inner join ['+@GESTION+'].dbo.almacen alm on 1=1
left join  ['+@LOTES+'].dbo.artlot lot on lot.articulo=art.codigo
left join (
	select empresa, almacen, articulo, sum(peso) as stock_peso_lot, sum(unidades) as stock_uds_lot 
	from ['+@LOTES+'].[dbo].stocklotes 
	group by empresa, almacen, articulo
) stl on stl.articulo=art.codigo and stl.almacen=alm.codigo
left join (
	select empresa, almacen, articulo, sum(final) as stock_uds 
	from ['+@GESTION+'].dbo.stocks2 
	group by empresa, almacen, articulo
) stc on stc.almacen=alm.codigo and stc.articulo=art.codigo
where art.BAJA=0
')
select 'vArticulosStock'




IF EXISTS (SELECT * FROM sys.objects where name='vBuscarLotes') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vBuscarLotes]
as
select b.PESO as pesoArticulo, b.stock as controlStock, 
isnull(c.CADUCIDAD,'''') as Lcaducidad, c.LOTE as Llote, c.UNIDADES-coalesce(convert(decimal(16,4),t.unidades),0.00) as Lunidades, 
c.PESO-coalesce(convert(decimal(16,4),t.peso),0.00) as Lpeso, 
a.EMPRESA, a.NUMERO, A.LETRA, a.LINIA, a.ARTICULO, a.DEFINICION, a.UNIDADES, a.SERVIDAS, a.CAJAS as pCajas, 
a.CAJASERV as pCajaServ, 
isnull((select MAX(CAJAS) from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
       and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO),0) 
       as tempMaxCajas, 
isnull((select UNIDADES from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
       and linea=a.LINIA 
       and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO 
       and lote  collate Modern_Spanish_CI_AI=c.LOTE),0) 
       as tempUnidades, 
isnull((select CAJAS from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
       and linea=a.LINIA 
       and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO 
       and lote  collate Modern_Spanish_CI_AI=c.LOTE),0) 
       as tempCajas, 
isnull((select PESO from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
       and linea=a.LINIA 
       and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO 
       and lote  collate Modern_Spanish_CI_AI=c.LOTE),0.000) 
       as tempPeso, 
b.UNICAJA 
       , CASE WHEN coalesce(m2.valor,'''')='''' OR  coalesce(m2.valor,'''')=''.F.'' THEN 0 ELSE 1 END as VENTA_CAJAS
       , c.almacen
FROM ['+@GESTION+'].[dbo].[d_pedive] a 
INNER JOIN ['+@GESTION+'].[dbo].[C_pedive] D ON D.EMPRESA=A.EMPRESA AND D.NUMERO=A.NUMERO AND D.LETRA=A.LETRA
left join ['+@GESTION+'].[dbo].articulo b on b.CODIGO=a.ARTICULO 
left join ['+@LOTES+'].[dbo].stocklotes c on c.ARTICULO=a.ARTICULO and c.almacen=d.almacen
LEFT JOIN (select T.empresa, T.almacen, T.articulo, T.LOTE, SUM(T.unidades) AS UNIDADES, 
	SUM(CONVERT(DECIMAL(16,4),COALESCE(T.PESO,''0.00''))) AS PESO from Traspaso_Temporal t 
	inner join ['+@GESTION+'].dbo.C_pedive c on c.empresa=t.empresa collate database_default and c.numero=t.numero collate database_default 
	and c.letra=t.letra collate database_default 
	INNER JOIN ['+@GESTION+'].dbo.D_pedive D on D.empresa=c.empresa collate database_default and D.numero=c.numero collate database_default 
	and D.letra=c.letra AND D.LINIA=T.LINeA  where c.traspasado=0 AND D.SERVIDAS<D.UNIDADES 
	GROUP BY T.empresa, T.almacen, T.articulo, T.LOTE
) T on T.EMPRESA collate SQL_Latin1_General_CP1_CI_AS=A.EMPRESA 
	AND t.articulo collate SQL_Latin1_General_CP1_CI_AS=a.articulo 
	and t.almacen collate SQL_Latin1_General_CP1_CI_AS=d.almacen 
	and t.lote collate SQL_Latin1_General_CP1_CI_AS=c.lote
left join ['+@GESTION+'].dbo.multicam m2 on m2.codigo=A.articulo and m2.campo=''SCA''
')
select 'vBuscarLotes'




IF EXISTS (SELECT * FROM sys.objects where name='vSTOCKS') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vSTOCKS] as
(
SELECT TOP 100 PERCENT A.CODIGO, A.NOMBRE, COALESCE(MOV.EMPRESA,''01'') AS EMPRESA, COALESCE(MOV.ALMACEN,'''') AS ALMACEN, COALESCE(MOV.TIPO,'''') AS TIPOMOV, COALESCE(MOV.UNIDADES,0) AS UNIDADES, COALESCE(MOV.PESO,0) AS PESO , MOV.FECHASTOCK
FROM ['+@GESTION+'].DBO.ARTICULO A INNER JOIN ['+@LOTES+'] .DBO.artlot AL ON AL.articulo = A.CODIGO AND AL.LOTE=0
LEFT JOIN (
SELECT ''IN'' AS TIPO, I.EMPRESA, I.ALMACEN, I.ARTICULO, I.UNIDADES AS UNIDADES, I.PESO AS PESO, I.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.regulari I INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=I.ARTICULO AND LOT.LOTE=0 WHERE I.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND I.FECHA=(SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=I.EMPRESA AND ALMACEN=I.ALMACEN AND ARTICULO=I.ARTICULO   GROUP BY EMPRESA, ALMACEN, ARTICULO)
UNION ALL
SELECT ''SI'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, (L.UNIDADES) AS UNIDADES, L.PESO AS PESO, L.FECHA AS FECHASTOCKD FROM ['+@GESTION+'].DBO.stockini L  INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL 
select ''TO'' AS TIPO, C.EMPRESA, C.ALMORIG, L.ARTICULO, -L.UNIDADES, -L.PESO, c.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.c_albatr c inner join ['+@GESTION+'].DBO.d_albatr L on l.empresa=c.empresa and l.numero=c.numero INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE c.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND c.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMORIG AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
select ''TD'' AS TIPO, C.EMPRESA, C.ALMDEST, L.ARTICULO, L.UNIDADES, L.PESO, c.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.c_albatr c inner join ['+@GESTION+'].DBO.d_albatr L on l.empresa=c.empresa and l.numero=c.numero INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE c.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND c.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMDEST AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
		
UNION ALL 
SELECT ''AV'' AS TIPO, C.EMPRESA, C.ALMACEN, L.ARTICULO, -(L.UNIDADES) as UNIDADES, -(L.PESO) AS PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.C_ALBVEN C INNER JOIN ['+@GESTION+'].DBO.D_ALBVEN L ON L.EMPRESA=C.EMPRESA AND L.LETRA=C.LETRA AND L.NUMERO=C.NUMERO INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''AC'' AS TIPO, C.EMPRESA, C.ALMACEN, L.ARTICULO, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.C_ALBCOM C INNER JOIN ['+@GESTION+'].DBO.D_ALBCOM L ON L.EMPRESA=c.empresa and l.numero=c.numero and l.PROVEEDOR =c.proveedor INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''CP'' AS TIPO, C.EMPRESA, C.ALMACEN, C.ARTICULO, C.ENTRADA AS UNIDADES, C.PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.C_PROD C INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=C.ARTICULO AND LOT.LOTE=0 WHERE C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=C.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''DP'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, -(L.SALIDA) AS UNIDADES, -(L.PESO) AS PESO, L.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.D_PROD L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''CT'' AS TIPO, C.EMPRESA, C.ALMACEN, C.ARTICULO, -(C.SALIDA) AS UNIDADES, -(C.PESO) AS PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.C_TRANS C INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=C.ARTICULO AND LOT.LOTE=0 WHERE C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=C.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
		
UNION ALL
SELECT ''DT'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, (L.ENTRADA) AS UNIDADES, (L.PESO) AS PESO, L.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.D_TRANS L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''AR'' AS TIPO, C.EMPRESA, C.ALMACEN, L.ARTICULO, -L.UNIDADES, -L.PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.c_albare C INNER JOIN ['+@GESTION+'].DBO.D_albare L ON L.EMPRESA=C.EMPRESA AND L.NUMERO=C.NUMERO INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=L.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
select ''DV'' AS TIPO, C.EMPRESA, C.ALMACEN, L.ARTICULO, -(L.UNIDADES-L.SERVIDAS) AS UNIDADES, -(L.PESO-(L.SERVIDAS*(CONVERT(DECIMAL(10,3),CASE WHEN A.PESO='''' THEN ''0.00'' ELSE A.PESO END)))) AS PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.C_ALBDEP C INNER JOIN ['+@GESTION+'].DBO.D_ALBDEP L ON L.EMPRESA=C.EMPRESA AND L.LETRA=C.LETRA AND L.NUMERO=C.NUMERO INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 INNER JOIN ['+@GESTION+'].DBO.ARTICULO A ON A.CODIGO=L.ARTICULO WHERE C.TRASPASADO=0 AND L.UNIDADES>L.SERVIDAS AND C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''DC'' AS TIPO, C.EMPRESA, C.ALMACEN, L.ARTICULO, (L.UNIDADES-L.SERVIDAS) AS UNIDADES, (L.PESO-(L.SERVIDAS*(CONVERT(DECIMAL(10,3),CASE WHEN A.PESO='''' THEN ''0.00'' ELSE A.PESO END)))) AS PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.c_depcom C INNER JOIN ['+@GESTION+'].DBO.D_DEPCOM L ON L.EMPRESA=C.EMPRESA AND L.NUMERO=C.NUMERO INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 INNER JOIN ['+@GESTION+'].DBO.ARTICULO A ON A.CODIGO=L.ARTICULO WHERE C.TRASPASADO=0 AND L.UNIDADES>L.SERVIDAS AND C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
) MOV ON A.CODIGO=MOV.ARTICULO and MOV.EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
WHERE A.STOCK=0 ORDER BY 1,4
)
')
select 'vSTOCKS'




IF EXISTS (SELECT * FROM sys.objects where name='vStock') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vStock] as
SELECT     EMPRESA, CODIGO AS ARTICULO, ALMACEN, SUM(COALESCE(UNIDADES,0)) AS UNIDADES, SUM(COALESCE(PESO,0)) AS PESO
FROM         dbo.vSTOCKS 
where EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
GROUP BY EMPRESA, CODIGO, ALMACEN
HAVING SUM(COALESCE(UNIDADES,0))<>0
')
select 'vStock'




IF EXISTS (SELECT * FROM sys.objects where name='vCajasOunidades') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vCajasOunidades] as
select isnull(b.PESO,0.000) as pesoArticulo, isnull(b.UNICAJA,0) as cajasArticulo, isnull(b.stock,0) as cntrlSotck, 
a.EMPRESA, a.NUMERO, a.LETRA, a.LINIA, a.ARTICULO, a.DEFINICION
, isnull(a.UNIDADES,0.00) as UNIDADES
, isnull(a.CAJAS,0.00) as pCajas
, isnull(a.PESO,0.000) as PESO
, isnull(a.TRASPASO,0.00) as TRASPASO
, isnull(C.unidades,0.00) as elStock
, isnull(c.peso,0.000) AS STOCKPESO
, isnull(c.unidades*b.unicaja,0.00) AS STOCKCAJAS
,
isnull(
	(select cajas from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
	and linea=a.LINIA 
	and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO )
,a.CAJAS) as tempCajas, 
isnull(
	(select unidades from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
	and linea=a.LINIA 
	and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO )
,a.UNIDADES) as tempUds, 
isnull(
	(select peso from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
	and linea=a.LINIA 
	and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO)
,0.000) as tempPeso 

FROM ['+@GESTION+'].[dbo].[d_pedive] a 
left join ['+@GESTION+'].[dbo].articulo b on b.CODIGO=a.ARTICULO 
left join [vStock] c on c.ARTICULO=a.ARTICULO 
where a.EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from Configuracion)
')
select 'vCajasOunidades'




IF EXISTS (SELECT * FROM sys.objects where name='vContadorDescuentoAlbaran') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vContadorDescuentoAlbaran] AS
select IdAlbaran, sum(DTO1) as DTO1, sum(DTO2) as DTO2 from vAlbaranes_Detalle
group by IdAlbaran
')
select 'vContadorDescuentoAlbaran'




IF EXISTS (SELECT * FROM sys.objects where name='vDatosEmpresa') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vDatosEmpresa] AS
SELECT E.CODIGO, RTRIM(LTRIM(E.NOMBRE)) AS NOMBRE, RTRIM(LTRIM(E.NOMBRE2)) AS NOMBRE2, RTRIM(LTRIM(E.CIF)) AS CIF, 
RTRIM(LTRIM(E.DIRECCION)) AS DIRECCION, RTRIM(LTRIM(E.CODPOS)) AS CODPOS, RTRIM(LTRIM(E.POBLACION)) AS POBLACION, 
RTRIM(LTRIM(E.PROVINCIA)) AS PROVINCIA, RTRIM(LTRIM(E.TELEFONO)) AS TELEFONO, RTRIM(LTRIM(E.FAX)) AS FAX, 
RTRIM(LTRIM(E.MOBIL)) AS MOBIL, RTRIM(LTRIM(E.EMAIL)) AS EMAIL, RTRIM(LTRIM(E.[HTTP])) AS WEB, 
RTRIM(LTRIM(E.TXTFACTU1)) AS TXTFACTU1, RTRIM(LTRIM(E.TXTFACTU2)) AS TXTFACTU2, e.logo, e.almacen, f.tarifapret, E.LETRA
FROM ['+@GESTION+'].DBO.EMPRESA E  
LEFT JOIN ['+@GESTION+'].DBO.FACTUCNF F ON F.EMPRESA=E.CODIGO
')
select 'vDatosEmpresa'




IF EXISTS (SELECT * FROM sys.objects where name='vEnvioeti_Rep') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vEnvioeti_Rep] as
SELECT cav.empresa, emp.nombre as n_empresa, cav.numero, cav.letra, cav.fecha, cav.ruta, coalesce(rut.nombre,'''''''') as n_ruta
, cav.cliente, cli.nombre as n_cliente, coalesce(env.DIRECCiON,'''') as direccion, coalesce(env.CODPOS,'''') as codpos 
, coalesce(env.POBLACION,'''') as poblacion, coalesce(env.PROVINCIA,'''') as provincia, convert(int,coalesce(eti.bultos,0)) as bultos
FROM ['+@GESTION+'].[dbo].[c_albven] cav 
inner join ['+@GESTION+'].dbo.empresa emp on emp.codigo=cav.empresa
inner join ['+@GESTION+'].dbo.clientes cli on cli.codigo=cav.cliente
left  join ['+@GESTION+'].dbo.rutas rut on rut.codigo=cav.ruta
left  join ['+@GESTION+'].dbo.env_cli env on env.cliente=cav.cliente and env.LINEA=cav.ENV_CLI 
left  join ['+@GESTION+'].dbo.envioeti eti on eti.empresa=cav.empresa and eti.numero=cav.numero and eti.letra=cav.letra
')
select 'vEnvioeti_Rep'




IF EXISTS (SELECT * FROM sys.objects where name='vErrorLotesTrasTemp') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vErrorLotesTrasTemp] as
SELECT T.empresa, T.numero, T.letra, T.ARTICULO , art.NOMBRE
FROM Traspaso_Temporal T 
INNER JOIN ['+@LOTES+'].dbo.artlot A ON A.ARTICULO collate Modern_Spanish_CI_AI=T.articulo 
LEFT JOIN ['+@GESTION+'].dbo.articulo art on art.CODIGO collate Modern_Spanish_CI_AI=T.articulo
WHERE A.lote =1 AND T.lote collate Modern_Spanish_CI_AI=''''
')
select 'vErrorLotesTrasTemp'




IF EXISTS (SELECT * FROM sys.objects where name='vFamilias') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vFamilias] as
SELECT codigo,nombre from ['+@GESTION+'].[dbo].familias
')
select 'vFamilias'




IF EXISTS (SELECT * FROM sys.objects where name='vIVA') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vIVA] AS
SELECT '''+@EJERCICIO+'''+empresa+CAST(CUENTA AS VARCHAR)+replace(NUMFRA,space(1),''0'') AS IDIVA, 
'''+@EJERCICIO+''' AS EJER, EMPRESA, CUENTA, FECHA,
'''+@EJERCICIO+'''+EMPRESA+replace(CAST(NUMFRA AS VARCHAR(10)),space(1), ''0'') COLLATE Modern_Spanish_CI_AI AS IDFACTURA, 
NUMFRA AS NUMFRA, SUM(BIMPO) AS BASE, PORCEN_IVA, SUM(IVA) AS IMP_IVA, PORCEN_REC,  SUM(RECARGO) AS IMP_RECARGO,
SUM(BIMPO)+SUM(IVA)+SUM(RECARGO) AS TOTAL, tipo_iva
FROM ['+@GESTION+'].DBO.IVAREPER
WHERE LEFT(CUENTA,3)=''430''
group by EMPRESA, CUENTA, FECHA, NUMFRA, PORCEN_IVA, PORCEN_REC, tipo_iva
')
select 'vIVA'




IF EXISTS (SELECT * FROM sys.objects where name='vLotesDelArticulo') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vLotesDelArticulo] as
select	a.almacen, a.articulo, a.caducidad, a.color, a.empresa, a.lote, a.peso, a.talla, a.ubica, a.unidades,
b.peso as pesoArticulo, b.unicaja as unidadesOcajas
from ['+@LOTES+'].[dbo].[stocklotes] a
left join ['+@GESTION+'].[dbo].articulo b on b.CODIGO=a.ARTICULO
where unidades>0
')
select 'vLotesDelArticulo'




IF EXISTS (SELECT * FROM sys.objects where name='vMarcas') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vMarcas]
as
SELECT CODIGO, NOMBRE, DESCUEN, TCP, MARGEN, FOTO from ['+@GESTION+'].[dbo].marcas
')
select 'vMarcas'




IF EXISTS (SELECT * FROM sys.objects where name='vOfertas') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vOfertas]
AS
SELECT REPLACE(TIPO+CLIENTE+ARTICULO+color+TALLA+FAMILIA+SUBFAMILIA+STR(LINEA,5),SPACE(1),'''') AS IDOFERTA,TIPO, CLIENTE, ARTICULO, color, TALLA, FAMILIA, SUBFAMILIA, MARCA, LINDES, LINEA, FECHA_IN, FECHA_FIN, UNI_INI, UNI_FIN, PVP, DTO1, DTO2, MONEDA, TARIFA
FROM (
SELECT ''CLIENTE'' AS TIPO, CLIENTE, ARTICULO, color, TALLA, FAMILIA, SUBFAMILIA, MARCA, '''' AS LINDES, LINIA AS LINEA, FECHA_IN, FECHA_FIN, UNI_MIN AS UNI_INI, (CASE WHEN UNI_MAX=0 THEN 999999.99 ELSE UNI_MAX END) AS UNI_FIN, PVP, DTO1, DTO2, MONEDA, '''' AS TARIFA 
FROM ['+@GESTION+'].DBO.DESCUEN
UNION 
SELECT ''ARTICULO'' AS TIPO, '''' AS CLIENTE, ARTICULO, color, talla, '''' AS FAMILIA, '''' AS SUBFAMILIA, '''' AS MARCA, '''' AS  LINDES, LINEA, FECHA_IN, FECHA_FIN , DESDE AS UNI_INI, (CASE WHEN HASTA=0 THEN 999999.99 ELSE HASTA END) AS UD_FIN, PVP, DESCUENTO AS DTO1, 0 AS DTO2, MONEDA, TARIFA FROM ['+@GESTION+'].DBO.OFERTAS
UNION 
SELECT ''LINDESC'' AS TIPO, C.CODIGO AS CLIENTE, D.ARTICULO, SPACE(6) AS COLOR, SPACE(8) AS TALLA, D.FAMILIA, D.SUBFAMILIA, D.MARCA, D.CODIGO AS LINDES, D.LINEA, D.FECHA_IN, D.FECHA_FIN, D.UNI_INI, D.UNI_FIN, D.PVP, D.DTO1, D.DTO2, D.MONEDA, '''' AS TARIFA FROM ['+@GESTION+'].DBO.CLIENTES C INNER JOIN ['+@GESTION+'].DBO.LIN_DESC D ON D.CODIGO=C.LIN_DES
) O 
')
select 'vOfertas'




IF EXISTS (SELECT * FROM sys.objects where name='vPedidos_Pie') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vPedidos_Pie] AS
SELECT 
'''+@EJERCICIO+''' as EJER,
('''+@EJERCICIO+'''+CAV.empresa+replace(CAV.LETRA,space(1),''0'')+replace(LEFT(CAV.NUMERO,10),space(1),''0'')) collate Modern_Spanish_CI_AI as IDPEDIDO,
CAV.EMPRESA, CAV.NUMERO, CAV.LETRA, CAV.CLIENTE, IVA.IVA as IVApc, 
case when cli.recargo=1 then IVA.RECARG else 0 end as recargoPC, 
ISNULL(CAV.ENTREGA,0.00) as ENTREGA,
CAV.PRONTO as pPago,

dav.tipo_iva, iva.IVA as porcen_iva, iva.iva,
CASE WHEN CLI.RECARGO=1 THEN IVA.RECARG ELSE 0.00 END AS TIPO_RE, SUM(DAV.PVERDE) AS PVERDE,

SUM(CASE WHEN DAV.IVA_INC=1 THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE 
	ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE) AS IMPORTE,

ROUND(SUM((CASE WHEN DAV.IVA_INC=1 THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE
	ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE)*(IVA.IVA/100)),2) AS IVAimp,

coalesce(CASE WHEN CLI.RECARGO=1 THEN round(SUM((CASE WHEN DAV.IVA_INC=1 THEN DAV.IMPORTE*(1-(IVA.RECARG/100)) 
	ELSE CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE 
		ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE)*(IVA.RECARG/100)),2) END, 0.00) AS RECARGO,

(CAV.TOTALPED * CAV.PRONTO) / 100 as impPP,
isnull(ent.IMPORTE,0.00) as entIMP,

-- TOTALDOC (lo calculamos por si hay más de un IVA en el documento)
SUM(CASE WHEN DAV.IVA_INC=1 THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE 
	ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE)+ROUND(SUM((CASE WHEN DAV.IVA_INC=1 THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE
	ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE)*(IVA.IVA/100)),2)+coalesce(CASE WHEN CLI.RECARGO=1 THEN round(SUM((CASE WHEN DAV.IVA_INC=1			THEN CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE 
		ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE)*(IVA.RECARG/100)),2) END, 0.00) as TOTALDOC,

CAV.TOTALPED,
isnull(CAV.PRONTO,0.00) as PRONTO,

isnull((select max(isnull(cast(PVERDE as int),0)) from ['+@GESTION+'].dbo.articulo where CODIGO in
(select ARTICULO from ['+@GESTION+'].DBO.D_PEDIVE where EMPRESA+NUMERO+LETRA=CAV.EMPRESA+CAV.NUMERO+CAV.LETRA and PVERDE>0)
),0) as PVERDEver

FROM ['+@GESTION+'].DBO.C_PEDIVE CAV 
INNER JOIN 
(select EMPRESA, NUMERO, LETRA, ARTICULO, IMPORTE, TIPO_iva, 1 AS INC_PP, 0 AS IVA_INC, PVERDE 
from ['+@GESTION+'].dbo.D_PEDIVE where TIPO_IVA<>'''') 
DAV ON DAV.EMPRESA=CAV.EMPRESA AND DAV.NUMERO=CAV.NUMERO AND DAV.LETRA=CAV.LETRA 
LEFT JOIN ['+@GESTION+'].dbo.tipo_iva IVA ON IVA.CODIGO=DAV.TIPO_IVA
INNER JOIN ['+@GESTION+'].dbo.clientes CLI ON CLI.CODIGO=CAV.CLIENTE
left join ['+@GESTION+'].dbo.entre_pv ent on ent.NUMERO=CAV.NUMERO and ent.EMPRESA=CAV.EMPRESA and ent.LETRA=CAV.letra
GROUP BY CAV.EMPRESA, CAV.NUMERO, CAV.LETRA, CAV.CLIENTE, CLI.RECARGO, IVA.IVA, IVA.RECARG, CAV.ENTREGA, ent.IMPORTE
	,CAV.TOTALDOC,CAV.PRONTO,CAV.TOTALPED,DAV.TIPO_IVA,CAV.PRONTO
')
select 'vPedidos_Pie'




IF EXISTS (SELECT * FROM sys.objects where name='vPedidos') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vPedidos] AS
SELECT  1 as Nodo
	, cast('''' as varchar(50)) as MODO
	, CONCAT('''+@EJERCICIO+''',CAV.empresa,replace(CAV.LETRA,space(1),''0''),replace(LEFT(CAV.NUMERO,10),space(1),''0'')) as IDPEDIDO	
	, '''+@EJERCICIO+''' as EJER , CAV.EMPRESA, convert(varchar(10), CAV.ENTREGA, 103) as ENTREGA
	, CAV.LETRA + CAV.NUMERO as PEDIDO , CAV.NUMERO as numero, CAV.LETRA, CAV.FECHA as sqlFecha
	, convert(varchar(10), CAV.FECHA, 103) as FECHA
	, CAV.CLIENTE, CAV.REFERCLI, CAV.ENV_CLI as DIRECCION, CAV.CLIENTE as codCliente 			
	, CAV.USUARIO, CAV.pronto, CAV.VENDEDOR
	, ISNULL(CAST(CAV.OBSERVACIO AS VARCHAR(max)),'''') AS OBSERVACIO
	, env.DIRECCION as nDireccion, ven.nombre as nVendedor
	, cli.nombre collate Modern_Spanish_CI_AI as nCliente, cli.RUTA
	, env.CODPOS+'' ''+env.POBLACION as Ciudad, env.PROVINCIA as Provincia
	, DAV.PRESUP as presupuesto, '''+@EJERCICIO+''' + CAV.empresa + CAV.letra + DAV.PRESUP as idpresuven
	, case when TRASPASADO=1 then ''TRASPASADO''
			when FINALIZADO=1 then ''FINALIZADO''
			when CANCELADO =1 then ''CANCELADO'' 
			else ''PENDIENTE'' END 
			as ESTADO			  
	, PCN.Contacto as contacto, CON.Persona  as nContacto
	, ser.nombre as laSerie		
	, CPA.FAMILIA as EWFAMILIA, FAM.NOMBRE as FAMILIA
	, sum(isnull(pie.TOTALDOC,0.00)) as TOTALDOC
	, case when sum(pie.TOTALDOC)>0 then 
		replace(replace(replace(convert(varchar, cast(sum(isnull(pie.TOTALDOC,0)) as money),1),''.'',''_''),'','',''.''),''_'','','') +'' €''
		else '''' end as TOTALDOCformato
	,CAV.TOTALDOC-CAV.TOTALPED AS importeIVA, CAV.TOTALDOC as totalConIVA
	,cast(0.00 as numeriC(18,2)) AS TOTAL, CAST(ROUND(CAV.TOTALPED,2) AS NUMERIC(18,2)) AS TOTALPED, cast(0.00 as numeriC(18,2)) as IVA

FROM ['+@GESTION+'].dbo.c_pedive CAV
INNER JOIN ['+@GESTION+'].dbo.clientes CLI ON CLI.CODIGO=CAV.CLIENTE
left join  ['+@GESTION+'].dbo.entre_pv ent on ent.NUMERO=CAV.NUMERO and ent.EMPRESA=CAV.EMPRESA and ent.LETRA=CAV.letra
left join  ['+@GESTION+'].dbo.env_cli env on env.cliente=cav.cliente and env.linea=cav.env_cli
left join  ['+@GESTION+'].dbo.vendedor ven on ven.codigo=cav.vendedor
LEFT JOIN (SELECT EMPRESA, NUMERO, LETRA, LEFT(MAX(DOC_NUM),10) AS PRESUP 
		FROM ['+@GESTION+'].DBO.D_PEDIVE 
		GROUP BY EMPRESA, NUMERO, LETRA) DAV 
		ON DAV.EMPRESA=CAV.EMPRESA AND DAV.LETRA=CAV.LETRA AND DAV.NUMERO=CAV.NUMERO
LEFT JOIN Pedidos_Contactos PCN ON PCN.IDPEDIDO COLLATE Modern_Spanish_CI_AI='''+@EJERCICIO+'''+CAV.LETRA+CAV.NUMERO
LEFT JOIN ['+@GESTION+'].DBO.CONT_CLI CON ON CON.CLIENTE=CAV.CLIENTE AND CON.LINEA=PCN.CONTACTO
LEFT JOIN Pedidos_Familias CPA ON CPA.EJERCICIO='''+@EJERCICIO+''' COLLATE Modern_Spanish_CI_AI
		AND CPA.NUMERO COLLATE Modern_Spanish_CI_AI=CAV.NUMERO AND CPA.LETRA COLLATE Modern_Spanish_CI_AI=CAV.LETRA
LEFT JOIN ['+@GESTION+'].DBO.FAMILIAS FAM ON FAM.CODIGO=CPA.FAMILIA COLLATE Modern_Spanish_CI_AI
LEFT JOIN vSeries ser on ser.codigo=CAV.LETRA
LEFT JOIN vPedidos_Pie pie on pie.IDPEDIDO collate Modern_Spanish_CI_AI=CONCAT('''+@EJERCICIO+''',CAV.empresa,replace(CAV.LETRA,space(1),''0''),replace(LEFT(CAV.NUMERO,10),space(1),''0''))
	
GROUP BY  CAV.empresa, CAV.LETRA, CAV.NUMERO, CAV.EMPRESA, CAV.ENTREGA, CAV.FECHA
	, CAV.CLIENTE, CAV.REFERCLI, CAV.ENV_CLI, CAV.CLIENTE	
	, CAV.USUARIO, CAV.pronto, CAV.VENDEDOR
	, env.DIRECCION, ven.nombre, CAST(CAV.OBSERVACIO AS VARCHAR(max))
	, cli.nombre collate Modern_Spanish_CI_AI
	, cli.RUTA, env.CODPOS, env.POBLACION, env.PROVINCIA, DAV.PRESUP, TRASPASADO, FINALIZADO, CANCELADO, PCN.Contacto
	, CON.Persona, ser.nombre, CPA.FAMILIA, FAM.NOMBRE,  CAV.TOTALDOC, CAV.TOTALPED	
')
select 'vPedidos'




IF EXISTS (SELECT * FROM sys.objects where name='vPedidos_Detalle') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vPedidos_Detalle] as
SELECT '''+@EJERCICIO+''' + D.empresa + cast(D.letra as char(2)) + D.numero + CAST(D.linia as varchar) AS IDPEDIDOLIN, 
CONCAT('''+@EJERCICIO+''',D.empresa,replace(D.LETRA,space(1),''0''),replace(LEFT(D.NUMERO,10),space(1),''0'')) AS IDPEDIDO, 
'''+@EJERCICIO+''' AS EJER, cast(D.letra as char(2))+d.numero as PEDIDO, D.CLIENTE, D.ARTICULO, D.DEFINICION, D.UNIDADES, D.cajas, 
isnull(D.PRECIO,0.00) as PRECIO,  
case when D.PRECIO>0 then 
replace(replace(replace(convert(varchar, cast(D.PRECIO as money),1),''.'',''_''),'','',''.''),''_'','','')+'' €'' else '''' end as PRECIOf, 
isnull(D.importe,0.00) as IMPORTE, 
case when D.importe>0 then 
replace(replace(replace(convert(varchar, cast(D.importe as money),1),''.'',''_''),'','',''.''),''_'','','')+'' €'' else '''' end as IMPORTEf,  
D.TIPO_IVA, COALESCE(iva.iva, 0) AS TPCIVA,  
CAST(ROUND(D.IMPORTE*COALESCE(iva.iva, 0)*0.01,2) AS NUMERIC(18,2)) AS IVA, D.SERVIDAS, D.LINIA, D.DTO1, D.DTO2, D.EMPRESA, 
D.importeiva AS IMPORTEIVA, 
isnull(D.PESO,0.00) as PESO, 
case when D.PESO>0 then 
replace(replace(replace(convert(varchar, cast(D.PESO as money),1),''.'',''_''),'','',''.''),''_'','','')+'' €'' else '''' end as PESOf,
D.NUMERO, cast(D.letra as char(2)) as LETRA,
art.UNICAJA, D.PVERDE,
case when D.PVERDE>0 then 
replace(replace(replace(convert(varchar, cast(D.PVERDE as money),1),''.'',''_''),'','',''.''),''_'','','')+'' €'' else '''' end as PVERDEf,

(select max(isnull(cast(PVERDE as int),0)) from ['+@GESTION+'].dbo.articulo where CODIGO in
(select CODIGO from ['+@GESTION+'].DBO.D_PEDIVE where EMPRESA+NUMERO+LETRA=D.EMPRESA+D.NUMERO+D.LETRA)
) as PVERDEver

FROM ['+@GESTION+'].DBO.D_PEDIVE D
LEFT JOIN ['+@GESTION+'].dbo.tipo_iva iva ON D.tipo_iva=iva.codigo
LEFT JOIN ['+@GESTION+'].dbo.articulo art ON art.CODIGO=D.ARTICULO
WHERE LEFT(D.CLIENTE,3)=''430''
')
select 'vPedidos_Detalle'




IF EXISTS (SELECT * FROM sys.objects where name='vPers_ContadorCajasAlbaran') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vPers_ContadorCajasAlbaran] as
select IDALBARAN, sum(cajas) as cajas from vAlbaranes_Detalle
group by IDALBARAN
')
select 'vPers_ContadorCajasAlbaran'




IF EXISTS (SELECT * FROM sys.objects where name='vReport_Pedidos') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vReport_Pedidos] as
SELECT "vClientes"."CP", "vClientes"."POBLACION", "vClientes"."PROVINCIA", "vClientes"."CIF", "vClientes"."DIRECCION", 
"vDatosEmpresa"."NOMBRE" as NombreEmpresa, "vDatosEmpresa"."DIRECCION" as DireccionEmpresa, "vDatosEmpresa"."CODPOS", 
"vDatosEmpresa"."POBLACION" as PoblacionEmpresa, "vDatosEmpresa"."PROVINCIA" as ProvinciaEmpresa, "vDatosEmpresa"."CIF" CifEmpresa, 
"vDatosEmpresa"."TXTFACTU1", "vDatosEmpresa"."TXTFACTU2", "vDatosEmpresa"."TELEFONO", "vDatosEmpresa"."FAX", "vDatosEmpresa"."WEB", 
"vClientes"."NOMBRE", "vPedidos"."CLIENTE", "vPedidos_Detalle"."IMPORTE", coalesce("vPedidos_Detalle"."DTO2",0) as DTO2, coalesce("vPedidos_Detalle"."DTO1",0) as DTO1, 
"vPedidos_Detalle"."DEFINICION", "vPedidos_Detalle"."UNIDADES", "vPedidos_Detalle"."PRECIO", "vPedidos".totalConIVA, "vPedidos"."TOTAL", 
"vPedidos".importeIVA, "vPedidos"."TOTALPED", "vClientes"."BANCO", "vClientes"."N_PAG", "vPedidos".numero, "vPedidos".FECHA, 
"vPedidos_Detalle".ARTICULO, "vPedidos".IDPEDIDO, "vPedidos".IVA, vPedidos_Detalle.TIPO_IVA, coalesce("vPedidos_Detalle".peso,0.00) as peso
, coalesce(vPedidos_Detalle.cajas, 0.00) as cajas, coalesce(vPedidos_Detalle.unicaja,0.00) as unicaja
FROM   (("vPedidos" "vPedidos" 
LEFT OUTER JOIN "vPedidos_Detalle" "vPedidos_Detalle" ON ("vPedidos"."IDPEDIDO"  COLLATE Modern_Spanish_CI_AI ="vPedidos_Detalle"."IDPEDIDO") 
				AND ("vPedidos"."EJER"="vPedidos_Detalle"."EJER")) 
LEFT OUTER JOIN "vDatosEmpresa" "vDatosEmpresa" ON "vPedidos"."EMPRESA"="vDatosEmpresa"."CODIGO") 
LEFT OUTER JOIN "vClientes" "vClientes" ON "vPedidos"."CLIENTE"="vClientes"."CODIGO"
')
select 'vReport_Pedidos'




IF EXISTS (SELECT * FROM sys.objects where name='vPers_ContadorPeso') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vPers_ContadorPeso] as
select idPedido, sum(peso) as peso from vReport_Pedidos
group by IdPedido
')
select 'vPers_ContadorPeso'




IF EXISTS (SELECT * FROM sys.objects where name='vPers_ContadorPesoAlbaran') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vPers_ContadorPesoAlbaran] as
select IDALBARAN, sum(PESO) as peso from vAlbaranes_Detalle
group by IDALBARAN
')
select 'vPers_ContadorPesoAlbaran'




IF EXISTS (SELECT * FROM sys.objects where name='vPvP') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vPvP]
as
SELECT tarifa+articulo as idpvp, articulo, tarifa, pvp, pvpiva
FROM ['+@GESTION+'].dbo.pvp
')
select 'vPvP'




IF EXISTS (SELECT * FROM sys.objects where name='vRutas') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +' view [dbo].[vRutas] as
select	codigo, nombre from ['+@GESTION+'].dbo.rutas
')
select 'vRutas'




IF EXISTS (SELECT * FROM sys.objects where name='vSTOCK_LOTES') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vSTOCK_LOTES] as
(
SELECT TOP 100 PERCENT A.CODIGO, A.NOMBRE, COALESCE(MOV.EMPRESA,''01'') AS EMPRESA, COALESCE(MOV.ALMACEN,'''') AS ALMACEN, COALESCE(MOV.TIPO,'''') AS TIPOMOV, COALESCE(MOV.LOTE,''00000000'') AS LOTE, COALESCE(MOV.UNIDADES,0) AS UNIDADES, COALESCE(MOV.PESO,0) AS PESO , MOV.FECHASTOCK, MOV.CADUCIDAD
FROM ['+@GESTION+'].DBO.ARTICULO A 
LEFT JOIN (
SELECT  ''IN'' AS TIPO, I.EMPRESA, I.ALMACEN, I.ARTICULO, I.LOTE, I.UNIDADES-I.DEPOVEN+I.DEPOCOM AS UNIDADES, I.PESO-I.PESOVEN+I.PESOCOM AS PESO, I.FECHA AS FECHASTOCK, I.CADUCIDAD FROM ['+@LOTES+'].DBO.LTREGUL I INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=I.ARTICULO AND LOT.LOTE=1 WHERE I.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND I.FECHA=(SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=I.EMPRESA AND ALMACEN=I.ALMACEN AND ARTICULO=I.ARTICULO AND LOTE=I.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE)
UNION ALL
SELECT  ''SI'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, (L.UNIDADES-L.DEPOVEN+L.DEPOCOM) AS UNIDADES, L.PESO-L.PESOVEN+L.PESOCOM AS PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTSTINI L  INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL 
SELECT ''TO'' AS TIPO, L.EMPRESA, L.ALMORIG, L.ARTICULO, L.LOTE AS LOTE, -L.UNIDADES, -L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTALBTR L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMORIG AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
		
SELECT ''TD'' AS TIPO, L.EMPRESA, L.ALMDEST, L.ARTICULO, L.LOTE AS LOTE, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTALBTR L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMDEST AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL 
SELECT ''AV'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, -(L.UNIDADES) as UNIDADES, -(L.PESO) AS PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD FROM ['+@LOTES+'].DBO.LTALBVE L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT ''AC'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD FROM ['+@LOTES+'].DBO.LTALBCO L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT  ''CP'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTCPROD L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT  ''DP'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, -L.UNIDADES, -L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTDPROD L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT  ''CT'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, -L.UNIDADES, -L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD FROM ['+@LOTES+'].DBO.LTCTRAN L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
		
UNION ALL
SELECT  ''DT'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD FROM ['+@LOTES+'].DBO.LTDTRAN L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT ''AR'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, -L.UNIDADES, -L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTALBRE L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT  ''DV'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, -L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTDEPVE L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT  ''DC'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTDEPCO L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
) MOV ON A.CODIGO=MOV.ARTICULO AND MOV.EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
ORDER BY 1,4
)
')
select 'vSTOCK_LOTES'




IF EXISTS (SELECT * FROM sys.objects where name='vStockLOTE') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vStockLOTE] as
SELECT     EMPRESA, CODIGO AS ARTICULO, ALMACEN, LOTE, SUM(COALESCE(UNIDADES,0)) AS UNIDADES, SUM(COALESCE(PESO,0)) AS PESO
, MAX(CADUCIDAD) AS CADUCIDAD
FROM  dbo.vSTOCK_LOTES /*WHERE CODIGO=''V338''*/
where EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
GROUP BY EMPRESA, CODIGO, ALMACEN, LOTE
HAVING SUM(COALESCE(UNIDADES,0))<>0
')
select 'vStockLOTE'




IF EXISTS (SELECT * FROM sys.objects where name='vZonas') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vZonas] as
select M.CODIGO, M.NOMBRE, V.VALOR 
from ['+@GESTION+'] .dbo.mcampos m 
inner join ['+@GESTION+'].dbo.valmulti v on v.codigo=m.codigo where m.codigo=''ZNA''
')
select 'vZonas'




IF EXISTS (SELECT * FROM sys.objects where name='vImpresion') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view vImpresion
as
select distinct tipo, ltrim(rtrim(crNumero)) as crNumero, crEmpresa, crLetra 
from MI_ImpresionPDF
where fechaInsertUpdate>dateadd(week,-1,getdate())
')
select 'vImpresion'




	RETURN -1
END TRY

BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH