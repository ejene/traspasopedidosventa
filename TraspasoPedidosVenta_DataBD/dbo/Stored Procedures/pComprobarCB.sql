﻿CREATE PROCEDURE [dbo].[pComprobarCB] (@parametros varchar(max))
AS
/*-----------------------------------------------------------------------------------
--#AUTHOR: JUAN CARLOS VILLALOBOS
--#NAME: pComprobarCB
--#CREATION: 06/05/2021
--#DESCRIPTION: Comprobación y análisis de un código de barras
--#PARAMETERS:	@parametros formato JSON (@codigoBarras)
--#CHANGES
-- 06/05/2021 - JUAN CARLOS VILLALOBOS Creación del procedimiento
-- 14/06/2021 - JUAN CARLOS VILLALOBOS Adaptacion al nuevo script
-- 07/10/2021 - Elías Jené - Si existe el identificador 37 obtenemos las unidades de los siguientes 8 dígitos
--						   - Tomamos 13 dígitos en lugar de 14 para encontrar el artículo
-- 14/12/2021 - Albert Rodríguez: Ajustar retorno datos.
-- 23/12/2021 - Elías Jené: Dinamización del procedimiento.
-------------------------------------------------------------------------------------

Pasos a seguir al leer un código de barras:

Comprobamos si el código EAN existe en la BBDD:
1.	No Existe
Mostramos el aviso “El código EAN leído no está asociado a ningún artículo”.
Dejamos el cursor en el campo BARRAS

2.	Existe
Comprobamos si el artículo asociado al código de barras está dentro del documento

	a.	No existe
	Mostramos el aviso “El artículo asociado al código EAN leído no está dentro del documento”.
	Dejamos el cursor en el campo BARRAS

	b.	Existe
	Obtenemos las unidades del código EAN.
	Primero revisaremos si el código leído tiene el prefijo 37, si no es así, obtendremos las unidades de la tabla Barras y en el caso de que no hayamos obtenido ningún valor aun, asignaremos un 1.
	Revisaremos si las unidades obtenidas son mayores a las unidades pendientes de la línea del documento (si hay varias líneas con el mismo código, sumaremos las unidades de estas líneas al comparar).

		i.	Unidades mayores
		Mostramos el aviso “Las unidades leídas, superan las unidades pendientes”.
		Dejamos el cursor en el campo BARRAS

		ii.	Unidades menores
		Comprobaremos si el artículo trabaja con lotes

Lógica:
1.	No lotes
Insertamos las unidades leídas en el campo correspondiente, si ya hay valores, los sumamos. 

2.	Lotes
	Funcionalidad Artículos Lotes 
	1.	Lote
		a.	Si no tenemos el lote, revisaremos si el ERP trabaja con lote automático
			i.	Lote Automático
				Asignaremos las unidades leídas al lote con la fecha de caducidad más cercana, si hay varios lotes con la misma caducidad, asignaremos las unidades al lote más reciente.
			ii.	No lote Automático
				Abriremos la pantalla de lotes.
				Si tenemos el lote, añadiremos las unidades al lote leído.
	2.	Peso
		a.	Si el artículo trabaja con peso, si hemos recibido el peso al leer el código EAN, insertaremos el lote con el peso obtenido. 
		b.	Si no tenemos el peso, se abrirá la pantalla de lotes, añadiremos las unidades al lote correspondiente a la espera de poner el peso.

=========================================================================================================================================================================

ARTICULO	CODIGO BARRAS										EAN					LOTE			PESO
MI16104		010040000007300210PI21000402						00400000073002		PI21000402	
MI16105		01984370019350041026007215220726					98437001935004		260072	
MI-24302	01184252140010603103000700152101151020392124		18425214001060		20392124		000700
MILP		0198411477904619152101213103000615103910			98411477904619		3910			000615
MILCP		01984370036042043102001062152105181019056001		98437003604204		19056001		001062
			01004000000108543103004800370000001010CA21000925	(Identificador 37 - los 8 siguientes dígitos son las unidades)
ET1085		0100400000010854310300480037000000101020211220430954
MIL			0178414620091583172202221020211220430955
MI-24302	01184252140010603103003000152101151020211220430956

[pComprobarCB] '{"sp":"pComprobarCB","CODBAR":"01984370036042043102001062152105181019056001"}'
*/

-----------------------------------------------------------------------------------------------
--	Parámetros JSON 
	declare @CODBAR VARCHAR(255) = isnull((select JSON_VALUE(@parametros,'$.CODBAR')),'')  
-----------------------------------------------------------------------------------------------
--	Variables de Configuración 
	declare @EMPRESA char(2), @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select  @EMPRESA=Empresa, @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
-----------------------------------------------------------------------------------------------
--	Variables TABLA
	declare @tbBarras TABLE (ARTICULO varchar(20), UNIDADES numeric(15,6), VENTA_CAJA int)
	declare @tbLote TABLE (LOTE bit)
	declare @tbArticulo TABLE (UNICAJA int)
-----------------------------------------------------------------------------------------------
-- Variables del procedimiento
DECLARE	@ARTICULO CHAR(50),
		@BARRAS CHAR(50),
		@UDS DECIMAL(16,4),
		@CAJAS DECIMAL(16,4),
		@PESO DECIMAL(16,4)=0.0000,
		@PESOBAR CHAR(6),
		@CADUCIDAD CHAR(8),
		@LOTE CHAR(50),
		@EXISTE BIT,
		@ARTLOT BIT, 
		@TIPOEAN CHAR(5), 
		@FALTACOD BIT,
		@UNICAJA numeric(15,6),
		@VENTA_CAJA smallint,
		@DECIMALES INT,
		@MISSATGE VARCHAR(255),
		@sql varchar(max),
		@respuesta varchar(max)
-----------------------------------------------------------------------------------------------

BEGIN TRY
	-- Comprobar si el codigo de barras leido ya está relacionado con algún artículo. 
	-- Esto se realiza por si el código leido es un EAN13, si es un GS1 nunca lo encontraremos.
	declare @registros int=0
	declare @tbReg TABLE (registros int)
	INSERT @tbReg EXEC('SELECT COUNT(ARTICULO) FROM ['+@GESTION+'].[DBO].BARRAS WHERE LEFT(BARRAS,13) = LEFT('''+@CODBAR+''',13)')	
	SELECT @registros=registros FROM @tbReg		
	delete @tbReg
	IF @registros>0	BEGIN
		SET @BARRAS = LEFT(@CODBAR,13)
		-- Si existe el codigo de barras, obtenemos el código de artículo y las unidades			
		INSERT @tbBarras EXEC('
			SELECT	ARTICULO
				,	UNIDADES 
				,	CASE WHEN coalesce(m2.valor,'''')='''' OR  coalesce(m2.valor,'''')=''.F.'' THEN 0 ELSE 1 END
			FROM ['+@GESTION+'].DBO.BARRAS 
			left join ['+@GESTION+'].dbo.multicam m2 on m2.codigo=ARTICULO and m2.campo=''SCA''
			WHERE LEFT(BARRAS,13)='''+@BARRAS+'''		
		')	
		SELECT @ARTICULO=ARTICULO, @UDS=UNIDADES, @VENTA_CAJA=VENTA_CAJA FROM @tbBarras		
		delete @tbBarras
		-- obtener LOTE			
		INSERT @tbLote EXEC('SELECT LOTE FROM ['+@LOTES+'].DBO.artlot WHERE ARTICULO='''+@ARTICULO+'''')	
		SELECT @ARTLOT=LOTE FROM @tbLote		
		delete @tbLote
		-- obtener UNICAJA del artículo			
		INSERT @tbArticulo EXEC('SELECT isnull(UNICAJA,0) FROM ['+@GESTION+'].DBO.ARTICULO WHERE CODIGO='''+@ARTICULO+'''')	
		SELECT @UNICAJA=UNICAJA FROM @tbArticulo		
		delete @tbArticulo

		SET @PESO=0.0000
		SET @CADUCIDAD=''
		SET @LOTE=''
		SET @EXISTE=1
		SET @TIPOEAN = 'EAN13'
		set @FALTACOD = 0
		SET @DECIMALES = 0
				
		-- Mensaje de retorno
		set @respuesta = CONCAT('{"Error":"","EXISTE":"',@EXISTE,'","ARTLOT":"',@ARTLOT,'","ARTICULO":"',ltrim(rtrim(@ARTICULO)),'","UNIDADES":"',@Uds,'"'
						,',"UNICAJA":"',@UNICAJA,'","PESO":"',@PESO,'","CADUCIDAD":"',@CADUCIDAD,'","LOTE":"',ltrim(rtrim(@LOTE)),'","VENTA_CAJA":"',@VENTA_CAJA,'"'
						,',"TIPOEAN":"',@TIPOEAN,'","FALTACOD":"',@FALTACOD,'","EAN":"',@CODBAR,'"}')
	END
	-- Si no encontramos el código, comprobamos si es un código GS1
	ELSE BEGIN   
		-- Si los dos primeros dígitos es 01 sabemos que los siguientes 14 dígitos  
		-- son el código de barras del artículo, el cual buscaremos en la tabla correspondiente.
		IF SUBSTRING(@CODBAR,1,2) = '01' BEGIN
			-- comprobar si el código de barras está dividido. Si lo está pedimos la segunda lectura.
			IF LEN(@CODBAR)<27 and SUBSTRING(@CODBAR,17,2) != '10' BEGIN set @respuesta = '{"Error":"CodigoDividido"}' return -1 END

			SET @BARRAS = SUBSTRING(@CODBAR,3,13)
			INSERT @tbReg EXEC('SELECT COUNT(ARTICULO) FROM ['+@GESTION+'].[DBO].BARRAS WHERE LEFT(BARRAS,13) = '''+@BARRAS+'''')	
			SELECT @registros=registros FROM @tbReg		
			delete @tbReg
			IF @registros>0 BEGIN
				-- Si existe el codigo de barras, obtenemos el código de artículo y las unidades
				INSERT @tbBarras EXEC('
					SELECT	ARTICULO
						,	UNIDADES 
						,	CASE WHEN coalesce(m2.valor,'''')='''' OR  coalesce(m2.valor,'''')=''.F.'' THEN 0 ELSE 1 END
					FROM ['+@GESTION+'].DBO.BARRAS 
					left join ['+@GESTION+'].dbo.multicam m2 on m2.codigo=ARTICULO and m2.campo=''SCA''
					WHERE LEFT(BARRAS,13)='''+@BARRAS+'''		
				')	
				SELECT @ARTICULO=ARTICULO, @UDS=UNIDADES, @VENTA_CAJA=VENTA_CAJA FROM @tbBarras		
				delete @tbBarras
				-- obtener LOTE
				INSERT @tbLote EXEC('SELECT LOTE FROM ['+@LOTES+'].DBO.artlot WHERE ARTICULO='''+@ARTICULO+'''')	
				SELECT @ARTLOT=LOTE FROM @tbLote		
				delete @tbLote
				-- obtener UNICAJA del artículo
				INSERT @tbArticulo EXEC('SELECT isnull(UNICAJA,0) FROM ['+@GESTION+'].DBO.ARTICULO WHERE CODIGO='''+@ARTICULO+'''')	
				SELECT @UNICAJA=UNICAJA FROM @tbArticulo		
				delete @tbArticulo

				SET @EXISTE=1
				SET @TIPOEAN = 'GS1'
				SET @FALTACOD = 0
				-- Obtenemos las unidades por caja y el peso por unidad para devolver el valor correspondiente 
				-- según las unidades del código de barras
				declare @tbArt2 TABLE (CAJAS numeric(15,6), PESO varchar(20))
				INSERT @tbArt2 EXEC('SELECT CASE WHEN UNICAJA=0 THEN 0 ELSE CONVERT(INT,1/UNICAJA) END as CAJAS
									, cast(cast(CASE WHEN ltrim(rtrim(PESO))='''' THEN ''0.00'' ELSE REPLACE(PESO,'','',''.'') END as numeric(15,6)) * '+@UDS+' as varchar(50)) as PESO
									FROM ['+@GESTION+'].DBO.ARTICULO WHERE CODIGO='''+@ARTICULO+'''')	
				SELECT @CAJAS=CAJAS, @PESO=PESO FROM @tbArt2		
				delete @tbArt2

				-- Si el siguiente tramos del código es 310, obtendremos el peso								
				IF SUBSTRING(@CODBAR,17,3) = '310' BEGIN
					SET @PESOBAR = SUBSTRING(@CODBAR,21,6)
					SET @DECIMALES = SUBSTRING(@CODBAR,20,1)
					-- Si el siguiente tramo del código es 15, obtendremos la caducidad
					IF SUBSTRING(@CODBAR,27,2) = '15' OR SUBSTRING(@CODBAR,27,2) = '17' BEGIN
						SET @CADUCIDAD = SUBSTRING(@CODBAR,29,6)
						-- Si el siguiente tramos del código es 10, obtendremos el lote
						IF SUBSTRING(@CODBAR,35,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,37,50)
					END
					-- Si el siguiente tramo del código es 37, obtendremos las unidades de la caja, las cuales substituirán las unidades del ERP				
					IF SUBSTRING(@CODBAR,27,2) = '37' BEGIN
						set @Uds=cast(SUBSTRING(@CODBAR,29,8) as decimal(16,4))						
						-- Si el siguiente tramos del código es 10, obtendremos el lote
						IF SUBSTRING(@CODBAR,37,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,39,50)
					END	
					-- Si el siguiente tramo del código es 10, obtendremos el lote
					IF SUBSTRING(@CODBAR,27,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,29,50)
					-- Si el siguiente tramo está vacío quiere decir que el código 
					-- está dividido en 2 y hay que leer la segunda parte.
					-- Cuando pasa esto el procedimiento devuelve un aviso para que la aplicación lea el resto 
					-- del código y vuelva a enviarlo uniendo las dos lecturas
					IF SUBSTRING(@CODBAR,27,2) = ' ' SET @FALTACOD = 1
				END			
				-- Si el siguiente tramo del código es 15 ó 17, obtendremos la caducidad
				IF SUBSTRING(@CODBAR,17,2) = '15' OR SUBSTRING(@CODBAR,17,2) = '17' BEGIN
					SET @CADUCIDAD = SUBSTRING(@CODBAR,19,6)
					-- Si el siguiente tramo del código es 310, obtendremos el peso
					IF SUBSTRING(@CODBAR,25,3) = '310' BEGIN
						SET @PESOBAR = SUBSTRING(@CODBAR,29,6)
						SET @DECIMALES = SUBSTRING(@CODBAR,28,1)
						-- Si el siguiente tramo del código es 10, obtendremos el lote
						IF SUBSTRING(@CODBAR,35,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,37,50)
					END
					-- Si el siguiente tramo del código es 10, obtendremos el lote
					IF SUBSTRING(@CODBAR,25,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,27,50)
					-- Si el siguiente tramo está vacío quiere decir que el código 
					-- está dividido en 2 y hay que leer la segunda parte.
					-- Cuando pasa esto el procedimiento devuelve un aviso para que la aplicación lea el resto 
					-- del código y vuelva a enviarlo uniendo las dos lecturas
					IF SUBSTRING(@CODBAR,25,2) = ' ' SET @FALTACOD = 1
				END
				-- Si el siguiente tramos del código es 10, obtendremos el lote.
				IF SUBSTRING(@CODBAR,17,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,19,50)

				IF @DECIMALES = 1 SET @DECIMALES = 10								
				IF @DECIMALES = 2 SET @DECIMALES = 100								
				IF @DECIMALES = 3 SET @DECIMALES = 1000							
				IF @PESOBAR!='000000' SET @PESO=CONVERT(DECIMAL(16,4),@PESOBAR)/@DECIMALES
				if @Uds=0 set @Uds=1

				set @respuesta = CONCAT('{"Error":"","EXISTE":"',@EXISTE,'","ARTLOT":"',@ARTLOT,'","ARTICULO":"',ltrim(rtrim(@ARTICULO)),'","UNIDADES":"',@Uds,'"'
								,',"UNICAJA":"',@UNICAJA,'","PESO":"',@PESO,'","CADUCIDAD":"',@CADUCIDAD,'","LOTE":"',ltrim(rtrim(@LOTE)),'","VENTA_CAJA":"',@VENTA_CAJA,'"'
								,',"TIPOEAN":"',@TIPOEAN,'","FALTACOD":"',@FALTACOD,'","EAN":"',@CODBAR,'"}')
			END ELSE BEGIN
				-- Si el código de barras no esta asociado a ningún artículo no continuamos.
				SET @MISSATGE='ERROR001 - El Código de Barras no está asociado a ningún artículo!'
				set @respuesta = '{"Error":"'+@MISSATGE+'"}'
			END						
		END
		-- Si hemos llegado aquí y el código no empieza por 01, no hacemos más comprobaciones. 
		-- Entendemos que el código no está asociado a ningún artículo.
		ELSE BEGIN
			SET @MISSATGE='ERROR001 - El Código de Barras no está asociado a ningún artículo!'
			set @respuesta = '{"Error":"'+@MISSATGE+'"}'
		END								
	END 
	SELECT @respuesta AS JAVASCRIPT
	RETURN -1
END TRY
BEGIN CATCH
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError=CONCAT(ERROR_MESSAGE(),ERROR_NUMBER(),ERROR_PROCEDURE(),@@PROCID ,ERROR_LINE())
	RAISERROR(@CatchError,12,1) 
	RETURN 0
END CATCH