﻿CREATE PROCEDURE [dbo].[pBuscarGlobal] @parametros varchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		-----------------------------------------------------------------------------------------------
		--									Parámetros JSON 
		-----------------------------------------------------------------------------------------------
		declare @pedidoActivo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.pedidoActivo')),'')
			,	@numero varchar(50) = isnull((select JSON_VALUE(@parametros,'$.numero')),'')
			,	@serie varchar(50) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
			,	@ruta varchar(50) = isnull((select JSON_VALUE(@parametros,'$.ruta')),'')
			,	@zona varchar(50) = isnull((select JSON_VALUE(@parametros,'$.zona')),'')
			,	@articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
			,	@entrega varchar(50) = isnull((select JSON_VALUE(@parametros,'$.entrega')),'')
			,	@campo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.campo')),'')
		declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			,	@rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
		-----------------------------------------------------------------------------------------------
		--								Variables de Configuración  
		-----------------------------------------------------------------------------------------------
		declare @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
		select @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
		-------------------------------------------------------------------------------------------------

		declare @laConsulta varchar(max)=''
		declare @fechaDeManyana varchar(10) = format(getdate()+1,'yyyy-MM-dd')
		declare @fechaHoyMas2 varchar(10) = format(getdate()+2,'yyyy-MM-dd')
		declare @laVista varchar(100) = 'd_Busca_PV'
		declare @elWhere varchar(max) = ' where 1=1 '
		declare @elOrderBy varchar(max) = ' order by a.entrega asc, a.numero asc '
		declare @elInnerJoin varchar(max) = ''
	
		-- eliminamos el registro de [COMU0001].[DBO].[en_uso] si procede
		EXEC('delete from ['+@COMUN+'].[DBO].[en_uso] where CLAVE='''+@pedidoActivo+''' and USUARIO='''+@usuario+'''')
		
		-- obtener/asignar filtros de usuario
		if @campo<>'' BEGIN
			declare @orden varchar(4)='DESC'
			if not exists (select * from Usuarios_Filtros where usuario=@usuario and campo=@campo) insert into Usuarios_Filtros (usuario,campo,orden) values (@usuario,@campo,@orden)
			select @orden=orden from Usuarios_Filtros where usuario=@usuario and campo=@campo
			if @orden='ASC' set @orden='DESC' else set @orden='ASC'
			update Usuarios_Filtros set orden=@orden where usuario=@usuario and campo=@campo
		END
		
		-- montar filtros
		declare @existenFiltros bit = 0
		declare @_campo varchar(50), @_orden varchar(4)
		declare @CampoOrden varchar(max)=''
		declare elCursor CURSOR for select campo, orden from Usuarios_Filtros where usuario=@usuario
		open elCursor FETCH NEXT FROM elCursor INTO @_campo, @_orden
		WHILE (@@FETCH_STATUS=0) BEGIN
				set @CampoOrden = @CampoOrden + ' a.' + @_campo + ' ' + @_orden + ','
				set @existenFiltros=1
			FETCH NEXT FROM elCursor INTO @_campo, @_orden
		END	CLOSE elCursor deallocate elCursor
		if @existenFiltros=1 BEGIN 
			set @CampoOrden=SUBSTRING(@CampoOrden,0,LEN(@CampoOrden)) 
			set @elOrderBy = ' order by '+@CampoOrden
		END

		if @numero<>'' set @elWhere = @elWhere+' and ltrim(rtrim(numero))=ltrim(rtrim('''+@numero+''')) '
		if @serie<>'' set @elWhere = @elWhere+' and upper(a.letra)=upper('''+@serie+''') '
		if @ruta<>'' set @elWhere = @elWhere+' and upper(a.ruta)=upper('''+@ruta+''') '
		if @zona<>'' BEGIN set @elWhere = @elWhere+' and upper(a.ZONA)=upper('''+@zona+''') ' set @laVista='"d_Busca_PV_Zona"' END
		if @entrega<>'' set @elWhere = @elWhere+' and cast(FORMAT(a.entrega,''yyyyMMddHHmmss'') as bigint) 
										<= cast(FORMAT(cast('''+@entrega+''' as datetime),''yyyyMMddHHmmss'') as bigint)  '
		if @articulo<>'' set @elInnerJoin = '
			inner join ['+@GESTION+'].[dbo].[d_pedive] b on CONCAT(b.EMPRESA,b.NUMERO,b.LETRA)=CONCAT(a.EMPRESA,a.NUMERO,a.LETRA) 
			and (UPPER(b.DEFINICION) like ''%''+UPPER('''+@articulo+''')+''%'' or UPPER(b.ARTICULO) like ''%''+UPPER('''+@articulo+''')+''%'' )  
		'
	
		set @laConsulta = 'select isnull(
							(select distinct a.*
								, isnull((select campo, orden from Usuarios_Filtros where usuario='''+@usuario+''' FOR JSON AUTO, INCLUDE_NULL_VALUES),''[]'') as filtros
							from ' + @laVista + ' a ' + @elInnerJoin + @elWhere + @elOrderBy 
						+ ' FOR JSON AUTO, INCLUDE_NULL_VALUES)
						,''[]'') AS JAVASCRIPT'
	
		exec (@laConsulta)
		return -1 
	END TRY
	BEGIN CATCH
		DECLARE @CatchError NVARCHAR(MAX)
		SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
		select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
		RAISERROR(@CatchError,12,1)
		RETURN 0
	END CATCH
END
