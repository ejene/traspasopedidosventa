﻿CREATE PROCEDURE [dbo].[pEmpresas] @parametros varchar(max)
AS
BEGIN TRY
	declare @comun varchar(50) = (select JSON_VALUE(@parametros,'$.comun'))


	DECLARE @respuesta varchar(max)=''	
	DECLARE @sql nvarchar(max)
	declare @ejercicio varchar(10)
	declare @codigo varchar(50)
	declare @nombre varchar(100)

	-- obtener ejercicio predeterminado
	declare @SentenciaSQL varchar(max) = 'SELECT ltrim(rtrim(RUTA)) as RUTA INTO ##pEmpresasTMP1 FROM ['+@comun+'].dbo.ejercici where predet=1'
	EXEC (@SentenciaSQL)
	SET @ejercicio = (SELECT RUTA FROM ##pEmpresasTMP1)
	DROP TABLE ##pEmpresasTMP1
	
	-- Obtener empresas
	set @sql = '
		select CODIGO, replace(ltrim(rtrim(NOMBRE)),'''''''',''-'') as NOMBRE
		into ##pEmpresasTMP2
		from ['+@ejercicio+'].dbo.empresa where TIPO=''Normal''		
	'
	EXEC (@sql)

	DECLARE CUREmpresas CURSOR FOR
	select CODIGO, NOMBRE from ##pEmpresasTMP2
	OPEN CUREmpresas
	FETCH NEXT FROM CUREmpresas INTO @codigo, @nombre
	WHILE (@@FETCH_STATUS=0) BEGIN
		set @respuesta = @respuesta + '{"codigo":"'+@codigo+'","nombre":"'+@nombre+'"}'
	FETCH NEXT FROM CUREmpresas INTO @codigo, @nombre 
	END	CLOSE CUREmpresas deallocate CUREmpresas

	drop table ##pEmpresasTMP2

	select '{"empresas":['+isnull(replace(@respuesta,'}{','},{'),'')+']}' as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select @CatchError as JAVASCRIPT
	RETURN 0
END CATCH