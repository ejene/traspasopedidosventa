﻿CREATE PROCEDURE [dbo].[pTraspLinCompleta] @parametros varchar(max)
AS
/*
[pTraspLinCompleta]
'{"sp":"pTraspLinCompleta","almacen":"00","articulo":"MIL      ","cajas":"0","unidades":"0","peso":"0","linea":"2","numero":"         1","serie":"PR"}'
*/
BEGIN TRY	
	SET NOCOUNT ON	
	begin tran
	-----------------------------------------------------------------------------------------------
	-- Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare	  @almacen varchar(2) = isnull((select JSON_VALUE(@parametros,'$.almacen')),'')
			, @articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
			, @cajas int = cast(isnull((select JSON_VALUE(@parametros,'$.cajas')),'0') as int)
			, @unidades int = cast(isnull((select JSON_VALUE(@parametros,'$.unidades')),'0') as int)
			, @peso varchar(10) = isnull((select JSON_VALUE(@parametros,'$.peso')),'0')
			, @linea int = cast(isnull((select JSON_VALUE(@parametros,'$.linea')),'0') as int)
			, @numero char(10) = isnull((select JSON_VALUE(@parametros,'$.numero')),'')
			, @serie varchar(10) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	declare @laSesion varchar(50) = @usuario
	declare @nu varchar(50) = 
	replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','')   
	-----------------------------------------------------------------------------------------------
	-- Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EJERCICIO char(4)
	declare @GESTION char(6)
	declare @COMUN char(8)
	declare @LOTES char(8)
	select @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-----------------------------------------------------------------------------------------------
	-- Variables del SP 
	-----------------------------------------------------------------------------------------------
	declare @sql nvarchar(max)	
		,	@respuesta varchar(max) = ''
	-----------------------------------------------------------------------------------------------

	declare   @empresa varchar(10)	= (select Empresa from Configuracion)
			, @cliente varchar(20)
			, @ruta varchar(10)
			, @artUnicaja int
			, @artPeso varchar(10)
			, @newCajas int			= 0
			, @newUnidades int		= 0
			, @newPeso varchar(10)	= ''

	
	-- tabla temporal para recopilación de datos ------------------------------------------------------
	declare @tablaTemp TABLE (CLIENTE nvarchar(20), RUTA varchar(10), UNICAJA int, PESO varchar(20))
	SET @sql = 'select top 1 CLIENTE 
				, (select top 1 ruta from Detalle_Pedidos where numero='''+@numero+''') as RUTA
				, (select UNICAJA from ['+@GESTION+'].dbo.articulo where CODIGO='''+@articulo+''') as UNICAJA
				, (select PESO from ['+@GESTION+'].dbo.articulo where CODIGO='''+@articulo+''') as PESO
				from Detalle_Pedidos where numero='''+@numero+''''
	INSERT @tablaTemp EXEC(@sql)	
	SELECT @cliente=CLIENTE, @ruta=RUTA, @artUnicaja=UNICAJA, @artPeso=PESO FROM @tablaTemp
	delete @tablaTemp
	--------------------------------------------------------------------------------------------------

	if (select count(NUMERO) from vBuscarLotes where EMPRESA=@empresa and LETRA=@serie and NUMERO=@numero and ARTICULO=@articulo and LINIA=@linea and Llote is not null)>0 BEGIN
					
		--	Trabaja con lotes ------------------------------
			declare @_pesoArticulo varchar(10), @_Lcaducidad smalldatetime, @_Llote varchar(50), @_Lunidades int
					, @_Lpeso numeric(15,6), @_ARTICULO varchar(20), @_UNIDADES int, @_SERVIDAS int, @_UNICAJA int
					, @_pCajas numeric(15,6), @_pCajaServ numeric(15,6), @_tempCajas numeric(15,6)
					, @_tempMaxCajas numeric(15,6), @_tempPeso varchar(10), @_tempUnidades numeric(15,6)
							  
			declare @unidadesG int = @unidades					 
					, @cajasG int
					, @pesoG varchar(10)

			declare elCursor CURSOR LOCAL STATIC for
				select pesoArticulo, Lcaducidad, Llote, Lunidades, Lpeso, ARTICULO, UNIDADES, SERVIDAS, UNICAJA
						, pCajas, pCajaServ, tempCajas, tempMaxCajas, tempPeso, tempUnidades
				from vBuscarLotes 	
				where EMPRESA=@empresa and LETRA=@serie and NUMERO=@numero and ARTICULO=@articulo and LINIA=@linea  and Llote is not null
				order by Lcaducidad asc	
			open elCursor
			FETCH NEXT FROM elCursor INTO @_pesoArticulo, @_Lcaducidad, @_Llote, @_Lunidades, @_Lpeso, @_ARTICULO, @_UNIDADES
										, @_SERVIDAS, @_UNICAJA, @_pCajas, @_pCajaServ, @_tempCajas , @_tempMaxCajas, @_tempPeso
										, @_tempUnidades
			WHILE @@FETCH_STATUS=0 AND @unidadesG>0 BEGIN	
				declare @udsAcero int=0;
				if @_Lunidades<=@unidadesG begin set @unidadesG=@_Lunidades end else begin set @udsAcero=1 end
				if @artUnicaja>0 set @cajasG = @unidadesG / @artUnicaja
				if @artPeso<>'' set @pesoG = cast((cast(@artPeso as numeric(15,6)) * @unidadesG) as varchar(10))
						
				delete Traspaso_Temporal 
				where  empresa=@empresa and numero=@numero and letra=@serie and linea=@linea and articulo=@articulo and lote=@_Llote  and cliente=@cliente
							
				if @PesoG='' set @PesoG='0'

				insert into Traspaso_Temporal 
				(sesion, id, empresa, numero, letra, linea, articulo, lote, cajas, unidades, peso, cliente, ruta, almacen, caducidad) 
				values 
				(@laSesion, @nu, @empresa, @numero, @serie, @linea, @articulo, @_Llote, isnull(@cajasG,0), isnull(@unidadesG,0), isnull(@PesoG,'0'), @cliente
				, @ruta, @almacen, @_Lcaducidad)

				if @udsAcero=1 begin set @unidadesG=0 end else begin set @unidadesG = @unidades - @unidadesG end

				FETCH NEXT FROM elCursor INTO @_pesoArticulo, @_Lcaducidad, @_Llote, @_Lunidades, @_Lpeso, @_ARTICULO, @_UNIDADES
										, @_SERVIDAS, @_UNICAJA, @_pCajas, @_pCajaServ, @_tempCajas , @_tempMaxCajas, @_tempPeso
										, @_tempUnidades
			END close elCursor deallocate elCursor
			set @respuesta = 'Artículo con lotes'
	END ELSE BEGIN

		--	NO trabaja con lotes ------------------------------
		if @artUnicaja>0  set @cajas=@unidades/@artUnicaja
		if @artPeso<>'' set  @peso = cast( (@unidades * cast(@artPeso as numeric(15,6)) ) as varchar(10))
		if @artPeso='' set @artPeso='0.00'
					
		delete Traspaso_Temporal 
		where	empresa=@empresa and numero=@numero and letra=@serie and linea=@linea and articulo=@articulo 
				and lote='-'  and cliente=@cliente
		
		if @Peso='' set @Peso='0'

		insert into Traspaso_Temporal 
		(sesion, id, empresa, numero, letra, linea, articulo, lote, cajas, unidades, peso, cliente, ruta, almacen, caducidad) 
		values 
		(@laSesion, @nu, @empresa, @numero, @serie, @linea, @articulo, '-', isnull(@cajas,0), isnull(@unidades,0), isnull(@Peso,'0'), @cliente
		, @ruta, @almacen, '')

		set @respuesta = 'Artículo sin lotes'
	END

	select @respuesta as JAVASCRIPT
	commit tran
	RETURN -1
END TRY

BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	rollback tran
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
