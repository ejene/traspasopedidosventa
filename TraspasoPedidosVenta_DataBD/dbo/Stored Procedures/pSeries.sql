﻿CREATE PROCEDURE [dbo].[pSeries] @parametros varchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

		declare @modo varchar(50) = (select isnull((select JSON_VALUE(@parametros,'$.modo')),'[]'))
			,	@objeto varchar(50) = (select isnull((select JSON_VALUE(@parametros,'$.objeto')),''))
			,	@serie char(2) = (select isnull((select JSON_VALUE(@parametros,'$.serie')),''))


		-- Series Asignar
		if @modo='asignarSerieConf' BEGIN 
			if exists (select * from ConfigSeries where objeto=@objeto and serie=@serie) delete ConfigSeries where objeto=@objeto and serie=@serie
			else insert into ConfigSeries (objeto,serie) values (@objeto,@serie)
			return -1 
		END


		-- Series del Objeto
		if @modo='cargarSeriesObjeto' BEGIN 
			select isnull((select isnull(serie,'') as codigo from ConfigSeries where objeto=@objeto FOR JSON AUTO, INCLUDE_NULL_VALUES),'[]') as JAVASCRIPT 
			return -1 
		END


		-- Series
		select isnull((select codigo, nombre from vSeries order by codigo ASC FOR JSON AUTO, INCLUDE_NULL_VALUES),'[]') as JAVASCRIPT
		
		return -1 
	END TRY
	BEGIN CATCH
		DECLARE @CatchError NVARCHAR(MAX)
		SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
		RAISERROR(@CatchError,12,1)
		RETURN 0
	END CATCH
END