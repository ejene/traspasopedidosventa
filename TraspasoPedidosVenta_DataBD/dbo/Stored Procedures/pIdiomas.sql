﻿CREATE PROCEDURE [dbo].[pIdiomas] @parametros varchar(max)=''
AS
BEGIN TRY
	declare   @modo varchar(50)		  = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
			, @idioma varchar(50)	  = isnull((select JSON_VALUE(@parametros,'$.idioma')),'')
			, @nombre varchar(100)	  = isnull((select JSON_VALUE(@parametros,'$.nombre')),'')
			, @tipo   varchar(50)	  = isnull((select JSON_VALUE(@parametros,'$.tipo')),'')
			, @tipoNombre varchar(50) = isnull((select JSON_VALUE(@parametros,'$.tipoNombre')),'')
			, @report varchar(100)	  = isnull((select JSON_VALUE(@parametros,'$.report')),'')

	if @modo='actualizar' BEGIN
		if not exists (select * from [dbo].[ConfigIdiomaReport] where idioma=@idioma and tipo=@tipo and report=@report)
		insert into [dbo].[ConfigIdiomaReport] (idioma,nombre,tipo,report) 
		values (@idioma, @nombre, @tipo+' - '+@tipoNombre, @report)

		-- actualizar Idiomas_Idiomas
		if not exists (select * from [dbo].Idiomas_Idiomas where Codigo=@idioma)
		insert into [dbo].Idiomas_Idiomas (Codigo,Nombre) values (@idioma,@nombre)

		-- actualizar Idiomas_Tipos
		if not exists (select * from [dbo].Idiomas_Tipos where Codigo=@tipo)
		insert into [dbo].Idiomas_Tipos (Codigo,Nombre) values (@tipo,@tipoNombre)
	END

	if @modo='eliminar' BEGIN
		delete [ConfigIdiomaReport] where idioma=@idioma and tipo=@tipo and report=@report
	END

	select isnull((select * from ConfigIdiomaReport for JSON AUTO, INCLUDE_NULL_VALUES),'[]') as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH