﻿CREATE PROCEDURE [dbo].[pBuscarPedido] @parametros varchar(max)	
AS
SET NOCOUNT ON;
-----------------------------------------------------------------------------------------------
-- Variables de Configuración 
-----------------------------------------------------------------------------------------------
declare @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
select @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
-----------------------------------------------------------------------------------------------
-- Parámetros JSON 
declare @pedidoActivo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.pedidoActivo')),'')
	,	@pedido varchar(50) = isnull((select JSON_VALUE(@parametros,'$.pedido')),'')
	,	@serie varchar(10) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
	,	@ruta varchar(10) = isnull((select JSON_VALUE(@parametros,'$.ruta')),'')			
	,	@nombreRuta varchar(100) = isnull((select JSON_VALUE(@parametros,'$.nombreRuta')),'')			
	,	@zona varchar(10) = isnull((select JSON_VALUE(@parametros,'$.zona')),'')			
declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
	,	@rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
declare @laClave varchar(50) = (select CONCAT(Ejercicio,EMPRESA) from Configuracion) + @pedido + @serie
declare @nu varchar(50) = 
replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 
-----------------------------------------------------------------------------------------------
-- Variables
declare @laConsulta varchar(max)=''
	,	@elCodigo varchar(50) = ''
	,	@elNombre varchar(100) = ''
	,	@laDireccion varchar(255) = ''
	,	@laRuta varchar(10) = ''
	,	@elVendedor varchar(50) = ''
	,	@elID varchar(50) = ''
	,	@cliObs varchar(1000) = ''
	,	@pedidoObs varchar(1000) = ''
	,	@cnt int
-----------------------------------------------------------------------------------------------

BEGIN TRY
	while LEN(@pedido)<10 set @pedido = ' ' + @pedido

	-- quitamos en_uso del usuario
	EXEC('delete ['+@COMUN+'].[DBO].[en_uso] where USUARIO='''+@usuario+'''')

	-- detectar si está en uso por otro usuario
	declare @tbRes TABLE (res int)
	delete @tbRes insert  @tbRes EXEC('select count(USUARIO) from ['+@COMUN+'].[DBO].[en_uso] where CLAVE='''+@pedidoActivo+''' and USUARIO!='''+@usuario+'''')
	if (select res from @tbRes)>0 BEGIN
		declare @tbUsuario TABLE (USUARIO varchar(50))
		delete @tbUsuario insert  @tbUsuario EXEC('select top 1 USUARIO from ['+@COMUN+'].[DBO].[en_uso] where CLAVE='''+@pedidoActivo+''' and USUARIO!='''+@usuario+'''')
		select 'enUSO!'+(select USUARIO from @tbUsuario) as JAVASCRIPT 
		return -1 
	END

	-- ponemos EN_USO EN EUROWIN 
	EXEC('
			begin try
			insert into ['+@COMUN+'].[DBO].[en_uso] (TIPO,CLAVE,TS,USUARIO,TERMINAL,VISTA,GUID_ID) 
			values (''PEDIVEN'','''+@laClave+''','''+@nu+''','''+@usuario+''',''WEB'',1,'''+@nu+''')
			end try begin catch end catch
	')

	declare @buscarSerie varchar(1000) = ' and upper(letra)=upper('''+@serie+''') '
	if @serie='x' or @serie='' set @buscarSerie=''
	declare @buscaRuta varchar(1000) = ' and upper(ruta)=upper('''+@ruta+''') '
	if @ruta='x' or @ruta='' set @buscaRuta=''
	declare @buscaZona varchar(1000) = ''
	if @zona<>'' set @buscaZona=' and upper(zona)=upper('''+@zona+''') '

	declare @elOrdeBy varchar(1000) = ' ORDER BY servidas ASC, linia ASC '
	if @serie='ET'  set @elOrdeBy = ' ORDER BY uds_traspasadas ASC, zona ASC, Ubicacion ASC, linia ASC '

	set @laConsulta = 'select isnull(
						(
							select a.* from 
							(
								select '''+@laClave+''' as PedidoActivo, p.* , ser.*
								from Detalle_Pedidos p
								left join [ConfigSeries] ser on objeto=''ResLin'' and serie='''+@serie+''' 
								where NUMERO='''+@pedido+''' '+@buscarSerie+' '+@buscaRuta+' '+@buscaZona+'						
							) a 
							'+@elOrdeBy+'
							FOR JSON AUTO, INCLUDE_NULL_VALUES
						)
					,''[]'') AS JAVASCRIPT'	

	exec (@laConsulta)
		
	return -1 
END TRY
BEGIN CATCH
	DECLARE @Message varchar(MAX) = ERROR_MESSAGE(), @Severity int = ERROR_SEVERITY(), @State smallint = ERROR_STATE() 
	RAISERROR (@Message, @Severity, @State)
	RETURN 0
END CATCH
