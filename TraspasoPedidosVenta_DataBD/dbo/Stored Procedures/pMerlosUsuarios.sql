﻿CREATE PROCEDURE [dbo].[pMerlosUsuarios] @parametros varchar(max)
AS
SET NOCOUNT ON
BEGIN TRY	
	declare @modo			varchar(50)  = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
		,	@error			varchar(max) = ''
		,	@datos			varchar(max) = ''
		,	@usuario		varchar(50)  = isnull((select JSON_VALUE(@parametros,'$.usuario')),'')
		,	@ImpDocu		varchar(100) = isnull((select JSON_VALUE(@parametros,'$.ImpDocu')),'')
		,	@ImpEti			varchar(100) = isnull((select JSON_VALUE(@parametros,'$.ImpEti')),'')
		,	@ReportAlb		varchar(100) = isnull((select JSON_VALUE(@parametros,'$.ReportAlb')),'')
		,	@ReportFra		varchar(100) = isnull((select JSON_VALUE(@parametros,'$.ReportFra')),'')
		,	@ReportEti		varchar(100) = isnull((select JSON_VALUE(@parametros,'$.ReportEti')),'')
		,	@BasculaPredet	varchar(100) = isnull((select JSON_VALUE(@parametros,'$.BasculaPredet')),'')


	if @modo='dameConfiguracion' BEGIN
		select isnull(	
			( 
				select * from (
					select  b.usuario, b.bascula, b.predet
						,	c.impDocu, c.impEti
						,	d.reportAlbaran, d.reportEtiquetas, d.reportFactura
					from usuarios_basculas b
					left join usuarios_impresoras c on c.usuario=b.usuario
					left join usuarios_reports d	on d.usuario=b.usuario
					where b.usuario=@usuario
				)a
				for JSON AUTO,INCLUDE_NULL_VALUES 
			)
		,'[]') as JAVASCRIPT
	END


	if @modo='guardarDatos' BEGIN
		-- básculas
		if not exists (select * from usuarios_basculas where usuario=@usuario)
			insert into usuarios_basculas (usuario,bascula,predet) values (@usuario,@BasculaPredet,1)
		else update usuarios_basculas set bascula=@BasculaPredet where usuario=@usuario
		-- impresoras
		if not exists (select * from usuarios_impresoras where usuario=@usuario)
			insert into usuarios_impresoras (usuario,impDocu,impEti) values (@usuario,@ImpDocu,@ImpEti)
		else update usuarios_impresoras set impDocu=@ImpDocu, impEti=@ImpEti where usuario=@usuario
		-- reports
		if not exists (select * from usuarios_reports where usuario=@usuario)
			insert into usuarios_reports (usuario, reportAlbaran, reportEtiquetas, reportFactura) 
			values (@usuario, @ReportAlb, @ReportEti, @ReportFra)
		else update usuarios_reports 
			 set usuario=@usuario, reportAlbaran=@ReportAlb, reportEtiquetas=@ReportEti, reportFactura=@ReportFra 
			 where usuario=@usuario

		select 'OK' as JAVASCRIPT
	END


	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
	select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH