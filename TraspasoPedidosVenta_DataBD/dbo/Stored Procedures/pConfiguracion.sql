﻿CREATE PROCEDURE [dbo].[pConfiguracion] @parametros varchar(max)='[]', @respuestaSP varchar(max)='' output
AS
BEGIN TRY	
	declare @modo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
		,	@comun varchar(50) = isnull((select JSON_VALUE(@parametros,'$.comun')),'')
		,	@empresa varchar(50) = isnull((select JSON_VALUE(@parametros,'$.empresa')),'')
		,	@nombreEmpresa varchar(50) = isnull((select JSON_VALUE(@parametros,'$.nombreEmpresa')),'')
		,	@objeto varchar(50) = isnull((select JSON_VALUE(@parametros,'$.objeto')),'')
		,	@valor varchar(50) = isnull((select JSON_VALUE(@parametros,'$.valor')),'')
		,	@confOpIO varchar(50) = isnull((select JSON_VALUE(@parametros,'$.confOpIO')),'')
		,	@cliente varchar(50) = isnull((select JSON_VALUE(@parametros,'$.cliente')),'')

	declare   @ejercicio varchar(4)
			, @LETRA varchar(2)
			, @GESTION varchar(6)
			, @CAMPOS varchar(8)
			, @LOTES varchar(8)
			, @SentenciaSQL varchar(max)

	declare @sp int


	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	if @modo='ComprobarEjercicio' and @GESTION<>'' BEGIN
		declare @laRuta char(6)
		declare @tbRuta table (RUTA char(6))
		insert  @tbRuta exec('select RUTA from ['+@COMUN+'].dbo.ejercici where PREDET=1')
		select  @laRuta=RUTA from @tbRuta 
		delete  @tbRuta
		if @GESTION<>@laRuta BEGIN
			update Configuracion set Ejercicio=LEFT(@laRuta,4), Gestion=@laRuta
			EXEC [dbo].[pAppVistas]
			EXEC [dbo].[pAppFunciones]
		END
		select @laRuta as JAVASCRIPT
		return -1
	END

	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
		if @modo='cargarConfiguracion' BEGIN			
			declare @configuracion varchar(max) = isnull( (select * from ConfigApp for JSON AUTO, INCLUDE_NULL_VALUES) ,'[]')
			declare @basculas varchar(max) = isnull( (select * from basculas for JSON AUTO, INCLUDE_NULL_VALUES) ,'[]')

			select CONCAT('{"configuracion":',@configuracion,',"basculas":',@basculas,'}') as JAVASCRIPT
			return -1
		END

	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
		if @modo='comprobarConfiguracionEmpresa' BEGIN
			if (select isnull(Empresa,'') from Configuracion)<>'' 
				select (select '' as msj, * from Configuracion for JSON AUTO, INCLUDE_NULL_VALUES) as JAVASCRIPT 
			else select '[{"msj":"NoExisteConfiguracion"}]' as JAVASCRIPT
			return -1
		END


	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
		if @modo='configurarEmpresa' BEGIN  
			-- obtener BD CAMPOSxx
			BEGIN TRY
				IF OBJECT_ID('tempdb..##tmpNomConex') IS NOT NULL BEGIN drop table ##tmpNomConex END
				set @SentenciaSQL = 'select NOM_CONEX into ##tmpNomConex from ['+@comun+'] .dbo.modulos where nombre=''CAMPOSAD'''
				EXEC (@SentenciaSQL)
				set @CAMPOS = (select NOM_CONEX from ##tmpNomConex)
				drop table ##tmpNomConex
			END TRY BEGIN CATCH END CATCH
		
			-- obtener BD LOTESxxx
			BEGIN TRY
				IF OBJECT_ID('tempdb..##tmpNomConex') IS NOT NULL BEGIN drop table ##tmpNomConex END
				set @SentenciaSQL = 'select NOM_CONEX into ##tmpNomConex from ['+@comun+'] .dbo.modulos where nombre=''LOTES'''
				EXEC (@SentenciaSQL)
				set @LOTES = (select NOM_CONEX from ##tmpNomConex)
				drop table ##tmpNomConex
			END TRY BEGIN CATCH END CATCH

			BEGIN TRY
				IF OBJECT_ID('tempdb..##pConfig01') IS NOT NULL BEGIN drop table ##tmpNomConex END
				set @SentenciaSQL = 'SELECT ltrim(rtrim(RUTA)) as RUTA, [ANY] INTO ##pConfig01 FROM ['+@comun+'].dbo.ejercici where predet=1'
				EXEC (@SentenciaSQL)
				set @GESTION  = (SELECT RUTA FROM ##pConfig01)
				SET @ejercicio = (SELECT [ANY] FROM ##pConfig01)
				set @LETRA = RIGHT(@GESTION,2)
				DROP TABLE ##pConfig01 
			END TRY BEGIN CATCH END CATCH

			if exists (select * from Configuracion)
			update Configuracion set Ejercicio=@ejercicio, Gestion=@GESTION, Letra=@LETRA, Comun=@comun, Campos=@CAMPOS, Lotes=@LOTES
								   , Empresa=@empresa, NombreEmpresa=@nombreEmpresa
			else insert into Configuracion (Ejercicio, Gestion, Letra, Comun, Campos, Lotes, Empresa, NombreEmpresa)
				 values (@ejercicio, @GESTION, @LETRA, @comun, @CAMPOS, @LOTES, @empresa, @nombreEmpresa)
		END


	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	if @modo='crearVistas' BEGIN 		
		EXEC @sp=pAppVistas 
		if @sp<>-1 	select 'Error! SP: pAppVistas!'  as JAVASCRIPT 	
		return -1
	END

	if @modo='crearProcedimientos' BEGIN 		
		--EXEC @sp=pAppProcedimientos 
		--if @sp<>-1 	select 'Error! SP: pAppProcedimientos!'  as JAVASCRIPT 	
		return -1
	END

	if @modo='crearFunciones' BEGIN 		
		EXEC @sp=pAppFunciones 
		if @sp<>-1 	select 'Error! SP: pAppFunciones!'  as JAVASCRIPT 	
		return -1
	END


	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	if @modo='objetoIO' BEGIN 		
		if exists (select * from ConfigApp where objeto=@objeto) 
			update ConfigApp set valor=@valor, activo=@confOpIO where objeto=@objeto
        else insert into ConfigApp (objeto,valor,activo) values (@objeto, @valor, @confOpIO) 
	END		
	

	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	select 'OK' as JAVASCRIPT        --   select * from Configuracion    --   select * from ConfigApp

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX) = ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH