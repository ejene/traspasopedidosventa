﻿CREATE PROCEDURE [dbo].[pArticulos] (@parametros varchar(max))
AS
SET NOCOUNT ON
BEGIN TRY	
	declare @modo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
		,	@almacen varchar(5) = isnull((select JSON_VALUE(@parametros,'$.almacen')),'')
		,	@error   varchar(max) = ''
		,	@datos   varchar(max) = ''

	if @modo='lista' BEGIN
		set @datos =	( select * from vArticulosStock where almacen=@almacen for JSON AUTO,INCLUDE_NULL_VALUES )
	END

	select (CONCAT('{"error":"',@error,'","datos":',@datos,'}')) as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
	select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
