﻿CREATE PROCEDURE [dbo].[pImprimir_Docs]
@empresa varchar(2) = '', 
@letra varchar(2) = '',
@numero varchar(10)= '',
@terminal varchar(50),
@num_fa varchar (10) ='',
@imprimir INT,
@imp_eti INT,
@ETICAJAS int,
@bultos VARCHAR (7),
@nEti INT,
@respuestaSP VARCHAR(100)='' output

AS

BEGIN TRY
				
	DECLARE @Mensaje varchar(max), @MensajeError varchar(max)

	BEGIN
		if @imprimir=1 or @imp_eti=1
			-- Albert R. 23/07/12 - de momento la serie DI no imrpimirá albaranes
			if @letra='DI'
				SET @imprimir=0
			begin
				if @LETRA='DI'
					BEGIN
						INSERT INTO MI_Impresion (terminal, empresa, numero, letra, factura, imprimir, imp_eti, bultos)  
						VALUES (@terminal, @empresa, @numero, @letra, @num_fa, @imprimir, @imp_eti, @ETICAJAS)
					END
				else 
					BEGIN
						INSERT INTO MI_Impresion (terminal, empresa, numero, letra, factura, imprimir, imp_eti, bultos)  
						VALUES (@terminal, @empresa, @numero, @letra, @num_fa, @imprimir, @imp_eti, @bultos)
					END
			end

	end	

END TRY
				
BEGIN CATCH 
	CLOSE Employee_Cursor
	DEALLOCATE Employee_Cursor 

	DECLARE @ErrorMessage NVARCHAR(4000);  
	DECLARE @ErrorSeverity INT;  
	DECLARE @ErrorState INT;  

	SELECT   
		@ErrorMessage = ERROR_MESSAGE(),  
		@ErrorSeverity = ERROR_SEVERITY(),  
		@ErrorState = ERROR_STATE();  

	RAISERROR (@ErrorMessage, -- Message text.  
				@ErrorSeverity, -- Severity.  
				@ErrorState -- State.  
				);  
	SET @Mensaje='","ERROR":"NO se ha podido crear el albarán'
	--PRINT(@Mensaje)
	SELECT @Mensaje AS JAVASCRIPT

END CATCH
