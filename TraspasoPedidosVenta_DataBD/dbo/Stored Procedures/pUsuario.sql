﻿CREATE PROCEDURE [dbo].[pUsuario] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare	@modo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
			
	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------
	

	if @modo='dameFiltro' BEGIN
		select isnull(
			(select * from Usuarios_Filtros where usuario=@usuario for JSON AUTO, INCLUDE_NULL_VALUES)
		,'[]') as JAVASCRIPT
	END

	if @modo='eliminarFiltros' BEGIN
		delete Usuarios_Filtros where usuario=@usuario
	END

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
