﻿CREATE PROCEDURE [dbo].[pImpresion] @parametros varchar(max)=''
AS
BEGIN TRY
	declare   @modo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
			, @tipo varchar(50)	= isnull((select JSON_VALUE(@parametros,'$.tipo')),'')
			, @numero varchar(50)	= isnull((select JSON_VALUE(@parametros,'$.numero')),'')
			, @empresa varchar(50)	= isnull((select JSON_VALUE(@parametros,'$.empresa')),'')
			, @letra varchar(50)	= isnull((select JSON_VALUE(@parametros,'$.letra')),'')


	if @modo='reimpresion' BEGIN
		update MI_ImpresionPDF set report=0, [log]=NULL
		where id= (
					select top 1 id from MI_ImpresionPDF 
					where concat(tipo,ltrim(rtrim(crNumero)),crEmpresa,crLetra)
						= concat(@tipo,ltrim(rtrim(@numero)),@empresa,@letra)
					order by fechaInsertUpdate desc
				)
	END

	select 'OK' as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() 
					+ char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select 'KO' + char(13) + @CatchError as JAVASCRIPT
	RETURN 0
END CATCH