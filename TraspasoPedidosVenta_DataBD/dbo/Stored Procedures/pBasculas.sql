﻿CREATE PROCEDURE [dbo].[pBasculas] @parametros varchar(max)='[]'
AS
BEGIN TRY	
	declare @modo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
		,	@bascula varchar(50)
		,	@basculaTipoConexion varchar(50)
		,	@IP varchar(50)
		,	@puerto varchar(50)
		,	@sentencia varchar(50)
		,	@activa int


	--	BÁSCULA 01 =============================================================================================================
		set @bascula				= isnull((select JSON_VALUE(@parametros,'$.bascula1[0].bascula')),'')
		set @basculaTipoConexion	= isnull((select JSON_VALUE(@parametros,'$.bascula1[0].basculaTipoConexion')),'')
		set @IP						= isnull((select JSON_VALUE(@parametros,'$.bascula1[0].IP')),'')
		set @puerto					= isnull((select JSON_VALUE(@parametros,'$.bascula1[0].puerto')),'')
		set @sentencia				= isnull((select JSON_VALUE(@parametros,'$.bascula1[0].sentencia')),'')
		set @activa					= cast( isnull((select JSON_VALUE(@parametros,'$.bascula1[0].activa')),'') as int)

		if exists (select * from basculas where bascula=@bascula)
			update basculas set tipoConexion=@basculaTipoConexion, [ip]=@IP, puerto=@puerto, sentencia=@sentencia, activa=@activa
			where bascula=@bascula
		else
			insert into basculas (bascula, tipoConexion, [ip], puerto, sentencia, activa)
			values (@bascula, @basculaTipoConexion, @IP, @puerto, @sentencia, @activa)

	
	--	BÁSCULA 02 =============================================================================================================
		set @bascula				= isnull((select JSON_VALUE(@parametros,'$.bascula2[0].bascula')),'')
		set @basculaTipoConexion	= isnull((select JSON_VALUE(@parametros,'$.bascula2[0].basculaTipoConexion')),'')
		set @IP						= isnull((select JSON_VALUE(@parametros,'$.bascula2[0].IP')),'')
		set @puerto					= isnull((select JSON_VALUE(@parametros,'$.bascula2[0].puerto')),'')
		set @sentencia				= isnull((select JSON_VALUE(@parametros,'$.bascula2[0].sentencia')),'')
		set @activa					= cast( isnull((select JSON_VALUE(@parametros,'$.bascula2[0].activa')),'') as int)

		if exists (select * from basculas where bascula=@bascula)
			update basculas set tipoConexion=@basculaTipoConexion, [ip]=@IP, puerto=@puerto, sentencia=@sentencia, activa=@activa
			where bascula=@bascula
		else
			insert into basculas (bascula, tipoConexion, [ip], puerto, sentencia, activa)
			values (@bascula, @basculaTipoConexion, @IP, @puerto, @sentencia, @activa)


	--	BÁSCULA 03 =============================================================================================================
		set @bascula				= isnull((select JSON_VALUE(@parametros,'$.bascula3[0].bascula')),'')
		set @basculaTipoConexion	= isnull((select JSON_VALUE(@parametros,'$.bascula3[0].basculaTipoConexion')),'')
		set @IP						= isnull((select JSON_VALUE(@parametros,'$.bascula3[0].IP')),'')
		set @puerto					= isnull((select JSON_VALUE(@parametros,'$.bascula3[0].puerto')),'')
		set @sentencia				= isnull((select JSON_VALUE(@parametros,'$.bascula3[0].sentencia')),'')
		set @activa					= cast( isnull((select JSON_VALUE(@parametros,'$.bascula3[0].activa')),'') as int)

		if exists (select * from basculas where bascula=@bascula)
			update basculas set tipoConexion=@basculaTipoConexion, [ip]=@IP, puerto=@puerto, sentencia=@sentencia, activa=@activa
			where bascula=@bascula
		else
			insert into basculas (bascula, tipoConexion, [ip], puerto, sentencia, activa)
			values (@bascula, @basculaTipoConexion, @IP, @puerto, @sentencia, @activa)


	--	BÁSCULA 04 =============================================================================================================
		set @bascula				= isnull((select JSON_VALUE(@parametros,'$.bascula4[0].bascula')),'')
		set @basculaTipoConexion	= isnull((select JSON_VALUE(@parametros,'$.bascula4[0].basculaTipoConexion')),'')
		set @IP						= isnull((select JSON_VALUE(@parametros,'$.bascula4[0].IP')),'')
		set @puerto					= isnull((select JSON_VALUE(@parametros,'$.bascula4[0].puerto')),'')
		set @sentencia				= isnull((select JSON_VALUE(@parametros,'$.bascula4[0].sentencia')),'')
		set @activa					= cast( isnull((select JSON_VALUE(@parametros,'$.bascula4[0].activa')),'') as int)

		if exists (select * from basculas where bascula=@bascula)
			update basculas set tipoConexion=@basculaTipoConexion, [ip]=@IP, puerto=@puerto, sentencia=@sentencia, activa=@activa
			where bascula=@bascula
		else
			insert into basculas (bascula, tipoConexion, [ip], puerto, sentencia, activa)
			values (@bascula, @basculaTipoConexion, @IP, @puerto, @sentencia, @activa)
		

	--	========================================================================================================================
		select 'OK' as JAVASCRIPT       --	select * from basculas

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select 'Error! - '+@CatchError as JAVASCRIPT
	RETURN 0
END CATCH