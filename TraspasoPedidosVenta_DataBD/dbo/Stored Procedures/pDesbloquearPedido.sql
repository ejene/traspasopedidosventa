﻿CREATE PROCEDURE [dbo].[pDesbloquearPedido] @parametros varchar(max)	
AS
BEGIN
	SET NOCOUNT ON;
	-----------------------------------------------------------------------------------------------
	-- Variables de Configuración 
	-----------------------------------------------------------------------------------------------
	declare @EJERCICIO char(4)
	declare @GESTION char(6)
	declare @COMUN char(8)
	declare @LOTES char(8)
	select @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-----------------------------------------------------------------------------------------------

	BEGIN TRY

		declare @numero varchar(50) = isnull((select JSON_VALUE(@parametros,'$.numero')),'')
			,	@serie varchar(50) = isnull((select JSON_VALUE(@parametros,'$.letra')),'')
			,	@ruta varchar(50) = isnull((select JSON_VALUE(@parametros,'$.ruta')),'')
		declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			,	@rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')

		declare @laClave varchar(50) = concat(
												  (select Ejercicio from Configuracion)
												, (select Empresa from Configuracion)
												, @numero
												, @serie
										) 

		EXEC('delete ['+@COMUN+'].[DBO].[en_uso] where CLAVE='''+@laClave+'''')

		return -1 
	END TRY
	BEGIN CATCH
		DECLARE @CatchError NVARCHAR(MAX)
		SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
		select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
		RAISERROR(@CatchError,12,1)
		RETURN 0
	END CATCH
END