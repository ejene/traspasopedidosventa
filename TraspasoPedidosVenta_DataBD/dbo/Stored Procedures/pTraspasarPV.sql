﻿CREATE PROCEDURE [dbo].[pTraspasarPV] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare	@numero varchar(50) = isnull((select JSON_VALUE(@parametros,'$.numero')),'')
		,	@letra varchar(2) = isnull((select JSON_VALUE(@parametros,'$.letra')),'')
			
	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EJERCICIO char(4)
	declare @GESTION char(6)
	declare @COMUN char(8)
	declare @LOTES char(8)
	select @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------
	declare @sql varchar(max)
	declare @empresa char(2) = (select Empresa from Configuracion)
	declare @nu varchar(50) = 
		replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 

	--	verificar EnUso 
		if exists (SELECT EnUso from EnUso) BEGIN select '{"msj":"enUso"}' as JAVASCRIPT return -1 END

	--	verificar lotes
		if exists (SELECT ARTICULO, NOMBRE from vErrorLotesTrasTemp where empresa=@empresa and numero=@numero and letra=@letra) BEGIN
			select (SELECT 'verifLotes' as msj, ARTICULO, NOMBRE from vErrorLotesTrasTemp where empresa=@empresa and numero=@numero and letra=@letra 
			for JSON AUTO, INCLUDE_NULL_VALUES) as JAVASCRIPT
			return -1
		END
	


	--	Traspaso del Pedido #########################################################################################################################
		-- comprobar que existan lineas para traspasar
			if not exists (select * from [Traspaso_Temporal] where empresa=@empresa and numero=@numero and letra=@letra) 
			begin select '{"msj":"linTraspKO"}' as JAVASCRIPT  return -1 end

			declare   @todoTraspasado bit = 0
					, @impAlb bit = 0
					, @pedidoLineas int = isnull((select count(numero) from [Detalle_Pedidos]   
											where empresa+numero+letra=@empresa+@numero+@letra
												  and articulo is not null and ltrim(rtrim(articulo))<>'' )
										,0)
					, @linTrasp int     = isnull((select count(numero) from [Traspaso_Temporal] 
											where empresa+numero+letra=@empresa+@numero+@letra
												  and articulo is not null and ltrim(rtrim(articulo))<>'' )
										,0)
					, @etiEtiquetas varchar(50) = 'Nº Etiquetas'

			if @linTrasp>0 and @pedidoLineas>0 and @linTrasp=@pedidoLineas BEGIN set @todoTraspasado=1 set @impAlb=1 END
			if exists (SELECT * FROM [ConfigSeries] WHERE objeto='ImpAlbGen' and serie=@letra) set @impAlb=1
			if exists (SELECT * FROM [ConfigSeries] WHERE objeto='FactCajas' and serie=@letra) set @etiEtiquetas='Cajas'

			declare   @cliente varchar(20) = (select top 1 CLIENTE from Detalle_Pedidos where NUMERO=@numero and LETRA=@letra)
					, @pesoTotal numeric(15,6) = (select SUM(isnull(case when peso='' then 0.000 else cast(peso as numeric(15,6)) end,0.000)) 
												 from [Traspaso_Temporal] where NUMERO=@numero and LETRA=@letra)

			-- tabla temporal para recopilación de datos ------------------------------------------------------
			declare @tablaTemp TABLE (codigo varchar(20), NOMBRE varchar(100), direccion varchar(255), poblacion varchar(100), codpost varchar(10) 
									, provincia varchar(100), AGENCIA_NOMBRE varchar(100), TELEFONO varchar(20))
			SET @sql = 'select A.CODIGO as codigo,
					CASE WHEN A.contado = 0 
					THEN
						case when coalesce(c.NOMBRE,'''')='''' then A.NOMBRE else c.NOMBRE end 
					ELSE
						CON.NOMBRE
					END	as NOMBRE
					, CASE WHEN A.CONTADO = 0 
					THEN
						case when coalesce(c.direccion,'''')='''' then A.direccion else c.direccion end 
					ELSE
						CON.DIRECCION
					END	as direccion
					, CASE WHEN A.CONTADO = 0 
					THEN
						case when coalesce(c.poblacion,'''')='''' then A.poblacion else c.poblacion end
					ELSE
						CON.POBLACION 
					END as poblacion
					, CASE WHEN A.CONTADO = 0 
					THEN
					case when coalesce(c.codpos,'''')='''' then A.codpost else c.codpos end
					ELSE
						CON.CODPOST 
					END as codpost
					, CASE WHEN A.CONTADO = 0 
					THEN
						case when coalesce(c.provincia,'''')='''' then A.provincia else c.provincia end
					ELSE
						CON.PROVINCIA 
					END as provincia
					, ltrim(rtrim(coalesce(B.NOMBRE,''''))) AS AGENCIA_NOMBRE
					, CASE WHEN A.CONTADO = 0 
					THEN
						case when coalesce(c.TELEFONO,'''')='''' then coalesce(TEL.TELEFONO,'''') else c.TELEFONO end 
					ELSE
						CON.TELEFONO
					END	as TELEFONO 
				from ['+@GESTION+'].dbo.c_pedive cpv 	
				INNER JOIN ['+@GESTION+'].[dbo].clientes A ON A.CODIGO=CPV.CLIENTE
				LEFT JOIN ['+@GESTION+'].[dbo].agencia B ON B.CODIGO=A.AGENCIA 
				LEFT JOIN ['+@GESTION+'].[dbo].env_cli C ON C.LINEA=cpv.ENV_CLI and C.CLIENTE=cpv.cliente
				LEFT JOIN ['+@GESTION+'].[dbo].TELF_CLI tel ON tel.cliente=cpv.cliente and tel.orden=1		
				LEFT JOIN ['+@GESTION+'].DBO.CONTADO CON ON CON.EMPRESA=CPV.EMPRESA AND CON.NUMERO=CPV.NUMERO AND CON.LETRA=CPV.LETRA AND CON.TIPO=2				
				where cpv.traspasado=0 and cpv.cancelado=0 and cpv.empresa='''+@empresa+''' and cpv.NUMERO='''+@numero+''' and cpv.lETRA='''+@letra+''''
			INSERT @tablaTemp EXEC(@sql)	
			declare @datosCliente varchar(max) = (SELECT * from @tablaTemp for JSON AUTO, INCLUDE_NULL_VALUES)
			delete @tablaTemp

			select CONCAT('[{"impAlb":"',@impAlb,'","etiEtiquetas":"',@etiEtiquetas,'","pesoTotal":"',@pesoTotal,'","datosCliente":',@datosCliente,'}]') as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
