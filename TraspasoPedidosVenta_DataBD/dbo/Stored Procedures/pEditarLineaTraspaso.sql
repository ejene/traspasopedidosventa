﻿CREATE PROCEDURE [dbo].[pEditarLineaTraspaso] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON

	-- Parámetros JSON ------------------------------------------------------------------------------
	declare	  @pedido varchar(50) = isnull((select JSON_VALUE(@parametros,'$.pedido')),'')
			, @serie varchar(10) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
			, @articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
			, @unidades varchar(20) = isnull((select JSON_VALUE(@parametros,'$.unidades')),'')
			, @linea varchar(10) = isnull((select JSON_VALUE(@parametros,'$.linea')),'0')
			, @cajas varchar(10) = isnull((select JSON_VALUE(@parametros,'$.cajas')),'0')
			, @peso varchar(10) = isnull((select JSON_VALUE(@parametros,'$.peso')),'0')
			, @servidas varchar(10) = isnull((select JSON_VALUE(@parametros,'$.servidas')),'0')
			, @traspaso varchar(10) = isnull((select JSON_VALUE(@parametros,'$.traspaso')),'0')
			, @pesoPend varchar(10) = isnull((select JSON_VALUE(@parametros,'$.pesoPend')),'0')
			, @basculaEnUso varchar(10) = isnull((select JSON_VALUE(@parametros,'$.basculaEnUso')),'0')
	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-------------------------------------------------------------------------------------------------
	-- variables App
	declare @laSesion varchar(50) = @usuario
	declare @nu varchar(50) = 
	replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 
	declare @empresa varchar(2) = (select Empresa from Configuracion)
	---------------------------------------------------------------------------------------------------------------
	
	declare @respuesta varchar(max) = ''

	-- Si existen registros en la tabla Traspaso_Temporal los devolvemos y salimos del procedimiento
	declare @existeTraspTemp varchar(max) = 
	(select 1 as existeTraspTemp, * from Traspaso_Temporal where concat(empresa,numero,letra,articulo)=concat(@empresa,@pedido,@serie,@articulo)
	FOR JSON AUTO, INCLUDE_NULL_VALUES)
	if LEN(@existeTraspTemp)>0 BEGIN select @existeTraspTemp as JAVASCRIPT return -1 END


	-- báscula predeterminada del usuario
	declare @basculaUsuario varchar(50) = (select bascula from usuarios_basculas where usuario=@usuario and predet=1)

	-- Obtener básculas
	declare @bsc1img varchar(50) = 'btnBascula01_Off.png'
		,	@bsc2img varchar(50) = 'btnBascula02_Off.png'
		,	@bsc3img varchar(50) = 'btnBascula03_Off.png'
		,	@bsc4img varchar(50) = 'btnBascula04_Off.png'
		,	@bascCont int = 0;

	declare @activa bit
	DECLARE elCursor CURSOR FOR	select activa from basculas
	OPEN elCursor
	FETCH NEXT FROM elCursor INTO @activa
	WHILE (@@FETCH_STATUS=0) BEGIN
		set @bascCont = @bascCont + 1

		if @bascCont=1 and @activa=1 BEGIN 
			if @basculaEnUso=1 set @bsc1img='btnBascula01_Selected.png' else set @bsc1img='btnBascula01_Activa.png'
		END
		if @bascCont=2 and @activa=1 BEGIN 
			if @basculaEnUso=2 set @bsc2img='btnBascula02_Selected.png' else set @bsc2img='btnBascula02_Activa.png'
		END
		if @bascCont=3 and @activa=1 BEGIN 
			if @basculaEnUso=3 set @bsc3img='btnBascula03_Selected.png' else set @bsc3img='btnBascula03_Activa.png'
		END
		if @bascCont=4 and @activa=1 BEGIN 
			if @basculaEnUso=4 set @bsc4img='btnBascula04_Selected.png' else set @bsc4img='btnBascula04_Activa.png'
		END
		
		FETCH NEXT FROM elCursor INTO @activa
	END	CLOSE elCursor deallocate elCursor

	-- tipo de conexión de báscula
	declare @verSpnTipoBascula varchar(50) = (select tipoConexion from basculas where bascula=@basculaEnUso)


	-- Trabajo CON lotes
	set @respuesta = (
		select isnull(
			(
				select	* 
					,	0 as existeTraspTemp
					,	(select NOMBRE from varticulos where CODIGO=@articulo) as descripcion
					,	isnull(@verSpnTipoBascula,'') as verSpnTipoBascula
					,	isnull(@bsc1img,'') as bsc1img
					,	isnull(@bsc2img,'') as bsc2img
					,	isnull(@bsc3img,'') as bsc3img
					,	isnull(@bsc4img,'') as bsc4img
				from vBuscarLotes
				where NUMERO=@pedido and ARTICULO=@articulo and LINIA=@linea and Llote is not null
				ORDER BY Lcaducidad asc 
				for JSON AUTO, INCLUDE_NULL_VALUES
			)
		,'[]')
	)

	if LEN(@respuesta)>3 BEGIN select @respuesta as JAVASCRIPT return -1 END


	-- Trabajo SIN lotes
	set @respuesta = (
		select isnull(
			(
				select	* 
					,	2 as existeTraspTemp 
					,	isnull(@verSpnTipoBascula,'') as verSpnTipoBascula
					,	isnull(@bsc1img,'') as bsc1img
					,	isnull(@bsc2img,'') as bsc2img
					,	isnull(@bsc3img,'') as bsc3img
					,	isnull(@bsc4img,'') as bsc4img
				from vCajasOunidades 
				where NUMERO=@pedido and LETRA=@serie and ARTICULO=@articulo and LINIA=@linea 
				for JSON AUTO, INCLUDE_NULL_VALUES
			)
		,'[]')
	)
	select @respuesta as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
	RETURN 0
END CATCH