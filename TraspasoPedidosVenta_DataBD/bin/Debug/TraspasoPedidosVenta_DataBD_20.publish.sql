﻿/*
Script de implementación para TraspasoPedidosVenta_DataBD

Una herramienta generó este código.
Los cambios realizados en este archivo podrían generar un comportamiento incorrecto y se perderán si
se vuelve a generar el código.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "TraspasoPedidosVenta_DataBD"
:setvar DefaultFilePrefix "TraspasoPedidosVenta_DataBD"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEW\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEW\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detectar el modo SQLCMD y deshabilitar la ejecución del script si no se admite el modo SQLCMD.
Para volver a habilitar el script después de habilitar el modo SQLCMD, ejecute lo siguiente:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'El modo SQLCMD debe estar habilitado para ejecutar correctamente este script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
/*
Se está quitando la columna [dbo].[ConfigPersonalizaciones].[Activo]; puede que se pierdan datos.

Se está quitando la columna [dbo].[ConfigPersonalizaciones].[Empresa]; puede que se pierdan datos.

Debe agregarse la columna [dbo].[ConfigPersonalizaciones].[Contenido] de la tabla [dbo].[ConfigPersonalizaciones], pero esta columna no tiene un valor predeterminado y no admite valores NULL. Si la tabla contiene datos, el script ALTER no funcionará. Para evitar esta incidencia, agregue un valor predeterminado a la columna, márquela de modo que permita valores NULL o habilite la generación de valores predeterminados inteligentes como opción de implementación.

Debe agregarse la columna [dbo].[ConfigPersonalizaciones].[IdEmpresa] de la tabla [dbo].[ConfigPersonalizaciones], pero esta columna no tiene un valor predeterminado y no admite valores NULL. Si la tabla contiene datos, el script ALTER no funcionará. Para evitar esta incidencia, agregue un valor predeterminado a la columna, márquela de modo que permita valores NULL o habilite la generación de valores predeterminados inteligentes como opción de implementación.

Debe agregarse la columna [dbo].[ConfigPersonalizaciones].[IdTipo] de la tabla [dbo].[ConfigPersonalizaciones], pero esta columna no tiene un valor predeterminado y no admite valores NULL. Si la tabla contiene datos, el script ALTER no funcionará. Para evitar esta incidencia, agregue un valor predeterminado a la columna, márquela de modo que permita valores NULL o habilite la generación de valores predeterminados inteligentes como opción de implementación.

Debe agregarse la columna [dbo].[ConfigPersonalizaciones].[Nombre] de la tabla [dbo].[ConfigPersonalizaciones], pero esta columna no tiene un valor predeterminado y no admite valores NULL. Si la tabla contiene datos, el script ALTER no funcionará. Para evitar esta incidencia, agregue un valor predeterminado a la columna, márquela de modo que permita valores NULL o habilite la generación de valores predeterminados inteligentes como opción de implementación.

La columna FechaInsertUpdate de la tabla [dbo].[ConfigPersonalizaciones] debe cambiarse de NULL a NOT NULL. Si la tabla contiene datos, puede que no funcione el script ALTER. Para evitar esta incidencia, debe agregar valores en todas las filas de esta columna, marcar la columna de modo que permita valores NULL o habilitar la generación de valores predeterminados inteligentes como opción de implementación.
*/

IF EXISTS (select top 1 1 from [dbo].[ConfigPersonalizaciones])
    RAISERROR (N'Se detectaron filas. La actualización del esquema va a terminar debido a una posible pérdida de datos.', 16, 127) WITH NOWAIT

GO
PRINT N'Quitando Restricción DEFAULT [dbo].[DF_ConfigPersonalizaciones_FechaInsertUpdate]...';


GO
ALTER TABLE [dbo].[ConfigPersonalizaciones] DROP CONSTRAINT [DF_ConfigPersonalizaciones_FechaInsertUpdate];


GO
PRINT N'Iniciando recompilación de la tabla [dbo].[ConfigPersonalizaciones]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_ConfigPersonalizaciones] (
    [Id]                INT           IDENTITY (1, 1) NOT NULL,
    [IdEmpresa]         INT           NOT NULL,
    [IdTipo]            INT           NOT NULL,
    [Nombre]            VARCHAR (100) NOT NULL,
    [Contenido]         VARCHAR (MAX) NOT NULL,
    [FechaInsertUpdate] DATETIME      DEFAULT (getdate()) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[ConfigPersonalizaciones])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_ConfigPersonalizaciones] ON;
        INSERT INTO [dbo].[tmp_ms_xx_ConfigPersonalizaciones] ([Id], [FechaInsertUpdate])
        SELECT [Id],
               [FechaInsertUpdate]
        FROM   [dbo].[ConfigPersonalizaciones];
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_ConfigPersonalizaciones] OFF;
    END

DROP TABLE [dbo].[ConfigPersonalizaciones];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_ConfigPersonalizaciones]', N'ConfigPersonalizaciones';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Creando Tabla [dbo].[ConfigPersEmpresas]...';


GO
CREATE TABLE [dbo].[ConfigPersEmpresas] (
    [Id]                INT           IDENTITY (1, 1) NOT NULL,
    [Empresa]           VARCHAR (100) NULL,
    [Activo]            BIT           NOT NULL,
    [FechaInsertUpdate] DATETIME      NOT NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[ConfigPersTipo]...';


GO
CREATE TABLE [dbo].[ConfigPersTipo] (
    [Id]                INT           IDENTITY (1, 1) NOT NULL,
    [Nombre]            VARCHAR (100) NOT NULL,
    [FechaInsertUpdate] DATETIME      NOT NULL
) ON [PRIMARY];


GO
PRINT N'Creando Restricción DEFAULT restricción sin nombre en [dbo].[ConfigPersEmpresas]...';


GO
ALTER TABLE [dbo].[ConfigPersEmpresas]
    ADD DEFAULT ((0)) FOR [Activo];


GO
PRINT N'Creando Restricción DEFAULT restricción sin nombre en [dbo].[ConfigPersEmpresas]...';


GO
ALTER TABLE [dbo].[ConfigPersEmpresas]
    ADD DEFAULT (getdate()) FOR [FechaInsertUpdate];


GO
PRINT N'Creando Restricción DEFAULT restricción sin nombre en [dbo].[ConfigPersTipo]...';


GO
ALTER TABLE [dbo].[ConfigPersTipo]
    ADD DEFAULT (getdate()) FOR [FechaInsertUpdate];


GO
PRINT N'Modificando Procedimiento [dbo].[pAppVistas]...';


GO
ALTER PROCEDURE [dbo].[pAppVistas]
AS
BEGIN TRY	
	SET NOCOUNT ON
	
	
	
	declare @EJERCICIO char(4)	= (select Ejercicio from Configuracion)
	declare @GESTION char(6)	= (select Gestion from Configuracion)
	declare @COMUN char(8)		= (select Comun from Configuracion)
	declare @LOTES char(8)		= (select Lotes from Configuracion)
	declare @alterCreate varchar(max)




IF EXISTS (SELECT * FROM sys.objects where name='vSeries') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vSeries] as
select codigo, nombre from ['+@COMUN+'].dbo.Letras
')
select 'vSeries'




IF EXISTS (SELECT * FROM sys.objects where name='Articulos_Sustitucion') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[Articulos_Sustitucion] AS
select ART1.CODIGO AS ARTICULO, ART1.NOMBRE AS NOMBRE
, case when coalesce(lot.lote,0)=1 then coalesce(stl.stock_uds_lot,0.00) else coalesce(stc.stock_uds,0.00) end  as stock
, art2.codigo as art_orig
from ['+@GESTION+'].dbo.articulo art1 
INNER JOIN ['+@GESTION+'].DBO.ARTICULO ART2 ON ART2.SUBFAMILIA=ART1.SUBFAMILIA
LEFT JOIN ['+@LOTES+'] .DBO.artlot LOT ON LOT.ARTICULO=ART1.CODIGO

left join (
	select articulo, sum(unidades) as stock_uds_lot, almacen 
	from ['+@LOTES+'].[dbo].stocklotes 
	group by articulo, almacen
) stl on stl.articulo=ART1.CODIGO

left join (
	select articulo, sum(final) as stock_uds, ALMACEN
	from ['+@GESTION+'].dbo.stocks2 
	group by articulo, ALMACEN
) stc on stc.articulo=ART1.CODIGO

WHERE ART1.BAJA=0 AND ART1.SUBFAMILIA!='''' 
AND ( case when coalesce(lot.lote,0)=1 then coalesce(stl.stock_uds_lot,0.00) else coalesce(stc.stock_uds,0.00) end)!=0.00 
and art1.codigo!=art2.codigo	
')
select 'Articulos_Sustitucion'




IF EXISTS (SELECT * FROM sys.objects where name='vClientes') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vClientes] AS
SELECT 1 as Nodo,C.CODIGO as codigo, RTRIM(C.NOMBRE) collate database_default AS NOMBRE , C.NOMBRE2 AS RCOMERCIAL,
C.CIF, cast(RTRIM(C.DIRECCION) as varchar(40)) AS DIRECCION, C.CODPOST AS CP, C.POBLACION AS POBLACION,
C.PROVINCIA as provincia, ISNULL(PAIS.NOMBRE,'''') AS PAIS, C.EMAIL as EMAIL, C.EMAIL_F, COALESCE(T.TELEFONO, SPACE(15))
AS TELEFONO, COALESCE(P.PERSONA, SPACE(30)) AS CONTACTO, C.HTTP AS WEB, FP.NOMBRE AS N_PAG,
C.DIAPAG AS DIA_PAG, CAST(ROUND(C.CREDITO,2) AS numeric(10,2)) AS CREDITO, COALESCE(B.BANCO,
SPACE(30)) AS BANCO, COALESCE(B.IBAN, SPACE(4))+'' ''+COALESCE(B.CUENTAIBAN, SPACE(20)) AS IBAN,
COALESCE(B.SWIFT, SPACE(10)) AS SWIFT, C.VENDEDOR AS VENDEDOR, v.NOMBRE as nVendedor,
REPLACE(CONVERT(VARCHAR(MAX), 
C.OBSERVACIO),CHAR(13),''<br/>'') AS OBSERVACIO, C.TARIFA AS TARIFA, ISNULL(FP.DIAS_RIESGO,0)AS DIAS_RIESGO, 
C.DESCU1 AS DESCU1, C.DESCU2 AS DESCU2, C.OFERTA, C.BLOQ_CLI AS BLOQ_CLI, C.ENV_CLI, C.FECHA_BAJ,
case when C.FECHA_BAJ IS NULL or C.FECHA_BAJ=''1900-01-01 00:00:00'' THEN 0 ELSE 1 END AS BAJA, 
'''' as Logo, 
tiva.CODIGO as tipoIVA, isnull(tiva.IVA,0) as IVA, c.AGENCIA,
isnull(tiva.RECARG,0) as recargoIVA,
COALESCE(MCA1.VALOR,'''') collate database_default AS CENTRO,
COALESCE(MCA2.VALOR,'''') collate database_default AS MEDICO,
COALESCE(MCA3.VALOR,'''') collate database_default AS RAZON_SOCIAL_FACT,
C.RUTA, r.NOMBRE as nRuta,
mLat.VALOR as LATITUD, mLon.VALOR as LONGITUD
FROM ['+@GESTION+'].DBO.CLIENTES C 
LEFT JOIN (SELECT CLIENTE, MAX(TELEFONO) AS TELEFONO FROM ['+@GESTION+'].dbo.TELF_CLI WHERE ORDEN=1 GROUP BY CLIENTE) T ON T.CLIENTE = C.CODIGO 
LEFT JOIN ['+@GESTION+'].dbo.CONT_CLI P ON P.CLIENTE = C.CODIGO AND P.ORDEN = 1 
LEFT JOIN ['+@GESTION+'].dbo.BANC_CLI B ON B.CLIENTE = C.CODIGO AND B.ORDEN = 1 
LEFT JOIN ['+@GESTION+'].dbo.FPAG FP    ON FP.CODIGO = C.FPAG 
LEFT JOIN ['+@GESTION+'].dbo.tipo_iva tiva ON tiva.CODIGO = C.TIPO_IVA 
LEFT JOIN ['+@COMUN+'].dbo.PAISES PAIS ON PAIS.CODIGO  = C.PAIS 
LEFT JOIN ['+@GESTION+'].dbo.rutas r on r.CODIGO=C.RUTA
LEFT JOIN ['+@GESTION+'].DBO.vendedor v on v.CODIGO=C.VENDEDOR
LEFT JOIN ['+@GESTION+'].DBO.multicam MCA1 ON MCA1.CODIGO=C.CODIGO AND MCA1.FICHERO=''CLIENTES'' AND MCA1.CAMPO=''CEN''
LEFT JOIN ['+@GESTION+'].DBO.multicam MCA2 ON MCA2.CODIGO=C.CODIGO AND MCA2.FICHERO=''CLIENTES'' AND MCA2.CAMPO=''MED''
LEFT JOIN ['+@GESTION+'].DBO.multicam MCA3 ON MCA3.CODIGO=C.CODIGO AND MCA3.FICHERO=''CLIENTES'' AND MCA3.CAMPO=''RSO''
LEFT JOIN ['+@GESTION+'].DBO.multicam mLat ON mLat.CODIGO=C.CODIGO and mLat.FICHERO=''CLIENTES'' and mLat.CAMPO=''LAT''
LEFT JOIN ['+@GESTION+'].DBO.multicam mLon ON mLon.CODIGO=C.CODIGO and mLon.FICHERO=''CLIENTES'' and mLon.CAMPO=''LGT''
WHERE LEFT(C.CODIGO,3)=''430''
')
select 'vClientes'




IF EXISTS (SELECT * FROM sys.objects where name='d_Busca_PV') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +' view [dbo].[d_Busca_PV] as
SELECT distinct a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre as n_ruta, a.empresa,
               ltrim(rtrim(b.nombre)) as nombre
             , ltrim(rtrim(b.NOMBRE2)) as rComercial 
             , cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra as EN_USO,
             MAX(DPV.SERVIDAS)+SUM(COALESCE(TP.UNIDADES,0.00)) AS servidas, isnull(TP.numero,0) as bNumero,
             e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, a.almacen, MAX(B.IDIOMA_IMP) AS IDIOMA_IMP
FROM ['+@GESTION+'].[dbo].[c_pedive] a
inner join ['+@GESTION+'].[dbo].[clientes] b on a.cliente=b.codigo
left  join ['+@GESTION+'].[dbo].rutas d on d.codigo=a.ruta
left  join Traspaso_Temporal TP on  TP.numero COLLATE Modern_Spanish_CI_AI =a.numero COLLATE Modern_Spanish_CI_AI
left  join ['+@COMUN+'].[DBO].[en_uso] e on RTRIM(LTRIM(e.TIPO))=''PEDIVEN'' and e.CLAVE=cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra
left join ['+@GESTION+'].dbo.d_pedive dpv on dpv.empresa=a.empresa and dpv.numero=a.numero and dpv.letra=a.letra and dpv.articulo!=''''
LEFT JOIN ['+@GESTION+'].dbo.articulo ART ON ART.CODIGO=dpv.ARTICULO
where a.traspasado=0 and a.cancelado=0 and a.TOTALPED>=0.00
GROUP BY a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre, a.empresa, b.nombre, b.NOMBRE2
, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra, isnull(TP.numero,0), e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, a.almacen

')
select 'd_Busca_PV'




IF EXISTS (SELECT * FROM sys.objects where name='d_Busca_PV_DI') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[d_Busca_PV_DI] as
SELECT distinct a.numero, a.fecha, a.entrega, a.letra, a.ruta
, replace(d.nombre,''"'',''-'') as n_ruta
, a.empresa
, replace(b.nombre,''"'',''-'') as nombre
, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra as EN_USO
, (select max(servidas) from ['+@GESTION+'].[dbo].[d_pedive] where NUMERO=a.NUMERO) as servidas, isnull(TP.numero,0) as bNumero
, e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL
FROM ['+@GESTION+'].[dbo].[c_pedive] a
inner join ['+@GESTION+'].[dbo].[clientes] b on a.cliente=b.codigo
left  join ['+@GESTION+'].[dbo].rutas d on d.codigo=a.ruta
left  join Traspaso_Temporal TP on  TP.numero COLLATE Modern_Spanish_CI_AI =a.numero COLLATE Modern_Spanish_CI_AI
left  join ['+@COMUN+'].[DBO].[en_uso] e on RTRIM(LTRIM(e.TIPO))=''PEDIVEN'' and e.CLAVE=cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra
where a.traspasado=0 and a.cancelado=0 and a.entrega<=getdate()+4
')
select 'd_Busca_PV_DI'




IF EXISTS (SELECT * FROM sys.objects where name='d_Busca_PV_Zona') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[d_Busca_PV_Zona] as
SELECT distinct a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre as n_ruta, a.empresa,
replace(b.nombre2,''"'',''-'') as rComercial,
replace(replace(b.nombre,''"'',''-''),'''''''',''-'') as nombre, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra as EN_USO,
(select max(servidas) from ['+@GESTION+'].[dbo].[d_pedive] where NUMERO=a.NUMERO) as servidas, isnull(TP.numero,0) as bNumero,
e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, COALESCE(UPPER(M.VALOR),'''') AS ZONA
FROM ['+@GESTION+'].[dbo].[c_pedive] a
inner join ['+@GESTION+'].[dbo].[clientes] b on a.cliente=b.codigo
left join ['+@GESTION+'].[dbo].rutas d on d.codigo=a.ruta
left join Traspaso_Temporal TP on  TP.numero COLLATE Modern_Spanish_CI_AI =a.numero COLLATE Modern_Spanish_CI_AI
left join ['+@COMUN+'].[DBO].[en_uso] e on RTRIM(LTRIM(e.TIPO))=''PEDIVEN'' and e.CLAVE=cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra
left join ['+@GESTION+'].dbo.d_pedive dpv on dpv.empresa=a.empresa and dpv.numero=a.numero and dpv.letra=a.letra and dpv.articulo!=''''
left join ['+@GESTION+'].dbo.multicam m on m.codigo=dpv.articulo and m.campo=''ZNA''
where a.traspasado=0 and a.cancelado=0 and a.entrega<=getdate()+1
GROUP BY a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre, a.empresa, b.nombre2, b.nombre, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra
, isnull(TP.numero,0),e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, UPPER(M.VALOR)
')
select 'd_Busca_PV_Zona'




IF EXISTS (SELECT * FROM sys.objects where name='d_PV')set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[d_PV] as
SELECT a.numero, a.articulo, a.definicion, a.cajas, a.unidades, a.peso, a.servidas,
b.fecha, b.letra, b.ruta,
c.nombre
FROM ['+@GESTION+'].[dbo].[d_pedive] a
inner join ['+@GESTION+'].[dbo].[c_pedive] b on a.numero=b.numero
inner join ['+@GESTION+'].[dbo].[clientes] c on b.cliente=c.codigo
WHERE a.EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
')
select 'd_PV'




IF EXISTS (SELECT * FROM sys.objects where name='Detalle_Pedidos') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[Detalle_Pedidos] AS
select cpv.cliente,        
    CASE 
    WHEN CLI.CONTADO = 0 THEN cli.nombre
    WHEN CLI.CONTADO = 1 AND COALESCE(CON.NOMBRE,'''')!='''' THEN CON.NOMBRE
    ELSE cli.nombre                   
    END as n_cliente, 
    dpv.linia, dpv.servidas,
    ISNULL(art.stock,1) as cntrlSotck,
    CASE WHEN CLI.CONTADO = 0 
    THEN
        case when env.direccion='''' then cli.direccion else env.direccion end 
    ELSE
        CON.DIRECCION
    END    as direccion, 
    CASE WHEN CLI.CONTADO = 0 
    THEN
    case when env.codpos='''' then cli.codpost else env.codpos end
    ELSE
        CON.CODPOST 
    END as copdpost, 
    CASE WHEN CLI.CONTADO = 0 
    THEN
        case when env.poblacion='''' then cli.poblacion else env.poblacion end
    ELSE
        CON.POBLACION 
    END as poblacion, 
    CASE WHEN CLI.CONTADO = 0 
    THEN
        case when env.provincia='''' then cli.provincia else env.provincia end
    ELSE
        CON.PROVINCIA 
    END as provincia, 

    cli.NOMBRE2 as rComercial,
             
    cpv.empresa, cpv.numero, cpv.letra, cpv.vendedor, ven.nombre as n_vendedor, ven.mobil as Movil_vendedor, 
    cpv.ruta, rut.nombre as n_ruta, dpv.articulo, replace(dpv.definicion,''"'','''') as definicion, coalesce(lot.lote,0) as articulo_lote, 
    dpv.cajas as cajas_pv, dpv.unidades as uds_pv, 
    case when cast(case when art.peso='''' then ''0.0000'' else art.peso end as decimal(16,4))!=0.00 and dpv.peso=0.00 then cast(case when art.peso='''' then ''0.0000'' else art.peso end as decimal(16,4))*dpv.unidades else dpv.peso end as peso_pv, 
    dpv.cajas-dpv.CAJASERV-coalesce(trt.cajas,0.00) as cajas_pendientes, 
    dpv.unidades-dpv.servidas-coalesce(trt.unidades,0.000) as uds_pendiente, 
    (case when cast(case when art.peso='''' then ''0.0000'' else art.peso end as decimal(16,4))!=0.00 and dpv.peso=0.00 then cast(case when art.peso='''' then ''0.0000'' else art.peso end as decimal(16,4))*dpv.unidades else dpv.peso end)-(dpv.servidas*cast(case when art.peso='''' then ''0.0000'' else art.peso end as decimal(16,4)))-cast(coalesce(trt.peso,''000000000.0000'') as decimal(16,4)) as peso_pendiente, 
    coalesce(trt.cajas,0.00) as cajas_traspasadas, 
    coalesce(trt.unidades,0.000) as uds_traspasadas, 
    cast(coalesce(trt.peso,''0.00'') as decimal(16,4)) as peso_traspasado,
    case when coalesce(lot.lote,0)=1 then coalesce(stl.stock_uds_lot,0.00) else coalesce(stc.stock_uds,0.00) end  as stock,
    '''' as ColID
    , coalesce(DPV.LIBRE_4,'''') as notas_traspasadas
    , dpv.dto1+dpv.dto2 as dto
    , coalesce(m.valor,'''') as zona
    , art.ubicacion
    , cli.OBSERVACIO as ClienteObs
    , cpv.OBSERVACIO as PedidoObs
    , art.UNICAJA 
    , CASE WHEN coalesce(m2.valor,'''')='''' OR  coalesce(m2.valor,'''')=''.F.'' THEN 0 ELSE 1 END as VENTA_CAJA
    , case when trt.articulo is null then 0 else 1 end preparado -- Para saber si las notas estan preparadas
	, dpv.TIPO_IVA as TipoIVA
from ['+@GESTION+'].dbo.c_pedive cpv 
inner join ['+@GESTION+'].dbo.d_pedive dpv on cpv.empresa=DPV.EMPRESA AND cpv.numero=DPV.NUMERO AND cpv.letra=dpv.letra
left  join  (select empresa, numero, letra, linea, articulo, sum(coalesce(cajas,0.00)) as cajas, sum(unidades) as unidades, sum(cast(case when peso='''' then ''0.00'' else coalesce(peso,''0.00'') end as decimal(16,4))) as peso from Traspaso_Temporal group by empresa, numero, letra, linea, articulo) trt on trt.empresa collate database_default=DPV.EMPRESA AND trt.numero collate database_default=DPV.NUMERO AND trt.letra collate database_default=DPV.LETRA AND trt.linea=DPV.LINIA 
left join ['+@GESTION+'].dbo.articulo art on art.codigo=dpv.articulo 
left join  ['+@GESTION+'].dbo.rutas rut on rut.codigo=cpv.ruta 
left join  ['+@GESTION+'].dbo.vendedor ven on ven.codigo=cpv.vendedor 
inner join ['+@GESTION+'].dbo.clientes cli on cli.codigo=cpv.cliente 
left join  ['+@GESTION+'].dbo.env_cli env on env.linea=cpv.env_cli and env.cliente=cpv.cliente 
left join  ['+@LOTES+'].dbo.artlot lot on lot.articulo=dpv.articulo
left join (select empresa, almacen, articulo, sum(peso) as stock_peso_lot, sum(unidades) as stock_uds_lot from ['+@LOTES+'].[dbo].stocklotes 
group by empresa, almacen, articulo) stl on stl.empresa=cpv.empresa and stl.articulo=dpv.articulo and stl.almacen=cpv.almacen
left join (select empresa, almacen, articulo, sum(final) as stock_uds from ['+@GESTION+'].dbo.stocks2 group by empresa, almacen, articulo) stc on stc.empresa=cpv.empresa and stc.almacen=cpv.almacen and stc.articulo=dpv.articulo
left join ['+@GESTION+'].dbo.multicam m on m.codigo=dpv.articulo and m.campo=''ZNA''
left join ['+@GESTION+'].dbo.multicam m2 on m2.codigo=dpv.articulo and m2.campo=''SCA''
LEFT JOIN ['+@GESTION+'].DBO.CONTADO CON ON CON.EMPRESA=CPV.EMPRESA AND CON.NUMERO=CPV.NUMERO AND CON.LETRA=CPV.LETRA AND CON.TIPO=2
where cpv.traspasado=0 and cpv.cancelado=0 and (dpv.servidas<dpv.unidades or dpv.articulo='''') and coalesce(DPV.LIBRE_4,'''')=''''
')
select 'Detalle_Pedidos'




IF EXISTS (SELECT * FROM sys.objects where name='Pendiente_Pedidos') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[Pendiente_Pedidos] AS
SELECT DPV.EMPRESA, DPV.NUMERO, DPV.LETRA, DPV.LINIA, DPV.ARTICULO, DPV.UNIDADES 
FROM ['+@GESTION+'].DBO.D_PEDIVE DPV 
INNER JOIN ['+@GESTION+'].DBO.C_PEDIVE CPV ON CPV.EMPRESA=DPV.EMPRESA AND CPV.NUMERO=DPV.NUMERO AND CPV.LETRA=DPV.LETRA 
LEFT JOIN (SELECT EMPRESA, NUMERO, LETRA, LINEA, SUM(COALESCE(UNIDADES,0.00)) AS UNIDADES 
FROM Traspaso_Temporal 
GROUP BY  EMPRESA, NUMERO, LETRA, LINEA
) TEM ON TEM.EMPRESA collate Modern_Spanish_CI_AI=DPV.EMPRESA 
AND TEM.NUMERO collate Modern_Spanish_CI_AI=DPV.NUMERO 
AND TEM.LETRA collate Modern_Spanish_CI_AI=DPV.LETRA AND TEM.LINEA=DPV.LINIA 
WHERE CPV.TRASPASADO=0 AND CPV.CANCELADO=0 AND DPV.ARTICULO!='''' AND DPV.UNIDADES>DPV.SERVIDAS AND DPV.UNIDADES>COALESCE(TEM.UNIDADES,0.00)
')
select 'Pendiente_Pedidos'




IF EXISTS (SELECT * FROM sys.objects where name='v_d_pedive') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[v_d_pedive] as
SELECT	a.empresa, a.numero, a.linia, a.letra,
a.articulo,a.definicion,a.cajas,a.unidades, a.peso, a.servidas, a.traspaso,
c.CODIGO, c.NOMBRE, c.DIRECCION, c.CODPOST, c.POBLACION, c.PROVINCIA,
d.CODIGO as rutaCodigo, d.NOMBRE as rutaNombre,
e.CODIGO as vendedorCodigo, e.NOMBRE as vendedorNombre, e.MOBIL as vendedorMovil,
cast(f.LOTE as int) as usaLote
from ['+@GESTION+'].[dbo].d_pedive a
inner join ['+@GESTION+'].[dbo].c_pedive	b on a.NUMERO=b.NUMERO
inner join ['+@GESTION+'].[dbo].clientes	c on c.CODIGO=b.CLIENTE
inner join ['+@GESTION+'].[dbo].rutas		d on d.CODIGO=c.RUTA
inner join ['+@GESTION+'].[dbo].vendedor	e on e.CODIGO=c.VENDEDOR
left  join ['+@LOTES+'].[dbo].artlot	f on f.ARTICULO=a.ARTICULO  
where a.EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
')
select 'v_d_pedive'




IF EXISTS (SELECT * FROM sys.objects where name='v_d_traspaso') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[v_d_traspaso] as
SELECT empresa, numero, linia, letra, articulo, definicion, cajas, unidades, peso, servidas, traspaso
FROM ['+@GESTION+'].[dbo].[d_pedive]
')
select 'v_d_traspaso'




IF EXISTS (SELECT * FROM sys.objects where name='vAlbaranes') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vAlbaranes] AS
SELECT 
'''+@EJERCICIO+'''+CA.EMPRESA+REPLACE(CA.letra,SPACE(1),''0'')+REPLACE(CA.NUMERO, SPACE(1), ''0'') COLLATE Modern_Spanish_CI_AI AS IDALBARAN
,'''+@EJERCICIO+''' AS EJER, CA.EMPRESA, CA.LETRA as letra, ltrim(rtrim(CA.NUMERO)) as NUMERO, CA.LETRA+CA.NUMERO AS ALBARAN
, CA.CLIENTE
		
, CASE WHEN vcli.CONTADO = 0 
THEN
case when coalesce(vCON.PERSONA,'''')='''' then vcli.nombre else vCON.PERSONA end 
ELSE
CON.nombre
END	as nCliente	
, vcli.nombre as nFiscal
, convert(varchar(10),CA.FECHA,103) as FECHA, CAST(ROUND(CA.IMPORTE,2) AS NUMERIC(18,2)) AS IMPORTE, CA.IMPORTE as IMPORTEIVA, CA.TOTALDOC as TOTAL
, CAST(COALESCE(CASE WHEN RIGHT(CA.PEDIDO,2) = '''' THEN LTRIM(RTRIM(CA.PEDIDO)) ELSE LTRIM(RTRIM(RIGHT(CA.PEDIDO,2)))+''-''+LTRIM(RTRIM(LEFT(CA.PEDIDO,10))) END, SPACE(0)) AS VARCHAR(13)) AS PEDIDO
, CA.VENDEDOR, vven.NOMBRE COLLATE Modern_Spanish_CI_AI as nVendedor, CA.ALMACEN, CA.FINAN, CA.PRONTO 
	
, CASE WHEN vcli.CONTADO = 0 
THEN
case when coalesce(EC.direccion,'''')='''' then vcli.direccion else EC.direccion end 
ELSE
CON.DIRECCION
END	as direccion
, CASE WHEN vcli.CONTADO = 0 
THEN
case when coalesce(EC.POBLACION,'''')='''' then vcli.POBLACION else EC.POBLACION end 
ELSE
CON.POBLACION
END	as POBLACION
, CASE WHEN vcli.CONTADO = 0 
THEN
case when coalesce(EC.CODPOS,'''')='''' then vcli.CODPOST else EC.CODPOS end 
ELSE
CON.CODPOST
END	as CODPOS
, CASE WHEN vcli.CONTADO = 0 
THEN
case when coalesce(EC.PROVINCIA,'''')='''' then vcli.PROVINCIA else EC.PROVINCIA end 
ELSE
CON.PROVINCIA
END	as PROVINCIA
	
, CA.FPAG, COALESCE(B.BANCO,SPACE(0)) AS BANCO, COALESCE((B.IBAN+B.CUENTAIBAN),SPACE(0)) AS IBAN
, CA.FACTURA
, vcli.RUTA
,vcli.NOMBRE2
, concat(ltrim(rtrim(vcli.RUTA)),''-'',datepart(DW,CA.FECHA),''-'',LEFT(RIGHT(ltrim(rtrim((CA.PEDIDO))),7),5)) as rutaComp
, CAST(vcon.CLIENTE AS VARCHAR)+Replace(str(CAST(vcon.LINEA AS VARCHAR),4),space(1),''0'') COLLATE Modern_Spanish_CI_AI as contacto, vcon.PERSONA COLLATE Modern_Spanish_CI_AI as nContacto
, POR.AGENCIA COLLATE Modern_Spanish_CI_AI as agencia, ltrim(rtrim(ag.NOMBRE)) COLLATE Modern_Spanish_CI_AI as nAgencia
, VVEN.MOBIL, M.VALOR AS HORARIO, VCLI.VALOR_ALB, COALESCE(EE.BULTOS,00000) AS BULTOS, (CA.IMPORTE * CA.PRONTO)/100 as importePP, por.IMPORTE as portesIMP
, DA.SUM_CAJAS, DA.SUM_PESO, DA.SUM_DTO1, DA.SUM_DTO2, VCLI.CIF, TELF.TELEFONO, convert(nvarchar(250),ca.observacio) as observaciones
FROM ['+@GESTION+'].DBO.C_ALBVEN CA
LEFT JOIN ['+@GESTION+'].dbo.ENV_CLI EC ON CA.CLIENTE = EC.CLIENTE AND CA.ENV_CLI = EC.LINEA 
LEFT JOIN ['+@GESTION+'].DBO.BANC_CLI B ON B.CLIENTE=CA.CLIENTE AND B.CODIGO=CA.BANC_CLI
LEFT JOIN ['+@GESTION+'].dbo.clientes vcli on vcli.CODIGO =CA.CLIENTE
LEFT JOIN ['+@GESTION+'].dbo.vendedor vven on vven.CODIGO =CA.VENDEDOR
LEFT JOIN ['+@GESTION+'].DBO.CONT_CLI  vcon on vcon.CLIENTE=CA.CLIENTE and vcon.LINEA=1
LEFT JOIN ['+@GESTION+'].DBO.PORTES POR ON POR.EMPRESA=CA.EMPRESA AND POR.LETRA=CA.LETRA AND POR.ALBARAN=CA.NUMERO
LEFT JOIN ['+@GESTION+'].DBO.agencia ag on ag.CODIGO=POR.AGENCIA
LEFT JOIN ['+@GESTION+'].[dbo].MULTICAM M on M.FICHERO=''CLIENTES'' and M.CAMPO=''HOR'' AND M.CODIGO=CA.CLIENTE
left join ['+@GESTION+'].DBO.ENVIOETI EE ON EE.EMPRESA=CA.EMPRESA AND EE.NUMERO=CA.NUMERO AND EE.LETRA=CA.LETRA
INNER JOIN (SELECT D.EMPRESA, D.NUMERO, D.LETRA, SUM(D.CAJAS) AS SUM_CAJAS, SUM(D.PESO+CONVERT(DECIMAL(16,3),CASE WHEN A.LITROS='''' THEN ''0.00'' ELSE A.LITROS END)) AS SUM_PESO, SUM(D.DTO1) AS SUM_DTO1, SUM(D.DTO2) AS SUM_DTO2 FROM ['+@GESTION+'].DBO.D_ALBVEN D INNER JOIN ['+@GESTION+'].DBO.ARTICULO A ON A.CODIGO=D.ARTICULO GROUP BY D.EMPRESA, D.NUMERO, D.LETRA) DA ON DA.EMPRESA=CA.EMPRESA AND DA.NUMERO=CA.NUMERO AND DA.LETRA=CA.LETRA
LEFT JOIN (SELECT CLIENTE, MAX(TELEFONO) AS TELEFONO FROM ['+@GESTION+'].DBO.TELF_CLI WHERE ORDEN=1 GROUP BY CLIENTE) TELF ON TELF.CLIENTE=CA.CLIENTE
LEFT JOIN ['+@GESTION+'].DBO.CONTADO CON ON CON.EMPRESA=CA.EMPRESA AND CON.NUMERO=CA.NUMERO AND CON.LETRA=CA.LETRA AND CON.TIPO=1
WHERE CA.TRASPASADO = 0 AND LEFT(CA.CLIENTE,3)=''430''
')
select 'vAlbaranes'




IF EXISTS (SELECT * FROM sys.objects where name='vAlbaranes_Cabecera') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vAlbaranes_Cabecera] as
select	
'''+@EJERCICIO+'''+CA.EMPRESA  collate Modern_Spanish_CI_AI +REPLACE(CA.LETRA  collate Modern_Spanish_CI_AI,SPACE(1),''0'')
+REPLACE(CA.NUMERO  collate Modern_Spanish_CI_AI, SPACE(1),''0'') as IDALBARAN
, CA.LETRA + CA.NUMERO AS ALBARAN
, CA.PRONTO
, CA.IMPORTE as BaseImponible
, p.IMPORTE as portesIMP
, (CA.IMPORTE * CA.PRONTO)/100 as importePP
, ca.totaldoc as TOTALDOC
, e.IMPORTE as entregaIMP 
, CAST(CASE WHEN CA.FACTURA = '''' THEN (CASE WHEN CA.FACTURABLE=1 THEN ''PENDIENTE'' ELSE ''NO FACTURABLE'' END) ELSE ''FACTURADO'' END AS varchar(50)) 
    COLLATE Modern_Spanish_CI_AI AS ESTADO 
FROM ['+@GESTION+'].DBO.C_ALBVEN CA 
LEFT JOIN ['+@GESTION+'].dbo.ENV_CLI EC ON CA.CLIENTE = EC.CLIENTE AND CA.ENV_CLI = EC.LINEA 
left join ['+@GESTION+'].dbo.portes p on p.ALBARAN=CA.NUMERO and CA.EMPRESA=p.EMPRESA and CA.LETRA=p.LETRA
left join ['+@GESTION+'].dbo.entregas e on e.ALBARAN=CA.NUMERO and CA.EMPRESA=e.EMPRESA and CA.LETRA=e.LETRA
')
select 'vAlbaranes_Cabecera'




IF EXISTS (SELECT * FROM sys.objects where name='vAlbaranes_Detalle') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vAlbaranes_Detalle] AS
SELECT 
'''+@EJERCICIO+'''+C.EMPRESA+REPLACE(C.letra,SPACE(1),''0'')+REPLACE(C.NUMERO, SPACE(1), ''0'') COLLATE Modern_Spanish_CI_AI AS IDALBARAN
,'''+@EJERCICIO+''' AS EJER, d.empresa, d.letra, d.numero, C.LETRA+D.NUMERO AS ALBARAN, D.CLIENTE
, D.ARTICULO, D.DEFINICION, D.UNIDADES, D.CAJAS, D.PESO, D.SERIE, D.PRECIO, D.LINIA, D.DTO1, D.DTO2, D.IMPORTE, D.Tipo_IVA as Tipo_IVA, d.pverde, d.PRECIOIVA , d.IMPDIVIVA, coalesce(iva.iva,0.00) as porcen_iva
,case when d.doc=1 then d.DOC_NUM else '''' end as PEDIDO, C.VENDEDOR
FROM ['+@GESTION+'].DBO.D_ALBVEN D
INNER JOIN ['+@GESTION+'].dbo.C_ALBVEN C ON D.EMPRESA = C.EMPRESA AND D.LETRA = C.LETRA AND D.NUMERO = C.NUMERO
left join ['+@GESTION+'].dbo.tipo_iva iva on iva.codigo=d.TIPO_IVA 
WHERE C.TRASPASADO = 0 AND LEFT(D.CLIENTE,3)=''430''
')
select 'vAlbaranes_Detalle'




IF EXISTS (SELECT * FROM sys.objects where name='vAlbaranes_Lote') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vAlbaranes_Lote] AS
SELECT
'''+@EJERCICIO+'''+d.EMPRESA+REPLACE(d.letra,SPACE(1),''0'')+REPLACE(d.NUMERO, SPACE(1), ''0'') COLLATE Modern_Spanish_CI_AI AS IDALBARAN
,'''+@EJERCICIO+''' AS PERIODO, D.EMPRESA, d.numero, d.letra, d.letra+D.NUMERO AS ALBARAN, d.LINIA, d.LOTE, d.UNIDADES
from ['+@LOTES+'].dbo.LTALBVE d where d.periodo='''+@EJERCICIO+'''
')
select 'vAlbaranes_Lote'




IF EXISTS (SELECT * FROM sys.objects where name='vAlbaranes_Pie') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vAlbaranes_Pie] AS 	
SELECT 
'''+@EJERCICIO+'''+CAv.EMPRESA+REPLACE(CAv.letra,SPACE(1),''0'')+REPLACE(CAv.NUMERO, SPACE(1), ''0'') COLLATE Modern_Spanish_CI_AI AS IDALBARAN
,'''+@EJERCICIO+''' as EJER,	CAV.EMPRESA, CAV.LETRA, ltrim(rtrim(CAv.NUMERO)) as NUMERO, CAv.LETRA+CAv.NUMERO AS ALBARAN
, CAV.CLIENTE, CLI.RECARGO as CliRecargo, dav.tipo_iva, iva.IVA as porcen_iva, CASE WHEN CLI.RECARGO=1 THEN IVA.RECARG ELSE 0.00 END AS TIPO_RE,
CAV.PRONTO as pPago,

SUM(DAV.PVERDE) AS PVERDE, 

SUM(CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END  + DAV.PVERDE)
AS IMPORTE,

ROUND(SUM((
CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END + DAV.PVERDE)*(IVA.IVA/100)),2) 
AS IVA,

CASE WHEN CLI.RECARGO=1 
THEN round(SUM((
CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.RECARG/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END + DAV.PVERDE)*(IVA.RECARG/100)),2) 
ELSE 0.00 
END  
AS RECARGO,

SUM(CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END + DAV.PVERDE)  
+
ROUND(SUM((
CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END + DAV.PVERDE)*(IVA.IVA/100)),2) 
+
CASE WHEN CLI.RECARGO=1 
THEN round(SUM((
CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.RECARG/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END + DAV.PVERDE)*(IVA.RECARG/100)),2) 
ELSE 0.00 
END  
AS TOTALDOC

FROM ['+@GESTION+'].DBO.C_ALBVEN CAV 
INNER JOIN 
(select EMPRESA, NUMERO, LETRA, ARTICULO, IMPORTE, TIPO_iva, 1 AS INC_PP, 0 AS IVA_INC, PVERDE
from ['+@GESTION+'].dbo.d_albven where tipo_iva!='''' and importe!=0.00

union all

select  EMPRESA, albaran, LETRA, ''PORTES'' as ARTICULO, IMPORTE, TIPO_iva, INC_PP, IVA_INC, 0.00 AS PVERDE 
from ['+@GESTION+'].dbo.portes) DAV ON DAV.EMPRESA=CAV.EMPRESA AND DAV.NUMERO=CAV.NUMERO AND DAV.LETRA=CAV.LETRA 
LEFT JOIN ['+@GESTION+'].dbo.tipo_iva IVA ON IVA.CODIGO=DAV.TIPO_IVA
INNER JOIN ['+@GESTION+'].dbo.clientes CLI ON CLI.CODIGO=CAV.CLIENTE
LEFT JOIN ['+@GESTION+'].dbo.tipo_iva IVC ON IVC.CODIGO=CLI.TIPO_IVA WHERE DAV.IMPORTE!=0.00
GROUP BY CAV.EMPRESA, CAV.NUMERO, CAV.LETRA, CAV.CLIENTE, CLI.RECARGO, dav.tipo_iva, iva.IVA, IVA.RECARG, CAV.PRONTO
')
select 'vAlbaranes_Pie'




IF EXISTS (SELECT * FROM sys.objects where name='vArticulos') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vArticulos]
as
select * from ['+@GESTION+'].dbo.articulo WHERE BAJA=0
')
select 'vArticulos'




IF EXISTS (SELECT * FROM sys.objects where name='vArticulosStock') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vArticulosStock] as

select alm.codigo as almacen, 
case when coalesce(lot.lote,0)=1 then coalesce(stl.stock_uds_lot,0.00) 
else coalesce(stc.stock_uds,0.00) end as ArticuloStock

,art.[CODIGO]
,replace(art.NOMBRE,''"'',''-'') as NOMBRE
,art.[ABREV]
,art.[FAMILIA]
,art.[MARCA]
,art.[MINIMO]
,art.[MAXIMO]
,art.[AVISO]
,art.[BAJA]
,art.[TIPO_IVA]
,art.[RETENCION]
,art.[IVA_INC]
,art.[COST_ULT1]
,art.[FECHA_ULT]
,art.[ULT_FECHA]
,art.[PMCOM1]
,art.[IMAGEN]
,art.[CARAC]
,art.[FECHAALTA]
,art.[FECHABAJA]
,art.[UBICACION]
,art.[MEDIDAS]
,art.[PESO]
,art.[LITROS]
,replace(cast(art.OBSERVACIO as varchar(max)),''"'',''-'') as OBSERVACIO
,art.[UNICAJA]
,art.[DESGLOSE]
,art.[ARANCELES]
,art.[DEFINICION2]
,art.[SUBFAMILIA]
,art.[INTERNET]
,art.[VISTA]
,art.[FPAG]
,art.[PVERDE]
,art.[P_IMPORTE]
,art.[P_TAN]
,art.[LCOLOR]
,art.[MARGEN]
,art.[TCP]
,art.[VENSERIE]
,art.[PUNTOS]
,art.[DES_ESC]
,art.[TIPO_ART]
,art.[MODELO]
,art.[COCINA]
,art.[STOCK]
,art.[ART_IMPUES]
,replace(cast(art.NOMBRE2 as varchar(max)),''"'',''-'') as NOMBRE2
,art.[COLOR_ART]
,art.[TIPO_PVP]
,art.[COST_ESCAN]
,art.[TIPO_ESCAN]
,art.[ART_CANON]
,art.[ACTUA_COLO]
,art.[FACT_AREPE]
,art.[GARANTIA]
,art.[ALQUILER]
,art.[ORDEN]
,art.[C_ENT]
,art.[CN8]
,art.[IVALOT]
,art.[ARTANT]
,art.[REPORTETIQ]
,art.[GUID]
,art.[IMPORTAR]
,art.[DTO1]
,art.[DTO2]
,art.[DTO3]
,art.[ISP]
,art.[GRUPOIVA]

from ['+@GESTION+'].dbo.articulo art
inner join ['+@GESTION+'].dbo.almacen alm on 1=1
left join  ['+@LOTES+'].dbo.artlot lot on lot.articulo=art.codigo
left join (
	select empresa, almacen, articulo, sum(peso) as stock_peso_lot, sum(unidades) as stock_uds_lot 
	from ['+@LOTES+'].[dbo].stocklotes 
	group by empresa, almacen, articulo
) stl on stl.articulo=art.codigo and stl.almacen=alm.codigo
left join (
	select empresa, almacen, articulo, sum(final) as stock_uds 
	from ['+@GESTION+'].dbo.stocks2 
	group by empresa, almacen, articulo
) stc on stc.almacen=alm.codigo and stc.articulo=art.codigo
where art.BAJA=0
')
select 'vArticulosStock'




IF EXISTS (SELECT * FROM sys.objects where name='vBuscarLotes') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vBuscarLotes]
as
select b.PESO as pesoArticulo, b.stock as controlStock, 
isnull(c.CADUCIDAD,'''') as Lcaducidad, c.LOTE as Llote, c.UNIDADES-coalesce(convert(decimal(16,4),t.unidades),0.00) as Lunidades, 
c.PESO-coalesce(convert(decimal(16,4),t.peso),0.00) as Lpeso, 
a.EMPRESA, a.NUMERO, A.LETRA, a.LINIA, a.ARTICULO, a.DEFINICION, a.UNIDADES, a.SERVIDAS, a.CAJAS as pCajas, 
a.CAJASERV as pCajaServ, 
isnull((select MAX(CAJAS) from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
       and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO),0) 
       as tempMaxCajas, 
isnull((select UNIDADES from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
       and linea=a.LINIA 
       and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO 
       and lote  collate Modern_Spanish_CI_AI=c.LOTE),0) 
       as tempUnidades, 
isnull((select CAJAS from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
       and linea=a.LINIA 
       and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO 
       and lote  collate Modern_Spanish_CI_AI=c.LOTE),0) 
       as tempCajas, 
isnull((select PESO from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
       and linea=a.LINIA 
       and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO 
       and lote  collate Modern_Spanish_CI_AI=c.LOTE),0.000) 
       as tempPeso, 
b.UNICAJA 
       , CASE WHEN coalesce(m2.valor,'''')='''' OR  coalesce(m2.valor,'''')=''.F.'' THEN 0 ELSE 1 END as VENTA_CAJAS
       , c.almacen
FROM ['+@GESTION+'].[dbo].[d_pedive] a 
INNER JOIN ['+@GESTION+'].[dbo].[C_pedive] D ON D.EMPRESA=A.EMPRESA AND D.NUMERO=A.NUMERO AND D.LETRA=A.LETRA
left join ['+@GESTION+'].[dbo].articulo b on b.CODIGO=a.ARTICULO 
left join ['+@LOTES+'].[dbo].stocklotes c on c.ARTICULO=a.ARTICULO and c.almacen=d.almacen
LEFT JOIN (select T.empresa, T.almacen, T.articulo, T.LOTE, SUM(T.unidades) AS UNIDADES, 
	SUM(CONVERT(DECIMAL(16,4),COALESCE(T.PESO,''0.00''))) AS PESO from Traspaso_Temporal t 
	inner join ['+@GESTION+'].dbo.C_pedive c on c.empresa=t.empresa collate database_default and c.numero=t.numero collate database_default 
	and c.letra=t.letra collate database_default 
	INNER JOIN ['+@GESTION+'].dbo.D_pedive D on D.empresa=c.empresa collate database_default and D.numero=c.numero collate database_default 
	and D.letra=c.letra AND D.LINIA=T.LINeA  where c.traspasado=0 AND D.SERVIDAS<D.UNIDADES 
	GROUP BY T.empresa, T.almacen, T.articulo, T.LOTE
) T on T.EMPRESA collate SQL_Latin1_General_CP1_CI_AS=A.EMPRESA 
	AND t.articulo collate SQL_Latin1_General_CP1_CI_AS=a.articulo 
	and t.almacen collate SQL_Latin1_General_CP1_CI_AS=d.almacen 
	and t.lote collate SQL_Latin1_General_CP1_CI_AS=c.lote
left join ['+@GESTION+'].dbo.multicam m2 on m2.codigo=A.articulo and m2.campo=''SCA''
')
select 'vBuscarLotes'




IF EXISTS (SELECT * FROM sys.objects where name='vSTOCKS') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vSTOCKS] as
(
SELECT TOP 100 PERCENT A.CODIGO, A.NOMBRE, COALESCE(MOV.EMPRESA,''01'') AS EMPRESA, COALESCE(MOV.ALMACEN,'''') AS ALMACEN, COALESCE(MOV.TIPO,'''') AS TIPOMOV, COALESCE(MOV.UNIDADES,0) AS UNIDADES, COALESCE(MOV.PESO,0) AS PESO , MOV.FECHASTOCK
FROM ['+@GESTION+'].DBO.ARTICULO A INNER JOIN ['+@LOTES+'] .DBO.artlot AL ON AL.articulo = A.CODIGO AND AL.LOTE=0
LEFT JOIN (
SELECT ''IN'' AS TIPO, I.EMPRESA, I.ALMACEN, I.ARTICULO, I.UNIDADES AS UNIDADES, I.PESO AS PESO, I.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.regulari I INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=I.ARTICULO AND LOT.LOTE=0 WHERE I.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND I.FECHA=(SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=I.EMPRESA AND ALMACEN=I.ALMACEN AND ARTICULO=I.ARTICULO   GROUP BY EMPRESA, ALMACEN, ARTICULO)
UNION ALL
SELECT ''SI'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, (L.UNIDADES) AS UNIDADES, L.PESO AS PESO, L.FECHA AS FECHASTOCKD FROM ['+@GESTION+'].DBO.stockini L  INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL 
select ''TO'' AS TIPO, C.EMPRESA, C.ALMORIG, L.ARTICULO, -L.UNIDADES, -L.PESO, c.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.c_albatr c inner join ['+@GESTION+'].DBO.d_albatr L on l.empresa=c.empresa and l.numero=c.numero INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE c.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND c.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMORIG AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
select ''TD'' AS TIPO, C.EMPRESA, C.ALMDEST, L.ARTICULO, L.UNIDADES, L.PESO, c.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.c_albatr c inner join ['+@GESTION+'].DBO.d_albatr L on l.empresa=c.empresa and l.numero=c.numero INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE c.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND c.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMDEST AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
		
UNION ALL 
SELECT ''AV'' AS TIPO, C.EMPRESA, C.ALMACEN, L.ARTICULO, -(L.UNIDADES) as UNIDADES, -(L.PESO) AS PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.C_ALBVEN C INNER JOIN ['+@GESTION+'].DBO.D_ALBVEN L ON L.EMPRESA=C.EMPRESA AND L.LETRA=C.LETRA AND L.NUMERO=C.NUMERO INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''AC'' AS TIPO, C.EMPRESA, C.ALMACEN, L.ARTICULO, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.C_ALBCOM C INNER JOIN ['+@GESTION+'].DBO.D_ALBCOM L ON L.EMPRESA=c.empresa and l.numero=c.numero and l.PROVEEDOR =c.proveedor INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''CP'' AS TIPO, C.EMPRESA, C.ALMACEN, C.ARTICULO, C.ENTRADA AS UNIDADES, C.PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.C_PROD C INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=C.ARTICULO AND LOT.LOTE=0 WHERE C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=C.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''DP'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, -(L.SALIDA) AS UNIDADES, -(L.PESO) AS PESO, L.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.D_PROD L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''CT'' AS TIPO, C.EMPRESA, C.ALMACEN, C.ARTICULO, -(C.SALIDA) AS UNIDADES, -(C.PESO) AS PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.C_TRANS C INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=C.ARTICULO AND LOT.LOTE=0 WHERE C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=C.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
		
UNION ALL
SELECT ''DT'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, (L.ENTRADA) AS UNIDADES, (L.PESO) AS PESO, L.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.D_TRANS L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''AR'' AS TIPO, C.EMPRESA, C.ALMACEN, L.ARTICULO, -L.UNIDADES, -L.PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.c_albare C INNER JOIN ['+@GESTION+'].DBO.D_albare L ON L.EMPRESA=C.EMPRESA AND L.NUMERO=C.NUMERO INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=L.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
select ''DV'' AS TIPO, C.EMPRESA, C.ALMACEN, L.ARTICULO, -(L.UNIDADES-L.SERVIDAS) AS UNIDADES, -(L.PESO-(L.SERVIDAS*(CONVERT(DECIMAL(10,3),CASE WHEN A.PESO='''' THEN ''0.00'' ELSE A.PESO END)))) AS PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.C_ALBDEP C INNER JOIN ['+@GESTION+'].DBO.D_ALBDEP L ON L.EMPRESA=C.EMPRESA AND L.LETRA=C.LETRA AND L.NUMERO=C.NUMERO INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 INNER JOIN ['+@GESTION+'].DBO.ARTICULO A ON A.CODIGO=L.ARTICULO WHERE C.TRASPASADO=0 AND L.UNIDADES>L.SERVIDAS AND C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''DC'' AS TIPO, C.EMPRESA, C.ALMACEN, L.ARTICULO, (L.UNIDADES-L.SERVIDAS) AS UNIDADES, (L.PESO-(L.SERVIDAS*(CONVERT(DECIMAL(10,3),CASE WHEN A.PESO='''' THEN ''0.00'' ELSE A.PESO END)))) AS PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.c_depcom C INNER JOIN ['+@GESTION+'].DBO.D_DEPCOM L ON L.EMPRESA=C.EMPRESA AND L.NUMERO=C.NUMERO INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 INNER JOIN ['+@GESTION+'].DBO.ARTICULO A ON A.CODIGO=L.ARTICULO WHERE C.TRASPASADO=0 AND L.UNIDADES>L.SERVIDAS AND C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
) MOV ON A.CODIGO=MOV.ARTICULO and MOV.EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
WHERE A.STOCK=0 ORDER BY 1,4
)
')
select 'vSTOCKS'




IF EXISTS (SELECT * FROM sys.objects where name='vStock') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vStock] as
SELECT     EMPRESA, CODIGO AS ARTICULO, ALMACEN, SUM(COALESCE(UNIDADES,0)) AS UNIDADES, SUM(COALESCE(PESO,0)) AS PESO
FROM         dbo.vSTOCKS 
where EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
GROUP BY EMPRESA, CODIGO, ALMACEN
HAVING SUM(COALESCE(UNIDADES,0))<>0
')
select 'vStock'




IF EXISTS (SELECT * FROM sys.objects where name='vCajasOunidades') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vCajasOunidades] as
select isnull(b.PESO,0.000) as pesoArticulo, isnull(b.UNICAJA,0) as cajasArticulo, isnull(b.stock,0) as cntrlSotck, 
a.EMPRESA, a.NUMERO, a.LETRA, a.LINIA, a.ARTICULO, a.DEFINICION
, isnull(a.UNIDADES,0.00) as UNIDADES
, isnull(a.CAJAS,0.00) as pCajas
, isnull(a.PESO,0.000) as PESO
, isnull(a.TRASPASO,0.00) as TRASPASO
, isnull(C.unidades,0.00) as elStock
, isnull(c.peso,0.000) AS STOCKPESO
, isnull(c.unidades*b.unicaja,0.00) AS STOCKCAJAS
,
isnull(
	(select cajas from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
	and linea=a.LINIA 
	and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO )
,a.CAJAS) as tempCajas, 
isnull(
	(select unidades from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
	and linea=a.LINIA 
	and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO )
,a.UNIDADES) as tempUds, 
isnull(
	(select peso from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
	and linea=a.LINIA 
	and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO)
,0.000) as tempPeso 

FROM ['+@GESTION+'].[dbo].[d_pedive] a 
left join ['+@GESTION+'].[dbo].articulo b on b.CODIGO=a.ARTICULO 
left join [vStock] c on c.ARTICULO=a.ARTICULO 
where a.EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from Configuracion)
')
select 'vCajasOunidades'




IF EXISTS (SELECT * FROM sys.objects where name='vContadorDescuentoAlbaran') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vContadorDescuentoAlbaran] AS
select IdAlbaran, sum(DTO1) as DTO1, sum(DTO2) as DTO2 from vAlbaranes_Detalle
group by IdAlbaran
')
select 'vContadorDescuentoAlbaran'




IF EXISTS (SELECT * FROM sys.objects where name='vDatosEmpresa') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vDatosEmpresa] AS
SELECT E.CODIGO, RTRIM(LTRIM(E.NOMBRE)) AS NOMBRE, RTRIM(LTRIM(E.NOMBRE2)) AS NOMBRE2, RTRIM(LTRIM(E.CIF)) AS CIF, 
RTRIM(LTRIM(E.DIRECCION)) AS DIRECCION, RTRIM(LTRIM(E.CODPOS)) AS CODPOS, RTRIM(LTRIM(E.POBLACION)) AS POBLACION, 
RTRIM(LTRIM(E.PROVINCIA)) AS PROVINCIA, RTRIM(LTRIM(E.TELEFONO)) AS TELEFONO, RTRIM(LTRIM(E.FAX)) AS FAX, 
RTRIM(LTRIM(E.MOBIL)) AS MOBIL, RTRIM(LTRIM(E.EMAIL)) AS EMAIL, RTRIM(LTRIM(E.[HTTP])) AS WEB, 
RTRIM(LTRIM(E.TXTFACTU1)) AS TXTFACTU1, RTRIM(LTRIM(E.TXTFACTU2)) AS TXTFACTU2, e.logo, e.almacen, f.tarifapret, E.LETRA
FROM ['+@GESTION+'].DBO.EMPRESA E  
LEFT JOIN ['+@GESTION+'].DBO.FACTUCNF F ON F.EMPRESA=E.CODIGO
')
select 'vDatosEmpresa'




IF EXISTS (SELECT * FROM sys.objects where name='vEnvioeti_Rep') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vEnvioeti_Rep] as
SELECT cav.empresa, emp.nombre as n_empresa, cav.numero, cav.letra, cav.fecha, cav.ruta, coalesce(rut.nombre,'''''''') as n_ruta
, cav.cliente, cli.nombre as n_cliente, coalesce(env.DIRECCiON,'''') as direccion, coalesce(env.CODPOS,'''') as codpos 
, coalesce(env.POBLACION,'''') as poblacion, coalesce(env.PROVINCIA,'''') as provincia, convert(int,coalesce(eti.bultos,0)) as bultos
FROM ['+@GESTION+'].[dbo].[c_albven] cav 
inner join ['+@GESTION+'].dbo.empresa emp on emp.codigo=cav.empresa
inner join ['+@GESTION+'].dbo.clientes cli on cli.codigo=cav.cliente
left  join ['+@GESTION+'].dbo.rutas rut on rut.codigo=cav.ruta
left  join ['+@GESTION+'].dbo.env_cli env on env.cliente=cav.cliente and env.LINEA=cav.ENV_CLI 
left  join ['+@GESTION+'].dbo.envioeti eti on eti.empresa=cav.empresa and eti.numero=cav.numero and eti.letra=cav.letra
')
select 'vEnvioeti_Rep'




IF EXISTS (SELECT * FROM sys.objects where name='vErrorLotesTrasTemp') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vErrorLotesTrasTemp] as
SELECT T.empresa, T.numero, T.letra, T.ARTICULO , art.NOMBRE
FROM Traspaso_Temporal T 
INNER JOIN ['+@LOTES+'].dbo.artlot A ON A.ARTICULO collate Modern_Spanish_CI_AI=T.articulo 
LEFT JOIN ['+@GESTION+'].dbo.articulo art on art.CODIGO collate Modern_Spanish_CI_AI=T.articulo
WHERE A.lote =1 AND T.lote collate Modern_Spanish_CI_AI=''''
')
select 'vErrorLotesTrasTemp'




IF EXISTS (SELECT * FROM sys.objects where name='vFamilias') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vFamilias] as
SELECT codigo,nombre from ['+@GESTION+'].[dbo].familias
')
select 'vFamilias'




IF EXISTS (SELECT * FROM sys.objects where name='vIVA') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vIVA] AS
SELECT '''+@EJERCICIO+'''+empresa+CAST(CUENTA AS VARCHAR)+replace(NUMFRA,space(1),''0'') AS IDIVA, 
'''+@EJERCICIO+''' AS EJER, EMPRESA, CUENTA, FECHA,
'''+@EJERCICIO+'''+EMPRESA+replace(CAST(NUMFRA AS VARCHAR(10)),space(1), ''0'') COLLATE Modern_Spanish_CI_AI AS IDFACTURA, 
NUMFRA AS NUMFRA, SUM(BIMPO) AS BASE, PORCEN_IVA, SUM(IVA) AS IMP_IVA, PORCEN_REC,  SUM(RECARGO) AS IMP_RECARGO,
SUM(BIMPO)+SUM(IVA)+SUM(RECARGO) AS TOTAL, tipo_iva
FROM ['+@GESTION+'].DBO.IVAREPER
WHERE LEFT(CUENTA,3)=''430''
group by EMPRESA, CUENTA, FECHA, NUMFRA, PORCEN_IVA, PORCEN_REC, tipo_iva
')
select 'vIVA'




IF EXISTS (SELECT * FROM sys.objects where name='vLotesDelArticulo') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vLotesDelArticulo] as
select	a.almacen, a.articulo, a.caducidad, a.color, a.empresa, a.lote, a.peso, a.talla, a.ubica, a.unidades,
b.peso as pesoArticulo, b.unicaja as unidadesOcajas
from ['+@LOTES+'].[dbo].[stocklotes] a
left join ['+@GESTION+'].[dbo].articulo b on b.CODIGO=a.ARTICULO
where unidades>0
')
select 'vLotesDelArticulo'




IF EXISTS (SELECT * FROM sys.objects where name='vMarcas') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vMarcas]
as
SELECT CODIGO, NOMBRE, DESCUEN, TCP, MARGEN, FOTO from ['+@GESTION+'].[dbo].marcas
')
select 'vMarcas'




IF EXISTS (SELECT * FROM sys.objects where name='vOfertas') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vOfertas]
AS
SELECT REPLACE(TIPO+CLIENTE+ARTICULO+color+TALLA+FAMILIA+SUBFAMILIA+STR(LINEA,5),SPACE(1),'''') AS IDOFERTA,TIPO, CLIENTE, ARTICULO, color, TALLA, FAMILIA, SUBFAMILIA, MARCA, LINDES, LINEA, FECHA_IN, FECHA_FIN, UNI_INI, UNI_FIN, PVP, DTO1, DTO2, MONEDA, TARIFA
FROM (
SELECT ''CLIENTE'' AS TIPO, CLIENTE, ARTICULO, color, TALLA, FAMILIA, SUBFAMILIA, MARCA, '''' AS LINDES, LINIA AS LINEA, FECHA_IN, FECHA_FIN, UNI_MIN AS UNI_INI, (CASE WHEN UNI_MAX=0 THEN 999999.99 ELSE UNI_MAX END) AS UNI_FIN, PVP, DTO1, DTO2, MONEDA, '''' AS TARIFA 
FROM ['+@GESTION+'].DBO.DESCUEN
UNION 
SELECT ''ARTICULO'' AS TIPO, '''' AS CLIENTE, ARTICULO, color, talla, '''' AS FAMILIA, '''' AS SUBFAMILIA, '''' AS MARCA, '''' AS  LINDES, LINEA, FECHA_IN, FECHA_FIN , DESDE AS UNI_INI, (CASE WHEN HASTA=0 THEN 999999.99 ELSE HASTA END) AS UD_FIN, PVP, DESCUENTO AS DTO1, 0 AS DTO2, MONEDA, TARIFA FROM ['+@GESTION+'].DBO.OFERTAS
UNION 
SELECT ''LINDESC'' AS TIPO, C.CODIGO AS CLIENTE, D.ARTICULO, SPACE(6) AS COLOR, SPACE(8) AS TALLA, D.FAMILIA, D.SUBFAMILIA, D.MARCA, D.CODIGO AS LINDES, D.LINEA, D.FECHA_IN, D.FECHA_FIN, D.UNI_INI, D.UNI_FIN, D.PVP, D.DTO1, D.DTO2, D.MONEDA, '''' AS TARIFA FROM ['+@GESTION+'].DBO.CLIENTES C INNER JOIN ['+@GESTION+'].DBO.LIN_DESC D ON D.CODIGO=C.LIN_DES
) O 
')
select 'vOfertas'




IF EXISTS (SELECT * FROM sys.objects where name='vPedidos_Pie') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vPedidos_Pie] AS
SELECT 
'''+@EJERCICIO+''' as EJER,
('''+@EJERCICIO+'''+CAV.empresa+replace(CAV.LETRA,space(1),''0'')+replace(LEFT(CAV.NUMERO,10),space(1),''0'')) collate Modern_Spanish_CI_AI as IDPEDIDO,
CAV.EMPRESA, CAV.NUMERO, CAV.LETRA, CAV.CLIENTE, IVA.IVA as IVApc, 
case when cli.recargo=1 then IVA.RECARG else 0 end as recargoPC, 
ISNULL(CAV.ENTREGA,0.00) as ENTREGA,
CAV.PRONTO as pPago,

dav.tipo_iva, iva.IVA as porcen_iva, iva.iva,
CASE WHEN CLI.RECARGO=1 THEN IVA.RECARG ELSE 0.00 END AS TIPO_RE, SUM(DAV.PVERDE) AS PVERDE,

SUM(CASE WHEN DAV.IVA_INC=1 THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE 
	ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE) AS IMPORTE,

ROUND(SUM((CASE WHEN DAV.IVA_INC=1 THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE
	ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE)*(IVA.IVA/100)),2) AS IVAimp,

coalesce(CASE WHEN CLI.RECARGO=1 THEN round(SUM((CASE WHEN DAV.IVA_INC=1 THEN DAV.IMPORTE*(1-(IVA.RECARG/100)) 
	ELSE CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE 
		ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE)*(IVA.RECARG/100)),2) END, 0.00) AS RECARGO,

(CAV.TOTALPED * CAV.PRONTO) / 100 as impPP,
isnull(ent.IMPORTE,0.00) as entIMP,

-- TOTALDOC (lo calculamos por si hay más de un IVA en el documento)
SUM(CASE WHEN DAV.IVA_INC=1 THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE 
	ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE)+ROUND(SUM((CASE WHEN DAV.IVA_INC=1 THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE
	ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE)*(IVA.IVA/100)),2)+coalesce(CASE WHEN CLI.RECARGO=1 THEN round(SUM((CASE WHEN DAV.IVA_INC=1			THEN CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE 
		ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE)*(IVA.RECARG/100)),2) END, 0.00) as TOTALDOC,

CAV.TOTALPED,
isnull(CAV.PRONTO,0.00) as PRONTO,

isnull((select max(isnull(cast(PVERDE as int),0)) from ['+@GESTION+'].dbo.articulo where CODIGO in
(select ARTICULO from ['+@GESTION+'].DBO.D_PEDIVE where EMPRESA+NUMERO+LETRA=CAV.EMPRESA+CAV.NUMERO+CAV.LETRA and PVERDE>0)
),0) as PVERDEver

FROM ['+@GESTION+'].DBO.C_PEDIVE CAV 
INNER JOIN 
(select EMPRESA, NUMERO, LETRA, ARTICULO, IMPORTE, TIPO_iva, 1 AS INC_PP, 0 AS IVA_INC, PVERDE 
from ['+@GESTION+'].dbo.D_PEDIVE where TIPO_IVA<>'''') 
DAV ON DAV.EMPRESA=CAV.EMPRESA AND DAV.NUMERO=CAV.NUMERO AND DAV.LETRA=CAV.LETRA 
LEFT JOIN ['+@GESTION+'].dbo.tipo_iva IVA ON IVA.CODIGO=DAV.TIPO_IVA
INNER JOIN ['+@GESTION+'].dbo.clientes CLI ON CLI.CODIGO=CAV.CLIENTE
left join ['+@GESTION+'].dbo.entre_pv ent on ent.NUMERO=CAV.NUMERO and ent.EMPRESA=CAV.EMPRESA and ent.LETRA=CAV.letra
GROUP BY CAV.EMPRESA, CAV.NUMERO, CAV.LETRA, CAV.CLIENTE, CLI.RECARGO, IVA.IVA, IVA.RECARG, CAV.ENTREGA, ent.IMPORTE
	,CAV.TOTALDOC,CAV.PRONTO,CAV.TOTALPED,DAV.TIPO_IVA,CAV.PRONTO
')
select 'vPedidos_Pie'




IF EXISTS (SELECT * FROM sys.objects where name='vPedidos') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vPedidos] AS
SELECT  1 as Nodo
	, cast('''' as varchar(50)) as MODO
	, CONCAT('''+@EJERCICIO+''',CAV.empresa,replace(CAV.LETRA,space(1),''0''),replace(LEFT(CAV.NUMERO,10),space(1),''0'')) as IDPEDIDO	
	, '''+@EJERCICIO+''' as EJER , CAV.EMPRESA, convert(varchar(10), CAV.ENTREGA, 103) as ENTREGA
	, CAV.LETRA + CAV.NUMERO as PEDIDO , CAV.NUMERO as numero, CAV.LETRA, CAV.FECHA as sqlFecha
	, convert(varchar(10), CAV.FECHA, 103) as FECHA
	, CAV.CLIENTE, CAV.REFERCLI, CAV.ENV_CLI as DIRECCION, CAV.CLIENTE as codCliente 			
	, CAV.USUARIO, CAV.pronto, CAV.VENDEDOR
	, ISNULL(CAST(CAV.OBSERVACIO AS VARCHAR(max)),'''') AS OBSERVACIO
	, env.DIRECCION as nDireccion, ven.nombre as nVendedor
	, cli.nombre collate Modern_Spanish_CI_AI as nCliente, cli.RUTA
	, env.CODPOS+'' ''+env.POBLACION as Ciudad, env.PROVINCIA as Provincia
	, DAV.PRESUP as presupuesto, '''+@EJERCICIO+''' + CAV.empresa + CAV.letra + DAV.PRESUP as idpresuven
	, case when TRASPASADO=1 then ''TRASPASADO''
			when FINALIZADO=1 then ''FINALIZADO''
			when CANCELADO =1 then ''CANCELADO'' 
			else ''PENDIENTE'' END 
			as ESTADO			  
	, PCN.Contacto as contacto, CON.Persona  as nContacto
	, ser.nombre as laSerie		
	, CPA.FAMILIA as EWFAMILIA, FAM.NOMBRE as FAMILIA
	, sum(isnull(pie.TOTALDOC,0.00)) as TOTALDOC
	, case when sum(pie.TOTALDOC)>0 then 
		replace(replace(replace(convert(varchar, cast(sum(isnull(pie.TOTALDOC,0)) as money),1),''.'',''_''),'','',''.''),''_'','','') +'' €''
		else '''' end as TOTALDOCformato
	,CAV.TOTALDOC-CAV.TOTALPED AS importeIVA, CAV.TOTALDOC as totalConIVA
	,cast(0.00 as numeriC(18,2)) AS TOTAL, CAST(ROUND(CAV.TOTALPED,2) AS NUMERIC(18,2)) AS TOTALPED, cast(0.00 as numeriC(18,2)) as IVA

FROM ['+@GESTION+'].dbo.c_pedive CAV
INNER JOIN ['+@GESTION+'].dbo.clientes CLI ON CLI.CODIGO=CAV.CLIENTE
left join  ['+@GESTION+'].dbo.entre_pv ent on ent.NUMERO=CAV.NUMERO and ent.EMPRESA=CAV.EMPRESA and ent.LETRA=CAV.letra
left join  ['+@GESTION+'].dbo.env_cli env on env.cliente=cav.cliente and env.linea=cav.env_cli
left join  ['+@GESTION+'].dbo.vendedor ven on ven.codigo=cav.vendedor
LEFT JOIN (SELECT EMPRESA, NUMERO, LETRA, LEFT(MAX(DOC_NUM),10) AS PRESUP 
		FROM ['+@GESTION+'].DBO.D_PEDIVE 
		GROUP BY EMPRESA, NUMERO, LETRA) DAV 
		ON DAV.EMPRESA=CAV.EMPRESA AND DAV.LETRA=CAV.LETRA AND DAV.NUMERO=CAV.NUMERO
LEFT JOIN Pedidos_Contactos PCN ON PCN.IDPEDIDO COLLATE Modern_Spanish_CI_AI='''+@EJERCICIO+'''+CAV.LETRA+CAV.NUMERO
LEFT JOIN ['+@GESTION+'].DBO.CONT_CLI CON ON CON.CLIENTE=CAV.CLIENTE AND CON.LINEA=PCN.CONTACTO
LEFT JOIN Pedidos_Familias CPA ON CPA.EJERCICIO='''+@EJERCICIO+''' COLLATE Modern_Spanish_CI_AI
		AND CPA.NUMERO COLLATE Modern_Spanish_CI_AI=CAV.NUMERO AND CPA.LETRA COLLATE Modern_Spanish_CI_AI=CAV.LETRA
LEFT JOIN ['+@GESTION+'].DBO.FAMILIAS FAM ON FAM.CODIGO=CPA.FAMILIA COLLATE Modern_Spanish_CI_AI
LEFT JOIN vSeries ser on ser.codigo=CAV.LETRA
LEFT JOIN vPedidos_Pie pie on pie.IDPEDIDO collate Modern_Spanish_CI_AI=CONCAT('''+@EJERCICIO+''',CAV.empresa,replace(CAV.LETRA,space(1),''0''),replace(LEFT(CAV.NUMERO,10),space(1),''0''))
	
GROUP BY  CAV.empresa, CAV.LETRA, CAV.NUMERO, CAV.EMPRESA, CAV.ENTREGA, CAV.FECHA
	, CAV.CLIENTE, CAV.REFERCLI, CAV.ENV_CLI, CAV.CLIENTE	
	, CAV.USUARIO, CAV.pronto, CAV.VENDEDOR
	, env.DIRECCION, ven.nombre, CAST(CAV.OBSERVACIO AS VARCHAR(max))
	, cli.nombre collate Modern_Spanish_CI_AI
	, cli.RUTA, env.CODPOS, env.POBLACION, env.PROVINCIA, DAV.PRESUP, TRASPASADO, FINALIZADO, CANCELADO, PCN.Contacto
	, CON.Persona, ser.nombre, CPA.FAMILIA, FAM.NOMBRE,  CAV.TOTALDOC, CAV.TOTALPED	
')
select 'vPedidos'




IF EXISTS (SELECT * FROM sys.objects where name='vPedidos_Detalle') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vPedidos_Detalle] as
SELECT '''+@EJERCICIO+''' + D.empresa + cast(D.letra as char(2)) + D.numero + CAST(D.linia as varchar) AS IDPEDIDOLIN, 
CONCAT('''+@EJERCICIO+''',D.empresa,replace(D.LETRA,space(1),''0''),replace(LEFT(D.NUMERO,10),space(1),''0'')) AS IDPEDIDO, 
'''+@EJERCICIO+''' AS EJER, cast(D.letra as char(2))+d.numero as PEDIDO, D.CLIENTE, D.ARTICULO, D.DEFINICION, D.UNIDADES, D.cajas, 
isnull(D.PRECIO,0.00) as PRECIO,  
case when D.PRECIO>0 then 
replace(replace(replace(convert(varchar, cast(D.PRECIO as money),1),''.'',''_''),'','',''.''),''_'','','')+'' €'' else '''' end as PRECIOf, 
isnull(D.importe,0.00) as IMPORTE, 
case when D.importe>0 then 
replace(replace(replace(convert(varchar, cast(D.importe as money),1),''.'',''_''),'','',''.''),''_'','','')+'' €'' else '''' end as IMPORTEf,  
D.TIPO_IVA, COALESCE(iva.iva, 0) AS TPCIVA,  
CAST(ROUND(D.IMPORTE*COALESCE(iva.iva, 0)*0.01,2) AS NUMERIC(18,2)) AS IVA, D.SERVIDAS, D.LINIA, D.DTO1, D.DTO2, D.EMPRESA, 
D.importeiva AS IMPORTEIVA, 
isnull(D.PESO,0.00) as PESO, 
case when D.PESO>0 then 
replace(replace(replace(convert(varchar, cast(D.PESO as money),1),''.'',''_''),'','',''.''),''_'','','')+'' €'' else '''' end as PESOf,
D.NUMERO, cast(D.letra as char(2)) as LETRA,
art.UNICAJA, D.PVERDE,
case when D.PVERDE>0 then 
replace(replace(replace(convert(varchar, cast(D.PVERDE as money),1),''.'',''_''),'','',''.''),''_'','','')+'' €'' else '''' end as PVERDEf,

(select max(isnull(cast(PVERDE as int),0)) from ['+@GESTION+'].dbo.articulo where CODIGO in
(select CODIGO from ['+@GESTION+'].DBO.D_PEDIVE where EMPRESA+NUMERO+LETRA=D.EMPRESA+D.NUMERO+D.LETRA)
) as PVERDEver

FROM ['+@GESTION+'].DBO.D_PEDIVE D
LEFT JOIN ['+@GESTION+'].dbo.tipo_iva iva ON D.tipo_iva=iva.codigo
LEFT JOIN ['+@GESTION+'].dbo.articulo art ON art.CODIGO=D.ARTICULO
WHERE LEFT(D.CLIENTE,3)=''430''
')
select 'vPedidos_Detalle'




IF EXISTS (SELECT * FROM sys.objects where name='vPers_ContadorCajasAlbaran') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vPers_ContadorCajasAlbaran] as
select IDALBARAN, sum(cajas) as cajas from vAlbaranes_Detalle
group by IDALBARAN
')
select 'vPers_ContadorCajasAlbaran'




IF EXISTS (SELECT * FROM sys.objects where name='vReport_Pedidos') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vReport_Pedidos] as
SELECT "vClientes"."CP", "vClientes"."POBLACION", "vClientes"."PROVINCIA", "vClientes"."CIF", "vClientes"."DIRECCION", 
"vDatosEmpresa"."NOMBRE" as NombreEmpresa, "vDatosEmpresa"."DIRECCION" as DireccionEmpresa, "vDatosEmpresa"."CODPOS", 
"vDatosEmpresa"."POBLACION" as PoblacionEmpresa, "vDatosEmpresa"."PROVINCIA" as ProvinciaEmpresa, "vDatosEmpresa"."CIF" CifEmpresa, 
"vDatosEmpresa"."TXTFACTU1", "vDatosEmpresa"."TXTFACTU2", "vDatosEmpresa"."TELEFONO", "vDatosEmpresa"."FAX", "vDatosEmpresa"."WEB", 
"vClientes"."NOMBRE", "vPedidos"."CLIENTE", "vPedidos_Detalle"."IMPORTE", coalesce("vPedidos_Detalle"."DTO2",0) as DTO2, coalesce("vPedidos_Detalle"."DTO1",0) as DTO1, 
"vPedidos_Detalle"."DEFINICION", "vPedidos_Detalle"."UNIDADES", "vPedidos_Detalle"."PRECIO", "vPedidos".totalConIVA, "vPedidos"."TOTAL", 
"vPedidos".importeIVA, "vPedidos"."TOTALPED", "vClientes"."BANCO", "vClientes"."N_PAG", "vPedidos".numero, "vPedidos".FECHA, 
"vPedidos_Detalle".ARTICULO, "vPedidos".IDPEDIDO, "vPedidos".IVA, vPedidos_Detalle.TIPO_IVA, coalesce("vPedidos_Detalle".peso,0.00) as peso
, coalesce(vPedidos_Detalle.cajas, 0.00) as cajas, coalesce(vPedidos_Detalle.unicaja,0.00) as unicaja
FROM   (("vPedidos" "vPedidos" 
LEFT OUTER JOIN "vPedidos_Detalle" "vPedidos_Detalle" ON ("vPedidos"."IDPEDIDO"  COLLATE Modern_Spanish_CI_AI ="vPedidos_Detalle"."IDPEDIDO") 
				AND ("vPedidos"."EJER"="vPedidos_Detalle"."EJER")) 
LEFT OUTER JOIN "vDatosEmpresa" "vDatosEmpresa" ON "vPedidos"."EMPRESA"="vDatosEmpresa"."CODIGO") 
LEFT OUTER JOIN "vClientes" "vClientes" ON "vPedidos"."CLIENTE"="vClientes"."CODIGO"
')
select 'vReport_Pedidos'




IF EXISTS (SELECT * FROM sys.objects where name='vPers_ContadorPeso') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vPers_ContadorPeso] as
select idPedido, sum(peso) as peso from vReport_Pedidos
group by IdPedido
')
select 'vPers_ContadorPeso'




IF EXISTS (SELECT * FROM sys.objects where name='vPers_ContadorPesoAlbaran') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vPers_ContadorPesoAlbaran] as
select IDALBARAN, sum(PESO) as peso from vAlbaranes_Detalle
group by IDALBARAN
')
select 'vPers_ContadorPesoAlbaran'




IF EXISTS (SELECT * FROM sys.objects where name='vPvP') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vPvP]
as
SELECT tarifa+articulo as idpvp, articulo, tarifa, pvp, pvpiva
FROM ['+@GESTION+'].dbo.pvp
')
select 'vPvP'




IF EXISTS (SELECT * FROM sys.objects where name='vRutas') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +' view [dbo].[vRutas] as
select	codigo, nombre from ['+@GESTION+'].dbo.rutas
')
select 'vRutas'




IF EXISTS (SELECT * FROM sys.objects where name='vSTOCK_LOTES') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vSTOCK_LOTES] as
(
SELECT TOP 100 PERCENT A.CODIGO, A.NOMBRE, COALESCE(MOV.EMPRESA,''01'') AS EMPRESA, COALESCE(MOV.ALMACEN,'''') AS ALMACEN, COALESCE(MOV.TIPO,'''') AS TIPOMOV, COALESCE(MOV.LOTE,''00000000'') AS LOTE, COALESCE(MOV.UNIDADES,0) AS UNIDADES, COALESCE(MOV.PESO,0) AS PESO , MOV.FECHASTOCK, MOV.CADUCIDAD
FROM ['+@GESTION+'].DBO.ARTICULO A 
LEFT JOIN (
SELECT  ''IN'' AS TIPO, I.EMPRESA, I.ALMACEN, I.ARTICULO, I.LOTE, I.UNIDADES-I.DEPOVEN+I.DEPOCOM AS UNIDADES, I.PESO-I.PESOVEN+I.PESOCOM AS PESO, I.FECHA AS FECHASTOCK, I.CADUCIDAD FROM ['+@LOTES+'].DBO.LTREGUL I INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=I.ARTICULO AND LOT.LOTE=1 WHERE I.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND I.FECHA=(SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=I.EMPRESA AND ALMACEN=I.ALMACEN AND ARTICULO=I.ARTICULO AND LOTE=I.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE)
UNION ALL
SELECT  ''SI'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, (L.UNIDADES-L.DEPOVEN+L.DEPOCOM) AS UNIDADES, L.PESO-L.PESOVEN+L.PESOCOM AS PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTSTINI L  INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL 
SELECT ''TO'' AS TIPO, L.EMPRESA, L.ALMORIG, L.ARTICULO, L.LOTE AS LOTE, -L.UNIDADES, -L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTALBTR L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMORIG AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
		
SELECT ''TD'' AS TIPO, L.EMPRESA, L.ALMDEST, L.ARTICULO, L.LOTE AS LOTE, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTALBTR L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMDEST AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL 
SELECT ''AV'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, -(L.UNIDADES) as UNIDADES, -(L.PESO) AS PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD FROM ['+@LOTES+'].DBO.LTALBVE L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT ''AC'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD FROM ['+@LOTES+'].DBO.LTALBCO L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT  ''CP'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTCPROD L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT  ''DP'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, -L.UNIDADES, -L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTDPROD L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT  ''CT'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, -L.UNIDADES, -L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD FROM ['+@LOTES+'].DBO.LTCTRAN L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
		
UNION ALL
SELECT  ''DT'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD FROM ['+@LOTES+'].DBO.LTDTRAN L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT ''AR'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, -L.UNIDADES, -L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTALBRE L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT  ''DV'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, -L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTDEPVE L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT  ''DC'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTDEPCO L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
) MOV ON A.CODIGO=MOV.ARTICULO AND MOV.EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
ORDER BY 1,4
)
')
select 'vSTOCK_LOTES'




IF EXISTS (SELECT * FROM sys.objects where name='vStockLOTE') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vStockLOTE] as
SELECT     EMPRESA, CODIGO AS ARTICULO, ALMACEN, LOTE, SUM(COALESCE(UNIDADES,0)) AS UNIDADES, SUM(COALESCE(PESO,0)) AS PESO
, MAX(CADUCIDAD) AS CADUCIDAD
FROM  dbo.vSTOCK_LOTES /*WHERE CODIGO=''V338''*/
where EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
GROUP BY EMPRESA, CODIGO, ALMACEN, LOTE
HAVING SUM(COALESCE(UNIDADES,0))<>0
')
select 'vStockLOTE'




IF EXISTS (SELECT * FROM sys.objects where name='vZonas') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vZonas] as
select M.CODIGO, M.NOMBRE, V.VALOR 
from ['+@GESTION+'] .dbo.mcampos m 
inner join ['+@GESTION+'].dbo.valmulti v on v.codigo=m.codigo where m.codigo=''ZNA''
')
select 'vZonas'




IF EXISTS (SELECT * FROM sys.objects where name='vImpresion') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view vImpresion
as
select distinct tipo, ltrim(rtrim(crNumero)) as crNumero, crEmpresa, crLetra 
from MI_ImpresionPDF
where fechaInsertUpdate>dateadd(week,-1,getdate())
')
select 'vImpresion'




	RETURN -1
END TRY

BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Modificando Procedimiento [dbo].[pArticulos]...';


GO
ALTER PROCEDURE [dbo].[pArticulos] (@parametros varchar(max))
AS
SET NOCOUNT ON
BEGIN TRY	
	declare @modo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
		,	@almacen varchar(5) = isnull((select JSON_VALUE(@parametros,'$.almacen')),'')
		,	@error   varchar(max) = ''
		,	@datos   varchar(max) = ''

	if @modo='lista' BEGIN
		set @datos =	( select * from vArticulosStock where almacen=@almacen for JSON AUTO,INCLUDE_NULL_VALUES )
	END

	select (CONCAT('{"error":"',@error,'","datos":',@datos,'}')) as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
	select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Modificando Procedimiento [dbo].[pCambiarArticulos]...';


GO
ALTER PROCEDURE [dbo].[pCambiarArticulos] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON

	-- datos JSON ----------------------------------------------------------------------------------
	declare	  @modo varchar(50)		= isnull((select JSON_VALUE(@parametros,'$.modo')),'')
			, @pedido varchar(50)	= isnull((select JSON_VALUE(@parametros,'$.pedido')),'')
			, @serie varchar(10)	= isnull((select JSON_VALUE(@parametros,'$.serie')),'')
			, @articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
			, @Almacen varchar(50)  = isnull((select JSON_VALUE(@parametros,'$.almacen')),'')

	declare @usuario varchar(50)	= isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50)		= isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-------------------------------------------------------------------------------------------------

	declare @empresa char(2) = (select Empresa from Configuracion)
		,	@sql varchar(max) 
	

	set @sql = (
		SELECT articulo, definicion, cajas_pv, uds_pv, peso_pv, linia 
		FROM [dbo].[Detalle_Pedidos] 
		WHERE empresa=@empresa and letra=@serie and numero=@pedido AND UDS_TRASPASADAS=0 AND ARTICULO!=''
		order by linia asc 
		for JSON AUTO, INCLUDE_NULL_VALUES
	)


	if @modo='equivalencia' BEGIN
		set @sql = (SELECT ARTICULO, NOMBRE, stock FROM Articulos_Sustitucion WHERE art_orig=@articulo and almacenSTC=@Almacen for JSON AUTO, INCLUDE_NULL_VALUES)
	END

	select isnull((@sql),'[]') as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Modificando Procedimiento [dbo].[pComprobarCB]...';


GO
ALTER PROCEDURE [dbo].[pComprobarCB] (@parametros varchar(max))
AS
/*-----------------------------------------------------------------------------------
--#AUTHOR: JUAN CARLOS VILLALOBOS
--#NAME: pComprobarCB
--#CREATION: 06/05/2021
--#DESCRIPTION: Comprobación y análisis de un código de barras
--#PARAMETERS:	@parametros formato JSON (@codigoBarras)
--#CHANGES
-- 06/05/2021 - JUAN CARLOS VILLALOBOS Creación del procedimiento
-- 14/06/2021 - JUAN CARLOS VILLALOBOS Adaptacion al nuevo script
-- 07/10/2021 - Elías Jené - Si existe el identificador 37 obtenemos las unidades de los siguientes 8 dígitos
--						   - Tomamos 13 dígitos en lugar de 14 para encontrar el artículo
-- 14/12/2021 - Albert Rodríguez: Ajustar retorno datos.
-- 23/12/2021 - Elías Jené: Dinamización del procedimiento.
-------------------------------------------------------------------------------------

Pasos a seguir al leer un código de barras:

Comprobamos si el código EAN existe en la BBDD:
1.	No Existe
Mostramos el aviso “El código EAN leído no está asociado a ningún artículo”.
Dejamos el cursor en el campo BARRAS

2.	Existe
Comprobamos si el artículo asociado al código de barras está dentro del documento

	a.	No existe
	Mostramos el aviso “El artículo asociado al código EAN leído no está dentro del documento”.
	Dejamos el cursor en el campo BARRAS

	b.	Existe
	Obtenemos las unidades del código EAN.
	Primero revisaremos si el código leído tiene el prefijo 37, si no es así, obtendremos las unidades de la tabla Barras y en el caso de que no hayamos obtenido ningún valor aun, asignaremos un 1.
	Revisaremos si las unidades obtenidas son mayores a las unidades pendientes de la línea del documento (si hay varias líneas con el mismo código, sumaremos las unidades de estas líneas al comparar).

		i.	Unidades mayores
		Mostramos el aviso “Las unidades leídas, superan las unidades pendientes”.
		Dejamos el cursor en el campo BARRAS

		ii.	Unidades menores
		Comprobaremos si el artículo trabaja con lotes

Lógica:
1.	No lotes
Insertamos las unidades leídas en el campo correspondiente, si ya hay valores, los sumamos. 

2.	Lotes
	Funcionalidad Artículos Lotes 
	1.	Lote
		a.	Si no tenemos el lote, revisaremos si el ERP trabaja con lote automático
			i.	Lote Automático
				Asignaremos las unidades leídas al lote con la fecha de caducidad más cercana, si hay varios lotes con la misma caducidad, asignaremos las unidades al lote más reciente.
			ii.	No lote Automático
				Abriremos la pantalla de lotes.
				Si tenemos el lote, añadiremos las unidades al lote leído.
	2.	Peso
		a.	Si el artículo trabaja con peso, si hemos recibido el peso al leer el código EAN, insertaremos el lote con el peso obtenido. 
		b.	Si no tenemos el peso, se abrirá la pantalla de lotes, añadiremos las unidades al lote correspondiente a la espera de poner el peso.

=========================================================================================================================================================================

ARTICULO	CODIGO BARRAS										EAN					LOTE			PESO
MI16104		010040000007300210PI21000402						00400000073002		PI21000402	
MI16105		01984370019350041026007215220726					98437001935004		260072	
MI-24302	01184252140010603103000700152101151020392124		18425214001060		20392124		000700
MILP		0198411477904619152101213103000615103910			98411477904619		3910			000615
MILCP		01984370036042043102001062152105181019056001		98437003604204		19056001		001062
			01004000000108543103004800370000001010CA21000925	(Identificador 37 - los 8 siguientes dígitos son las unidades)
ET1085		0100400000010854310300480037000000101020211220430954
MIL			0178414620091583172202221020211220430955
MI-24302	01184252140010603103003000152101151020211220430956

[pComprobarCB] '{"sp":"pComprobarCB","CODBAR":"01984370036042043102001062152105181019056001"}'
*/

-----------------------------------------------------------------------------------------------
--	Parámetros JSON 
	declare @CODBAR VARCHAR(255) = isnull((select JSON_VALUE(@parametros,'$.CODBAR')),'')  
-----------------------------------------------------------------------------------------------
--	Variables de Configuración 
	declare @EMPRESA char(2), @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select  @EMPRESA=Empresa, @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
-----------------------------------------------------------------------------------------------
--	Variables TABLA
	declare @tbBarras TABLE (ARTICULO varchar(20), UNIDADES numeric(15,6), VENTA_CAJA int)
	declare @tbLote TABLE (LOTE bit)
	declare @tbArticulo TABLE (UNICAJA int)
-----------------------------------------------------------------------------------------------
-- Variables del procedimiento
DECLARE	@ARTICULO CHAR(50),
		@BARRAS CHAR(50),
		@UDS DECIMAL(16,4),
		@CAJAS DECIMAL(16,4),
		@PESO DECIMAL(16,4)=0.0000,
		@PESOBAR CHAR(6),
		@CADUCIDAD CHAR(8),
		@LOTE CHAR(50),
		@EXISTE BIT,
		@ARTLOT BIT, 
		@TIPOEAN CHAR(5), 
		@FALTACOD BIT,
		@UNICAJA numeric(15,6),
		@VENTA_CAJA smallint,
		@DECIMALES INT,
		@MISSATGE VARCHAR(255),
		@sql varchar(max),
		@respuesta varchar(max)
-----------------------------------------------------------------------------------------------

BEGIN TRY
	-- Comprobar si el codigo de barras leido ya está relacionado con algún artículo. 
	-- Esto se realiza por si el código leido es un EAN13, si es un GS1 nunca lo encontraremos.
	declare @registros int=0
	declare @tbReg TABLE (registros int)
	INSERT @tbReg EXEC('SELECT COUNT(ARTICULO) FROM ['+@GESTION+'].[DBO].BARRAS WHERE LEFT(BARRAS,13) = LEFT('''+@CODBAR+''',13)')	
	SELECT @registros=registros FROM @tbReg		
	delete @tbReg
	IF @registros>0	BEGIN
		SET @BARRAS = LEFT(@CODBAR,13)
		-- Si existe el codigo de barras, obtenemos el código de artículo y las unidades			
		INSERT @tbBarras EXEC('
			SELECT	ARTICULO
				,	UNIDADES 
				,	CASE WHEN coalesce(m2.valor,'''')='''' OR  coalesce(m2.valor,'''')=''.F.'' THEN 0 ELSE 1 END
			FROM ['+@GESTION+'].DBO.BARRAS 
			left join ['+@GESTION+'].dbo.multicam m2 on m2.codigo=ARTICULO and m2.campo=''SCA''
			WHERE LEFT(BARRAS,13)='''+@BARRAS+'''		
		')	
		SELECT @ARTICULO=ARTICULO, @UDS=UNIDADES, @VENTA_CAJA=VENTA_CAJA FROM @tbBarras		
		delete @tbBarras
		-- obtener LOTE			
		INSERT @tbLote EXEC('SELECT LOTE FROM ['+@LOTES+'].DBO.artlot WHERE ARTICULO='''+@ARTICULO+'''')	
		SELECT @ARTLOT=LOTE FROM @tbLote		
		delete @tbLote
		-- obtener UNICAJA del artículo			
		INSERT @tbArticulo EXEC('SELECT isnull(UNICAJA,0) FROM ['+@GESTION+'].DBO.ARTICULO WHERE CODIGO='''+@ARTICULO+'''')	
		SELECT @UNICAJA=UNICAJA FROM @tbArticulo		
		delete @tbArticulo

		SET @PESO=0.0000
		SET @CADUCIDAD=''
		SET @LOTE=''
		SET @EXISTE=1
		SET @TIPOEAN = 'EAN13'
		set @FALTACOD = 0
		SET @DECIMALES = 0
				
		-- Mensaje de retorno
		set @respuesta = CONCAT('{"Error":"","EXISTE":"',@EXISTE,'","ARTLOT":"',@ARTLOT,'","ARTICULO":"',ltrim(rtrim(@ARTICULO)),'","UNIDADES":"',@Uds,'"'
						,',"UNICAJA":"',@UNICAJA,'","PESO":"',@PESO,'","CADUCIDAD":"',@CADUCIDAD,'","LOTE":"',ltrim(rtrim(@LOTE)),'","VENTA_CAJA":"',@VENTA_CAJA,'"'
						,',"TIPOEAN":"',@TIPOEAN,'","FALTACOD":"',@FALTACOD,'","EAN":"',@CODBAR,'"}')
	END
	-- Si no encontramos el código, comprobamos si es un código GS1
	ELSE BEGIN   
		-- Si los dos primeros dígitos es 01 sabemos que los siguientes 14 dígitos  
		-- son el código de barras del artículo, el cual buscaremos en la tabla correspondiente.
		IF SUBSTRING(@CODBAR,1,2) = '01' BEGIN
			-- comprobar si el código de barras está dividido. Si lo está pedimos la segunda lectura.
			IF LEN(@CODBAR)<27 and SUBSTRING(@CODBAR,17,2) != '10' BEGIN set @respuesta = '{"Error":"CodigoDividido"}' return -1 END

			SET @BARRAS = SUBSTRING(@CODBAR,3,13)
			INSERT @tbReg EXEC('SELECT COUNT(ARTICULO) FROM ['+@GESTION+'].[DBO].BARRAS WHERE LEFT(BARRAS,13) = '''+@BARRAS+'''')	
			SELECT @registros=registros FROM @tbReg		
			delete @tbReg
			IF @registros>0 BEGIN
				-- Si existe el codigo de barras, obtenemos el código de artículo y las unidades
				INSERT @tbBarras EXEC('
					SELECT	ARTICULO
						,	UNIDADES 
						,	CASE WHEN coalesce(m2.valor,'''')='''' OR  coalesce(m2.valor,'''')=''.F.'' THEN 0 ELSE 1 END
					FROM ['+@GESTION+'].DBO.BARRAS 
					left join ['+@GESTION+'].dbo.multicam m2 on m2.codigo=ARTICULO and m2.campo=''SCA''
					WHERE LEFT(BARRAS,13)='''+@BARRAS+'''		
				')	
				SELECT @ARTICULO=ARTICULO, @UDS=UNIDADES, @VENTA_CAJA=VENTA_CAJA FROM @tbBarras		
				delete @tbBarras
				-- obtener LOTE
				INSERT @tbLote EXEC('SELECT LOTE FROM ['+@LOTES+'].DBO.artlot WHERE ARTICULO='''+@ARTICULO+'''')	
				SELECT @ARTLOT=LOTE FROM @tbLote		
				delete @tbLote
				-- obtener UNICAJA del artículo
				INSERT @tbArticulo EXEC('SELECT isnull(UNICAJA,0) FROM ['+@GESTION+'].DBO.ARTICULO WHERE CODIGO='''+@ARTICULO+'''')	
				SELECT @UNICAJA=UNICAJA FROM @tbArticulo		
				delete @tbArticulo

				SET @EXISTE=1
				SET @TIPOEAN = 'GS1'
				SET @FALTACOD = 0
				-- Obtenemos las unidades por caja y el peso por unidad para devolver el valor correspondiente 
				-- según las unidades del código de barras
				declare @tbArt2 TABLE (CAJAS numeric(15,6), PESO varchar(20))
				INSERT @tbArt2 EXEC('SELECT CASE WHEN UNICAJA=0 THEN 0 ELSE CONVERT(INT,1/UNICAJA) END as CAJAS
									, cast(cast(CASE WHEN ltrim(rtrim(PESO))='''' THEN ''0.00'' ELSE REPLACE(PESO,'','',''.'') END as numeric(15,6)) * '+@UDS+' as varchar(50)) as PESO
									FROM ['+@GESTION+'].DBO.ARTICULO WHERE CODIGO='''+@ARTICULO+'''')	
				SELECT @CAJAS=CAJAS, @PESO=PESO FROM @tbArt2		
				delete @tbArt2

				-- Si el siguiente tramos del código es 310, obtendremos el peso								
				IF SUBSTRING(@CODBAR,17,3) = '310' BEGIN
					SET @PESOBAR = SUBSTRING(@CODBAR,21,6)
					SET @DECIMALES = SUBSTRING(@CODBAR,20,1)
					-- Si el siguiente tramo del código es 15, obtendremos la caducidad
					IF SUBSTRING(@CODBAR,27,2) = '15' OR SUBSTRING(@CODBAR,27,2) = '17' BEGIN
						SET @CADUCIDAD = SUBSTRING(@CODBAR,29,6)
						-- Si el siguiente tramos del código es 10, obtendremos el lote
						IF SUBSTRING(@CODBAR,35,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,37,50)
					END
					-- Si el siguiente tramo del código es 37, obtendremos las unidades de la caja, las cuales substituirán las unidades del ERP				
					IF SUBSTRING(@CODBAR,27,2) = '37' BEGIN
						set @Uds=cast(SUBSTRING(@CODBAR,29,8) as decimal(16,4))						
						-- Si el siguiente tramos del código es 10, obtendremos el lote
						IF SUBSTRING(@CODBAR,37,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,39,50)
					END	
					-- Si el siguiente tramo del código es 10, obtendremos el lote
					IF SUBSTRING(@CODBAR,27,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,29,50)
					-- Si el siguiente tramo está vacío quiere decir que el código 
					-- está dividido en 2 y hay que leer la segunda parte.
					-- Cuando pasa esto el procedimiento devuelve un aviso para que la aplicación lea el resto 
					-- del código y vuelva a enviarlo uniendo las dos lecturas
					IF SUBSTRING(@CODBAR,27,2) = ' ' SET @FALTACOD = 1
				END			
				-- Si el siguiente tramo del código es 15 ó 17, obtendremos la caducidad
				IF SUBSTRING(@CODBAR,17,2) = '15' OR SUBSTRING(@CODBAR,17,2) = '17' BEGIN
					SET @CADUCIDAD = SUBSTRING(@CODBAR,19,6)
					-- Si el siguiente tramo del código es 310, obtendremos el peso
					IF SUBSTRING(@CODBAR,25,3) = '310' BEGIN
						SET @PESOBAR = SUBSTRING(@CODBAR,29,6)
						SET @DECIMALES = SUBSTRING(@CODBAR,28,1)
						-- Si el siguiente tramo del código es 10, obtendremos el lote
						IF SUBSTRING(@CODBAR,35,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,37,50)
					END
					-- Si el siguiente tramo del código es 10, obtendremos el lote
					IF SUBSTRING(@CODBAR,25,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,27,50)
					-- Si el siguiente tramo está vacío quiere decir que el código 
					-- está dividido en 2 y hay que leer la segunda parte.
					-- Cuando pasa esto el procedimiento devuelve un aviso para que la aplicación lea el resto 
					-- del código y vuelva a enviarlo uniendo las dos lecturas
					IF SUBSTRING(@CODBAR,25,2) = ' ' SET @FALTACOD = 1
				END
				-- Si el siguiente tramos del código es 10, obtendremos el lote.
				IF SUBSTRING(@CODBAR,17,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,19,50)

				IF @DECIMALES = 1 SET @DECIMALES = 10								
				IF @DECIMALES = 2 SET @DECIMALES = 100								
				IF @DECIMALES = 3 SET @DECIMALES = 1000							
				IF @PESOBAR!='000000' SET @PESO=CONVERT(DECIMAL(16,4),@PESOBAR)/@DECIMALES
				if @Uds=0 set @Uds=1

				set @respuesta = CONCAT('{"Error":"","EXISTE":"',@EXISTE,'","ARTLOT":"',@ARTLOT,'","ARTICULO":"',ltrim(rtrim(@ARTICULO)),'","UNIDADES":"',@Uds,'"'
								,',"UNICAJA":"',@UNICAJA,'","PESO":"',@PESO,'","CADUCIDAD":"',@CADUCIDAD,'","LOTE":"',ltrim(rtrim(@LOTE)),'","VENTA_CAJA":"',@VENTA_CAJA,'"'
								,',"TIPOEAN":"',@TIPOEAN,'","FALTACOD":"',@FALTACOD,'","EAN":"',@CODBAR,'"}')
			END ELSE BEGIN
				-- Si el código de barras no esta asociado a ningún artículo no continuamos.
				SET @MISSATGE='ERROR001 - El Código de Barras no está asociado a ningún artículo!'
				set @respuesta = '{"Error":"'+@MISSATGE+'"}'
			END						
		END
		-- Si hemos llegado aquí y el código no empieza por 01, no hacemos más comprobaciones. 
		-- Entendemos que el código no está asociado a ningún artículo.
		ELSE BEGIN
			SET @MISSATGE='ERROR001 - El Código de Barras no está asociado a ningún artículo!'
			set @respuesta = '{"Error":"'+@MISSATGE+'"}'
		END								
	END 
	SELECT @respuesta AS JAVASCRIPT
	RETURN -1
END TRY
BEGIN CATCH
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError=CONCAT(ERROR_MESSAGE(),ERROR_NUMBER(),ERROR_PROCEDURE(),@@PROCID ,ERROR_LINE())
	RAISERROR(@CatchError,12,1) 
	RETURN 0
END CATCH
GO
PRINT N'Modificando Procedimiento [dbo].[pSustituirArticulos]...';


GO
ALTER PROCEDURE [dbo].[pSustituirArticulos] @parametros varchar(max)
AS
BEGIN
	SET NOCOUNT ON
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare @articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
		,	@linea int = cast(isnull((select JSON_VALUE(@parametros,'$.linea')),'0') as int)
		,	@unidades int = cast(isnull((select JSON_VALUE(@parametros,'$.unidades')),'0') as int)
		,	@letra varchar(2) = isnull((select JSON_VALUE(@parametros,'$.letra')),'')
		,	@numero varchar(50) = isnull((select JSON_VALUE(@parametros,'$.numero')),'')

	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EMPRESA char(2), @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select  @EMPRESA=Empresa, @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------	
	-- Variables
	DECLARE @CONSULTA NVARCHAR(MAX)
	-------------------------------------------------------------------------------------------------


	-- obtener datos del artículo
	DECLARE @SQLString nvarchar(500);  
	DECLARE @ParamDefinition nvarchar(500);  
	DECLARE @NOMBRE nvarchar(75), @CAJAS int=0, @UNICAJA int, @COST_ULT1 numeric(15,6), @IMPORTE numeric(15,6), @PESO varchar(50)
	SET @SQLString = N'SELECT @NOMBRE=nombre, @UNICAJA=UNICAJA, @COST_ULT1=COST_ULT1, @PESO=PESO 
					 FROM ['+@GESTION+'].DBO.articulo WHERE CODIGO=@CODIGO';
	SET @ParamDefinition = N'@CODIGO varchar(20), @NOMBRE nvarchar(75) OUTPUT, @UNICAJA int OUTPUT, @COST_ULT1 int OUTPUT
						 , @PESO varchar(50) OUTPUT';
	EXECUTE sp_executesql @SQLString, @ParamDefinition, @CODIGO=@articulo, @NOMBRE=@NOMBRE OUTPUT, @UNICAJA=@UNICAJA OUTPUT
						, @COST_ULT1=@COST_ULT1 OUTPUT, @PESO=@PESO OUTPUT 

	if @UNICAJA>0 set @CAJAS=@unidades/@UNICAJA
	if isnull(@PESO,'0.000')='' set @PESO='0.000'

	-- actualizar linea de pedido
	SET @CONSULTA=CONCAT('UPDATE [',@GESTION,'].DBO.d_pedive 
		SET ARTICULO=''',@articulo,''', DEFINICION=''',@NOMBRE,''', PRECIO=''',@COST_ULT1,'''  
		, UNIDADES=',@unidades,', CAJAS=',@CAJAS,', PESO=',@PESO,'  
		WHERE EMPRESA=''',@EMPRESA,''' AND NUMERO=''',@NUMERO,''' AND LETRA=''',@LETRA,''' AND LINIA=',@linea)
	EXEC (@CONSULTA)

	
	-- recálculo de la linea
	declare @param varchar(max) = 
	CONCAT('{"numero":"',@numero,'","letra":"',@letra,'","articulo":"',@articulo,'","linea":"',@linea,'"'
	,',"uds":"',@unidades,'","peso":"',@PESO,'"}')

	EXEC [pCalculosPV] @param

	RETURN -1 
END
GO
PRINT N'Creando Procedimiento [dbo].[pCrearAV]...';


GO
CREATE PROCEDURE [dbo].[pCrearAV]
	@EMPRESA varchar(2),
	@PEDIDO varchar(20),
	@LETRA varchar(2),
	@TERMINAL char(50),
	@imprimir INT,
	@imp_eti int,
	@respuestaSP VARCHAR(20)='' output,
	@nEti int = 0

AS
-- ========================================================================
-- Author:		Albert Rodriguez
-- Create date: 04/09/18
-- Revision:	15/11/21
-- Description:	Creación de Cabecera de albarán de venta a partir de un ID
-- 				Procedimiento para enlace de web App 
-- Modifica:	Harold 10/09/2020
--				Harold 08/10/2020 Variable @totaldoc
--				Albert Variable @totaldoc
--				Albert 21/08/2021 Limpieza del procedimiento entero
--				Albert R. 15/11/21 Modificamos el procedimiento para que sea dinámico y no se tegna que modificar cada año.
-- ========================================================================

BEGIN TRY

	set nocount ON -- evitar mensajes de SQL

	DECLARE @Mensaje varchar(max), @cSQL NVARCHAR(max), @cSQL2 NVARCHAR(max), @cSQL3 NVARCHAR(max), @cSQL4 NVARCHAR(max), @cSQL5 NVARCHAR(max), @cSQL6 NVARCHAR(max), @cSQL7 NVARCHAR(max), @cSQL8 NVARCHAR(max), @cSQL9 NVARCHAR(max), @cSQL10 NVARCHAR(max), @cSQL11 NVARCHAR(max), @cSQL12 NVARCHAR(max), @cSQL13 NVARCHAR(max), @cSQL14 NVARCHAR(max), @GESTION CHAR(8), @EJERCICIO CHAR(4), @enuso  bit

	-- Marcamos en uso la tabla en EnUso para que no se pueda ejecutar el procedimiento CrearAV desde dos terminales hasta que el primero que lo ha ejecutado acabe.
	UPDATE dbo.EnUso set Enuso=1, fecha=getdate() WHERE TIPO='PROCEDURE'
	
	SELECT @GESTION=Gestion , @EJERCICIO=Ejercicio from Configuracion
		
	--15/10/2020 BEGIN TRAN crearAV SIRVE PARA QUE SI ALGUNA OPERACION FALLA SE DEVUELVAN TODOS LOS PROCESOS Y NO SE DAÑEN LOS DATOS.
	--BEGIN TRAN crearAV 
	
		DECLARE @LETRAFAC CHAR(2), @NUMERO VARCHAR(10)='', @NUMEROOUT VARCHAR(10)='', @NUMAV VARCHAR(10)='', @NUMAVOUT VARCHAR(10)='',@LINIA INT, @ParmDefinition nvarchar(max)

			-- Si no existe el pedido a traspasar en la tabla Temporal no continuamos con el procedimiento
		if not exists (select * FROM TRASPASO_TEMPORAL TT WHERE TT.EMPRESA=@EMPRESA AND TT.NUMERO=@PEDIDO AND TT.LETRA=@LETRA GROUP BY TT.EMPRESA, TT.NUMERO, TT.LETRA)
		begin 
			set @respuestaSP='No existe el Traspaso Temporal'
			raiserror ('Sale porque no existe el Traspaso Temporal',16,1);
			return 0
		end

		-- Albert Rodriguez 23/10/2021 - Si la serie es CO, al albaranar/facturar la cambiaremos por la serie DI (Pedido por Ignasi)
		IF @LETRA='CO'
			SET @LETRAFAC='DI'
		ELSE
			SET @LETRAFAC=@LETRA

		SET @NUMERO=''

		SET @cSQL = N'	SELECT @NUMAVOUT=NUMERO FROM ['+@GESTION+'].DBO.C_ALBVEN WHERE LETRA=''FR'' AND PEDIDO='''+@PEDIDO+@LETRAFAC+''' AND FACTURA='''' AND IMPRESO=0';
		SET @ParmDefinition = N'@NUMAVOUT VARCHAR(10) OUTPUT'; 
		EXECUTE sp_executesql @cSQL, @ParmDefinition, @NUMAVOUT=@NUMAV OUTPUT; 

		SET @NUMAV=COALESCE(@NUMAV,'')
		
		-- Si el numero de albarán no existe obtenemos un nuevo numero, si existe añadiremos las líneas.
		IF ISNULL(@NUMAV,'')=''
			BEGIN
				SET @cSQL = N'SET @NUMEROOUT=(SELECT SPACE(10-LEN(CAST(CONTADOR+1 AS VARCHAR(10))))+CAST(CONTADOR+1 AS VARCHAR(10)) 
				from ['+@GESTION+'].dbo.series 
					WHERE empresa='''+@EMPRESA+''' and serie='''+@LETRAFAC+''' and TIPODOC=1)
				WHILE (SELECT NUMERO FROM ['+@GESTION+'].DBO.C_ALBVEN WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO=@NUMEROOUT AND LETRA='''+@LETRAFAC+''') IS NOT NULL
				BEGIN
					SET @NUMEROOUT=@NUMEROOUT+1
				END';
				SET @ParmDefinition = N'@NUMEROOUT VARCHAR(10) OUTPUT'; 
				EXECUTE sp_executesql @cSQL, @ParmDefinition, @NUMEROOUT=@NUMERO OUTPUT; 

				set @cSQL ='UPDATE ['+@GESTION+'].dbo.series set CONTADOR='''+@NUMERO+''' where empresa='''+@EMPRESA+''' and serie='''+@LETRAFAC+''' and TIPODOC=1'
				exec(@cSQL)
			END
		ELSE
			BEGIN
				SET @NUMERO=@NUMAV
			END
		
		-- Añadimos los espacios delta del número de albarán
		SET @NUMERO = SPACE(10-LEN(LTRIM(RTRIM(@NUMERO))))+LTRIM(RTRIM(@NUMERO))

		--LOG1
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES ('Emp: '+ltrim(rtrim(@EMPRESA))+' / Numero: '+ltrim(Rtrim(@PEDIDO))+' / Letra: '+ltrim(Rtrim(@LETRA))+' / Log:1',GETDATE(), @EMPRESA, @PEDIDO, @LETRA)
		

		SET @cSQL = '	
		-- Declaración de variables	
		DECLARE	@USUARIO VARCHAR(25)='''', @FECHA VARCHAR(10), @CLIENTE VARCHAR(10), @CLIMANDATO VARCHAR(10), @ENV_CLI INT, 
		@PRONTO NUMERIC(20,2), @NCLIENTE VARCHAR(80), @VENDEDOR VARCHAR(2), @RUTA VARCHAR(2), @ALMACEN VARCHAR(2), @FACTURA VARCHAR(10), 
		@FECHA_FAC SMALLDATETIME, @ASI VARCHAR(20), @FPAG VARCHAR(2), @IMPORTE NUMERIC(15,6), @OBSERVACIO VARCHAR(250), @BANC_CLI INT, 
		@DIVISA VARCHAR(3), @CAMBIO NUMERIC(20,6), @IMPDIVISA NUMERIC(15,6), @FINAN NUMERIC(20,4), @VISTA BIT, @COSTE NUMERIC(20,6), 
		@PESO NUMERIC(20,4), @LITROS NUMERIC(20,4), @OBRA VARCHAR(5), @TRASPASADO BIT, @RECEQUIV BIT, @TAG BIT, @OPERARIO VARCHAR(2), 
		@FACTURABLE BIT, @COT_PUNT NUMERIC(20,4), @PUNTOS NUMERIC(20,4), @CLIFINAL VARCHAR(10), @IMPRESO BIT, @LIBRE_1 VARCHAR(10), 
		@LIBRE_2 VARCHAR(10),@LIBRE_3 VARCHAR(10), @CERTIFIC INT, @STOCK_COEF NUMERIC(4,0), @EDI BIT, @GASTOS BIT, @FECHASTOCK SMALLDATETIME, 
		@MANDATO VARCHAR(35), @RECC BIT, @TOTALDOC NUMERIC(15,6), @AGENCIA VARCHAR(3), @TIPOPORTE INT, @CONTADOR VARCHAR(5), @BULTOS VARCHAR(7), 
		@PESTOTAL NUMERIC(20,4), @PORTES VARCHAR(7), @CONDUCTO VARCHAR(30), @DIR_ENV VARCHAR(80), @CP_ENV VARCHAR(10), @POBL_ENV VARCHAR(30), 
		@PROV_ENV VARCHAR(30), @TELF_ENV VARCHAR(15), @TARIFA VARCHAR(2)='''', @REPARTIDOR VARCHAR(2), @IVAPORT varchar(2), @LINIA INT,
		@EJERCICIO varchar(4), @Factdirv BIT, @IVATOT NUMERIC(20,2), @SIREC bit=0, @IVAREC NUMERIC(20,2), @ETICAJAS VARCHAR(7),
		@PVERDECLI INT,  @FECHA_ENTREGA VARCHAR(10), @VALPORTES NUMERIC(20,3), @PORTCOMP NUMERIC(20,3), @IVAPORTES NUMERIC(20,3), @IMPPORTES NUMERIC(20,3),
		@RECPORTES NUMERIC(20,3), @RECARGO BIT, @NOPORTES BIT, @CONTADO BIT, @cSQL NVARCHAR(max), @TERMINAL_new char(50), @MensajeError varchar(max)
			
	
		
		-- Asignación de variables

		SET @MensajeError=''","ERROR":"''
		SET @EJERCICIO='''+@EJERCICIO+'''
		set @terminal_new='''+@TERMINAL+'''
		
		SELECT @BULTOS=MAX(isnull(BULTOS,''0'')), @ETICAJAS=MAX(isnull(ETICAJAS,''0'')), @PESTOTAL=SUM(CAST(CASE WHEN COALESCE(peso,''0.00'')='''' THEN ''0.00'' ELSE COALESCE(peso,''0.00'') END AS DECIMAL(15,4)))
		FROM TRASPASO_TEMPORAL 
		WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''' GROUP BY SESION+EMPRESA+NUMERO+LETRA
		
		SET @USUARIO=''MERLOS#API#''		
		'
		SET @cSQL2 = '		
		-- Albert R., si estamos a viernes y la fecha de entrega es lunes, la fecha de todos los documentos será sábado. Esto lo ha pedido Ignasi.
		SET @FECHA_ENTREGA=(SELECT CONVERT(CHAR(11), ENTREGA, 103) FROM ['+@GESTION+'].dbo.c_pedive WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''')
		IF datepart(weekday, getdate())=5 AND (datepart(weekday, @FECHA_ENTREGA)=1 or datepart(weekday, @FECHA_ENTREGA)=2 or datepart(weekday, @FECHA_ENTREGA)=3 or datepart(weekday, @FECHA_ENTREGA)=4)
			BEGIN
				SET @FECHA=CAST(CONVERT(char(11), getdate()+1-0.375, 103) as VARCHAR) 
				SET @FECHA_FAC=CAST(CONVERT(char(11), getdate()+1-0.375, 103) as VARCHAR)
				SET @FECHASTOCK=getdate()+1-0.375
			END
		ELSE
			BEGIN
				SET @FECHA=CAST(CONVERT(char(11), getdate()-0.375, 103) as VARCHAR) 
				SET @FECHA_FAC=CAST(CONVERT(char(11), getdate()-0.375, 103) as VARCHAR)
				SET @FECHASTOCK=getdate()-0.375
			END

		-- Serie ET, al generar el albarán que se genere con la fecha del día
		IF '''+@LETRA+''' = ''ET''
			SET @FECHA=CAST(CONVERT(char(11), getdate(), 103) as VARCHAR) 

		--LOG2
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:2'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL3 = '		
		-- Si la fecha es 31/12/XX la fecha de los albaranes y facturas será 02/01/ del siguiente año
		IF CAST(CONVERT(char(11), getdate()-0.375, 103) as VARCHAR) = ''31/12/2020''
			BEGIN
				SET @FECHA=CAST(CONVERT(char(11), getdate()+2-0.375, 103) as VARCHAR) 
				SET @FECHA_FAC=CAST(CONVERT(char(11), getdate()+2-0.375, 103) as VARCHAR)
				SET @FECHASTOCK=getdate()+2-0.375
			END


		-- Asignamos valor a las variables con los datos del pedido
		SELECT	@CLIENTE=C_PEDIVE.CLIENTE, @NCLIENTE=CLIENTES.NOMBRE, @ENV_CLI=case when C_PEDIVE.ENV_CLI=0 then clientes.env_cli else C_PEDIVE.ENV_CLI end, @PRONTO=C_PEDIVE.PRONTO, 
				@VENDEDOR=C_PEDIVE.VENDEDOR, @RUTA=C_PEDIVE.RUTA, @ALMACEN=C_PEDIVE.ALMACEN, @Factdirv=0, @CLIFINAL=CLIENTES.CLIFINAL, @FPAG=C_PEDIVE.FPAG,
				@IMPORTE=C_PEDIVE.TOTALPED, @OBSERVACIO=C_PEDIVE.OBSERVACIO, @BANC_CLI=BANC_CLI.CODIGO, @DIVISA=C_PEDIVE.DIVISA,
				@CAMBIO=C_PEDIVE.CAMBIO, @IMPDIVISA=C_PEDIVE.IMPDIVISA, @VISTA=C_PEDIVE.VISTA, @PESO=C_PEDIVE.PESO, @LITROS=C_PEDIVE.LITROS,
				@OBRA=C_PEDIVE.OBRA, @OPERARIO=C_PEDIVE.OPERARIO, @FACTURABLE=0,
				@AGENCIA=CLIENTES.AGENCIA, @TIPOPORTE=CASE WHEN CLIENTES.PORTES=''DEBIDOS'' THEN 1 ELSE 2 END,
				@PORTES=CLIENTES.PORTES, @VALPORTES=CLIENTES.VALPORTES, @PORTCOMP=CLIENTES.PORTCOMP, @REPARTIDOR='''', @TARIFA=CLIENTES.TARIFA, @PVERDECLI=CLIENTES.PVERDE,
				@RECARGO=CLIENTES.RECARGO, @NOPORTES=0 , @CONTADO=CLIENTES.CONTADO
		FROM ['+@GESTION+'].DBO.C_PEDIVE INNER JOIN ['+@GESTION+'].DBO.CLIENTES ON C_PEDIVE.CLIENTE=CLIENTES.CODIGO
				LEFT JOIN ['+@GESTION+'].DBO.BANC_CLI ON C_PEDIVE.CLIENTE=BANC_CLI.CLIENTE AND ORDEN=1
		WHERE C_PEDIVE.EMPRESA='''+@EMPRESA+''' AND C_PEDIVE.LETRA='''+@LETRA+''' AND C_PEDIVE.NUMERO='''+@PEDIDO+'''

		-- Asiganción de variables
		SELECT @FACTURA='''', @ASI=''''

		BEGIN
			IF @CLIFINAL=''''
				SET @CLIMANDATO=@CLIENTE
			ELSE
				SET @CLIMANDATO=@CLIFINAL
		END


		SELECT @IVAPORTES=IVA.IVA, @RECPORTES=CASE WHEN @RECARGO=1 THEN IVA.RECARG ELSE 0.00 END FROM ['+@GESTION+'].DBO.CODIGOS COD INNER JOIN ['+@GESTION+'].DBO.TIPO_IVA IVA ON IVA.CODIGO=COD.IVA_PORTES WHERE COD.EMPRESA='''+@EMPRESA+'''
		
		-- Asignación de variables 
		SELECT @FINAN=0.00, @COSTE=0.00, @TRASPASADO=0, @RECEQUIV=0,  @TAG=0, @COT_PUNT=0.00, @PUNTOS=0.00, @IMPRESO=0,
			@LIBRE_1='''', @LIBRE_2='''', @LIBRE_3='''', @CERTIFIC=0, @STOCK_COEF=1, @EDI=0, @GASTOS=1, @RECC=0

		--LOG3
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:3'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL4 = '		
		
		SET @MANDATO=COALESCE((SELECT MANDATO FROM [COMU0001].DBO.MANDATOS M 
			INNER JOIN ['+@GESTION+'].DBO.BANC_CLI B ON B.CLIENTE=M.CLIENTE AND B.IBAN+B.CUENTAIBAN=M.CLI_IBAN 
		WHERE M.CLIENTE=@CLIMANDATO AND M.DEFECTO=1 AND B.ORDEN=1),'''')

		SET @CONTADOR=(SELECT ETIQUETA+1 FROM ['+@GESTION+'].DBO.CONTADOR WHERE EMPRESA='''+@EMPRESA+''')
		UPDATE ['+@GESTION+'].DBO.CONTADOR SET ETIQUETA=ETIQUETA+1  WHERE EMPRESA='''+@EMPRESA+'''
		IF NOT EXISTS (SELECT NOMBRE FROM ['+@GESTION+'].DBO.AGENCIA WHERE CODIGO=@AGENCIA)
			SET @CONDUCTO=''''
		ELSE
			SET @CONDUCTO=(SELECT NOMBRE FROM ['+@GESTION+'].DBO.AGENCIA WHERE CODIGO=@AGENCIA)
		
		-- Albert Rodriguez 27/10/20
		-- Si el cliente es contado, los datos de la dirección de envío hay que cogerlos de otra tabla
		IF @CONTADO=0
			SELECT @DIR_ENV=DIRECCION, @CP_ENV=CODPOS, @POBL_ENV=POBLACION, @PROV_ENV=PROVINCIA, @TELF_ENV=TELEFONO FROM ['+@GESTION+'].DBO.ENV_CLI 
			WHERE CLIENTE=@CLIENTE AND LINEA=@ENV_CLI
		ELSE
			SELECT @DIR_ENV=DIRECCION, @CP_ENV=CODPOST, @POBL_ENV=POBLACION, @PROV_ENV=PROVINCIA, @TELF_ENV=TELEFONO FROM ['+@GESTION+'].DBO.CONTADO 
			WHERE EMPRESA='''+@EMPRESA+''' AND LETRA='''+@LETRA+''' AND NUMERO='''+@PEDIDO+''' AND TIPO=2
		
		IF @CONTADO=1
			BEGIN
				INSERT INTO ['+@GESTION+'].DBO.CONTADO (EMPRESA, NUMERO, TIPO, NOMBRE, DIRECCION, CODPOST, POBLACION, PROVINCIA, PAIS, CIF, TELEFONO, LETRA, EMAIL)
				SELECT EMPRESA, '''+@NUMERO+''', 1 AS TIPO, NOMBRE, DIRECCION, CODPOST, POBLACION, PROVINCIA, PAIS, CIF, TELEFONO, LETRA, EMAIL
				FROM ['+@GESTION+'].DBO.CONTADO WHERE EMPRESA='''+@EMPRESA+''' AND LETRA='''+@LETRA+''' AND NUMERO='''+@PEDIDO+''' AND TIPO=2
			END
			
		
		IF ltrim(rtrim(isnull(@TELF_ENV,'''')))=''''
			SET @TELF_ENV=(SELECT TELEFONO FROM ['+@GESTION+'].DBO.TELF_CLI WHERE CLIENTE=@CLIENTE AND ORDEN=1)

		IF ltrim(rtrim(isnull(@TELF_ENV,'''')))=''''
			SET @TELF_ENV=''          ''

		IF @RUTA IS NULL OR @RUTA = ''''
			SET @RUTA = (SELECT RUTA FROM ['+@GESTION+'].dbo.clientes WHERE CODIGO=@CLIENTE)

		--LOG4
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:4'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'

		SET @cSQL5 = '		
		IF @BANC_CLI IS NULL 
			SET @BANC_CLI = 0 
		IF '''+@NUMAV+'''='''' -- IS NULL
			BEGIN
				-- CREACION DE ALBARANES
				-- ****** CABECERAS ALBARAN ******
				-- INSERTAR C_ALBVEN
				INSERT INTO ['+@GESTION+'].DBO.C_ALBVEN (PEDIDO, USUARIO, EMPRESA, NUMERO, LETRA, FECHA, CLIENTE, ENV_CLI, PRONTO, VENDEDOR,
				RUTA, ALMACEN, FACTURA, FECHA_FAC, ASI, FPAG, IMPORTE, OBSERVACIO, BANC_CLI, DIVISA, CAMBIO, IMPDIVISA, FINAN, VISTA, COSTE, PESO, 
				LITROS, OBRA, TRASPASADO, RECEQUIV, TAG, OPERARIO, FACTURABLE, COT_PUNT, PUNTOS, CLIFINAL, IMPRESO, LIBRE_1, LIBRE_2, LIBRE_3, 
				CERTIFIC, STOCK_COEF, EDI, GASTOS, FECHASTOCK, MANDATO, RECC, TOTALDOC) 
				VALUES ('''+@PEDIDO+'''+'''+@LETRA+''', @USUARIO, '''+@EMPRESA+''', '''+@NUMERO+''', '''+@LETRAFAC+''', @FECHA, @CLIENTE, @ENV_CLI, @PRONTO, @VENDEDOR, @RUTA, 
				@ALMACEN, @FACTURA, @FECHA_FAC, @ASI, @FPAG, @IMPORTE, @OBSERVACIO, @BANC_CLI, @DIVISA, @CAMBIO, @IMPDIVISA, @FINAN, @VISTA, @COSTE,
				@PESTOTAL, 
				@LITROS, @OBRA, @TRASPASADO, @RECEQUIV, @TAG, @OPERARIO, @FACTURABLE, @COT_PUNT, @PUNTOS, @CLIFINAL, @IMPRESO, @LIBRE_1, @LIBRE_2,
				@LIBRE_3, @CERTIFIC, @STOCK_COEF, @EDI, @GASTOS, @FECHASTOCK, @MANDATO, @RECC, 0.00)

				-- INSERTAR ALBV_ADI
				INSERT INTO ['+@GESTION+'].DBO.ALBV_ADI (EMPRESA, NUMERO, COSTEDIV, LETRA) 
					VALUES ('''+@EMPRESA+''', '''+@NUMERO+''', 0.00, '''+@LETRAFAC+''')

				-- INSERTAR ALB_FPAG
				INSERT INTO ['+@GESTION+'].DBO.ALB_FPAG (EMPRESA, NUMERO, GIRO, LETRA) 
					SELECT '''+@EMPRESA+''', '''+@NUMERO+''', GIRO, '''+@LETRAFAC+''' FROM [COMU0001].DBO.FPAG_GIR WHERE FPAG=@FPAG

				-- INSERTAR MULTICA2
				INSERT INTO ['+@GESTION+'].DBO.MULTICA2 (EMPRESA, NUMERO, FICHERO, CAMPO, VALOR, LETRA) 
					SELECT EMPRESA, '''+@NUMERO+''', 1, CAMPO, VALOR, '''+@LETRAFAC+''' FROM ['+@GESTION+'].DBO.MULTICA2 WHERE FICHERO=2 AND EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+'''

				-- INSERTAR PORTES --> tipo iva tienes ''03'' quemado
				SELECT @IVAPORT=IVA_PORTES FROM ['+@GESTION+'].DBO.CODIGOS WHERE EMPRESA='''+@EMPRESA+'''
				INSERT INTO ['+@GESTION+'].DBO.PORTES (EMPRESA, CLIENTE, ALBARAN, INC_PP, IVA_INC, IMPORTE, IMPORTEDIV, TIPO_IVA, AGENCIA, LETRA, INC_FRA, TIPO_PORTE) 
					VALUES('''+@EMPRESA+''', @CLIENTE, '''+@NUMERO+''', 1, 0, 0.00, 0.00, @IVAPORT, @AGENCIA, '''+@LETRAFAC+''', 1, @TIPOPORTE)

				-- INSERTAR ETIQUETAS ENVIO
				INSERT INTO ['+@GESTION+'].DBO.ENVIOETI (EMPRESA, CONTADOR, NUMERO, CODIGOCLI, CLIENTE, BULTOS, PESO, PORTES, CONDUCTO, FECHA, DIRECCION, 
					CODPOS, POBLACION, PROVINCIA, AGENCIA, LETRA, RECOGIDA, TELEFONO) 
				VALUES ('''+@EMPRESA+''', @CONTADOR, '''+@NUMERO+''', @CLIENTE, @NCLIENTE, @BULTOS, @PESTOTAL, @PORTES, @CONDUCTO, @FECHA, COALESCE(@DIR_ENV,''''), 
					COALESCE(@CP_ENV,''''), COALESCE(@POBL_ENV,''''), COALESCE(@PROV_ENV,''''), @AGENCIA, '''+@LETRAFAC+''', @FECHA, @TELF_ENV)

		--LOG5
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:5'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL6 = '	
				-- ****** LINEAS DE ALBARAN ******
				-- INSERTAR D_ALBVEN
				-- PRIMERA LÍNEA CON UNA NOTA CON EL NÚMERO DE PEDIDO
				INSERT INTO ['+@GESTION+'].DBO.D_ALBVEN (USUARIO, EMPRESA, NUMERO, DEFINICION, PEDIDO, FECHA, LINIA, CLIENTE, DOC, DOC_NUM, DOC_LIN, LETRA, FLACTUAL, FLANTERIOR) 
					VALUES (@USUARIO, '''+@EMPRESA+''', '''+@NUMERO+''', ''Pedido nº ''+'''+@LETRA+'''+'' - ''+LTRIM(RTRIM('''+@PEDIDO+'''))+'' de fecha ''+ @FECHA, '''+@PEDIDO+'''+'''+@LETRA+''', @FECHA, 1, @CLIENTE, 1, 
					'''+@PEDIDO+'''+'''+@LETRA+''', 0, '''+@LETRAFAC+''', @FECHA, @FECHA)
				-- Líneas de Artículos
				INSERT INTO ['+@GESTION+'].DBO.D_ALBVEN (USUARIO, EMPRESA, NUMERO, ARTICULO, DEFINICION, UNIDADES, DTO1, DTO2, TIPO_IVA, COSTE, FECHA, PEDIDO, LINIA, CLIENTE, PRECIO, 
					IMPORTE, PRECIOIVA, IMPORTEIVA, CAJAS, FAMILIA, PRECIODIV, IMPORTEDIV, TIPO, COMISION, IMP_COM, PESO, DOC, DOC_NUM, DOC_LIN, DOC_UNID, VISTA, PVERDE, LETRA, 
					IMPDIVIVA, PREDIVIVA, FLACTUAL, FLANTERIOR, DOC_CAJA)
				SELECT @USUARIO AS USUARIO, '''+@EMPRESA+''' AS EMPRESA, '''+@NUMERO+''' AS NUMERO, MAX(TT.ARTICULO) COLLATE DATABASE_DEFAULT AS ARTICULO, MAX(DPV.DEFINICION) AS DEFINICION, 
					SUM(TT.UNIDADES) AS UNIDADES, MAX(DPV.DTO1) AS DTO1, MAX(DPV.DTO2) AS DTO2, MAX(DPV.TIPO_IVA) AS TIPO_IVA, MAX(DPV.COSTE) AS COSTE, @FECHA AS FECHA, 
					'''+@PEDIDO+'''+'''+@LETRA+''' AS PEDIDO, DPV.LINIA+1 AS LINIA, @CLIENTE AS CLIENTE, MAX(DPV.PRECIO) AS PRECIO, 
					0.00 AS IMPORTE, MAX(DPV.PRECIOIVA) AS PRECIOIVA, 0.00 AS IMPORTEIVA, CASE WHEN MAX(ART.UNICAJA) != 0.00 then floor(SUM(COALESCE(TT.UNIDADES,0.00))/MAX(art.unicaja)) else SUM(COALESCE(TT.CAJAS,0.00)) end AS CAJAS, MAX(DPV.FAMILIA) AS FAMILIA, MAX(DPV.PRECIODIV) AS PRECIODIV,
					0.00 AS IMPORTEDIV, 0 AS TIPO, 0.00 AS COMISION, 0.00 AS IMP_COM, SUM(CAST(CASE WHEN COALESCE(TT.PESO,''0.00'')='''' THEN ''0.00'' ELSE COALESCE(TT.PESO,''0.00'') END AS DECIMAL(16,4))) AS PESO, 1 AS DOC, '''+@PEDIDO+'''+'''+@LETRA+''' AS DOC_NUM, 
					DPV.LINIA AS LIN_DOC, SUM(TT.UNIDADES) AS UDS_DOC, 1 AS VISTA, MAX(DPV.PVERDE) AS PVERDE, '''+@LETRAFAC+''' AS LETRA, 
					0.00 AS IMPDIVIVA, MAX(DPV.PREDIVIVA) AS PREDIVIVA, @FECHA AS FLACTUAL, @FECHA AS FLANTERIOR, SUM(coalesce(TT.CAJAS,0.00)) AS CAJAS_DOC 
				FROM TRASPASO_TEMPORAL TT 
					INNER JOIN ['+@GESTION+'].DBO.D_PEDIVE DPV ON DPV.EMPRESA+DPV.LETRA+DPV.NUMERO+STR(DPV.LINIA)=TT.EMPRESA+TT.LETRA+TT.NUMERO+STR(TT.LINEA) COLLATE DATABASE_DEFAULT 
					LEFT JOIN ['+@GESTION+'].DBO.ARTICULO ART ON ART.CODIGO=TT.ARTICULO COLLATE DATABASE_DEFAULT 
				WHERE TT.EMPRESA='''+@EMPRESA+''' AND TT.NUMERO='''+@PEDIDO+''' AND TT.LETRA='''+@LETRA+''' GROUP BY TT.EMPRESA, TT.NUMERO, TT.LETRA, DPV.LINIA 

		--LOG6
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:6'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL7 = '	
			END
		ELSE
			BEGIN
				-- INSERTAR LINEAS EN ALBARANES EXISTENTES

				-- ****** LINEAS DE ALBARAN ******
				SET @LINIA=(SELECT MAX(LINIA) AS LINIA FROM ['+@GESTION+'].DBO.D_ALBVEN WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+''')
				UPDATE TRASPASO_TEMPORAL SET LINEA_AV=T2.LINIA_AV FROM TRASPASO_TEMPORAL T INNER JOIN 
					(select ROW_NUMBER() OVER(PARTITION BY SESION, EMPRESA, NUMERO, LETRA ORDER BY SESION, EMPRESA, NUMERO, LETRA )+@LINIA AS LINIA_AV, SESION, EMPRESA, NUMERO, LETRA, LINEA 
					FROM TRASPASO_TEMPORAL where EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''' 
					GROUP BY SESION, EMPRESA, NUMERO, LETRA, LINEA) T2 ON T2.SESION=T.SESION AND T2.EMPRESA=T.EMPRESA AND T2.NUMERO=T.NUMERO AND T2.LETRA=T.LETRA AND T2.LINEA=T.LINEA
		--LOG7
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:7'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL8 = '				
				-- INSERTAR D_ALBVEN
				INSERT INTO ['+@GESTION+'].DBO.D_ALBVEN (USUARIO, EMPRESA, NUMERO, ARTICULO, DEFINICION, UNIDADES, DTO1, DTO2, TIPO_IVA, COSTE, FECHA, PEDIDO, LINIA, CLIENTE, PRECIO, 
					IMPORTE, PRECIOIVA, IMPORTEIVA, CAJAS, FAMILIA,PRECIODIV, IMPORTEDIV, TIPO, COMISION, IMP_COM, PESO, DOC, DOC_NUM, DOC_LIN, DOC_UNID, VISTA, PVERDE, LETRA, 
					IMPDIVIVA, PREDIVIVA, FLACTUAL, FLANTERIOR, DOC_CAJA)
				SELECT @USUARIO AS USUARIO, '''+@EMPRESA+''' AS EMPRESA, '''+@NUMERO+''' AS NUMERO, MAX(TT.ARTICULO) COLLATE DATABASE_DEFAULT AS ARTICULO, MAX(DPV.DEFINICION) AS DEFINICION, 
					SUM(TT.UNIDADES) AS UNIDADES, MAX(DPV.DTO1) AS DTO1, MAX(DPV.DTO2) AS DTO2, MAX(DPV.TIPO_IVA)AS TIPO_IVA, MAX(DPV.COSTE) AS COSTE, @FECHA AS FECHA, 
					'''+@PEDIDO+'''+'''+@LETRA+''' AS PEDIDO, TT.LINEA_AV, @CLIENTE AS CLIENTE, MAX(DPV.PRECIO) AS PRECIO, 
					0.00 AS IMPORTE, MAX(DPV.PRECIOIVA) AS PRECIOIVA, 0.00 AS IMPORTEIVA, SUM(COALESCE(TT.CAJAS,0.00)) AS CAJAS, MAX(DPV.FAMILIA) AS FAMILIA, MAX(DPV.PRECIODIV) AS PRECIODIV, 
					0.00 AS IMPORTEDIV, 0 AS TIPO, 0.00 AS COMISION, 0.00 AS IMP_COM, SUM(CAST(CASE WHEN COALESCE(TT.PESO,''0.00'')='''' THEN ''0.00'' ELSE COALESCE(TT.PESO,''0.00'') END AS DECIMAL(16,4))) AS PESO, 1 AS DOC, '''+@PEDIDO+'''+'''+@LETRA+''' AS DOC_NUM, 
					TT.LINEA AS LIN_DOC, SUM(TT.UNIDADES) AS UDS_DOC, 1 AS VISTA, MAX(DPV.PVERDE) AS PVERDE, '''+@LETRAFAC+''' AS LETRA, 
					0.00 AS IMPDIVIVA, MAX(DPV.PREDIVIVA) AS PREDIVIVA, @FECHA AS FLACTUAL, @FECHA AS FLANTERIOR, SUM(COALESCE(TT.CAJAS,0.00)) AS CAJAS_DOC 
				FROM TRASPASO_TEMPORAL TT 
					INNER JOIN ['+@GESTION+'].DBO.D_PEDIVE DPV ON DPV.EMPRESA+DPV.LETRA+DPV.NUMERO+STR(DPV.LINIA)=TT.EMPRESA+TT.LETRA+TT.NUMERO+STR(TT.LINEA) COLLATE DATABASE_DEFAULT 
				WHERE TT.EMPRESA='''+@EMPRESA+''' AND TT.NUMERO='''+@PEDIDO+''' AND TT.LETRA='''+@LETRA+''' AND (DPV.SERVIDAS<DPV.UNIDADES OR DPV.TIPO_IVA='''') GROUP BY TT.EMPRESA, TT.NUMERO, TT.LETRA, TT.LINEA, TT.LINEA_AV

				-- ACTUALIZAR BULTOS	
				UPDATE ['+@GESTION+'].DBO.ENVIOETI SET BULTOS=CAST(BULTOS AS DECIMAL(7))+CAST(@BULTOS AS DECIMAL(7)) WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+'''

				-- ACTUALIZAR Nº ETIQUETAS CAMPOSAD!C_ALBVENEW
				UPDATE [CAMPOSYB].DBO.C_ALBVENEW SET EWNETI=EWNETI+@ETICAJAS WHERE EJERCICIO=@EJERCICIO AND EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+'''
							
				-- INSERTAR CAMPOSAD!D_ALBVENEW
				INSERT INTO [CAMPOSYB].DBO.D_ALBVENEW (EJERCICIO, EMPRESA, NUMERO, LETRA, LINIA) 
					SELECT @EJERCICIO, EMPRESA, '''+@NUMERO+''', '''+@LETRAFAC+''', LINEA_AV FROM TRASPASO_TEMPORAL 
				WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''' GROUP BY EMPRESA, NUMERO, LETRA, LINEA_AV
		
				-- INSERTAR DISTRIBU!D_ADIALBVE
				INSERT INTO [DISTRIYB].DBO.D_ADIALBVE (EJERCICIO, EMPRESA, NUMERO, LETRA, LINIA, OPERACION, TARIFA) 
					SELECT @EJERCICIO, EMPRESA, '''+@NUMERO+''', '''+@LETRAFAC+''', LINEA_AV, 1, @TARIFA FROM TRASPASO_TEMPORAL 
				WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''' GROUP BY EMPRESA, NUMERO, LETRA, LINEA_AV
		--LOG8
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:8'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL9 = '		
				-- INSERTAR LOTES!D_ALBVEN2
				INSERT INTO [LOTES0YB].DBO.D_ALBVEN2 (EJERCICIO, EMPRESA, NUMERO, LETRA, LINIA) 
					SELECT @EJERCICIO, T.EMPRESA, '''+@NUMERO+''', '''+@LETRAFAC+''', T.LINEA_AV FROM TRASPASO_TEMPORAL T INNER JOIN [LOTES0YB] .DBO.artlot A ON A.ARTICULO=T.ARTICULO COLLATE DATABASE_DEFAULT
				WHERE T.EMPRESA='''+@EMPRESA+''' AND T.NUMERO='''+@PEDIDO+''' AND T.LETRA='''+@LETRA+''' AND A.LOTE=1 GROUP BY T.EMPRESA, T.NUMERO, T.LETRA, T.LINEA_AV 

				--  15/10/2020 HAROLD: PARA AGREGAR LAS ENTREGAS A CUENTA SE BORRAN LAS EXISTENTES
				DELETE from ['+@GESTION+'].DBO.ENTREGAS WHERE EMPRESA='''+@EMPRESA+''' AND LETRA='''+@LETRAFAC+''' AND ALBARAN='''+@NUMERO+'''
			END


		-- Insertar CAJAS A FACTURAR en el detalle del albarán creado.
		IF '''+@LETRA+'''=''FR''
			BEGIN
			DECLARE @ARTCAJA CHAR(8), @LINCAJAS INT
			SET @ARTCAJA=''Z1''
			SET @LINCAJAS=(SELECT MAX(LINIA) FROM ['+@GESTION+'].DBO.D_ALBVEN WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+''')
			IF EXISTS (SELECT * FROM ['+@GESTION+'].DBO.D_ALBVEN WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+''' AND ARTICULO=@ARTCAJA)
					UPDATE ['+@GESTION+'].DBO.D_ALBVEN SET UNIDADES=UNIDADES+CAST(@ETICAJAS AS DECIMAL(7)), CAJAS=CAJAS+CAST(@ETICAJAS AS DECIMAL(7)), IMPORTE=PRECIO*CAST(@ETICAJAS AS DECIMAL(7)), IMPORTEDIV=PRECIO*CAST(@ETICAJAS AS DECIMAL(7)), IMPORTEIVA=PRECIOIVA*CAST(@ETICAJAS AS DECIMAL(7)), IMPDIVIVA=PRECIOIVA*CAST(@ETICAJAS AS DECIMAL(7)), LINIA=@LINCAJAS+1 WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+''' AND ARTICULO=@ARTCAJA
			ELSE
				INSERT INTO ['+@GESTION+'].DBO.D_ALBVEN (USUARIO, EMPRESA, NUMERO, ARTICULO, DEFINICION, UNIDADES, DTO1, DTO2, TIPO_IVA, COSTE, FECHA, PEDIDO, LINIA, CLIENTE, PRECIO, 
				IMPORTE, PRECIOIVA, IMPORTEIVA, CAJAS, FAMILIA,PRECIODIV, IMPORTEDIV, TIPO, COMISION, IMP_COM, PESO, DOC, DOC_NUM, DOC_LIN, DOC_UNID, VISTA, PVERDE, LETRA, 
				IMPDIVIVA, PREDIVIVA, FLACTUAL, FLANTERIOR, DOC_CAJA) 
				SELECT @USUARIO, '''+@EMPRESA+''', '''+@NUMERO+''', ART.CODIGO, ART.NOMBRE, CAST(@ETICAJAS AS DECIMAL(7)) AS UNIDADES,  0.00 AS DTO1, 0.00 AS DTO2, ART.TIPO_IVA, ART.COST_ULT1, @FECHA, '''' AS PEDIDO, @LINCAJAS+1 AS LINIA, @CLIENTE, PVP.PVP AS PRECIO, PVP.PVP*CAST(@ETICAJAS AS DECIMAL(7)) AS IMPORTE, PVP.PVP*(1+(IVA.IVA/100)) AS PRECIOIVA, (PVP.PVP*(1+(IVA.IVA/100)))*(CAST(@ETICAJAS AS DECIMAL(7))) AS IMPORTEIVA, CAST(@ETICAJAS AS DECIMAL(7)) AS CAJAS, ART.FAMILIA, PVP.PVP AS PRECIODIV, PVP.PVP*CAST(@ETICAJAS AS DECIMAL(7)) AS IMPORTEDIV, 0 AS TIPO, 0.00 AS COMISION, 0.00 AS IMP_COM, 0.00 AS PESO, 0 AS DOC, '''' AS DOC_NUM, 0 AS LIN_DOC, 0.00 AS UDS_DOC, 1 AS VISTA, 0.00 AS PVERDE, '''+@LETRAFAC+''', (PVP.PVP*(1+(IVA.IVA/100)))*(CAST(@ETICAJAS AS DECIMAL(7))) AS IMPDIVIVA, PVP.PVP*(1+(IVA.IVA/100)) AS PREDIVIVA, @FECHA AS FLACTUAL, @FECHA AS FLANTERIOR, 0.00 AS CAJAS_DOC		
				FROM ['+@GESTION+'].DBO.ARTICULO ART INNER JOIN ['+@GESTION+'].DBO.PVP PVP ON PVP.ARTICULO=ART.CODIGO LEFT JOIN ['+@GESTION+'].DBO.TIPO_IVA IVA ON IVA.CODIGO=ART.TIPO_IVA WHERE ART.CODIGO=@ARTCAJA AND PVP.TARIFA=@TARIFA
			
			-- ACTUALIZAR STOCK CAJAS (ARTICULO Z1)	
			UPDATE ['+@GESTION+'].DBO.STOCKS2 SET FINAL=FINAL-CAST(@ETICAJAS AS DECIMAL(7)), SALIDAS=SALIDAS+CAST(@ETICAJAS AS DECIMAL(7)) WHERE ARTICULO=@ARTCAJA AND EMPRESA='''+@EMPRESA+''' AND ALMACEN=@ALMACEN
			END

		--LOG9
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:9'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL10 = '		
		-- ****** PROCESOS A EJECUTAR CADA VEZ QUE SE INSERTAN DATOS
		-- ACTUALIZAR STOCKS2
		UPDATE ['+@GESTION+'].DBO.STOCKS2 SET FINAL=S.FINAL-T.UNIDADES, SALIDAS=S.SALIDAS+T.UNIDADES 
		FROM ['+@GESTION+'].DBO.STOCKS2 S 
		INNER JOIN (SELECT ARTICULO, ALMACEN, SUM(UNIDADES) AS UNIDADES, SUM(CAST(CASE WHEN COALESCE(PESO,''0.00'')='''' THEN ''0.00'' ELSE COALESCE(PESO,''0.00'') END AS DECIMAL(16,4))) AS PESO FROM Traspaso_Temporal
		WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''' 
		GROUP BY ARTICULO, ALMACEN) T ON T.ARTICULO=S.ARTICULO COLLATE DATABASE_DEFAULT AND T.ALMACEN=S.ALMACEN COLLATE DATABASE_DEFAULT

		-- ACTUALIZAR ARTICULO
		UPDATE ['+@GESTION+'].dbo.articulo  SET fecha_ult=@FECHA FROM ['+@GESTION+'].dbo.articulo A 
			INNER JOIN Traspaso_Temporal T ON T.ARTICULO COLLATE DATABASE_DEFAULT=A.CODIGO 
		WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+'''

		-- ACTUALIZAR IMPORTES D_ALBVEN 15/10/2020 HAROLD, ESTABA MALO IMPORTEDIV
		UPDATE ['+@GESTION+'].DBO.D_ALBVEN SET 
			IMPORTE=ROUND((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIO ELSE PESO*PRECIO END)
				-((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIO ELSE PESO*PRECIO END)*(DTO1/100))
				-(((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIO ELSE PESO*PRECIO END)
				-(CASE WHEN PESO=0.00 THEN UNIDADES*PRECIO ELSE PESO*PRECIO END)*(DTO1/100))*(DTO2/100)),2), 
			IMPORTEIVA=ROUND((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIOIVA ELSE PESO*PRECIOIVA END)
				-((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIOIVA ELSE PESO*PRECIOIVA END)*(DTO1/100))
				-(((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIOIVA ELSE PESO*PRECIOIVA END)
				-(CASE WHEN PESO=0.00 THEN UNIDADES*PRECIOIVA ELSE PESO*PRECIOIVA END)*(DTO1/100))*(DTO2/100)),2), 
			IMPORTEDIV=ROUND((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIODIV ELSE PESO*PRECIODIV END)
				-((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIODIV ELSE PESO*PRECIODIV END)*(DTO1/100))
				-(((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIODIV ELSE PESO*PRECIODIV END)
				-(CASE WHEN PESO=0.00 THEN UNIDADES*PRECIODIV ELSE PESO*PRECIODIV END)*(DTO1/100))*(DTO2/100)),2), 
			IMPDIVIVA=ROUND((CASE WHEN PESO=0.00 THEN UNIDADES*PREDIVIVA ELSE PESO*PREDIVIVA END)
				-((CASE WHEN PESO=0.00 THEN UNIDADES*PREDIVIVA ELSE PESO*PREDIVIVA END)*(DTO1/100))
				-(((CASE WHEN PESO=0.00 THEN UNIDADES*PREDIVIVA ELSE PESO*PREDIVIVA END)
				-(CASE WHEN PESO=0.00 THEN UNIDADES*PREDIVIVA ELSE PESO*PREDIVIVA END)*(DTO1/100))*(DTO2/100)),2) 
		WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+'''	

		--LOG10
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:10'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL11 = '		
		-- ACTUALIZAR PVERDE
		BEGIN
		IF @PVERDECLI=0
			UPDATE ['+@GESTION+'].DBO.D_ALBVEN SET PVERDE=ROUND(CASE WHEN D.PESO=0.00 THEN D.UNIDADES*A.P_IMPORTE ELSE D.PESO*A.P_IMPORTE END,2) 
			FROM ['+@GESTION+'].DBO.D_ALBVEN D INNER JOIN ['+@GESTION+'].DBO.ARTICULO A ON A.CODIGO=D.ARTICULO AND A.PVERDE=1 
			WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+'''	 
		END

		-- ACTUALIZAR IMPORTES C_ALBVEN LA SUMA PARA TOTALDOC INCLIDA
		UPDATE ['+@GESTION+'].DBO.C_ALBVEN SET IMPORTE=D.IMPORTE, IMPDIVISA=D.IMPDIVISA, TOTALDOC=0.00, 
		COSTE=D.COSTE, PESO=D.PESO, LITROS=D.LITROS FROM ['+@GESTION+'].DBO.C_ALBVEN C 
		INNER JOIN (SELECT MAX(D.EMPRESA) AS EMPRESA, MAX(D.NUMERO) AS NUMERO, MAX(D.LETRA) AS LETRA, SUM(D.IMPORTE)+SUM(D.PVERDE) AS IMPORTE, 
		SUM(D.IMPORTEDIV)+SUM(D.PVERDE) AS IMPDIVISA, SUM(D.IMPORTEIVA) AS IMPORTEIVA, SUM(D.PVERDE) AS PVERDE, 
		SUM(CASE WHEN D.PESO!=0.00 THEN D.UNIDADES*D.COSTE ELSE D.PESO*D.UNIDADES END) AS COSTE, SUM(D.PESO) AS PESO, 
		SUM(D.UNIDADES*(CAST(CASE WHEN D.PESO=0.00 THEN CASE WHEN LTRIM(RTRIM(COALESCE(A.LITROS,'''')))='''' THEN ''0.00'' ELSE COALESCE(A.LITROS,'''') END ELSE ''0.00'' END AS DECIMAL(10,2)))) AS LITROS 
		FROM ['+@GESTION+'].DBO.D_ALBVEN D LEFT JOIN ['+@GESTION+'].DBO.ARTICULO A ON A.CODIGO=D.ARTICULO 
		WHERE D.EMPRESA='''+@EMPRESA+''' AND D.NUMERO='''+@NUMERO+''' AND D.LETRA='''+@LETRAFAC+''') D ON D.EMPRESA+D.NUMERO+D.LETRA=C.EMPRESA+C.NUMERO+C.LETRA 
		WHERE C.EMPRESA='''+@EMPRESA+''' AND C.NUMERO='''+@NUMERO+''' AND C.LETRA='''+@LETRAFAC+'''

		-- ACTUALIZAR COSTE ALBV_ADI
		UPDATE ['+@GESTION+'].DBO.albv_adi SET COSTEDIV=A2.COSTE FROM ['+@GESTION+'].DBO.albv_adi A1 
		INNER JOIN ['+@GESTION+'].DBO.C_ALBVEN A2 ON A1.EMPRESA+A1.NUMERO+A1.LETRA=A2.EMPRESA+A2.NUMERO+A2.LETRA
		WHERE A1.EMPRESA='''+@EMPRESA+''' AND A1.NUMERO='''+@NUMERO+''' AND A1.LETRA='''+@LETRAFAC+'''

		UPDATE ['+@GESTION+'].DBO.D_PEDIVE SET SERVIDAS=CASE WHEN (DPV.SERVIDAS+TT.UNIDADES)>DPV.UNIDADES THEN DPV.UNIDADES ELSE DPV.SERVIDAS+TT.UNIDADES END, CAJASERV=CASE WHEN (DPV.CAJASERV+TT.CAJAS)>DPV.CAJAS THEN DPV.CAJAS ELSE DPV.CAJASERV+TT.CAJAS END, LIBRE_4=CASE WHEN DPV.TIPO_IVA='''' THEN ''TRASPASADO'' ELSE '''' END
		FROM ['+@GESTION+'].DBO.D_PEDIVE DPV 
		INNER JOIN (SELECT SESION, EMPRESA, NUMERO, LETRA, LINEA, SUM(UNIDADES) AS UNIDADES, SUM(coalesce(CAJAS,0.00)) AS CAJAS FROM TRASPASO_TEMPORAL 
		WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''' GROUP BY SESION, EMPRESA, LETRA, NUMERO, LINEA) TT 
		ON DPV.EMPRESA+DPV.LETRA+DPV.NUMERO+STR(DPV.LINIA)=TT.EMPRESA+TT.LETRA+TT.NUMERO+STR(TT.LINEA) COLLATE DATABASE_DEFAULT

		--LOG11
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:11'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL12 = '		
		-- BORRAR REGISTROS TABLA DE TRASPASOS
		-- Albert R. 21/07/2021 logs de traspasos temporales
		--insert into Traspaso_Temporal_log select * FROM Traspaso_Temporal WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+'''
		--El delete de la tabla temporal no se puede comentar, sino los traspasas parciales fallan.
		DELETE FROM Traspaso_Temporal WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+'''

		-- Calcular TOTALDOC
		-- 09/05/2020 Albert Rodriguez: Actualizar TotalDoc	
		-- 08/10/2020 Harold, cambios en totaldoc	
		-- 04/08/2021 Albert R, conviErto el campo para dejarlo solo con dos decimales, no redondeo porque se quito en su momento por descuadres y el fallo ahora es solo en los asientos y con estos se soluciona.
		SELECT @TOTALDOC = CONVERT(DECIMAL(16,2),sum(totimpiva)) from (SELECT sum( (D.IMPORTE+D.PVERDE)-((D.IMPORTE+D.PVERDE)*(C.PRONTO/100)) ) +
				round( sum(( (D.IMPORTE+D.PVERDE)-((D.IMPORTE+D.PVERDE)*(C.PRONTO/100)) )*(COALESCE(IVA.IVA,0.00)/100)),2) +
				round( sum(( (D.IMPORTE+D.PVERDE)-((D.IMPORTE+D.PVERDE)*(C.PRONTO/100)) )*(CASE WHEN CLI.RECARGO=1 THEN COALESCE(IVA.RECARG,0.00)/100 ELSE 0 END)),2) as totimpiva, IVA.iva
				FROM ['+@GESTION+'].DBO.D_ALBVEN D JOIN ['+@GESTION+'].DBO.C_ALBVEN C ON D.EMPRESA=C.EMPRESA AND D.LETRA=C.LETRA AND D.NUMERO=C.NUMERO
				LEFT JOIN ['+@GESTION+'].DBO.TIPO_IVA IVA ON IVA.CODIGO=D.TIPO_IVA LEFT JOIN ['+@GESTION+'].DBO.CLIENTES CLI ON CLI.CODIGO=C.CLIENTE
				WHERE d.EMPRESA='''+@EMPRESA+''' and d.LETRA='''+@LETRAFAC+''' and d.NUMERO='''+@NUMERO+''' group by c.empresa, c.numero, c.letra, IVA.iva) a

		-- PORTES
		/*SELECT @IMPPORTES=CASE WHEN @NOPORTES=1 THEN 0.00 WHEN @PORTCOMP>IMPORTE OR @PORTCOMP=0.00 THEN @VALPORTES ELSE 0.00 END FROM ['+@GESTION+'].DBO.C_ALBVEN WHERE EMPRESA='''+@EMPRESA+''' and LETRA='''+@LETRAFAC+''' and NUMERO='''+@NUMERO+'''*/

		SELECT @IMPPORTES=CASE WHEN @NOPORTES=1 THEN 0.00 WHEN @PORTCOMP>TOTALPED OR @PORTCOMP=0.00 THEN @VALPORTES ELSE 0.00 END FROM ['+@GESTION+'].DBO.C_PEDIVE WHERE EMPRESA='''+@EMPRESA+''' and LETRA='''+@LETRA+''' and NUMERO='''+@PEDIDO+'''

		-- Pedidos de a serie DI, Si al crear el nuevo albarán, ya existe un albarán que tiene ese número de pedido y ese albarán tiene portes, los portes no se insertarán en el nuevo documento
		IF '''+@LETRA+'''=''DI''
			BEGIN
				IF EXISTS (SELECT * FROM ['+@GESTION+'].DBO.C_ALBVEN C INNER JOIN ['+@GESTION+'].DBO.PORTES P ON P.EMPRESA=C.EMPRESA AND P.ALBARAN=C.NUMERO AND P.LETRA=C.LETRA WHERE LTRIM(PEDIDO)='''+LTRIM(RTRIM(@PEDIDO))+@LETRA+''' AND P.IMPORTE!=0.00)
					SET @IMPPORTES = 0.00
			END
				
		UPDATE ['+@GESTION+'].DBO.C_ALBVEN SET IMPORTE=IMPORTE+@IMPPORTES, IMPDIVISA=IMPDIVISA+@IMPPORTES
		, TOTALDOC=isnull(@TOTALDOC,0.00)+ISNULL(@IMPPORTES+ROUND((@IMPPORTES*((@IVAPORTES/100))),2)+ROUND((@IMPPORTES*(@RECPORTES/100)),2),0.00) WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+'''
		UPDATE ['+@GESTION+'].DBO.PORTES SET IMPORTE=@IMPPORTES, IMPORTEDIV=@IMPPORTES WHERE EMPRESA='''+@EMPRESA+''' AND ALBARAN='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+'''

		--LOG12
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:12'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL13 = '		
		-- Insertar Entregas a cuenta	
		DECLARE @IMPORTEDIV NUMERIC(15,6), @CUENTA VARCHAR(10), @FECHA_POR smalldatetime, @LETRA_POR VARCHAR(2), @TOTENTREGAS NUMERIC(15,6)
		select null mykey, LINEA, IMPORTE, IMPORTEDIV, ASI, CUENTA, FECHA, LETRA into #mysec from ['+@GESTION+'].DBO.entre_pv 
			WHERE EMPRESA='''+@EMPRESA+''' AND LETRA='''+@LETRA+''' AND NUMERO='''+@PEDIDO+''' order by LINEA
		SET @TOTENTREGAS=(SELECT SUM(IMPORTE) FROM #mysec)
		while (select count(*) from #mysec where isnull(mykey, 0)= 0 ) > 0 
		begin
			SET @LINIA=(SELECT MIN(LINEA) FROM #mysec where isnull(mykey, 0)= 0)
			SELECT @IMPORTE=IMPORTE, @IMPORTEDIV=IMPORTEDIV, @ASI=ASI, @CUENTA=CUENTA, @FECHA_POR=FECHA, @LETRA_POR=LETRA from #mysec WHERE LINEA=@LINIA
			IF @TOTALDOC=@IMPORTE OR @TOTALDOC=@TOTENTREGAS
				BEGIN
					INSERT INTO ['+@GESTION+'].DBO.ENTREGAS (EMPRESA, ALBARAN, LINEA, IMPORTE, IMPORTEDIV, ASI, CUENTA, FECHA, LETRA, PED_LETRA, PED_NUM)
						SELECT EMPRESA, '''+@NUMERO+''' AS NUMERO, LINEA, IMPORTE, IMPORTEDIV, ASI, CUENTA, FECHA, '''+@LETRAFAC+''', '''+@LETRA+''' AS PED_LETRA, '''+@PEDIDO+''' AS PED_NUM 
						FROM ['+@GESTION+'].DBO.entre_pv 
						WHERE EMPRESA='''+@EMPRESA+''' AND LETRA='''+@LETRA+''' AND NUMERO='''+@PEDIDO+''' order by LINEA
					UPDATE #mysec SET mykey=1
					set @TOTALDOC=0
				END
			ELSE
				BEGIN
					while (@TOTALDOC>0 )
					begin 
						DECLARE @FINPROG INT=0
						if @TOTALDOC>=@IMPORTE
							begin
							INSERT INTO ['+@GESTION+'].DBO.ENTREGAS (EMPRESA, ALBARAN, LINEA, IMPORTE, IMPORTEDIV, ASI, CUENTA, FECHA, LETRA, PED_LETRA, PED_NUM)
								VALUES('''+@EMPRESA+''', '''+@NUMERO+''', @LINIA, @IMPORTE, @IMPORTEDIV, @ASI, @CUENTA, @FECHA_POR, '''+@LETRAFAC+''', @LETRA_POR, '''+@PEDIDO+''')
							end
						else
							begin
							INSERT INTO ['+@GESTION+'].DBO.ENTREGAS (EMPRESA, ALBARAN, LINEA, IMPORTE, IMPORTEDIV, ASI, CUENTA, FECHA, LETRA, PED_LETRA, PED_NUM)
									VALUES('''+@EMPRESA+''', '''+@NUMERO+''', @LINIA, @TOTALDOC, @TOTALDOC, @ASI, @CUENTA, @FECHA_POR, '''+@LETRAFAC+''', @LETRA_POR, '''+@PEDIDO+''')
							end 
						SET @TOTALDOC=@TOTALDOC-@IMPORTE	
						if @TOTALDOC>0.09
							update #mysec set mykey=1 where LINEA=@LINIA
						ELSE
							begin
								set @TOTALDOC=0
								update #mysec set mykey=1
							end
						SET @LINIA=(SELECT MIN(LINEA) FROM #mysec where isnull(mykey, 0)= 0)
						SELECT @IMPORTE=IMPORTE, @IMPORTEDIV=IMPORTEDIV, @ASI=ASI, @CUENTA=CUENTA, @FECHA_POR=FECHA, @LETRA_POR=LETRA from #mysec WHERE LINEA=@LINIA
					end
				END
		end 
		drop table #mysec

		--LOG13
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:13'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL14 = '		
		-- Albert R. 09/07/2021. Creo un procedimiento para enviar los registros de impresión a la tabla MI_Impresión. Ahora los registros de impresión se crean al final de la creación del documento y si se genera una factura se crean al final de la factura.
		declare @NUM_FA varchar(10)=''''
		
		-- 16/10/2020 si tiene Facturación Directa generar factura de inmediato
		IF @Factdirv=1 AND '+CONVERT(CHAR(1),@imprimir)+'=1
			exec [CrearFA] '''+@EMPRESA+''', '''+@LETRAFAC+''', '''+@NUMERO+''', @FECHA_FAC, @terminal_new, '+CONVERT(CHAR(1),@imprimir)+','+CONVERT(CHAR(1),@imp_eti)+', @ETICAJAS, @bultos, '+LTRIM(rTRIM(CONVERT(CHAR(3),@nEti)))+'
		ELSE
			BEGIN
				IF '''+@LETRAFAC+'''=''ET''
					--SET @terminal_new=''DESKTOP-NEKGMLE''
					SET @terminal_new=''pc-nave-nueva''
				exec [pImprimir_Docs] '''+@EMPRESA+''', '''+@LETRAFAC+''', '''+@NUMERO+''', @terminal_new, @num_fa, '+CONVERT(CHAR(1),@imprimir)+','+CONVERT(CHAR(1),@imp_eti)+', @ETICAJAS, @bultos, '+LTRIM(RTRIM(CONVERT(CHAR(3),@nEti)))+'
			END

		--LOG14
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:14'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		--print @cSQL12
		EXEC(@cSQL+@cSQL2+@cSQL3+@cSQL4+@cSQL5+@cSQL6+@cSQL7+@cSQL8+@cSQL9+@cSQL10+@cSQL11+@cSQL12+@cSQL13+@cSQL14)

						
		-- MARCAR COMO TRASPASADO SI SE TRASPASAN TODAS LAS LINEAS
		-- Hemos creado una tabla intermedia para que la aplicación sepa si tiene que marcar el pedido como traspasado o no.
		set @cSQL = '
		DECLARE @PENDIENTE BIT
		SET @PENDIENTE=(SELECT CASE WHEN SUM(UNIDADES)-SUM(SERVIDAS)<=0.00 THEN 1 ELSE 0 END AS PENDIENTE FROM ['+@GESTION+'].DBO.D_PEDIVE WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''')
		IF @PENDIENTE=1
			BEGIN
				--UPDATE ['+@GESTION+'].DBO.C_PEDIVE SET TRASPASADO=1 WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+'''

				INSERT INTO TRASPASAR_PEDIDO (EMPRESA, LETRA, NUMERO)
				VALUES ('''+@EMPRESA+''','''+@LETRA+''','''+@PEDIDO+''')
			END
		'
		EXEC(@cSQL)

		--LOG15
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES ('Emp: '+ltrim(rtrim(@EMPRESA))+' / Numero: '+ltrim(Rtrim(@PEDIDO))+' / Letra: '+ltrim(Rtrim(@LETRA))+' / Log:15',GETDATE(), @EMPRESA, @PEDIDO, @LETRA)
		
		-- Borrar log
		delete from LOG_CREARAV WHERE LTRIM(rTRIM(EMPRESA))=LTRIM(RTRIM(@EMPRESA)) AND LTRIM(rTRIM(NUMERO))=LTRIM(RTRIM(@PEDIDO)) AND LTRIM(rTRIM(LETRA))=LTRIM(rTRIM(@LETRA))

		-- Devolvemos el número de albarán generado
		SELECT @respuestaSP=@NUMERO
		
				-- Quitamos en uso la tabla en EnUso para que no se pueda ejecutar el procedimiento CrearAV desde dos terminales hasta que el primero que lo ha ejecutado acabe.
		UPDATE EnUso set Enuso=0, fecha=getdate()  WHERE TIPO='PROCEDURE'

	--COMMIT TRAN crearAV
	RETURN -1

END TRY

BEGIN CATCH 
	-- ROLLBACK TRAN crearAV 
	DECLARE @ErrorMessage NVARCHAR(4000), @ErrorSeverity INT, @ErrorState INT, @ErrorNumber INT, @ErrorProcedure NVARCHAR(50), @ErrorLine INT

    SELECT  @ErrorNumber = ERROR_NUMBER(),  @ErrorProcedure = ERROR_PROCEDURE(), @ErrorLine = ERROR_LINE(), @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(),  @ErrorState = ERROR_STATE();  

	-- Quitamos en uso la tabla en EnUso para que no se pueda ejecutar el procedimiento CrearAV desde dos terminales hasta que el primero que lo ha ejecutado acabe.
	UPDATE EnUso set Enuso=0, fecha=getdate()  WHERE TIPO='PROCEDURE'
	
	SET @Mensaje='","ERROR":"NO se ha podido crear el albarán'
	PRINT(@ErrorMessage)
	SELECT @Mensaje AS JAVASCRIPT

    SELECT @respuestaSP='ERROR!'
	RETURN 0
END CATCH
GO
PRINT N'Modificando Procedimiento [dbo].[pConfiguracion]...';


GO
ALTER PROCEDURE [dbo].[pConfiguracion] @parametros varchar(max)='[]', @respuestaSP varchar(max)='' output
AS
BEGIN TRY	
	declare @modo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
		,	@comun varchar(50) = isnull((select JSON_VALUE(@parametros,'$.comun')),'')
		,	@empresa varchar(50) = isnull((select JSON_VALUE(@parametros,'$.empresa')),'')
		,	@nombreEmpresa varchar(50) = isnull((select JSON_VALUE(@parametros,'$.nombreEmpresa')),'')
		,	@objeto varchar(50) = isnull((select JSON_VALUE(@parametros,'$.objeto')),'')
		,	@valor varchar(50) = isnull((select JSON_VALUE(@parametros,'$.valor')),'')
		,	@confOpIO varchar(50) = isnull((select JSON_VALUE(@parametros,'$.confOpIO')),'')
		,	@cliente varchar(50) = isnull((select JSON_VALUE(@parametros,'$.cliente')),'')

	declare   @ejercicio varchar(4)
			, @LETRA varchar(2)
			, @GESTION varchar(6)
			, @CAMPOS varchar(8)
			, @LOTES varchar(8)
			, @SentenciaSQL varchar(max)

	declare @sp int


	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	if @modo='ComprobarEjercicio' and @GESTION<>'' BEGIN
		declare @laRuta char(6)
		declare @tbRuta table (RUTA char(6))
		insert  @tbRuta exec('select RUTA from ['+@COMUN+'].dbo.ejercici where PREDET=1')
		select  @laRuta=RUTA from @tbRuta 
		delete  @tbRuta
		if @GESTION<>@laRuta BEGIN
			update Configuracion set Ejercicio=LEFT(@laRuta,4), Gestion=@laRuta
			EXEC [dbo].[pAppVistas]
			EXEC [dbo].[pAppFunciones]
		END
		select @laRuta as JAVASCRIPT
		return -1
	END

	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
		if @modo='cargarConfiguracion' BEGIN			
			declare @configuracion varchar(max) = isnull( (select * from ConfigApp for JSON AUTO, INCLUDE_NULL_VALUES) ,'[]')
			declare @basculas varchar(max) = isnull( (select * from basculas for JSON AUTO, INCLUDE_NULL_VALUES) ,'[]')

			select CONCAT('{"configuracion":',@configuracion,',"basculas":',@basculas,'}') as JAVASCRIPT
			return -1
		END

	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
		if @modo='comprobarConfiguracionEmpresa' BEGIN
			if (select isnull(Empresa,'') from Configuracion)<>'' 
				select (select '' as msj, * from Configuracion for JSON AUTO, INCLUDE_NULL_VALUES) as JAVASCRIPT 
			else select '[{"msj":"NoExisteConfiguracion"}]' as JAVASCRIPT
			return -1
		END


	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
		if @modo='configurarEmpresa' BEGIN  
			-- obtener BD CAMPOSxx
			BEGIN TRY
				IF OBJECT_ID('tempdb..##tmpNomConex') IS NOT NULL BEGIN drop table ##tmpNomConex END
				set @SentenciaSQL = 'select NOM_CONEX into ##tmpNomConex from ['+@comun+'] .dbo.modulos where nombre=''CAMPOSAD'''
				EXEC (@SentenciaSQL)
				set @CAMPOS = (select NOM_CONEX from ##tmpNomConex)
				drop table ##tmpNomConex
			END TRY BEGIN CATCH END CATCH
		
			-- obtener BD LOTESxxx
			BEGIN TRY
				IF OBJECT_ID('tempdb..##tmpNomConex') IS NOT NULL BEGIN drop table ##tmpNomConex END
				set @SentenciaSQL = 'select NOM_CONEX into ##tmpNomConex from ['+@comun+'] .dbo.modulos where nombre=''LOTES'''
				EXEC (@SentenciaSQL)
				set @LOTES = (select NOM_CONEX from ##tmpNomConex)
				drop table ##tmpNomConex
			END TRY BEGIN CATCH END CATCH

			BEGIN TRY
				IF OBJECT_ID('tempdb..##pConfig01') IS NOT NULL BEGIN drop table ##tmpNomConex END
				set @SentenciaSQL = 'SELECT ltrim(rtrim(RUTA)) as RUTA, [ANY] INTO ##pConfig01 FROM ['+@comun+'].dbo.ejercici where predet=1'
				EXEC (@SentenciaSQL)
				set @GESTION  = (SELECT RUTA FROM ##pConfig01)
				SET @ejercicio = (SELECT [ANY] FROM ##pConfig01)
				set @LETRA = RIGHT(@GESTION,2)
				DROP TABLE ##pConfig01 
			END TRY BEGIN CATCH END CATCH

			if exists (select * from Configuracion)
			update Configuracion set Ejercicio=@ejercicio, Gestion=@GESTION, Letra=@LETRA, Comun=@comun, Campos=@CAMPOS, Lotes=@LOTES
								   , Empresa=@empresa, NombreEmpresa=@nombreEmpresa
			else insert into Configuracion (Ejercicio, Gestion, Letra, Comun, Campos, Lotes, Empresa, NombreEmpresa)
				 values (@ejercicio, @GESTION, @LETRA, @comun, @CAMPOS, @LOTES, @empresa, @nombreEmpresa)
		END


	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	if @modo='crearVistas' BEGIN 		
		EXEC @sp=pAppVistas 
		if @sp<>-1 	select 'Error! SP: pAppVistas!'  as JAVASCRIPT 	
		return -1
	END

	if @modo='crearProcedimientos' BEGIN 		
		--EXEC @sp=pAppProcedimientos 
		--if @sp<>-1 	select 'Error! SP: pAppProcedimientos!'  as JAVASCRIPT 	
		return -1
	END

	if @modo='crearFunciones' BEGIN 		
		EXEC @sp=pAppFunciones 
		if @sp<>-1 	select 'Error! SP: pAppFunciones!'  as JAVASCRIPT 	
		return -1
	END

	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	


	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	if @modo='objetoIO' BEGIN 		
		if exists (select * from ConfigApp where objeto=@objeto) 
			update ConfigApp set valor=@valor, activo=@confOpIO where objeto=@objeto
        else insert into ConfigApp (objeto,valor,activo) values (@objeto, @valor, @confOpIO) 
	END		

	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	

	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	select 'OK' as JAVASCRIPT        --   select * from Configuracion    --   select * from ConfigApp

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX) = ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Modificando Procedimiento [dbo].[pAlbaranDeVenta]...';


GO

ALTER PROCEDURE [dbo].[pAlbaranDeVenta] (@parametros varchar(max))
AS
BEGIN TRY		
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare   @numero varchar(20)	= (select JSON_VALUE(@parametros,'$.numero'))
			, @letra char(2)		= (select JSON_VALUE(@parametros,'$.letra'))
			, @bultos varchar(5)	= (select JSON_VALUE(@parametros,'$.bultos'))
			, @cajas varchar(5)		= (select JSON_VALUE(@parametros,'$.cajas'))
			, @impAlb int			= isnull(cast((select JSON_VALUE(@parametros,'$.impAlb')) as int),0)
			, @impEti int			= isnull(cast((select JSON_VALUE(@parametros,'$.impEti')) as int),0)
			, @FacturaDirecta int	= isnull(cast((select JSON_VALUE(@parametros,'$.FacturaDirecta')) as int),0)
			, @usuarioSesion varchar(50)= (select JSON_VALUE(@parametros,'$.usuarioSesion'))			

	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EMPRESA char(2), @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select @EMPRESA=Empresa, @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------

	declare @sql varchar(max)
	declare @nu varchar(50) = replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 

	declare   @nEti int = 1
			, @CrearAV varchar(max) = ''
			, @printerEti varchar(100) = ''
			, @printerDoc varchar(100) = ''
			, @reportEti  varchar(100) = ''
			, @reportAlb  varchar(100) = ''
			, @reportFac  varchar(100) = ''
			, @msj varchar(max) = ''
			, @unico varchar(50) = ''
			, @elPDF varchar(100) = ''
			, @error int = 0
			, @cliente varchar(20)
			, @copiasAlb int = 1
			, @copiasFac int = 1	
	

	update [Traspaso_Temporal] set bultos=@bultos, eticajas=@cajas, impAlb=@impAlb, impEti=@impEti where empresa=@EMPRESA and NUMERO=@numero and LETRA=@letra	

	-- Equipo del Usuario -- (DISTECO)
	declare @EquipoUsuario varchar(50) = 'WebApp'
	if @usuarioSesion='1544458254550078' set @EquipoUsuario='PC-CAMARA2'
	if @usuarioSesion='1544458275396522' set @EquipoUsuario='DS4JX7688'

	-- obtener impresoras y reports ---------------------------------------------------------------------------------
	if @impEti=1 begin
		set @printerEti = (select isnull(impEti,'') from [usuarios_impresoras] where usuario=@usuario)		
		set @reportEti  = (select isnull(reportEtiquetas,'') from [usuarios_reports] where usuario=@usuario)
		if @reportEti='' set @reportEti = (select isnull(report,'') from [ConfigIdiomaReport] where idioma = 
											(select top 1 IDIOMA_IMP from d_Busca_PV 
											where concat(empresa,numero,letra) collate Modern_Spanish_CI_AI=concat(@EMPRESA,@numero,@letra)) collate Modern_Spanish_CI_AI
											and LEFT(tipo,2)='03'
										  )
		if @reportEti='' set @reportEti = (select isnull(report,'') from [ConfigIdiomaReport] where idioma='000' and LEFT(tipo,2)='03')
		if  @printerEti='' or @reportEti='' set @error=1
	end
	
	if @impAlb=1 begin
		set @printerDoc = (select impDocu from [usuarios_impresoras] where usuario=@usuario)
		set @reportAlb  = (select reportAlbaran from [usuarios_reports] where usuario=@usuario)
		if	@reportAlb='' set @reportAlb = (select isnull(report,'') from [ConfigIdiomaReport] where idioma = 
											(select top 1 IDIOMA_IMP from d_Busca_PV 
											where concat(empresa,numero,letra) collate Modern_Spanish_CI_AI=concat(@EMPRESA,@numero,@letra)) collate Modern_Spanish_CI_AI
											and LEFT(tipo,2)='01'
										  )
		if @reportAlb='' set @reportAlb = (select isnull(report,'') from [ConfigIdiomaReport] where idioma='000' and LEFT(tipo,2)='01')			
		if @printerDoc='' or @reportAlb=''set @error=2
	end
	
	if @FacturaDirecta=1 begin
		set @printerDoc = (select impDocu from [usuarios_impresoras] where usuario=@usuario)
		set @reportFac  = (select reportFactura from [usuarios_reports] where usuario=@usuario)
		if	@reportFac='' set @reportFac = (select isnull(report,'') from [ConfigIdiomaReport] where idioma = 
											(select top 1 IDIOMA_IMP from d_Busca_PV 
											where concat(empresa,numero,letra) collate Modern_Spanish_CI_AI=concat(@EMPRESA,@numero,@letra)) collate Modern_Spanish_CI_AI
											and LEFT(tipo,2)='02'
										  )
		if @reportFac='' set @reportFac = (select isnull(report,'') from [ConfigIdiomaReport] where idioma='000' and LEFT(tipo,2)='02')
		if  @printerDoc='' or @reportFac=''set @error=3
	end

	--	actualizar C_PEDIVE TRASPASADO=1 y eliminar registro tabla [Traspasar_Pedido]
		EXEC('update ['+@GESTION+'].dbo.c_pedive set TRASPASADO=1 where CONCAT(EMPRESA,NUMERO,LETRA)=CONCAT('''+@EMPRESA+''','''+@numero+''','''+@letra+''')')
		delete [Traspasar_Pedido] where CONCAT(empresa,numero,letra)=CONCAT(@EMPRESA,@numero,@letra)

	-- --------------------------------------------------------------------------------------------------------------
	--	generar albarán de venta 
		declare @sp int  
		EXEC @sp = [pCrearAV] @EMPRESA, @numero, @letra, @EquipoUsuario, @impAlb, @impEti, @CrearAV output, @nEti
		if @sp<>-1 BEGIN
			select CONCAT('{"error":"',@CrearAV,'"}') as JAVASCRIPT
			return -1
		END
	-- --------------------------------------------------------------------------------------------------------------

	-- tabla temporal para recopilación de datos ------------------------------------------------------
	declare @tablaTemp TABLE (C_ALBAVEN int, copiasFac int)
	SET @sql = 'select C_ALBAVEN as C_ALBAVEN
				, (select COPIA_FRA from ['+@GESTION+'].dbo.clientes where CODIGO='''+@cliente+''') as copiasFac
				from ['+@GESTION+'].dbo.factucnf'
	INSERT @tablaTemp EXEC(@sql)
	SELECT @copiasAlb=C_ALBAVEN, @copiasFac=copiasFac FROM @tablaTemp
	delete @tablaTemp

	-- guardar datos en tabla de impresión --------------------------------------------------------------------------
	if @error=0 begin
		if @impEti=1 or (select activo from ConfigApp where objeto='impEtiquetas')=1 begin
			set @unico = 'E' + replace(replace(replace(replace(replace(convert(varchar(50),getdate(),121),' ',''),'/',''),':',''),'.',''),'-','')
			set @elPDF = replace(CONCAT('Etiquetas_',@EMPRESA,@letra,@CrearAV,'.pdf'),' ','_');
			insert into MI_ImpresionPDF (id, usuario, terminal, tipo, pdf, impresora, crReport, crNumero, crEmpresa, crLetra, bultos) 
			values (@unico,@usuario,'WebApp','Etiquetas',@elPDF,@printerEti,@reportEti,@CrearAV,@EMPRESA,@letra,@bultos)
		end

		if @impAlb=1 or (select activo from ConfigApp where objeto='impAlbGen')=1 begin 
			set @unico ='A' +  replace(replace(replace(replace(replace(convert(varchar(50),getdate(),121),' ',''),'/',''),':',''),'.',''),'-','')
			set @elPDF = replace(CONCAT('Albaran',@EMPRESA,@letra,@CrearAV,'.pdf'),' ','_');
			insert into MI_ImpresionPDF (id, usuario, terminal, tipo, pdf, impresora, crReport, crNumero, crEmpresa, crLetra) 
			values (@unico,@usuario,'WebApp','Albaran',@elPDF,@printerDoc,@reportAlb,ltrim(rtrim(@CrearAV)),@EMPRESA,@letra)
		end

		if @FacturaDirecta=1 begin
			set @cliente = (select CLIENTE from vFacturas where empresa=@EMPRESA and numfra=CONCAT(@letra,@numero))
			set @unico = 'F' + replace(replace(replace(replace(replace(convert(varchar(50),getdate(),121),' ',''),'/',''),':',''),'.',''),'-','')
			set @elPDF = replace(CONCAT('"Factura"',@EMPRESA,@letra,@CrearAV,'.pdf'),' ','_');
			insert into MI_ImpresionPDF (id, usuario, terminal, tipo, pdf, impresora, crReport, crNumero, crEmpresa, crLetra) 
			values (@unico,@usuario,'WebApp','Factura',@elPDF,@printerDoc,@reportFac,ltrim(rtrim(@CrearAV)),@EMPRESA,@letra)
		end
	end
	
	select @CrearAV as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	select CONCAT('{"error":"', replace(ERROR_MESSAGE(),'"','-'),'"}') as JAVASCRIPT
	RETURN 0
END CATCH
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET XACT_ABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

BEGIN TRANSACTION

EXEC sp_MSforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT all'

EXEC sp_MSforeachtable 'ALTER TABLE ? DISABLE TRIGGER all'

-- Data merge


-- Merlos
truncate table [AuxiliarBultos]
insert into [AuxiliarBultos] (NumBultos)
values (1)
		,(2),(2)
		,(3),(3),(3)
		,(4),(4),(4),(4)
		,(5),(5),(5),(5),(5)
		,(6),(6),(6),(6),(6),(6)
		,(7),(7),(7),(7),(7),(7),(7)
		,(8),(8),(8),(8),(8),(8),(8),(8)
		,(9),(9),(9),(9),(9),(9),(9),(9),(9)
		,(10),(10),(10),(10),(10),(10),(10),(10),(10),(10)


if not exists (select objeto from [ConfigApp] where objeto='FactCajas')		insert into [ConfigApp] (objeto, valor, activo)	values ('FactCajas','',0)
if not exists (select objeto from [ConfigApp] where objeto='ImpAlbFact')	insert into [ConfigApp] (objeto, valor, activo)	values ('ImpAlbFact','',0)
if not exists (select objeto from [ConfigApp] where objeto='ImpAlbGen')		insert into [ConfigApp] (objeto, valor, activo)	values ('ImpAlbGen','',0)
if not exists (select objeto from [ConfigApp] where objeto='ImpEtiquetas')	insert into [ConfigApp] (objeto, valor, activo)	values ('ImpEtiquetas','',0)
if not exists (select objeto from [ConfigApp] where objeto='ResLin')		insert into [ConfigApp] (objeto, valor, activo)	values ('ResLin','',0)
if not exists (select objeto from [ConfigApp] where objeto='SeriesFiltro')	insert into [ConfigApp] (objeto, valor, activo)	values ('SeriesFiltro','',0)
	

if not exists (select email from [ConfigEmail])	
insert into [ConfigEmail] (email, smtp, puerto, usuario, pswrd, tls)
values ('noreply@cliseller.com','smtp.ionos.es','587','noreply@cliseller.com','4N6Brt6IH4DKNTYzZfE@',1)


if not exists (select * from [Idiomas_Idiomas] where nombre='Por Defecto') insert into [Idiomas_Idiomas] (Codigo, Nombre) values ('000','Por Defecto')


if not exists (select * from [Idiomas_Tipos] where nombre='Albarán')	insert into [Idiomas_Tipos] (Codigo, Nombre) values ('01','Albarán')
if not exists (select * from [Idiomas_Tipos] where nombre='Factura')	insert into [Idiomas_Tipos] (Codigo, Nombre) values ('02','Factura')
if not exists (select * from [Idiomas_Tipos] where nombre='Etiquetas')	insert into [Idiomas_Tipos] (Codigo, Nombre) values ('03','Etiquetas')


GO

EXEC sp_MSforeachtable 'ALTER TABLE ? ENABLE TRIGGER all'

EXEC sp_MSforeachtable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT ALL'

COMMIT TRANSACTION

GO

GO
PRINT N'Actualización completada.';


GO
