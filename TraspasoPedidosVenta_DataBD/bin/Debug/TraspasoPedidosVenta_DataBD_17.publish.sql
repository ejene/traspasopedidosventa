﻿/*
Script de implementación para TraspasoPedidosVenta_DataBD

Una herramienta generó este código.
Los cambios realizados en este archivo podrían generar un comportamiento incorrecto y se perderán si
se vuelve a generar el código.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "TraspasoPedidosVenta_DataBD"
:setvar DefaultFilePrefix "TraspasoPedidosVenta_DataBD"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEW\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEW\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detectar el modo SQLCMD y deshabilitar la ejecución del script si no se admite el modo SQLCMD.
Para volver a habilitar el script después de habilitar el modo SQLCMD, ejecute lo siguiente:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'El modo SQLCMD debe estar habilitado para ejecutar correctamente este script.';
        SET NOEXEC ON;
    END


GO
USE [master];


GO

IF (DB_ID(N'$(DatabaseName)') IS NOT NULL) 
BEGIN
    ALTER DATABASE [$(DatabaseName)]
    SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
    DROP DATABASE [$(DatabaseName)];
END

GO
PRINT N'Creando la base de datos $(DatabaseName)...'
GO
CREATE DATABASE [$(DatabaseName)]
    ON 
    PRIMARY(NAME = [$(DatabaseName)], FILENAME = N'$(DefaultDataPath)$(DefaultFilePrefix)_Primary.mdf')
    LOG ON (NAME = [$(DatabaseName)_log], FILENAME = N'$(DefaultLogPath)$(DefaultFilePrefix)_Primary.ldf') COLLATE SQL_Latin1_General_CP1_CI_AS
GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_CLOSE OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
USE [$(DatabaseName)];


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ANSI_NULLS ON,
                ANSI_PADDING ON,
                ANSI_WARNINGS ON,
                ARITHABORT ON,
                CONCAT_NULL_YIELDS_NULL ON,
                NUMERIC_ROUNDABORT OFF,
                QUOTED_IDENTIFIER ON,
                ANSI_NULL_DEFAULT ON,
                CURSOR_DEFAULT LOCAL,
                RECOVERY SIMPLE,
                CURSOR_CLOSE_ON_COMMIT OFF,
                AUTO_CREATE_STATISTICS ON,
                AUTO_SHRINK OFF,
                AUTO_UPDATE_STATISTICS ON,
                RECURSIVE_TRIGGERS OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ALLOW_SNAPSHOT_ISOLATION OFF;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET READ_COMMITTED_SNAPSHOT OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_UPDATE_STATISTICS_ASYNC OFF,
                PAGE_VERIFY NONE,
                DATE_CORRELATION_OPTIMIZATION OFF,
                DISABLE_BROKER,
                PARAMETERIZATION SIMPLE,
                SUPPLEMENTAL_LOGGING OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF IS_SRVROLEMEMBER(N'sysadmin') = 1
    BEGIN
        IF EXISTS (SELECT 1
                   FROM   [master].[dbo].[sysdatabases]
                   WHERE  [name] = N'$(DatabaseName)')
            BEGIN
                EXECUTE sp_executesql N'ALTER DATABASE [$(DatabaseName)]
    SET TRUSTWORTHY OFF,
        DB_CHAINING OFF 
    WITH ROLLBACK IMMEDIATE';
            END
    END
ELSE
    BEGIN
        PRINT N'No se puede modificar la configuración de la base de datos. Debe ser un administrador del sistema para poder aplicar esta configuración.';
    END


GO
IF IS_SRVROLEMEMBER(N'sysadmin') = 1
    BEGIN
        IF EXISTS (SELECT 1
                   FROM   [master].[dbo].[sysdatabases]
                   WHERE  [name] = N'$(DatabaseName)')
            BEGIN
                EXECUTE sp_executesql N'ALTER DATABASE [$(DatabaseName)]
    SET HONOR_BROKER_PRIORITY OFF 
    WITH ROLLBACK IMMEDIATE';
            END
    END
ELSE
    BEGIN
        PRINT N'No se puede modificar la configuración de la base de datos. Debe ser un administrador del sistema para poder aplicar esta configuración.';
    END


GO
ALTER DATABASE [$(DatabaseName)]
    SET TARGET_RECOVERY_TIME = 0 SECONDS 
    WITH ROLLBACK IMMEDIATE;


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET FILESTREAM(NON_TRANSACTED_ACCESS = OFF),
                CONTAINMENT = NONE 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_CREATE_STATISTICS ON(INCREMENTAL = OFF),
                MEMORY_OPTIMIZED_ELEVATE_TO_SNAPSHOT = OFF,
                DELAYED_DURABILITY = DISABLED 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET QUERY_STORE (QUERY_CAPTURE_MODE = ALL, DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_PLANS_PER_QUERY = 200, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 367), MAX_STORAGE_SIZE_MB = 100) 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET QUERY_STORE = OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
        ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
        ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
        ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
        ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
        ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
        ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
        ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
    END


GO
IF fulltextserviceproperty(N'IsFulltextInstalled') = 1
    EXECUTE sp_fulltext_database 'enable';


GO
PRINT N'Creando Tabla [dbo].[AuxiliarBultos]...';


GO
CREATE TABLE [dbo].[AuxiliarBultos] (
    [NumBultos] INT NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[basculas]...';


GO
CREATE TABLE [dbo].[basculas] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [bascula]           VARCHAR (50)     NULL,
    [tipoConexion]      VARCHAR (50)     NOT NULL,
    [ip]                VARCHAR (50)     NULL,
    [puerto]            VARCHAR (50)     NULL,
    [sentencia]         VARCHAR (100)    NULL,
    [activa]            BIT              NOT NULL,
    [Baudios]           VARCHAR (50)     NULL,
    [Paridad]           VARCHAR (50)     NULL,
    [Data]              VARCHAR (50)     NULL,
    [Stop]              VARCHAR (50)     NULL,
    [Xon]               VARCHAR (50)     NULL,
    [Peso]              VARCHAR (50)     NULL,
    [FechaInsertUpdate] DATETIME         NOT NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[ConfigApp]...';


GO
CREATE TABLE [dbo].[ConfigApp] (
    [objeto] VARCHAR (50)  NOT NULL,
    [valor]  VARCHAR (MAX) NULL,
    [activo] TINYINT       NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[ConfigEmail]...';


GO
CREATE TABLE [dbo].[ConfigEmail] (
    [Id]      INT            IDENTITY (1, 1) NOT NULL,
    [email]   NVARCHAR (250) NULL,
    [smtp]    NVARCHAR (250) NULL,
    [puerto]  NVARCHAR (10)  NULL,
    [usuario] NVARCHAR (50)  NULL,
    [pswrd]   NVARCHAR (100) NULL,
    [tls]     BIT            NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC) ON [PRIMARY]
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[ConfigIdiomaReport]...';


GO
CREATE TABLE [dbo].[ConfigIdiomaReport] (
    [idioma] VARCHAR (50)  NOT NULL,
    [nombre] VARCHAR (100) NULL,
    [tipo]   VARCHAR (50)  NOT NULL,
    [report] VARCHAR (200) NOT NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[ConfigPersonalizaciones]...';


GO
CREATE TABLE [dbo].[ConfigPersonalizaciones] (
    [Id]                INT            IDENTITY (1, 1) NOT NULL,
    [Empresa]           NVARCHAR (100) NULL,
    [Activo]            BIT            NULL,
    [FechaInsertUpdate] DATETIME       NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[ConfigSeries]...';


GO
CREATE TABLE [dbo].[ConfigSeries] (
    [objeto] VARCHAR (50) NULL,
    [serie]  CHAR (2)     NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[Configuracion]...';


GO
CREATE TABLE [dbo].[Configuracion] (
    [id]                INT           IDENTITY (1, 1) NOT NULL,
    [Ejercicio]         VARCHAR (4)   NULL,
    [Gestion]           VARCHAR (6)   NULL,
    [Letra]             CHAR (2)      NULL,
    [Comun]             CHAR (8)      NULL,
    [Campos]            VARCHAR (8)   NULL,
    [Lotes]             VARCHAR (8)   NULL,
    [Empresa]           CHAR (2)      NULL,
    [NombreEmpresa]     VARCHAR (100) NULL,
    [FechaInsertUpdate] DATETIME      NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[EnUso]...';


GO
CREATE TABLE [dbo].[EnUso] (
    [EnUso] BIT        NULL,
    [fecha] DATETIME   NULL,
    [TIPO]  NCHAR (10) NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[Idiomas_Idiomas]...';


GO
CREATE TABLE [dbo].[Idiomas_Idiomas] (
    [Id]                INT           IDENTITY (1, 1) NOT NULL,
    [Codigo]            VARCHAR (50)  NULL,
    [Nombre]            VARCHAR (100) NULL,
    [FechaInsertUpdate] DATETIME      NOT NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[Idiomas_Tipos]...';


GO
CREATE TABLE [dbo].[Idiomas_Tipos] (
    [Id]                INT           IDENTITY (1, 1) NOT NULL,
    [Codigo]            VARCHAR (50)  NULL,
    [Nombre]            VARCHAR (100) NULL,
    [FechaInsertUpdate] DATETIME      NOT NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[LOG_CREARAV]...';


GO
CREATE TABLE [dbo].[LOG_CREARAV] (
    [VALOR]   NVARCHAR (MAX) NULL,
    [FECHA]   DATETIME       NULL,
    [EMPRESA] CHAR (2)       NULL,
    [NUMERO]  CHAR (20)      NULL,
    [LETRA]   CHAR (2)       NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[Log_Errores_pCrearAv]...';


GO
CREATE TABLE [dbo].[Log_Errores_pCrearAv] (
    [MensajeError] NVARCHAR (MAX) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[MI_Impresion]...';


GO
CREATE TABLE [dbo].[MI_Impresion] (
    [terminal] CHAR (50) NULL,
    [empresa]  CHAR (2)  NULL,
    [numero]   CHAR (10) NULL,
    [letra]    CHAR (2)  NULL,
    [factura]  CHAR (10) NULL,
    [imprimir] BIT       NULL,
    [imp_eti]  BIT       NULL,
    [bultos]   CHAR (7)  NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[MI_ImpresionPDF]...';


GO
CREATE TABLE [dbo].[MI_ImpresionPDF] (
    [id]                VARCHAR (50)   NOT NULL,
    [usuario]           VARCHAR (50)   NULL,
    [terminal]          VARCHAR (100)  NULL,
    [tipo]              VARCHAR (50)   NULL,
    [pdf]               VARCHAR (250)  NULL,
    [impresora]         VARCHAR (250)  NULL,
    [crReport]          VARCHAR (250)  NULL,
    [crNumero]          VARCHAR (50)   NULL,
    [crEmpresa]         VARCHAR (10)   NULL,
    [crLetra]           VARCHAR (10)   NULL,
    [bultos]            INT            NULL,
    [report]            INT            NULL,
    [copias]            INT            NULL,
    [orientacion]       CHAR (1)       NULL,
    [log]               NVARCHAR (MAX) NULL,
    [fechaInsertUpdate] DATETIME       NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[Pedidos_Contactos]...';


GO
CREATE TABLE [dbo].[Pedidos_Contactos] (
    [id]                INT          IDENTITY (1, 1) NOT NULL,
    [IDPEDIDO]          VARCHAR (18) NULL,
    [Cliente]           CHAR (8)     NULL,
    [Contacto]          INT          NULL,
    [FechaInsertUpdate] DATETIME     NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[Pedidos_Familias]...';


GO
CREATE TABLE [dbo].[Pedidos_Familias] (
    [EJERCICIO] CHAR (4)  NULL,
    [EMPRESA]   CHAR (2)  NULL,
    [NUMERO]    CHAR (10) NULL,
    [LETRA]     CHAR (2)  NULL,
    [FAMILIA]   CHAR (4)  NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[pesos]...';


GO
CREATE TABLE [dbo].[pesos] (
    [id]   VARCHAR (50)   NOT NULL,
    [peso] VARCHAR (1000) NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[Traspasar_Pedido]...';


GO
CREATE TABLE [dbo].[Traspasar_Pedido] (
    [empresa] CHAR (2)  NULL,
    [letra]   CHAR (2)  NULL,
    [numero]  CHAR (10) NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[Traspaso_Temporal]...';


GO
CREATE TABLE [dbo].[Traspaso_Temporal] (
    [sesion]            VARCHAR (50)    NOT NULL,
    [id]                VARCHAR (50)    NULL,
    [empresa]           CHAR (2)        NOT NULL,
    [numero]            CHAR (10)       NOT NULL,
    [letra]             VARCHAR (10)    NOT NULL,
    [linea]             INT             NOT NULL,
    [articulo]          VARCHAR (20)    NULL,
    [lote]              VARCHAR (50)    NOT NULL,
    [cajas]             NUMERIC (10, 2) NULL,
    [unidades]          NUMERIC (15, 6) NULL,
    [peso]              VARCHAR (50)    NULL,
    [cliente]           VARCHAR (50)    NULL,
    [ruta]              VARCHAR (50)    NULL,
    [almacen]           CHAR (2)        NULL,
    [caducidad]         SMALLDATETIME   NULL,
    [bultos]            CHAR (7)        NULL,
    [impAlb]            INT             NULL,
    [impEti]            INT             NULL,
    [linea_av]          INT             NULL,
    [eticajas]          CHAR (7)        NULL,
    [FechaInsertUpdate] SMALLDATETIME   NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[usuarios_basculas]...';


GO
CREATE TABLE [dbo].[usuarios_basculas] (
    [usuario] VARCHAR (50) NULL,
    [bascula] VARCHAR (50) NULL,
    [predet]  BIT          NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[usuarios_impresoras]...';


GO
CREATE TABLE [dbo].[usuarios_impresoras] (
    [id]      VARCHAR (50)  NULL,
    [usuario] VARCHAR (50)  NULL,
    [impDocu] VARCHAR (100) NULL,
    [impEti]  VARCHAR (100) NULL
) ON [PRIMARY];


GO
PRINT N'Creando Tabla [dbo].[usuarios_reports]...';


GO
CREATE TABLE [dbo].[usuarios_reports] (
    [id]              VARCHAR (50)  NULL,
    [usuario]         VARCHAR (50)  NULL,
    [reportAlbaran]   VARCHAR (100) NULL,
    [reportFactura]   VARCHAR (100) NULL,
    [reportEtiquetas] VARCHAR (100) NULL
) ON [PRIMARY];


GO
PRINT N'Creando Restricción DEFAULT restricción sin nombre en [dbo].[basculas]...';


GO
ALTER TABLE [dbo].[basculas]
    ADD DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creando Restricción DEFAULT restricción sin nombre en [dbo].[basculas]...';


GO
ALTER TABLE [dbo].[basculas]
    ADD DEFAULT ('ETHERNET') FOR [tipoConexion];


GO
PRINT N'Creando Restricción DEFAULT restricción sin nombre en [dbo].[basculas]...';


GO
ALTER TABLE [dbo].[basculas]
    ADD DEFAULT ('192.168.0.101') FOR [ip];


GO
PRINT N'Creando Restricción DEFAULT restricción sin nombre en [dbo].[basculas]...';


GO
ALTER TABLE [dbo].[basculas]
    ADD DEFAULT ((6000)) FOR [puerto];


GO
PRINT N'Creando Restricción DEFAULT restricción sin nombre en [dbo].[basculas]...';


GO
ALTER TABLE [dbo].[basculas]
    ADD DEFAULT (('$')) FOR [sentencia];


GO
PRINT N'Creando Restricción DEFAULT restricción sin nombre en [dbo].[basculas]...';


GO
ALTER TABLE [dbo].[basculas]
    ADD DEFAULT ((0)) FOR [activa];


GO
PRINT N'Creando Restricción DEFAULT restricción sin nombre en [dbo].[basculas]...';


GO
ALTER TABLE [dbo].[basculas]
    ADD DEFAULT (getdate()) FOR [FechaInsertUpdate];


GO
PRINT N'Creando Restricción DEFAULT <sin nombre>...';


GO
ALTER TABLE [dbo].[ConfigApp]
    ADD DEFAULT ((0)) FOR [activo];


GO
PRINT N'Creando Restricción DEFAULT <sin nombre>...';


GO
ALTER TABLE [dbo].[ConfigEmail]
    ADD DEFAULT ((0)) FOR [tls];


GO
PRINT N'Creando Restricción DEFAULT [dbo].[DF_ConfigPersonalizaciones_FechaInsertUpdate]...';


GO
ALTER TABLE [dbo].[ConfigPersonalizaciones]
    ADD CONSTRAINT [DF_ConfigPersonalizaciones_FechaInsertUpdate] DEFAULT (getdate()) FOR [FechaInsertUpdate];


GO
PRINT N'Creando Restricción DEFAULT restricción sin nombre en [dbo].[Configuracion]...';


GO
ALTER TABLE [dbo].[Configuracion]
    ADD DEFAULT (getdate()) FOR [FechaInsertUpdate];


GO
PRINT N'Creando Restricción DEFAULT [dbo].[DF_Idioma_Idiomas_FechaInsertUpdate]...';


GO
ALTER TABLE [dbo].[Idiomas_Idiomas]
    ADD CONSTRAINT [DF_Idioma_Idiomas_FechaInsertUpdate] DEFAULT (getdate()) FOR [FechaInsertUpdate];


GO
PRINT N'Creando Restricción DEFAULT [dbo].[DF_Idiomas_Tipos_FechaInsertUpdate]...';


GO
ALTER TABLE [dbo].[Idiomas_Tipos]
    ADD CONSTRAINT [DF_Idiomas_Tipos_FechaInsertUpdate] DEFAULT (getdate()) FOR [FechaInsertUpdate];


GO
PRINT N'Creando Restricción DEFAULT <sin nombre>...';


GO
ALTER TABLE [dbo].[MI_ImpresionPDF]
    ADD DEFAULT ((1)) FOR [bultos];


GO
PRINT N'Creando Restricción DEFAULT <sin nombre>...';


GO
ALTER TABLE [dbo].[MI_ImpresionPDF]
    ADD DEFAULT ((0)) FOR [report];


GO
PRINT N'Creando Restricción DEFAULT <sin nombre>...';


GO
ALTER TABLE [dbo].[MI_ImpresionPDF]
    ADD DEFAULT ((1)) FOR [copias];


GO
PRINT N'Creando Restricción DEFAULT <sin nombre>...';


GO
ALTER TABLE [dbo].[MI_ImpresionPDF]
    ADD DEFAULT ('V') FOR [orientacion];


GO
PRINT N'Creando Restricción DEFAULT <sin nombre>...';


GO
ALTER TABLE [dbo].[MI_ImpresionPDF]
    ADD DEFAULT (getdate()) FOR [fechaInsertUpdate];


GO
PRINT N'Creando Restricción DEFAULT <sin nombre>...';


GO
ALTER TABLE [dbo].[Pedidos_Contactos]
    ADD DEFAULT (getdate()) FOR [FechaInsertUpdate];


GO
PRINT N'Creando Restricción DEFAULT <sin nombre>...';


GO
ALTER TABLE [dbo].[Traspaso_Temporal]
    ADD DEFAULT ((0)) FOR [impAlb];


GO
PRINT N'Creando Restricción DEFAULT <sin nombre>...';


GO
ALTER TABLE [dbo].[Traspaso_Temporal]
    ADD DEFAULT ((0)) FOR [impEti];


GO
PRINT N'Creando Restricción DEFAULT <sin nombre>...';


GO
ALTER TABLE [dbo].[Traspaso_Temporal]
    ADD DEFAULT (getdate()) FOR [FechaInsertUpdate];


GO
PRINT N'Creando Restricción DEFAULT <sin nombre>...';


GO
ALTER TABLE [dbo].[usuarios_basculas]
    ADD DEFAULT ((0)) FOR [predet];


GO
PRINT N'Creando Función [dbo].[funPrintError]...';


GO
CREATE FUNCTION [dbo].[funPrintError](@Message nvarchar(2048),@NumError int,@Procedure nvarchar(126),@IdProcCall int,@Line int) 

RETURNS NVARCHAR(MAX) AS
BEGIN
      DECLARE @CatchError NVARCHAR(MAX) 
      SET @CatchError=CASE WHEN @NumError=3609 OR @NumError=266 THEN '' ELSE  ISNULL(@Message,'') + NCHAR(13) END  +  CASE WHEN @Line<>0 THEN 
            '[Trace: ' + CASE WHEN ISNULL(@Procedure,'')= OBJECT_NAME(@IdProcCall) THEN '' ELSE OBJECT_NAME(@IdProcCall)+'.' END 
                                                           + ISNULL(@Procedure,'')  + '.' + ISNULL(CAST(@Line AS NVARCHAR(MAX)),'') + ']' 
                                                           ELSE '[' + OBJECT_NAME(@IdProcCall) +']'  END
      RETURN @CatchError

END
GO
PRINT N'Creando Procedimiento [dbo].[pActualizaLineaTraspasoLote]...';


GO
CREATE PROCEDURE [dbo].[pActualizaLineaTraspasoLote] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare	  @nu varchar(50) = isnull((select JSON_VALUE(@parametros,'$.nu')),'')
			, @pedido varchar(50) = isnull((select JSON_VALUE(@parametros,'$.pedido')),'')
			, @serie varchar(10) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
			, @articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
			, @almacen varchar(10) = isnull((select JSON_VALUE(@parametros,'$.almacen')),'')
			, @lote varchar(50) = isnull((select JSON_VALUE(@parametros,'$.lote')),'')
			, @linea int = cast(isnull((select JSON_VALUE(@parametros,'$.linea')),'0') as int)
			, @unidades int = cast(isnull((select JSON_VALUE(@parametros,'$.unidades')),'0') as int)
			, @peso varchar(10) = isnull((select JSON_VALUE(@parametros,'$.peso')),'')
			, @cajas int = cast(isnull((select JSON_VALUE(@parametros,'$.cajas')),'0') as int)
	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EMPRESA char(2), @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select  @EMPRESA=Empresa, @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------	

	declare   @cliente varchar(20)
			, @ruta char(2)
			, @unicaja int
			, @caducidad smalldatetime
			, @sql varchar(max)

	-- tabla temporal para recopilación de datos ------------------------------------------------------
	declare @tablaTemp TABLE (CLIENTE nvarchar(20), RUTA varchar(10), UNICAJA int, caducidad smalldatetime)
	SET @sql = 'select top 1 CLIENTE
				, (select top 1 ruta from Detalle_Pedidos where numero='''+@pedido+''') as RUTA
				, (select cast(UNICAJA as varchar) from ['+@GESTION+'].dbo.articulo where CODIGO='''+@articulo+''') as UNICAJA
				, (select CADUCIDAD from ['+@LOTES+'].[dbo].[stocklotes] where ALMACEN='''+@almacen+''' and ARTICULO='''+@articulo+''' and LOTE='''+@lote+''') as caducidad
				from Detalle_Pedidos where numero='''+@pedido+'''  and LETRA='''+@serie+''''
	INSERT @tablaTemp EXEC(@sql)
	SELECT @cliente=CLIENTE, @ruta=RUTA, @unicaja=UNICAJA, @caducidad=caducidad FROM @tablaTemp
	delete @tablaTemp

	-- si existe la linea la borramos
	delete from [Traspaso_Temporal] 
	where empresa=@EMPRESA and numero=@pedido and letra=@serie and linea=@linea and lote=@lote

	-- guardamos la linea en la tabla temporal
	insert into [Traspaso_Temporal] 
	(sesion, id, empresa, numero, letra, linea, articulo, lote, cajas, unidades, peso, cliente, ruta, almacen, caducidad) 
	values (@usuario, @nu, @EMPRESA, @pedido, @serie, @linea, @articulo, @lote, @cajas, @unidades, @peso, @cliente, @ruta
	, @almacen, @caducidad)

	select @ruta as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select @CatchError as JAVASCRIPT
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pActualizaLineaTraspasoSinLote]...';


GO
CREATE PROCEDURE [dbo].[pActualizaLineaTraspasoSinLote] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare	  @pedido varchar(50) = isnull((select JSON_VALUE(@parametros,'$.pedido')),'')
			, @serie varchar(10) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
			, @lote varchar(50) = isnull((select JSON_VALUE(@parametros,'$.lote')),'')
			, @articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
			, @cajas int = cast(isnull((select JSON_VALUE(@parametros,'$.cajas')),'0') as int)
			, @unidades int = cast(isnull((select JSON_VALUE(@parametros,'$.unidades')),'0') as int)
			, @peso varchar(10) = isnull((select JSON_VALUE(@parametros,'$.peso')),'')
			, @linea int = cast(isnull((select JSON_VALUE(@parametros,'$.linea')),'0') as int)			
			
	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EJERCICIO char(4)
	declare @GESTION char(6)
	declare @COMUN char(8)
	declare @LOTES char(8)
	select @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	declare @empresa char(2) = (select Empresa from Configuracion)
		,	@sql varchar(max)      
	    
    declare @elCliente varchar(50), @laRuta varchar(10), @almacen varchar(10)
		,	@unicaja int=0
		,	@laCaducidad smalldatetime = cast(getdate() as smalldatetime)

	-- tabla temporal para recopilación de datos ------------------------------------------------------
	declare @tablaTemp TABLE (CLIENTE nvarchar(20), RUTA varchar(10), UNICAJA int, caducidad smalldatetime)
	SET @sql = 'select top 1 CLIENTE, RUTA
				, (select top 1 ruta from Detalle_Pedidos where numero='''+@pedido+''' and LETRA='''+@serie+''') as RUTA
				, (select UNICAJA from ['+@GESTION+'].dbo.articulo where CODIGO='''+@articulo+''') as UNICAJA
				, (select CADUCIDAD from ['+@LOTES+'].[dbo].[stocklotes] where ALMACEN='''+@almacen+''' and ARTICULO='''+@articulo+''') as caducidad
				from Detalle_Pedidos where numero='''+@pedido+'''  and LETRA='''+@serie+''''
	INSERT @tablaTemp EXEC(@sql)	
	SELECT @elCliente=CLIENTE, @laRuta=RUTA, @unicaja=UNICAJA, @laCaducidad=caducidad FROM @tablaTemp
	delete @tablaTemp
        
	-- si existe la linea en [Traspaso_Temporal] la borramos
	delete from [Traspaso_Temporal] where empresa=@empresa and numero=@pedido and letra=@serie and linea=@linea and lote=@lote
	
    -- guardamos la linea en la tabla temporal
	if(@cajas>0 or @unidades>0 or cast(@peso as numeric(15,6))>0.000) BEGIN
	declare @nu varchar(50) = replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 
	insert into [Traspaso_Temporal]
		(sesion, id, empresa, numero, letra, linea, articulo, lote, cajas, unidades, peso, cliente, ruta, almacen, caducidad) 
		values 
		(@usuario, @nu, @empresa, @pedido, @serie, @linea, @articulo, @lote, @cajas, @unidades, @peso, @elCliente, @laRuta, @almacen, @laCaducidad)
	END

	select '{"respuesta":"OK!","pedido":"'+@pedido+'","serie":"'+@serie+'","ruta":"'+@laRuta+'"}' as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pAppFunciones]...';


GO
CREATE PROCEDURE [dbo].[pAppFunciones]
AS
SET NOCOUNT ON
BEGIN TRY	
-- ============================
--	Variables de configuración
-- ============================
	declare @GESTION	varchar(20)	 set @GESTION = '['+(select top 1 GESTION from Configuracion)+']'
	declare @GESTIONAnt varchar(20)	 set @GESTIONAnt = '['+ cast(cast((select top 1 EJERCICIO from Configuracion) as int)-1 as varchar)+']'
	declare @CAMPOS		varchar(10)  set @CAMPOS = '['+(select top 1 isnull(CAMPOS,'xxxxxx') from Configuracion)+']'
	declare @COMUN		varchar(10)  set @COMUN = '['+(select top 1 COMUN from Configuracion)+']'
	declare @ANY		char(4)		 set @ANY = (select top 1 EJERCICIO from Configuracion)
	declare @ANYLETRA	char(6)		 set @ANYLETRA = (select top 1 EJERCICIO +''+ LETRA from Configuracion)
	declare @EMPRESA	char(2)		 set @EMPRESA   = (select top 1 EMPRESA from Configuracion)
	declare @Sentencia	varchar(MAX)
-- ====================================================================================================================================


IF EXISTS ( SELECT 1 FROM    Information_schema.Routines
            WHERE   Specific_schema = 'dbo' AND specific_name = 'ftDonamPreu' AND Routine_Type = 'FUNCTION'
			) drop function ftDonamPreu	


set @Sentencia = '	
CREATE FUNCTION [dbo].[ftDonamPreu](@EMPRESA CHAR(2),@CLIENTE CHAR(20),@ARTICULO CHAR(20)/*, @TALLA CHAR(8), @COLOR CHAR(8)*/, @TARIFA CHAR(2),@UNIDADES NUMERIC(18,4) ,@FECHA SMALLDATETIME )
RETURNS @aPreus TABLE 
(
   --Columns returned by the function
   PVP numeric(15,6) NOT NULL DEFAULT(0), 
   DTO1 NUMERIC(15,6) NOT NULL DEFAULT(0), 
   DTO2 NUMERIC(15,6) NOT NULL DEFAULT(0),
   IDERROR INT
)
AS 


BEGIN

    DECLARE @PREU NUMERIC(15,6), @DTO1 NUMERIC(15,6), @DTO2 NUMERIC(15,6), @FAMILIA VARCHAR(20), @MARCA VARCHAR(20), @SUBFAMILIA VARCHAR(20), @TIPO_PVP AS CHAR(8), @IMPORT NUMERIC(15,6), @PRECIOTARIFA NUMERIC(15,6), @ESCALADO BIT, @StrFecha VARCHAR(8), @CliOferta BIT, @TarifaForzada BIT, @TarifaFin char(2)
		
	-- Si el ERP trabaja con talla y color, hay que comantar la declaración de las variables talla y color y añadirlas a los parametros de la funcion
	DECLARE	@TALLA CHAR(8)='''', @COLOR CHAR(8)=''''
		
	--INICIALIZAR VARIABLES

	SET @PREU = 0.0000
	SET @PRECIOTARIFA=0.0000
	SET @DTO1 = 0.00
	SET @DTO2 = 0.00
	SET @IMPORT=0.00
	SET @FAMILIA=''''
	SET @MARCA=''''
	SET @SUBFAMILIA=''''
	SET @TIPO_PVP=''''



	--Mirar empresa y Articulo
	IF COALESCE(@ARTICULO,'''') = '''' OR COALESCE(@EMPRESA,'''')='''' -- Si no hay articulo devuleve 0s
	BEGIN
		INSERT @aPreus SELECT @PREU, @DTO1, @DTO2, 1
		RETURN 
	END
	-- Mirar Tarifa
	IF COALESCE(@TARIFA,'''') = '''' 
	BEGIN
		SELECT @TARIFA=TARIFA FROM '+@GESTION+'.DBO.CLIENTES WHERE CODIGO=@CLIENTE
		IF @TARIFA = '''' 
			SELECT @TARIFA=TARIFAPRET FROM '+@GESTION+'.dbo.FACTUCNF WHERE EMPRESA=@EMPRESA
	END
	'set @Sentencia = @Sentencia + '	
	-- Mirar si el cliente tiene marcado el check de ofertas
	SET @CliOferta = (SELECT OFERTA FROM '+@GESTION+'.DBO.CLIENTES WHERE CODIGO=@CLIENTE)
	-- Si no passan el cliente el valor de Clioferta será 1
	IF @CliOferta IS NULL
		set @CliOferta=1
	
	IF @FECHA IS NULL OR @FECHA=''''
		BEGIN
			SET @StrFecha=CONVERT(VARCHAR(8), GETDATE(), 112)  -- Mirar Fecha

		END
	ELSE
		BEGIN
			SET @StrFecha=CONVERT(VARCHAR(8), @FECHA, 112) 
		END
	IF @UNIDADES IS NULL SET @UNIDADES=0  -- Mirar Unidades
		
	SELECT @FAMILIA=FAMILIA, @MARCA=MARCA, @SUBFAMILIA=SUBFAMILIA FROM '+@GESTION+'.DBO.ARTICULO WHERE CODIGO=@ARTICULO -- Sacar Familia, Marca y Subfamilia del artículo
	
	DECLARE @TMP_MI_PREUS TABLE
		( TIPO_PVP CHAR(8) COLLATE Modern_Spanish_CS_AI, PVP NUMERIC(15,6), DTO1 NUMERIC(15,6), DTO2 NUMERIC(15,6), IMPORTE NUMERIC(15,6) )
	
	-- Articulos con precio por talla y color o mismo precio para todas las tallas y colores
	DECLARE @TIPO_PVP_TC INT
	SELECT @TIPO_PVP_TC = ACTUA_COLO FROM '+@GESTION+'.DBO.ARTICULO WHERE CODIGO=@ARTICULO
	
	-- CALCULAR EL PRECIO DE TARIFA
	IF @TIPO_PVP_TC = 2
		SELECT @PREU=PVP FROM '+@GESTION+'.dbo.PVPCOL WHERE ARTICULO=@ARTICULO AND TARIFA=@TARIFA AND TALLA=@TALLA AND COLOR=@COLOR
	ELSE
		SELECT @PREU=PVP FROM '+@GESTION+'.dbo.PVP WHERE ARTICULO=@ARTICULO AND TARIFA=@TARIFA

	SET @PRECIOTARIFA=@PREU

	IF COALESCE(@CLIENTE,'''')=''''
	BEGIN
		INSERT @aPreus SELECT @PREU, @DTO1, @DTO2, 2
		RETURN
	END
	'set @Sentencia = @Sentencia + '	

	/* CALCULO DE PRECIOS Y DESCUENTOS */
		--1.1 buscar precio con dto grales cliente (tabla clientes)
		SELECT @DTO1=DESCU1, @DTO2=DESCU2 FROM '+@GESTION+'.dbo.CLIENTES WHERE CODIGO = @CLIENTE
		
		IF @TIPO_PVP_TC = 2
			SELECT @PREU=PVP FROM '+@GESTION+'.dbo.PVPCOL WHERE ARTICULO=@ARTICULO AND TARIFA=@TARIFA AND TALLA=@TALLA AND COLOR=@COLOR
		ELSE
			SELECT @PREU=PVP FROM '+@GESTION+'.dbo.PVP WHERE ARTICULO=@ARTICULO AND TARIFA=@TARIFA
		
		SET @PRECIOTARIFA=@PREU
		SET @IMPORT=@PREU*(1-(@DTO1/100))*(1-(@DTO2/100))
		IF @DTO1<>0 OR @DTO2<>0 INSERT INTO @TMP_MI_PREUS SELECT ''CLI_GRAL'', @PREU, @DTO1, @DTO2, @IMPORT
		
		--1.2. Buscar descuentos de clientes por articulo
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''CLI_ARTI'' AS TIPO_PVP, (CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2,(CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE 
		FROM vOfertas WHERE TIPO=''CLIENTE'' AND CLIENTE=@CLIENTE AND ARTICULO=@ARTICULO AND TALLA=@TALLA AND COLOR=@COLOR AND ARTICULO!='''' AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) ORDER BY IMPORTE ASC

		--1.3 Buscar descuentos de cliente por familia
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''CLI_FAMI'' AS TIPO_PVP, (CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2,(CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE 
		FROM vOfertas WHERE TIPO=''CLIENTE'' AND CLIENTE=@CLIENTE AND FAMILIA=@FAMILIA AND FAMILIA!='''' AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) ORDER BY IMPORTE ASC

		--1.4 Buscar descuentos de cliente por marca
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''CLI_MARC'' AS TIPO_PVP, (CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2,(CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE 
		FROM vOfertas WHERE TIPO=''CLIENTE'' AND CLIENTE=@CLIENTE AND MARCA=@MARCA AND MARCA!='''' AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) ORDER BY IMPORTE ASC

		--1.5 Buscar descuentos de cliente por subfamilia
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''CLI_SUBF'' AS TIPO_PVP, (CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2,(CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE 
		FROM vOfertas WHERE TIPO=''CLIENTE'' AND CLIENTE=@CLIENTE AND SUBFAMILIA=@SUBFAMILIA AND SUBFAMILIA!='''' AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) ORDER BY IMPORTE ASC
	'set @Sentencia = @Sentencia + '	


		--1.6 Buscar ofertas de articulos
		IF @CliOferta=1
			BEGIN
				IF EXISTS (SELECT * 
				FROM vOfertas WHERE TIPO=''ARTICULO'' AND ARTICULO=@ARTICULO AND TALLA=@TALLA AND COLOR=@COLOR AND TARIFA=@TARIFA AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN))
					BEGIN
						INSERT INTO @TMP_MI_PREUS 
						SELECT TOP 1 ''ART_OFER'' AS TIPO_PVP, (CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2,(CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE 
						FROM vOfertas WHERE TIPO=''ARTICULO'' AND ARTICULO=@ARTICULO AND TALLA=@TALLA AND COLOR=@COLOR AND TARIFA=@TARIFA AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) 
					END
				ELSE
					BEGIN
						INSERT INTO @TMP_MI_PREUS 
						SELECT TOP 1 ''ART_OFER'' AS TIPO_PVP, (CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2,(CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE 
						FROM vOfertas WHERE TIPO=''ARTICULO'' AND ARTICULO=@ARTICULO AND TALLA=@TALLA AND COLOR=@COLOR AND TARIFA='''' AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) 
					END
			END

		--1.7 Buscar descuentos de familia
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''DTO_FAMI'' AS TIPO_PVP, @PRECIOTARIFA AS PVP, [TAN] AS DTO1, 0 AS DTO2, @PRECIOTARIFA*(1-([TAN]/100)) AS IMPORTE 
		FROM '+@COMUN+'.DBO.desc_fam WHERE FAMILIA=@FAMILIA AND (@StrFecha BETWEEN CONVERT(SMALLDATETIME,REPLACE(FECHA_INI,''00'',''01'')+''/''+RIGHT(YEAR(GETDATE()),2),3) AND CONVERT(SMALLDATETIME,FECHA_FIN+''/''+RIGHT(YEAR(GETDATE()),2),3)) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) AND [TAN]!=0.00 ORDER BY IMPORTE ASC

		--1.8 Buscar descuentos de subfamilia
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''DTO_SUBF'' AS TIPO_PVP, @PRECIOTARIFA AS PVP, [TAN] AS DTO1, 0 AS DTO2, @PRECIOTARIFA*(1-([TAN]/100)) AS IMPORTE 
		FROM '+@COMUN+'.DBO.desc_sub WHERE SUBFAM=@SUBFAMILIA AND (@StrFecha BETWEEN CONVERT(SMALLDATETIME,REPLACE(FECHA_INI,''00'',''01'')+''/''+RIGHT(YEAR(GETDATE()),2),3) AND CONVERT(SMALLDATETIME,FECHA_FIN+''/''+RIGHT(YEAR(GETDATE()),2),3)) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) AND [TAN]!=0.00 ORDER BY IMPORTE ASC
		
		--1.9 Buscar descuentos de marca
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''TAR_MARC'' AS TIPO_PVP, @PRECIOTARIFA AS PVP, descuen AS DTO1, 0 AS DTO2, @PRECIOTARIFA*(1-(descuen/100)) AS IMPORTE 
		FROM '+@GESTION+'.dbo.MARCAS WHERE CODIGO=@MARCA AND descuen!=0.00 ORDER BY IMPORTE ASC
	'set @Sentencia = @Sentencia + '	

		--1.10 Buscar descuentos de lineas de descuento por articulo
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''L_D_ARTI'' AS TIPO_PVP, (CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2, (CASE WHEN PVP=0.00 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE  
		FROM vOfertas WHERE TIPO=''LINDESC'' AND ARTICULO=@ARTICULO AND TALLA=@TALLA AND COLOR=@COLOR AND CLIENTE=@CLIENTE AND ARTICULO!='''' AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) ORDER BY IMPORTE ASC 

		--1.11 Buscar descuentos de lineas de descuento por familia
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''L_D_FAMI'' AS TIPO_PVP, (CASE WHEN PVP=0.00 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2, (CASE WHEN PVP=0.00 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE  
		FROM vOfertas WHERE TIPO=''LINDESC'' AND FAMILIA=@FAMILIA AND CLIENTE=@CLIENTE AND FAMILIA!='''' AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) AND DTO1+DTO2!=0.00 ORDER BY IMPORTE ASC 

		--1.12 Buscar descuentos de lineas de descuento por marca
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''L_D_MARC'' AS TIPO_PVP, (CASE WHEN PVP=0.00 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2, (CASE WHEN PVP=0.00 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE  
		FROM vOfertas WHERE TIPO=''LINDESC'' AND MARCA=@MARCA AND CLIENTE=@CLIENTE AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) AND MARCA!='''' AND DTO1+DTO2!=0.00  ORDER BY IMPORTE ASC 


		--1.13 Buscar descuentos de lineas de descuento por subfamilia
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''L_D_SUBF'' AS TIPO_PVP, (CASE WHEN PVP=0.00 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2, (CASE WHEN PVP=0.00 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE  
		FROM vOfertas WHERE TIPO=''LINDESC'' AND SUBFAMILIA=@SUBFAMILIA  AND CLIENTE=@CLIENTE AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) AND SUBFAMILIA!='''' AND DTO1+DTO2!=0.00 ORDER BY IMPORTE ASC 

		-- Añadir el precio de tarifa
		INSERT INTO @TMP_MI_PREUS SELECT ''TARIFA'' AS TIPO_PVP, @PRECIOTARIFA AS PVP, 0 AS DTO1, 0 AS DTO2, @PRECIOTARIFA AS IMPORTE
	
	'set @Sentencia = @Sentencia + '	

	SELECT @ESCALADO=ESTADO FROM '+@COMUN+'.DBO.OPCEMP WHERE EMPRESA=@EMPRESA AND TIPO_OPC=''10027'' 
	BEGIN
		IF @ESCALADO=1 
			BEGIN
				INSERT INTO @aPreus SELECT TOP 1 T.PVP,T.DTO1, T.DTO2, -1 FROM @TMP_MI_PREUS T INNER JOIN (SELECT CODIGO, ORDEN FROM '+@COMUN+'.DBO.ESCALADO
				UNION 
				SELECT ''TARIFA'' AS CODIGO, 999 AS ORDEN) E 
				ON E.CODIGO = T.TIPO_PVP WHERE T.PVP!=0.00 ORDER BY COALESCE(E.ORDEN,999) ASC
			END
		ELSE 
			BEGIN
				INSERT INTO @aPreus SELECT TOP 1 PVP,DTO1,DTO2, -2 FROM @TMP_MI_PREUS WHERE PVP!=0.00 ORDER BY IMPORTE ASC
			END
		RETURN
	END
END
'

	exec (@Sentencia) 




	
	IF EXISTS ( SELECT 1 FROM    Information_schema.Routines
            WHERE   Specific_schema = 'dbo' AND specific_name = 'ftDonamPreuTC' AND Routine_Type = 'FUNCTION'
			) drop function ftDonamPreuTC	
	
	set @Sentencia = '
		CREATE FUNCTION [dbo].[ftDonamPreuTC](@EMPRESA CHAR(2),@CLIENTE CHAR(20),@ARTICULO CHAR(20), @TALLA CHAR(8), @COLOR CHAR(8), @TARIFA CHAR(2),@UNIDADES NUMERIC(18,4) ,@FECHA SMALLDATETIME )
RETURNS @aPreus TABLE 
(
   --Columns returned by the function
   PVP numeric(15,6) NOT NULL DEFAULT(0), 
   DTO1 NUMERIC(15,6) NOT NULL DEFAULT(0), 
   DTO2 NUMERIC(15,6) NOT NULL DEFAULT(0),
   IDERROR INT
)
AS 


BEGIN

    DECLARE @PREU NUMERIC(15,6), @DTO1 NUMERIC(15,6), @DTO2 NUMERIC(15,6), @FAMILIA VARCHAR(20), @MARCA VARCHAR(20), @SUBFAMILIA VARCHAR(20), @TIPO_PVP AS CHAR(8), @IMPORT NUMERIC(15,6), @PRECIOTARIFA NUMERIC(15,6), @ESCALADO BIT, @StrFecha VARCHAR(8), @CliOferta BIT, @TarifaForzada BIT, @TarifaFin char(2)
		
	-- Si el ERP trabaja con talla y color, hay que comantar la declaración de las variables talla y color y añadirlas a los parametros de la funcion
	--DECLARE	@TALLA CHAR(8)='''', @COLOR CHAR(8)=''''
		
	--INICIALIZAR VARIABLES

	SET @PREU = 0.0000
	SET @PRECIOTARIFA=0.0000
	SET @DTO1 = 0.00
	SET @DTO2 = 0.00
	SET @IMPORT=0.00
	SET @FAMILIA=''''
	SET @MARCA=''''
	SET @SUBFAMILIA=''''
	SET @TIPO_PVP=''''



	--Mirar empresa y Articulo
	IF COALESCE(@ARTICULO,'''') = '''' OR COALESCE(@EMPRESA,'''')='''' -- Si no hay articulo devuleve 0s
	BEGIN
		INSERT @aPreus SELECT @PREU, @DTO1, @DTO2, 1
		RETURN 
	END
	-- Mirar Tarifa
	IF COALESCE(@TARIFA,'''') = '''' 
	BEGIN
		SELECT @TARIFA=TARIFA FROM '+@GESTION+'.DBO.CLIENTES WHERE CODIGO=@CLIENTE
		IF @TARIFA = '''' 
			SELECT @TARIFA=TARIFAPRET FROM '+@GESTION+'.dbo.FACTUCNF WHERE EMPRESA=@EMPRESA
	END
	
	-- Mirar si el cliente tiene marcado el check de ofertas
	SET @CliOferta = (SELECT OFERTA FROM '+@GESTION+'.DBO.CLIENTES WHERE CODIGO=@CLIENTE)
	-- Si no passan el cliente el valor de Clioferta será 1
	IF @CliOferta IS NULL
		set @CliOferta=1
	'set @Sentencia = @Sentencia + '	
	
	IF @FECHA IS NULL OR @FECHA=''''
		BEGIN
			SET @StrFecha=CONVERT(VARCHAR(8), GETDATE(), 112)  -- Mirar Fecha

		END
	ELSE
		BEGIN
			SET @StrFecha=CONVERT(VARCHAR(8), @FECHA, 112) 
		END
	IF @UNIDADES IS NULL SET @UNIDADES=0  -- Mirar Unidades
		
	SELECT @FAMILIA=FAMILIA, @MARCA=MARCA, @SUBFAMILIA=SUBFAMILIA FROM '+@GESTION+'.DBO.ARTICULO WHERE CODIGO=@ARTICULO -- Sacar Familia, Marca y Subfamilia del artículo
	
	DECLARE @TMP_MI_PREUS TABLE
		( TIPO_PVP CHAR(8) COLLATE Modern_Spanish_CS_AI, PVP NUMERIC(15,6), DTO1 NUMERIC(15,6), DTO2 NUMERIC(15,6), IMPORTE NUMERIC(15,6) )
	
	-- Articulos con precio por talla y color o mismo precio para todas las tallas y colores
	DECLARE @TIPO_PVP_TC INT
	SELECT @TIPO_PVP_TC = ACTUA_COLO FROM '+@GESTION+'.DBO.ARTICULO WHERE CODIGO=@ARTICULO
	
	-- CALCULAR EL PRECIO DE TARIFA
	IF @TIPO_PVP_TC = 2
		SELECT @PREU=PVP FROM '+@GESTION+'.dbo.PVPCOL WHERE ARTICULO=@ARTICULO AND TARIFA=@TARIFA AND TALLA=@TALLA AND COLOR=@COLOR
	ELSE
		SELECT @PREU=PVP FROM '+@GESTION+'.dbo.PVP WHERE ARTICULO=@ARTICULO AND TARIFA=@TARIFA

	SET @PRECIOTARIFA=@PREU

	IF COALESCE(@CLIENTE,'''')=''''
	BEGIN
		INSERT @aPreus SELECT @PREU, @DTO1, @DTO2, 2
		RETURN
	END
	'set @Sentencia = @Sentencia + '	

	/* CALCULO DE PRECIOS Y DESCUENTOS */
		--1.1 buscar precio con dto grales cliente (tabla clientes)
		SELECT @DTO1=DESCU1, @DTO2=DESCU2 FROM '+@GESTION+'.dbo.CLIENTES WHERE CODIGO = @CLIENTE
		
		IF @TIPO_PVP_TC = 2
			SELECT @PREU=PVP FROM '+@GESTION+'.dbo.PVPCOL WHERE ARTICULO=@ARTICULO AND TARIFA=@TARIFA AND TALLA=@TALLA AND COLOR=@COLOR
		ELSE
			SELECT @PREU=PVP FROM '+@GESTION+'.dbo.PVP WHERE ARTICULO=@ARTICULO AND TARIFA=@TARIFA
		
		SET @PRECIOTARIFA=@PREU
		SET @IMPORT=@PREU*(1-(@DTO1/100))*(1-(@DTO2/100))
		IF @DTO1<>0 OR @DTO2<>0 INSERT INTO @TMP_MI_PREUS SELECT ''CLI_GRAL'', @PREU, @DTO1, @DTO2, @IMPORT
		
		--1.2. Buscar descuentos de clientes por articulo
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''CLI_ARTI'' AS TIPO_PVP, (CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2,(CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE 
		FROM vOfertas WHERE TIPO=''CLIENTE'' AND CLIENTE=@CLIENTE AND ARTICULO=@ARTICULO AND TALLA=@TALLA AND COLOR=@COLOR AND ARTICULO!='''' AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) ORDER BY IMPORTE ASC

		--1.3 Buscar descuentos de cliente por familia
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''CLI_FAMI'' AS TIPO_PVP, (CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2,(CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE 
		FROM vOfertas WHERE TIPO=''CLIENTE'' AND CLIENTE=@CLIENTE AND FAMILIA=@FAMILIA AND FAMILIA!='''' AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) ORDER BY IMPORTE ASC

		--1.4 Buscar descuentos de cliente por marca
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''CLI_MARC'' AS TIPO_PVP, (CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2,(CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE 
		FROM vOfertas WHERE TIPO=''CLIENTE'' AND CLIENTE=@CLIENTE AND MARCA=@MARCA AND MARCA!='''' AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) ORDER BY IMPORTE ASC
	'set @Sentencia = @Sentencia + '	

		--1.5 Buscar descuentos de cliente por subfamilia
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''CLI_SUBF'' AS TIPO_PVP, (CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2,(CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE 
		FROM vOfertas WHERE TIPO=''CLIENTE'' AND CLIENTE=@CLIENTE AND SUBFAMILIA=@SUBFAMILIA AND SUBFAMILIA!='''' AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) ORDER BY IMPORTE ASC


		--1.6 Buscar ofertas de articulos
		IF @CliOferta=1
			BEGIN
				IF EXISTS (SELECT * 
				FROM vOfertas WHERE TIPO=''ARTICULO'' AND ARTICULO=@ARTICULO AND TALLA=@TALLA AND COLOR=@COLOR AND TARIFA=@TARIFA AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN))
					BEGIN
						INSERT INTO @TMP_MI_PREUS 
						SELECT TOP 1 ''ART_OFER'' AS TIPO_PVP, (CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2,(CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE 
						FROM vOfertas WHERE TIPO=''ARTICULO'' AND ARTICULO=@ARTICULO AND TALLA=@TALLA AND COLOR=@COLOR AND TARIFA=@TARIFA AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) 
					END
				ELSE
					BEGIN
						INSERT INTO @TMP_MI_PREUS 
						SELECT TOP 1 ''ART_OFER'' AS TIPO_PVP, (CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2,(CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE 
						FROM vOfertas WHERE TIPO=''ARTICULO'' AND ARTICULO=@ARTICULO AND TALLA=@TALLA AND COLOR=@COLOR AND TARIFA='''' AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) 
					END
			END

		--1.7 Buscar descuentos de familia
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''DTO_FAMI'' AS TIPO_PVP, @PRECIOTARIFA AS PVP, [TAN] AS DTO1, 0 AS DTO2, @PRECIOTARIFA*(1-([TAN]/100)) AS IMPORTE 
		FROM '+@COMUN+'.DBO.desc_fam WHERE FAMILIA=@FAMILIA AND (@StrFecha BETWEEN CONVERT(SMALLDATETIME,REPLACE(FECHA_INI,''00'',''01'')+''/''+RIGHT(YEAR(GETDATE()),2),3) AND CONVERT(SMALLDATETIME,FECHA_FIN+''/''+RIGHT(YEAR(GETDATE()),2),3)) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) AND [TAN]!=0.00 ORDER BY IMPORTE ASC

		--1.8 Buscar descuentos de subfamilia
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''DTO_SUBF'' AS TIPO_PVP, @PRECIOTARIFA AS PVP, [TAN] AS DTO1, 0 AS DTO2, @PRECIOTARIFA*(1-([TAN]/100)) AS IMPORTE 
		FROM '+@COMUN+'.DBO.desc_sub WHERE SUBFAM=@SUBFAMILIA AND (@StrFecha BETWEEN CONVERT(SMALLDATETIME,REPLACE(FECHA_INI,''00'',''01'')+''/''+RIGHT(YEAR(GETDATE()),2),3) AND CONVERT(SMALLDATETIME,FECHA_FIN+''/''+RIGHT(YEAR(GETDATE()),2),3)) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) AND [TAN]!=0.00 ORDER BY IMPORTE ASC
	'set @Sentencia = @Sentencia + '	
		
		--1.9 Buscar descuentos de marca
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''TAR_MARC'' AS TIPO_PVP, @PRECIOTARIFA AS PVP, descuen AS DTO1, 0 AS DTO2, @PRECIOTARIFA*(1-(descuen/100)) AS IMPORTE 
		FROM '+@GESTION+'.dbo.MARCAS WHERE CODIGO=@MARCA AND descuen!=0.00 ORDER BY IMPORTE ASC

		--1.10 Buscar descuentos de lineas de descuento por articulo
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''L_D_ARTI'' AS TIPO_PVP, (CASE WHEN PVP=0 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2, (CASE WHEN PVP=0.00 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE  
		FROM vOfertas WHERE TIPO=''LINDESC'' AND ARTICULO=@ARTICULO AND TALLA=@TALLA AND COLOR=@COLOR AND CLIENTE=@CLIENTE AND ARTICULO!='''' AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) ORDER BY IMPORTE ASC 

		--1.11 Buscar descuentos de lineas de descuento por familia
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''L_D_FAMI'' AS TIPO_PVP, (CASE WHEN PVP=0.00 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2, (CASE WHEN PVP=0.00 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE  
		FROM vOfertas WHERE TIPO=''LINDESC'' AND FAMILIA=@FAMILIA AND CLIENTE=@CLIENTE AND FAMILIA!='''' AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) AND DTO1+DTO2!=0.00 ORDER BY IMPORTE ASC 

		--1.12 Buscar descuentos de lineas de descuento por marca
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''L_D_MARC'' AS TIPO_PVP, (CASE WHEN PVP=0.00 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2, (CASE WHEN PVP=0.00 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE  
		FROM vOfertas WHERE TIPO=''LINDESC'' AND MARCA=@MARCA AND CLIENTE=@CLIENTE AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) AND MARCA!='''' AND DTO1+DTO2!=0.00  ORDER BY IMPORTE ASC 


		--1.13 Buscar descuentos de lineas de descuento por subfamilia
		INSERT INTO @TMP_MI_PREUS 
		SELECT TOP 1 ''L_D_SUBF'' AS TIPO_PVP, (CASE WHEN PVP=0.00 THEN @PRECIOTARIFA ELSE PVP END) AS PVP, DTO1, DTO2, (CASE WHEN PVP=0.00 THEN @PRECIOTARIFA ELSE PVP END)*(1-(DTO1/100))*(1-(DTO2/100)) AS IMPORTE  
		FROM vOfertas WHERE TIPO=''LINDESC'' AND SUBFAMILIA=@SUBFAMILIA  AND CLIENTE=@CLIENTE AND (@StrFecha BETWEEN FECHA_IN AND FECHA_FIN) AND (@UNIDADES BETWEEN UNI_INI AND UNI_FIN) AND SUBFAMILIA!='''' AND DTO1+DTO2!=0.00 ORDER BY IMPORTE ASC 

		-- Añadir el precio de tarifa
		INSERT INTO @TMP_MI_PREUS SELECT ''TARIFA'' AS TIPO_PVP, @PRECIOTARIFA AS PVP, 0 AS DTO1, 0 AS DTO2, @PRECIOTARIFA AS IMPORTE
	
	'set @Sentencia = @Sentencia + '	

	SELECT @ESCALADO=ESTADO FROM '+@COMUN+'.DBO.OPCEMP WHERE EMPRESA=@EMPRESA AND TIPO_OPC=''10027'' 
	BEGIN
		IF @ESCALADO=1 
			BEGIN
				INSERT INTO @aPreus SELECT TOP 1 T.PVP,T.DTO1, T.DTO2, -1 FROM @TMP_MI_PREUS T INNER JOIN (SELECT CODIGO, ORDEN FROM '+@COMUN+'.DBO.ESCALADO
				UNION 
				SELECT ''TARIFA'' AS CODIGO, 999 AS ORDEN) E 
				ON E.CODIGO = T.TIPO_PVP WHERE T.PVP!=0.00 ORDER BY COALESCE(E.ORDEN,999) ASC
			END
		ELSE 
			BEGIN
				INSERT INTO @aPreus SELECT TOP 1 PVP,DTO1,DTO2, -2 FROM @TMP_MI_PREUS WHERE PVP!=0.00 ORDER BY IMPORTE ASC
			END
		RETURN
	END
END
	'
	exec (@Sentencia) 

	RETURN -1
END TRY

BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pAppVistas]...';


GO
CREATE PROCEDURE [dbo].[pAppVistas]
AS
BEGIN TRY	
	SET NOCOUNT ON
	
	
	
	declare @EJERCICIO char(4)	= (select Ejercicio from Configuracion)
	declare @GESTION char(6)	= (select Gestion from Configuracion)
	declare @COMUN char(8)		= (select Comun from Configuracion)
	declare @LOTES char(8)		= (select Lotes from Configuracion)
	declare @alterCreate varchar(max)




IF EXISTS (SELECT * FROM sys.objects where name='vSeries') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vSeries] as
select codigo, nombre from ['+@COMUN+'].dbo.Letras
')
select 'vSeries'




IF EXISTS (SELECT * FROM sys.objects where name='Articulos_Sustitucion') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[Articulos_Sustitucion] AS
select ART1.CODIGO AS ARTICULO, ART1.NOMBRE AS NOMBRE
, case when coalesce(lot.lote,0)=1 then coalesce(stl.stock_uds_lot,0.00) else coalesce(stc.stock_uds,0.00) end  as stock
, art2.codigo as art_orig
from ['+@GESTION+'].dbo.articulo art1 
INNER JOIN ['+@GESTION+'].DBO.ARTICULO ART2 ON ART2.SUBFAMILIA=ART1.SUBFAMILIA
LEFT JOIN ['+@LOTES+'] .DBO.artlot LOT ON LOT.ARTICULO=ART1.CODIGO
left join (select articulo, sum(unidades) as stock_uds_lot from ['+@LOTES+'].[dbo].stocklotes 
group by articulo) stl on stl.articulo=ART1.CODIGO
left join (select articulo, sum(final) as stock_uds from ['+@GESTION+'].dbo.stocks2 group by articulo) stc on stc.articulo=ART1.CODIGO
WHERE ART1.BAJA=0 AND ART1.SUBFAMILIA!='''' 
AND ( case when coalesce(lot.lote,0)=1 then coalesce(stl.stock_uds_lot,0.00) else coalesce(stc.stock_uds,0.00) end)!=0.00 
and art1.codigo!=art2.codigo	
')
select 'Articulos_Sustitucion'




IF EXISTS (SELECT * FROM sys.objects where name='vClientes') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vClientes] AS
SELECT 1 as Nodo,C.CODIGO as codigo, RTRIM(C.NOMBRE) collate database_default AS NOMBRE , C.NOMBRE2 AS RCOMERCIAL,
C.CIF, cast(RTRIM(C.DIRECCION) as varchar(40)) AS DIRECCION, C.CODPOST AS CP, C.POBLACION AS POBLACION,
C.PROVINCIA as provincia, ISNULL(PAIS.NOMBRE,'''') AS PAIS, C.EMAIL as EMAIL, C.EMAIL_F, COALESCE(T.TELEFONO, SPACE(15))
AS TELEFONO, COALESCE(P.PERSONA, SPACE(30)) AS CONTACTO, C.HTTP AS WEB, FP.NOMBRE AS N_PAG,
C.DIAPAG AS DIA_PAG, CAST(ROUND(C.CREDITO,2) AS numeric(10,2)) AS CREDITO, COALESCE(B.BANCO,
SPACE(30)) AS BANCO, COALESCE(B.IBAN, SPACE(4))+'' ''+COALESCE(B.CUENTAIBAN, SPACE(20)) AS IBAN,
COALESCE(B.SWIFT, SPACE(10)) AS SWIFT, C.VENDEDOR AS VENDEDOR, v.NOMBRE as nVendedor,
REPLACE(CONVERT(VARCHAR(MAX), 
C.OBSERVACIO),CHAR(13),''<br/>'') AS OBSERVACIO, C.TARIFA AS TARIFA, ISNULL(FP.DIAS_RIESGO,0)AS DIAS_RIESGO, 
C.DESCU1 AS DESCU1, C.DESCU2 AS DESCU2, C.OFERTA, C.BLOQ_CLI AS BLOQ_CLI, C.ENV_CLI, C.FECHA_BAJ,
case when C.FECHA_BAJ IS NULL or C.FECHA_BAJ=''1900-01-01 00:00:00'' THEN 0 ELSE 1 END AS BAJA, 
'''' as Logo, 
tiva.CODIGO as tipoIVA, isnull(tiva.IVA,0) as IVA, c.AGENCIA,
isnull(tiva.RECARG,0) as recargoIVA,
COALESCE(MCA1.VALOR,'''') collate database_default AS CENTRO,
COALESCE(MCA2.VALOR,'''') collate database_default AS MEDICO,
COALESCE(MCA3.VALOR,'''') collate database_default AS RAZON_SOCIAL_FACT,
C.RUTA, r.NOMBRE as nRuta,
mLat.VALOR as LATITUD, mLon.VALOR as LONGITUD
FROM ['+@GESTION+'].DBO.CLIENTES C 
LEFT JOIN (SELECT CLIENTE, MAX(TELEFONO) AS TELEFONO FROM ['+@GESTION+'].dbo.TELF_CLI WHERE ORDEN=1 GROUP BY CLIENTE) T ON T.CLIENTE = C.CODIGO 
LEFT JOIN ['+@GESTION+'].dbo.CONT_CLI P ON P.CLIENTE = C.CODIGO AND P.ORDEN = 1 
LEFT JOIN ['+@GESTION+'].dbo.BANC_CLI B ON B.CLIENTE = C.CODIGO AND B.ORDEN = 1 
LEFT JOIN ['+@GESTION+'].dbo.FPAG FP    ON FP.CODIGO = C.FPAG 
LEFT JOIN ['+@GESTION+'].dbo.tipo_iva tiva ON tiva.CODIGO = C.TIPO_IVA 
LEFT JOIN ['+@COMUN+'].dbo.PAISES PAIS ON PAIS.CODIGO  = C.PAIS 
LEFT JOIN ['+@GESTION+'].dbo.rutas r on r.CODIGO=C.RUTA
LEFT JOIN ['+@GESTION+'].DBO.vendedor v on v.CODIGO=C.VENDEDOR
LEFT JOIN ['+@GESTION+'].DBO.multicam MCA1 ON MCA1.CODIGO=C.CODIGO AND MCA1.FICHERO=''CLIENTES'' AND MCA1.CAMPO=''CEN''
LEFT JOIN ['+@GESTION+'].DBO.multicam MCA2 ON MCA2.CODIGO=C.CODIGO AND MCA2.FICHERO=''CLIENTES'' AND MCA2.CAMPO=''MED''
LEFT JOIN ['+@GESTION+'].DBO.multicam MCA3 ON MCA3.CODIGO=C.CODIGO AND MCA3.FICHERO=''CLIENTES'' AND MCA3.CAMPO=''RSO''
LEFT JOIN ['+@GESTION+'].DBO.multicam mLat ON mLat.CODIGO=C.CODIGO and mLat.FICHERO=''CLIENTES'' and mLat.CAMPO=''LAT''
LEFT JOIN ['+@GESTION+'].DBO.multicam mLon ON mLon.CODIGO=C.CODIGO and mLon.FICHERO=''CLIENTES'' and mLon.CAMPO=''LGT''
WHERE LEFT(C.CODIGO,3)=''430''
')
select 'vClientes'




IF EXISTS (SELECT * FROM sys.objects where name='d_Busca_PV') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +' view [dbo].[d_Busca_PV] as
SELECT distinct a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre as n_ruta, a.empresa,
    CASE 
    WHEN B.CONTADO = 0 THEN replace(B.nombre,''"'',''-'')
    WHEN B.CONTADO = 1 AND COALESCE(MAX(CON.NOMBRE),'''')!='''' THEN MAX(replace(CON.NOMBRE,''"'',''-''))
    ELSE B.nombre              
    END AS nombre, 
    replace(b.nombre2,''"'',''-'') as rComercial,
    cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra as EN_USO,
    (select max(servidas) from ['+@GESTION+'].[dbo].[d_pedive] where NUMERO=a.NUMERO)+SUM(COALESCE(TP.UNIDADES,0.00)) as servidas, 
	isnull(TP.numero,0) as bNumero,
    e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, a.almacen, MAX(B.IDIOMA_IMP) AS IDIOMA_IMP
FROM ['+@GESTION+'].[dbo].[c_pedive] a
inner join ['+@GESTION+'].[dbo].[clientes] b on a.cliente=b.codigo
left  join ['+@GESTION+'].[dbo].rutas d on d.codigo=a.ruta
left  join Traspaso_Temporal TP on  TP.numero COLLATE Modern_Spanish_CI_AI =a.numero COLLATE Modern_Spanish_CI_AI
left  join ['+@COMUN+'].[DBO].[en_uso] e on RTRIM(LTRIM(e.TIPO))=''PEDIVEN'' and e.CLAVE=cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra
LEFT JOIN ['+@GESTION+'].DBO.CONTADO CON ON CON.EMPRESA=A.EMPRESA AND CON.NUMERO=A.NUMERO AND CON.LETRA=A.LETRA AND CON.TIPO=2
where a.traspasado=0 and a.cancelado=0 and a.TOTALPED>=0.00
GROUP BY a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre, a.empresa, b.nombre,  b.nombre2,
cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra, isnull(TP.numero,0), e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, a.almacen, B.CONTADO
')
select 'd_Busca_PV'




IF EXISTS (SELECT * FROM sys.objects where name='d_Busca_PV_DI') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[d_Busca_PV_DI] as
SELECT distinct a.numero, a.fecha, a.entrega, a.letra, a.ruta
, replace(d.nombre,''"'',''-'') as n_ruta
, a.empresa
, replace(b.nombre,''"'',''-'') as nombre
, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra as EN_USO
, (select max(servidas) from ['+@GESTION+'].[dbo].[d_pedive] where NUMERO=a.NUMERO) as servidas, isnull(TP.numero,0) as bNumero
, e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL
FROM ['+@GESTION+'].[dbo].[c_pedive] a
inner join ['+@GESTION+'].[dbo].[clientes] b on a.cliente=b.codigo
left  join ['+@GESTION+'].[dbo].rutas d on d.codigo=a.ruta
left  join Traspaso_Temporal TP on  TP.numero COLLATE Modern_Spanish_CI_AI =a.numero COLLATE Modern_Spanish_CI_AI
left  join ['+@COMUN+'].[DBO].[en_uso] e on RTRIM(LTRIM(e.TIPO))=''PEDIVEN'' and e.CLAVE=cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra
where a.traspasado=0 and a.cancelado=0 and a.entrega<=getdate()+4
')
select 'd_Busca_PV_DI'




IF EXISTS (SELECT * FROM sys.objects where name='d_Busca_PV_Zona') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[d_Busca_PV_Zona] as
SELECT distinct a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre as n_ruta, a.empresa,
replace(replace(b.nombre,''"'',''-''),'''''''',''-'') as nombre, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra as EN_USO,
(select max(servidas) from ['+@GESTION+'].[dbo].[d_pedive] where NUMERO=a.NUMERO) as servidas, isnull(TP.numero,0) as bNumero,
e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, COALESCE(UPPER(M.VALOR),'''') AS ZONA
FROM ['+@GESTION+'].[dbo].[c_pedive] a
inner join ['+@GESTION+'].[dbo].[clientes] b on a.cliente=b.codigo
left join ['+@GESTION+'].[dbo].rutas d on d.codigo=a.ruta
left join Traspaso_Temporal TP on  TP.numero COLLATE Modern_Spanish_CI_AI =a.numero COLLATE Modern_Spanish_CI_AI
left join ['+@COMUN+'].[DBO].[en_uso] e on RTRIM(LTRIM(e.TIPO))=''PEDIVEN'' and e.CLAVE=cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra
left join ['+@GESTION+'].dbo.d_pedive dpv on dpv.empresa=a.empresa and dpv.numero=a.numero and dpv.letra=a.letra and dpv.articulo!=''''
left join ['+@GESTION+'].dbo.multicam m on m.codigo=dpv.articulo and m.campo=''ZNA''
where a.traspasado=0 and a.cancelado=0 and a.entrega<=getdate()+1
GROUP BY a.numero, a.fecha, a.entrega, a.letra, a.ruta, d.nombre, a.empresa, b.nombre, cast(year(getdate()) as char(4))+a.empresa+a.NUMERO+a.letra
, isnull(TP.numero,0),e.TIPO, e.CLAVE, e.USUARIO, e.TERMINAL, UPPER(M.VALOR)
')
select 'd_Busca_PV_Zona'




IF EXISTS (SELECT * FROM sys.objects where name='d_PV')set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[d_PV] as
SELECT a.numero, a.articulo, a.definicion, a.cajas, a.unidades, a.peso, a.servidas,
b.fecha, b.letra, b.ruta,
c.nombre
FROM ['+@GESTION+'].[dbo].[d_pedive] a
inner join ['+@GESTION+'].[dbo].[c_pedive] b on a.numero=b.numero
inner join ['+@GESTION+'].[dbo].[clientes] c on b.cliente=c.codigo
WHERE a.EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
')
select 'd_PV'




IF EXISTS (SELECT * FROM sys.objects where name='Detalle_Pedidos') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[Detalle_Pedidos] AS
select cpv.cliente,        
    CASE 
    WHEN CLI.CONTADO = 0 THEN cli.nombre
    WHEN CLI.CONTADO = 1 AND COALESCE(CON.NOMBRE,'''')!='''' THEN CON.NOMBRE
    ELSE cli.nombre                   
    END as n_cliente, 
    dpv.linia, dpv.servidas,
    ISNULL(art.stock,1) as cntrlSotck,
    CASE WHEN CLI.CONTADO = 0 
    THEN
        case when env.direccion='''' then cli.direccion else env.direccion end 
    ELSE
        CON.DIRECCION
    END    as direccion, 
    CASE WHEN CLI.CONTADO = 0 
    THEN
    case when env.codpos='''' then cli.codpost else env.codpos end
    ELSE
        CON.CODPOST 
    END as copdpost, 
    CASE WHEN CLI.CONTADO = 0 
    THEN
        case when env.poblacion='''' then cli.poblacion else env.poblacion end
    ELSE
        CON.POBLACION 
    END as poblacion, 
    CASE WHEN CLI.CONTADO = 0 
    THEN
        case when env.provincia='''' then cli.provincia else env.provincia end
    ELSE
        CON.PROVINCIA 
    END as provincia, 

    cli.NOMBRE2 as rComercial,
             
    cpv.empresa, cpv.numero, cpv.letra, cpv.vendedor, ven.nombre as n_vendedor, ven.mobil as Movil_vendedor, 
    cpv.ruta, rut.nombre as n_ruta, dpv.articulo, replace(dpv.definicion,''"'','''') as definicion, coalesce(lot.lote,0) as articulo_lote, 
    dpv.cajas as cajas_pv, dpv.unidades as uds_pv, 
    case when cast(case when art.peso='''' then ''0.0000'' else art.peso end as decimal(16,4))!=0.00 and dpv.peso=0.00 then cast(case when art.peso='''' then ''0.0000'' else art.peso end as decimal(16,4))*dpv.unidades else dpv.peso end as peso_pv, 
    dpv.cajas-dpv.CAJASERV-coalesce(trt.cajas,0.00) as cajas_pendientes, 
    dpv.unidades-dpv.servidas-coalesce(trt.unidades,0.000) as uds_pendiente, 
    (case when cast(case when art.peso='''' then ''0.0000'' else art.peso end as decimal(16,4))!=0.00 and dpv.peso=0.00 then cast(case when art.peso='''' then ''0.0000'' else art.peso end as decimal(16,4))*dpv.unidades else dpv.peso end)-(dpv.servidas*cast(case when art.peso='''' then ''0.0000'' else art.peso end as decimal(16,4)))-cast(coalesce(trt.peso,''000000000.0000'') as decimal(16,4)) as peso_pendiente, 
    coalesce(trt.cajas,0.00) as cajas_traspasadas, 
    coalesce(trt.unidades,0.000) as uds_traspasadas, 
    cast(coalesce(trt.peso,''0.00'') as decimal(16,4)) as peso_traspasado,
    case when coalesce(lot.lote,0)=1 then coalesce(stl.stock_uds_lot,0.00) else coalesce(stc.stock_uds,0.00) end  as stock,
    '''' as ColID
    , coalesce(DPV.LIBRE_4,'''') as notas_traspasadas
    , dpv.dto1+dpv.dto2 as dto
    , coalesce(m.valor,'''') as zona
    , art.ubicacion
    , cli.OBSERVACIO as ClienteObs
    , cpv.OBSERVACIO as PedidoObs
    , art.UNICAJA 
    , CASE WHEN coalesce(m2.valor,'''')='''' OR  coalesce(m2.valor,'''')=''.F.'' THEN 0 ELSE 1 END as VENTA_CAJA
    , case when trt.articulo is null then 0 else 1 end preparado -- Para saber si las notas estan preparadas
	, dpv.TIPO_IVA as TipoIVA
from ['+@GESTION+'].dbo.c_pedive cpv 
inner join ['+@GESTION+'].dbo.d_pedive dpv on cpv.empresa=DPV.EMPRESA AND cpv.numero=DPV.NUMERO AND cpv.letra=dpv.letra
left  join  (select empresa, numero, letra, linea, articulo, sum(coalesce(cajas,0.00)) as cajas, sum(unidades) as unidades, sum(cast(case when peso='''' then ''0.00'' else coalesce(peso,''0.00'') end as decimal(16,4))) as peso from Traspaso_Temporal group by empresa, numero, letra, linea, articulo) trt on trt.empresa collate database_default=DPV.EMPRESA AND trt.numero collate database_default=DPV.NUMERO AND trt.letra collate database_default=DPV.LETRA AND trt.linea=DPV.LINIA 
left join ['+@GESTION+'].dbo.articulo art on art.codigo=dpv.articulo 
left join  ['+@GESTION+'].dbo.rutas rut on rut.codigo=cpv.ruta 
left join  ['+@GESTION+'].dbo.vendedor ven on ven.codigo=cpv.vendedor 
inner join ['+@GESTION+'].dbo.clientes cli on cli.codigo=cpv.cliente 
left join  ['+@GESTION+'].dbo.env_cli env on env.linea=cpv.env_cli and env.cliente=cpv.cliente 
left join  ['+@LOTES+'].dbo.artlot lot on lot.articulo=dpv.articulo
left join (select empresa, almacen, articulo, sum(peso) as stock_peso_lot, sum(unidades) as stock_uds_lot from ['+@LOTES+'].[dbo].stocklotes 
group by empresa, almacen, articulo) stl on stl.empresa=cpv.empresa and stl.articulo=dpv.articulo and stl.almacen=cpv.almacen
left join (select empresa, almacen, articulo, sum(final) as stock_uds from ['+@GESTION+'].dbo.stocks2 group by empresa, almacen, articulo) stc on stc.empresa=cpv.empresa and stc.almacen=cpv.almacen and stc.articulo=dpv.articulo
left join ['+@GESTION+'].dbo.multicam m on m.codigo=dpv.articulo and m.campo=''ZNA''
left join ['+@GESTION+'].dbo.multicam m2 on m2.codigo=dpv.articulo and m2.campo=''SCA''
LEFT JOIN ['+@GESTION+'].DBO.CONTADO CON ON CON.EMPRESA=CPV.EMPRESA AND CON.NUMERO=CPV.NUMERO AND CON.LETRA=CPV.LETRA AND CON.TIPO=2
where cpv.traspasado=0 and cpv.cancelado=0 and (dpv.servidas<dpv.unidades or dpv.articulo='''') and coalesce(DPV.LIBRE_4,'''')=''''
')
select 'Detalle_Pedidos'




IF EXISTS (SELECT * FROM sys.objects where name='Pendiente_Pedidos') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[Pendiente_Pedidos] AS
SELECT DPV.EMPRESA, DPV.NUMERO, DPV.LETRA, DPV.LINIA, DPV.ARTICULO, DPV.UNIDADES 
FROM ['+@GESTION+'].DBO.D_PEDIVE DPV 
INNER JOIN ['+@GESTION+'].DBO.C_PEDIVE CPV ON CPV.EMPRESA=DPV.EMPRESA AND CPV.NUMERO=DPV.NUMERO AND CPV.LETRA=DPV.LETRA 
LEFT JOIN (SELECT EMPRESA, NUMERO, LETRA, LINEA, SUM(COALESCE(UNIDADES,0.00)) AS UNIDADES 
FROM Traspaso_Temporal 
GROUP BY  EMPRESA, NUMERO, LETRA, LINEA
) TEM ON TEM.EMPRESA collate Modern_Spanish_CI_AI=DPV.EMPRESA 
AND TEM.NUMERO collate Modern_Spanish_CI_AI=DPV.NUMERO 
AND TEM.LETRA collate Modern_Spanish_CI_AI=DPV.LETRA AND TEM.LINEA=DPV.LINIA 
WHERE CPV.TRASPASADO=0 AND CPV.CANCELADO=0 AND DPV.ARTICULO!='''' AND DPV.UNIDADES>DPV.SERVIDAS AND DPV.UNIDADES>COALESCE(TEM.UNIDADES,0.00)
')
select 'Pendiente_Pedidos'




IF EXISTS (SELECT * FROM sys.objects where name='v_d_pedive') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[v_d_pedive] as
SELECT	a.empresa, a.numero, a.linia, a.letra,
a.articulo,a.definicion,a.cajas,a.unidades, a.peso, a.servidas, a.traspaso,
c.CODIGO, c.NOMBRE, c.DIRECCION, c.CODPOST, c.POBLACION, c.PROVINCIA,
d.CODIGO as rutaCodigo, d.NOMBRE as rutaNombre,
e.CODIGO as vendedorCodigo, e.NOMBRE as vendedorNombre, e.MOBIL as vendedorMovil,
cast(f.LOTE as int) as usaLote
from ['+@GESTION+'].[dbo].d_pedive a
inner join ['+@GESTION+'].[dbo].c_pedive	b on a.NUMERO=b.NUMERO
inner join ['+@GESTION+'].[dbo].clientes	c on c.CODIGO=b.CLIENTE
inner join ['+@GESTION+'].[dbo].rutas		d on d.CODIGO=c.RUTA
inner join ['+@GESTION+'].[dbo].vendedor	e on e.CODIGO=c.VENDEDOR
left  join ['+@LOTES+'].[dbo].artlot	f on f.ARTICULO=a.ARTICULO  
where a.EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
')
select 'v_d_pedive'




IF EXISTS (SELECT * FROM sys.objects where name='v_d_traspaso') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[v_d_traspaso] as
SELECT empresa, numero, linia, letra, articulo, definicion, cajas, unidades, peso, servidas, traspaso
FROM ['+@GESTION+'].[dbo].[d_pedive]
')
select 'v_d_traspaso'




IF EXISTS (SELECT * FROM sys.objects where name='vAlbaranes') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vAlbaranes] AS
SELECT 
'''+@EJERCICIO+'''+CA.EMPRESA+REPLACE(CA.letra,SPACE(1),''0'')+REPLACE(CA.NUMERO, SPACE(1), ''0'') COLLATE Modern_Spanish_CI_AI AS IDALBARAN
,'''+@EJERCICIO+''' AS EJER, CA.EMPRESA, CA.LETRA as letra, ltrim(rtrim(CA.NUMERO)) as NUMERO, CA.LETRA+CA.NUMERO AS ALBARAN
, CA.CLIENTE
		
, CASE WHEN vcli.CONTADO = 0 
THEN
case when coalesce(vCON.PERSONA,'''')='''' then vcli.nombre else vCON.PERSONA end 
ELSE
CON.nombre
END	as nCliente	
, vcli.nombre as nFiscal
, convert(varchar(10),CA.FECHA,103) as FECHA, CAST(ROUND(CA.IMPORTE,2) AS NUMERIC(18,2)) AS IMPORTE, CA.IMPORTE as IMPORTEIVA, CA.TOTALDOC as TOTAL
, CAST(COALESCE(CASE WHEN RIGHT(CA.PEDIDO,2) = '''' THEN LTRIM(RTRIM(CA.PEDIDO)) ELSE LTRIM(RTRIM(RIGHT(CA.PEDIDO,2)))+''-''+LTRIM(RTRIM(LEFT(CA.PEDIDO,10))) END, SPACE(0)) AS VARCHAR(13)) AS PEDIDO
, CA.VENDEDOR, vven.NOMBRE COLLATE Modern_Spanish_CI_AI as nVendedor, CA.ALMACEN, CA.FINAN, CA.PRONTO 
	
, CASE WHEN vcli.CONTADO = 0 
THEN
case when coalesce(EC.direccion,'''')='''' then vcli.direccion else EC.direccion end 
ELSE
CON.DIRECCION
END	as direccion
, CASE WHEN vcli.CONTADO = 0 
THEN
case when coalesce(EC.POBLACION,'''')='''' then vcli.POBLACION else EC.POBLACION end 
ELSE
CON.POBLACION
END	as POBLACION
, CASE WHEN vcli.CONTADO = 0 
THEN
case when coalesce(EC.CODPOS,'''')='''' then vcli.CODPOST else EC.CODPOS end 
ELSE
CON.CODPOST
END	as CODPOS
, CASE WHEN vcli.CONTADO = 0 
THEN
case when coalesce(EC.PROVINCIA,'''')='''' then vcli.PROVINCIA else EC.PROVINCIA end 
ELSE
CON.PROVINCIA
END	as PROVINCIA
	
, CA.FPAG, COALESCE(B.BANCO,SPACE(0)) AS BANCO, COALESCE((B.IBAN+B.CUENTAIBAN),SPACE(0)) AS IBAN
, CA.FACTURA
, vcli.RUTA
,vcli.NOMBRE2
, concat(ltrim(rtrim(vcli.RUTA)),''-'',datepart(DW,CA.FECHA),''-'',LEFT(RIGHT(ltrim(rtrim((CA.PEDIDO))),7),5)) as rutaComp
, CAST(vcon.CLIENTE AS VARCHAR)+Replace(str(CAST(vcon.LINEA AS VARCHAR),4),space(1),''0'') COLLATE Modern_Spanish_CI_AI as contacto, vcon.PERSONA COLLATE Modern_Spanish_CI_AI as nContacto
, POR.AGENCIA COLLATE Modern_Spanish_CI_AI as agencia, ltrim(rtrim(ag.NOMBRE)) COLLATE Modern_Spanish_CI_AI as nAgencia
, VVEN.MOBIL, M.VALOR AS HORARIO, VCLI.VALOR_ALB, COALESCE(EE.BULTOS,00000) AS BULTOS, (CA.IMPORTE * CA.PRONTO)/100 as importePP, por.IMPORTE as portesIMP
, DA.SUM_CAJAS, DA.SUM_PESO, DA.SUM_DTO1, DA.SUM_DTO2, VCLI.CIF, TELF.TELEFONO, convert(nvarchar(250),ca.observacio) as observaciones
FROM ['+@GESTION+'].DBO.C_ALBVEN CA
LEFT JOIN ['+@GESTION+'].dbo.ENV_CLI EC ON CA.CLIENTE = EC.CLIENTE AND CA.ENV_CLI = EC.LINEA 
LEFT JOIN ['+@GESTION+'].DBO.BANC_CLI B ON B.CLIENTE=CA.CLIENTE AND B.CODIGO=CA.BANC_CLI
LEFT JOIN ['+@GESTION+'].dbo.clientes vcli on vcli.CODIGO =CA.CLIENTE
LEFT JOIN ['+@GESTION+'].dbo.vendedor vven on vven.CODIGO =CA.VENDEDOR
LEFT JOIN ['+@GESTION+'].DBO.CONT_CLI  vcon on vcon.CLIENTE=CA.CLIENTE and vcon.LINEA=1
LEFT JOIN ['+@GESTION+'].DBO.PORTES POR ON POR.EMPRESA=CA.EMPRESA AND POR.LETRA=CA.LETRA AND POR.ALBARAN=CA.NUMERO
LEFT JOIN ['+@GESTION+'].DBO.agencia ag on ag.CODIGO=POR.AGENCIA
LEFT JOIN ['+@GESTION+'].[dbo].MULTICAM M on M.FICHERO=''CLIENTES'' and M.CAMPO=''HOR'' AND M.CODIGO=CA.CLIENTE
left join ['+@GESTION+'].DBO.ENVIOETI EE ON EE.EMPRESA=CA.EMPRESA AND EE.NUMERO=CA.NUMERO AND EE.LETRA=CA.LETRA
INNER JOIN (SELECT D.EMPRESA, D.NUMERO, D.LETRA, SUM(D.CAJAS) AS SUM_CAJAS, SUM(D.PESO+CONVERT(DECIMAL(16,3),CASE WHEN A.LITROS='''' THEN ''0.00'' ELSE A.LITROS END)) AS SUM_PESO, SUM(D.DTO1) AS SUM_DTO1, SUM(D.DTO2) AS SUM_DTO2 FROM ['+@GESTION+'].DBO.D_ALBVEN D INNER JOIN ['+@GESTION+'].DBO.ARTICULO A ON A.CODIGO=D.ARTICULO GROUP BY D.EMPRESA, D.NUMERO, D.LETRA) DA ON DA.EMPRESA=CA.EMPRESA AND DA.NUMERO=CA.NUMERO AND DA.LETRA=CA.LETRA
LEFT JOIN (SELECT CLIENTE, MAX(TELEFONO) AS TELEFONO FROM ['+@GESTION+'].DBO.TELF_CLI WHERE ORDEN=1 GROUP BY CLIENTE) TELF ON TELF.CLIENTE=CA.CLIENTE
LEFT JOIN ['+@GESTION+'].DBO.CONTADO CON ON CON.EMPRESA=CA.EMPRESA AND CON.NUMERO=CA.NUMERO AND CON.LETRA=CA.LETRA AND CON.TIPO=1
WHERE CA.TRASPASADO = 0 AND LEFT(CA.CLIENTE,3)=''430''
')
select 'vAlbaranes'




IF EXISTS (SELECT * FROM sys.objects where name='vAlbaranes_Cabecera') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vAlbaranes_Cabecera] as
select	
'''+@EJERCICIO+'''+CA.EMPRESA  collate Modern_Spanish_CI_AI +REPLACE(CA.LETRA  collate Modern_Spanish_CI_AI,SPACE(1),''0'')
+REPLACE(CA.NUMERO  collate Modern_Spanish_CI_AI, SPACE(1),''0'') as IDALBARAN
, CA.LETRA + CA.NUMERO AS ALBARAN
, CA.PRONTO
, CA.IMPORTE as BaseImponible
, p.IMPORTE as portesIMP
, (CA.IMPORTE * CA.PRONTO)/100 as importePP
, ca.totaldoc as TOTALDOC
, e.IMPORTE as entregaIMP 
, CAST(CASE WHEN CA.FACTURA = '''' THEN (CASE WHEN CA.FACTURABLE=1 THEN ''PENDIENTE'' ELSE ''NO FACTURABLE'' END) ELSE ''FACTURADO'' END AS varchar(50)) 
    COLLATE Modern_Spanish_CI_AI AS ESTADO 
FROM ['+@GESTION+'].DBO.C_ALBVEN CA 
LEFT JOIN ['+@GESTION+'].dbo.ENV_CLI EC ON CA.CLIENTE = EC.CLIENTE AND CA.ENV_CLI = EC.LINEA 
left join ['+@GESTION+'].dbo.portes p on p.ALBARAN=CA.NUMERO and CA.EMPRESA=p.EMPRESA and CA.LETRA=p.LETRA
left join ['+@GESTION+'].dbo.entregas e on e.ALBARAN=CA.NUMERO and CA.EMPRESA=e.EMPRESA and CA.LETRA=e.LETRA
')
select 'vAlbaranes_Cabecera'




IF EXISTS (SELECT * FROM sys.objects where name='vAlbaranes_Detalle') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vAlbaranes_Detalle] AS
SELECT 
'''+@EJERCICIO+'''+C.EMPRESA+REPLACE(C.letra,SPACE(1),''0'')+REPLACE(C.NUMERO, SPACE(1), ''0'') COLLATE Modern_Spanish_CI_AI AS IDALBARAN
,'''+@EJERCICIO+''' AS EJER, d.empresa, d.letra, d.numero, C.LETRA+D.NUMERO AS ALBARAN, D.CLIENTE
, D.ARTICULO, D.DEFINICION, D.UNIDADES, D.CAJAS, D.PESO, D.SERIE, D.PRECIO, D.LINIA, D.DTO1, D.DTO2, D.IMPORTE, D.Tipo_IVA as Tipo_IVA, d.pverde, d.PRECIOIVA , d.IMPDIVIVA, coalesce(iva.iva,0.00) as porcen_iva
,case when d.doc=1 then d.DOC_NUM else '''' end as PEDIDO, C.VENDEDOR
FROM ['+@GESTION+'].DBO.D_ALBVEN D
INNER JOIN ['+@GESTION+'].dbo.C_ALBVEN C ON D.EMPRESA = C.EMPRESA AND D.LETRA = C.LETRA AND D.NUMERO = C.NUMERO
left join ['+@GESTION+'].dbo.tipo_iva iva on iva.codigo=d.TIPO_IVA 
WHERE C.TRASPASADO = 0 AND LEFT(D.CLIENTE,3)=''430''
')
select 'vAlbaranes_Detalle'




IF EXISTS (SELECT * FROM sys.objects where name='vAlbaranes_Lote') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vAlbaranes_Lote] AS
SELECT
'''+@EJERCICIO+'''+d.EMPRESA+REPLACE(d.letra,SPACE(1),''0'')+REPLACE(d.NUMERO, SPACE(1), ''0'') COLLATE Modern_Spanish_CI_AI AS IDALBARAN
,'''+@EJERCICIO+''' AS PERIODO, D.EMPRESA, d.numero, d.letra, d.letra+D.NUMERO AS ALBARAN, d.LINIA, d.LOTE, d.UNIDADES
from ['+@LOTES+'].dbo.LTALBVE d where d.periodo='''+@EJERCICIO+'''
')
select 'vAlbaranes_Lote'




IF EXISTS (SELECT * FROM sys.objects where name='vAlbaranes_Pie') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vAlbaranes_Pie] AS 	
SELECT 
'''+@EJERCICIO+'''+CAv.EMPRESA+REPLACE(CAv.letra,SPACE(1),''0'')+REPLACE(CAv.NUMERO, SPACE(1), ''0'') COLLATE Modern_Spanish_CI_AI AS IDALBARAN
,'''+@EJERCICIO+''' as EJER,	CAV.EMPRESA, CAV.LETRA, ltrim(rtrim(CAv.NUMERO)) as NUMERO, CAv.LETRA+CAv.NUMERO AS ALBARAN
, CAV.CLIENTE, CLI.RECARGO as CliRecargo, dav.tipo_iva, iva.IVA as porcen_iva, CASE WHEN CLI.RECARGO=1 THEN IVA.RECARG ELSE 0.00 END AS TIPO_RE,
CAV.PRONTO as pPago,

SUM(DAV.PVERDE) AS PVERDE, 

SUM(CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END  + DAV.PVERDE)
AS IMPORTE,

ROUND(SUM((
CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END + DAV.PVERDE)*(IVA.IVA/100)),2) 
AS IVA,

CASE WHEN CLI.RECARGO=1 
THEN round(SUM((
CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.RECARG/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END + DAV.PVERDE)*(IVA.RECARG/100)),2) 
ELSE 0.00 
END  
AS RECARGO,

SUM(CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END + DAV.PVERDE)  
+
ROUND(SUM((
CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END + DAV.PVERDE)*(IVA.IVA/100)),2) 
+
CASE WHEN CLI.RECARGO=1 
THEN round(SUM((
CASE WHEN DAV.IVA_INC=1 
THEN DAV.IMPORTE*(1-(IVA.RECARG/100)) 
ELSE 
CASE WHEN DAV.INC_PP=0 
THEN DAV.IMPORTE 
ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) 
END 
END + DAV.PVERDE)*(IVA.RECARG/100)),2) 
ELSE 0.00 
END  
AS TOTALDOC

FROM ['+@GESTION+'].DBO.C_ALBVEN CAV 
INNER JOIN 
(select EMPRESA, NUMERO, LETRA, ARTICULO, IMPORTE, TIPO_iva, 1 AS INC_PP, 0 AS IVA_INC, PVERDE
from ['+@GESTION+'].dbo.d_albven where tipo_iva!='''' and importe!=0.00

union all

select  EMPRESA, albaran, LETRA, ''PORTES'' as ARTICULO, IMPORTE, TIPO_iva, INC_PP, IVA_INC, 0.00 AS PVERDE 
from ['+@GESTION+'].dbo.portes) DAV ON DAV.EMPRESA=CAV.EMPRESA AND DAV.NUMERO=CAV.NUMERO AND DAV.LETRA=CAV.LETRA 
LEFT JOIN ['+@GESTION+'].dbo.tipo_iva IVA ON IVA.CODIGO=DAV.TIPO_IVA
INNER JOIN ['+@GESTION+'].dbo.clientes CLI ON CLI.CODIGO=CAV.CLIENTE
LEFT JOIN ['+@GESTION+'].dbo.tipo_iva IVC ON IVC.CODIGO=CLI.TIPO_IVA WHERE DAV.IMPORTE!=0.00
GROUP BY CAV.EMPRESA, CAV.NUMERO, CAV.LETRA, CAV.CLIENTE, CLI.RECARGO, dav.tipo_iva, iva.IVA, IVA.RECARG, CAV.PRONTO
')
select 'vAlbaranes_Pie'




IF EXISTS (SELECT * FROM sys.objects where name='vArticulos') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vArticulos]
as
select * from ['+@GESTION+'].dbo.articulo WHERE BAJA=0
')
select 'vArticulos'




IF EXISTS (SELECT * FROM sys.objects where name='vArticulosStock') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vArticulosStock] as

select alm.codigo as almacen, 
case when coalesce(lot.lote,0)=1 then coalesce(stl.stock_uds_lot,0.00) 
else coalesce(stc.stock_uds,0.00) end as ArticuloStock

,art.[CODIGO]
,replace(art.NOMBRE,''"'',''-'') as NOMBRE
,art.[ABREV]
,art.[FAMILIA]
,art.[MARCA]
,art.[MINIMO]
,art.[MAXIMO]
,art.[AVISO]
,art.[BAJA]
,art.[TIPO_IVA]
,art.[RETENCION]
,art.[IVA_INC]
,art.[COST_ULT1]
,art.[FECHA_ULT]
,art.[ULT_FECHA]
,art.[PMCOM1]
,art.[IMAGEN]
,art.[CARAC]
,art.[FECHAALTA]
,art.[FECHABAJA]
,art.[UBICACION]
,art.[MEDIDAS]
,art.[PESO]
,art.[LITROS]
,replace(cast(art.OBSERVACIO as varchar(max)),''"'',''-'') as OBSERVACIO
,art.[UNICAJA]
,art.[DESGLOSE]
,art.[ARANCELES]
,art.[DEFINICION2]
,art.[SUBFAMILIA]
,art.[INTERNET]
,art.[VISTA]
,art.[FPAG]
,art.[PVERDE]
,art.[P_IMPORTE]
,art.[P_TAN]
,art.[LCOLOR]
,art.[MARGEN]
,art.[TCP]
,art.[VENSERIE]
,art.[PUNTOS]
,art.[DES_ESC]
,art.[TIPO_ART]
,art.[MODELO]
,art.[COCINA]
,art.[STOCK]
,art.[ART_IMPUES]
,replace(cast(art.NOMBRE2 as varchar(max)),''"'',''-'') as NOMBRE2
,art.[COLOR_ART]
,art.[TIPO_PVP]
,art.[COST_ESCAN]
,art.[TIPO_ESCAN]
,art.[ART_CANON]
,art.[ACTUA_COLO]
,art.[FACT_AREPE]
,art.[GARANTIA]
,art.[ALQUILER]
,art.[ORDEN]
,art.[C_ENT]
,art.[CN8]
,art.[IVALOT]
,art.[ARTANT]
,art.[REPORTETIQ]
,art.[GUID]
,art.[IMPORTAR]
,art.[DTO1]
,art.[DTO2]
,art.[DTO3]
,art.[ISP]
,art.[GRUPOIVA]

from ['+@GESTION+'].dbo.articulo art
inner join ['+@GESTION+'].dbo.almacen alm on 1=1
left join  ['+@LOTES+'].dbo.artlot lot on lot.articulo=art.codigo
left join (
	select empresa, almacen, articulo, sum(peso) as stock_peso_lot, sum(unidades) as stock_uds_lot 
	from ['+@LOTES+'].[dbo].stocklotes 
	group by empresa, almacen, articulo
) stl on stl.articulo=art.codigo and stl.almacen=alm.codigo
left join (
	select empresa, almacen, articulo, sum(final) as stock_uds 
	from ['+@GESTION+'].dbo.stocks2 
	group by empresa, almacen, articulo
) stc on stc.almacen=alm.codigo and stc.articulo=art.codigo
where art.BAJA=0
')
select 'vArticulosStock'




IF EXISTS (SELECT * FROM sys.objects where name='vBuscarLotes') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vBuscarLotes]
as
select b.PESO as pesoArticulo, b.stock as controlStock, 
isnull(c.CADUCIDAD,'''') as Lcaducidad, c.LOTE as Llote, c.UNIDADES-coalesce(convert(decimal(16,4),t.unidades),0.00) as Lunidades, 
c.PESO-coalesce(convert(decimal(16,4),t.peso),0.00) as Lpeso, 
a.EMPRESA, a.NUMERO, A.LETRA, a.LINIA, a.ARTICULO, a.DEFINICION, a.UNIDADES, a.SERVIDAS, a.CAJAS as pCajas, 
a.CAJASERV as pCajaServ, 
isnull((select MAX(CAJAS) from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
       and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO),0) 
       as tempMaxCajas, 
isnull((select UNIDADES from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
       and linea=a.LINIA 
       and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO 
       and lote  collate Modern_Spanish_CI_AI=c.LOTE),0) 
       as tempUnidades, 
isnull((select CAJAS from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
       and linea=a.LINIA 
       and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO 
       and lote  collate Modern_Spanish_CI_AI=c.LOTE),0) 
       as tempCajas, 
isnull((select PESO from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
       and linea=a.LINIA 
       and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO 
       and lote  collate Modern_Spanish_CI_AI=c.LOTE),0.000) 
       as tempPeso, 
b.UNICAJA 
       , CASE WHEN coalesce(m2.valor,'''')='''' OR  coalesce(m2.valor,'''')=''.F.'' THEN 0 ELSE 1 END as VENTA_CAJAS
       , c.almacen
FROM ['+@GESTION+'].[dbo].[d_pedive] a 
INNER JOIN ['+@GESTION+'].[dbo].[C_pedive] D ON D.EMPRESA=A.EMPRESA AND D.NUMERO=A.NUMERO AND D.LETRA=A.LETRA
left join ['+@GESTION+'].[dbo].articulo b on b.CODIGO=a.ARTICULO 
left join ['+@LOTES+'].[dbo].stocklotes c on c.ARTICULO=a.ARTICULO and c.almacen=d.almacen
LEFT JOIN (select T.empresa, T.almacen, T.articulo, T.LOTE, SUM(T.unidades) AS UNIDADES, 
	SUM(CONVERT(DECIMAL(16,4),COALESCE(T.PESO,''0.00''))) AS PESO from Traspaso_Temporal t 
	inner join ['+@GESTION+'].dbo.C_pedive c on c.empresa=t.empresa collate database_default and c.numero=t.numero collate database_default 
	and c.letra=t.letra collate database_default 
	INNER JOIN ['+@GESTION+'].dbo.D_pedive D on D.empresa=c.empresa collate database_default and D.numero=c.numero collate database_default 
	and D.letra=c.letra AND D.LINIA=T.LINeA  where c.traspasado=0 AND D.SERVIDAS<D.UNIDADES 
	GROUP BY T.empresa, T.almacen, T.articulo, T.LOTE
) T on T.EMPRESA collate SQL_Latin1_General_CP1_CI_AS=A.EMPRESA 
	AND t.articulo collate SQL_Latin1_General_CP1_CI_AS=a.articulo 
	and t.almacen collate SQL_Latin1_General_CP1_CI_AS=d.almacen 
	and t.lote collate SQL_Latin1_General_CP1_CI_AS=c.lote
left join ['+@GESTION+'].dbo.multicam m2 on m2.codigo=A.articulo and m2.campo=''SCA''
')
select 'vBuscarLotes'




IF EXISTS (SELECT * FROM sys.objects where name='vSTOCKS') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vSTOCKS] as
(
SELECT TOP 100 PERCENT A.CODIGO, A.NOMBRE, COALESCE(MOV.EMPRESA,''01'') AS EMPRESA, COALESCE(MOV.ALMACEN,'''') AS ALMACEN, COALESCE(MOV.TIPO,'''') AS TIPOMOV, COALESCE(MOV.UNIDADES,0) AS UNIDADES, COALESCE(MOV.PESO,0) AS PESO , MOV.FECHASTOCK
FROM ['+@GESTION+'].DBO.ARTICULO A INNER JOIN ['+@LOTES+'] .DBO.artlot AL ON AL.articulo = A.CODIGO AND AL.LOTE=0
LEFT JOIN (
SELECT ''IN'' AS TIPO, I.EMPRESA, I.ALMACEN, I.ARTICULO, I.UNIDADES AS UNIDADES, I.PESO AS PESO, I.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.regulari I INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=I.ARTICULO AND LOT.LOTE=0 WHERE I.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND I.FECHA=(SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=I.EMPRESA AND ALMACEN=I.ALMACEN AND ARTICULO=I.ARTICULO   GROUP BY EMPRESA, ALMACEN, ARTICULO)
UNION ALL
SELECT ''SI'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, (L.UNIDADES) AS UNIDADES, L.PESO AS PESO, L.FECHA AS FECHASTOCKD FROM ['+@GESTION+'].DBO.stockini L  INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL 
select ''TO'' AS TIPO, C.EMPRESA, C.ALMORIG, L.ARTICULO, -L.UNIDADES, -L.PESO, c.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.c_albatr c inner join ['+@GESTION+'].DBO.d_albatr L on l.empresa=c.empresa and l.numero=c.numero INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE c.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND c.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMORIG AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
select ''TD'' AS TIPO, C.EMPRESA, C.ALMDEST, L.ARTICULO, L.UNIDADES, L.PESO, c.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.c_albatr c inner join ['+@GESTION+'].DBO.d_albatr L on l.empresa=c.empresa and l.numero=c.numero INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE c.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND c.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMDEST AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
		
UNION ALL 
SELECT ''AV'' AS TIPO, C.EMPRESA, C.ALMACEN, L.ARTICULO, -(L.UNIDADES) as UNIDADES, -(L.PESO) AS PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.C_ALBVEN C INNER JOIN ['+@GESTION+'].DBO.D_ALBVEN L ON L.EMPRESA=C.EMPRESA AND L.LETRA=C.LETRA AND L.NUMERO=C.NUMERO INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''AC'' AS TIPO, C.EMPRESA, C.ALMACEN, L.ARTICULO, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.C_ALBCOM C INNER JOIN ['+@GESTION+'].DBO.D_ALBCOM L ON L.EMPRESA=c.empresa and l.numero=c.numero and l.PROVEEDOR =c.proveedor INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''CP'' AS TIPO, C.EMPRESA, C.ALMACEN, C.ARTICULO, C.ENTRADA AS UNIDADES, C.PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.C_PROD C INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=C.ARTICULO AND LOT.LOTE=0 WHERE C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=C.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''DP'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, -(L.SALIDA) AS UNIDADES, -(L.PESO) AS PESO, L.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.D_PROD L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''CT'' AS TIPO, C.EMPRESA, C.ALMACEN, C.ARTICULO, -(C.SALIDA) AS UNIDADES, -(C.PESO) AS PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.C_TRANS C INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=C.ARTICULO AND LOT.LOTE=0 WHERE C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=C.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
		
UNION ALL
SELECT ''DT'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, (L.ENTRADA) AS UNIDADES, (L.PESO) AS PESO, L.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.D_TRANS L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''AR'' AS TIPO, C.EMPRESA, C.ALMACEN, L.ARTICULO, -L.UNIDADES, -L.PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.c_albare C INNER JOIN ['+@GESTION+'].DBO.D_albare L ON L.EMPRESA=C.EMPRESA AND L.NUMERO=C.NUMERO INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 WHERE C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=L.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
select ''DV'' AS TIPO, C.EMPRESA, C.ALMACEN, L.ARTICULO, -(L.UNIDADES-L.SERVIDAS) AS UNIDADES, -(L.PESO-(L.SERVIDAS*(CONVERT(DECIMAL(10,3),CASE WHEN A.PESO='''' THEN ''0.00'' ELSE A.PESO END)))) AS PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.C_ALBDEP C INNER JOIN ['+@GESTION+'].DBO.D_ALBDEP L ON L.EMPRESA=C.EMPRESA AND L.LETRA=C.LETRA AND L.NUMERO=C.NUMERO INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 INNER JOIN ['+@GESTION+'].DBO.ARTICULO A ON A.CODIGO=L.ARTICULO WHERE C.TRASPASADO=0 AND L.UNIDADES>L.SERVIDAS AND C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
UNION ALL
SELECT ''DC'' AS TIPO, C.EMPRESA, C.ALMACEN, L.ARTICULO, (L.UNIDADES-L.SERVIDAS) AS UNIDADES, (L.PESO-(L.SERVIDAS*(CONVERT(DECIMAL(10,3),CASE WHEN A.PESO='''' THEN ''0.00'' ELSE A.PESO END)))) AS PESO, C.FECHA AS FECHASTOCK FROM ['+@GESTION+'].DBO.c_depcom C INNER JOIN ['+@GESTION+'].DBO.D_DEPCOM L ON L.EMPRESA=C.EMPRESA AND L.NUMERO=C.NUMERO INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=0 INNER JOIN ['+@GESTION+'].DBO.ARTICULO A ON A.CODIGO=L.ARTICULO WHERE C.TRASPASADO=0 AND L.UNIDADES>L.SERVIDAS AND C.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND C.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@GESTION+'].DBO.regulari WHERE EMPRESA=C.EMPRESA AND ALMACEN=C.ALMACEN AND ARTICULO=L.ARTICULO GROUP BY EMPRESA, ALMACEN, ARTICULO),''01-01-2001'')
) MOV ON A.CODIGO=MOV.ARTICULO and MOV.EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
WHERE A.STOCK=0 ORDER BY 1,4
)
')
select 'vSTOCKS'




IF EXISTS (SELECT * FROM sys.objects where name='vStock') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vStock] as
SELECT     EMPRESA, CODIGO AS ARTICULO, ALMACEN, SUM(COALESCE(UNIDADES,0)) AS UNIDADES, SUM(COALESCE(PESO,0)) AS PESO
FROM         dbo.vSTOCKS 
where EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
GROUP BY EMPRESA, CODIGO, ALMACEN
HAVING SUM(COALESCE(UNIDADES,0))<>0
')
select 'vStock'




IF EXISTS (SELECT * FROM sys.objects where name='vCajasOunidades') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vCajasOunidades] as
select isnull(b.PESO,0.000) as pesoArticulo, isnull(b.UNICAJA,0) as cajasArticulo, isnull(b.stock,0) as cntrlSotck, 
a.EMPRESA, a.NUMERO, a.LETRA, a.LINIA, a.ARTICULO, a.DEFINICION
, isnull(a.UNIDADES,0.00) as UNIDADES
, isnull(a.CAJAS,0.00) as pCajas
, isnull(a.PESO,0.000) as PESO
, isnull(a.TRASPASO,0.00) as TRASPASO
, isnull(C.unidades,0.00) as elStock
, isnull(c.peso,0.000) AS STOCKPESO
, isnull(c.unidades*b.unicaja,0.00) AS STOCKCAJAS
,
isnull(
	(select cajas from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
	and linea=a.LINIA 
	and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO )
,a.CAJAS) as tempCajas, 
isnull(
	(select unidades from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
	and linea=a.LINIA 
	and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO )
,a.UNIDADES) as tempUds, 
isnull(
	(select peso from Traspaso_Temporal where numero collate Modern_Spanish_CI_AI=a.NUMERO 
	and linea=a.LINIA 
	and articulo  collate Modern_Spanish_CI_AI=a.ARTICULO)
,0.000) as tempPeso 

FROM ['+@GESTION+'].[dbo].[d_pedive] a 
left join ['+@GESTION+'].[dbo].articulo b on b.CODIGO=a.ARTICULO 
left join [vStock] c on c.ARTICULO=a.ARTICULO 
where a.EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from Configuracion)
')
select 'vCajasOunidades'




IF EXISTS (SELECT * FROM sys.objects where name='vContadorDescuentoAlbaran') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vContadorDescuentoAlbaran] AS
select IdAlbaran, sum(DTO1) as DTO1, sum(DTO2) as DTO2 from vAlbaranes_Detalle
group by IdAlbaran
')
select 'vContadorDescuentoAlbaran'




IF EXISTS (SELECT * FROM sys.objects where name='vDatosEmpresa') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vDatosEmpresa] AS
SELECT E.CODIGO, RTRIM(LTRIM(E.NOMBRE)) AS NOMBRE, RTRIM(LTRIM(E.NOMBRE2)) AS NOMBRE2, RTRIM(LTRIM(E.CIF)) AS CIF, 
RTRIM(LTRIM(E.DIRECCION)) AS DIRECCION, RTRIM(LTRIM(E.CODPOS)) AS CODPOS, RTRIM(LTRIM(E.POBLACION)) AS POBLACION, 
RTRIM(LTRIM(E.PROVINCIA)) AS PROVINCIA, RTRIM(LTRIM(E.TELEFONO)) AS TELEFONO, RTRIM(LTRIM(E.FAX)) AS FAX, 
RTRIM(LTRIM(E.MOBIL)) AS MOBIL, RTRIM(LTRIM(E.EMAIL)) AS EMAIL, RTRIM(LTRIM(E.[HTTP])) AS WEB, 
RTRIM(LTRIM(E.TXTFACTU1)) AS TXTFACTU1, RTRIM(LTRIM(E.TXTFACTU2)) AS TXTFACTU2, e.logo, e.almacen, f.tarifapret, E.LETRA
FROM ['+@GESTION+'].DBO.EMPRESA E  
LEFT JOIN ['+@GESTION+'].DBO.FACTUCNF F ON F.EMPRESA=E.CODIGO
')
select 'vDatosEmpresa'




IF EXISTS (SELECT * FROM sys.objects where name='vEnvioeti_Rep') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vEnvioeti_Rep] as
SELECT cav.empresa, emp.nombre as n_empresa, cav.numero, cav.letra, cav.fecha, cav.ruta, coalesce(rut.nombre,'''''''') as n_ruta
, cav.cliente, cli.nombre as n_cliente, coalesce(env.DIRECCiON,'''') as direccion, coalesce(env.CODPOS,'''') as codpos 
, coalesce(env.POBLACION,'''') as poblacion, coalesce(env.PROVINCIA,'''') as provincia, convert(int,coalesce(eti.bultos,0)) as bultos
FROM ['+@GESTION+'].[dbo].[c_albven] cav 
inner join ['+@GESTION+'].dbo.empresa emp on emp.codigo=cav.empresa
inner join ['+@GESTION+'].dbo.clientes cli on cli.codigo=cav.cliente
left  join ['+@GESTION+'].dbo.rutas rut on rut.codigo=cav.ruta
left  join ['+@GESTION+'].dbo.env_cli env on env.cliente=cav.cliente and env.LINEA=cav.ENV_CLI 
left  join ['+@GESTION+'].dbo.envioeti eti on eti.empresa=cav.empresa and eti.numero=cav.numero and eti.letra=cav.letra
')
select 'vEnvioeti_Rep'




IF EXISTS (SELECT * FROM sys.objects where name='vErrorLotesTrasTemp') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vErrorLotesTrasTemp] as
SELECT T.empresa, T.numero, T.letra, T.ARTICULO , art.NOMBRE
FROM Traspaso_Temporal T 
INNER JOIN ['+@LOTES+'].dbo.artlot A ON A.ARTICULO collate Modern_Spanish_CI_AI=T.articulo 
LEFT JOIN ['+@GESTION+'].dbo.articulo art on art.CODIGO collate Modern_Spanish_CI_AI=T.articulo
WHERE A.lote =1 AND T.lote collate Modern_Spanish_CI_AI=''''
')
select 'vErrorLotesTrasTemp'




IF EXISTS (SELECT * FROM sys.objects where name='vFamilias') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vFamilias] as
SELECT codigo,nombre from ['+@GESTION+'].[dbo].familias
')
select 'vFamilias'




IF EXISTS (SELECT * FROM sys.objects where name='vIVA') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vIVA] AS
SELECT '''+@EJERCICIO+'''+empresa+CAST(CUENTA AS VARCHAR)+replace(NUMFRA,space(1),''0'') AS IDIVA, 
'''+@EJERCICIO+''' AS EJER, EMPRESA, CUENTA, FECHA,
'''+@EJERCICIO+'''+EMPRESA+replace(CAST(NUMFRA AS VARCHAR(10)),space(1), ''0'') COLLATE Modern_Spanish_CI_AI AS IDFACTURA, 
NUMFRA AS NUMFRA, SUM(BIMPO) AS BASE, PORCEN_IVA, SUM(IVA) AS IMP_IVA, PORCEN_REC,  SUM(RECARGO) AS IMP_RECARGO,
SUM(BIMPO)+SUM(IVA)+SUM(RECARGO) AS TOTAL, tipo_iva
FROM ['+@GESTION+'].DBO.IVAREPER
WHERE LEFT(CUENTA,3)=''430''
group by EMPRESA, CUENTA, FECHA, NUMFRA, PORCEN_IVA, PORCEN_REC, tipo_iva
')
select 'vIVA'




IF EXISTS (SELECT * FROM sys.objects where name='vLotesDelArticulo') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vLotesDelArticulo] as
select	a.almacen, a.articulo, a.caducidad, a.color, a.empresa, a.lote, a.peso, a.talla, a.ubica, a.unidades,
b.peso as pesoArticulo, b.unicaja as unidadesOcajas
from ['+@LOTES+'].[dbo].[stocklotes] a
left join ['+@GESTION+'].[dbo].articulo b on b.CODIGO=a.ARTICULO
where unidades>0
')
select 'vLotesDelArticulo'




IF EXISTS (SELECT * FROM sys.objects where name='vMarcas') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vMarcas]
as
SELECT CODIGO, NOMBRE, DESCUEN, TCP, MARGEN, FOTO from ['+@GESTION+'].[dbo].marcas
')
select 'vMarcas'




IF EXISTS (SELECT * FROM sys.objects where name='vOfertas') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vOfertas]
AS
SELECT REPLACE(TIPO+CLIENTE+ARTICULO+color+TALLA+FAMILIA+SUBFAMILIA+STR(LINEA,5),SPACE(1),'''') AS IDOFERTA,TIPO, CLIENTE, ARTICULO, color, TALLA, FAMILIA, SUBFAMILIA, MARCA, LINDES, LINEA, FECHA_IN, FECHA_FIN, UNI_INI, UNI_FIN, PVP, DTO1, DTO2, MONEDA, TARIFA
FROM (
SELECT ''CLIENTE'' AS TIPO, CLIENTE, ARTICULO, color, TALLA, FAMILIA, SUBFAMILIA, MARCA, '''' AS LINDES, LINIA AS LINEA, FECHA_IN, FECHA_FIN, UNI_MIN AS UNI_INI, (CASE WHEN UNI_MAX=0 THEN 999999.99 ELSE UNI_MAX END) AS UNI_FIN, PVP, DTO1, DTO2, MONEDA, '''' AS TARIFA 
FROM ['+@GESTION+'].DBO.DESCUEN
UNION 
SELECT ''ARTICULO'' AS TIPO, '''' AS CLIENTE, ARTICULO, color, talla, '''' AS FAMILIA, '''' AS SUBFAMILIA, '''' AS MARCA, '''' AS  LINDES, LINEA, FECHA_IN, FECHA_FIN , DESDE AS UNI_INI, (CASE WHEN HASTA=0 THEN 999999.99 ELSE HASTA END) AS UD_FIN, PVP, DESCUENTO AS DTO1, 0 AS DTO2, MONEDA, TARIFA FROM ['+@GESTION+'].DBO.OFERTAS
UNION 
SELECT ''LINDESC'' AS TIPO, C.CODIGO AS CLIENTE, D.ARTICULO, SPACE(6) AS COLOR, SPACE(8) AS TALLA, D.FAMILIA, D.SUBFAMILIA, D.MARCA, D.CODIGO AS LINDES, D.LINEA, D.FECHA_IN, D.FECHA_FIN, D.UNI_INI, D.UNI_FIN, D.PVP, D.DTO1, D.DTO2, D.MONEDA, '''' AS TARIFA FROM ['+@GESTION+'].DBO.CLIENTES C INNER JOIN ['+@GESTION+'].DBO.LIN_DESC D ON D.CODIGO=C.LIN_DES
) O 
')
select 'vOfertas'




IF EXISTS (SELECT * FROM sys.objects where name='vPedidos_Pie') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vPedidos_Pie] AS
SELECT 
'''+@EJERCICIO+''' as EJER,
('''+@EJERCICIO+'''+CAV.empresa+replace(CAV.LETRA,space(1),''0'')+replace(LEFT(CAV.NUMERO,10),space(1),''0'')) collate Modern_Spanish_CI_AI as IDPEDIDO,
CAV.EMPRESA, CAV.NUMERO, CAV.LETRA, CAV.CLIENTE, IVA.IVA as IVApc, 
case when cli.recargo=1 then IVA.RECARG else 0 end as recargoPC, 
ISNULL(CAV.ENTREGA,0.00) as ENTREGA,
CAV.PRONTO as pPago,

dav.tipo_iva, iva.IVA as porcen_iva, iva.iva,
CASE WHEN CLI.RECARGO=1 THEN IVA.RECARG ELSE 0.00 END AS TIPO_RE, SUM(DAV.PVERDE) AS PVERDE,

SUM(CASE WHEN DAV.IVA_INC=1 THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE 
	ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE) AS IMPORTE,

ROUND(SUM((CASE WHEN DAV.IVA_INC=1 THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE
	ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE)*(IVA.IVA/100)),2) AS IVAimp,

coalesce(CASE WHEN CLI.RECARGO=1 THEN round(SUM((CASE WHEN DAV.IVA_INC=1 THEN DAV.IMPORTE*(1-(IVA.RECARG/100)) 
	ELSE CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE 
		ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE)*(IVA.RECARG/100)),2) END, 0.00) AS RECARGO,

(CAV.TOTALPED * CAV.PRONTO) / 100 as impPP,
isnull(ent.IMPORTE,0.00) as entIMP,

-- TOTALDOC (lo calculamos por si hay más de un IVA en el documento)
SUM(CASE WHEN DAV.IVA_INC=1 THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE 
	ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE)+ROUND(SUM((CASE WHEN DAV.IVA_INC=1 THEN DAV.IMPORTE*(1-(IVA.IVA/100)) 
ELSE CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE
	ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE)*(IVA.IVA/100)),2)+coalesce(CASE WHEN CLI.RECARGO=1 THEN round(SUM((CASE WHEN DAV.IVA_INC=1			THEN CASE WHEN DAV.INC_PP=0 THEN DAV.IMPORTE 
		ELSE (DAV.IMPORTE)*(1-((CAV.PRONTO/100))) END END + DAV.PVERDE)*(IVA.RECARG/100)),2) END, 0.00) as TOTALDOC,

CAV.TOTALPED,
isnull(CAV.PRONTO,0.00) as PRONTO,

isnull((select max(isnull(cast(PVERDE as int),0)) from ['+@GESTION+'].dbo.articulo where CODIGO in
(select ARTICULO from ['+@GESTION+'].DBO.D_PEDIVE where EMPRESA+NUMERO+LETRA=CAV.EMPRESA+CAV.NUMERO+CAV.LETRA and PVERDE>0)
),0) as PVERDEver

FROM ['+@GESTION+'].DBO.C_PEDIVE CAV 
INNER JOIN 
(select EMPRESA, NUMERO, LETRA, ARTICULO, IMPORTE, TIPO_iva, 1 AS INC_PP, 0 AS IVA_INC, PVERDE 
from ['+@GESTION+'].dbo.D_PEDIVE where TIPO_IVA<>'''') 
DAV ON DAV.EMPRESA=CAV.EMPRESA AND DAV.NUMERO=CAV.NUMERO AND DAV.LETRA=CAV.LETRA 
LEFT JOIN ['+@GESTION+'].dbo.tipo_iva IVA ON IVA.CODIGO=DAV.TIPO_IVA
INNER JOIN ['+@GESTION+'].dbo.clientes CLI ON CLI.CODIGO=CAV.CLIENTE
left join ['+@GESTION+'].dbo.entre_pv ent on ent.NUMERO=CAV.NUMERO and ent.EMPRESA=CAV.EMPRESA and ent.LETRA=CAV.letra
GROUP BY CAV.EMPRESA, CAV.NUMERO, CAV.LETRA, CAV.CLIENTE, CLI.RECARGO, IVA.IVA, IVA.RECARG, CAV.ENTREGA, ent.IMPORTE
	,CAV.TOTALDOC,CAV.PRONTO,CAV.TOTALPED,DAV.TIPO_IVA,CAV.PRONTO
')
select 'vPedidos_Pie'




IF EXISTS (SELECT * FROM sys.objects where name='vPedidos') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vPedidos] AS
SELECT  1 as Nodo
	, cast('''' as varchar(50)) as MODO
	, CONCAT('''+@EJERCICIO+''',CAV.empresa,replace(CAV.LETRA,space(1),''0''),replace(LEFT(CAV.NUMERO,10),space(1),''0'')) as IDPEDIDO	
	, '''+@EJERCICIO+''' as EJER , CAV.EMPRESA, convert(varchar(10), CAV.ENTREGA, 103) as ENTREGA
	, CAV.LETRA + CAV.NUMERO as PEDIDO , CAV.NUMERO as numero, CAV.LETRA, CAV.FECHA as sqlFecha
	, convert(varchar(10), CAV.FECHA, 103) as FECHA
	, CAV.CLIENTE, CAV.REFERCLI, CAV.ENV_CLI as DIRECCION, CAV.CLIENTE as codCliente 			
	, CAV.USUARIO, CAV.pronto, CAV.VENDEDOR
	, ISNULL(CAST(CAV.OBSERVACIO AS VARCHAR(max)),'''') AS OBSERVACIO
	, env.DIRECCION as nDireccion, ven.nombre as nVendedor
	, cli.nombre collate Modern_Spanish_CI_AI as nCliente, cli.RUTA
	, env.CODPOS+'' ''+env.POBLACION as Ciudad, env.PROVINCIA as Provincia
	, DAV.PRESUP as presupuesto, '''+@EJERCICIO+''' + CAV.empresa + CAV.letra + DAV.PRESUP as idpresuven
	, case when TRASPASADO=1 then ''TRASPASADO''
			when FINALIZADO=1 then ''FINALIZADO''
			when CANCELADO =1 then ''CANCELADO'' 
			else ''PENDIENTE'' END 
			as ESTADO			  
	, PCN.Contacto as contacto, CON.Persona  as nContacto
	, ser.nombre as laSerie		
	, CPA.FAMILIA as EWFAMILIA, FAM.NOMBRE as FAMILIA
	, sum(isnull(pie.TOTALDOC,0.00)) as TOTALDOC
	, case when sum(pie.TOTALDOC)>0 then 
		replace(replace(replace(convert(varchar, cast(sum(isnull(pie.TOTALDOC,0)) as money),1),''.'',''_''),'','',''.''),''_'','','') +'' €''
		else '''' end as TOTALDOCformato
	,CAV.TOTALDOC-CAV.TOTALPED AS importeIVA, CAV.TOTALDOC as totalConIVA
	,cast(0.00 as numeriC(18,2)) AS TOTAL, CAST(ROUND(CAV.TOTALPED,2) AS NUMERIC(18,2)) AS TOTALPED, cast(0.00 as numeriC(18,2)) as IVA

FROM ['+@GESTION+'].dbo.c_pedive CAV
INNER JOIN ['+@GESTION+'].dbo.clientes CLI ON CLI.CODIGO=CAV.CLIENTE
left join  ['+@GESTION+'].dbo.entre_pv ent on ent.NUMERO=CAV.NUMERO and ent.EMPRESA=CAV.EMPRESA and ent.LETRA=CAV.letra
left join  ['+@GESTION+'].dbo.env_cli env on env.cliente=cav.cliente and env.linea=cav.env_cli
left join  ['+@GESTION+'].dbo.vendedor ven on ven.codigo=cav.vendedor
LEFT JOIN (SELECT EMPRESA, NUMERO, LETRA, LEFT(MAX(DOC_NUM),10) AS PRESUP 
		FROM ['+@GESTION+'].DBO.D_PEDIVE 
		GROUP BY EMPRESA, NUMERO, LETRA) DAV 
		ON DAV.EMPRESA=CAV.EMPRESA AND DAV.LETRA=CAV.LETRA AND DAV.NUMERO=CAV.NUMERO
LEFT JOIN Pedidos_Contactos PCN ON PCN.IDPEDIDO COLLATE Modern_Spanish_CI_AI='''+@EJERCICIO+'''+CAV.LETRA+CAV.NUMERO
LEFT JOIN ['+@GESTION+'].DBO.CONT_CLI CON ON CON.CLIENTE=CAV.CLIENTE AND CON.LINEA=PCN.CONTACTO
LEFT JOIN Pedidos_Familias CPA ON CPA.EJERCICIO='''+@EJERCICIO+''' COLLATE Modern_Spanish_CI_AI
		AND CPA.NUMERO COLLATE Modern_Spanish_CI_AI=CAV.NUMERO AND CPA.LETRA COLLATE Modern_Spanish_CI_AI=CAV.LETRA
LEFT JOIN ['+@GESTION+'].DBO.FAMILIAS FAM ON FAM.CODIGO=CPA.FAMILIA COLLATE Modern_Spanish_CI_AI
LEFT JOIN vSeries ser on ser.codigo=CAV.LETRA
LEFT JOIN vPedidos_Pie pie on pie.IDPEDIDO collate Modern_Spanish_CI_AI=CONCAT('''+@EJERCICIO+''',CAV.empresa,replace(CAV.LETRA,space(1),''0''),replace(LEFT(CAV.NUMERO,10),space(1),''0''))
	
GROUP BY  CAV.empresa, CAV.LETRA, CAV.NUMERO, CAV.EMPRESA, CAV.ENTREGA, CAV.FECHA
	, CAV.CLIENTE, CAV.REFERCLI, CAV.ENV_CLI, CAV.CLIENTE	
	, CAV.USUARIO, CAV.pronto, CAV.VENDEDOR
	, env.DIRECCION, ven.nombre, CAST(CAV.OBSERVACIO AS VARCHAR(max))
	, cli.nombre collate Modern_Spanish_CI_AI
	, cli.RUTA, env.CODPOS, env.POBLACION, env.PROVINCIA, DAV.PRESUP, TRASPASADO, FINALIZADO, CANCELADO, PCN.Contacto
	, CON.Persona, ser.nombre, CPA.FAMILIA, FAM.NOMBRE,  CAV.TOTALDOC, CAV.TOTALPED	
')
select 'vPedidos'




IF EXISTS (SELECT * FROM sys.objects where name='vPedidos_Detalle') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vPedidos_Detalle] as
SELECT '''+@EJERCICIO+''' + D.empresa + cast(D.letra as char(2)) + D.numero + CAST(D.linia as varchar) AS IDPEDIDOLIN, 
CONCAT('''+@EJERCICIO+''',D.empresa,replace(D.LETRA,space(1),''0''),replace(LEFT(D.NUMERO,10),space(1),''0'')) AS IDPEDIDO, 
'''+@EJERCICIO+''' AS EJER, cast(D.letra as char(2))+d.numero as PEDIDO, D.CLIENTE, D.ARTICULO, D.DEFINICION, D.UNIDADES, D.cajas, 
isnull(D.PRECIO,0.00) as PRECIO,  
case when D.PRECIO>0 then 
replace(replace(replace(convert(varchar, cast(D.PRECIO as money),1),''.'',''_''),'','',''.''),''_'','','')+'' €'' else '''' end as PRECIOf, 
isnull(D.importe,0.00) as IMPORTE, 
case when D.importe>0 then 
replace(replace(replace(convert(varchar, cast(D.importe as money),1),''.'',''_''),'','',''.''),''_'','','')+'' €'' else '''' end as IMPORTEf,  
D.TIPO_IVA, COALESCE(iva.iva, 0) AS TPCIVA,  
CAST(ROUND(D.IMPORTE*COALESCE(iva.iva, 0)*0.01,2) AS NUMERIC(18,2)) AS IVA, D.SERVIDAS, D.LINIA, D.DTO1, D.DTO2, D.EMPRESA, 
D.importeiva AS IMPORTEIVA, 
isnull(D.PESO,0.00) as PESO, 
case when D.PESO>0 then 
replace(replace(replace(convert(varchar, cast(D.PESO as money),1),''.'',''_''),'','',''.''),''_'','','')+'' €'' else '''' end as PESOf,
D.NUMERO, cast(D.letra as char(2)) as LETRA,
art.UNICAJA, D.PVERDE,
case when D.PVERDE>0 then 
replace(replace(replace(convert(varchar, cast(D.PVERDE as money),1),''.'',''_''),'','',''.''),''_'','','')+'' €'' else '''' end as PVERDEf,

(select max(isnull(cast(PVERDE as int),0)) from ['+@GESTION+'].dbo.articulo where CODIGO in
(select CODIGO from ['+@GESTION+'].DBO.D_PEDIVE where EMPRESA+NUMERO+LETRA=D.EMPRESA+D.NUMERO+D.LETRA)
) as PVERDEver

FROM ['+@GESTION+'].DBO.D_PEDIVE D
LEFT JOIN ['+@GESTION+'].dbo.tipo_iva iva ON D.tipo_iva=iva.codigo
LEFT JOIN ['+@GESTION+'].dbo.articulo art ON art.CODIGO=D.ARTICULO
WHERE LEFT(D.CLIENTE,3)=''430''
')
select 'vPedidos_Detalle'




IF EXISTS (SELECT * FROM sys.objects where name='vPers_ContadorCajasAlbaran') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vPers_ContadorCajasAlbaran] as
select IDALBARAN, sum(cajas) as cajas from vAlbaranes_Detalle
group by IDALBARAN
')
select 'vPers_ContadorCajasAlbaran'




IF EXISTS (SELECT * FROM sys.objects where name='vReport_Pedidos') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vReport_Pedidos] as
SELECT "vClientes"."CP", "vClientes"."POBLACION", "vClientes"."PROVINCIA", "vClientes"."CIF", "vClientes"."DIRECCION", 
"vDatosEmpresa"."NOMBRE" as NombreEmpresa, "vDatosEmpresa"."DIRECCION" as DireccionEmpresa, "vDatosEmpresa"."CODPOS", 
"vDatosEmpresa"."POBLACION" as PoblacionEmpresa, "vDatosEmpresa"."PROVINCIA" as ProvinciaEmpresa, "vDatosEmpresa"."CIF" CifEmpresa, 
"vDatosEmpresa"."TXTFACTU1", "vDatosEmpresa"."TXTFACTU2", "vDatosEmpresa"."TELEFONO", "vDatosEmpresa"."FAX", "vDatosEmpresa"."WEB", 
"vClientes"."NOMBRE", "vPedidos"."CLIENTE", "vPedidos_Detalle"."IMPORTE", coalesce("vPedidos_Detalle"."DTO2",0) as DTO2, coalesce("vPedidos_Detalle"."DTO1",0) as DTO1, 
"vPedidos_Detalle"."DEFINICION", "vPedidos_Detalle"."UNIDADES", "vPedidos_Detalle"."PRECIO", "vPedidos".totalConIVA, "vPedidos"."TOTAL", 
"vPedidos".importeIVA, "vPedidos"."TOTALPED", "vClientes"."BANCO", "vClientes"."N_PAG", "vPedidos".numero, "vPedidos".FECHA, 
"vPedidos_Detalle".ARTICULO, "vPedidos".IDPEDIDO, "vPedidos".IVA, vPedidos_Detalle.TIPO_IVA, coalesce("vPedidos_Detalle".peso,0.00) as peso
, coalesce(vPedidos_Detalle.cajas, 0.00) as cajas, coalesce(vPedidos_Detalle.unicaja,0.00) as unicaja
FROM   (("vPedidos" "vPedidos" 
LEFT OUTER JOIN "vPedidos_Detalle" "vPedidos_Detalle" ON ("vPedidos"."IDPEDIDO"  COLLATE Modern_Spanish_CI_AI ="vPedidos_Detalle"."IDPEDIDO") 
				AND ("vPedidos"."EJER"="vPedidos_Detalle"."EJER")) 
LEFT OUTER JOIN "vDatosEmpresa" "vDatosEmpresa" ON "vPedidos"."EMPRESA"="vDatosEmpresa"."CODIGO") 
LEFT OUTER JOIN "vClientes" "vClientes" ON "vPedidos"."CLIENTE"="vClientes"."CODIGO"
')
select 'vReport_Pedidos'




IF EXISTS (SELECT * FROM sys.objects where name='vPers_ContadorPeso') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vPers_ContadorPeso] as
select idPedido, sum(peso) as peso from vReport_Pedidos
group by IdPedido
')
select 'vPers_ContadorPeso'




IF EXISTS (SELECT * FROM sys.objects where name='vPers_ContadorPesoAlbaran') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vPers_ContadorPesoAlbaran] as
select IDALBARAN, sum(PESO) as peso from vAlbaranes_Detalle
group by IDALBARAN
')
select 'vPers_ContadorPesoAlbaran'




IF EXISTS (SELECT * FROM sys.objects where name='vPvP') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  VIEW [dbo].[vPvP]
as
SELECT tarifa+articulo as idpvp, articulo, tarifa, pvp, pvpiva
FROM ['+@GESTION+'].dbo.pvp
')
select 'vPvP'




IF EXISTS (SELECT * FROM sys.objects where name='vRutas') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +' view [dbo].[vRutas] as
select	codigo, nombre from ['+@GESTION+'].dbo.rutas
')
select 'vRutas'




IF EXISTS (SELECT * FROM sys.objects where name='vSTOCK_LOTES') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vSTOCK_LOTES] as
(
SELECT TOP 100 PERCENT A.CODIGO, A.NOMBRE, COALESCE(MOV.EMPRESA,''01'') AS EMPRESA, COALESCE(MOV.ALMACEN,'''') AS ALMACEN, COALESCE(MOV.TIPO,'''') AS TIPOMOV, COALESCE(MOV.LOTE,''00000000'') AS LOTE, COALESCE(MOV.UNIDADES,0) AS UNIDADES, COALESCE(MOV.PESO,0) AS PESO , MOV.FECHASTOCK, MOV.CADUCIDAD
FROM ['+@GESTION+'].DBO.ARTICULO A 
LEFT JOIN (
SELECT  ''IN'' AS TIPO, I.EMPRESA, I.ALMACEN, I.ARTICULO, I.LOTE, I.UNIDADES-I.DEPOVEN+I.DEPOCOM AS UNIDADES, I.PESO-I.PESOVEN+I.PESOCOM AS PESO, I.FECHA AS FECHASTOCK, I.CADUCIDAD FROM ['+@LOTES+'].DBO.LTREGUL I INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=I.ARTICULO AND LOT.LOTE=1 WHERE I.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND I.FECHA=(SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=I.EMPRESA AND ALMACEN=I.ALMACEN AND ARTICULO=I.ARTICULO AND LOTE=I.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE)
UNION ALL
SELECT  ''SI'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, (L.UNIDADES-L.DEPOVEN+L.DEPOCOM) AS UNIDADES, L.PESO-L.PESOVEN+L.PESOCOM AS PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTSTINI L  INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL 
SELECT ''TO'' AS TIPO, L.EMPRESA, L.ALMORIG, L.ARTICULO, L.LOTE AS LOTE, -L.UNIDADES, -L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTALBTR L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMORIG AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
		
SELECT ''TD'' AS TIPO, L.EMPRESA, L.ALMDEST, L.ARTICULO, L.LOTE AS LOTE, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTALBTR L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMDEST AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL 
SELECT ''AV'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, -(L.UNIDADES) as UNIDADES, -(L.PESO) AS PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD FROM ['+@LOTES+'].DBO.LTALBVE L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT ''AC'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD FROM ['+@LOTES+'].DBO.LTALBCO L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT  ''CP'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTCPROD L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT  ''DP'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, -L.UNIDADES, -L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTDPROD L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT  ''CT'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, -L.UNIDADES, -L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD FROM ['+@LOTES+'].DBO.LTCTRAN L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
		
UNION ALL
SELECT  ''DT'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD FROM ['+@LOTES+'].DBO.LTDTRAN L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT ''AR'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, -L.UNIDADES, -L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTALBRE L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT  ''DV'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, -L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTDEPVE L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
UNION ALL
SELECT  ''DC'' AS TIPO, L.EMPRESA, L.ALMACEN, L.ARTICULO, L.LOTE, L.UNIDADES, L.PESO, L.FECHA AS FECHASTOCK, L.CADUCIDAD  FROM ['+@LOTES+'].DBO.LTDEPCO L INNER JOIN ['+@LOTES+'].DBO.ARTLOT LOT ON LOT.ARTICULO=L.ARTICULO AND LOT.LOTE=1 WHERE L.FECHA BETWEEN ''01-01-''+STR(YEAR(GETDATE()),4) AND GETDATE() AND L.FECHA>ISNULL((SELECT MAX(FECHA) FROM ['+@LOTES+'].DBO.LTREGUL WHERE EMPRESA=L.EMPRESA AND ALMACEN=L.ALMACEN AND ARTICULO=L.ARTICULO AND LOTE=L.LOTE GROUP BY EMPRESA, ALMACEN, ARTICULO, LOTE),''01-01-2001'')
) MOV ON A.CODIGO=MOV.ARTICULO AND MOV.EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
ORDER BY 1,4
)
')
select 'vSTOCK_LOTES'




IF EXISTS (SELECT * FROM sys.objects where name='vStockLOTE') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vStockLOTE] as
SELECT     EMPRESA, CODIGO AS ARTICULO, ALMACEN, LOTE, SUM(COALESCE(UNIDADES,0)) AS UNIDADES, SUM(COALESCE(PESO,0)) AS PESO
, MAX(CADUCIDAD) AS CADUCIDAD
FROM  dbo.vSTOCK_LOTES /*WHERE CODIGO=''V338''*/
where EMPRESA=(select Empresa collate Modern_Spanish_CS_AI from configuracion)
GROUP BY EMPRESA, CODIGO, ALMACEN, LOTE
HAVING SUM(COALESCE(UNIDADES,0))<>0
')
select 'vStockLOTE'




IF EXISTS (SELECT * FROM sys.objects where name='vZonas') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view [dbo].[vZonas] as
select M.CODIGO, M.NOMBRE, V.VALOR 
from ['+@GESTION+'] .dbo.mcampos m 
inner join ['+@GESTION+'].dbo.valmulti v on v.codigo=m.codigo where m.codigo=''ZNA''
')
select 'vZonas'




IF EXISTS (SELECT * FROM sys.objects where name='vImpresion') set @alterCreate = 'ALTER' else set @alterCreate='CREATE'
exec(@alterCreate +'  view vImpresion
as
select distinct tipo, ltrim(rtrim(crNumero)) as crNumero, crEmpresa, crLetra 
from MI_ImpresionPDF
where fechaInsertUpdate>dateadd(week,-1,getdate())
')
select 'vImpresion'




	RETURN -1
END TRY

BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pArticulos]...';


GO
CREATE PROCEDURE [dbo].[pArticulos] (@parametros varchar(max))
AS
SET NOCOUNT ON
BEGIN TRY	
	declare @modo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
		,	@error   varchar(max) = ''
		,	@datos   varchar(max) = ''

	if @modo='lista' BEGIN
		set @datos = (select * from vArticulosStock for JSON AUTO,INCLUDE_NULL_VALUES)
	END

	select (CONCAT('{"error":"',@error,'","datos":',@datos,'}')) as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
	select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pBasculas]...';


GO
CREATE PROCEDURE [dbo].[pBasculas] @parametros varchar(max)='[]'
AS
BEGIN TRY	
	declare @modo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
		,	@bascula varchar(50)
		,	@basculaTipoConexion varchar(50)
		,	@IP varchar(50)
		,	@puerto varchar(50)
		,	@sentencia varchar(50)
		,	@activa int


	--	BÁSCULA 01 =============================================================================================================
		set @bascula				= isnull((select JSON_VALUE(@parametros,'$.bascula1[0].bascula')),'')
		set @basculaTipoConexion	= isnull((select JSON_VALUE(@parametros,'$.bascula1[0].basculaTipoConexion')),'')
		set @IP						= isnull((select JSON_VALUE(@parametros,'$.bascula1[0].IP')),'')
		set @puerto					= isnull((select JSON_VALUE(@parametros,'$.bascula1[0].puerto')),'')
		set @sentencia				= isnull((select JSON_VALUE(@parametros,'$.bascula1[0].sentencia')),'')
		set @activa					= cast( isnull((select JSON_VALUE(@parametros,'$.bascula1[0].activa')),'') as int)

		if exists (select * from basculas where bascula=@bascula)
			update basculas set tipoConexion=@basculaTipoConexion, [ip]=@IP, puerto=@puerto, sentencia=@sentencia, activa=@activa
			where bascula=@bascula
		else
			insert into basculas (bascula, tipoConexion, [ip], puerto, sentencia, activa)
			values (@bascula, @basculaTipoConexion, @IP, @puerto, @sentencia, @activa)

	
	--	BÁSCULA 02 =============================================================================================================
		set @bascula				= isnull((select JSON_VALUE(@parametros,'$.bascula2[0].bascula')),'')
		set @basculaTipoConexion	= isnull((select JSON_VALUE(@parametros,'$.bascula2[0].basculaTipoConexion')),'')
		set @IP						= isnull((select JSON_VALUE(@parametros,'$.bascula2[0].IP')),'')
		set @puerto					= isnull((select JSON_VALUE(@parametros,'$.bascula2[0].puerto')),'')
		set @sentencia				= isnull((select JSON_VALUE(@parametros,'$.bascula2[0].sentencia')),'')
		set @activa					= cast( isnull((select JSON_VALUE(@parametros,'$.bascula2[0].activa')),'') as int)

		if exists (select * from basculas where bascula=@bascula)
			update basculas set tipoConexion=@basculaTipoConexion, [ip]=@IP, puerto=@puerto, sentencia=@sentencia, activa=@activa
			where bascula=@bascula
		else
			insert into basculas (bascula, tipoConexion, [ip], puerto, sentencia, activa)
			values (@bascula, @basculaTipoConexion, @IP, @puerto, @sentencia, @activa)


	--	BÁSCULA 03 =============================================================================================================
		set @bascula				= isnull((select JSON_VALUE(@parametros,'$.bascula3[0].bascula')),'')
		set @basculaTipoConexion	= isnull((select JSON_VALUE(@parametros,'$.bascula3[0].basculaTipoConexion')),'')
		set @IP						= isnull((select JSON_VALUE(@parametros,'$.bascula3[0].IP')),'')
		set @puerto					= isnull((select JSON_VALUE(@parametros,'$.bascula3[0].puerto')),'')
		set @sentencia				= isnull((select JSON_VALUE(@parametros,'$.bascula3[0].sentencia')),'')
		set @activa					= cast( isnull((select JSON_VALUE(@parametros,'$.bascula3[0].activa')),'') as int)

		if exists (select * from basculas where bascula=@bascula)
			update basculas set tipoConexion=@basculaTipoConexion, [ip]=@IP, puerto=@puerto, sentencia=@sentencia, activa=@activa
			where bascula=@bascula
		else
			insert into basculas (bascula, tipoConexion, [ip], puerto, sentencia, activa)
			values (@bascula, @basculaTipoConexion, @IP, @puerto, @sentencia, @activa)


	--	BÁSCULA 04 =============================================================================================================
		set @bascula				= isnull((select JSON_VALUE(@parametros,'$.bascula4[0].bascula')),'')
		set @basculaTipoConexion	= isnull((select JSON_VALUE(@parametros,'$.bascula4[0].basculaTipoConexion')),'')
		set @IP						= isnull((select JSON_VALUE(@parametros,'$.bascula4[0].IP')),'')
		set @puerto					= isnull((select JSON_VALUE(@parametros,'$.bascula4[0].puerto')),'')
		set @sentencia				= isnull((select JSON_VALUE(@parametros,'$.bascula4[0].sentencia')),'')
		set @activa					= cast( isnull((select JSON_VALUE(@parametros,'$.bascula4[0].activa')),'') as int)

		if exists (select * from basculas where bascula=@bascula)
			update basculas set tipoConexion=@basculaTipoConexion, [ip]=@IP, puerto=@puerto, sentencia=@sentencia, activa=@activa
			where bascula=@bascula
		else
			insert into basculas (bascula, tipoConexion, [ip], puerto, sentencia, activa)
			values (@bascula, @basculaTipoConexion, @IP, @puerto, @sentencia, @activa)
		

	--	========================================================================================================================
		select 'OK' as JAVASCRIPT       --	select * from basculas

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select 'Error! - '+@CatchError as JAVASCRIPT
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pBuscarGlobal]...';


GO
CREATE PROCEDURE [dbo].[pBuscarGlobal] @parametros varchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	-----------------------------------------------------------------------------------------------
	-- Variables de Configuración 
	-----------------------------------------------------------------------------------------------
	declare @EJERCICIO char(4)
	declare @GESTION char(6)
	declare @COMUN char(8)
	declare @LOTES char(8)
	select @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-----------------------------------------------------------------------------------------------

	BEGIN TRY

		declare @pedidoActivo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.pedidoActivo')),'')
			,	@numero varchar(50) = isnull((select JSON_VALUE(@parametros,'$.numero')),'')
			,	@serie varchar(50) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
			,	@ruta varchar(50) = isnull((select JSON_VALUE(@parametros,'$.ruta')),'')
			,	@zona varchar(50) = isnull((select JSON_VALUE(@parametros,'$.zona')),'')
			,	@articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
			,	@entrega varchar(50) = isnull((select JSON_VALUE(@parametros,'$.entrega')),'')
			,	@orden varchar(50) = isnull((select JSON_VALUE(@parametros,'$.orden')),'')
		declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			,	@rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')

		declare @laConsulta varchar(max)=''
		declare @fechaDeManyana varchar(10) = format(getdate()+1,'yyyy-MM-dd')
		declare @fechaHoyMas2 varchar(10) = format(getdate()+2,'yyyy-MM-dd')
		declare @laVista varchar(100) = 'd_Busca_PV'
		declare @elWhere varchar(max) = ' where 1=1 '
		declare @elOrderBy varchar(max) = ' order by a.entrega asc, a.numero asc '
		declare @elInnerJoin varchar(max) = ''
	
		-- eliminamos el registro de ['+@COMUN+'].[DBO].[en_uso] si procede
		EXEC('delete from ['+@COMUN+'].[DBO].[en_uso] where CLAVE='''+@pedidoActivo+''' and USUARIO='''+@usuario+'''')
		
	
		if @orden<>'' set @elOrderBy = ' order by '+@orden+' asc '
		if @numero<>'' set @elWhere = @elWhere+' and ltrim(rtrim(numero))=ltrim(rtrim('''+@numero+''')) '
		if @serie<>'' set @elWhere = @elWhere+' and upper(a.letra)=upper('''+@serie+''') '
		if @ruta<>'' set @elWhere = @elWhere+' and upper(a.ruta)=upper('''+@ruta+''') '
		if @zona<>'' BEGIN set @elWhere = @elWhere+' and upper(a.ZONA)=upper('''+@zona+''') ' set @laVista='"d_Busca_PV_Zona"' END
		if @entrega<>'' set @elWhere = @elWhere+' and cast(FORMAT(a.entrega,''yyyyMMddHHmmss'') as bigint) 
										<= cast(FORMAT(cast('''+@entrega+''' as datetime),''yyyyMMddHHmmss'') as bigint)  '
		if @articulo<>'' set @elInnerJoin = '
			inner join ['+@GESTION+'].[dbo].[d_pedive] b on CONCAT(b.EMPRESA,b.NUMERO,b.LETRA)=CONCAT(a.EMPRESA,a.NUMERO,a.LETRA) 
			and UPPER(b.DEFINICION) like ''%''+UPPER('''+@articulo+''')+''%'' 
		'
	
		set @laConsulta = 'select isnull(
							(select a.* from ' + @laVista + ' a ' + @elInnerJoin + @elWhere + @elOrderBy 
						+ ' FOR JSON AUTO, INCLUDE_NULL_VALUES)
						,''[]'') AS JAVASCRIPT'	

		exec (@laConsulta)
		return -1 
	END TRY
	BEGIN CATCH
		DECLARE @CatchError NVARCHAR(MAX)
		SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
		select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
		RAISERROR(@CatchError,12,1)
		RETURN 0
	END CATCH
END
GO
PRINT N'Creando Procedimiento [dbo].[pBuscarPedido]...';


GO
CREATE PROCEDURE [dbo].[pBuscarPedido] @parametros varchar(max)	
AS
SET NOCOUNT ON;
-----------------------------------------------------------------------------------------------
-- Variables de Configuración 
-----------------------------------------------------------------------------------------------
declare @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
select @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
-----------------------------------------------------------------------------------------------
-- Parámetros JSON 
declare @pedidoActivo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.pedidoActivo')),'')
	,	@pedido varchar(50) = isnull((select JSON_VALUE(@parametros,'$.pedido')),'')
	,	@serie varchar(10) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
	,	@ruta varchar(10) = isnull((select JSON_VALUE(@parametros,'$.ruta')),'')			
	,	@nombreRuta varchar(100) = isnull((select JSON_VALUE(@parametros,'$.nombreRuta')),'')			
	,	@zona varchar(10) = isnull((select JSON_VALUE(@parametros,'$.zona')),'')			
declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
	,	@rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
declare @laClave varchar(50) = (select CONCAT(Ejercicio,EMPRESA) from Configuracion) + @pedido + @serie
declare @nu varchar(50) = 
replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 
-----------------------------------------------------------------------------------------------
-- Variables
declare @laConsulta varchar(max)=''
	,	@elCodigo varchar(50) = ''
	,	@elNombre varchar(100) = ''
	,	@laDireccion varchar(255) = ''
	,	@laRuta varchar(10) = ''
	,	@elVendedor varchar(50) = ''
	,	@elID varchar(50) = ''
	,	@cliObs varchar(1000) = ''
	,	@pedidoObs varchar(1000) = ''
	,	@cnt int
-----------------------------------------------------------------------------------------------

BEGIN TRY
	while LEN(@pedido)<10 set @pedido = ' ' + @pedido

	-- quitamos en_uso del usuario
	EXEC('delete ['+@COMUN+'].[DBO].[en_uso] where USUARIO='''+@usuario+'''')

	-- detectar si está en uso por otro usuario
	declare @tbRes TABLE (res int)
	delete @tbRes insert  @tbRes EXEC('select count(USUARIO) from ['+@COMUN+'].[DBO].[en_uso] where CLAVE='''+@pedidoActivo+''' and USUARIO!='''+@usuario+'''')
	if (select res from @tbRes)>0 BEGIN
		declare @tbUsuario TABLE (USUARIO varchar(50))
		delete @tbUsuario insert  @tbUsuario EXEC('select top 1 USUARIO from ['+@COMUN+'].[DBO].[en_uso] where CLAVE='''+@pedidoActivo+''' and USUARIO!='''+@usuario+'''')
		select 'enUSO!'+(select USUARIO from @tbUsuario) as JAVASCRIPT 
		return -1 
	END

	-- ponemos EN_USO EN EUROWIN 
	EXEC('
			begin try
			insert into ['+@COMUN+'].[DBO].[en_uso] (TIPO,CLAVE,TS,USUARIO,TERMINAL,VISTA,GUID_ID) 
			values (''PEDIVEN'','''+@laClave+''','''+@nu+''','''+@usuario+''',''WEB'',1,'''+@nu+''')
			end try begin catch end catch
	')

	declare @buscarSerie varchar(1000) = ' and upper(letra)=upper('''+@serie+''') '
	if @serie='x' or @serie='' set @buscarSerie=''
	declare @buscaRuta varchar(1000) = ' and upper(ruta)=upper('''+@ruta+''') '
	if @ruta='x' or @ruta='' set @buscaRuta=''
	declare @buscaZona varchar(1000) = ''
	if @zona<>'' set @buscaZona=' and upper(zona)=upper('''+@zona+''') '

	declare @elOrdeBy varchar(1000) = ' ORDER BY servidas ASC, linia ASC '
	if @serie='ET'  set @elOrdeBy = ' ORDER BY uds_traspasadas ASC, zona ASC, Ubicacion ASC, linia ASC '

	set @laConsulta = 'select isnull(
						(
							select a.* from 
							(
								select '''+@laClave+''' as PedidoActivo, p.* , ser.*
								from Detalle_Pedidos p
								left join [ConfigSeries] ser on objeto=''ResLin'' and serie='''+@serie+''' 
								where NUMERO='''+@pedido+''' '+@buscarSerie+' '+@buscaRuta+' '+@buscaZona+'						
							) a 
							'+@elOrdeBy+'
							FOR JSON AUTO, INCLUDE_NULL_VALUES
						)
					,''[]'') AS JAVASCRIPT'	

	exec (@laConsulta)
		
	return -1 
END TRY
BEGIN CATCH
	DECLARE @Message varchar(MAX) = ERROR_MESSAGE(), @Severity int = ERROR_SEVERITY(), @State smallint = ERROR_STATE() 
	RAISERROR (@Message, @Severity, @State)
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pCalculosPV]...';


GO
CREATE PROCEDURE [dbo].[pCalculosPV] (@parametros varchar(max))
AS
SET NOCOUNT ON
BEGIN TRY	
	BEGIN TRANSACTION
	-- Parámetros JSON
	declare @modo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
		,	@NUMERO varchar(20) = isnull((select JSON_VALUE(@parametros,'$.numero')),'')
		,	@LINEA varchar(20) = isnull((select JSON_VALUE(@parametros,'$.linea')),'')
		,	@LETRA char(2) = isnull((select JSON_VALUE(@parametros,'$.letra')),'')
		,	@ARTICULO varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
		,	@UNIDADES numeric(15,6) = cast(isnull((select JSON_VALUE(@parametros,'$.uds')),'0.00') as numeric(15,6))
		,	@PESO varchar(20) = isnull((select JSON_VALUE(@parametros,'$.peso')),'0.00')
		,	@TARIFA varchar(10) = isnull((select JSON_VALUE(@parametros,'$.tarifa')),'')

	-- variables
	declare @Sentencia varchar(max) = ''
		,	@TOTALDOC numeric(15,6)
		,	@PVERDECLI bit
		,	@GESTION varchar(10)
		,	@CLIENTE varchar(20)
		,	@empresa char(2)

	select @GESTION=Gestion, @empresa=Empresa from Configuracion
	select top 1 @CLIENTE=cliente from Detalle_Pedidos where concat(empresa,numero,letra)=concat(@empresa,@NUMERO,@LETRA) 
	
	-- Calcular Precios líneales
	set @Sentencia = '
	declare @TOTALDOC numeric(15,6), @PVERDECLI bit

	UPDATE ['+@GESTION+'].DBO.d_pedive SET
		PRECIO = CASE WHEN C.IVA_INC=0
				THEN coalesce((SELECT PVP FROM ftDonamPreu(C.EMPRESA,C.CLIENTE, D.ARTICULO, CLI.TARIFA, D.UNIDADES, GETDATE())),0.00)
				ELSE coalesce((SELECT PVP FROM ftDonamPreu(C.EMPRESA,C.CLIENTE, D.ARTICULO, CLI.TARIFA, D.UNIDADES, GETDATE())),0.00)/(1+(IVA.IVA/100))
				END,
		PRECIOIVA = CASE WHEN C.IVA_INC=0
				THEN coalesce((SELECT PVP FROM ftDonamPreu(C.EMPRESA,C.CLIENTE, D.ARTICULO, CLI.TARIFA, D.UNIDADES, GETDATE())),0.00)*(1+(IVA.IVA/100))
				ELSE coalesce((SELECT PVP FROM ftDonamPreu(C.EMPRESA,C.CLIENTE, D.ARTICULO, CLI.TARIFA, D.UNIDADES, GETDATE())),0.00)
				END,
		PRECIODIV = CASE WHEN C.IVA_INC=0
				THEN coalesce((SELECT PVP FROM ftDonamPreu(C.EMPRESA,C.CLIENTE, D.ARTICULO, CLI.TARIFA, D.UNIDADES, GETDATE())),0.00)
				ELSE coalesce((SELECT PVP FROM ftDonamPreu(C.EMPRESA,C.CLIENTE, D.ARTICULO, CLI.TARIFA, D.UNIDADES, GETDATE())),0.00)/(1+(IVA.IVA/100))
				END,
		PREDIVIVA = CASE WHEN C.IVA_INC=0
				THEN coalesce((SELECT PVP FROM ftDonamPreu(C.EMPRESA,C.CLIENTE, D.ARTICULO, CLI.TARIFA, D.UNIDADES, GETDATE())),0.00)*(1+(IVA.IVA/100))
				ELSE coalesce((SELECT PVP FROM ftDonamPreu(C.EMPRESA,C.CLIENTE, D.ARTICULO, CLI.TARIFA, D.UNIDADES, GETDATE())),0.00)
				END,
		DTO1 = coalesce((SELECT DTO1 FROM ftDonamPreu(C.EMPRESA,C.CLIENTE, D.ARTICULO, CLI.TARIFA, D.UNIDADES, GETDATE())),0.00),
		DTO2 = coalesce((SELECT DTO2 FROM ftDonamPreu(C.EMPRESA,C.CLIENTE, D.ARTICULO, CLI.TARIFA, D.UNIDADES, GETDATE())),0.00)
	FROM ['+@GESTION+'].DBO.C_PEDIVE C
	INNER JOIN ['+@GESTION+'].DBO.d_pedive D ON CONCAT(D.EMPRESA, LTRIM(RTRIM(D.NUMERO)), D.LETRA)
										= CONCAT(C.EMPRESA, LTRIM(RTRIM(C.NUMERO)), C.LETRA)
	INNER JOIN ['+@GESTION+'].DBO.CLIENTES CLI ON CLI.CODIGO=C.CLIENTE
	INNER JOIN ['+@GESTION+'].DBO.tipo_iva IVA ON IVA.CODIGO=D.TIPO_IVA
	WHERE C.EMPRESA='''+@empresa+''' AND LTRIM(RTRIM(C.NUMERO))=LTRIM(RTRIM('''+@NUMERO+''')) AND C.LETRA='''+@LETRA+''' AND D.LINIA='+@LINEA+'

	-- Calcular Importes lineales					
	UPDATE ['+@GESTION+'].DBO.d_pedive SET
			IMPORTE=ROUND((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIO ELSE PESO*PRECIO END)
					-((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIO ELSE PESO*PRECIO END)*(DTO1/100))
					-(((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIO ELSE PESO*PRECIO END)
					-(CASE WHEN PESO=0.00 THEN UNIDADES*PRECIO ELSE PESO*PRECIO END)*(DTO1/100))*(DTO2/100)),2),

			IMPORTEIVA=ROUND((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIOIVA ELSE PESO*PRECIOIVA END)
					-((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIOIVA ELSE PESO*PRECIOIVA END)*(DTO1/100))
					-(((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIOIVA ELSE PESO*PRECIOIVA END)
					-(CASE WHEN PESO=0.00 THEN UNIDADES*PRECIOIVA ELSE PESO*PRECIOIVA END)*(DTO1/100))*(DTO2/100)),2),

			IMPORTEDIV=ROUND((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIODIV ELSE PESO*PRECIODIV END)
					-((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIODIV ELSE PESO*PRECIODIV END)*(DTO1/100))
					-(((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIODIV ELSE PESO*PRECIODIV END)
					-(CASE WHEN PESO=0.00 THEN UNIDADES*PRECIODIV ELSE PESO*PRECIODIV END)*(DTO1/100))*(DTO2/100)),2),

			IMPDIVIVA=ROUND((CASE WHEN PESO=0.00 THEN UNIDADES*PREDIVIVA ELSE PESO*PREDIVIVA END)
					-((CASE WHEN PESO=0.00 THEN UNIDADES*PREDIVIVA ELSE PESO*PREDIVIVA END)*(DTO1/100))
					-(((CASE WHEN PESO=0.00 THEN UNIDADES*PREDIVIVA ELSE PESO*PREDIVIVA END)
					-(CASE WHEN PESO=0.00 THEN UNIDADES*PREDIVIVA ELSE PESO*PREDIVIVA END)*(DTO1/100))*(DTO2/100)),2)
	WHERE EMPRESA='''+@empresa+''' AND LTRIM(RTRIM(NUMERO))=LTRIM(RTRIM('''+@NUMERO+''')) AND LETRA='''+@LETRA+''' 
	
	-- ACTUALIZAR PVERDE
	select @pverdecli=PVERDE from ['+@GESTION+'].dbo.clientes WHERE CODIGO='''+@CLIENTE+'''
	IF @PVERDECLI=0
			UPDATE ['+@GESTION+'].DBO.D_PEDIVE SET PVERDE=ROUND(CASE WHEN D.PESO=0.00 THEN D.UNIDADES*A.P_IMPORTE ELSE D.PESO*A.P_IMPORTE END,2)
			FROM ['+@GESTION+'].DBO.D_PEDIVE D INNER JOIN ['+@GESTION+'].DBO.ARTICULO A ON A.CODIGO=D.ARTICULO AND A.PVERDE=1
			WHERE EMPRESA='''+@empresa+'''AND LTRIM(RTRIM(NUMERO))=LTRIM(RTRIM('''+@NUMERO+''')) AND LETRA='''+@LETRA+''' AND LINIA='+@LINEA+'

	-- ACTUALIZAR IMPORTES C_PEDIVE
	UPDATE ['+@GESTION+'].DBO.C_PEDIVE SET TOTALPED=D.IMPORTE, IMPDIVISA=D.IMPDIVISA, PESO=D.PESO, LITROS=D.LITROS
	FROM ['+@GESTION+'].DBO.C_PEDIVE C
	INNER JOIN (SELECT MAX(D.EMPRESA) AS EMPRESA, MAX(D.NUMERO) AS NUMERO, MAX(D.LETRA) AS LETRA, SUM(D.IMPORTE)+SUM(D.PVERDE) AS IMPORTE,
	SUM(D.IMPORTEDIV)+SUM(D.PVERDE) AS IMPDIVISA, SUM(D.IMPORTEIVA) AS IMPORTEIVA, SUM(D.PVERDE) AS PVERDE,
	SUM(CASE WHEN D.PESO!=0.00 THEN D.UNIDADES*D.COSTE ELSE D.PESO*D.UNIDADES END) AS COSTE, SUM(D.PESO) AS PESO,
	SUM(D.UNIDADES*(CAST(CASE WHEN D.PESO=0.00 THEN CASE WHEN LTRIM(RTRIM(A.LITROS))='''' THEN ''0.00'' ELSE A.LITROS END ELSE ''0.00'' END AS DECIMAL(10,2)))) AS LITROS
	FROM ['+@GESTION+'].DBO.D_PEDIVE D INNER JOIN ['+@GESTION+'].DBO.ARTICULO A ON A.CODIGO=D.ARTICULO
	WHERE D.EMPRESA='''+@empresa+''' AND LTRIM(RTRIM(D.NUMERO))=LTRIM(RTRIM('''+@NUMERO+''')) AND D.LETRA='''+@LETRA+''') D ON D.EMPRESA+D.NUMERO+D.LETRA=C.EMPRESA+C.NUMERO+C.LETRA
	WHERE C.EMPRESA='''+@empresa+''' AND LTRIM(RTRIM(C.NUMERO))=LTRIM(RTRIM('''+@NUMERO+''')) AND C.LETRA='''+@LETRA+'''  

	-- Calcular TOTALDOC
	SELECT @TOTALDOC = coalesce(CONVERT(DECIMAL(16,2),sum(totimpiva)),0.00) from (SELECT sum( (D.IMPORTE+D.PVERDE)-((D.IMPORTE+D.PVERDE)*(C.PRONTO/100)) ) +
	round( sum(( (D.IMPORTE+D.PVERDE)-((D.IMPORTE+D.PVERDE)*(C.PRONTO/100)) )*(COALESCE(IVA.IVA,0.00)/100)),2) +
	round( sum(( (D.IMPORTE+D.PVERDE)-((D.IMPORTE+D.PVERDE)*(C.PRONTO/100)) )*(CASE WHEN CLI.RECARGO=1 THEN COALESCE(IVA.RECARG,0.00)/100 ELSE 0 END)),2) as totimpiva, IVA.iva
	FROM ['+@GESTION+'].DBO.D_PEDIVE D JOIN ['+@GESTION+'].DBO.C_PEDIVE C ON D.EMPRESA=C.EMPRESA AND D.LETRA=C.LETRA AND LTRIM(RTRIM(D.NUMERO))=LTRIM(RTRIM(C.NUMERO))
	LEFT JOIN ['+@GESTION+'].DBO.TIPO_IVA IVA ON IVA.CODIGO=D.TIPO_IVA LEFT JOIN ['+@GESTION+'].DBO.CLIENTES CLI ON CLI.CODIGO=C.CLIENTE
	WHERE D.EMPRESA='''+@empresa+''' AND LTRIM(RTRIM(D.NUMERO))=LTRIM(RTRIM('''+@NUMERO+''')) AND D.LETRA='''+@LETRA+'''  group by c.empresa, c.numero, c.letra, IVA.iva) a 

	UPDATE ['+@GESTION+'].DBO.C_PEDIVE SET TOTALDOC=@TOTALDOC 
	WHERE EMPRESA='''+@empresa+''' AND LTRIM(RTRIM(NUMERO))=LTRIM(RTRIM('''+@NUMERO+''')) AND LETRA='''+@LETRA+'''
	'

	/**/ -- print @Sentencia return -1
	exec (@Sentencia)

	COMMIT TRANSACTION
	RETURN -1
END TRY
BEGIN CATCH	
	ROLLBACK TRANSACTION
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
	select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pCambiarArticulos]...';


GO
CREATE PROCEDURE [dbo].[pCambiarArticulos] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON

	-- datos JSON ----------------------------------------------------------------------------------
	declare	  @modo varchar(50)		= isnull((select JSON_VALUE(@parametros,'$.modo')),'')
			, @pedido varchar(50)	= isnull((select JSON_VALUE(@parametros,'$.pedido')),'')
			, @serie varchar(10)	= isnull((select JSON_VALUE(@parametros,'$.serie')),'')
			, @articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')

	declare @usuario varchar(50)	= isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50)		= isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-------------------------------------------------------------------------------------------------

	declare @empresa char(2) = (select Empresa from Configuracion)
		,	@sql varchar(max) 
	

	set @sql = (
		SELECT articulo, definicion, cajas_pv, uds_pv, peso_pv, linia 
		FROM [dbo].[Detalle_Pedidos] 
		WHERE empresa=@empresa and letra=@serie and numero=@pedido AND UDS_TRASPASADAS=0 AND ARTICULO!=''
		order by linia asc 
		for JSON AUTO, INCLUDE_NULL_VALUES
	)


	if @modo='equivalencia' BEGIN
		set @sql = (SELECT ARTICULO, NOMBRE, stock FROM Articulos_Sustitucion WHERE art_orig=@articulo for JSON AUTO, INCLUDE_NULL_VALUES)
	END

	select isnull((@sql),'[]') as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pCancelarPreparacion]...';


GO
CREATE PROCEDURE [dbo].[pCancelarPreparacion] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON

	-- datos JSON ----------------------------------------------------------------------------------
	declare	  @pedido varchar(50) = isnull((select JSON_VALUE(@parametros,'$.pedido')),'')
			, @serie varchar(10) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
			, @linea int = cast(isnull((select JSON_VALUE(@parametros,'$.linea')),'0') as int)
			, @almacen char(2) = isnull((select JSON_VALUE(@parametros,'$.almacen')),'')
	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-------------------------------------------------------------------------------------------------
	-- variables App
	declare @laSesion varchar(50) = @usuario
	declare @nu varchar(50) = 
	replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 
	declare @empresa char(2) = (select Empresa from Configuracion)
	---------------------------------------------------------------------------------------------------------------
	
	EXEC ('delete FROM [Traspaso_Temporal] where empresa='''+@empresa+''' and numero='''+@pedido+''' and letra='''+@serie+''' ')

	RETURN -1
END TRY

BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pComprobarCB]...';


GO
CREATE PROCEDURE pComprobarCB (@parametros varchar(max))
AS
/*-----------------------------------------------------------------------------------
--#AUTHOR: JUAN CARLOS VILLALOBOS
--#NAME: pComprobarCB
--#CREATION: 06/05/2021
--#DESCRIPTION: Comprueba que el codigo de barras exista y manda los datos
--#PARAMETERS:	@codigoBarras
--#CHANGES
-- 06/05/2021 - JUAN CARLOS VILLALOBOS Creación del procedimiento
-- 14/06/2021 - JUAN CARLOS VILLALOBOS Adaptacion al nuevo script
-- 07/10/2021 - Elías Jené - Si existe el identificador 37 obtenemos las unidades de los siguientes 8 dígitos
--						   - Tomamos 13 dígitos en lugar de 14 para encontrar el artículo
-- 14/12/2021 - Albert Rodríguez: Ajustar retorno datos.
-------------------------------------------------------------------------------------*/

/*
ARTICULO	CODIGO BARRAS										EAN					LOTE			PESO
MI16104		010040000007300210PI21000402						00400000073002		PI21000402	
MI16105		01984370019350041026007215220726					98437001935004		260072	
MI-24302	01184252140010603103000700152101151020392124		18425214001060		20392124		000700
MILP		0198411477904619152101213103000615103910			98411477904619		3910			000615
MILCP		01984370036042043102001062152105181019056001		98437003604204		19056001		001062
			01004000000108543103004800370000001010CA21000925	(Identificador 37 - los 8 siguientes dígitos son las unidades)
ET1085		0100400000010854310300480037000000101020211220430954
MIL			0178414620091583172202221020211220430955
MI-24302	01184252140010603103003000152101151020211220430956

[pComprobarCB] '{"sp":"pComprobarCB","CODBAR":"01184252140010603103003000152101151020211220430956"}'
*/

-----------------------------------------------------------------------------------------------
--									Parámetros JSON 
-----------------------------------------------------------------------------------------------
declare @CODBAR VARCHAR(255) = isnull((select JSON_VALUE(@parametros,'$.CODBAR')),'')  
-----------------------------------------------------------------------------------------------
-- Variables de Configuración 
-----------------------------------------------------------------------------------------------
declare @EMPRESA char(2), @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
select  @EMPRESA=Empresa, @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
-----------------------------------------------------------------------------------------------

/* Declaramos las variables necesarias para el procedimiento*/
DECLARE
	@ARTICULO CHAR(50),
	@BARRAS CHAR(50),
	@UDS DECIMAL(16,4),
	@CAJAS DECIMAL(16,4),
	@PESO DECIMAL(16,4)=0.0000,
	@PESOBAR CHAR(6),
	@CADUCIDAD CHAR(8),
	@LOTE CHAR(50),
	@EXISTE BIT,
	@ARTLOT BIT, 
	@TIPOEAN CHAR(5), 
	@FALTACOD BIT,
	@UNICAJA numeric(15,6),
	@VENTA_CAJA smallint,
	@DECIMALES INT,
	@MISSATGE VARCHAR(255),
	@sql varchar(max),
	@respuesta varchar(max)
BEGIN TRY
	-- Variables TABLA
	declare @tbBarras TABLE (ARTICULO varchar(20), UNIDADES numeric(15,6))
	declare @tbLote TABLE (LOTE bit)
	declare @tbArticulo TABLE (UNICAJA int)

	-- Comprobar si el codigo de barras leido ya esta relacionado con algún artículo. 
	-- Esto se realiza por si el código leido es un EAN13, si es un GS1 nunca lo encontraremos.
	declare @registros int=0
	declare @tbReg TABLE (registros int)
	INSERT @tbReg EXEC('SELECT COUNT(ARTICULO) FROM ['+@GESTION+'].[DBO].BARRAS WHERE LEFT(BARRAS,13) = '''+@CODBAR+'''')	
	SELECT @registros=registros FROM @tbReg		
	delete @tbReg
	IF @registros>0	BEGIN
		SET @BARRAS = @CODBAR
		-- Si existe el codigo de barras, obtenemos el código de artículo y las unidades			
		INSERT @tbBarras EXEC('SELECT ARTICULO, UNIDADES FROM ['+@GESTION+'].DBO.BARRAS WHERE LEFT(BARRAS,13) = '''+@BARRAS+'''')	
		SELECT @ARTICULO=ARTICULO, @UDS=UNIDADES FROM @tbBarras		
		delete @tbBarras
		-- obtener LOTE			
		INSERT @tbLote EXEC('SELECT LOTE FROM ['+@LOTES+'].DBO.artlot WHERE ARTICULO='''+@ARTICULO+'''')	
		SELECT @ARTLOT=LOTE FROM @tbLote		
		delete @tbLote
		-- obtener UNICAJA del artículo			
		INSERT @tbArticulo EXEC('SELECT isnull(UNICAJA,0) FROM ['+@GESTION+'].DBO.ARTICULO WHERE CODIGO='''+@ARTICULO+'''')	
		SELECT @UNICAJA=UNICAJA FROM @tbArticulo		
		delete @tbArticulo

		SET @PESO=0.0000
		SET @CADUCIDAD=''
		SET @LOTE=''
		SET @EXISTE=1
		SET @TIPOEAN = 'EAN13'
		set @FALTACOD = 0
		SET @DECIMALES = 0
				
		-- Mensaje de retorno
		set @respuesta = CONCAT('{"Error":"","EXISTE":"',@EXISTE,'","ARTLOT":"',@ARTLOT,'","ARTICULO":"',ltrim(rtrim(@ARTICULO)),'","UNIDADES":"',@Uds,'"'
						,',"UNICAJA":"',@UNICAJA,'","PESO":"',@PESO,'","CADUCIDAD":"',@CADUCIDAD,'","LOTE":"',ltrim(rtrim(@LOTE)),'"'
						,',"TIPOEAN":"',@TIPOEAN,'","FALTACOD":"',@FALTACOD,'","EAN":"',@CODBAR,'"}')
	END
	-- Si no encontramos el código, procedemos a desgranarlo para ver si es un código GS1
	ELSE BEGIN   
		-- Revisamos si los dos primeros dígitos del codigo leido es 01, si es así sabemos que los siguientes 14 dígitos  
		-- son el código de barras del artículo, el cual buscaremos también en la tabla correspondiente.
		IF SUBSTRING(@CODBAR,1,2) = '01' BEGIN
			-- comprobar si el código de barras está dividido. Si lo está pedimos la segunda lectura.
			IF LEN(@CODBAR)<27 BEGIN set @respuesta = '{"Error":"CodigoDividido"}' return -1 END

			SET @BARRAS = SUBSTRING(@CODBAR,3,13)
			INSERT @tbReg EXEC('SELECT COUNT(ARTICULO) FROM ['+@GESTION+'].[DBO].BARRAS WHERE LEFT(BARRAS,13) = '''+@BARRAS+'''')	
			SELECT @registros=registros FROM @tbReg		
			delete @tbReg
			IF @registros>0 BEGIN
				-- Si existe el codigo de barras, obtenemos el código de artículo y las unidades y continuamos 
				INSERT @tbBarras EXEC('SELECT ARTICULO, UNIDADES FROM ['+@GESTION+'].DBO.BARRAS WHERE LEFT(BARRAS,13) = '''+@BARRAS+'''')	
				SELECT @ARTICULO=ARTICULO, @UDS=UNIDADES FROM @tbBarras		
				delete @tbBarras
				-- obtener LOTE
				INSERT @tbLote EXEC('SELECT LOTE FROM ['+@LOTES+'].DBO.artlot WHERE ARTICULO='''+@ARTICULO+'''')	
				SELECT @ARTLOT=LOTE FROM @tbLote		
				delete @tbLote
				-- obtener UNICAJA del artículo
				INSERT @tbArticulo EXEC('SELECT isnull(UNICAJA,0) FROM ['+@GESTION+'].DBO.ARTICULO WHERE CODIGO='''+@ARTICULO+'''')	
				SELECT @UNICAJA=UNICAJA FROM @tbArticulo		
				delete @tbArticulo

				SET @EXISTE=1
				SET @TIPOEAN = 'GS1'
				SET @FALTACOD = 0
				-- Obtenemos el valor de las unidades por caja y del peso por unidad para devolver el valor correspondiente 
				-- según las unidades del código de barras
				declare @tbArt2 TABLE (CAJAS numeric(15,6), PESO varchar(20))
				INSERT @tbArt2 EXEC('SELECT 
										CASE WHEN UNICAJA=0 THEN 0 ELSE CONVERT(INT,1/UNICAJA) END as CAJAS
									, CASE WHEN ltrim(rtrim(PESO))='' THEN ''0.00'' ELSE REPLACE(PESO,'','',''.'') END * @UDS as PESO
									FROM ['+@GESTION+'].DBO.ARTICULO WHERE CODIGO='''+@ARTICULO+'''')	
				SELECT @CAJAS=CAJAS, @PESO=PESO FROM @tbArt2		
				delete @tbArt2

				-- Desgranamos la siguiente parte del código GS1
				-- Si el siguiente tramos del código de barras es 310, obtendremos el peso								
				IF SUBSTRING(@CODBAR,17,3) = '310' BEGIN
					SET @PESOBAR = SUBSTRING(@CODBAR,21,6)
					SET @DECIMALES = SUBSTRING(@CODBAR,20,1)
					-- Desgranamos la siguiente parte del código GS1
					-- Si el siguiente tramos del código de barras es 15, obtendremos la caducidad
					IF SUBSTRING(@CODBAR,27,2) = '15' OR SUBSTRING(@CODBAR,27,2) = '17' BEGIN
						SET @CADUCIDAD = SUBSTRING(@CODBAR,29,6)
						-- Desgranamos la siguiente parte del código GS1
						-- Si el siguiente tramos del código de barras es 10, obtendremos el lote
						IF SUBSTRING(@CODBAR,35,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,37,50)
					END
					-- Si el siguiente tramos del código de barras es 37, obtendremos las unidades de la caja, las cuales substituiran las unidades del ERP				
					IF SUBSTRING(@CODBAR,27,2) = '37' BEGIN
						set @Uds=cast(SUBSTRING(@CODBAR,29,8) as decimal(16,4))
						-- Si el siguiente tramos del código de barras es 10, obtendremos el lote
						IF SUBSTRING(@CODBAR,37,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,39,50)
					END	
					-- Si el siguiente tramos del código de barras es 10, obtendremos el lote
					IF SUBSTRING(@CODBAR,27,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,29,50)
					-- Si el siguiente tramo está vacío, según los ejemplos que tenemos, quiere decir que el código 
					-- está dividido en 2 y hay que leer la segunda parte.
					-- Cuando esto pasa, el procedimiento devuelve un aviso para que la aplicaci�n leea el resto 
					-- del código y vuelva a enviarlo uniendo las dos lecturas
					IF SUBSTRING(@CODBAR,27,2) = ' ' SET @FALTACOD = 1
				END			
				-- Si el siguiente tramo del código de barras es 15 � 17, obtendremos la caducidad
				IF SUBSTRING(@CODBAR,17,2) = '15' OR SUBSTRING(@CODBAR,17,2) = '17' BEGIN
					SET @CADUCIDAD = SUBSTRING(@CODBAR,19,6)
					-- Desgranamos la siguiente parte del código GS1
					-- Si el siguiente tramos del código de barras es 310, obtendremos el peso
					IF SUBSTRING(@CODBAR,25,3) = '310' BEGIN
						SET @PESOBAR = SUBSTRING(@CODBAR,29,6)
						SET @DECIMALES = SUBSTRING(@CODBAR,28,1)
						-- Desgranamos la siguiente parte del código GS1
						-- Si el siguiente tramos del código de barras es 10, obtendremos el lote
						IF SUBSTRING(@CODBAR,35,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,37,50)
					END
					-- Si el siguiente tramos del código de barras es 10, obtendremos el lote
					IF SUBSTRING(@CODBAR,25,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,27,50)
					-- Si el siguiente tramo está vacío, según los ejemplos que tenemos, quiere decir que el código 
					-- está dividido en 2 y hay que leer la segunda parte.
					-- Cuando esto pasa, el procedimiento devuelve un aviso para que la aplicaci�n leea el resto 
					-- del código y vuelva a enviarlo uniendo las dos lecturas
					IF SUBSTRING(@CODBAR,25,2) = ' '	
						BEGIN
							SET @FALTACOD = 1
						END
				END
				-- Si el siguiente tramos del código de barras es 10, obtendremos el lote.
				IF SUBSTRING(@CODBAR,17,2) = '10' SET @LOTE = SUBSTRING(@CODBAR,20,50)

				IF @DECIMALES = 1 SET @DECIMALES = 10								
				IF @DECIMALES = 2 SET @DECIMALES = 100								
				IF @DECIMALES = 3 SET @DECIMALES = 1000							
				IF @PESOBAR!='000000' SET @PESO=CONVERT(DECIMAL(16,4),@PESOBAR)/@DECIMALES

				set @respuesta = CONCAT('{"Error":"","EXISTE":"',@EXISTE,'","ARTLOT":"',@ARTLOT,'","ARTICULO":"',ltrim(rtrim(@ARTICULO)),'","UNIDADES":"',@Uds,'"'
								,',"UNICAJA":"',@UNICAJA,'","PESO":"',@PESO,'","CADUCIDAD":"',@CADUCIDAD,'","LOTE":"',ltrim(rtrim(@LOTE)),'"'
								,',"TIPOEAN":"',@TIPOEAN,'","FALTACOD":"',@FALTACOD,'","EAN":"',@CODBAR,'"}')
			END ELSE BEGIN
				-- Si el código de barras no esta asociado a ningún artículo no continuamos
				SET @MISSATGE='ERROR001 - El Código de Barras no está asociado a ningún artículo!'
				set @respuesta = '{"Error":"'+@MISSATGE+'"}'
			END						
		END
		-- Si hemos llegado aquí y el código no empieza por 01, no hacemos más comprobaciones. 
		-- Damos por sentado que el código no está asociado a niengun artículo del ERP. 
		-- Realmente hay más posibilidades pero de momento no las tendremos en cuenta.
		ELSE BEGIN
			SET @MISSATGE='ERROR001 - El Código de Barras no está asociado a ningún artículo!'
			set @respuesta = '{"Error":"'+@MISSATGE+'"}'
		END								
	END 
	SELECT @respuesta AS JAVASCRIPT
	RETURN -1
END TRY
BEGIN CATCH
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError=CONCAT(ERROR_MESSAGE(),ERROR_NUMBER(),ERROR_PROCEDURE(),@@PROCID ,ERROR_LINE())
	RAISERROR(@CatchError,12,1) 
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pComunes]...';


GO
CREATE PROCEDURE [dbo].[pComunes] @parametros varchar(max)='[]'
AS
BEGIN TRY	
	declare @modo varchar(50) = (select JSON_VALUE(@parametros,'$.modo'))

	Declare   @nombre varchar(10)
			, @comunes varchar(max)=''

	Declare elCursor CURSOR for
	SELECT name FROM sys.databases WHERE left(name,4) = 'COMU'
	OPEN elCursor FETCH NEXT FROM elCursor INTO @nombre
	WHILE (@@FETCH_STATUS=0)BEGIN
		set @comunes = @comunes + '{"nombre":"'+@nombre+'"}'

		declare @exec1 varchar(1000) = '
			declare @comunes varchar(max)=''''
			if (select left(PRODUCTNAME,4) from ['+@nombre+'].dbo.codcom)=''SAGE''
			set @comunes = @comunes + ''{"nombre":"'+@nombre+'"}''
		'
		exec (@exec1)

	FETCH NEXT FROM elCursor INTO @nombre END Close elCursor Deallocate elCursor
	
	select '['+replace(isnull(@comunes,'[]'),'}{','},{')+']'  as JAVASCRIPT   

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select @CatchError as JAVASCRIPT
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pConfiguracion]...';


GO
CREATE PROCEDURE [dbo].[pConfiguracion] @parametros varchar(max)='[]', @respuestaSP varchar(max)='' output
AS
BEGIN TRY	
	declare @modo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
		,	@comun varchar(50) = isnull((select JSON_VALUE(@parametros,'$.comun')),'')
		,	@empresa varchar(50) = isnull((select JSON_VALUE(@parametros,'$.empresa')),'')
		,	@nombreEmpresa varchar(50) = isnull((select JSON_VALUE(@parametros,'$.nombreEmpresa')),'')
		,	@objeto varchar(50) = isnull((select JSON_VALUE(@parametros,'$.objeto')),'')
		,	@valor varchar(50) = isnull((select JSON_VALUE(@parametros,'$.valor')),'')
		,	@confOpIO varchar(50) = isnull((select JSON_VALUE(@parametros,'$.confOpIO')),'')
		,	@cliente varchar(50) = isnull((select JSON_VALUE(@parametros,'$.cliente')),'')

	declare   @ejercicio varchar(4)
			, @LETRA varchar(2)
			, @GESTION varchar(6)
			, @CAMPOS varchar(8)
			, @LOTES varchar(8)
			, @SentenciaSQL varchar(max)

	declare @sp int


	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
		if @modo='cargarConfiguracion' BEGIN			
			declare @configuracion varchar(max) = isnull( (select * from ConfigApp for JSON AUTO, INCLUDE_NULL_VALUES) ,'[]')
			declare @basculas varchar(max) = isnull( (select * from basculas for JSON AUTO, INCLUDE_NULL_VALUES) ,'[]')

			select CONCAT('{"configuracion":',@configuracion,',"basculas":',@basculas,'}') as JAVASCRIPT
			return -1
		END

	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
		if @modo='comprobarConfiguracionEmpresa' BEGIN
			if (select isnull(Empresa,'') from Configuracion)<>'' 
				select (select '' as msj, * from Configuracion for JSON AUTO, INCLUDE_NULL_VALUES) as JAVASCRIPT 
			else select '[{"msj":"NoExisteConfiguracion"}]' as JAVASCRIPT
			return -1
		END


	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
		if @modo='configurarEmpresa' BEGIN  
			-- obtener BD CAMPOSxx
			BEGIN TRY
				IF OBJECT_ID('tempdb..##tmpNomConex') IS NOT NULL BEGIN drop table ##tmpNomConex END
				set @SentenciaSQL = 'select NOM_CONEX into ##tmpNomConex from ['+@comun+'] .dbo.modulos where nombre=''CAMPOSAD'''
				EXEC (@SentenciaSQL)
				set @CAMPOS = (select NOM_CONEX from ##tmpNomConex)
				drop table ##tmpNomConex
			END TRY BEGIN CATCH END CATCH
		
			-- obtener BD LOTESxxx
			BEGIN TRY
				IF OBJECT_ID('tempdb..##tmpNomConex') IS NOT NULL BEGIN drop table ##tmpNomConex END
				set @SentenciaSQL = 'select NOM_CONEX into ##tmpNomConex from ['+@comun+'] .dbo.modulos where nombre=''LOTES'''
				EXEC (@SentenciaSQL)
				set @LOTES = (select NOM_CONEX from ##tmpNomConex)
				drop table ##tmpNomConex
			END TRY BEGIN CATCH END CATCH

			BEGIN TRY
				IF OBJECT_ID('tempdb..##pConfig01') IS NOT NULL BEGIN drop table ##tmpNomConex END
				set @SentenciaSQL = 'SELECT ltrim(rtrim(RUTA)) as RUTA, [ANY] INTO ##pConfig01 FROM ['+@comun+'].dbo.ejercici where predet=1'
				EXEC (@SentenciaSQL)
				set @GESTION  = (SELECT RUTA FROM ##pConfig01)
				SET @ejercicio = (SELECT [ANY] FROM ##pConfig01)
				set @LETRA = RIGHT(@GESTION,2)
				DROP TABLE ##pConfig01 
			END TRY BEGIN CATCH END CATCH

			if exists (select * from Configuracion)
			update Configuracion set Ejercicio=@ejercicio, Gestion=@GESTION, Letra=@LETRA, Comun=@comun, Campos=@CAMPOS, Lotes=@LOTES
								   , Empresa=@empresa, NombreEmpresa=@nombreEmpresa
			else insert into Configuracion (Ejercicio, Gestion, Letra, Comun, Campos, Lotes, Empresa, NombreEmpresa)
				 values (@ejercicio, @GESTION, @LETRA, @comun, @CAMPOS, @LOTES, @empresa, @nombreEmpresa)
		END


	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	if @modo='crearVistas' BEGIN 		
		EXEC @sp=pAppVistas 
		if @sp<>-1 	select 'Error! SP: pAppVistas!'  as JAVASCRIPT 	
		return -1
	END


	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	if @modo='crearProcedimientos' BEGIN 		
		--EXEC @sp=pAppProcedimientos 
		--if @sp<>-1 	select 'Error! SP: pAppProcedimientos!'  as JAVASCRIPT 	
		return -1
	END


	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	if @modo='crearFunciones' BEGIN 		
		EXEC @sp=pAppFunciones 
		if @sp<>-1 	select 'Error! SP: pAppFunciones!'  as JAVASCRIPT 	
		return -1
	END

	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	if @modo='crearPersonalizaciones' BEGIN 		
		declare @PersEmpresa varchar(50)
		declare Pers CURSOR for select Empresa from ConfigPersonalizaciones where Activo=1
		OPEN Pers FETCH NEXT FROM Pers INTO @PersEmpresa
		WHILE (@@FETCH_STATUS=0) BEGIN
			-- Ejecutamos el SP de la empresa a personalizar
			BEGIN TRY EXEC ('exec pPers' + @PersEmpresa) END TRY BEGIN CATCH select 'Error! SP: pPers' + @PersEmpresa+'!'  as JAVASCRIPT END CATCH
		FETCH NEXT FROM Pers INTO @PersEmpresa	END	CLOSE Pers deallocate Pers
		return -1
	END


	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	if @modo='objetoIO' BEGIN 		
		if exists (select * from ConfigApp where objeto=@objeto) 
			update ConfigApp set valor=@valor, activo=@confOpIO where objeto=@objeto
        else insert into ConfigApp (objeto,valor,activo) values (@objeto, @valor, @confOpIO) 
	END		

	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	if @modo='personalizacion' BEGIN 		
		if exists (select * from ConfigPersonalizaciones where Empresa=@cliente) 
			update ConfigPersonalizaciones set Activo=@confOpIO where Empresa=@cliente
        else insert into ConfigPersonalizaciones (Empresa,Activo) values (@cliente, @confOpIO) 
	END		
	

	----------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------
	select 'OK' as JAVASCRIPT        --   select * from Configuracion    --   select * from ConfigApp

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX) = ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pCrearAV]...';


GO
CREATE PROCEDURE [dbo].[pCrearAV]
(
	@EMPRESA varchar(2),
	@PEDIDO varchar(20),
	@LETRA varchar(2),
	@TERMINAL char(50),
	@imprimir INT,
	@imp_eti int,
	@respuestaSP VARCHAR(20)='' output,
	@nEti int = 0
)
AS
SET NOCOUNT ON;
-- ========================================================================
-- Author:		Albert Rodriguez
-- Create date: 04/09/18
-- Revision:	15/11/21
-- Description:	Creación de Cabecera de albarán de venta a partir de un ID
-- 				Procedimiento para enlace de web App 
-- Modifica:	Harold 10/09/2020
--				Harold 08/10/2020 Variable @totaldoc
--				Albert Variable @totaldoc
--				Albert 21/08/2021 Limpieza del procedimiento entero
--				Albert R. 15/11/21 Modificamos el procedimiento para que sea dinámico y no se tegna que modificar cada año.
--				Elías Jené 23-12-2021 - Dinamización
-- ========================================================================

-----------------------------------------------------------------------------------------------
--								Variables de Configuración  
-----------------------------------------------------------------------------------------------
declare @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
select  @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
-------------------------------------------------------------------------------------------------	
-----------------------------------------------------------------------------------------------
--								   Variables Dinamización  
-----------------------------------------------------------------------------------------------
declare @cont int
declare @tbInt TABLE (dato int)
declare @tbNum TABLE (dato numeric(15,6))
declare @tbStr TABLE (dato varchar(1000))
-----------------------------------------------------------------------------------------------
BEGIN TRY

	DECLARE @Mensaje varchar(max), @cSQL NVARCHAR(max), @cSQL2 NVARCHAR(max), @cSQL3 NVARCHAR(max), @cSQL4 NVARCHAR(max), @cSQL5 NVARCHAR(max), @cSQL6 NVARCHAR(max), @cSQL7 NVARCHAR(max), @cSQL8 NVARCHAR(max), @cSQL9 NVARCHAR(max), @cSQL10 NVARCHAR(max), @cSQL11 NVARCHAR(max), @cSQL12 NVARCHAR(max), @cSQL13 NVARCHAR(max), @cSQL14 NVARCHAR(max), @enuso  bit

	-- Marcamos en uso la tabla en EnUso para que no se pueda ejecutar el procedimiento CrearAV desde dos terminales hasta que el primero que lo ha ejecutado acabe.
	UPDATE EnUso set Enuso=1, fecha=getdate() WHERE TIPO='PROCEDURE'
		
	--15/10/2020 BEGIN TRAN SIRVE PARA QUE SI ALGUNA OPERACION FALLA SE DEVUELVAN TODOS LOS PROCESOS Y NO SE DAÑEN LOS DATOS.
	BEGIN TRAN 	
		DECLARE @LETRAFAC CHAR(2), @NUMERO VARCHAR(10)='', @NUMEROOUT VARCHAR(10)='', @NUMAV VARCHAR(10)='', @NUMAVOUT VARCHAR(10)='',@LINIA INT, @ParmDefinition nvarchar(max)

			-- Si no existe el pedido a traspasar en la tabla Temporal no continuamos con el procedimiento
		if not exists (select * FROM TRASPASO_TEMPORAL TT WHERE TT.EMPRESA=@EMPRESA AND TT.NUMERO=@PEDIDO AND TT.LETRA=@LETRA GROUP BY TT.EMPRESA, TT.NUMERO, TT.LETRA)
		begin 
			set @respuestaSP='No existe el Traspaso Temporal'
			raiserror ('Sale porque no existe el Traspaso Temporal',16,1);
			return 0
		end

		-- Albert Rodriguez 23/10/2021 - Si la serie es CO, al albaranar/facturar la cambiaremos por la serie DI (Pedido por Ignasi)
		IF @LETRA='CO'
			SET @LETRAFAC='DI'
		ELSE
			SET @LETRAFAC=@LETRA

		SET @NUMERO=''

		SET @cSQL = N'	SELECT @NUMAVOUT=NUMERO FROM '+@GESTION+'.DBO.C_ALBVEN WHERE LETRA=''FR'' AND PEDIDO='''+@PEDIDO+@LETRAFAC+''' AND FACTURA='''' AND IMPRESO=0';
		SET @ParmDefinition = N'@NUMAVOUT VARCHAR(10) OUTPUT'; 
		EXECUTE sp_executesql @cSQL, @ParmDefinition, @NUMAVOUT=@NUMAV OUTPUT; 

		SET @NUMAV=COALESCE(@NUMAV,'')

		-- Si el numero de albarán no existe obtenemos un nuevo numero, si existe añadiremos las líneas.
		IF ISNULL(@NUMAV,'')=''
			BEGIN
				SET @cSQL = N'SET @NUMEROOUT=(SELECT SPACE(10-LEN(CAST(CONTADOR+1 AS VARCHAR(10))))+CAST(CONTADOR+1 AS VARCHAR(10)) 
				from '+@GESTION+'.dbo.series 
					WHERE empresa='''+@EMPRESA+''' and serie='''+@LETRAFAC+''' and TIPODOC=1)
				WHILE (SELECT NUMERO FROM '+@GESTION+'.DBO.C_ALBVEN WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO=@NUMEROOUT AND LETRA='''+@LETRAFAC+''') IS NOT NULL
				BEGIN
					SET @NUMEROOUT=@NUMEROOUT+1
				END';
				SET @ParmDefinition = N'@NUMEROOUT VARCHAR(10) OUTPUT'; 
				EXECUTE sp_executesql @cSQL, @ParmDefinition, @NUMEROOUT=@NUMERO OUTPUT; 

				set @cSQL ='UPDATE '+@GESTION+'.dbo.series set CONTADOR='''+@NUMERO+''' where empresa='''+@EMPRESA+''' and serie='''+@LETRAFAC+''' and TIPODOC=1'
				exec(@cSQL)
			END
		ELSE
			BEGIN
				SET @NUMERO=@NUMAV
			END
		
		-- Añadimos los espacios delta del número de albarán
		SET @NUMERO = SPACE(10-LEN(LTRIM(RTRIM(@NUMERO))))+LTRIM(RTRIM(@NUMERO))

		--LOG1
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES ('Emp: '+ltrim(rtrim(@EMPRESA))+' / Numero: '+ltrim(Rtrim(@PEDIDO))+' / Letra: '+ltrim(Rtrim(@LETRA))+' / Log:1',GETDATE(), @EMPRESA, @PEDIDO, @LETRA)
		

		SET @cSQL = '	
		-- Declaración de variables	
		DECLARE	@USUARIO VARCHAR(25)='''', @FECHA VARCHAR(10), @CLIENTE VARCHAR(10), @CLIMANDATO VARCHAR(10), @ENV_CLI INT, 
		@PRONTO NUMERIC(20,2), @NCLIENTE VARCHAR(80), @VENDEDOR VARCHAR(2), @RUTA VARCHAR(2), @ALMACEN VARCHAR(2), @FACTURA VARCHAR(10), 
		@FECHA_FAC SMALLDATETIME, @ASI VARCHAR(20), @FPAG VARCHAR(2), @IMPORTE NUMERIC(15,6), @OBSERVACIO VARCHAR(250), @BANC_CLI INT, 
		@DIVISA VARCHAR(3), @CAMBIO NUMERIC(20,6), @IMPDIVISA NUMERIC(15,6), @FINAN NUMERIC(20,4), @VISTA BIT, @COSTE NUMERIC(20,6), 
		@PESO NUMERIC(20,4), @LITROS NUMERIC(20,4), @OBRA VARCHAR(5), @TRASPASADO BIT, @RECEQUIV BIT, @TAG BIT, @OPERARIO VARCHAR(2), 
		@FACTURABLE BIT, @COT_PUNT NUMERIC(20,4), @PUNTOS NUMERIC(20,4), @CLIFINAL VARCHAR(10), @IMPRESO BIT, @LIBRE_1 VARCHAR(10), 
		@LIBRE_2 VARCHAR(10),@LIBRE_3 VARCHAR(10), @CERTIFIC INT, @STOCK_COEF NUMERIC(4,0), @EDI BIT, @GASTOS BIT, @FECHASTOCK SMALLDATETIME, 
		@MANDATO VARCHAR(35), @RECC BIT, @TOTALDOC NUMERIC(15,6), @AGENCIA VARCHAR(3), @TIPOPORTE INT, @CONTADOR VARCHAR(5), @BULTOS VARCHAR(7), 
		@PESTOTAL NUMERIC(20,4), @PORTES VARCHAR(7), @CONDUCTO VARCHAR(30), @DIR_ENV VARCHAR(80), @CP_ENV VARCHAR(10), @POBL_ENV VARCHAR(30), 
		@PROV_ENV VARCHAR(30), @TELF_ENV VARCHAR(15), @TARIFA VARCHAR(2)='''', @REPARTIDOR VARCHAR(2), @IVAPORT varchar(2), @LINIA INT,
		@EJERCICIO varchar(4), @Factdirv BIT, @IVATOT NUMERIC(20,2), @SIREC bit=0, @IVAREC NUMERIC(20,2), @ETICAJAS VARCHAR(7),
		@PVERDECLI INT,  @FECHA_ENTREGA VARCHAR(10), @VALPORTES NUMERIC(20,3), @PORTCOMP NUMERIC(20,3), @IVAPORTES NUMERIC(20,3), @IMPPORTES NUMERIC(20,3),
		@RECPORTES NUMERIC(20,3), @RECARGO BIT, @NOPORTES BIT, @CONTADO BIT, @cSQL NVARCHAR(max), @TERMINAL_new char(50), @MensajeError varchar(max)
			
	
		
		-- Asignación de variables

		SET @MensajeError=''","ERROR":"''
		SET @EJERCICIO='''+@EJERCICIO+'''
		set @terminal_new='''+@TERMINAL+'''
		
		SELECT @BULTOS=MAX(isnull(BULTOS,''0'')), @ETICAJAS=MAX(isnull(ETICAJAS,''0'')), @PESTOTAL=SUM(CAST(CASE WHEN COALESCE(peso,''0.00'')='''' THEN ''0.00'' ELSE COALESCE(peso,''0.00'') END AS DECIMAL(15,4)))
		FROM TRASPASO_TEMPORAL 
		WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''' GROUP BY SESION+EMPRESA+NUMERO+LETRA
		
		SET @USUARIO=''MERLOS#API#''		
		'
		SET @cSQL2 = '		
		-- Albert R., si estamos a viernes y la fecha de entrega es lunes, la fecha de todos los documentos será sábado. Esto lo ha pedido Ignasi.
		SET @FECHA_ENTREGA=(SELECT CONVERT(CHAR(11), ENTREGA, 103) FROM '+@GESTION+'.dbo.c_pedive WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''')
		IF datepart(weekday, getdate())=5 AND (datepart(weekday, @FECHA_ENTREGA)=1 or datepart(weekday, @FECHA_ENTREGA)=2 or datepart(weekday, @FECHA_ENTREGA)=3 or datepart(weekday, @FECHA_ENTREGA)=4)
			BEGIN
				SET @FECHA=CAST(CONVERT(char(11), getdate()+1-0.375, 103) as VARCHAR) 
				SET @FECHA_FAC=CAST(CONVERT(char(11), getdate()+1-0.375, 103) as VARCHAR)
				SET @FECHASTOCK=getdate()+1-0.375
			END
		ELSE
			BEGIN
				SET @FECHA=CAST(CONVERT(char(11), getdate()-0.375, 103) as VARCHAR) 
				SET @FECHA_FAC=CAST(CONVERT(char(11), getdate()-0.375, 103) as VARCHAR)
				SET @FECHASTOCK=getdate()-0.375
			END

		-- Serie ET, al generar el albarán que se genere con la fecha del día
		IF '''+@LETRA+''' = ''ET''
			SET @FECHA=CAST(CONVERT(char(11), getdate(), 103) as VARCHAR) 

		--LOG2
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:2'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL3 = '		
		-- Si la fecha es 31/12/XX la fecha de los albaranes y facturas será 02/01/ del siguiente año
		IF CAST(CONVERT(char(11), getdate()-0.375, 103) as VARCHAR) = ''31/12/2020''
			BEGIN
				SET @FECHA=CAST(CONVERT(char(11), getdate()+2-0.375, 103) as VARCHAR) 
				SET @FECHA_FAC=CAST(CONVERT(char(11), getdate()+2-0.375, 103) as VARCHAR)
				SET @FECHASTOCK=getdate()+2-0.375
			END

		-- Asignamos valor a las variables con los datos del pedido
		SELECT	@CLIENTE=C_PEDIVE.CLIENTE, @NCLIENTE=CLIENTES.NOMBRE, @ENV_CLI=case when C_PEDIVE.ENV_CLI=0 then clientes.env_cli else C_PEDIVE.ENV_CLI end, @PRONTO=C_PEDIVE.PRONTO, 
				@VENDEDOR=C_PEDIVE.VENDEDOR, @RUTA=C_PEDIVE.RUTA, @ALMACEN=C_PEDIVE.ALMACEN, @Factdirv=FACTDIRV, @CLIFINAL=CLIENTES.CLIFINAL, @FPAG=C_PEDIVE.FPAG,
				@IMPORTE=C_PEDIVE.TOTALPED, @OBSERVACIO=C_PEDIVE.OBSERVACIO, @BANC_CLI=BANC_CLI.CODIGO, @DIVISA=C_PEDIVE.DIVISA,
				@CAMBIO=C_PEDIVE.CAMBIO, @IMPDIVISA=C_PEDIVE.IMPDIVISA, @VISTA=C_PEDIVE.VISTA, @PESO=C_PEDIVE.PESO, @LITROS=C_PEDIVE.LITROS,
				@OBRA=C_PEDIVE.OBRA, @OPERARIO=C_PEDIVE.OPERARIO, @FACTURABLE=CASE WHEN coalesce(NOFACALB,0)=0 THEN 1 ELSE 0 END,
				@AGENCIA=CLIENTES.AGENCIA, @TIPOPORTE=CASE WHEN CLIENTES.PORTES=''DEBIDOS'' THEN 1 ELSE 2 END,
				@PORTES=CLIENTES.PORTES, @VALPORTES=CLIENTES.VALPORTES, @PORTCOMP=CLIENTES.PORTCOMP, @REPARTIDOR=coalesce(ADICLIENT.REPARTIDOR,''''), @TARIFA=CLIENTES.TARIFA, @PVERDECLI=CLIENTES.PVERDE,
				@RECARGO=CLIENTES.RECARGO, @NOPORTES=COALESCE(CAA.EWNOPORT,0) , @CONTADO=CLIENTES.CONTADO
		FROM '+@GESTION+'.DBO.C_PEDIVE INNER JOIN '+@GESTION+'.DBO.CLIENTES ON C_PEDIVE.CLIENTE=CLIENTES.CODIGO
				LEFT JOIN [DISTRIYB].DBO.ADICLIENT ON C_PEDIVE.CLIENTE=ADICLIENT.CLIENTE
				LEFT JOIN '+@GESTION+'.DBO.BANC_CLI ON C_PEDIVE.CLIENTE=BANC_CLI.CLIENTE AND ORDEN=1
				LEFT JOIN [CAMPOSYB].DBO.c_pediveew CAA ON CAA.EJERCICIO=''2021'' AND CAA.EMPRESA=C_PEDIVE.EMPRESA AND CAA.NUMERO=C_PEDIVE.NUMERO AND CAA.LETRA=C_PEDIVE.LETRA
		WHERE C_PEDIVE.EMPRESA='''+@EMPRESA+''' AND C_PEDIVE.LETRA='''+@LETRA+''' AND C_PEDIVE.NUMERO='''+@PEDIDO+'''

		-- Asiganción de variables
		SELECT @FACTURA='''', @ASI=''''

		BEGIN
			IF @CLIFINAL=''''
				SET @CLIMANDATO=@CLIENTE
			ELSE
				SET @CLIMANDATO=@CLIFINAL
		END


		SELECT @IVAPORTES=IVA.IVA, @RECPORTES=CASE WHEN @RECARGO=1 THEN IVA.RECARG ELSE 0.00 END FROM '+@GESTION+'.DBO.CODIGOS COD INNER JOIN '+@GESTION+'.DBO.TIPO_IVA IVA ON IVA.CODIGO=COD.IVA_PORTES WHERE COD.EMPRESA='''+@EMPRESA+'''
		
		-- Asignación de variables 
		SELECT @FINAN=0.00, @COSTE=0.00, @TRASPASADO=0, @RECEQUIV=0,  @TAG=0, @COT_PUNT=0.00, @PUNTOS=0.00, @IMPRESO=0,
			@LIBRE_1='''', @LIBRE_2='''', @LIBRE_3='''', @CERTIFIC=0, @STOCK_COEF=1, @EDI=0, @GASTOS=1, @RECC=0

		--LOG3
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:3'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL4 = '		
		
		SET @MANDATO=COALESCE((SELECT MANDATO FROM [COMU0001].DBO.MANDATOS M 
			INNER JOIN '+@GESTION+'.DBO.BANC_CLI B ON B.CLIENTE=M.CLIENTE AND B.IBAN+B.CUENTAIBAN=M.CLI_IBAN 
		WHERE M.CLIENTE=@CLIMANDATO AND M.DEFECTO=1 AND B.ORDEN=1),'''')

		SET @CONTADOR=(SELECT ETIQUETA+1 FROM '+@GESTION+'.DBO.CONTADOR WHERE EMPRESA='''+@EMPRESA+''')
		UPDATE '+@GESTION+'.DBO.CONTADOR SET ETIQUETA=ETIQUETA+1  WHERE EMPRESA='''+@EMPRESA+'''
		IF NOT EXISTS (SELECT NOMBRE FROM '+@GESTION+'.DBO.AGENCIA WHERE CODIGO=@AGENCIA)
			SET @CONDUCTO=''''
		ELSE
			SET @CONDUCTO=(SELECT NOMBRE FROM '+@GESTION+'.DBO.AGENCIA WHERE CODIGO=@AGENCIA)
		
		-- Albert Rodriguez 27/10/20
		-- Si el cliente es contado, los datos de la dirección de envío hay que cogerlos de otra tabla
		IF @CONTADO=0
			SELECT @DIR_ENV=DIRECCION, @CP_ENV=CODPOS, @POBL_ENV=POBLACION, @PROV_ENV=PROVINCIA, @TELF_ENV=TELEFONO FROM '+@GESTION+'.DBO.ENV_CLI 
			WHERE CLIENTE=@CLIENTE AND LINEA=@ENV_CLI
		ELSE
			SELECT @DIR_ENV=DIRECCION, @CP_ENV=CODPOST, @POBL_ENV=POBLACION, @PROV_ENV=PROVINCIA, @TELF_ENV=TELEFONO FROM '+@GESTION+'.DBO.CONTADO 
			WHERE EMPRESA='''+@EMPRESA+''' AND LETRA='''+@LETRA+''' AND NUMERO='''+@PEDIDO+''' AND TIPO=2
		
		IF @CONTADO=1
			BEGIN
				INSERT INTO '+@GESTION+'.DBO.CONTADO (EMPRESA, NUMERO, TIPO, NOMBRE, DIRECCION, CODPOST, POBLACION, PROVINCIA, PAIS, CIF, TELEFONO, LETRA, EMAIL)
				SELECT EMPRESA, '''+@NUMERO+''', 1 AS TIPO, NOMBRE, DIRECCION, CODPOST, POBLACION, PROVINCIA, PAIS, CIF, TELEFONO, LETRA, EMAIL
				FROM '+@GESTION+'.DBO.CONTADO WHERE EMPRESA='''+@EMPRESA+''' AND LETRA='''+@LETRA+''' AND NUMERO='''+@PEDIDO+''' AND TIPO=2
			END
			
		
		IF ltrim(rtrim(isnull(@TELF_ENV,'''')))=''''
			SET @TELF_ENV=(SELECT TELEFONO FROM '+@GESTION+'.DBO.TELF_CLI WHERE CLIENTE=@CLIENTE AND ORDEN=1)

		IF ltrim(rtrim(isnull(@TELF_ENV,'''')))=''''
			SET @TELF_ENV=''          ''

		IF @RUTA IS NULL OR @RUTA = ''''
			SET @RUTA = (SELECT RUTA FROM '+@GESTION+'.dbo.clientes WHERE CODIGO=@CLIENTE)

		--LOG4
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:4'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'

		SET @cSQL5 = '		
		IF @BANC_CLI IS NULL 
			SET @BANC_CLI = 0 
		IF '''+@NUMAV+'''='''' -- IS NULL
			BEGIN
				-- CREACION DE ALBARANES
				-- ****** CABECERAS ALBARAN ******
				-- INSERTAR C_ALBVEN
				INSERT INTO '+@GESTION+'.DBO.C_ALBVEN (PEDIDO, USUARIO, EMPRESA, NUMERO, LETRA, FECHA, CLIENTE, ENV_CLI, PRONTO, VENDEDOR,
				RUTA, ALMACEN, FACTURA, FECHA_FAC, ASI, FPAG, IMPORTE, OBSERVACIO, BANC_CLI, DIVISA, CAMBIO, IMPDIVISA, FINAN, VISTA, COSTE, PESO, 
				LITROS, OBRA, TRASPASADO, RECEQUIV, TAG, OPERARIO, FACTURABLE, COT_PUNT, PUNTOS, CLIFINAL, IMPRESO, LIBRE_1, LIBRE_2, LIBRE_3, 
				CERTIFIC, STOCK_COEF, EDI, GASTOS, FECHASTOCK, MANDATO, RECC, TOTALDOC) 
				VALUES ('''+@PEDIDO+'''+'''+@LETRA+''', @USUARIO, '''+@EMPRESA+''', '''+@NUMERO+''', '''+@LETRAFAC+''', @FECHA, @CLIENTE, @ENV_CLI, @PRONTO, @VENDEDOR, @RUTA, 
				@ALMACEN, @FACTURA, @FECHA_FAC, @ASI, @FPAG, @IMPORTE, @OBSERVACIO, @BANC_CLI, @DIVISA, @CAMBIO, @IMPDIVISA, @FINAN, @VISTA, @COSTE,
				@PESTOTAL, 
				@LITROS, @OBRA, @TRASPASADO, @RECEQUIV, @TAG, @OPERARIO, @FACTURABLE, @COT_PUNT, @PUNTOS, @CLIFINAL, @IMPRESO, @LIBRE_1, @LIBRE_2,
				@LIBRE_3, @CERTIFIC, @STOCK_COEF, @EDI, @GASTOS, @FECHASTOCK, @MANDATO, @RECC, 0.00)

				-- INSERTAR ADIENTREGA
				INSERT INTO [DISTRIYB].DBO.ADIENTREGA (EJERCICIO, EMPRESA, NUMERO, LETRA, LINEA) 
					SELECT @EJERCICIO, EMPRESA, '''+@NUMERO+''' AS NUMERO, '''+@LETRAFAC+''', LINEA 
					FROM '+@GESTION+'.DBO.entre_pv WHERE EMPRESA='''+@EMPRESA+''' AND LETRA='''+@LETRA+''' AND NUMERO='''+@PEDIDO+'''

				-- INSERTAR ALBV_ADI
				INSERT INTO '+@GESTION+'.DBO.ALBV_ADI (EMPRESA, NUMERO, COSTEDIV, LETRA) 
					VALUES ('''+@EMPRESA+''', '''+@NUMERO+''', 0.00, '''+@LETRAFAC+''')

				-- INSERTAR ALB_FPAG
				INSERT INTO '+@GESTION+'.DBO.ALB_FPAG (EMPRESA, NUMERO, GIRO, LETRA) 
					SELECT '''+@EMPRESA+''', '''+@NUMERO+''', GIRO, '''+@LETRAFAC+''' FROM [COMU0001].DBO.FPAG_GIR WHERE FPAG=@FPAG

				-- INSERTAR MULTICA2
				INSERT INTO '+@GESTION+'.DBO.MULTICA2 (EMPRESA, NUMERO, FICHERO, CAMPO, VALOR, LETRA) 
					SELECT EMPRESA, '''+@NUMERO+''', 1, CAMPO, '''', '''+@LETRAFAC+''' FROM '+@GESTION+'.DBO.MULTICA2 WHERE FICHERO=2 AND EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+'''

				-- INSERTAR PORTES --> tipo iva tienes ''03'' quemado
				SELECT @IVAPORT=IVA_PORTES FROM '+@GESTION+'.DBO.CODIGOS WHERE EMPRESA='''+@EMPRESA+'''
				INSERT INTO '+@GESTION+'.DBO.PORTES (EMPRESA, CLIENTE, ALBARAN, INC_PP, IVA_INC, IMPORTE, IMPORTEDIV, TIPO_IVA, AGENCIA, LETRA, INC_FRA, TIPO_PORTE) 
					VALUES('''+@EMPRESA+''', @CLIENTE, '''+@NUMERO+''', 1, 0, 0.00, 0.00, @IVAPORT, @AGENCIA, '''+@LETRAFAC+''', 1, @TIPOPORTE)

				-- INSERTAR ETIQUETAS ENVIO
				INSERT INTO '+@GESTION+'.DBO.ENVIOETI (EMPRESA, CONTADOR, NUMERO, CODIGOCLI, CLIENTE, BULTOS, PESO, PORTES, CONDUCTO, FECHA, DIRECCION, 
					CODPOS, POBLACION, PROVINCIA, AGENCIA, LETRA, RECOGIDA, TELEFONO) 
				VALUES ('''+@EMPRESA+''', @CONTADOR, '''+@NUMERO+''', @CLIENTE, @NCLIENTE, @BULTOS, @PESTOTAL, @PORTES, @CONDUCTO, @FECHA, COALESCE(@DIR_ENV,''''), 
					COALESCE(@CP_ENV,''''), COALESCE(@POBL_ENV,''''), COALESCE(@PROV_ENV,''''), @AGENCIA, '''+@LETRAFAC+''', @FECHA, @TELF_ENV)

		--LOG5
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:5'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL6 = '		
				-- INSERTAR CAMPOSAD!C_ALBVENEW
				INSERT INTO [CAMPOSYB].DBO.C_ALBVENEW (EJERCICIO, EMPRESA, NUMERO, LETRA, EWNETI)
					VALUES (@EJERCICIO, '''+@EMPRESA+''', '''+@NUMERO+''', '''+@LETRAFAC+''', @ETICAJAS)

				-- INSERTAR DISTRIBU!C_ADIALBVE
				INSERT INTO [DISTRIYB].DBO.C_ADIALBVE (EJERCICIO, EMPRESA, NUMERO, LETRA, REPARTIDOR, TOTALALB)
					VALUES (@EJERCICIO, '''+@EMPRESA+''', '''+@NUMERO+''', '''+@LETRAFAC+''', @REPARTIDOR, @IMPORTE)

				-- ****** LINEAS DE ALBARAN ******
				-- INSERTAR D_ALBVEN
				-- PRIMERA LÍNEA CON UNA NOTA CON EL NÚMERO DE PEDIDO
				INSERT INTO '+@GESTION+'.DBO.D_ALBVEN (USUARIO, EMPRESA, NUMERO, DEFINICION, PEDIDO, FECHA, LINIA, CLIENTE, DOC, DOC_NUM, DOC_LIN, LETRA, FLACTUAL, FLANTERIOR) 
					VALUES (@USUARIO, '''+@EMPRESA+''', '''+@NUMERO+''', ''Pedido nº ''+'''+@LETRA+'''+'' - ''+LTRIM(RTRIM('''+@PEDIDO+'''))+'' de fecha ''+ @FECHA, '''+@PEDIDO+'''+'''+@LETRA+''', @FECHA, 1, @CLIENTE, 1, 
					'''+@PEDIDO+'''+'''+@LETRA+''', 0, '''+@LETRAFAC+''', @FECHA, @FECHA)
				-- Líneas de Artículos
				INSERT INTO '+@GESTION+'.DBO.D_ALBVEN (USUARIO, EMPRESA, NUMERO, ARTICULO, DEFINICION, UNIDADES, DTO1, DTO2, TIPO_IVA, COSTE, FECHA, PEDIDO, LINIA, CLIENTE, PRECIO, 
					IMPORTE, PRECIOIVA, IMPORTEIVA, CAJAS, FAMILIA, PRECIODIV, IMPORTEDIV, TIPO, COMISION, IMP_COM, PESO, DOC, DOC_NUM, DOC_LIN, DOC_UNID, VISTA, PVERDE, LETRA, 
					IMPDIVIVA, PREDIVIVA, FLACTUAL, FLANTERIOR, DOC_CAJA)
				SELECT @USUARIO AS USUARIO, '''+@EMPRESA+''' AS EMPRESA, '''+@NUMERO+''' AS NUMERO, MAX(TT.ARTICULO) COLLATE DATABASE_DEFAULT AS ARTICULO, MAX(DPV.DEFINICION) AS DEFINICION, 
					SUM(TT.UNIDADES) AS UNIDADES, MAX(DPV.DTO1) AS DTO1, MAX(DPV.DTO2) AS DTO2, MAX(DPV.TIPO_IVA) AS TIPO_IVA, MAX(DPV.COSTE) AS COSTE, @FECHA AS FECHA, 
					'''+@PEDIDO+'''+'''+@LETRA+''' AS PEDIDO, DPV.LINIA+1 AS LINIA, @CLIENTE AS CLIENTE, MAX(DPV.PRECIO) AS PRECIO, 
					0.00 AS IMPORTE, MAX(DPV.PRECIOIVA) AS PRECIOIVA, 0.00 AS IMPORTEIVA, CASE WHEN MAX(ART.UNICAJA) != 0.00 then floor(SUM(COALESCE(TT.UNIDADES,0.00))/MAX(art.unicaja)) else SUM(COALESCE(TT.CAJAS,0.00)) end AS CAJAS, MAX(DPV.FAMILIA) AS FAMILIA, MAX(DPV.PRECIODIV) AS PRECIODIV,
					0.00 AS IMPORTEDIV, 0 AS TIPO, 0.00 AS COMISION, 0.00 AS IMP_COM, SUM(CAST(CASE WHEN COALESCE(TT.PESO,''0.00'')='''' THEN ''0.00'' ELSE COALESCE(TT.PESO,''0.00'') END AS DECIMAL(16,4))) AS PESO, 1 AS DOC, '''+@PEDIDO+'''+'''+@LETRA+''' AS DOC_NUM, 
					DPV.LINIA AS LIN_DOC, SUM(TT.UNIDADES) AS UDS_DOC, 1 AS VISTA, MAX(DPV.PVERDE) AS PVERDE, '''+@LETRAFAC+''' AS LETRA, 
					0.00 AS IMPDIVIVA, MAX(DPV.PREDIVIVA) AS PREDIVIVA, @FECHA AS FLACTUAL, @FECHA AS FLANTERIOR, SUM(coalesce(TT.CAJAS,0.00)) AS CAJAS_DOC 
				FROM TRASPASO_TEMPORAL TT 
					INNER JOIN '+@GESTION+'.DBO.D_PEDIVE DPV ON DPV.EMPRESA+DPV.LETRA+DPV.NUMERO+STR(DPV.LINIA)=TT.EMPRESA+TT.LETRA+TT.NUMERO+STR(TT.LINEA) COLLATE DATABASE_DEFAULT 
					LEFT JOIN '+@GESTION+'.DBO.ARTICULO ART ON ART.CODIGO=TT.ARTICULO COLLATE DATABASE_DEFAULT 
				WHERE TT.EMPRESA='''+@EMPRESA+''' AND TT.NUMERO='''+@PEDIDO+''' AND TT.LETRA='''+@LETRA+''' GROUP BY TT.EMPRESA, TT.NUMERO, TT.LETRA, DPV.LINIA 

				-- INSERTAR CAMPOSAD!D_ALBVENEW
				INSERT INTO [CAMPOSYB].DBO.D_ALBVENEW (EJERCICIO, EMPRESA, NUMERO, LETRA, LINIA) 
					SELECT @EJERCICIO, EMPRESA, '''+@NUMERO+''', '''+@LETRAFAC+''', LINEA+1 FROM TRASPASO_TEMPORAL 
				WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''' GROUP BY EMPRESA, NUMERO, LETRA, LINEA
		
				-- INSERTAR DISTRIBU!D_ADIALBVE
				INSERT INTO [DISTRIYB].DBO.D_ADIALBVE (EJERCICIO, EMPRESA, NUMERO, LETRA, LINIA, OPERACION, TARIFA)
					VALUES (@EJERCICIO, '''+@EMPRESA+''', '''+@NUMERO+''', '''+@LETRAFAC+''', 1, 1, @TARIFA)

		--LOG6
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:6'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL7 = '				
				INSERT INTO [DISTRIYB].DBO.D_ADIALBVE (EJERCICIO, EMPRESA, NUMERO, LETRA, LINIA, OPERACION, TARIFA) 
					SELECT @EJERCICIO, EMPRESA, '''+@NUMERO+''', '''+@LETRAFAC+''', LINEA+1, 1, @TARIFA FROM TRASPASO_TEMPORAL 
				WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''' GROUP BY EMPRESA, NUMERO, LETRA, LINEA
			
				-- INSERTAR LOTES
				INSERT INTO [LOTES0YB].DBO.LTALBVE (EMPRESA, LETRA, NUMERO, FECHA, ARTICULO, LINIA, LOTE, UNIDADES, PERIODO, ALMACEN, CADUCIDAD, FECHA2, PESO, ASI)
				SELECT '''+@EMPRESA+''' AS EMPRESA, '''+@LETRA+''' AS LETRA, '''+@NUMERO+''' AS NUMERO, @FECHA AS FECHA, T.ARTICULO, T.LINEA+1, T.LOTE, T.UNIDADES, @EJERCICIO AS PERIODO, @ALMACEN, 
				CADUCIDAD, @FECHASTOCK, CASE WHEN COALESCE(PESO,''0.00'')='''' THEN ''0.00'' ELSE COALESCE(PESO,''0.00'') END, '''' AS ASI FROM TRASPASO_TEMPORAL T INNER JOIN [LOTES0YB] .DBO.artlot A ON A.ARTICULO=T.ARTICULO COLLATE DATABASE_DEFAULT
				WHERE T.EMPRESA='''+@EMPRESA+''' AND T.NUMERO='''+@PEDIDO+''' AND T.LETRA='''+@LETRA+''' AND T.LOTE!='''' AND T.UNIDADES!=0.00 AND A.LOTE=1


				-- INSERTAR LOTES!D_ALBVEN2
				INSERT INTO [LOTES0YB].DBO.D_ALBVEN2 (EJERCICIO, EMPRESA, NUMERO, LETRA, LINIA) 
					SELECT @EJERCICIO, T.EMPRESA, '''+@NUMERO+''', '''+@LETRAFAC+''', T.LINEA FROM TRASPASO_TEMPORAL T INNER JOIN [LOTES0YB] .DBO.artlot A ON A.ARTICULO=T.ARTICULO COLLATE DATABASE_DEFAULT 
				WHERE T.EMPRESA='''+@EMPRESA+''' AND T.NUMERO='''+@PEDIDO+''' AND T.LETRA='''+@LETRA+''' AND A.LOTE=1 AND T.LINEA NOT IN (SELECT LINEA FROM [LOTES0YB].DBO.D_ALBVEN2 WHERE EJERCICIO=@EJERCICIO AND EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRA+''') GROUP BY T.EMPRESA, T.NUMERO, T.LETRA, T.LINEA

			END
		ELSE
			BEGIN
				-- INSERTAR LINEAS EN ALBARANES EXISTENTES

				-- ****** LINEAS DE ALBARAN ******
				SET @LINIA=(SELECT MAX(LINIA) AS LINIA FROM '+@GESTION+'.DBO.D_ALBVEN WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+''')
				UPDATE TRASPASO_TEMPORAL SET LINEA_AV=T2.LINIA_AV FROM TRASPASO_TEMPORAL T INNER JOIN 
					(select ROW_NUMBER() OVER(PARTITION BY SESION, EMPRESA, NUMERO, LETRA ORDER BY SESION, EMPRESA, NUMERO, LETRA )+@LINIA AS LINIA_AV, SESION, EMPRESA, NUMERO, LETRA, LINEA 
					FROM TRASPASO_TEMPORAL where EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''' 
					GROUP BY SESION, EMPRESA, NUMERO, LETRA, LINEA) T2 ON T2.SESION=T.SESION AND T2.EMPRESA=T.EMPRESA AND T2.NUMERO=T.NUMERO AND T2.LETRA=T.LETRA AND T2.LINEA=T.LINEA
		--LOG7
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:7'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL8 = '				
				-- INSERTAR D_ALBVEN
				INSERT INTO '+@GESTION+'.DBO.D_ALBVEN (USUARIO, EMPRESA, NUMERO, ARTICULO, DEFINICION, UNIDADES, DTO1, DTO2, TIPO_IVA, COSTE, FECHA, PEDIDO, LINIA, CLIENTE, PRECIO, 
					IMPORTE, PRECIOIVA, IMPORTEIVA, CAJAS, FAMILIA,PRECIODIV, IMPORTEDIV, TIPO, COMISION, IMP_COM, PESO, DOC, DOC_NUM, DOC_LIN, DOC_UNID, VISTA, PVERDE, LETRA, 
					IMPDIVIVA, PREDIVIVA, FLACTUAL, FLANTERIOR, DOC_CAJA)
				SELECT @USUARIO AS USUARIO, '''+@EMPRESA+''' AS EMPRESA, '''+@NUMERO+''' AS NUMERO, MAX(TT.ARTICULO) COLLATE DATABASE_DEFAULT AS ARTICULO, MAX(DPV.DEFINICION) AS DEFINICION, 
					SUM(TT.UNIDADES) AS UNIDADES, MAX(DPV.DTO1) AS DTO1, MAX(DPV.DTO2) AS DTO2, MAX(DPV.TIPO_IVA)AS TIPO_IVA, MAX(DPV.COSTE) AS COSTE, @FECHA AS FECHA, 
					'''+@PEDIDO+'''+'''+@LETRA+''' AS PEDIDO, TT.LINEA_AV, @CLIENTE AS CLIENTE, MAX(DPV.PRECIO) AS PRECIO, 
					0.00 AS IMPORTE, MAX(DPV.PRECIOIVA) AS PRECIOIVA, 0.00 AS IMPORTEIVA, SUM(COALESCE(TT.CAJAS,0.00)) AS CAJAS, MAX(DPV.FAMILIA) AS FAMILIA, MAX(DPV.PRECIODIV) AS PRECIODIV, 
					0.00 AS IMPORTEDIV, 0 AS TIPO, 0.00 AS COMISION, 0.00 AS IMP_COM, SUM(CAST(CASE WHEN COALESCE(TT.PESO,''0.00'')='''' THEN ''0.00'' ELSE COALESCE(TT.PESO,''0.00'') END AS DECIMAL(16,4))) AS PESO, 1 AS DOC, '''+@PEDIDO+'''+'''+@LETRA+''' AS DOC_NUM, 
					TT.LINEA AS LIN_DOC, SUM(TT.UNIDADES) AS UDS_DOC, 1 AS VISTA, MAX(DPV.PVERDE) AS PVERDE, '''+@LETRAFAC+''' AS LETRA, 
					0.00 AS IMPDIVIVA, MAX(DPV.PREDIVIVA) AS PREDIVIVA, @FECHA AS FLACTUAL, @FECHA AS FLANTERIOR, SUM(COALESCE(TT.CAJAS,0.00)) AS CAJAS_DOC 
				FROM TRASPASO_TEMPORAL TT 
					INNER JOIN '+@GESTION+'.DBO.D_PEDIVE DPV ON DPV.EMPRESA+DPV.LETRA+DPV.NUMERO+STR(DPV.LINIA)=TT.EMPRESA+TT.LETRA+TT.NUMERO+STR(TT.LINEA) COLLATE DATABASE_DEFAULT 
				WHERE TT.EMPRESA='''+@EMPRESA+''' AND TT.NUMERO='''+@PEDIDO+''' AND TT.LETRA='''+@LETRA+''' AND (DPV.SERVIDAS<DPV.UNIDADES OR DPV.TIPO_IVA='''') GROUP BY TT.EMPRESA, TT.NUMERO, TT.LETRA, TT.LINEA, TT.LINEA_AV

				-- ACTUALIZAR BULTOS	
				UPDATE '+@GESTION+'.DBO.ENVIOETI SET BULTOS=CAST(BULTOS AS DECIMAL(7))+CAST(@BULTOS AS DECIMAL(7)) WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+'''

				-- ACTUALIZAR Nº ETIQUETAS CAMPOSAD!C_ALBVENEW
				UPDATE [CAMPOSYB].DBO.C_ALBVENEW SET EWNETI=EWNETI+@ETICAJAS WHERE EJERCICIO=@EJERCICIO AND EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+'''
							
				-- INSERTAR CAMPOSAD!D_ALBVENEW
				INSERT INTO [CAMPOSYB].DBO.D_ALBVENEW (EJERCICIO, EMPRESA, NUMERO, LETRA, LINIA) 
					SELECT @EJERCICIO, EMPRESA, '''+@NUMERO+''', '''+@LETRAFAC+''', LINEA_AV FROM TRASPASO_TEMPORAL 
				WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''' GROUP BY EMPRESA, NUMERO, LETRA, LINEA_AV
		
				-- INSERTAR DISTRIBU!D_ADIALBVE
				INSERT INTO [DISTRIYB].DBO.D_ADIALBVE (EJERCICIO, EMPRESA, NUMERO, LETRA, LINIA, OPERACION, TARIFA) 
					SELECT @EJERCICIO, EMPRESA, '''+@NUMERO+''', '''+@LETRAFAC+''', LINEA_AV, 1, @TARIFA FROM TRASPASO_TEMPORAL 
				WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''' GROUP BY EMPRESA, NUMERO, LETRA, LINEA_AV

				-- INSERTAR LOTES
				INSERT INTO [LOTES0YB].DBO.LTALBVE (EMPRESA, LETRA, NUMERO, FECHA, ARTICULO, LINIA, LOTE, UNIDADES, PERIODO, ALMACEN, CADUCIDAD, FECHA2, PESO, ASI)
					SELECT '''+@EMPRESA+''' AS EMPRESA, '''+@LETRAFAC+''' AS LETRA, '''+@NUMERO+''' AS NUMERO, @FECHA AS FECHA, T.ARTICULO, T.LINEA_AV AS LINIA, T.LOTE, T.UNIDADES, @EJERCICIO AS PERIODO, 
					@ALMACEN, T.CADUCIDAD, @FECHASTOCK, CASE WHEN COALESCE(T.PESO,''0.00'')='''' THEN ''0.00'' ELSE COALESCE(T.PESO,''0.00'') END AS PESO, '''' AS ASI FROM TRASPASO_TEMPORAL T INNER JOIN [LOTES0YB] .DBO.artlot A ON A.ARTICULO=T.ARTICULO COLLATE DATABASE_DEFAULT 
				WHERE T.EMPRESA='''+@EMPRESA+''' AND T.NUMERO='''+@PEDIDO+''' AND T.LETRA='''+@LETRA+''' AND T.LOTE!='''' AND A.LOTE=1

		--LOG8
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:8'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL9 = '		
				-- INSERTAR LOTES!D_ALBVEN2
				INSERT INTO [LOTES0YB].DBO.D_ALBVEN2 (EJERCICIO, EMPRESA, NUMERO, LETRA, LINIA) 
					SELECT @EJERCICIO, T.EMPRESA, '''+@NUMERO+''', '''+@LETRAFAC+''', T.LINEA_AV FROM TRASPASO_TEMPORAL T INNER JOIN [LOTES0YB] .DBO.artlot A ON A.ARTICULO=T.ARTICULO COLLATE DATABASE_DEFAULT
				WHERE T.EMPRESA='''+@EMPRESA+''' AND T.NUMERO='''+@PEDIDO+''' AND T.LETRA='''+@LETRA+''' AND A.LOTE=1 GROUP BY T.EMPRESA, T.NUMERO, T.LETRA, T.LINEA_AV 

				--  15/10/2020 HAROLD: PARA AGREGAR LAS ENTREGAS A CUENTA SE BORRAN LAS EXISTENTES
				DELETE from '+@GESTION+'.DBO.ENTREGAS WHERE EMPRESA='''+@EMPRESA+''' AND LETRA='''+@LETRAFAC+''' AND ALBARAN='''+@NUMERO+'''
			END


		-- Insertar CAJAS A FACTURAR en el detalle del albarán creado.
		IF '''+@LETRA+'''=''FR''
			BEGIN
			DECLARE @ARTCAJA CHAR(8), @LINCAJAS INT
			SET @ARTCAJA=''Z1''
			SET @LINCAJAS=(SELECT MAX(LINIA) FROM '+@GESTION+'.DBO.D_ALBVEN WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+''')
			IF EXISTS (SELECT * FROM '+@GESTION+'.DBO.D_ALBVEN WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+''' AND ARTICULO=@ARTCAJA)
					UPDATE '+@GESTION+'.DBO.D_ALBVEN SET UNIDADES=UNIDADES+CAST(@ETICAJAS AS DECIMAL(7)), CAJAS=CAJAS+CAST(@ETICAJAS AS DECIMAL(7)), IMPORTE=PRECIO*CAST(@ETICAJAS AS DECIMAL(7)), IMPORTEDIV=PRECIO*CAST(@ETICAJAS AS DECIMAL(7)), IMPORTEIVA=PRECIOIVA*CAST(@ETICAJAS AS DECIMAL(7)), IMPDIVIVA=PRECIOIVA*CAST(@ETICAJAS AS DECIMAL(7)), LINIA=@LINCAJAS+1 WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+''' AND ARTICULO=@ARTCAJA
			ELSE
				INSERT INTO '+@GESTION+'.DBO.D_ALBVEN (USUARIO, EMPRESA, NUMERO, ARTICULO, DEFINICION, UNIDADES, DTO1, DTO2, TIPO_IVA, COSTE, FECHA, PEDIDO, LINIA, CLIENTE, PRECIO, 
				IMPORTE, PRECIOIVA, IMPORTEIVA, CAJAS, FAMILIA,PRECIODIV, IMPORTEDIV, TIPO, COMISION, IMP_COM, PESO, DOC, DOC_NUM, DOC_LIN, DOC_UNID, VISTA, PVERDE, LETRA, 
				IMPDIVIVA, PREDIVIVA, FLACTUAL, FLANTERIOR, DOC_CAJA) 
				SELECT @USUARIO, '''+@EMPRESA+''', '''+@NUMERO+''', ART.CODIGO, ART.NOMBRE, CAST(@ETICAJAS AS DECIMAL(7)) AS UNIDADES,  0.00 AS DTO1, 0.00 AS DTO2, ART.TIPO_IVA, ART.COST_ULT1, @FECHA, '''' AS PEDIDO, @LINCAJAS+1 AS LINIA, @CLIENTE, PVP.PVP AS PRECIO, PVP.PVP*CAST(@ETICAJAS AS DECIMAL(7)) AS IMPORTE, PVP.PVP*(1+(IVA.IVA/100)) AS PRECIOIVA, (PVP.PVP*(1+(IVA.IVA/100)))*(CAST(@ETICAJAS AS DECIMAL(7))) AS IMPORTEIVA, CAST(@ETICAJAS AS DECIMAL(7)) AS CAJAS, ART.FAMILIA, PVP.PVP AS PRECIODIV, PVP.PVP*CAST(@ETICAJAS AS DECIMAL(7)) AS IMPORTEDIV, 0 AS TIPO, 0.00 AS COMISION, 0.00 AS IMP_COM, 0.00 AS PESO, 0 AS DOC, '''' AS DOC_NUM, 0 AS LIN_DOC, 0.00 AS UDS_DOC, 1 AS VISTA, 0.00 AS PVERDE, '''+@LETRAFAC+''', (PVP.PVP*(1+(IVA.IVA/100)))*(CAST(@ETICAJAS AS DECIMAL(7))) AS IMPDIVIVA, PVP.PVP*(1+(IVA.IVA/100)) AS PREDIVIVA, @FECHA AS FLACTUAL, @FECHA AS FLANTERIOR, 0.00 AS CAJAS_DOC		
				FROM '+@GESTION+'.DBO.ARTICULO ART INNER JOIN '+@GESTION+'.DBO.PVP PVP ON PVP.ARTICULO=ART.CODIGO LEFT JOIN '+@GESTION+'.DBO.TIPO_IVA IVA ON IVA.CODIGO=ART.TIPO_IVA WHERE ART.CODIGO=@ARTCAJA AND PVP.TARIFA=@TARIFA
			
			-- ACTUALIZAR STOCK CAJAS (ARTICULO Z1)	
			UPDATE '+@GESTION+'.DBO.STOCKS2 SET FINAL=FINAL-CAST(@ETICAJAS AS DECIMAL(7)), SALIDAS=SALIDAS+CAST(@ETICAJAS AS DECIMAL(7)) WHERE ARTICULO=@ARTCAJA AND EMPRESA='''+@EMPRESA+''' AND ALMACEN=@ALMACEN
			END

		--LOG9
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:9'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL10 = '		
		-- ****** PROCESOS A EJECUTAR CADA VEZ QUE SE INSERTAN DATOS
		-- ACTUALIZAR STOCKS2
		UPDATE '+@GESTION+'.DBO.STOCKS2 SET FINAL=S.FINAL-T.UNIDADES, SALIDAS=S.SALIDAS+T.UNIDADES 
		FROM '+@GESTION+'.DBO.STOCKS2 S 
		INNER JOIN (SELECT ARTICULO, ALMACEN, SUM(UNIDADES) AS UNIDADES, SUM(CAST(CASE WHEN COALESCE(PESO,''0.00'')='''' THEN ''0.00'' ELSE COALESCE(PESO,''0.00'') END AS DECIMAL(16,4))) AS PESO FROM Traspaso_Temporal
		WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''' 
		GROUP BY ARTICULO, ALMACEN) T ON T.ARTICULO=S.ARTICULO COLLATE DATABASE_DEFAULT AND T.ALMACEN=S.ALMACEN COLLATE DATABASE_DEFAULT

		-- ACTUALIZAR STOCKLOTES
		UPDATE [LOTES0YB].DBO.STOCKLOTES SET PESO=S.PESO-CASE WHEN COALESCE(T.PESO,''0.00'')='''' THEN ''0.00'' ELSE COALESCE(T.PESO,''0.00'') END, UNIDADES=S.UNIDADES-T.UNIDADES FROM [LOTES0YB].DBO.STOCKLOTES S 
			INNER JOIN Traspaso_Temporal T ON T.EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''' 
			AND T.ALMACEN=S.ALMACEN AND T.ARTICULO COLLATE DATABASE_DEFAULT=S.ARTICULO AND T.LOTE COLLATE DATABASE_DEFAULT=S.LOTE INNER JOIN [LOTES0YB] .DBO.artlot A ON A.ARTICULO=S.ARTICULO COLLATE DATABASE_DEFAULT WHERE A.LOTE=1

		-- ACTUALIZAR ARTICULO
		UPDATE '+@GESTION+'.dbo.articulo  SET fecha_ult=@FECHA FROM '+@GESTION+'.dbo.articulo A 
			INNER JOIN Traspaso_Temporal T ON T.ARTICULO COLLATE DATABASE_DEFAULT=A.CODIGO 
		WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+'''

		-- ACTUALIZAR IMPORTES D_ALBVEN 15/10/2020 HAROLD, ESTABA MALO IMPORTEDIV
		UPDATE '+@GESTION+'.DBO.D_ALBVEN SET 
			IMPORTE=ROUND((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIO ELSE PESO*PRECIO END)
				-((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIO ELSE PESO*PRECIO END)*(DTO1/100))
				-(((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIO ELSE PESO*PRECIO END)
				-(CASE WHEN PESO=0.00 THEN UNIDADES*PRECIO ELSE PESO*PRECIO END)*(DTO1/100))*(DTO2/100)),2), 
			IMPORTEIVA=ROUND((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIOIVA ELSE PESO*PRECIOIVA END)
				-((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIOIVA ELSE PESO*PRECIOIVA END)*(DTO1/100))
				-(((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIOIVA ELSE PESO*PRECIOIVA END)
				-(CASE WHEN PESO=0.00 THEN UNIDADES*PRECIOIVA ELSE PESO*PRECIOIVA END)*(DTO1/100))*(DTO2/100)),2), 
			IMPORTEDIV=ROUND((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIODIV ELSE PESO*PRECIODIV END)
				-((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIODIV ELSE PESO*PRECIODIV END)*(DTO1/100))
				-(((CASE WHEN PESO=0.00 THEN UNIDADES*PRECIODIV ELSE PESO*PRECIODIV END)
				-(CASE WHEN PESO=0.00 THEN UNIDADES*PRECIODIV ELSE PESO*PRECIODIV END)*(DTO1/100))*(DTO2/100)),2), 
			IMPDIVIVA=ROUND((CASE WHEN PESO=0.00 THEN UNIDADES*PREDIVIVA ELSE PESO*PREDIVIVA END)
				-((CASE WHEN PESO=0.00 THEN UNIDADES*PREDIVIVA ELSE PESO*PREDIVIVA END)*(DTO1/100))
				-(((CASE WHEN PESO=0.00 THEN UNIDADES*PREDIVIVA ELSE PESO*PREDIVIVA END)
				-(CASE WHEN PESO=0.00 THEN UNIDADES*PREDIVIVA ELSE PESO*PREDIVIVA END)*(DTO1/100))*(DTO2/100)),2) 
		WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+'''	

		--LOG10
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:10'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL11 = '		
		-- ACTUALIZAR PVERDE
		BEGIN
		IF @PVERDECLI=0
			UPDATE '+@GESTION+'.DBO.D_ALBVEN SET PVERDE=ROUND(CASE WHEN D.PESO=0.00 THEN D.UNIDADES*A.P_IMPORTE ELSE D.PESO*A.P_IMPORTE END,2) 
			FROM '+@GESTION+'.DBO.D_ALBVEN D INNER JOIN '+@GESTION+'.DBO.ARTICULO A ON A.CODIGO=D.ARTICULO AND A.PVERDE=1 
			WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+'''	 
		END

		-- ACTUALIZAR IMPORTES C_ALBVEN LA SUMA PARA TOTALDOC INCLIDA
		UPDATE '+@GESTION+'.DBO.C_ALBVEN SET IMPORTE=D.IMPORTE, IMPDIVISA=D.IMPDIVISA, TOTALDOC=0.00, 
		COSTE=D.COSTE, PESO=D.PESO, LITROS=D.LITROS FROM '+@GESTION+'.DBO.C_ALBVEN C 
		INNER JOIN (SELECT MAX(D.EMPRESA) AS EMPRESA, MAX(D.NUMERO) AS NUMERO, MAX(D.LETRA) AS LETRA, SUM(D.IMPORTE)+SUM(D.PVERDE) AS IMPORTE, 
		SUM(D.IMPORTEDIV)+SUM(D.PVERDE) AS IMPDIVISA, SUM(D.IMPORTEIVA) AS IMPORTEIVA, SUM(D.PVERDE) AS PVERDE, 
		SUM(CASE WHEN D.PESO!=0.00 THEN D.UNIDADES*D.COSTE ELSE D.PESO*D.UNIDADES END) AS COSTE, SUM(D.PESO) AS PESO, 
		SUM(D.UNIDADES*(CAST(CASE WHEN D.PESO=0.00 THEN CASE WHEN LTRIM(RTRIM(COALESCE(A.LITROS,'''')))='''' THEN ''0.00'' ELSE COALESCE(A.LITROS,'''') END ELSE ''0.00'' END AS DECIMAL(10,2)))) AS LITROS 
		FROM '+@GESTION+'.DBO.D_ALBVEN D LEFT JOIN '+@GESTION+'.DBO.ARTICULO A ON A.CODIGO=D.ARTICULO 
		WHERE D.EMPRESA='''+@EMPRESA+''' AND D.NUMERO='''+@NUMERO+''' AND D.LETRA='''+@LETRAFAC+''') D ON D.EMPRESA+D.NUMERO+D.LETRA=C.EMPRESA+C.NUMERO+C.LETRA 
		WHERE C.EMPRESA='''+@EMPRESA+''' AND C.NUMERO='''+@NUMERO+''' AND C.LETRA='''+@LETRAFAC+'''

		-- ACTUALIZAR COSTE ALBV_ADI
		UPDATE '+@GESTION+'.DBO.albv_adi SET COSTEDIV=A2.COSTE FROM '+@GESTION+'.DBO.albv_adi A1 
		INNER JOIN '+@GESTION+'.DBO.C_ALBVEN A2 ON A1.EMPRESA+A1.NUMERO+A1.LETRA=A2.EMPRESA+A2.NUMERO+A2.LETRA
		WHERE A1.EMPRESA='''+@EMPRESA+''' AND A1.NUMERO='''+@NUMERO+''' AND A1.LETRA='''+@LETRAFAC+'''

		UPDATE '+@GESTION+'.DBO.D_PEDIVE SET SERVIDAS=CASE WHEN (DPV.SERVIDAS+TT.UNIDADES)>DPV.UNIDADES THEN DPV.UNIDADES ELSE DPV.SERVIDAS+TT.UNIDADES END, CAJASERV=CASE WHEN (DPV.CAJASERV+TT.CAJAS)>DPV.CAJAS THEN DPV.CAJAS ELSE DPV.CAJASERV+TT.CAJAS END, LIBRE_4=CASE WHEN DPV.TIPO_IVA='''' THEN ''TRASPASADO'' ELSE '''' END
		FROM '+@GESTION+'.DBO.D_PEDIVE DPV 
		INNER JOIN (SELECT SESION, EMPRESA, NUMERO, LETRA, LINEA, SUM(UNIDADES) AS UNIDADES, SUM(coalesce(CAJAS,0.00)) AS CAJAS FROM TRASPASO_TEMPORAL 
		WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''' GROUP BY SESION, EMPRESA, LETRA, NUMERO, LINEA) TT 
		ON DPV.EMPRESA+DPV.LETRA+DPV.NUMERO+STR(DPV.LINIA)=TT.EMPRESA+TT.LETRA+TT.NUMERO+STR(TT.LINEA) COLLATE DATABASE_DEFAULT

		--LOG11
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:11'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL12 = '		
		-- BORRAR REGISTROS TABLA DE TRASPASOS
		-- Albert R. 21/07/2021 logs de traspasos temporales
		--insert into Traspaso_Temporal_log select * FROM Traspaso_Temporal WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+'''
		--El delete de la tabla temporal no se puede comentar, sino los traspasas parciales fallan.
		DELETE FROM Traspaso_Temporal WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+'''

		-- Calcular TOTALDOC
		-- 09/05/2020 Albert Rodriguez: Actualizar TotalDoc	
		-- 08/10/2020 Harold, cambios en totaldoc	
		-- 04/08/2021 Albert R, conviErto el campo para dejarlo solo con dos decimales, no redondeo porque se quito en su momento por descuadres y el fallo ahora es solo en los asientos y con estos se soluciona.
		SELECT @TOTALDOC = CONVERT(DECIMAL(16,2),sum(totimpiva)) from (SELECT sum( (D.IMPORTE+D.PVERDE)-((D.IMPORTE+D.PVERDE)*(C.PRONTO/100)) ) +
				round( sum(( (D.IMPORTE+D.PVERDE)-((D.IMPORTE+D.PVERDE)*(C.PRONTO/100)) )*(COALESCE(IVA.IVA,0.00)/100)),2) +
				round( sum(( (D.IMPORTE+D.PVERDE)-((D.IMPORTE+D.PVERDE)*(C.PRONTO/100)) )*(CASE WHEN CLI.RECARGO=1 THEN COALESCE(IVA.RECARG,0.00)/100 ELSE 0 END)),2) as totimpiva, IVA.iva
				FROM '+@GESTION+'.DBO.D_ALBVEN D JOIN '+@GESTION+'.DBO.C_ALBVEN C ON D.EMPRESA=C.EMPRESA AND D.LETRA=C.LETRA AND D.NUMERO=C.NUMERO
				LEFT JOIN '+@GESTION+'.DBO.TIPO_IVA IVA ON IVA.CODIGO=D.TIPO_IVA LEFT JOIN '+@GESTION+'.DBO.CLIENTES CLI ON CLI.CODIGO=C.CLIENTE
				WHERE d.EMPRESA='''+@EMPRESA+''' and d.LETRA='''+@LETRAFAC+''' and d.NUMERO='''+@NUMERO+''' group by c.empresa, c.numero, c.letra, IVA.iva) a

		-- PORTES
		/*SELECT @IMPPORTES=CASE WHEN @NOPORTES=1 THEN 0.00 WHEN @PORTCOMP>IMPORTE OR @PORTCOMP=0.00 THEN @VALPORTES ELSE 0.00 END FROM '+@GESTION+'.DBO.C_ALBVEN WHERE EMPRESA='''+@EMPRESA+''' and LETRA='''+@LETRAFAC+''' and NUMERO='''+@NUMERO+'''*/

		SELECT @IMPPORTES=CASE WHEN @NOPORTES=1 THEN 0.00 WHEN @PORTCOMP>TOTALPED OR @PORTCOMP=0.00 THEN @VALPORTES ELSE 0.00 END FROM '+@GESTION+'.DBO.C_PEDIVE WHERE EMPRESA='''+@EMPRESA+''' and LETRA='''+@LETRA+''' and NUMERO='''+@PEDIDO+'''

		-- Pedidos de a serie DI, Si al crear el nuevo albarán, ya existe un albarán que tiene ese número de pedido y ese albarán tiene portes, los portes no se insertarán en el nuevo documento
		IF '''+@LETRA+'''=''DI''
			BEGIN
				IF EXISTS (SELECT * FROM '+@GESTION+'.DBO.C_ALBVEN C INNER JOIN '+@GESTION+'.DBO.PORTES P ON P.EMPRESA=C.EMPRESA AND P.ALBARAN=C.NUMERO AND P.LETRA=C.LETRA WHERE LTRIM(PEDIDO)='''+LTRIM(RTRIM(@PEDIDO))+@LETRA+''' AND P.IMPORTE!=0.00)
					SET @IMPPORTES = 0.00
			END
				
		UPDATE '+@GESTION+'.DBO.C_ALBVEN SET IMPORTE=IMPORTE+@IMPPORTES, IMPDIVISA=IMPDIVISA+@IMPPORTES, TOTALDOC=@TOTALDOC+@IMPPORTES+ROUND((@IMPPORTES*((@IVAPORTES/100))),2)+ROUND((@IMPPORTES*(@RECPORTES/100)),2) WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+'''
		UPDATE '+@GESTION+'.DBO.PORTES SET IMPORTE=@IMPPORTES, IMPORTEDIV=@IMPPORTES WHERE EMPRESA='''+@EMPRESA+''' AND ALBARAN='''+@NUMERO+''' AND LETRA='''+@LETRAFAC+'''

		-- ACTUALIZAR DISTRYB C_ADIALBVEN
		UPDATE [DISTRIYB].DBO.C_ADIALBVE SET TOTALALB=C2.TOTALDOC FROM [DISTRIYB].DBO.C_ADIALBVE C1 
		INNER JOIN '+@GESTION+'.DBO.C_ALBVEN C2 ON @EJERCICIO=C1.EJERCICIO AND C2.EMPRESA=C1.EMPRESA AND C2.NUMERO=C1.NUMERO AND C2.LETRA=C1.LETRA 
		WHERE C2.EMPRESA='''+@EMPRESA+''' AND C2.NUMERO='''+@NUMERO+''' AND C2.LETRA='''+@LETRAFAC+'''

		--LOG12
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:12'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL13 = '		
		-- Insertar Entregas a cuenta	
		DECLARE @IMPORTEDIV NUMERIC(15,6), @CUENTA VARCHAR(10), @FECHA_POR smalldatetime, @LETRA_POR VARCHAR(2), @TOTENTREGAS NUMERIC(15,6)
		select null mykey, LINEA, IMPORTE, IMPORTEDIV, ASI, CUENTA, FECHA, LETRA into #mysec from '+@GESTION+'.DBO.entre_pv 
			WHERE EMPRESA='''+@EMPRESA+''' AND LETRA='''+@LETRA+''' AND NUMERO='''+@PEDIDO+''' order by LINEA
		SET @TOTENTREGAS=(SELECT SUM(IMPORTE) FROM #mysec)
		while (select count(*) from #mysec where isnull(mykey, 0)= 0 ) > 0 
		begin
			SET @LINIA=(SELECT MIN(LINEA) FROM #mysec where isnull(mykey, 0)= 0)
			SELECT @IMPORTE=IMPORTE, @IMPORTEDIV=IMPORTEDIV, @ASI=ASI, @CUENTA=CUENTA, @FECHA_POR=FECHA, @LETRA_POR=LETRA from #mysec WHERE LINEA=@LINIA
			IF @TOTALDOC=@IMPORTE OR @TOTALDOC=@TOTENTREGAS
				BEGIN
					INSERT INTO '+@GESTION+'.DBO.ENTREGAS (EMPRESA, ALBARAN, LINEA, IMPORTE, IMPORTEDIV, ASI, CUENTA, FECHA, LETRA, PED_LETRA, PED_NUM)
						SELECT EMPRESA, '''+@NUMERO+''' AS NUMERO, LINEA, IMPORTE, IMPORTEDIV, ASI, CUENTA, FECHA, '''+@LETRAFAC+''', '''+@LETRA+''' AS PED_LETRA, '''+@PEDIDO+''' AS PED_NUM 
						FROM '+@GESTION+'.DBO.entre_pv 
						WHERE EMPRESA='''+@EMPRESA+''' AND LETRA='''+@LETRA+''' AND NUMERO='''+@PEDIDO+''' order by LINEA
					UPDATE #mysec SET mykey=1
					set @TOTALDOC=0
				END
			ELSE
				BEGIN
					while (@TOTALDOC>0 )
					begin 
						DECLARE @FINPROG INT=0
						if @TOTALDOC>=@IMPORTE
							begin
							INSERT INTO '+@GESTION+'.DBO.ENTREGAS (EMPRESA, ALBARAN, LINEA, IMPORTE, IMPORTEDIV, ASI, CUENTA, FECHA, LETRA, PED_LETRA, PED_NUM)
								VALUES('''+@EMPRESA+''', '''+@NUMERO+''', @LINIA, @IMPORTE, @IMPORTEDIV, @ASI, @CUENTA, @FECHA_POR, '''+@LETRAFAC+''', @LETRA_POR, '''+@PEDIDO+''')
							end
						else
							begin
							INSERT INTO '+@GESTION+'.DBO.ENTREGAS (EMPRESA, ALBARAN, LINEA, IMPORTE, IMPORTEDIV, ASI, CUENTA, FECHA, LETRA, PED_LETRA, PED_NUM)
									VALUES('''+@EMPRESA+''', '''+@NUMERO+''', @LINIA, @TOTALDOC, @TOTALDOC, @ASI, @CUENTA, @FECHA_POR, '''+@LETRAFAC+''', @LETRA_POR, '''+@PEDIDO+''')
							end 
						SET @TOTALDOC=@TOTALDOC-@IMPORTE	
						if @TOTALDOC>0.09
							update #mysec set mykey=1 where LINEA=@LINIA
						ELSE
							begin
								set @TOTALDOC=0
								update #mysec set mykey=1
							end
						SET @LINIA=(SELECT MIN(LINEA) FROM #mysec where isnull(mykey, 0)= 0)
						SELECT @IMPORTE=IMPORTE, @IMPORTEDIV=IMPORTEDIV, @ASI=ASI, @CUENTA=CUENTA, @FECHA_POR=FECHA, @LETRA_POR=LETRA from #mysec WHERE LINEA=@LINIA
					end
				END
		end 
		drop table #mysec

		--LOG13
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:13'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		SET @cSQL14 = '		
		-- Albert R. 09/07/2021. Creo un procedimiento para enviar los registros de impresión a la tabla MI_Impresión. Ahora los registros de impresión se crean al final de la creación del documento y si se genera una factura se crean al final de la factura.
		declare @NUM_FA varchar(10)=''''
		
		-- 16/10/2020 si tiene Facturación Directa generar factura de inmediato
		IF @Factdirv=1 AND '+CONVERT(CHAR(1),@imprimir)+'=1
			exec [CrearFA] '''+@EMPRESA+''', '''+@LETRAFAC+''', '''+@NUMERO+''', @FECHA_FAC, @terminal_new, '+CONVERT(CHAR(1),@imprimir)+','+CONVERT(CHAR(1),@imp_eti)+', @ETICAJAS, @bultos, '+LTRIM(rTRIM(CONVERT(CHAR(3),@nEti)))+'
		ELSE
			BEGIN
				IF '''+@LETRAFAC+'''=''ET''
					--SET @terminal_new=''DESKTOP-NEKGMLE''
					SET @terminal_new=''pc-nave-nueva''
				exec [Imprimir_Docs] '''+@EMPRESA+''', '''+@LETRAFAC+''', '''+@NUMERO+''', @terminal_new, @num_fa, '+CONVERT(CHAR(1),@imprimir)+','+CONVERT(CHAR(1),@imp_eti)+', @ETICAJAS, @bultos, '+LTRIM(RTRIM(CONVERT(CHAR(3),@nEti)))+'
			END

		--LOG14
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES (''Emp: ''+ltrim(rtrim('''+@EMPRESA+'''))+'' / Numero: ''+ltrim(Rtrim('''+@PEDIDO+'''))+'' / Letra: ''+ltrim(Rtrim('''+@LETRA+'''))+'' / Log:14'',GETDATE(), '''+@EMPRESA+''', '''+@PEDIDO+''', '''+@LETRA+''')
		'
		--print @cSQL12
		EXEC(@cSQL+@cSQL2+@cSQL3+@cSQL4+@cSQL5+@cSQL6+@cSQL7+@cSQL8+@cSQL9+@cSQL10+@cSQL11+@cSQL12+@cSQL13+@cSQL14)

						
		-- MARCAR COMO TRASPASADO SI SE TRASPASAN TODAS LAS LINEAS
		-- Hemos creado una tabla intermedia para que la aplicación sepa si tiene que marcar el pedido como traspasado o no.
		set @cSQL = '
		DECLARE @PENDIENTE BIT
		SET @PENDIENTE=(SELECT CASE WHEN SUM(UNIDADES)-SUM(SERVIDAS)<=0.00 THEN 1 ELSE 0 END AS PENDIENTE FROM '+@GESTION+'.DBO.D_PEDIVE WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+''')
		IF @PENDIENTE=1
			BEGIN
				--UPDATE '+@GESTION+'.DBO.C_PEDIVE SET TRASPASADO=1 WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='''+@PEDIDO+''' AND LETRA='''+@LETRA+'''

				INSERT INTO TRASPASAR_PEDIDO (EMPRESA, LETRA, NUMERO)
				VALUES ('''+@EMPRESA+''','''+@LETRA+''','''+@PEDIDO+''')
			END
		'
		EXEC(@cSQL)

		--LOG15
		INSERT INTO LOG_CREARAV (VALOR, FECHA, EMPRESA, NUMERO, LETRA)
		VALUES ('Emp: '+ltrim(rtrim(@EMPRESA))+' / Numero: '+ltrim(Rtrim(@PEDIDO))+' / Letra: '+ltrim(Rtrim(@LETRA))+' / Log:15',GETDATE(), @EMPRESA, @PEDIDO, @LETRA)
		
		-- Borrar log
		delete from LOG_CREARAV WHERE LTRIM(rTRIM(EMPRESA))=LTRIM(RTRIM(@EMPRESA)) AND LTRIM(rTRIM(NUMERO))=LTRIM(RTRIM(@PEDIDO)) AND LTRIM(rTRIM(LETRA))=LTRIM(rTRIM(@LETRA))

		-- Devolvemos el número de albarán generado
		SELECT @respuestaSP=@NUMERO
		
				-- Quitamos en uso la tabla en EnUso para que no se pueda ejecutar el procedimiento CrearAV desde dos terminales hasta que el primero que lo ha ejecutado acabe.
		UPDATE EnUso set Enuso=0, fecha=getdate()  WHERE TIPO='PROCEDURE'

	COMMIT TRAN
	RETURN -1

END TRY

BEGIN CATCH 
	ROLLBACK TRAN
	DECLARE @ErrorMessage NVARCHAR(4000), @ErrorSeverity INT, @ErrorState INT, @ErrorNumber INT, @ErrorProcedure NVARCHAR(50), @ErrorLine INT

    SELECT  @ErrorNumber = ERROR_NUMBER(),  @ErrorProcedure = ERROR_PROCEDURE(), @ErrorLine = ERROR_LINE(), @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(),  @ErrorState = ERROR_STATE();  

	-- Quitamos en uso la tabla en EnUso para que no se pueda ejecutar el procedimiento CrearAV desde dos terminales hasta que el primero que lo ha ejecutado acabe.
	UPDATE dbo.EnUso set Enuso=0, fecha=getdate()  WHERE TIPO='PROCEDURE'
	
	SET @Mensaje='","ERROR":"NO se ha podido crear el albarán'
	PRINT(@ErrorMessage)
	SELECT @Mensaje AS JAVASCRIPT

    SELECT @respuestaSP='ERROR!'
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pDatos]...';


GO
CREATE PROCEDURE [dbo].[pDatos] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare	@modo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
		,	@articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')

	declare	@usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
		,	@rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EMPRESA char(2), @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select @EMPRESA=Empresa, @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------	
	
	if @modo='dameUNICAJA' EXEC('select (select isnull(UNICAJA,0) from ['+@GESTION+'].[dbo].articulo where CODIGO='''+@articulo+''') as JAVASCRIPT')

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pDesbloquearPedido]...';


GO
CREATE PROCEDURE [dbo].[pDesbloquearPedido] @parametros varchar(max)	
AS
BEGIN
	SET NOCOUNT ON;
	-----------------------------------------------------------------------------------------------
	-- Variables de Configuración 
	-----------------------------------------------------------------------------------------------
	declare @EJERCICIO char(4)
	declare @GESTION char(6)
	declare @COMUN char(8)
	declare @LOTES char(8)
	select @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-----------------------------------------------------------------------------------------------

	BEGIN TRY

		declare @numero varchar(50) = isnull((select JSON_VALUE(@parametros,'$.numero')),'')
			,	@serie varchar(50) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
			,	@ruta varchar(50) = isnull((select JSON_VALUE(@parametros,'$.ruta')),'')
		declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			,	@rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')

		declare @laClave varchar(50) = concat(
												  (select Ejercicio from Configuracion)
												, (select Empresa from Configuracion)
												, @numero
												, @serie
										) 

		EXEC('delete ['+@COMUN+'].[DBO].[en_uso] where CLAVE='''+@laClave+'''')

		return -1 
	END TRY
	BEGIN CATCH
		DECLARE @CatchError NVARCHAR(MAX)
		SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
		select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
		RAISERROR(@CatchError,12,1)
		RETURN 0
	END CATCH
END
GO
PRINT N'Creando Procedimiento [dbo].[pEditarLineaTraspaso]...';


GO
CREATE PROCEDURE [dbo].[pEditarLineaTraspaso] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON

	-- Parámetros JSON ------------------------------------------------------------------------------
	declare	  @pedido varchar(50) = isnull((select JSON_VALUE(@parametros,'$.pedido')),'')
			, @serie varchar(10) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
			, @articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
			, @unidades varchar(20) = isnull((select JSON_VALUE(@parametros,'$.unidades')),'')
			, @linea varchar(10) = isnull((select JSON_VALUE(@parametros,'$.linea')),'0')
			, @cajas varchar(10) = isnull((select JSON_VALUE(@parametros,'$.cajas')),'0')
			, @peso varchar(10) = isnull((select JSON_VALUE(@parametros,'$.peso')),'0')
			, @servidas varchar(10) = isnull((select JSON_VALUE(@parametros,'$.servidas')),'0')
			, @traspaso varchar(10) = isnull((select JSON_VALUE(@parametros,'$.traspaso')),'0')
			, @pesoPend varchar(10) = isnull((select JSON_VALUE(@parametros,'$.pesoPend')),'0')
			, @basculaEnUso varchar(10) = isnull((select JSON_VALUE(@parametros,'$.basculaEnUso')),'0')
	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-------------------------------------------------------------------------------------------------
	-- variables App
	declare @laSesion varchar(50) = @usuario
	declare @nu varchar(50) = 
	replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 
	declare @empresa varchar(2) = (select Empresa from Configuracion)
	---------------------------------------------------------------------------------------------------------------
	
	declare @respuesta varchar(max) = ''

	-- Si existen registros en la tabla Traspaso_Temporal los devolvemos y salimos del procedimiento
	declare @existeTraspTemp varchar(max) = 
	(select 1 as existeTraspTemp, * from Traspaso_Temporal where concat(empresa,numero,letra,articulo)=concat(@empresa,@pedido,@serie,@articulo)
	FOR JSON AUTO, INCLUDE_NULL_VALUES)
	if LEN(@existeTraspTemp)>0 BEGIN select @existeTraspTemp as JAVASCRIPT return -1 END


	-- báscula predeterminada del usuario
	declare @basculaUsuario varchar(50) = (select bascula from usuarios_basculas where usuario=@usuario and predet=1)

	-- Obtener básculas
	declare @bsc1img varchar(50) = 'btnBascula01_Off.png'
		,	@bsc2img varchar(50) = 'btnBascula02_Off.png'
		,	@bsc3img varchar(50) = 'btnBascula03_Off.png'
		,	@bsc4img varchar(50) = 'btnBascula04_Off.png'
		,	@bascCont int = 0;

	declare @activa bit
	DECLARE elCursor CURSOR FOR	select activa from basculas
	OPEN elCursor
	FETCH NEXT FROM elCursor INTO @activa
	WHILE (@@FETCH_STATUS=0) BEGIN
		set @bascCont = @bascCont + 1

		if @bascCont=1 and @activa=1 BEGIN 
			if @basculaEnUso=1 set @bsc1img='btnBascula01_Selected.png' else set @bsc1img='btnBascula01_Activa.png'
		END
		if @bascCont=2 and @activa=1 BEGIN 
			if @basculaEnUso=2 set @bsc2img='btnBascula02_Selected.png' else set @bsc2img='btnBascula02_Activa.png'
		END
		if @bascCont=3 and @activa=1 BEGIN 
			if @basculaEnUso=3 set @bsc3img='btnBascula03_Selected.png' else set @bsc3img='btnBascula03_Activa.png'
		END
		if @bascCont=4 and @activa=1 BEGIN 
			if @basculaEnUso=4 set @bsc4img='btnBascula04_Selected.png' else set @bsc4img='btnBascula04_Activa.png'
		END
		
		FETCH NEXT FROM elCursor INTO @activa
	END	CLOSE elCursor deallocate elCursor

	-- tipo de conexión de báscula
	declare @verSpnTipoBascula varchar(50) = (select tipoConexion from basculas where bascula=@basculaEnUso)


	-- Trabajo CON lotes
	set @respuesta = (
		select isnull(
			(
				select	* 
					,	0 as existeTraspTemp
					,	(select NOMBRE from varticulos where CODIGO=@articulo) as descripcion
					,	isnull(@verSpnTipoBascula,'') as verSpnTipoBascula
					,	isnull(@bsc1img,'') as bsc1img
					,	isnull(@bsc2img,'') as bsc2img
					,	isnull(@bsc3img,'') as bsc3img
					,	isnull(@bsc4img,'') as bsc4img
				from vBuscarLotes
				where NUMERO=@pedido and ARTICULO=@articulo and LINIA=@linea and Llote is not null
				ORDER BY Lcaducidad asc 
				for JSON AUTO, INCLUDE_NULL_VALUES
			)
		,'[]')
	)

	if LEN(@respuesta)>3 BEGIN select @respuesta as JAVASCRIPT return -1 END


	-- Trabajo SIN lotes
	set @respuesta = (
		select isnull(
			(
				select	* 
					,	2 as existeTraspTemp 
					,	isnull(@verSpnTipoBascula,'') as verSpnTipoBascula
					,	isnull(@bsc1img,'') as bsc1img
					,	isnull(@bsc2img,'') as bsc2img
					,	isnull(@bsc3img,'') as bsc3img
					,	isnull(@bsc4img,'') as bsc4img
				from vCajasOunidades 
				where NUMERO=@pedido and LETRA=@serie and ARTICULO=@articulo and LINIA=@linea 
				for JSON AUTO, INCLUDE_NULL_VALUES
			)
		,'[]')
	)
	select @respuesta as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pEmpresas]...';


GO
CREATE PROCEDURE [dbo].[pEmpresas] @parametros varchar(max)
AS
BEGIN TRY
	declare @comun varchar(50) = (select JSON_VALUE(@parametros,'$.comun'))


	DECLARE @respuesta varchar(max)=''	
	DECLARE @sql nvarchar(max)
	declare @ejercicio varchar(10)
	declare @codigo varchar(50)
	declare @nombre varchar(100)

	-- obtener ejercicio predeterminado
	declare @SentenciaSQL varchar(max) = 'SELECT ltrim(rtrim(RUTA)) as RUTA INTO ##pEmpresasTMP1 FROM ['+@comun+'].dbo.ejercici where predet=1'
	EXEC (@SentenciaSQL)
	SET @ejercicio = (SELECT RUTA FROM ##pEmpresasTMP1)
	DROP TABLE ##pEmpresasTMP1
	
	-- Obtener empresas
	set @sql = '
		select CODIGO, replace(ltrim(rtrim(NOMBRE)),'''''''',''-'') as NOMBRE
		into ##pEmpresasTMP2
		from ['+@ejercicio+'].dbo.empresa where TIPO=''Normal''		
	'
	EXEC (@sql)

	DECLARE CUREmpresas CURSOR FOR
	select CODIGO, NOMBRE from ##pEmpresasTMP2
	OPEN CUREmpresas
	FETCH NEXT FROM CUREmpresas INTO @codigo, @nombre
	WHILE (@@FETCH_STATUS=0) BEGIN
		set @respuesta = @respuesta + '{"codigo":"'+@codigo+'","nombre":"'+@nombre+'"}'
	FETCH NEXT FROM CUREmpresas INTO @codigo, @nombre 
	END	CLOSE CUREmpresas deallocate CUREmpresas

	drop table ##pEmpresasTMP2

	select '{"empresas":['+isnull(replace(@respuesta,'}{','},{'),'')+']}' as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select @CatchError as JAVASCRIPT
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pIdiomas]...';


GO
CREATE PROCEDURE [dbo].[pIdiomas] @parametros varchar(max)=''
AS
BEGIN TRY
	declare   @modo varchar(50)		  = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
			, @idioma varchar(50)	  = isnull((select JSON_VALUE(@parametros,'$.idioma')),'')
			, @nombre varchar(100)	  = isnull((select JSON_VALUE(@parametros,'$.nombre')),'')
			, @tipo   varchar(50)	  = isnull((select JSON_VALUE(@parametros,'$.tipo')),'')
			, @tipoNombre varchar(50) = isnull((select JSON_VALUE(@parametros,'$.tipoNombre')),'')
			, @report varchar(100)	  = isnull((select JSON_VALUE(@parametros,'$.report')),'')

	if @modo='actualizar' BEGIN
		if not exists (select * from [dbo].[ConfigIdiomaReport] where idioma=@idioma and tipo=@tipo and report=@report)
		insert into [dbo].[ConfigIdiomaReport] (idioma,nombre,tipo,report) 
		values (@idioma, @nombre, @tipo+' - '+@tipoNombre, @report)

		-- actualizar Idiomas_Idiomas
		if not exists (select * from [dbo].Idiomas_Idiomas where Codigo=@idioma)
		insert into [dbo].Idiomas_Idiomas (Codigo,Nombre) values (@idioma,@nombre)

		-- actualizar Idiomas_Tipos
		if not exists (select * from [dbo].Idiomas_Tipos where Codigo=@tipo)
		insert into [dbo].Idiomas_Tipos (Codigo,Nombre) values (@tipo,@tipoNombre)
	END

	if @modo='eliminar' BEGIN
		delete [ConfigIdiomaReport] where idioma=@idioma and tipo=@tipo and report=@report
	END

	select isnull((select * from ConfigIdiomaReport for JSON AUTO, INCLUDE_NULL_VALUES),'[]') as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pImpresion]...';


GO
CREATE PROCEDURE [dbo].[pImpresion] @parametros varchar(max)=''
AS
BEGIN TRY
	declare   @modo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
			, @tipo varchar(50)	= isnull((select JSON_VALUE(@parametros,'$.tipo')),'')
			, @numero varchar(50)	= isnull((select JSON_VALUE(@parametros,'$.numero')),'')
			, @empresa varchar(50)	= isnull((select JSON_VALUE(@parametros,'$.empresa')),'')
			, @letra varchar(50)	= isnull((select JSON_VALUE(@parametros,'$.letra')),'')


	if @modo='reimpresion' BEGIN
		update MI_ImpresionPDF set report=0, [log]=NULL
		where id= (
					select top 1 id from MI_ImpresionPDF 
					where concat(tipo,ltrim(rtrim(crNumero)),crEmpresa,crLetra)
						= concat(@tipo,ltrim(rtrim(@numero)),@empresa,@letra)
					order by fechaInsertUpdate desc
				)
	END

	select 'OK' as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() 
					+ char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select 'KO' + char(13) + @CatchError as JAVASCRIPT
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pImprimir_Docs]...';


GO
CREATE PROCEDURE [dbo].[pImprimir_Docs]
@empresa varchar(2) = '', 
@letra varchar(2) = '',
@numero varchar(10)= '',
@terminal varchar(50),
@num_fa varchar (10) ='',
@imprimir INT,
@imp_eti INT,
@ETICAJAS int,
@bultos VARCHAR (7),
@nEti INT,
@respuestaSP VARCHAR(100)='' output

AS

BEGIN TRY
				
	DECLARE @Mensaje varchar(max), @MensajeError varchar(max)

	BEGIN
		if @imprimir=1 or @imp_eti=1
			-- Albert R. 23/07/12 - de momento la serie DI no imrpimirá albaranes
			if @letra='DI'
				SET @imprimir=0
			begin
				if @LETRA='DI'
					BEGIN
						INSERT INTO MI_Impresion (terminal, empresa, numero, letra, factura, imprimir, imp_eti, bultos)  
						VALUES (@terminal, @empresa, @numero, @letra, @num_fa, @imprimir, @imp_eti, @ETICAJAS)
					END
				else 
					BEGIN
						INSERT INTO MI_Impresion (terminal, empresa, numero, letra, factura, imprimir, imp_eti, bultos)  
						VALUES (@terminal, @empresa, @numero, @letra, @num_fa, @imprimir, @imp_eti, @bultos)
					END
			end

	end	

END TRY
				
BEGIN CATCH 
	CLOSE Employee_Cursor
	DEALLOCATE Employee_Cursor 

	DECLARE @ErrorMessage NVARCHAR(4000);  
	DECLARE @ErrorSeverity INT;  
	DECLARE @ErrorState INT;  

	SELECT   
		@ErrorMessage = ERROR_MESSAGE(),  
		@ErrorSeverity = ERROR_SEVERITY(),  
		@ErrorState = ERROR_STATE();  

	RAISERROR (@ErrorMessage, -- Message text.  
				@ErrorSeverity, -- Severity.  
				@ErrorState -- State.  
				);  
	SET @Mensaje='","ERROR":"NO se ha podido crear el albarán'
	--PRINT(@Mensaje)
	SELECT @Mensaje AS JAVASCRIPT

END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pMerlos]...';


GO
CREATE PROCEDURE [dbo].[pMerlos] @parametros varchar(max)
AS
SET NOCOUNT ON
BEGIN TRY
	declare @sp varchar(50) = isnull((select JSON_VALUE(@parametros,'$.sp')),'')

	EXEC ('exec ' + @sp + ' '''+@parametros+''' ')

	return -1
END TRY
BEGIN CATCH
	DECLARE @Message varchar(MAX) = CONCAT('{"error":"',ERROR_MESSAGE(),'","sp","'+@sp+'","parametros","'+@parametros+'"}')
		,   @Severity int = ERROR_SEVERITY(), @State smallint = ERROR_STATE() 
	RAISERROR (@Message, @Severity, @State)
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pMerlosUsuarios]...';


GO
CREATE PROCEDURE [dbo].[pMerlosUsuarios] @parametros varchar(max)
AS
SET NOCOUNT ON
BEGIN TRY	
	declare @modo			varchar(50)  = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
		,	@error			varchar(max) = ''
		,	@datos			varchar(max) = ''
		,	@usuario		varchar(50)  = isnull((select JSON_VALUE(@parametros,'$.usuario')),'')
		,	@ImpDocu		varchar(100) = isnull((select JSON_VALUE(@parametros,'$.ImpDocu')),'')
		,	@ImpEti			varchar(100) = isnull((select JSON_VALUE(@parametros,'$.ImpEti')),'')
		,	@ReportAlb		varchar(100) = isnull((select JSON_VALUE(@parametros,'$.ReportAlb')),'')
		,	@ReportFra		varchar(100) = isnull((select JSON_VALUE(@parametros,'$.ReportFra')),'')
		,	@ReportEti		varchar(100) = isnull((select JSON_VALUE(@parametros,'$.ReportEti')),'')
		,	@BasculaPredet	varchar(100) = isnull((select JSON_VALUE(@parametros,'$.BasculaPredet')),'')


	if @modo='dameConfiguracion' BEGIN
		select isnull(	
			( 
				select * from (
					select  b.usuario, b.bascula, b.predet
						,	c.impDocu, c.impEti
						,	d.reportAlbaran, d.reportEtiquetas, d.reportFactura
					from usuarios_basculas b
					left join usuarios_impresoras c on c.usuario=b.usuario
					left join usuarios_reports d	on d.usuario=b.usuario
					where b.usuario=@usuario
				)a
				for JSON AUTO,INCLUDE_NULL_VALUES 
			)
		,'[]') as JAVASCRIPT
	END


	if @modo='guardarDatos' BEGIN
		-- básculas
		if not exists (select * from usuarios_basculas where usuario=@usuario)
			insert into usuarios_basculas (usuario,bascula,predet) values (@usuario,@BasculaPredet,1)
		else update usuarios_basculas set bascula=@BasculaPredet where usuario=@usuario
		-- impresoras
		if not exists (select * from usuarios_impresoras where usuario=@usuario)
			insert into usuarios_impresoras (usuario,impDocu,impEti) values (@usuario,@ImpDocu,@ImpEti)
		else update usuarios_impresoras set impDocu=@ImpDocu, impEti=@ImpEti where usuario=@usuario
		-- reports
		if not exists (select * from usuarios_reports where usuario=@usuario)
			insert into usuarios_reports (usuario, reportAlbaran, reportEtiquetas, reportFactura) 
			values (@usuario, @ReportAlb, @ReportEti, @ReportFra)
		else update usuarios_reports 
			 set usuario=@usuario, reportAlbaran=@ReportAlb, reportEtiquetas=@ReportEti, reportFactura=@ReportFra 
			 where usuario=@usuario

		select 'OK' as JAVASCRIPT
	END


	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
	select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pObjetoDatos]...';


GO
CREATE PROCEDURE [dbo].[pObjetoDatos] @parametros varchar(max)=''
AS
BEGIN TRY
	declare   @objeto varchar(50) = (select JSON_VALUE(@parametros,'$.objeto'))

	if @objeto='Idiomas' BEGIN
		select isnull((select ltrim(rtrim(Codigo)) as Codigo, ltrim(rtrim(Nombre)) as Nombre from Idiomas_Idiomas for JSON AUTO,INCLUDE_NULL_VALUES),'[]') as JAVASCRIPT
	END

	if @objeto='Tipos' BEGIN
		select isnull((select ltrim(rtrim(Codigo)) as Codigo, ltrim(rtrim(Nombre)) as Nombre from Idiomas_Tipos for JSON AUTO,INCLUDE_NULL_VALUES),'[]') as JAVASCRIPT
	END

	RETURN -1
END TRY

BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pPedidos]...';


GO
CREATE PROCEDURE [dbo].[pPedidos] ( @parametros varchar(max) )
AS
SET NOCOUNT ON
BEGIN TRY	
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare @modo     varchar(50) = isnull((select JSON_VALUE(@parametros,'$.modo')),'')
		,	@Empresa  char(2)     = isnull((select JSON_VALUE(@parametros,'$.Empresa')),'')
		,	@Numero	  varchar(20) = isnull((select JSON_VALUE(@parametros,'$.Numero')),'')
		,	@Serie    varchar(10) = isnull((select JSON_VALUE(@parametros,'$.Serie')),'')
		,	@Ruta     varchar(10) = isnull((select JSON_VALUE(@parametros,'$.Ruta')),'')
		,	@Almacen  varchar(10) = isnull((select JSON_VALUE(@parametros,'$.Almacen')),'')
		,	@articulo varchar(10) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
		,	@linia    varchar(10) = isnull((select JSON_VALUE(@parametros,'$.linia')),'')
		,	@cajas    varchar(10) = isnull((select JSON_VALUE(@parametros,'$.cajas')),'0')
		,	@uds      varchar(10) = isnull((select JSON_VALUE(@parametros,'$.uds')),'0')
		,	@peso     varchar(10) = isnull((select JSON_VALUE(@parametros,'$.peso')),'0.000')
		,	@pedidoActivo  varchar(50) = isnull((select JSON_VALUE(@parametros,'$.pedidoActivo')),'')
		,	@error   varchar(max) = ''
		,	@datos   varchar(max) = ''
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select  @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------	
	-- variables
	declare @sql varchar(max)
	-------------------------------------------------------------------------------------------------


	if @modo='dameLineas' BEGIN
		set @datos =	isnull((
							select a.*
								,  art.UNICAJA, case when ltrim(rtrim(art.PESO))='' then '0' else ltrim(rtrim(art.PESO)) end as PESO
								,  vas.ArticuloStock
							from v_d_pedive a
							inner join vArticulos art on art.CODIGO=a.articulo
							left join  vArticulosStock vas on vas.CODIGO=a.articulo and vas.almacen=@Almacen
							where a.empresa COLLATE Modern_Spanish_CS_AI=@Empresa 
								  and ltrim(rtrim(a.numero)) COLLATE Modern_Spanish_CS_AI=ltrim(rtrim(@Numero)) 
								  and a.letra COLLATE Modern_Spanish_CS_AI=@Serie 
								  and a.servidas=0
								  and a.articulo COLLATE Modern_Spanish_CS_AI not in (
										select articulo from Traspaso_Temporal
										where  empresa COLLATE Modern_Spanish_CS_AI=@Empresa
											and ltrim(rtrim(numero)) COLLATE Modern_Spanish_CS_AI=ltrim(rtrim(@Numero)) 
											and letra   COLLATE Modern_Spanish_CS_AI=@Serie
								  )
							for JSON AUTO,INCLUDE_NULL_VALUES
						),'[]')
	END


	if @modo='intercambiarArticulo' BEGIN 
		declare @articuloAnterior varchar(20) = isnull((select JSON_VALUE(@parametros,'$.articuloAnterior')),'')
		declare @udsAnterior numeric(15,6) = cast(isnull((select JSON_VALUE(@parametros,'$.udsAnterior')),'0.00') as numeric(15,6))
		declare @articuloNuevo varchar(20) = isnull((select JSON_VALUE(@parametros,'$.articuloNuevo')),'')
		declare @linea varchar(10) = isnull((select JSON_VALUE(@parametros,'$.linea')),'')

		-- calcular cajas y peso y stock para el @articuloNuevo
		declare @articuloNuevoStock numeric(15,6)
		select @articuloNuevoStock=ArticuloStock from vArticulosStock where CODIGO=@articuloNuevo and almacen=@Almacen
		if @articuloNuevoStock<0.000001 BEGIN select 'Error! - El artículo no tiene Stock!' as JAVASCRIPT return -1 END
		if @articuloNuevoStock<@udsAnterior BEGIN select 'Error! - El artículo no tiene suficiente Stock!' as JAVASCRIPT return -1 END
		
		declare @nuevoCajas int=0
		declare @nuevoUNICAJA int = (select isnull(UNICAJA,0) from vArticulos where CODIGO=@articuloNuevo)
		if @nuevoUNICAJA>0 set  @nuevoCajas=@udsAnterior/@nuevoUNICAJA

		declare @nuevoPeso numeric(15,6)=0.000
		declare @nuevoArtPeso numeric(15,6) = (select case when PESO is null then '0.000' 
											when PESO='' then '0.000' else PESO END from vArticulos where CODIGO=@articuloNuevo)
		if @nuevoArtPeso>0 set  @nuevoPeso=@udsAnterior*@nuevoArtPeso


		declare @param varchar(1000) =	concat('{"numero":"',@Numero,'","linea":"',@linea,'","letra":"',@Serie,'"'
										,',"articulo":"',@articuloNuevo,'","uds":"',@udsAnterior,'"'
										,',"peso":"',@nuevoPeso,'","tarifa":""}')
		begin try EXEC pCalculosPV @param end try begin catch select 'Error! - EXEC pCalculosPV @param: '+@param  as JAVASCRIPT return -1 end catch 

		-- actualizar la linea
		set @sql = '
		update ['+@GESTION+'].[dbo].d_pedive
		set ARTICULO='''+@articuloNuevo+''', DEFINICION=(select NOMBRE from vArticulos where CODIGO='''+@articuloNuevo+''')
			, CAJAS='+cast(@nuevoCajas as varchar(50))+', UNIDADES='+cast(@udsAnterior as varchar(50))+', PESO='+cast(@nuevoPeso as varchar(50))+'
		where EMPRESA='''+@Empresa+''' and NUMERO='''+@Numero+''' and LETRA='''+@Serie+''' and ARTICULO='''+@articuloAnterior+''' and LINIA='+@linea+'
		'
		EXEC(@sql)

		select CONCAT('{"cajas":"',@nuevoCajas,'","uds":"',@udsAnterior,'","peso":"',@nuevoPeso,'"}')  as JAVASCRIPT
	END


	if @modo='modificarPedido' BEGIN
		declare @valor varchar(1000) 
		declare cur CURSOR for select [value] from openjson(@parametros,'$.lineasModificadas')
		OPEN cur FETCH NEXT FROM cur INTO @valor
		WHILE (@@FETCH_STATUS=0) BEGIN

			set @linia = isnull((select JSON_VALUE(@valor,'$.linea')),'')
			set @articulo = isnull((select JSON_VALUE(@valor,'$.articulo')),'')
			set @cajas = isnull((select JSON_VALUE(@valor,'$.cajas')),'0')
			set @uds = isnull((select JSON_VALUE(@valor,'$.unidades')),'0')
			set @peso = isnull((select JSON_VALUE(@valor,'$.peso')),'0.000')

			-- actualizar linea
			set @sql = '
			update ['+@GESTION+'].[dbo].d_pedive
			set ARTICULO='''+@articulo+''', CAJAS='+@cajas+', UNIDADES='+@uds+', PESO='''+@peso+'''
			,	DEFINICION=(select nombre from vArticulos where CODIGO='''+@articulo+''')
			where EMPRESA='''+@Empresa+''' and NUMERO='''+@Numero+''' and LETRA='''+@Serie+''' and ARTICULO='''+@articuloAnterior+''' and LINIA='+@linea+'
			'
			EXEC(@sql)

			-- recalcular linea
			declare @prmtrs varchar(max) = CONCAT('{"numero":"',@Numero,'","letra":"',@Serie,'","linea":"',@linia,'"',',"articulo":"',@articulo,'","uds":"',@uds,'","peso":"',@peso,'"}')
			EXEC [pCalculosPV] @prmtrs

			FETCH NEXT FROM cur INTO @valor
		END CLOSE cur deallocate cur		
	END


	if @modo='traspasarObservacion' BEGIN		
		if exists (
			select * from Traspaso_Temporal 
			where concat(empresa,ltrim(rtrim(numero)),letra,linea)=concat(@Empresa,ltrim(rtrim(@Numero)),@Serie,@linia)
		)BEGIN
			delete Traspaso_Temporal 
			where concat(empresa,ltrim(rtrim(numero)),letra,linea)=concat(@Empresa,ltrim(rtrim(@Numero)),@Serie,@linia)
			select 'borrado' as JAVASCRIPT
		END ELSE BEGIN
			declare @nu varchar(50) = 
			replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 

			insert into Traspaso_Temporal 
			(sesion, id, empresa, numero, letra, linea, articulo, lote, cajas, unidades, peso, cliente, ruta, almacen, caducidad)
			values ('', @nu, @Empresa, @Numero, @Serie, @linia,'','','0','0','0','','','','')

			select 'insertado' as JAVASCRIPT
		END
		
		return -1
	END


	if @modo='finalizarPedido' BEGIN
		set @sql = 'update ['+@GESTION+'].dbo.c_pedive set TRASPASADO=1 where EMPRESA='''+@Empresa+''' and NUMERO='''+@Numero+''' and LETRA='''+@Serie+''''
		EXEC(@sql)
		select 'OK' as JAVASCRIPT
		return -1
	END


	if @datos='' set @datos='[]'
	
	select CONCAT('{"error":"',@error,'","datos":',@datos,'}') as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
	select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pRutas]...';


GO

CREATE PROCEDURE [dbo].[pRutas] @parametros varchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

		select isnull((select codigo, nombre from vRutas order by codigo ASC FOR JSON AUTO, INCLUDE_NULL_VALUES),'[]') as JAVASCRIPT

		return -1 
	END TRY
	BEGIN CATCH
		DECLARE @CatchError NVARCHAR(MAX)
		SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
		RAISERROR(@CatchError,12,1)
		RETURN 0
	END CATCH
END
GO
PRINT N'Creando Procedimiento [dbo].[pSeries]...';


GO
CREATE PROCEDURE [dbo].[pSeries] @parametros varchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

		declare @modo varchar(50) = (select isnull((select JSON_VALUE(@parametros,'$.modo')),'[]'))
			,	@objeto varchar(50) = (select isnull((select JSON_VALUE(@parametros,'$.objeto')),''))
			,	@serie char(2) = (select isnull((select JSON_VALUE(@parametros,'$.serie')),''))


		-- Series Asignar
		if @modo='asignarSerieConf' BEGIN 
			if exists (select * from ConfigSeries where objeto=@objeto and serie=@serie) delete ConfigSeries where objeto=@objeto and serie=@serie
			else insert into ConfigSeries (objeto,serie) values (@objeto,@serie)
			return -1 
		END


		-- Series del Objeto
		if @modo='cargarSeriesObjeto' BEGIN 
			select isnull((select isnull(serie,'') as codigo from ConfigSeries where objeto=@objeto FOR JSON AUTO, INCLUDE_NULL_VALUES),'[]') as JAVASCRIPT 
			return -1 
		END


		-- Series
		select isnull((select codigo, nombre from vSeries order by codigo ASC FOR JSON AUTO, INCLUDE_NULL_VALUES),'[]') as JAVASCRIPT
		
		return -1 
	END TRY
	BEGIN CATCH
		DECLARE @CatchError NVARCHAR(MAX)
		SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
		RAISERROR(@CatchError,12,1)
		RETURN 0
	END CATCH
END
GO
PRINT N'Creando Procedimiento [dbo].[pSustituirArticulos]...';


GO
CREATE PROCEDURE [dbo].[pSustituirArticulos] @parametros varchar(max)
AS
BEGIN
	SET NOCOUNT ON
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare @articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
		,	@linea int = cast(isnull((select JSON_VALUE(@parametros,'$.linea')),'0') as int)
		,	@unidades int = cast(isnull((select JSON_VALUE(@parametros,'$.unidades')),'0') as int)
		,	@letra varchar(2) = isnull((select JSON_VALUE(@parametros,'$.letra')),'')
		,	@numero varchar(50) = isnull((select JSON_VALUE(@parametros,'$.numero')),'')

	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EMPRESA char(2), @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select  @EMPRESA=Empresa, @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------	
	-- Variables
	DECLARE @CONSULTA NVARCHAR(MAX)
	-------------------------------------------------------------------------------------------------


	-- obtener datos del artículo
	DECLARE @SQLString nvarchar(500);  
	DECLARE @ParamDefinition nvarchar(500);  
	DECLARE @NOMBRE nvarchar(75), @CAJAS int=0, @UNICAJA int, @COST_ULT1 numeric(15,6), @IMPORTE numeric(15,6), @PESO varchar(50)
	SET @SQLString = N'SELECT @NOMBRE=nombre, @UNICAJA=UNICAJA, @COST_ULT1=COST_ULT1, @PESO=PESO 
					 FROM ['+@EJERCICIO+'].DBO.articulo WHERE CODIGO=@CODIGO';
	SET @ParamDefinition = N'@CODIGO varchar(20), @NOMBRE nvarchar(75) OUTPUT, @UNICAJA int OUTPUT, @COST_ULT1 int OUTPUT
						 , @PESO varchar(50) OUTPUT';
	EXECUTE sp_executesql @SQLString, @ParamDefinition, @CODIGO=@articulo, @NOMBRE=@NOMBRE OUTPUT, @UNICAJA=@UNICAJA OUTPUT
						, @COST_ULT1=@COST_ULT1 OUTPUT, @PESO=@PESO OUTPUT 

	if @UNICAJA>0 set @CAJAS=@unidades/@UNICAJA
	if isnull(@PESO,'0.000')='' set @PESO='0.000'

	-- actualizar linea de pedido
	SET @CONSULTA=CONCAT('UPDATE [',@EJERCICIO,'].DBO.d_pedive 
		SET ARTICULO=''',@articulo,''', DEFINICION=''',@NOMBRE,''', PRECIO=''',@COST_ULT1,'''  
		, UNIDADES=',@unidades,', CAJAS=',@CAJAS,', PESO=',@PESO,'  
		WHERE EMPRESA=''',@EMPRESA,''' AND NUMERO=''',@NUMERO,''' AND LETRA=''',@LETRA,''' AND LINIA=',@linea)
	EXEC (@CONSULTA)

	
	-- recálculo de la linea
	declare @param varchar(max) = 
	CONCAT('{"numero":"',@numero,'","letra":"',@letra,'","articulo":"',@articulo,'","linea":"',@linea,'"'
	,',"uds":"',@unidades,'","peso":"',@PESO,'"}')

	EXEC [pCalculosPV] @param

	RETURN -1 
END
GO
PRINT N'Creando Procedimiento [dbo].[pTraspasarPV]...';


GO
CREATE PROCEDURE [dbo].[pTraspasarPV] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare	@numero varchar(50) = isnull((select JSON_VALUE(@parametros,'$.numero')),'')
		,	@letra varchar(2) = isnull((select JSON_VALUE(@parametros,'$.letra')),'')
			
	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EJERCICIO char(4)
	declare @GESTION char(6)
	declare @COMUN char(8)
	declare @LOTES char(8)
	select @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------
	declare @sql varchar(max)
	declare @empresa char(2) = (select Empresa from Configuracion)
	declare @nu varchar(50) = 
		replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 

	--	verificar EnUso 
		if exists (SELECT EnUso from EnUso) BEGIN select '{"msj":"enUso"}' as JAVASCRIPT return -1 END

	--	verificar lotes
		if exists (SELECT ARTICULO, NOMBRE from vErrorLotesTrasTemp where empresa=@empresa and numero=@numero and letra=@letra) BEGIN
			select (SELECT 'verifLotes' as msj, ARTICULO, NOMBRE from vErrorLotesTrasTemp where empresa=@empresa and numero=@numero and letra=@letra 
			for JSON AUTO, INCLUDE_NULL_VALUES) as JAVASCRIPT
			return -1
		END
	


	--	Traspaso del Pedido #########################################################################################################################
		-- comprobar que existan lineas para traspasar
			if not exists (select * from [Traspaso_Temporal] where empresa=@empresa and numero=@numero and letra=@letra) 
			begin select '{"msj":"linTraspKO"}' as JAVASCRIPT  return -1 end

			declare   @todoTraspasado bit = 0
					, @impAlb bit = 0
					, @pedidoLineas int = isnull((select count(numero) from [Detalle_Pedidos]   
											where empresa+numero+letra=@empresa+@numero+@letra
												  and articulo is not null and ltrim(rtrim(articulo))<>'' )
										,0)
					, @linTrasp int     = isnull((select count(numero) from [Traspaso_Temporal] 
											where empresa+numero+letra=@empresa+@numero+@letra
												  and articulo is not null and ltrim(rtrim(articulo))<>'' )
										,0)
					, @etiEtiquetas varchar(50) = 'Nº Etiquetas'

			if @linTrasp>0 and @pedidoLineas>0 and @linTrasp=@pedidoLineas BEGIN set @todoTraspasado=1 set @impAlb=1 END
			if exists (SELECT * FROM [ConfigSeries] WHERE objeto='ImpAlbGen' and serie=@letra) set @impAlb=1
			if exists (SELECT * FROM [ConfigSeries] WHERE objeto='FactCajas' and serie=@letra) set @etiEtiquetas='Cajas'

			declare   @cliente varchar(20) = (select top 1 CLIENTE from Detalle_Pedidos where NUMERO=@numero and LETRA=@letra)
					, @pesoTotal numeric(15,6) = (select SUM(isnull(case when peso='' then 0.000 else cast(peso as numeric(15,6)) end,0.000)) 
												 from [Traspaso_Temporal] where NUMERO=@numero and LETRA=@letra)

			-- tabla temporal para recopilación de datos ------------------------------------------------------
			declare @tablaTemp TABLE (codigo varchar(20), NOMBRE varchar(100), direccion varchar(255), poblacion varchar(100), codpost varchar(10) 
									, provincia varchar(100), AGENCIA_NOMBRE varchar(100), TELEFONO varchar(20))
			SET @sql = 'select A.CODIGO as codigo,
					CASE WHEN A.contado = 0 
					THEN
						case when coalesce(c.NOMBRE,'''')='''' then A.NOMBRE else c.NOMBRE end 
					ELSE
						CON.NOMBRE
					END	as NOMBRE
					, CASE WHEN A.CONTADO = 0 
					THEN
						case when coalesce(c.direccion,'''')='''' then A.direccion else c.direccion end 
					ELSE
						CON.DIRECCION
					END	as direccion
					, CASE WHEN A.CONTADO = 0 
					THEN
						case when coalesce(c.poblacion,'''')='''' then A.poblacion else c.poblacion end
					ELSE
						CON.POBLACION 
					END as poblacion
					, CASE WHEN A.CONTADO = 0 
					THEN
					case when coalesce(c.codpos,'''')='''' then A.codpost else c.codpos end
					ELSE
						CON.CODPOST 
					END as codpost
					, CASE WHEN A.CONTADO = 0 
					THEN
						case when coalesce(c.provincia,'''')='''' then A.provincia else c.provincia end
					ELSE
						CON.PROVINCIA 
					END as provincia
					, ltrim(rtrim(coalesce(B.NOMBRE,''''))) AS AGENCIA_NOMBRE
					, CASE WHEN A.CONTADO = 0 
					THEN
						case when coalesce(c.TELEFONO,'''')='''' then coalesce(TEL.TELEFONO,'''') else c.TELEFONO end 
					ELSE
						CON.TELEFONO
					END	as TELEFONO 
				from ['+@GESTION+'].dbo.c_pedive cpv 	
				INNER JOIN ['+@GESTION+'].[dbo].clientes A ON A.CODIGO=CPV.CLIENTE
				LEFT JOIN ['+@GESTION+'].[dbo].agencia B ON B.CODIGO=A.AGENCIA 
				LEFT JOIN ['+@GESTION+'].[dbo].env_cli C ON C.LINEA=cpv.ENV_CLI and C.CLIENTE=cpv.cliente
				LEFT JOIN ['+@GESTION+'].[dbo].TELF_CLI tel ON tel.cliente=cpv.cliente and tel.orden=1		
				LEFT JOIN ['+@GESTION+'].DBO.CONTADO CON ON CON.EMPRESA=CPV.EMPRESA AND CON.NUMERO=CPV.NUMERO AND CON.LETRA=CPV.LETRA AND CON.TIPO=2				
				where cpv.traspasado=0 and cpv.cancelado=0 and cpv.empresa='''+@empresa+''' and cpv.NUMERO='''+@numero+''' and cpv.lETRA='''+@letra+''''
			INSERT @tablaTemp EXEC(@sql)	
			declare @datosCliente varchar(max) = (SELECT * from @tablaTemp for JSON AUTO, INCLUDE_NULL_VALUES)
			delete @tablaTemp

			select CONCAT('[{"impAlb":"',@impAlb,'","etiEtiquetas":"',@etiEtiquetas,'","pesoTotal":"',@pesoTotal,'","datosCliente":',@datosCliente,'}]') as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pTraspasoCompleto]...';


GO
CREATE PROCEDURE [dbo].[pTraspasoCompleto] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON
	-----------------------------------------------------------------------------------------------
	-- Variables de Configuración 
	-----------------------------------------------------------------------------------------------
	declare @EJERCICIO char(4)
	declare @GESTION char(6)
	declare @COMUN char(8)
	declare @LOTES char(8)
	select @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-----------------------------------------------------------------------------------------------
	-- Parámetros JSON 
	declare	  @numero varchar(50) = isnull((select JSON_VALUE(@parametros,'$.numero')),'')
			, @serie varchar(10) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
			, @ruta varchar(10) = isnull((select JSON_VALUE(@parametros,'$.ruta')),'')
			, @almacen varchar(10) = isnull((select JSON_VALUE(@parametros,'$.almacen')),'')
			, @articulos varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulos')),'')

	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-------------------------------------------------------------------------------------------------
	
	declare @empresa char(2) = (select Empresa from Configuracion)
	declare @nu varchar(50) = 
		replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 

	declare @artSinStock varchar(max) = ''


	-- artículos
	declare @articulo varchar(50)
		,	@unidades numeric(15,6)
		,	@linea int
		,	@cajas numeric(15,6)
		,	@peso numeric(15,6)
		,	@art varchar(max) 

	declare cur CURSOR for select [value] from openjson(@parametros,'$.articulos')
	OPEN cur FETCH NEXT FROM cur INTO @art
	WHILE (@@FETCH_STATUS=0) BEGIN
		select @articulo = isnull((select JSON_VALUE(@art,'$.articulo')),'')
		select @cajas = cast(isnull((select JSON_VALUE(@art,'$.cajas')),'0.00') as numeric(15,6))
		select @unidades = cast(isnull((select JSON_VALUE(@art,'$.unidades')),'0.00') as numeric(15,6))
		select @peso = cast(isnull((select JSON_VALUE(@art,'$.peso')),'0.00') as numeric(15,6))
		select @linea = cast(isnull((select JSON_VALUE(@art,'$.linea')),'0') as int)
		
		declare   @cliente varchar(20)	= (select top 1 CLIENTE from Detalle_Pedidos where numero=@numero)
				, @artUnicaja int		
				, @artPeso varchar(10)	
				, @newCajas int			= 0
				, @newUnidades int		= 0
				, @newPeso varchar(10)	= ''	

		declare   @artStock numeric(15,6)
				, @pedCliente varchar(20)
				, @caducidad smalldatetime
				, @lote varchar(50) = '-'
				, @controlStock bit				
			
		-- Tabla temporal para recopilación de datos ---------------------------------------------------------------------------------------------------------------------
		DECLARE @tbTabla TABLE (UNICAJA int, PESO varchar(20), stock bit, artStock numeric(15,6), pedCliente varchar(20))
		DECLARE @sql nvarchar(max)
		SET @sql = 'select UNICAJA,PESO, stock 
					, COALESCE((select max(FINAL) from ['+@GESTION+'].dbo.stocks2 where ARTICULO='''+@articulo+'''),0.00) as artStock
					, (select CLIENTE from ['+@GESTION+'].[dbo].c_pedive where EMPRESA='''+@empresa+''' and NUMERO='''+@numero+''' and LETRA='''+@serie+''') as pedCliente
					from ['+@GESTION+'].dbo.articulo where CODIGO='''+@articulo+''''
		INSERT @tbTabla EXEC(@sql)
		select @artUnicaja=UNICAJA, @artPeso=PESO, @controlStock=stock, @artStock=artStock, @pedCliente=pedCliente from @tbTabla
		delete @tbTabla
		-------------------------------------------------------------------------------------------------------------------------------------------------------------------

		if @artStock<@unidades and @controlStock=0  
		set @artSinStock = @artSinStock + ',{"articuloSinStock":"'+@artSinStock+'"}'


		--	si trabaja con lotes 
		if (select count(NUMERO) from vBuscarLotes 	where EMPRESA=@empresa and LETRA=@serie and NUMERO=@numero 
			and ARTICULO=@articulo and LINIA=@linea  and Llote is not null)>0 BEGIN
					
				--	Trabaja con lotes ------------------------------
					declare @_pesoArticulo varchar(10), @_Lcaducidad smalldatetime, @_Llote varchar(50)
					, @_Lunidades numeric(15,6), @_Lpeso numeric(15,6), @_ARTICULO varchar(20), @_UNIDADES numeric(15,6)
					, @_SERVIDAS numeric(15,6), @_UNICAJA numeric(15,6), @_pCajas numeric(15,6), @_pCajaServ numeric(15,6)
					, @_tempCajas numeric(15,6), @_tempMaxCajas numeric(15,6), @_tempPeso varchar(10), @_tempUnidades numeric(15,6)
							  
					declare @unidadesG numeric(15,6) = @unidades					 
							, @cajasG numeric(15,6)
							, @pesoG numeric(15,6)			

					declare elCursor CURSOR LOCAL STATIC for
						select pesoArticulo, Lcaducidad, Llote, Lunidades, Lpeso, ARTICULO, UNIDADES, SERVIDAS, UNICAJA
								, pCajas, pCajaServ, tempCajas, tempMaxCajas, tempPeso, tempUnidades
						from vBuscarLotes 	
						where EMPRESA=@empresa and LETRA=@serie and NUMERO=@numero and ARTICULO=@articulo and LINIA=@linea  
							and Llote is not null  AND LUNIDADES>0.00
						order by Lcaducidad asc	
					open elCursor
					FETCH NEXT FROM elCursor 
					INTO  @_pesoArticulo, @_Lcaducidad, @_Llote, @_Lunidades, @_Lpeso, @_ARTICULO, @_UNIDADES
						, @_SERVIDAS, @_UNICAJA, @_pCajas, @_pCajaServ, @_tempCajas , @_tempMaxCajas, @_tempPeso
						, @_tempUnidades
					WHILE @@FETCH_STATUS=0 AND @unidadesG>0 BEGIN	
						declare @udsAcero int=0;
						if @artUnicaja is null or @artUnicaja=0 set @artUnicaja=1
						if @_Lunidades<=@unidadesG begin set @unidadesG=@_Lunidades end else begin set @udsAcero=1 end
						set @cajasG = @unidadesG / @artUnicaja
						if @artPeso<>'' set @pesoG = cast(@artPeso as numeric(15,6)) * @unidadesG
						
						delete	Traspaso_Temporal 
						where	empresa=@empresa and numero=@numero and letra=@serie and linea=@linea 
								and articulo=@articulo and lote=@_Llote  and cliente=@cliente
							
						insert into Traspaso_Temporal 
						(sesion, id, empresa, numero, letra, linea, articulo, lote, cajas, unidades, peso, cliente
						, ruta, almacen, caducidad) 
						values 
						(@usuario, @nu, @empresa, @numero, @serie, @linea, @articulo, @_Llote, @cajasG, @unidadesG
						, cast(@pesoG as varchar(10)), @cliente, @ruta, @almacen, @_Lcaducidad)

						if @udsAcero=1 
							begin 
								set @unidadesG=0
							end 
						else 
							begin 
								set @unidadesG = @unidades - @unidadesG 
								set @unidades = @unidadesG
							end
							
						FETCH NEXT FROM elCursor 
						INTO  @_pesoArticulo, @_Lcaducidad, @_Llote, @_Lunidades, @_Lpeso, @_ARTICULO, @_UNIDADES
							, @_SERVIDAS, @_UNICAJA, @_pCajas, @_pCajaServ, @_tempCajas , @_tempMaxCajas, @_tempPeso
							, @_tempUnidades
					END close elCursor deallocate elCursor
			END ELSE BEGIN
			
				--	NO trabaja con lotes ------------------------------

				if @artUnicaja>0  set @cajas=@unidades/@artUnicaja
				if @artPeso<>'' set  @peso =@unidades * cast(@artPeso as numeric(15,6))
				if @artPeso='' set @artPeso='0.00'
					
				delete	Traspaso_Temporal 
				where	empresa=@empresa and numero=@numero and letra=@serie and linea=@linea and articulo=@articulo 
						and lote='-'  and cliente=@cliente
					
				insert into Traspaso_Temporal 
				(sesion, id, empresa, numero, letra, linea, articulo, lote, cajas, unidades, peso, cliente, ruta
				, almacen, caducidad)
				values 
				(@usuario, @nu, @empresa, @numero, @serie, @linea, @articulo, '-', @cajas, @unidades
				, cast(@Peso as varchar(10)), @cliente, @ruta, @almacen, '')
			END

		FETCH NEXT FROM cur INTO @art
	END CLOSE cur deallocate cur

	select '{"artSinStock":['+@artSinStock+']}' as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select 'Error! - SP pTraspasoCompleto...'+@CatchError as JAVASCRIPT
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pTraspLinCompleta]...';


GO
CREATE PROCEDURE [dbo].[pTraspLinCompleta] @parametros varchar(max)
AS
BEGIN TRY	
	SET NOCOUNT ON	
	begin tran
	-----------------------------------------------------------------------------------------------
	-- Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare	  @almacen varchar(2) = isnull((select JSON_VALUE(@parametros,'$.almacen')),'')
			, @articulo varchar(50) = isnull((select JSON_VALUE(@parametros,'$.articulo')),'')
			, @cajas int = isnull((select JSON_VALUE(@parametros,'$.cajas')),'')
			, @unidades int = isnull((select JSON_VALUE(@parametros,'$.unidades')),'')
			, @peso varchar(10) = isnull((select JSON_VALUE(@parametros,'$.peso')),'')
			, @linea int = isnull((select JSON_VALUE(@parametros,'$.linea')),'')
			, @numero char(10) = isnull((select JSON_VALUE(@parametros,'$.numero')),'')
			, @serie varchar(10) = isnull((select JSON_VALUE(@parametros,'$.serie')),'')
	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	declare @laSesion varchar(50) = @usuario
	declare @nu varchar(50) = 
	replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 
	-----------------------------------------------------------------------------------------------
	-- Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EJERCICIO char(4)
	declare @GESTION char(6)
	declare @COMUN char(8)
	declare @LOTES char(8)
	select @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-----------------------------------------------------------------------------------------------
	-- Variables del SP 
	-----------------------------------------------------------------------------------------------
	declare @sql nvarchar(max)	
	-----------------------------------------------------------------------------------------------

	declare   @empresa varchar(10)	= (select Empresa from Configuracion)
			, @cliente varchar(20)
			, @ruta varchar(10)
			, @artUnicaja int
			, @artPeso varchar(10)
			, @newCajas int			= 0
			, @newUnidades int		= 0
			, @newPeso varchar(10)	= ''

	
	-- tabla temporal para recopilación de datos ------------------------------------------------------
	declare @tablaTemp TABLE (CLIENTE nvarchar(20), RUTA varchar(10), UNICAJA int, PESO varchar(20))
	SET @sql = 'select top 1 CLIENTE 
				, (select top 1 ruta from Detalle_Pedidos where numero='''+@numero+''') as RUTA
				, (select UNICAJA from ['+@GESTION+'].dbo.articulo where CODIGO='''+@articulo+''') as UNICAJA
				, (select PESO from ['+@GESTION+'].dbo.articulo where CODIGO='''+@articulo+''') as PESO
				from Detalle_Pedidos where numero='''+@numero+''''
	INSERT @tablaTemp EXEC(@sql)	
	SELECT @cliente=CLIENTE, @ruta=RUTA, @artUnicaja=UNICAJA, @artPeso=PESO FROM @tablaTemp
	delete @tablaTemp
	--------------------------------------------------------------------------------------------------

	if (select count(NUMERO) from vBuscarLotes 	where EMPRESA=@empresa and LETRA=@serie and NUMERO=@numero 
	and ARTICULO=@articulo and LINIA=@linea  and Llote is not null)>0 BEGIN
					
		--	Trabaja con lotes ------------------------------

			declare @_pesoArticulo varchar(10), @_Lcaducidad smalldatetime, @_Llote varchar(50), @_Lunidades int
					, @_Lpeso numeric(15,6), @_ARTICULO varchar(20), @_UNIDADES int, @_SERVIDAS int, @_UNICAJA int
					, @_pCajas numeric(15,6), @_pCajaServ numeric(15,6), @_tempCajas numeric(15,6)
					, @_tempMaxCajas numeric(15,6), @_tempPeso varchar(10), @_tempUnidades numeric(15,6)
							  
			declare @unidadesG int = @unidades					 
					, @cajasG int
					, @pesoG varchar(10)

			declare elCursor CURSOR LOCAL STATIC for
				select pesoArticulo, Lcaducidad, Llote, Lunidades, Lpeso, ARTICULO, UNIDADES, SERVIDAS, UNICAJA
						, pCajas, pCajaServ, tempCajas, tempMaxCajas, tempPeso, tempUnidades
				from vBuscarLotes 	
				where EMPRESA=@empresa and LETRA=@serie and NUMERO=@numero and ARTICULO=@articulo and LINIA=@linea  and Llote is not null
				order by Lcaducidad asc	
			open elCursor
			FETCH NEXT FROM elCursor INTO @_pesoArticulo, @_Lcaducidad, @_Llote, @_Lunidades, @_Lpeso, @_ARTICULO, @_UNIDADES
										, @_SERVIDAS, @_UNICAJA, @_pCajas, @_pCajaServ, @_tempCajas , @_tempMaxCajas, @_tempPeso
										, @_tempUnidades
			WHILE @@FETCH_STATUS=0 AND @unidadesG>0 BEGIN	
				declare @udsAcero int=0;
				if @_Lunidades<=@unidadesG begin set @unidadesG=@_Lunidades end else begin set @udsAcero=1 end
				if @artUnicaja>0 set @cajasG = @unidadesG / @artUnicaja
				if @artPeso<>'' set @pesoG = cast((cast(@artPeso as numeric(15,6)) * @unidadesG) as varchar(10))
						
				delete Traspaso_Temporal 
				where	empresa=@empresa and numero=@numero and letra=@serie and linea=@linea and articulo=@articulo 
						and lote=@_Llote  and cliente=@cliente
							
				insert into Traspaso_Temporal 
				(sesion, id, empresa, numero, letra, linea, articulo, lote, cajas, unidades, peso, cliente, ruta, almacen, caducidad) 
				values 
				(@laSesion, @nu, @empresa, @numero, @serie, @linea, @articulo, @_Llote, @cajasG, @unidadesG, @pesoG, @cliente
				, @ruta, @almacen, @_Lcaducidad)

				if @udsAcero=1 begin set @unidadesG=0 end else begin set @unidadesG = @unidades - @unidadesG end

				FETCH NEXT FROM elCursor INTO @_pesoArticulo, @_Lcaducidad, @_Llote, @_Lunidades, @_Lpeso, @_ARTICULO, @_UNIDADES
										, @_SERVIDAS, @_UNICAJA, @_pCajas, @_pCajaServ, @_tempCajas , @_tempMaxCajas, @_tempPeso
										, @_tempUnidades
			END close elCursor deallocate elCursor
	END ELSE BEGIN

		--	NO trabaja con lotes ------------------------------
		if @artUnicaja>0  set @cajas=@unidades/@artUnicaja
		if @artPeso<>'' set  @peso = cast( (@unidades * cast(@artPeso as numeric(15,6)) ) as varchar(10))
		if @artPeso='' set @artPeso='0.00'
					
		delete Traspaso_Temporal 
		where	empresa=@empresa and numero=@numero and letra=@serie and linea=@linea and articulo=@articulo 
				and lote='-'  and cliente=@cliente
					
		insert into Traspaso_Temporal 
		(sesion, id, empresa, numero, letra, linea, articulo, lote, cajas, unidades, peso, cliente, ruta, almacen, caducidad) 
		values 
		(@laSesion, @nu, @empresa, @numero, @serie, @linea, @articulo, '-', @cajas, @unidades, @Peso, @cliente
		, @ruta, @almacen, '')
	END

	select 'OK!' as JAVASCRIPT
	commit tran
	RETURN -1
END TRY

BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	rollback tran
	SET @CatchError= ERROR_MESSAGE() + char(13) + ERROR_NUMBER() + char(13) + ERROR_PROCEDURE() + char(13) + @@PROCID + char(13) + ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pZonas]...';


GO
CREATE PROCEDURE [dbo].[pZonas] @parametros varchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

		select isnull((select VALOR from vZonas order by VALOR ASC FOR JSON AUTO, INCLUDE_NULL_VALUES),'[]') as JAVASCRIPT

		return -1 
	END TRY
	BEGIN CATCH
		DECLARE @CatchError NVARCHAR(MAX)
		SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
		RAISERROR(@CatchError,12,1)
		RETURN 0
	END CATCH
END
GO
PRINT N'Creando Procedimiento [dbo].[sp_generate_merge]...';


GO
CREATE PROCEDURE [dbo].[sp_generate_merge]
(
 @table_name varchar(776), -- The table/view for which the MERGE statement will be generated using the existing data
 @target_table varchar(776) = NULL, -- Use this parameter to specify a different table name into which the data will be inserted/updated/deleted
 @from varchar(800) = NULL, -- Use this parameter to filter the rows based on a filter condition (using WHERE)
 @include_timestamp bit = 0, -- Specify 1 for this parameter, if you want to include the TIMESTAMP/ROWVERSION column's data in the MERGE statement
 @debug_mode bit = 0, -- If @debug_mode is set to 1, the SQL statements constructed by this procedure will be printed for later examination
 @schema varchar(64) = NULL, -- Use this parameter if you are not the owner of the table
 @ommit_images bit = 0, -- Use this parameter to generate MERGE statement by omitting the 'image' columns
 @ommit_identity bit = 0, -- Use this parameter to ommit the identity columns
 @top int = NULL, -- Use this parameter to generate a MERGE statement only for the TOP n rows
 @cols_to_include varchar(8000) = NULL, -- List of columns to be included in the MERGE statement
 @cols_to_exclude varchar(8000) = NULL, -- List of columns to be excluded from the MERGE statement
 @update_only_if_changed bit = 1, -- When 1, only performs an UPDATE operation if an included column in a matched row has changed.
 @delete_if_not_matched bit = 1, -- When 1, deletes unmatched source rows from target, when 0 source rows will only be used to update existing rows or insert new.
 @disable_constraints bit = 0, -- When 1, disables foreign key constraints and enables them after the MERGE statement
 @ommit_computed_cols bit = 0, -- When 1, computed columns will not be included in the MERGE statement
 @include_use_db bit = 0, -- When 1, includes a USE [DatabaseName] statement at the beginning of the generated batch
 @results_to_text bit = 0, -- When 1, outputs results to grid/messages window. When 0, outputs MERGE statement in an XML fragment.
 @include_rowsaffected bit = 0, -- When 1, a section is added to the end of the batch which outputs rows affected by the MERGE
 @nologo bit = 1, -- When 1, the "About" comment is suppressed from output
 @batch_separator VARCHAR(50) = 'GO', -- Batch separator to use
 @OriginId int = 0, --Default filter for OriginId fields
 @ReturnSql bit = 0, --return to variable
 @OutputSQL nvarchar(max) = null OUTPUT --return  variable
 
)
AS
BEGIN

/***********************************************************************************************************

--2017.09.12.Customized for FlexyGo Use Cases:
	-- If table contains an OriginId Field, DELETE clause WILL NOT delete records with OriginId = 2



Procedure: sp_generate_merge (Version 0.93)
 (Adapted by Daniel Nolan for SQL Server 2008/2012)

Adapted from: sp_generate_inserts (Build 22) 
 (Copyright © 2002 Narayana Vyas Kondreddi. All rights reserved.)

Purpose: To generate a MERGE statement from existing data, which will INSERT/UPDATE/DELETE data based
 on matching primary key values in the source/target table.
 
 The generated statements can be executed to replicate the data in some other location.
 
 Typical use cases:
 * Generate statements for static data tables, store the .SQL file in source control and use 
 it as part of your Dev/Test/Prod deployment. The generated statements are re-runnable, so 
 you can make changes to the file and migrate those changes between environments.
 
 * Generate statements from your Production tables and then run those statements in your 
 Dev/Test environments. Schedule this as part of a SQL Job to keep all of your environments 
 in-sync.
 
 * Enter test data into your Dev environment, and then generate statements from the Dev
 tables so that you can always reproduce your test database with valid sample data.
 

Written by: Narayana Vyas Kondreddi
 http://vyaskn.tripod.com

 Daniel Nolan
 http://danere.com
 @dan3r3

Acknowledgements (sp_generate_merge):
 Nathan Skerl -- StackOverflow answer that provided a workaround for the output truncation problem
 http://stackoverflow.com/a/10489767/266882

 Bill Gibson -- Blog that detailed the static data table use case; the inspiration for this proc
 http://blogs.msdn.com/b/ssdt/archive/2012/02/02/including-data-in-an-sql-server-database-project.aspx
 
 Bill Graziano -- Blog that provided the groundwork for MERGE statement generation
 http://weblogs.sqlteam.com/billg/archive/2011/02/15/generate-merge-statements-from-a-table.aspx 

Acknowledgements (sp_generate_inserts):
 Divya Kalra -- For beta testing
 Mark Charsley -- For reporting a problem with scripting uniqueidentifier columns with NULL values
 Artur Zeygman -- For helping me simplify a bit of code for handling non-dbo owned tables
 Joris Laperre -- For reporting a regression bug in handling text/ntext columns

Tested on: SQL Server 2008 (10.50.1600), SQL Server 2012 (11.0.2100)

Date created: January 17th 2001 21:52 GMT
Modified: May 1st 2002 19:50 GMT
Last Modified: September 27th 2012 10:00 AEDT

Email: dan@danere.com, vyaskn@hotmail.com

NOTE: This procedure may not work with tables with a large number of columns (> 500).
 Results can be unpredictable with huge text columns or SQL Server 2000's sql_variant data types
 IMPORTANT: This procedure has not been extensively tested with international data (Extended characters or Unicode). If needed
 you might want to convert the datatypes of character variables in this procedure to their respective unicode counterparts
 like nchar and nvarchar

Get Started: Ensure that your SQL client is configured to send results to grid (default SSMS behaviour).
This ensures that the generated MERGE statement can be output in full, getting around SSMS's 4000 nchar limit.
After running this proc, click the hyperlink within the single row returned to copy the generated MERGE statement.

Example 1: To generate a MERGE statement for table 'titles':
 
 EXEC sp_generate_merge 'titles'

Example 2: To generate a MERGE statement for 'titlesCopy' table from 'titles' table:

 EXEC sp_generate_merge 'titles', 'titlesCopy'

Example 3: To generate a MERGE statement for table 'titles' that will unconditionally UPDATE matching rows 
 (ie. not perform a "has data changed?" check prior to going ahead with an UPDATE):
 
 EXEC sp_generate_merge 'titles', @update_only_if_changed = 0

Example 4: To generate a MERGE statement for 'titles' table for only those titles 
 which contain the word 'Computer' in them:
 NOTE: Do not complicate the FROM or WHERE clause here. It's assumed that you are good with T-SQL if you are using this parameter

 EXEC sp_generate_merge 'titles', @from = "from titles where title like '%Computer%'"

Example 5: To specify that you want to include TIMESTAMP column's data as well in the MERGE statement:
 (By default TIMESTAMP column's data is not scripted)

 EXEC sp_generate_merge 'titles', @include_timestamp = 1

Example 6: To print the debug information:

 EXEC sp_generate_merge 'titles', @debug_mode = 1

Example 7: If the table is in a different schema to the default, use @schema parameter to specify the schema name
 To use this option, you must have SELECT permissions on that table

 EXEC sp_generate_merge 'Nickstable', @schema = 'Nick'

Example 8: To generate a MERGE statement for the rest of the columns excluding images

 EXEC sp_generate_merge 'imgtable', @ommit_images = 1

Example 9: To generate a MERGE statement excluding (omitting) IDENTITY columns:
 (By default IDENTITY columns are included in the MERGE statement)

 EXEC sp_generate_merge 'mytable', @ommit_identity = 1

Example 10: To generate a MERGE statement for the TOP 10 rows in the table:
 
 EXEC sp_generate_merge 'mytable', @top = 10

Example 11: To generate a MERGE statement with only those columns you want:
 
 EXEC sp_generate_merge 'titles', @cols_to_include = "'title','title_id','au_id'"

Example 12: To generate a MERGE statement by omitting certain columns:
 
 EXEC sp_generate_merge 'titles', @cols_to_exclude = "'title','title_id','au_id'"

Example 13: To avoid checking the foreign key constraints while loading data with a MERGE statement:
 
 EXEC sp_generate_merge 'titles', @disable_constraints = 1

Example 14: To exclude computed columns from the MERGE statement:

 EXEC sp_generate_merge 'MyTable', @ommit_computed_cols = 1
 
***********************************************************************************************************/


SET NOCOUNT ON


--Making sure user only uses either @cols_to_include or @cols_to_exclude
IF ((@cols_to_include IS NOT NULL) AND (@cols_to_exclude IS NOT NULL))
 BEGIN
 RAISERROR('Use either @cols_to_include or @cols_to_exclude. Do not use both the parameters at once',16,1)
 RETURN -1 --Failure. Reason: Both @cols_to_include and @cols_to_exclude parameters are specified
 END


--Making sure the @cols_to_include and @cols_to_exclude parameters are receiving values in proper format
IF ((@cols_to_include IS NOT NULL) AND (PATINDEX('''%''',@cols_to_include) = 0))
 BEGIN
 RAISERROR('Invalid use of @cols_to_include property',16,1)
 PRINT 'Specify column names surrounded by single quotes and separated by commas'
 PRINT 'Eg: EXEC sp_generate_merge titles, @cols_to_include = "''title_id'',''title''"'
 RETURN -1 --Failure. Reason: Invalid use of @cols_to_include property
 END

IF ((@cols_to_exclude IS NOT NULL) AND (PATINDEX('''%''',@cols_to_exclude) = 0))
 BEGIN
 RAISERROR('Invalid use of @cols_to_exclude property',16,1)
 PRINT 'Specify column names surrounded by single quotes and separated by commas'
 PRINT 'Eg: EXEC sp_generate_merge titles, @cols_to_exclude = "''title_id'',''title''"'
 RETURN -1 --Failure. Reason: Invalid use of @cols_to_exclude property
 END


--Checking to see if the database name is specified along wih the table name
--Your database context should be local to the table for which you want to generate a MERGE statement
--specifying the database name is not allowed
IF (PARSENAME(@table_name,3)) IS NOT NULL
 BEGIN
 RAISERROR('Do not specify the database name. Be in the required database and just specify the table name.',16,1)
 RETURN -1 --Failure. Reason: Database name is specified along with the table name, which is not allowed
 END


--Checking for the existence of 'user table' or 'view'
--This procedure is not written to work on system tables
--To script the data in system tables, just create a view on the system tables and script the view instead
IF @schema IS NULL
 BEGIN
 IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @table_name AND (TABLE_TYPE = 'BASE TABLE' OR TABLE_TYPE = 'VIEW') AND TABLE_SCHEMA = SCHEMA_NAME())
 BEGIN
 RAISERROR('User table or view not found.',16,1)
 PRINT 'You may see this error if the specified table is not in your default schema (' + SCHEMA_NAME() + '). In that case use @schema parameter to specify the schema name.'
 PRINT 'Make sure you have SELECT permission on that table or view.'
 RETURN -1 --Failure. Reason: There is no user table or view with this name
 END
 END
ELSE
 BEGIN
 IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @table_name AND (TABLE_TYPE = 'BASE TABLE' OR TABLE_TYPE = 'VIEW') AND TABLE_SCHEMA = @schema)
 BEGIN
 RAISERROR('User table or view not found.',16,1)
 PRINT 'Make sure you have SELECT permission on that table or view.'
 RETURN -1 --Failure. Reason: There is no user table or view with this name 
 END
 END


--Variable declarations
DECLARE @Column_ID int, 
 @Column_List varchar(8000), 
 @Column_List_For_Update varchar(max), 
 @Column_List_For_Check varchar(max), 
 @Column_Name varchar(128), 
 @Column_Name_Unquoted varchar(128), 
 @Data_Type varchar(128), 
 @Actual_Values nvarchar(max), --This is the string that will be finally executed to generate a MERGE statement
 @IDN varchar(128), --Will contain the IDENTITY column's name in the table
 @Target_Table_For_Output varchar(776),
 @Source_Table_Qualified varchar(776),
 @HasOriginId bit
 
 
DECLARE @b CHAR(2) = CHAR(13)+CHAR(10)

--Variable Initialization
SET @IDN = ''
SET @Column_ID = 0
SET @Column_Name = ''
SET @Column_Name_Unquoted = ''
SET @Column_List = ''
SET @Column_List_For_Update = ''
SET @Column_List_For_Check = ''
SET @Actual_Values = ''

--Variable Defaults
IF @schema IS NULL
 BEGIN
 SET @Target_Table_For_Output = QUOTENAME(COALESCE(@target_table, @table_name))
 END
ELSE
 BEGIN
 SET @Target_Table_For_Output = QUOTENAME(@schema) + '.' + QUOTENAME(COALESCE(@target_table, @table_name))
 END

SET @Source_Table_Qualified = QUOTENAME(COALESCE(@schema,SCHEMA_NAME())) + '.' + QUOTENAME(@table_name)

--To get the first column's ID
SELECT @Column_ID = MIN(ORDINAL_POSITION) 
FROM INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
WHERE TABLE_NAME = @table_name
AND TABLE_SCHEMA = COALESCE(@schema, SCHEMA_NAME())

IF EXISTS(
	SELECT COLUMN_NAME
	FROM INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
	WHERE TABLE_NAME = @table_name AND COLUMN_NAME='OriginId'
) SET @HasOriginId = 1

--Loop through all the columns of the table, to get the column names and their data types
WHILE @Column_ID IS NOT NULL
 BEGIN
 SELECT @Column_Name = QUOTENAME(COLUMN_NAME), 
 @Column_Name_Unquoted = COLUMN_NAME,
 @Data_Type = DATA_TYPE 
 FROM INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
 WHERE ORDINAL_POSITION = @Column_ID
 AND TABLE_NAME = @table_name
 AND TABLE_SCHEMA = COALESCE(@schema, SCHEMA_NAME())

 IF @cols_to_include IS NOT NULL --Selecting only user specified columns
 BEGIN
 IF CHARINDEX( '''' + SUBSTRING(@Column_Name,2,LEN(@Column_Name)-2) + '''',@cols_to_include) = 0 
 BEGIN
 GOTO SKIP_LOOP
 END
 END

 IF @cols_to_exclude IS NOT NULL --Selecting only user specified columns
 BEGIN
 IF CHARINDEX( '''' + SUBSTRING(@Column_Name,2,LEN(@Column_Name)-2) + '''',@cols_to_exclude) <> 0 
 BEGIN
 GOTO SKIP_LOOP
 END
 END

 --Making sure to output SET IDENTITY_INSERT ON/OFF in case the table has an IDENTITY column
 IF (SELECT COLUMNPROPERTY( OBJECT_ID(@Source_Table_Qualified),SUBSTRING(@Column_Name,2,LEN(@Column_Name) - 2),'IsIdentity')) = 1 
 BEGIN
 IF @ommit_identity = 0 --Determing whether to include or exclude the IDENTITY column
 SET @IDN = @Column_Name
 ELSE
 GOTO SKIP_LOOP 
 END
 
 --Making sure whether to output computed columns or not
 IF @ommit_computed_cols = 1
 BEGIN
 IF (SELECT COLUMNPROPERTY( OBJECT_ID(@Source_Table_Qualified),SUBSTRING(@Column_Name,2,LEN(@Column_Name) - 2),'IsComputed')) = 1 
 BEGIN
 GOTO SKIP_LOOP 
 END
 END
 
 --Tables with columns of IMAGE data type are not supported for obvious reasons
 IF(@Data_Type in ('image'))
 BEGIN
 IF (@ommit_images = 0)
 BEGIN
 RAISERROR('Tables with image columns are not supported.',16,1)
 PRINT 'Use @ommit_images = 1 parameter to generate a MERGE for the rest of the columns.'
 RETURN -1 --Failure. Reason: There is a column with image data type
 END
 ELSE
 BEGIN
 GOTO SKIP_LOOP
 END
 END

 --Determining the data type of the column and depending on the data type, the VALUES part of
 --the MERGE statement is generated. Care is taken to handle columns with NULL values. Also
 --making sure, not to lose any data from flot, real, money, smallmomey, datetime columns
 SET @Actual_Values = @Actual_Values +
 CASE 
 WHEN @Data_Type IN ('char','nchar') 
 THEN 
 'COALESCE(''N'''''' + REPLACE(RTRIM(' + @Column_Name + '),'''''''','''''''''''')+'''''''',''NULL'')'
 WHEN @Data_Type IN ('varchar','nvarchar') 
 THEN 
 'COALESCE(''N'''''' + REPLACE(' + @Column_Name + ','''''''','''''''''''')+'''''''',''NULL'')'
 WHEN @Data_Type IN ('datetime','smalldatetime','datetime2','date') 
 THEN 
 'COALESCE('''''''' + RTRIM(CONVERT(char,' + @Column_Name + ',127))+'''''''',''NULL'')'
 WHEN @Data_Type IN ('uniqueidentifier') 
 THEN 
 'COALESCE(''N'''''' + REPLACE(CONVERT(char(36),RTRIM(' + @Column_Name + ')),'''''''','''''''''''')+'''''''',''NULL'')'
 WHEN @Data_Type IN ('text') 
 THEN 
 'COALESCE(''N'''''' + REPLACE(CONVERT(varchar(max),' + @Column_Name + '),'''''''','''''''''''')+'''''''',''NULL'')' 
 WHEN @Data_Type IN ('ntext') 
 THEN 
 'COALESCE('''''''' + REPLACE(CONVERT(nvarchar(max),' + @Column_Name + '),'''''''','''''''''''')+'''''''',''NULL'')' 
 WHEN @Data_Type IN ('xml') 
 THEN 
 'COALESCE('''''''' + REPLACE(CONVERT(nvarchar(max),' + @Column_Name + '),'''''''','''''''''''')+'''''''',''NULL'')' 
 WHEN @Data_Type IN ('binary','varbinary') 
 THEN 
 'COALESCE(RTRIM(CONVERT(varchar(max),' + @Column_Name + ', 1))),''NULL'')' 
 WHEN @Data_Type IN ('timestamp','rowversion') 
 THEN 
 CASE 
 WHEN @include_timestamp = 0 
 THEN 
 '''DEFAULT''' 
 ELSE 
 'COALESCE(RTRIM(CONVERT(char,' + 'CONVERT(int,' + @Column_Name + '))),''NULL'')' 
 END
 WHEN @Data_Type IN ('float','real','money','smallmoney')
 THEN
 'COALESCE(LTRIM(RTRIM(' + 'CONVERT(char, ' + @Column_Name + ',2)' + ')),''NULL'')' 
 WHEN @Data_Type IN ('hierarchyid')
 THEN 
  'COALESCE(''hierarchyid::Parse(''+'''''''' + LTRIM(RTRIM(' + 'CONVERT(char, ' + @Column_Name + ')' + '))+''''''''+'')'',''NULL'')' 
 ELSE 
 'COALESCE(LTRIM(RTRIM(' + 'CONVERT(char, ' + @Column_Name + ')' + ')),''NULL'')' 
 END + '+' + ''',''' + ' + '
 
 --Generating the column list for the MERGE statement
 SET @Column_List = @Column_List + @Column_Name + ',' 
 
 --Don't update Primary Key or Identity columns
 IF NOT EXISTS(
 SELECT 1
 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
 INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
 WHERE pk.TABLE_NAME = @table_name
 AND pk.TABLE_SCHEMA = COALESCE(@schema, SCHEMA_NAME())
 AND CONSTRAINT_TYPE = 'PRIMARY KEY'
 AND c.TABLE_NAME = pk.TABLE_NAME
 AND c.TABLE_SCHEMA = pk.TABLE_SCHEMA
 AND c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
 AND c.COLUMN_NAME = @Column_Name_Unquoted 
 )
 BEGIN
 SET @Column_List_For_Update = @Column_List_For_Update + @Column_Name + ' = Source.' + @Column_Name + ', 
  ' 
 SET @Column_List_For_Check = @Column_List_For_Check +
 CASE @Data_Type 
 WHEN 'text' THEN @b + CHAR(9) + 'NULLIF(CAST(Source.' + @Column_Name + ' AS VARCHAR(MAX)), CAST(Target.' + @Column_Name + ' AS VARCHAR(MAX))) IS NOT NULL OR NULLIF(CAST(Target.' + @Column_Name + ' AS VARCHAR(MAX)), CAST(Source.' + @Column_Name + ' AS VARCHAR(MAX))) IS NOT NULL OR '
 WHEN 'ntext' THEN @b + CHAR(9) + 'NULLIF(CAST(Source.' + @Column_Name + ' AS NVARCHAR(MAX)), CAST(Target.' + @Column_Name + ' AS NVARCHAR(MAX))) IS NOT NULL OR NULLIF(CAST(Target.' + @Column_Name + ' AS NVARCHAR(MAX)), CAST(Source.' + @Column_Name + ' AS NVARCHAR(MAX))) IS NOT NULL OR ' 
 ELSE @b + CHAR(9) + 'NULLIF(Source.' + @Column_Name + ', Target.' + @Column_Name + ') IS NOT NULL OR NULLIF(Target.' + @Column_Name + ', Source.' + @Column_Name + ') IS NOT NULL OR '
 END 
 END

 SKIP_LOOP: --The label used in GOTO

 SELECT @Column_ID = MIN(ORDINAL_POSITION) 
 FROM INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
 WHERE TABLE_NAME = @table_name
 AND TABLE_SCHEMA = COALESCE(@schema, SCHEMA_NAME())
 AND ORDINAL_POSITION > @Column_ID

 END --Loop ends here!


--To get rid of the extra characters that got concatenated during the last run through the loop
IF LEN(@Column_List_For_Update) <> 0
 BEGIN
 SET @Column_List_For_Update = ' ' + LEFT(@Column_List_For_Update,len(@Column_List_For_Update) - 4)
 END

IF LEN(@Column_List_For_Check) <> 0
 BEGIN
 SET @Column_List_For_Check = LEFT(@Column_List_For_Check,len(@Column_List_For_Check) - 3)
 END

SET @Actual_Values = LEFT(@Actual_Values,len(@Actual_Values) - 6)

SET @Column_List = LEFT(@Column_List,len(@Column_List) - 1)
IF LEN(LTRIM(@Column_List)) = 0
 BEGIN
 RAISERROR('No columns to select. There should at least be one column to generate the output',16,1)
 RETURN -1 --Failure. Reason: Looks like all the columns are ommitted using the @cols_to_exclude parameter
 END


--Get the join columns ----------------------------------------------------------
DECLARE @PK_column_list VARCHAR(8000)
DECLARE @PK_column_joins VARCHAR(8000)
SET @PK_column_list = ''
SET @PK_column_joins = ''

SELECT TOP 100 PERCENT @PK_column_list = @PK_column_list + '[' + c.COLUMN_NAME + '], '
, @PK_column_joins = @PK_column_joins + 'Target.[' + c.COLUMN_NAME + '] = Source.[' + c.COLUMN_NAME + '] AND '
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
WHERE pk.TABLE_NAME = @table_name
AND pk.TABLE_SCHEMA = COALESCE(@schema, SCHEMA_NAME())
AND CONSTRAINT_TYPE = 'PRIMARY KEY'
AND c.TABLE_NAME = pk.TABLE_NAME
AND c.TABLE_SCHEMA = pk.TABLE_SCHEMA
AND c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
ORDER BY
c.ORDINAL_POSITION 
IF IsNull(@PK_column_list, '') = '' 
 BEGIN
 RAISERROR('Table has no primary keys. There should at least be one column in order to have a valid join.',16,1)
 RETURN -1 --Failure. Reason: looks like table doesn't have any primary keys
 END

SET @PK_column_list = LEFT(@PK_column_list, LEN(@PK_column_list) -1)
SET @PK_column_joins = LEFT(@PK_column_joins, LEN(@PK_column_joins) -4)


--Forming the final string that will be executed, to output the a MERGE statement
SET @Actual_Values = 
 'SELECT ' + 
 CASE WHEN @top IS NULL OR @top < 0 THEN '' ELSE ' TOP ' + LTRIM(STR(@top)) + ' ' END + 
 '''' + 
 ' '' + CASE WHEN ROW_NUMBER() OVER (ORDER BY ' + @PK_column_list + ') = 1 THEN '' '' ELSE '','' END + ''(''+ ' + @Actual_Values + '+'')''' + ' ' + 
 COALESCE(@from,' FROM ' + @Source_Table_Qualified + ' (NOLOCK) ORDER BY ' + @PK_column_list)

 DECLARE @output VARCHAR(MAX) = ''
 

--Determining whether to ouput any debug information
IF @debug_mode =1
 BEGIN
 SET @output += @b + '/*****START OF DEBUG INFORMATION*****'
 SET @output += @b + ''
 SET @output += @b + 'The primary key column list:'
 SET @output += @b + @PK_column_list
 SET @output += @b + ''
 SET @output += @b + 'The INSERT column list:'
 SET @output += @b + @Column_List
 SET @output += @b + ''
 SET @output += @b + 'The UPDATE column list:'
 SET @output += @b + @Column_List_For_Update
 SET @output += @b + ''
 SET @output += @b + 'The SELECT statement executed to generate the MERGE:'
 SET @output += @b + @Actual_Values
 SET @output += @b + ''
 SET @output += @b + '*****END OF DEBUG INFORMATION*****/'
 SET @output += @b + ''
 END
 
IF (@include_use_db = 1)
BEGIN
	SET @output += @b + 'USE ' + DB_NAME()
	SET @output += @b + @batch_separator
	SET @output += @b + @b
END

IF (@nologo = 0)
BEGIN
 SET @output += @b + '--MERGE generated by ''sp_generate_merge'' stored procedure, Version 0.93'
 SET @output += @b + '--Originally by Vyas (http://vyaskn.tripod.com): sp_generate_inserts (build 22)'
 SET @output += @b + '--Adapted for SQL Server 2008/2012 by Daniel Nolan (http://danere.com)'
 SET @output += @b + '--Adapted for FlexyGo by JJR'
 SET @output += @b + ''
END

SET @output += @b 
 SET @output += @b + 'BEGIN TRY'


IF (@include_rowsaffected = 1) -- If the caller has elected not to include the "rows affected" section, let MERGE output the row count as it is executed.
 SET @output += @b + 'SET NOCOUNT ON'
 SET @output += @b + ''


--Determining whether to print IDENTITY_INSERT or not
IF (LEN(@IDN) <> 0)
 BEGIN
 SET @output += @b + 'SET IDENTITY_INSERT ' + @Target_Table_For_Output + ' ON'
 SET @output += @b + ''
 END


--Temporarily disable constraints on the target table
IF @disable_constraints = 1 AND (OBJECT_ID(@Source_Table_Qualified, 'U') IS NOT NULL)
 BEGIN
 SET @output += @b + 'ALTER TABLE ' + @Target_Table_For_Output + ' NOCHECK CONSTRAINT ALL' --Code to disable constraints temporarily
 END


--Output the start of the MERGE statement, qualifying with the schema name only if the caller explicitly specified it
SET @output += @b + 'MERGE INTO ' + @Target_Table_For_Output + ' AS Target'
SET @output += @b + 'USING (VALUES'


--All the hard work pays off here!!! You'll get your MERGE statement, when the next line executes!
DECLARE @tab TABLE (ID INT NOT NULL PRIMARY KEY IDENTITY(1,1), val NVARCHAR(max));
INSERT INTO @tab (val)
EXEC (@Actual_Values)

IF (SELECT COUNT(*) FROM @tab) <> 0 -- Ensure that rows were returned, otherwise the MERGE statement will get nullified.
BEGIN
 SET @output += CAST((SELECT @b + val FROM @tab ORDER BY ID FOR XML PATH('')) AS XML).value('.', 'VARCHAR(MAX)');
END

--Output the columns to correspond with each of the values above--------------------
SET @output += @b + ') AS Source (' + @Column_List + ')'


--Output the join columns ----------------------------------------------------------
SET @output += @b + 'ON (' + @PK_column_joins + ')'


--When matched, perform an UPDATE on any metadata columns only (ie. not on PK)------
IF LEN(@Column_List_For_Update) <> 0
BEGIN
 SET @output += @b + 'WHEN MATCHED ' + CASE WHEN @update_only_if_changed = 1 THEN 'AND (' + @Column_List_For_Check + ') ' ELSE '' END + 'THEN'
 SET @output += @b + ' UPDATE SET'
 SET @output += @b + '  ' + LTRIM(@Column_List_For_Update)
END


--When NOT matched by target, perform an INSERT------------------------------------
SET @output += @b + 'WHEN NOT MATCHED BY TARGET THEN';
SET @output += @b + ' INSERT(' + @Column_List + ')'
SET @output += @b + ' VALUES(' + REPLACE(@Column_List, '[', 'Source.[') + ')'


--When NOT matched by source, DELETE the row
IF @delete_if_not_matched=1 BEGIN
	IF @HasOriginId = 1 BEGIN
		SET @output += @b + 'WHEN NOT MATCHED BY SOURCE AND TARGET.OriginId = ' + CAST(@OriginId AS VARCHAR(5)) + ' THEN '
	END ELSE BEGIN
		SET @output += @b + 'WHEN NOT MATCHED BY SOURCE THEN '
	END
 SET @output += @b + ' DELETE'
END;
SET @output += @b + ';'



--Display the number of affected rows to the user, or report if an error occurred---
IF @include_rowsaffected = 1
BEGIN
 SET @output += @b + 'DECLARE @mergeCount int'
 SET @output += @b + 'SELECT @mergeCount = @@ROWCOUNT'
 SET @output += @b + ' PRINT ''' + @Target_Table_For_Output + ' rows affected by MERGE: '' + CAST(@mergeCount AS VARCHAR(100));';
 SET @output += @b + @b
END



SET @output += @b + 'END TRY'
SET @output += @b + 'BEGIN CATCH'

SET @output += @b + '    DECLARE @ERRORNUMBER	INT,@ERRORMSG		VARCHAR(MAX),@ERRORSTATE		INT'
SET @output += @b + '    SELECT @ERRORNUMBER = 50000 + ERROR_NUMBER(),@ERRORMSG = ERROR_MESSAGE(), @ERRORSTATE = ERROR_STATE();'
SET @output += @b + '    THROW @ERRORNUMBER, @ERRORMSG, @ERRORSTATE'
SET @output += @b + 'END CATCH'


 SET @output += @b + @batch_separator
 SET @output += @b + @b

--Re-enable the previously disabled constraints-------------------------------------
IF @disable_constraints = 1 AND (OBJECT_ID(@Source_Table_Qualified, 'U') IS NOT NULL)
 BEGIN
 SET @output +=      'ALTER TABLE ' + @Target_Table_For_Output + ' CHECK CONSTRAINT ALL' --Code to enable the previously disabled constraints
 SET @output += @b + @batch_separator
 SET @output += @b
 END


--Switch-off identity inserting------------------------------------------------------
IF (LEN(@IDN) <> 0)
 BEGIN
 SET @output +=      'SET IDENTITY_INSERT ' + @Target_Table_For_Output + ' OFF'
 SET @output += @b + @batch_separator
 SET @output += @b
 END

IF (@include_rowsaffected = 1)
BEGIN
 SET @output +=      'SET NOCOUNT OFF'
 SET @output += @b + @batch_separator
 SET @output += @b
END

SET @output += @b + ''
SET @output += @b + ''

SET @output = replace(@output,'$',''' + NCHAR(36) + N''')

IF @results_to_text = 1
BEGIN
	--output the statement to the Grid/Messages tab
	SELECT @output;
END
ELSE
BEGIN
	--output the statement as xml (to overcome SSMS 4000/8000 char limitation)
	IF @ReturnSql = 1 BEGIN
		SELECT @OutputSQL = @output
	END ELSE BEGIN
		SELECT [processing-instruction(x)]=@output FOR XML PATH(''),TYPE;
	END
	
	PRINT 'MERGE statement has been wrapped in an XML fragment and output successfully.'
	PRINT 'Ensure you have Results to Grid enabled and then click the hyperlink to copy the statement within the fragment.'
	PRINT ''
	PRINT 'If you would prefer to have results output directly (without XML) specify @results_to_text = 1, however please'
	PRINT 'note that the results may be truncated by your SQL client to 4000 nchars.'
END

SET NOCOUNT OFF
RETURN 0 --Success. We are done!
END
GO
PRINT N'Creando Procedimiento [dbo].[pAlbaranDeVenta]...';


GO
CREATE PROCEDURE [dbo].[pAlbaranDeVenta] (@parametros varchar(max))
AS
BEGIN TRY		
	-----------------------------------------------------------------------------------------------
	--									Parámetros JSON 
	-----------------------------------------------------------------------------------------------
	declare   @numero varchar(20)	= (select JSON_VALUE(@parametros,'$.numero'))
			, @letra char(2)		= (select JSON_VALUE(@parametros,'$.letra'))
			, @bultos varchar(5)	= (select JSON_VALUE(@parametros,'$.bultos'))
			, @cajas varchar(5)		= (select JSON_VALUE(@parametros,'$.cajas'))
			, @impAlb int			= isnull(cast((select JSON_VALUE(@parametros,'$.impAlb')) as int),0)
			, @impEti int			= isnull(cast((select JSON_VALUE(@parametros,'$.impEti')) as int),0)
			, @FacturaDirecta int	= isnull(cast((select JSON_VALUE(@parametros,'$.FacturaDirecta')) as int),0)
			, @usuarioSesion varchar(50)= (select JSON_VALUE(@parametros,'$.usuarioSesion'))			

	declare @usuario varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].usuario')),'')
			, @rol varchar(50) = isnull((select JSON_VALUE(@parametros,'$.params[0].rol')),'')
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EMPRESA char(2), @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select @EMPRESA=Empresa, @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------

	declare @sql varchar(max)
	declare @nu varchar(50) = replace(replace(replace(replace(replace(convert(varchar,getdate(),121),' ',''),'/',''),':',''),'.',''),'-','') 

	declare   @nEti int = 1
			, @CrearAV varchar(20) = ''
			, @printerEti varchar(100) = ''
			, @printerDoc varchar(100) = ''
			, @reportEti  varchar(100) = ''
			, @reportAlb  varchar(100) = ''
			, @reportFac  varchar(100) = ''
			, @msj varchar(max) = ''
			, @unico varchar(50) = ''
			, @elPDF varchar(100) = ''
			, @error int = 0
			, @cliente varchar(20)
			, @copiasAlb int = 1
			, @copiasFac int = 1	
	

	update [Traspaso_Temporal] set bultos=@bultos, eticajas=@cajas, impAlb=@impAlb, impEti=@impEti where empresa=@EMPRESA and NUMERO=@numero and LETRA=@letra	

	-- Equipo del Usuario -- (DISTECO)
	declare @EquipoUsuario varchar(50) = 'WebApp'
	if @usuarioSesion='1544458254550078' set @EquipoUsuario='PC-CAMARA2'
	if @usuarioSesion='1544458275396522' set @EquipoUsuario='DS4JX7688'

	-- obtener impresoras y reports ---------------------------------------------------------------------------------
	if @impEti=1 begin
		set @printerEti = (select isnull(impEti,'') from [usuarios_impresoras] where usuario=@usuario)		
		set @reportEti  = (select isnull(reportEtiquetas,'') from [usuarios_reports] where usuario=@usuario)
		if @reportEti='' set @reportEti = (select isnull(report,'') from [ConfigIdiomaReport] where idioma = 
											(select top 1 IDIOMA_IMP from d_Busca_PV 
											where concat(empresa,numero,letra) collate Modern_Spanish_CI_AI=concat(@EMPRESA,@numero,@letra)) collate Modern_Spanish_CI_AI
											and LEFT(tipo,2)='03'
										  )
		if @reportEti='' set @reportEti = (select isnull(report,'') from [ConfigIdiomaReport] where idioma='000' and LEFT(tipo,2)='03')
		if  @printerEti='' or @reportEti='' set @error=1
	end
	
	if @impAlb=1 begin
		set @printerDoc = (select impDocu from [usuarios_impresoras] where usuario=@usuario)
		set @reportAlb  = (select reportAlbaran from [usuarios_reports] where usuario=@usuario)
		if	@reportAlb='' set @reportAlb = (select isnull(report,'') from [ConfigIdiomaReport] where idioma = 
											(select top 1 IDIOMA_IMP from d_Busca_PV 
											where concat(empresa,numero,letra) collate Modern_Spanish_CI_AI=concat(@EMPRESA,@numero,@letra)) collate Modern_Spanish_CI_AI
											and LEFT(tipo,2)='01'
										  )
		if @reportAlb='' set @reportAlb = (select isnull(report,'') from [ConfigIdiomaReport] where idioma='000' and LEFT(tipo,2)='01')			
		if @printerDoc='' or @reportAlb=''set @error=2
	end
	
	if @FacturaDirecta=1 begin
		set @printerDoc = (select impDocu from [usuarios_impresoras] where usuario=@usuario)
		set @reportFac  = (select reportFactura from [usuarios_reports] where usuario=@usuario)
		if	@reportFac='' set @reportFac = (select isnull(report,'') from [ConfigIdiomaReport] where idioma = 
											(select top 1 IDIOMA_IMP from d_Busca_PV 
											where concat(empresa,numero,letra) collate Modern_Spanish_CI_AI=concat(@EMPRESA,@numero,@letra)) collate Modern_Spanish_CI_AI
											and LEFT(tipo,2)='02'
										  )
		if @reportFac='' set @reportFac = (select isnull(report,'') from [ConfigIdiomaReport] where idioma='000' and LEFT(tipo,2)='02')
		if  @printerDoc='' or @reportFac=''set @error=3
	end

	--	actualizar C_PEDIVE TRASPASADO=1 y eliminar registro tabla [Traspasar_Pedido]
		EXEC('update ['+@GESTION+'].dbo.c_pedive set TRASPASADO=1 where CONCAT(EMPRESA,NUMERO,LETRA)=CONCAT('''+@EMPRESA+''','''+@numero+''','''+@letra+''')')
		delete [Traspasar_Pedido] where CONCAT(empresa,numero,letra)=CONCAT(@EMPRESA,@numero,@letra)

	-- --------------------------------------------------------------------------------------------------------------
	--	generar albarán de venta 
		EXEC [pCrearAV] @EMPRESA, @numero, @letra, @EquipoUsuario, @impAlb, @impEti, @CrearAV output, @nEti
	-- --------------------------------------------------------------------------------------------------------------

	-- tabla temporal para recopilación de datos ------------------------------------------------------
	declare @tablaTemp TABLE (C_ALBAVEN int, copiasFac int)
	SET @sql = 'select C_ALBAVEN as C_ALBAVEN
				, (select COPIA_FRA from ['+@GESTION+'].dbo.clientes where CODIGO='''+@cliente+''') as copiasFac
				from ['+@GESTION+'].dbo.factucnf'
	INSERT @tablaTemp EXEC(@sql)	
	SELECT @copiasAlb=C_ALBAVEN, @copiasFac=copiasFac FROM @tablaTemp
	delete @tablaTemp

	-- guardar datos en tabla de impresión --------------------------------------------------------------------------
	if @error=0 begin
		if @impEti=1 or (select activo from ConfigApp where objeto='impEtiquetas')=1 begin
			set @unico = 'E' + replace(replace(replace(replace(replace(convert(varchar(50),getdate(),121),' ',''),'/',''),':',''),'.',''),'-','')
			set @elPDF = replace(CONCAT('Etiquetas_',@EMPRESA,@letra,@CrearAV,'.pdf'),' ','_');
			insert into MI_ImpresionPDF (id, usuario, terminal, tipo, pdf, impresora, crReport, crNumero, crEmpresa, crLetra, bultos) 
			values (@unico,@usuario,'WebApp','Etiquetas',@elPDF,@printerEti,@reportEti,@CrearAV,@EMPRESA,@letra,@bultos)
		end

		if @impAlb=1 or (select activo from ConfigApp where objeto='impAlbGen')=1 begin 
			set @unico ='A' +  replace(replace(replace(replace(replace(convert(varchar(50),getdate(),121),' ',''),'/',''),':',''),'.',''),'-','')
			set @elPDF = replace(CONCAT('Albaran',@EMPRESA,@letra,@CrearAV,'.pdf'),' ','_');
			insert into MI_ImpresionPDF (id, usuario, terminal, tipo, pdf, impresora, crReport, crNumero, crEmpresa, crLetra) 
			values (@unico,@usuario,'WebApp','Albaran',@elPDF,@printerDoc,@reportAlb,ltrim(rtrim(@CrearAV)),@EMPRESA,@letra)
		end

		if @FacturaDirecta=1 begin
			set @cliente = (select CLIENTE from vFacturas where empresa=@EMPRESA and numfra=CONCAT(@letra,@numero))
			set @unico = 'F' + replace(replace(replace(replace(replace(convert(varchar(50),getdate(),121),' ',''),'/',''),':',''),'.',''),'-','')
			set @elPDF = replace(CONCAT('"Factura"',@EMPRESA,@letra,@CrearAV,'.pdf'),' ','_');
			insert into MI_ImpresionPDF (id, usuario, terminal, tipo, pdf, impresora, crReport, crNumero, crEmpresa, crLetra) 
			values (@unico,@usuario,'WebApp','Factura',@elPDF,@printerDoc,@reportFac,ltrim(rtrim(@CrearAV)),@EMPRESA,@letra)
		end
	end
	
	select @CrearAV as JAVASCRIPT

	RETURN -1
END TRY
BEGIN CATCH	
	DECLARE @CatchError NVARCHAR(MAX)
	SET @CatchError=ERROR_MESSAGE()+char(13)+ERROR_NUMBER()+char(13)+ERROR_PROCEDURE()+char(13)+@@PROCID+char(13)+ERROR_LINE()
	RAISERROR(@CatchError,12,1)
	select CONCAT('{"error":"',@CatchError,'"}') as JAVASCRIPT
	RETURN 0
END CATCH
GO
PRINT N'Creando Procedimiento [dbo].[pCrearFA]...';


GO
CREATE PROCEDURE [dbo].[pCrearFA]
(
	@empresa varchar(2) = '', 
	@letra varchar(2) = '',
	@numero varchar(10)= '',
	@fecha_fac char(11) = '',
	@terminal CHAR(50) = '',
	@imprimir INT = 0,
	@imp_eti INT = 0,
	@ETICAJAS VARCHAR(7) = '',
	@bultos VARCHAR(7) = '',
	@nEti BIT = 0,
	@respuestaSP VARCHAR(100)='' output				
)			
AS
BEGIN TRY

	SET NOCOUNT ON;
	-----------------------------------------------------------------------------------------------
	--								Variables de Configuración  
	-----------------------------------------------------------------------------------------------
	declare @EJERCICIO char(4), @GESTION char(6), @COMUN char(8), @LOTES char(8)
	select  @EJERCICIO=Ejercicio, @GESTION=Gestion, @COMUN=Comun, @LOTES=Lotes from Configuracion
	-------------------------------------------------------------------------------------------------	
	-----------------------------------------------------------------------------------------------
	--								   Variables Dinamización  
	-----------------------------------------------------------------------------------------------
	declare @cont int
	declare @tbInt TABLE (dato int)
	declare @tbNum TABLE (dato numeric(15,6))
	declare @tbStr TABLE (dato varchar(1000))
	-----------------------------------------------------------------------------------------------

	DECLARE @Mensaje varchar(max), @MensajeError varchar(max)
	DECLARE @NUMFACTURA varchar(10)='', @FECHA varchar(10), @USUARIO varchar(25)='MERLOS#API#', @NUMASIENTO int, @ASI varchar(20), 
			@CLIENTE varchar(10), @NOMCLI varchar(100), @DIVISA varchar(3), @CAMBIO numeric(20,6), @GUID1 uniqueidentifier, @TOTALDOC numeric(15,6), 
			@CUENTA varchar(10), @ASI2 varchar(20), @LINASI integer = 1, @TIPOIVA varchar(2), @PORIVA numeric(20,2), @NMES int, @BASE numeric(15,6), 
			@RECAR numeric (15,6), @PORREC numeric(20,2), @CMES varchar(4), @CMES2 varchar(6), @DIAPAG int, @DIAPAG2 int, @MANDATO varchar(35), 
			@VENCIM varchar(10), @NUMGIROS int=0, @TOTALFAC numeric(15,6), @VALGIRO numeric(15,6), @FPAG varchar(2), @VENDEDOR varchar(2), 
			@HABER numeric(15,6), @DEBE numeric(15,6), @CTABAL2 varchar(4), @DIASGIRO int, @SECU varchar(4), @PORSE numeric(20,4), @CTAPORTES varchar(10), 
			@IVAPORT varchar(2), @PORIVAPOR numeric(20,2), @CLIFINAL varchar(10), @PRONTO NUMERIC(20,2), @IVATOT NUMERIC(20,2), @SIREC bit=0, 
			@IVAREC NUMERIC(20,2), @CLIIVA varchar(2), @IVACLI NUMERIC(20,2)=0.00, @RECDIV numeric(15,6)=0.00, @NUM_FA varchar(10)='', 
			@CUENTAREC varchar(10)='', @LINASIP integer = 1, @ASI3 varchar(20)

	declare @NROREG INT=0, @NROREG2 INT=0 ---para conteos en ciclos 26/10/2019

	SET @NMES=	(select MONTH(getdate()))
	SET @CMES=	(select right(year(getdate()),2)+right(replicate('0', 2)+ ltrim(rtrim(str(@NMES))), 2))
	SET @CMES2=	(select right(year(getdate()),4)+right(replicate('0', 2)+ ltrim(rtrim(str(@NMES))), 2))

	declare @tbCAV TABLE (CLIENTE varchar(10), DIVISA varchar(3), CAMBIO numeric(20,6), NUMFACTURA varchar(10), NOMCLI varchar(100), TOTALDOC numeric(15,6)
						, DIAPAG int, DIAPAG2 int, MANDATO varchar(35), TOTALFAC numeric(15,6), FPAG varchar(2), VENDEDOR varchar(2), CLIFINAL varchar(10)
						, PRONTO NUMERIC(20,2), SIREC bit, CLIIVA varchar(2))
	INSERT	@tbCAV EXEC('select C_ALBVEN.CLIENTE, C_ALBVEN.DIVISA, C_ALBVEN.CAMBIO, C_ALBVEN.FACTURA, NOMBRE, C_ALBVEN.TOTALDOC, CLIENTES.DIAPAG, CLIENTES.DIAPAG2
						, C_ALBVEN.MANDATO, =C_ALBVEN.TOTALDOC,	CLIENTES.FPAG, C_ALBVEN.VENDEDOR, CLIENTES.CLIFINAL, C_ALBVEN.PRONTO, CLIENTES.RECARGO, CLIENTES.TIPO_IVA
						FROM ['+@GESTION+'].DBO.C_ALBVEN 
						left join ['+@GESTION+'].DBO.CLIENTES on C_ALBVEN.CLIENTE=CLIENTES.CODIGO 
						where C_ALBVEN.EMPRESA='''+@empresa+''' and C_ALBVEN.LETRA='''+@letra+''' and C_ALBVEN.NUMERO='''+@numero+'''')	
	SELECT	@CLIENTE=CLIENTE, @DIVISA=DIVISA, @CAMBIO=CAMBIO, @NUMFACTURA=NUMFACTURA, @NOMCLI=NOMCLI, @TOTALDOC=TOTALDOC, @DIAPAG=DIAPAG, @DIAPAG2=DIAPAG2, @MANDATO=MANDATO
			, @TOTALFAC=TOTALDOC, @FPAG=FPAG, @VENDEDOR=VENDEDOR, @CLIFINAL=CLIFINAL, @PRONTO=PRONTO, @SIREC=SIREC, @CLIIVA=CLIIVA 
			FROM @tbCAV		
	delete	@tbCAV

	if ltrim(rtrim(isnull(@cliente,'')))=''
	begin 
		set @respuestaSP='No existe el albaran de venta'+@letra+' '+@numero
		SET @MensajeError='","ERROR":"'
		raiserror ('No existe el albarán de venta',16,1);
		return 0
	end
	if ltrim(rtrim(isnull(@NUMFACTURA,'')))<>''
	begin
		set @respuestaSP='Albarán ya Facturado '+@letra+' '+@numero
		SET @MensajeError='","ERROR":"'
		raiserror ('Albarán ya facturado ',16,1);
		return 0
	end

	declare @tbSeries TABLE (NUM_FA varchar(10))
	INSERT  @tbSeries EXEC('SELECT @letra+SPACE(8-LEN(CAST(CONTADOR+1 AS VARCHAR(8))))+CAST(CONTADOR+1 AS VARCHAR(8))
						    from ['+@GESTION+'].dbo.series where empresa='''+@empresa+''' and serie='''+@letra+''' and TIPODOC=7')	
	SELECT  @NUM_FA=NUM_FA FROM @tbSeries		
	delete  @tbSeries
	EXEC('UPDATE ['+@GESTION+'].dbo.series set CONTADOR=CONTADOR+1 where empresa='''+@empresa+''' and serie='''+@letra+''' and TIPODOC=7')	
				
	-- Albert R. 30/06/2020. Obtengo fecha factura asignada en el albarán de venta
	declare @tbFecha TABLE (fecha int)
	INSERT  @tbFecha EXEC('select CONVERT(CHAR(11), fecha_fac, 103) from ['+@GESTION+'].dbo.c_albven where empresa='''+@empresa+''' and numero='''+@numero+''' and letra='''+@letra+'''')	
	SELECT  @fecha=fecha FROM @tbFecha		
	delete  @tbFecha
				
	SET @NUMFACTURA=REPLACE(@NUM_FA, ' ', '')
	--Iniciamos registro contable
				
	-- Albert R. 30/06/20. Este punto se ha hecho porque varias veces que se ha intentado facturar, salta error de registro duplicado en los asientos. A ver sin con esto lo evitamos.
	declare @tbNumAsiento TABLE (NUMASIENTO int)
	INSERT  @tbNumAsiento EXEC('SELECT asiento+1 FROM ['+@GESTION+'].DBO.empresa WHERE codigo='''+@empresa+'''')	
	SELECT  @NUMASIENTO=NUMASIENTO FROM @tbNumAsiento		
	delete  @tbNumAsiento

	declare @tbAsientos TABLE (asientos int)
	INSERT  @tbAsientos EXEC('SELECT count(NUMERO) FROM ['+@GESTION+'].dbo.asientos WHERE NUMERO='+@NUMASIENTO)	
	SELECT  @NUMASIENTO=asientos FROM @tbAsientos		
	delete  @tbAsientos

	declare @asientosMax int
	INSERT  @tbAsientos EXEC('SELECT MAX(NUMERO)+1 FROM ['+@GESTION+'].dbo.asientos')	
	SELECT  @asientosMax=asientos FROM @tbAsientos		
	delete  @tbAsientos

	IF @NUMASIENTO>0 SET @NUMASIENTO=@asientosMax
				
	EXEC('UPDATE ['+@GESTION+'].dbo.empresa  SET asiento='+@NUMASIENTO+' WHERE codigo='''+@empresa+'''')
				
	SET @GUID1=NEWID()
	SET @ASI=(select 'M'+ltrim(@NUMFACTURA)+ltrim(convert(char(11),getdate(),112)))
	-- Llenamos el primer registro del asiento con el total por la cuenta del cliente
	-- 16/10/2019 El Cliente se cambia por clifinal si existe
	IF LTRIM(RTRIM(ISNULL(@CLIFINAL,'')))!='' SET @CLIENTE=@CLIFINAL

	EXEC('INSERT INTO ['+@GESTION+'].dbo.asientos  (usuario,empresa,numero,linea,cuenta,fecha,definicion,debe,haber,tipo,
	punteo,factura,proveedor,asi,importediv,divisa,debediv,haberdiv,cambio,referencia,guid)
	values('''+@USUARIO+''', '''+@empresa+''', '''+@NUMASIENTO+''', '''+@LINASI+''', '''+@CLIENTE+''', '''+@FECHA+''', ''N/ FRA ''+'''+@NUM_FA+''''
		+', '+@TOTALDOC+', 0, 0, 0, '''+@NUM_FA+''', '''', '''+@ASI+''', '+@TOTALDOC+', '''+@DIVISA+''', '+@TOTALDOC+', 0, '''+@CAMBIO+''''
		+', ltrim(str('''+@NUMASIENTO+''')), '''+@GUID1+''')')
	--Fin primer registro de asiento
				
	--09/10/2019 insertamos secundarias si existen --------------------------------------------------------------------------------------------
	declare @tbSecundarias TABLE (mykey int, secundaria varchar(50), porcentaje numeric(16,4), nroreg bigint)
	INSERT  @tbSecundarias EXEC('select null mykey, secundaria, porcentaje, row_number() over(order by secundaria) as nroreg 
								 from ['+@GESTION+'].DBO.otras where codigo='''+@CLIENTE+''' order by secundaria')

	while (select count(*) from @tbSecundarias where isnull(mykey, 0)= 0 ) > 0 /* @@rowcount > 0 */	begin
		SET @NROREG=(SELECT MIN(NROREG) FROM @tbSecundarias where isnull(mykey, 0)= 0)
		SELECT @SECU=SECUNDARIA, @PORSE=PORCENTAJE from @tbSecundarias where nroreg=@NROREG
		EXEC('INSERT INTO ['+@GESTION+'].dbo.otrasien (empresa,secundar,codcuen,fecha,debe,haber,asi,debediv,haberdiv,divisa) 
			VALUES ('''+@empresa+''','''+@SECU+''','''+@CLIENTE+''','''+@FECHA+''','+@TOTALDOC+'*'+@PORSE+'/100, 0.0000,'''+@ASI+''',0.0000,0.0000,''   '')')
		update @tbSecundarias set mykey=1 where nroreg=@NROREG
	end
	delete  @tbSecundarias
	--fin secundarias  ------------------------------------------------------------------------------------------------------------------------

	SET @CUENTA=@CLIENTE

	-------------------------------------------------------------------------------------------------------------------------------------------
	--08/10/2019 ACTUALIZAMOS SALDOS, BAL1 y BAL2
	declare @tbSaldos TABLE (dato numeric(15,6))
	insert @tbSaldos EXEC('select DEBE from ['+@GESTION+'].dbo.saldos where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
	select @DEBE=dato from @tbSaldos
	delete @tbSaldos
	if ISNULL(@DEBE, -1)=-1
		EXEC('INSERT INTO ['+@GESTION+'].dbo.saldos (empresa,cuenta,debe,haber,importediv,divisa) VALUES ('''+@empresa+''','''+@CUENTA+''','''+@TOTALDOC+''',0.000000,0.000000,''   '')')
	else 
		EXEC('update ['+@GESTION+'].dbo.saldos SET DEBE='+@DEBE+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		
	--bal1
	insert @tbSaldos EXEC('select DEBE from ['+@GESTION+'].dbo.bal1 where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
	select @DEBE=dato from @tbSaldos
	delete @tbSaldos
	if ISNULL(@DEBE, -1)=-1
		EXEC('insert into ['+@GESTION+'].dbo.bal1(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CUENTA+''','+@TOTALDOC+', 0.000000,'''+@CMES+''','' '','''+@CMES2+''')')
	else 
		EXEC('update ['+@GESTION+'].dbo.bal1 SET DEBE='+@DEBE+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
	
	SET @CTABAL2=(select LEFT(@CUENTA, 4))
	--bal2
	insert @tbSaldos EXEC('select DEBE from ['+@GESTION+'].dbo.bal2 where EMPRESA='''+@empresa+''' and CUENTA='''+@CTABAL2+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
	select @DEBE=dato from @tbSaldos
	delete @tbSaldos
	if ISNULL(@DEBE, -1)=-1
		EXEC('insert into ['+@GESTION+'].dbo.bal2(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CTABAL2+''','+@TOTALDOC+', 0.000000,'''+@CMES+''','' '','''+@CMES2+''')')
	else 
		EXEC('update ['+@GESTION+'].dbo.bal2 SET DEBE='+@DEBE+'+'+@TOTALDOC+' WHERE empresa='''+@EMPRESA+''' and cuenta='''+@CTABAL2+''' and mes='''+@CMES+''' and MES2='''+@CMES2+'''')
	
	---fin actualiza saldos -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
				
	--Actualizamos cabecera de albarán con la factura creada
	EXEC('UPDATE ['+@GESTION+'].DBO.C_ALBVEN set FACTURA='''+@NUM_FA+''', FECHA_FAC='''+@FECHA+''', ASI='''+@ASI+''', EXPORTAR=null 
		  WHERE EMPRESA='''+@empresa+''' and LETRA='''+@letra+''' and NUMERO='''+@numero+'''')

	--hacemos un ciclo por el detalle de las facturas para hacer los registros contables
	--cada registro lleva un asi diferente pero el primero comparte el asi con la cabecera del albarán
	--detalle de items de la factura agrupados por cuenta de ventas de la familia

	--09/05/2019 arreglo para que saque los valores sin el calculo del pronto pago
	declare @tbValores TABLE (mykey int, CTA_VENTA varchar(50), importe numeric(15,6), nroreg bigint)
	insert @tbValores EXEC('select null mykey, f.CTA_VENTA, round(sum(d.IMPORTE+d.PVERDE),2) as importe,
							row_number() over(order by f.CTA_VENTA) as nroreg 
							FROM ['+@GESTION+'].DBO.D_ALBVEN d 
							join ['+@GESTION+'].DBO.articulo a on d.ARTICULO=a.CODIGO 
							join ['+@GESTION+'].dbo.familias f on a.FAMILIA=f.CODIGO 
							where d.EMPRESA='''+@empresa+''' and d.LETRA='''+@letra+''' and d.NUMERO='''+@numero+'''
							group by f.CTA_VENTA')
	while (select count(*) from @tbValores where isnull(mykey, 0)= 0 ) > 0  begin   --@@rowcount > 0	
		SET @NROREG=(SELECT MIN(NROREG) FROM @tbValores where isnull(mykey, 0)= 0)
		select @CUENTA=CTA_VENTA, @TOTALDOC=importe from @tbValores where nroreg=@NROREG
		SET @LINASI=@LINASI+1
		SET @GUID1=NEWID()
		SET @ASI2=(SELECT RIGHT(newid(),20))   

		declare @fraCli varchar(100) = 'N/ FRA '+@NUM_FA+' '+@NOMCLI
		EXEC('INSERT INTO ['+@GESTION+'].dbo.asientos  (usuario,empresa,numero,linea,cuenta,fecha,definicion,debe,haber,tipo,
		punteo,factura,proveedor,asi,importediv,divisa,debediv,haberdiv,cambio,referencia,guid)
		values('''+@USUARIO+''', '''+@empresa+''', '''+@NUMASIENTO+''', '''+@LINASI+''', '''+@CUENTA+''', '''+@FECHA+'''
		, '''+@fraCli+''', 0, '''+@TOTALDOC+''', 0, 0, '''+@NUM_FA+''', '''', '''+@ASI2+''', '''+@TOTALDOC+''', '''+@DIVISA+'''
		, 0, '''+@TOTALDOC+''', '''+@CAMBIO+''', ltrim(str('''+@NUMASIENTO+''')), '''+@GUID1+''')')

		--09/10/2019 insertamos secundarias si existen
		INSERT  @tbSecundarias EXEC('select null mykey, secundaria, porcentaje, row_number() over(order by secundaria) as nroreg  
									 from ['+@GESTION+'].DBO.otras where codigo='''+@CUENTA+'''')

		while (select count(*) from @tbSecundarias where isnull(mykey, 0)= 0 ) > 0 BEGIN -- @@rowcount > 0
			SET @NROREG2=(SELECT MIN(NROREG) FROM @tbSecundarias where isnull(mykey, 0)= 0)
			SELECT @SECU=SECUNDARIA, @PORSE=PORCENTAJE from @tbSecundarias where nroreg=@NROREG2
			EXEC('INSERT INTO ['+@GESTION+'].dbo.otrasien (empresa,secundar,codcuen,fecha,debe,haber,asi,debediv,haberdiv,divisa) 
				 VALUES ('''+@empresa+''','''+@SECU+''','''+@CUENTA+''','''+@FECHA+''',0.0000,'+@TOTALDOC+'*'+@PORSE+'/100,'''+@ASI2+''', 0.0000, 0.0000, ''   '')')

			update @tbSecundarias set mykey = 1 where nroreg=@NROREG2
		end 
		delete @tbSecundarias
		--fin secundarias
				
		--09/10/2019 actualizamos/creamos el registro de la cuenta en bal1, bal2 y saldos 
		--08/10/2019 ACTUALIZAMOS SALDOS, BAL1 y BAL2
		insert @tbSaldos EXEC('select HABER from ['+@GESTION+'].dbo.saldos where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		select @HABER=dato from @tbSaldos
		delete @tbSaldos
		declare @elHaber numeric(15,6) = COALESCE(@HABER,0.0000)
		if ISNULL(@HABER, -1)=-1
			EXEC('INSERT INTO ['+@GESTION+'].dbo.saldos (empresa,cuenta,debe,haber,importediv,divisa) VALUES ('''+@empresa+''','''+@CUENTA+''',0.000000,'''+@elHaber+''',0.000000,''   '')')
		else 
			EXEC('update ['+@GESTION+'].dbo.saldos SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		
		--bal1
		insert @tbSaldos EXEC('select HABER from ['+@GESTION+'].dbo.bal1 where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		select @HABER=dato from @tbSaldos
		delete @tbSaldos
		if ISNULL(@HABER, -1)=-1
			EXEC('insert into ['+@GESTION+'].dbo.bal1(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CUENTA+''',0.000000,'''+@TOTALDOC+''', '''+@CMES+''','' '','''+@CMES2+''')')
		else 
			EXEC('update ['+@GESTION+'].dbo.bal1 SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		
		SET @CTABAL2=(select LEFT(@CUENTA, 4))
		--bal2
		insert @tbSaldos EXEC('select HABER from ['+@GESTION+'].dbo.bal2 where EMPRESA='''+@empresa+''' and CUENTA='''+@CTABAL2+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		select @HABER=dato from @tbSaldos
		delete @tbSaldos
		if ISNULL(@HABER, -1)=-1
			EXEC('insert into ['+@GESTION+'].dbo.bal2(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CTABAL2+''',0.000000,'+@TOTALDOC+', '''+@CMES+''','' '','''+@CMES2+''')')
		else 
			EXEC('update ['+@GESTION+'].dbo.bal2 SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE empresa='''+@EMPRESA+''' and cuenta='''+@CTABAL2+''' and mes='''+@CMES+''' and MES2='''+@CMES2+''')')
		
		---fin actualiza saldos

		update @tbValores set mykey = 1 where nroreg=@NROREG
	end
	delete @tbValores
				
	--17/10/2019 Si el cliente tiene un iva puesto, este se superpone a todos los ivas que existan
	IF @CLIIVA!='' BEGIN
		insert @tbNum EXEC('SELECT IVA FROM ['+@GESTION+'].dbo.tipo_iva where CODIGO='''+@CLIIVA+'''')
		SELECT @IVACLI=dato FROM @tbNum
		delete @tbNum
	END

	--10/10/2018 Registros de Iva vinculado a cada artículo agrupados por cuenta
	declare @tbIVAVinc TABLE (mykey int, CODIGO char(2), IVA numeric(20,2), CTA_IV_REP char(8), CTA_RE_REP char(8), RECARG numeric(20,2), total numeric(15,6), importe numeric(15,6), nroreg bigint)
	insert @tbIVAVinc EXEC('select null mykey, isnull(f.CODIGO,'''') as CODIGO, CASE WHEN '''+@CLIIVA+'''!='''' THEN '''+@IVACLI+''' ELSE isnull(f.IVA,0.00) END AS IVA, f.CTA_IV_REP, f.CTA_RE_REP,
							case when '+@SIREC+'=1 then isnull(f.RECARG,0.00) else 0.00 end as RECARG, 
							round(sum(d.IMPORTE+d.PVERDE)-(sum(d.IMPORTE+d.PVERDE)*'+@PRONTO+'/100),2) as total, 
							round(sum(d.IMPORTE+d.PVERDE)-(sum(d.IMPORTE+d.PVERDE)*'+@PRONTO+'/100),2) as importe, 
							row_number() over(order by isnull(f.codigo,'''')) as nroreg
							FROM ['+@GESTION+'].DBO.D_ALBVEN d 
							join ['+@GESTION+'].dbo.tipo_iva f on d.TIPO_IVA=f.CODIGO 
							where d.EMPRESA='''+@empresa+''' and d.LETRA='''+@letra+''' and d.NUMERO='''+@numero+'''
							group by f.CODIGO, f.IVA, f.CTA_IV_REP, f.CTA_RE_REP, f.RECARG')
--26/03/2019 ya tengo la variable recargo en @SIREC la cuenta es codcom!recfinan

	while (select count(*) from @tbIVAVinc where isnull(mykey, 0)= 0 ) > 0  
	begin

		SET @NROREG=(SELECT MIN(nroreg) from @tbIVAVinc where isnull(mykey, 0)= 0 )
		select @CUENTA=CTA_IV_REP, @BASE=total, @TOTALDOC=importe, @PORIVA=IVA, @TIPOIVA=CODIGO, @PORREC=RECARG, @CUENTAREC=CTA_RE_REP 
		from @tbIVAVinc where nroreg=@NROREG

		SET @LINASI=@LINASI+1
		SET @GUID1=NEWID()
		SET @ASI2=(SELECT RIGHT(newid(),20)) 
		SET @RECAR=round(@BASE*@PORREC/100,2) 
		SET @RECDIV=@RECAR*@CAMBIO

		EXEC('INSERT INTO ['+@GESTION+'].dbo.asientos  (usuario,empresa,numero,linea,cuenta,fecha,definicion,debe,haber,tipo,
			  punteo,factura,proveedor,asi,importediv,divisa,debediv,haberdiv,cambio,referencia,guid)
			  values('''+@USUARIO+''', '''+@empresa+''', '+@NUMASIENTO+', '+@LINASI+', '''+@CUENTA+''', '''+@FECHA+''', ''N/ FRA ''+'''+@NUM_FA+'''+'' ''+'''+@NOMCLI+'''
			  , 0, ROUND('+@BASE+'*'+@PORIVA+'/100,2), 0, 0, '''+@NUM_FA+''', '''', '''+@ASI2+''', ROUND('+@BASE+'*'+@PORIVA+'/100,2), '''+@DIVISA+''', 0
			  , ROUND('+@BASE+'*'+@PORIVA+'/100,2), '''+@CAMBIO+''', ltrim(str('+@NUMASIENTO+')), '''+@GUID1+''')')

		--27/03/2019 CLIENTE RECARGO
		SET @LINASI=@LINASI+1
		SET @GUID1=NEWID()
				
		declare @tempasi varchar(20)
		SET @tempasi=(SELECT RIGHT(newid(),20))
		if @RECAR>0
			EXEC('INSERT INTO ['+@GESTION+'].dbo.asientos  (usuario,empresa,numero,linea,cuenta,fecha,definicion,debe,haber,tipo,
			punteo,factura,proveedor,asi,importediv,divisa,debediv,haberdiv,cambio,referencia,guid)
			values('''+@USUARIO+''', '''+@empresa+''', '+@NUMASIENTO+', '+@LINASI+', '''+@CUENTAREC+''', '''+@FECHA+''', ''N/ FRA ''+'''+@NUM_FA+'''+'' ''+'''+@NOMCLI+'''
			, 0, '+@RECAR+', 0,	0, '''+@NUM_FA+''', '''', '''+@tempasi+''', '''+@RECAR+''', '''+@DIVISA+''', 0, '''+@RECAR+''', '''+@CAMBIO+''', ltrim(str('+@NUMASIENTO+')), '''+@GUID1+''')')

		EXEC('
			INSERT INTO ['+@GESTION+'].dbo.ivareper(empresa,cuenta,numfra,tipo_iva,asi,fecha,bimpo,porcen_iva,iva,porcen_rec,
			recargo,liquidacio,comunitari,bimpodiv,ivadiv,recdiv,divisa,nombre,cif,finan,
			finandi,tipo,regtrans,liq_op, ejerliq_op,fechaoper,totcob,fecmax,critcaja) 
			VALUES('''+@empresa+''','''+@CLIENTE+''','''+@NUM_FA+''','''+@TIPOIVA+''','''+@ASI2+''','''+@FECHA+''','''+@BASE+''','''+@PORIVA+''',ROUND('+@BASE+'*'+@PORIVA+'/100,2),'''+@PORREC+'''
			, '+@RECAR+', 0, 0, '+@BASE+', ROUND('+@BASE+'*'+@PORIVA+'/100,2),'''+@RECDIV+''', '''+@DIVISA+''','''','''',0.0000,0.0000,1,0,0,0,'''+@FECHA+''',0,null,0)
		')

		--09/10/2019 insertamos secundarias si existen
		INSERT  @tbSecundarias EXEC('select null mykey, secundaria, porcentaje, row_number() over(order by secundaria) as nroreg  
									 from ['+@GESTION+'].DBO.otras where codigo='''+@CUENTA+'''')
		select null mykey, secundaria, porcentaje, row_number() over(order by secundaria) as nroreg from @tbSecundarias
		while (select count(*) from @tbSecundarias where isnull(mykey, 0)= 0 ) > 0 begin
			SET @NROREG2=(select min(nroreg) from @tbSecundarias where isnull(mykey, 0)= 0 )
			SELECT @SECU=SECUNDARIA, @PORSE=PORCENTAJE from @tbSecundarias where nroreg=@NROREG2
			EXEC('INSERT INTO ['+@GESTION+'].dbo.otrasien (empresa,secundar,codcuen,fecha,debe,haber,asi,debediv,haberdiv,divisa) 
				  VALUES ('''+@empresa+''','''+@SECU+''','''+@CUENTA+''','''+@FECHA+''', 0.0000, '+@TOTALDOC+'*'+@PORSE+'/100, '''+@ASI2+''', 0.0000,0.0000,''   '')')

			update @tbSecundarias set mykey = 1 where nroreg=@NROREG2
		end 
		delete @tbSecundarias
				
		--fin secundarias
		--09/10/2019 actualizamos/creamos el registro de la cuenta en bal1, bal2 y saldos 
		--08/10/2019 ACTUALIZAMOS SALDOS, BAL1 y BAL2
		insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.saldos where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		select @HABER=dato from @tbNum  delete @tbNum
		if ISNULL(@HABER, -1)=-1
			EXEC('INSERT INTO ['+@GESTION+'].dbo.saldos (empresa,cuenta,debe,haber,importediv,divisa) VALUES ('''+@empresa+''','''+@CUENTA+''',0.000000,'+@TOTALDOC+',0.000000,''   '')')   
		else
			EXEC('update ['+@GESTION+'].dbo.saldos SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		--bal1
		insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.bal1 where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		select @HABER=dato from @tbNum  delete @tbNum
		if ISNULL(@HABER, -1)=-1
			EXEC('insert into ['+@GESTION+'].dbo.bal1(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CUENTA+''',0.000000,'+@TOTALDOC+', '''+@CMES+''','' '','''+@CMES2+''')')
		else
			EXEC('update ['+@GESTION+'].dbo.bal1 SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		SET @CTABAL2=(select LEFT(@CUENTA, 4))
		--bal2
		insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.bal2 where EMPRESA='''+@empresa+''' and CUENTA='''+@CTABAL2+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		select @HABER=dato from @tbNum  delete @tbNum
		if ISNULL(@HABER, -1)=-1
			EXEC('insert into ['+@GESTION+'].dbo.bal2(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CTABAL2+''',0.000000,'+@TOTALDOC+', '''+@CMES+''','' '','''+@CMES2+''')')
		else
			EXEC('update ['+@GESTION+'].dbo.bal2 SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE empresa='''+@EMPRESA+''' and cuenta='''+@CTABAL2+''' and mes='''+@CMES+''' and MES2='''+@CMES2+'''')
		---fin actualiza saldos

		update @tbIVAVinc set mykey = 1 where nroreg=@NROREG
	end
	delete @tbIVAVinc

	--10/10/2018 anexamos los registros de portes si existen con su respectivo iva
	SET @LINASI=@LINASI+1
	SET @ASI2=(SELECT RIGHT(newid(),20))
	insert @tbStr EXEC('SELECT portes FROM ['+@COMUN+'].dbo.codcom')
	SELECT @CTAPORTES=dato FROM @tbStr delete @tbStr
	DECLARE @TOTPORTES NUMERIC(15,6)

	declare @tbPortes TABLE (TIPO_IVA char(2), TOTPORTES numeric(15,6))
	insert @tbPortes EXEC('SELECT isnull(TIPO_IVA,''''), isnull(IMPORTE,0.00) FROM ['+@GESTION+'].dbo.portes 
							WHERE empresa='''+@empresa+''' and albaran='''+@numero+''' and letra='''+@letra+''' and inc_fra=1')
	SELECT @IVAPORT=TIPO_IVA, @TOTPORTES=TOTPORTES FROM @tbPortes delete @tbPortes

if ISNULL(@TOTPORTES, -1)>0	begin
		SET @TOTALDOC=@TOTPORTES
						
		-- Albert 17/09/2020 - Si la cuenta ya existe hay que sumar los valores
		insert @tbInt EXEC('SELECT COUNT(EMPRESA) FROM ['+@GESTION+'].DBO.ASIENTOS WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='+@NUMASIENTO+' AND CUENTA='''+@CTAPORTES+'''')
		set @cont=(select dato from @tbInt) delete @tbInt	
		IF @cont>0 BEGIN
				EXEC('UPDATE ['+@GESTION+'].dbo.asientos SET HABER=HABER+'+@TOTALDOC+', IMPORTEDIV=IMPORTEDIV+'+@TOTALDOC+', HABERDIV=HABERDIV+'+@TOTALDOC+' 
						WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='+@NUMASIENTO+' AND CUENTA='''+@CTAPORTES+'''')
				insert @tbInt EXEC('SELECT ASI FROM ['+@GESTION+'].DBO.ASIENTOS WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='+@NUMASIENTO+' AND CUENTA='''+@CTAPORTES+'''')
				SET @ASI2=(select dato from @tbInt) delete @tbInt	
		END ELSE				
			EXEC('INSERT INTO ['+@GESTION+'].dbo.asientos  
					(usuario,empresa,numero,linea,cuenta,fecha,definicion,debe,haber,tipo,punteo,factura,proveedor,asi,importediv,divisa,debediv,haberdiv,cambio,referencia,guid) 
				VALUES ('''+@USUARIO+''','''+@empresa+''', '+@NUMASIENTO+', '+@LINASI+', '''+@CTAPORTES+''', '''+@FECHA+''',''N/ FRA ''+@NUM_FA+'' ''+'''+@NOMCLI+''', 0.0000
				, '+@TOTALDOC+',''0'', 0, '''+@NUM_FA+''',''          '', '''+@ASI2+''', '+@TOTALDOC+',''000'',0.0000,'+@TOTALDOC+',1.000000, ltrim(str('+@NUMASIENTO+')), NEWID())')
						
		--09/10/2019 insertamos secundarias si existen
		set rowcount 0
		INSERT @tbSecundarias EXEC('select null mykey, secundaria, porcentaje, row_number() over(order by secundaria) as nroreg from ['+@GESTION+'].DBO.otras where codigo='''+@CUENTA+'''')

		while (select count(*) from @tbSecundarias where isnull(mykey,0)= 0)>0
		begin
			set rowcount 0
			SET @NROREG=(select min(nroreg) from @tbSecundarias where isnull(mykey, 0)= 0 )
			SELECT @SECU=SECUNDARIA, @PORSE=PORCENTAJE from @tbSecundarias where nroreg=@NROREG

			-- Albert 17/09/2020 - Si la SECUNDARIA ya existe hay que sumar los valores
			insert @tbInt EXEC('SELECT COUNT(EMPRESA) FROM ['+@GESTION+'].DBO.otrasien WHERE EMPRESA='''+@EMPRESA+''' AND SECUNDAR='''+@SECU+''' AND ASI='''+@ASI2+'''')
			set @cont=(select dato from @tbInt) delete @tbInt	
			IF @cont>0
				EXEC('UPDATE ['+@GESTION+'].dbo.otrasien SET HABER=HABER+ROUND('+@TOTALDOC+'*'+@PORSE+'/100,2) WHERE EMPRESA='''+@EMPRESA+''' AND SECUNDAR='''+@SECU+''' AND ASI='''+@ASI2+'''')
			ELSE
				EXEC('INSERT INTO ['+@GESTION+'].dbo.otrasien (empresa,secundar,codcuen,fecha,debe,haber,asi,debediv,haberdiv,divisa) 
					  VALUES ('''+@empresa+''','''+@SECU+''','''+@CUENTA+''','''+@FECHA+''', 0.0000, ROUND('+@TOTALDOC+'*'+@PORSE+'/100,2), '''+@ASI2+''', 0.0000,0.0000,''   '')')

			update @tbSecundarias set mykey=1 where nroreg=@NROREG
		end 
		delete @tbSecundarias
		--fin secundarias

		--09/10/2019 actualizamos/creamos el registro de la cuenta en bal1, bal2 y saldos 
		--08/10/2019 ACTUALIZAMOS SALDOS, BAL1 y BAL2
		insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.saldos where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		select @HABER=(select dato from @tbNum) delete @tbNum
		if ISNULL(@HABER, -1)=-1
			EXEC('INSERT INTO ['+@GESTION+'].dbo.saldos (empresa,cuenta,debe,haber,importediv,divisa) VALUES ('''+@empresa+''','''+@CUENTA+''',0.000000,'+@HABER+',0.000000,''   '')')         
		else
			EXEC('update ['+@GESTION+'].dbo.saldos SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		--bal1
		insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.bal1 where EMPRESA=@'''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		select @HABER=(select dato from @tbNum) delete @tbNum
		if ISNULL(@HABER, -1)=-1
			EXEC('insert into ['+@GESTION+'].dbo.bal1(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CUENTA+''',0.000000,'+@TOTALDOC+', '''+@CMES+''','' '','''+@CMES2+''')')
		else
			EXEC('update ['+@GESTION+'].dbo.bal1 SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		SET @CTABAL2=(select LEFT(@CUENTA, 4))
		--bal2
		insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.bal2 where EMPRESA='''+@empresa+''' and CUENTA='''+@CTABAL2+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		select @HABER=(select dato from @tbNum) delete @tbNum
		if ISNULL(@HABER, -1)=-1
			EXEC('insert into ['+@GESTION+'].dbo.bal2(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CTABAL2+''',0.000000,'+@TOTALDOC+', '''+@CMES+''','' '','''+@CMES2+''')')
		else
			EXEC('update ['+@GESTION+'].dbo.bal2 SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE empresa='''+@EMPRESA+''' and cuenta='''+@CTABAL2+''' and mes='''+@CMES+''' and MES2='''+@CMES2+'''')
		---fin actualiza saldos
			
		
		declare @tbTipoIVA TABLE (IVA numeric (20,2), CTA_IV_REP varchar(10), RECARG numeric (20,2))
		insert 	@tbTipoIVA EXEC('SELECT IVA, CTA_IV_REP, case when '+@SIREC+'=1 then RECARG else 0.00 end FROM ['+@GESTION+'].dbo.tipo_iva WHERE codigo='''+@IVAPORT+'''')
		SELECT  @PORIVAPOR=IVA, @CUENTA=CTA_IV_REP, @PORREC=RECARG FROM @tbTipoIVA
		delete  @tbTipoIVA
						
		SET @ASI2=(SELECT RIGHT(newid(),20))

		--ingresa el ivareper del valor total del registro de asiento anterior
		SET @RECAR=round(@TOTPORTES*@PORREC/100,2)
		SET @RECDIV=@RECAR*@CAMBIO
		SET @IVAPORT=isnull(@IVAPORT,'')

		IF @IVAPORT!= '' BEGIN
			-- Albert 17/09/2020 - Si el tipo de IVA ya existe hay que sumar los valores
			insert @tbInt EXEC('SELECT COUNT(EMPRESA) FROM ['+@GESTION+'].dbo.ivareper WHERE EMPRESA='''+@EMPRESA+''' AND NUMFRA='''+@NUM_FA+''' AND TIPO_IVA='''+@IVAPORT+'''')
			set @cont = (select dato from @tbInt) delete @tbInt
			IF @cont>0
				EXEC('UPDATE ['+@GESTION+'].dbo.ivareper SET BIMPO=BIMPO+'+@TOTPORTES+', IVA=IVA+ROUND('+@TOTPORTES+'*'+@PORIVAPOR+'/100,2), RECARGO=RECARGO+'+@RECAR+'
					, BIMPODIV=BIMPODIV+'+@TOTPORTES+', IVADIV=IVADIV+ROUND('+@TOTPORTES+'*'+@PORIVAPOR+'/100,2), RECDIV=RECDIV+'+@RECAR+' 
					WHERE EMPRESA='''+@EMPRESA+''' AND NUMFRA='''+@NUM_FA+''' AND TIPO_IVA='''+@IVAPORT+'''')
			ELSE
				EXEC('INSERT INTO ['+@GESTION+'].dbo.ivareper  (empresa,cuenta,numfra,tipo_iva,asi,fecha,bimpo,porcen_iva,iva,porcen_rec,recargo,liquidacio,comunitari,
					bimpodiv,ivadiv,recdiv,divisa,nombre,cif,finan,finandi,tipo,regtrans,liq_op, ejerliq_op,fechaoper,totcob,fecmax,critcaja) 
				VALUES('''+@empresa+''', '''+@CLIENTE+''', '''+@NUM_FA+''', '''+@IVAPORT+''', '''+@ASI2+''', '''+@FECHA+''', '+@TOTPORTES+', '+@PORIVAPOR+'
				, ROUND('+@TOTPORTES+'*'+@PORIVAPOR+'/100,2), '+@PORREC+', '+@RECAR+', 0, 0, '+@TOTPORTES+', ROUND('+@TOTPORTES+'*'+@PORIVAPOR+'/100,2)
				, '+@RECDIV+', '''+@DIVISA+''','''','''',0.0000,0.0000,1,0,0,0,'''+@FECHA+''',0,null,0)')
								
			DECLARE @TOTALPOR DECIMAL(15,3)
			SET @TOTALPOR=@TOTALDOC
								
			SET @TOTALDOC=ROUND(@TOTALDOC*@PORIVAPOR/100,2)
			SET @LINASI=@LINASI+1
								
			-- Albert 17/09/2020 - Si la CUENTA ya existe hay que sumar los valores
			insert @tbInt EXEC('SELECT COUNT(EMPRESA) FROM ['+@GESTION+'].DBO.ASIENTOS WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='+@NUMASIENTO+' AND CUENTA='''+@CUENTA+'''')
			set @cont=(select dato from @tbInt) delete @tbInt
			IF @cont>0
				EXEC('UPDATE ['+@GESTION+'].dbo.asientos SET HABER=HABER+'+@TOTALDOC+', IMPORTEDIV=IMPORTEDIV+'+@TOTALDOC+', HABERDIV=HABERDIV+'+@TOTALDOC+' 
					  WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='+@NUMASIENTO+' AND CUENTA='''+@CUENTA+'''')
			ELSE				
				EXEC('INSERT INTO ['+@GESTION+'].dbo.asientos  (usuario,empresa,numero,linea,cuenta,fecha,definicion,debe,haber,tipo,
				punteo,factura,proveedor,asi,importediv,divisa,debediv,haberdiv,cambio,referencia,guid) 
				VALUES('''+@USUARIO+''','''+@empresa+''', '+@NUMASIENTO+', '''+@LINASI+''', '''+@CUENTA+''', '''+@FECHA+''',''N/ FRA ''+'''+@NUM_FA+'''+'' ''+'''+@NOMCLI+'''
				, 0.0000, '+@TOTALDOC+', ''0'', 0, '''+@NUM_FA+''','''', '''+@ASI2+''', '+@TOTALDOC+',''000'',0.0000,'+@TOTALDOC+',1.000000, ltrim(str('''+@NUMASIENTO+''')), NEWID())')
				
			-- Albert 23/09/2020 - Registro Recardo iva si el cliente trabaja con recargo Si la cuenta ya existe hay que sumar los valores
			IF @SIREC=1
				BEGIN
					DECLARE @RECPORT DECIMAL(10,3), @CTAPORT CHAR(20)
					declare @tIVA TABLE (RECARG numeric(20,2), CTA_RE_REP varchar(50))
					insert @tIVA EXEC('SELECT RECARG, CTA_RE_REP FROM ['+@GESTION+'].DBO.TIPO_IVA WHERE CODIGO='''+@IVAPORT+'''')
					SELECT @RECPORT=RECARG, @CTAPORT=CTA_RE_REP from @tIVA
					delete @tIVA
					insert @tbInt EXEC('SELECT COUNT(EMPRESA) FROM ['+@GESTION+'].DBO.ASIENTOS WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='+@NUMASIENTO+' AND CUENTA='''+@CTAPORT+'''')
					set @cont=(select dato from @tbInt) delete @tbInt
					IF @cont>0
						EXEC('UPDATE ['+@GESTION+'].dbo.asientos SET HABER=HABER+ROUND('+@TOTALPOR+'*('+@RECPORT+'/100),2), IMPORTEDIV=IMPORTEDIV+ROUND('+@TOTALPOR+'*(@RECPORT/100),2)
						, HABERDIV=HABERDIV+ROUND('+@TOTALPOR+'*('+@RECPORT+'/100),2)WHERE EMPRESA='''+@EMPRESA+''' AND NUMERO='+@NUMASIENTO+' AND CUENTA='''+@CTAPORT+'''')
					ELSE
						BEGIN
							SET @ASI3=(SELECT RIGHT(newid(),20))
							SET @LINASI=@LINASI+1
							EXEC('INSERT INTO ['+@GESTION+'].dbo.asientos  (usuario,empresa,numero,linea,cuenta,fecha,definicion,debe,haber,tipo,punteo,factura,proveedor,asi,importediv,divisa,debediv,haberdiv,cambio,referencia,guid) 
							VALUES('''+@USUARIO+''','''+@empresa+''', '+@NUMASIENTO+', '''+@LINASI+''', '''+@CTAPORT+''', '''+@FECHA+''',''N/ FRA ''+@NUM_FA+'' ''+'+@NOMCLI+'
							, 0.0000, ROUND('+@TOTALPOR+'*('+@RECPORT+'/100),2),''0'', 0, '''+@NUM_FA+''',''          '', '''+@ASI3+''',ROUND('+@TOTALPOR+'*('+@RECPORT+'/100),2),''000''
							,0.0000,ROUND('+@TOTALPOR+'*('+@RECPORT+'/100),2),1.000000, ltrim(str('+@NUMASIENTO+')), NEWID())')
						END
				END
									
								
				--09/10/2019 actualizamos/creamos el registro de la cuenta en bal1, bal2 y saldos 
				--08/10/2019 ACTUALIZAMOS SALDOS, BAL1 y BAL2
				insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.saldos where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
				select @HABER=dato from @tbNum delete @tbNum
				if ISNULL(@HABER, -1)=-1
					EXEC('INSERT INTO ['+@GESTION+'].dbo.saldos(empresa,cuenta,debe,haber,importediv,divisa) VALUES('''+@empresa+''','''+@CUENTA+''',0.000000,'+@HABER+',0.000000,''   '')')
				else
					EXEC('update ['+@GESTION+'].dbo.saldos SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
				--bal1
				insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.bal1 where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
				select @HABER=dato from @tbNum delete @tbNum				
				if ISNULL(@HABER, -1)=-1
					EXEC('insert into ['+@GESTION+'].dbo.bal1(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CUENTA+''',0.000000,'+@TOTALDOC+', '''+@CMES+''','' '','''+@CMES2+''')')
				else
					EXEC('update ['+@GESTION+'].dbo.bal1 SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
				SET @CTABAL2=(select LEFT(@CUENTA, 4))
				--bal2
				insert @tbNum EXEC('select HABER from ['+@GESTION+'].dbo.bal2 where EMPRESA='''+@empresa+''' and CUENTA='''+@CTABAL2+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
				select @HABER=dato from @tbNum delete @tbNum				
				if ISNULL(@HABER, -1)=-1
					EXEC('insert into ['+@GESTION+'].dbo.bal2(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CTABAL2+''',0.000000,'+@TOTALDOC+', '''+@CMES+''','' '','''+@CMES2+''')')
				else
					EXEC('update ['+@GESTION+'].dbo.bal2 SET HABER='+@HABER+'+'+@TOTALDOC+' WHERE empresa='''+@EMPRESA+''' and cuenta='''+@CTABAL2+''' and mes='''+@CMES+''' and MES2='''+@CMES2+'''')
				---fin actualiza saldos
			END							
		END
				
	-- Albert R. 30/06/2020. Revisamos los giros del cliente, si este no tiene ningúno, añadimos un registro con el valor cero ya que sino no se crea la previsión.
	insert @tbInt EXEC('SELECT COUNT(EMPRESA) from ['+@GESTION+'].dbo.giro_cli where CLIENTE='''+@CLIENTE+'''')
	set @cont = (select dato from @tbInt) delete @tbInt
	IF @cont>0 EXEC('INSERT INTO ['+@GESTION+'].dbo.giro_cli (LINEA, CLIENTE, GIRO, VISTA) VALUES (1, '''+@CLIENTE+''', 0, 1)')

	--08/10/2019 agregamos los registros de previ_cl **segun los registros de giros del cliente en gestion!girocli
	declare @tbPREVICL TABLE (mykey int, LINEA int, GIRO int, nroreg bigint)
	insert @tbPREVICL EXEC('select null mykey, LINEA, GIRO, row_number() over(order by linea) as nroreg from ['+@GESTION+'].dbo.giro_cli where CLIENTE='''+@CLIENTE+'''')
	set @cont = (select count(LINEA) from @tbPREVICL)	
	if @cont>0 SET @NUMGIROS=@cont
	if @NUMGIROS<1 SET @NUMGIROS=1

	SET @VALGIRO= round(@TOTALFAC/@NUMGIROS,2)

	while (select count(*) from @tbPREVICL where isnull(mykey,0)=0)>0 begin
		SET @NROREG=(select min(nroreg) from @tbPREVICL where isnull(mykey, 0)= 0 )
		select @LINASIP=LINEA, @DIASGIRO=GIRO from @tbPREVICL where nroreg=@NROREG
		--
		SET @GUID1=NEWID()
		SET @ASI2=(SELECT RIGHT(newid(),20))
		SET @RECAR=round(@BASE*@PORREC/100,2)
		SET @VENCIM=(select CAST(CONVERT(char(11), dateadd(dd, @DIASGIRO, @FECHA), 103) as varchar))
		--09/10/2019 calculamos la fecha de pago si hay diapag y diapag2
		if @DIAPAG>0 begin
			--si el dia de la fecha calculada es menor al dia 1 entonces le asignamos el día 1
			if datepart(dd,@VENCIM)<@DIAPAG
				set @VENCIM=right(replicate('0', 2)+ ltrim(rtrim(str(@DIAPAG))), 2)+'/'+right(replicate('0', 2)+ ltrim(rtrim(str(DATEPART(mm,@VENCIM)))), 2)+'/'+str(datepart(yy,@VENCIM),4)
			--si el dia de la fecha calculada es mayor al día 1 verificamos si el día 2 aplica
			if DATEPART(dd,@VENCIM)>@DIAPAG begin
				--si no hay registrado dia 2 o este está registrado y el día de la fecha calculada es mayor, volvemos a la cuota del día 1
				if @DIAPAG2=0 or DATEPART(dd,@VENCIM)>@DIAPAG2 begin
						--le subimos al mes siguiente y ponemos el día 1
						set @VENCIM=(select CAST(CONVERT(char(11), dateadd(dd, 30, @VENCIM), 103) as varchar))
						set @VENCIM=right(replicate('0', 2)+ ltrim(rtrim(str(@DIAPAG))), 2)+'/'+right(replicate('0', 2)+ ltrim(rtrim(str(DATEPART(mm,@VENCIM)))), 2)+'/'+str(datepart(yy,@VENCIM),4)
				end else begin
					--si el dia de la fecha calculada es menor al día 2 se lo asignamos directamente
					if datepart(dd,@VENCIM)<@DIAPAG2
						set @VENCIM=right(replicate('0', 2)+ ltrim(rtrim(str(@DIAPAG2))), 2)+'/'+right(replicate('0', 2)+ ltrim(rtrim(str(DATEPART(mm,@VENCIM)))), 2)+'/'+str(datepart(yy,@VENCIM),4)
				end
			end
		end
				
		EXEC('INSERT INTO ['+@COMUN+'].dbo.previ_cl  (usuario,empresa,cliente,factura,orden,vencim,importe,banco,emision,impagado,impago,gastos,remesa,fecreme,
		asiento,impreso,cobro,vencim2,concepto,periodo,f_pago,pagare,imppagare,num_banco,pendiente,refundir,importediv,divisa,cambio,vendedor,cobrador,
		asi,recepcion,recibida,reclamacio,tipoprev,libre_1,fec_oper,doccargo,numcred,bco_org,mandato,recc) 
		VALUES('''+@USUARIO+''','''+@empresa+''','''+@CLIENTE+''','''+@NUM_FA+''','''+@LINASIP+''','''+@VENCIM+''','''+@VALGIRO+''','''','''+@FECHA+''',0,''N'',0.0000,0,null
				, 0,''N'',null,null,'''',DATEPART(yy,'''+@FECHA+'''),'''+@FPAG+''','''',0.0000,1,0,'''', '''+@VALGIRO+''', '''+@DIVISA+''', '''+@CAMBIO+''', '''+@VENDEDOR+'''
				, '''+@VENDEDOR+''','''',null,0,null,'''','''', '''+@FECHA+''','''','''','''', '''+@MANDATO+''',0)')
				
		EXEC('INSERT INTO ['+@COMUN+'].dbo.adiprevicl  (periodo,empresa,factura,orden,impagado,pendiente,liquicob,carga) 
				VALUES(DATEPART(yy,'''+@FECHA+'''), '''+@empresa+''', '''+@NUM_FA+''', 1 ,0 ,0 ,'''','''')')

		update @tbPREVICL set mykey = 1 where nroreg=@NROREG
	end
	delete @tbPREVICL
				
				
	--10/05/2019 ciclo pronto pago
	if @PRONTO>0 BEGIN
		SET @GUID1=NEWID()
		SET @ASI2=(SELECT RIGHT(newid(),20)) 
		insert @tbNum EXEC('SELECT ABS(SUM(DEBE-HABER)) FROM ['+@GESTION+'].dbo.asientos WHERE NUMERO='+@NUMASIENTO)
		SELECT @TOTALDOC=(select dato from @tbNum) delete @tbNum
		SET @LINASI=@LINASI+1
		insert @tbStr EXEC('select VDTOS from ['+@COMUN+'].dbo.codcom')
		select @CUENTA=(select dato from @tbStr) delete @tbStr

		EXEC('INSERT INTO ['+@GESTION+'].dbo.asientos  (usuario,empresa,numero,linea,cuenta,fecha,definicion,debe,haber,tipo,
		punteo,factura,proveedor,asi,importediv,divisa,debediv,haberdiv,cambio,referencia,guid) 
		VALUES('''+@USUARIO+''','''+@empresa+''', '+@NUMASIENTO+', '''+@LINASI+''', '''+@CTAPORTES+''', '''+@FECHA+''',''N/ FRA ''+'''+@NUM_FA+'''+'' ''+'''+@NOMCLI+'''
		, '+@TOTALDOC+', 0.0000,''0'', 0, '''+@NUM_FA+''',''          '', '''+@ASI2+''', '+@TOTALDOC+',''000'','+@TOTALDOC+',0.0000,1.000000, ltrim(str('+@NUMASIENTO+')), NEWID())')

		--10/05/2019 agrego registro de secundarias copiado
		--09/10/2018 insertamos secundarias si existen
		set rowcount 0
		INSERT @tbSecundarias EXEC('select null mykey, secundaria, porcentaje, row_number() over(order by secundaria) as nroreg from ['+@GESTION+'].DBO.otras where codigo='''+@CUENTA+'''')
		while (select count(*) from @tbSecundarias where isnull(mykey,0)=0)>0 begin
			set rowcount 0
			SET @NROREG=(select min(nroreg) from @tbSecundarias where isnull(mykey, 0)= 0 )
			SELECT @SECU=SECUNDARIA, @PORSE=PORCENTAJE from @tbSecundarias where nroreg=@NROREG
			EXEC('INSERT INTO ['+@GESTION+'].dbo.otrasien (empresa,secundar,codcuen,fecha,debe,haber,asi,debediv,haberdiv,divisa) 
			VALUES ('''+@empresa+''','''+@SECU+''','''+@CUENTA+''','''+@FECHA+''', ROUND('+@TOTALDOC+'*'+@PORSE+'/100,2), 0.0000, '''+@ASI2+''', 0.0000,0.0000,''   '')')

			update @tbSecundarias set mykey=1 where nroreg=@NROREG
		end 
		delete @tbSecundarias				
		--fin secundarias

		--09/10/2019 actualizamos/creamos el registro de la cuenta en bal1, bal2 y saldos 
		--08/10/2019 ACTUALIZAMOS SALDOS, BAL1 y BAL2
		insert @tbNum EXEC('select DEBE from ['+@GESTION+'].dbo.saldos where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		select @DEBE=dato from @tbNum delete @tbNum
		if ISNULL(@DEBE, -1)=-1
			EXEC('INSERT INTO ['+@GESTION+'].dbo.saldos (empresa,cuenta,debe,haber,importediv,divisa) VALUES ('''+@empresa+''','''+@CUENTA+''','+@DEBE+',0.000000,0.000000,''   '')')        
		else
			EXEC('update ['+@GESTION+'].dbo.saldos SET DEBE='+@DEBE+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+'''')
		--bal1
		insert @tbNum EXEC('select DEBE from ['+@GESTION+'].dbo.bal1 WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		select @DEBE=dato from @tbNum delete @tbNum		
		if ISNULL(@DEBE, -1)=-1
			EXEC('insert into ['+@GESTION+'].dbo.bal1(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CUENTA+''','+@TOTALDOC+', 0.000000,'''+@CMES+''','' '','''+@CMES2+''')')
		else
			EXEC('update ['+@GESTION+'].dbo.bal1 SET DEBE='+@DEBE+'+'+@TOTALDOC+' WHERE EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		SET @CTABAL2=(select LEFT(@CUENTA, 4))
		--bal2
		insert @tbNum EXEC('select @DEBE=DEBE from ['+@GESTION+'].dbo.bal2 where EMPRESA='''+@empresa+''' and CUENTA='''+@CUENTA+''' and MES='''+@CMES+''' and MES2='''+@CMES2+'''')
		select @DEBE=dato from @tbNum delete @tbNum			
		if ISNULL(@DEBE, -1)=-1
			EXEC('insert into ['+@GESTION+'].dbo.bal2(empresa,cuenta,debe,haber,mes,tempo,mes2) VALUES('''+@empresa+''','''+@CTABAL2+''','+@TOTALDOC+', 0.000000,'''+@CMES+''','' '','''+@CMES2+''')')
		else
			EXEC('update ['+@GESTION+'].dbo.bal2 SET DEBE='+@DEBE+'+'+@TOTALDOC+' WHERE empresa='''+@EMPRESA+''' and cuenta='''+@CTABAL2+''' and mes='''+@CMES+''' and MES2='''+@CMES2+'''')
		---fin actualiza saldos

	END
				
	-- Albert R. 20/11/18 - creación registro para las impresiones
	exec [pImprimir_Docs] @EMPRESA, @LETRA, @NUMERO, @terminal, @num_fa, @imprimir, @imp_eti, @ETICAJAS, @bultos, @nEti
				

	SELECT @respuestaSP='Factura nueva: '+@NUM_FA+' Fecha: '+@FECHA
	--COMMIT TRAN
	RETURN -1
END TRY
BEGIN CATCH 
	--ROLLBACK TRAN 
	DECLARE		@ErrorMessage NVARCHAR(4000);  
	DECLARE		@ErrorSeverity INT;  
	DECLARE		@ErrorState INT;  

	SELECT		@ErrorMessage = ERROR_MESSAGE(),  
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE();  

	RAISERROR	(@ErrorMessage, -- Message text.  
				@ErrorSeverity, -- Severity.  
				@ErrorState -- State.  
				);  
	SET @Mensaje='","ERROR":"NO se ha podido crear la factura'
	SELECT @Mensaje AS JAVASCRIPT
	RETURN 0
END CATCH
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET XACT_ABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

BEGIN TRANSACTION

EXEC sp_MSforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT all'

EXEC sp_MSforeachtable 'ALTER TABLE ? DISABLE TRIGGER all'

-- Data merge


-- Merlos
truncate table [AuxiliarBultos]
insert into [AuxiliarBultos] (NumBultos)
values (1)
		,(2),(2)
		,(3),(3),(3)
		,(4),(4),(4),(4)
		,(5),(5),(5),(5),(5)
		,(6),(6),(6),(6),(6),(6)
		,(7),(7),(7),(7),(7),(7),(7)
		,(8),(8),(8),(8),(8),(8),(8),(8)
		,(9),(9),(9),(9),(9),(9),(9),(9),(9)
		,(10),(10),(10),(10),(10),(10),(10),(10),(10),(10)


if not exists (select objeto from [ConfigApp] where objeto='FactCajas')		insert into [ConfigApp] (objeto, valor, activo)	values ('FactCajas','',0)
if not exists (select objeto from [ConfigApp] where objeto='ImpAlbFact')	insert into [ConfigApp] (objeto, valor, activo)	values ('ImpAlbFact','',0)
if not exists (select objeto from [ConfigApp] where objeto='ImpAlbGen')		insert into [ConfigApp] (objeto, valor, activo)	values ('ImpAlbGen','',0)
if not exists (select objeto from [ConfigApp] where objeto='ImpEtiquetas')	insert into [ConfigApp] (objeto, valor, activo)	values ('ImpEtiquetas','',0)
if not exists (select objeto from [ConfigApp] where objeto='ResLin')		insert into [ConfigApp] (objeto, valor, activo)	values ('ResLin','',0)
if not exists (select objeto from [ConfigApp] where objeto='SeriesFiltro')	insert into [ConfigApp] (objeto, valor, activo)	values ('SeriesFiltro','',0)
	

if not exists (select email from [ConfigEmail])	
insert into [ConfigEmail] (email, smtp, puerto, usuario, pswrd, tls)
values ('noreply@cliseller.com','smtp.ionos.es','587','noreply@cliseller.com','4N6Brt6IH4DKNTYzZfE@',1)


if not exists (select * from [Idiomas_Idiomas] where nombre='Por Defecto') insert into [Idiomas_Idiomas] (Codigo, Nombre) values ('000','Por Defecto')


if not exists (select * from [Idiomas_Tipos] where nombre='Albarán')	insert into [Idiomas_Tipos] (Codigo, Nombre) values ('01','Albarán')
if not exists (select * from [Idiomas_Tipos] where nombre='Factura')	insert into [Idiomas_Tipos] (Codigo, Nombre) values ('02','Factura')
if not exists (select * from [Idiomas_Tipos] where nombre='Etiquetas')	insert into [Idiomas_Tipos] (Codigo, Nombre) values ('03','Etiquetas')


GO

EXEC sp_MSforeachtable 'ALTER TABLE ? ENABLE TRIGGER all'

EXEC sp_MSforeachtable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT ALL'

COMMIT TRANSACTION

GO

GO
DECLARE @VarDecimalSupported AS BIT;

SELECT @VarDecimalSupported = 0;

IF ((ServerProperty(N'EngineEdition') = 3)
    AND (((@@microsoftversion / power(2, 24) = 9)
          AND (@@microsoftversion & 0xffff >= 3024))
         OR ((@@microsoftversion / power(2, 24) = 10)
             AND (@@microsoftversion & 0xffff >= 1600))))
    SELECT @VarDecimalSupported = 1;

IF (@VarDecimalSupported > 0)
    BEGIN
        EXECUTE sp_db_vardecimal_storage_format N'$(DatabaseName)', 'ON';
    END


GO
PRINT N'Actualización completada.';


GO
